package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.OpportunitiesDocService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.SalesStageService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.OpportunitiesUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/opportunity")
public class OpportunitiesRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OpportunitiesRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		OpportunitiesDO projectDO = new OpportunitiesDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			projectDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			projectDO.setContactId(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		projectDO.setProjectname(!inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : null);
			 		projectDO.setSalesStage(!inputJSON.get(CommonConstants.SALESSTAGE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.SALESSTAGE).toString()) : null);
			 		projectDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : null);
			 		projectDO.setProjectcode(!inputJSON.get(CommonConstants.PROJECTCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTCODE).toString() : null);
			 		projectDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		projectDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
			 		if(!inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
			 			projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		if(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			projectDO.setProjectType(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 		}
			 		projectDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		projectDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			projectDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		
			 		
			 	}
				new OpportunitiesService().persist(projectDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Opportunity Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(projectDO.getId()).toString();
	}
	
	/*@RequestMapping(value = "/retrieveActive{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<OpportunitiesDO> ProjectList = new OpportunitiesService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectList(ProjectList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/

	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<SalesStageDO> salesStageList = new SalesStageService().retrieveActive();
				respJSON = OpportunitiesUtil.getProjectChartList(salesStageList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveopp", method = RequestMethod.GET)
	public @ResponseBody String retrieveopp(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OpportunitiesDO> oppList = new OpportunitiesService().retrieveAll();
				respJSON = OpportunitiesUtil.getProjectListWithOutDoc(oppList).toString();
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
				}
				if(i==1){
					List<OpportunitiesDO> categoryList = new OpportunitiesService().retrieveAll();
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();
				}else{
					List<OpportunitiesDO> categoryList = new OpportunitiesService().retrieve(empIds);
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();
					//List<OpportunitiesDO> categoryList = new OpportunitiesService().retrieveAll();
					//respJSON = OpportunitiesUtil.getProjectListWithOutDoc(categoryList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		OpportunitiesDO projectDO = new OpportunitiesDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<OpportunitiesDO> ProjectList = new OpportunitiesService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		projectDO = ProjectList.get(0);
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			projectDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.COST).toString().isEmpty()){
			 			projectDO.setAmount(Long.parseLong(inputJSON.get(CommonConstants.COST).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.CONTACT_ID).toString().isEmpty()){
			 			projectDO.setContactId(Long.parseLong(inputJSON.get(CommonConstants.CONTACT_ID).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PROBOBILITY).toString().isEmpty()){
			 			projectDO.setProbability(Long.parseLong(inputJSON.get(CommonConstants.PROBOBILITY).toString()));
			 		}
			 		projectDO.setProjectname(!inputJSON.get(CommonConstants.PROJECTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTNAME).toString() : null);
			 		projectDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : null);
			 		projectDO.setProjectcode(!inputJSON.get(CommonConstants.PROJECTCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.PROJECTCODE).toString() : null);
			 		projectDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		projectDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : "");
			 		if(!inputJSON.get(CommonConstants.PROJECTDURATION).toString().isEmpty()){
				 		projectDO.setProjectduration(inputJSON.get(CommonConstants.PROJECTDURATION).toString());
			 		}
			 		projectDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		projectDO.setUpdatedon(new Date());
			 		//projectDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			projectDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new OpportunitiesService().update(projectDO);
			 		new OpportunitiesDocService().delete(projectDO.getId());
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Opportunity Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(projectDO.getId()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					//List<ProjectDO> ProjectList = new ProjectService().retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<OpportunitiesDO> ProjectDetail = new OpportunitiesService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectList(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<OpportunitiesDO> ProjectDetail = new OpportunitiesService().retrieveByCustomerId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OpportunitiesUtil.getProjectListWithOutDoc(ProjectDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				OpportunitiesDocDO opportunitiesDocDO = new OpportunitiesDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		opportunitiesDocDO.setOpportunityId(Long.parseLong(request.getParameter("id")));
			 		opportunitiesDocDO.setPhoto(request.getParameter("file"));
			 		opportunitiesDocDO.setFileName(request.getParameter("name"));
			 		new OpportunitiesDocService().persist(opportunitiesDocDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
