package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.DeductionTypeDAO;
import com.spheresuite.erp.domainobject.DeductionTypeDO;
import com.spheresuite.erp.exception.AppException;

public class DeductionTypeService {
	static Logger logger = Logger.getLogger(DeductionTypeService.class.getName());
	
	public DeductionTypeDO persist(DeductionTypeDO deductionTypeDO) throws AppException {
		return new DeductionTypeDAO().persist(deductionTypeDO);
	}

	public List<DeductionTypeDO> retrieveActive() throws AppException {
		return new DeductionTypeDAO().retrieveActive();
	}
	
	public List<DeductionTypeDO> retrieveById(Long id) throws AppException {
		return new DeductionTypeDAO().retrieveById(id);
	}
	
	public List<DeductionTypeDO> retrieve() throws AppException {
		return new DeductionTypeDAO().retrieve();
	}
	
	public DeductionTypeDO update(DeductionTypeDO deductionTypeDO) throws AppException {
		return new DeductionTypeDAO().update(deductionTypeDO);
	}
}
