package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;

public class NotesUtil {
	
	private NotesUtil() {}
	
	public static JSONObject getNotesList(List<NotesDO> notesList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (NotesDO notes : notesList) {
				resultJSONArray.put(getCategoryDetailObject(notes));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(NotesDO notes)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(notes.getId() != null ? notes.getId() : ""));
		result.put(CommonConstants.NOTES, String.valueOf(notes.getNote() != null ? notes.getNote() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToString(notes.getUpdatedon())));
		if(notes.getUpdatedBy() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(notes.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(notes.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(notes.getLeadId() != null ? notes.getLeadId() : ""));
		
		if(notes.getLeadId() != null){
			List<LeadDO> leads = new LeadService().retrieveById(notes.getLeadId());
			if(leads !=null && leads.size() > 0){
				result.put(CommonConstants.LEAD_NAME, String.valueOf(leads.get(0).getName()));
			}else{
				result.put(CommonConstants.LEAD_NAME,"");
			}
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
