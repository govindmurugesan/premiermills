package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProposalDetailsDAO;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.exception.AppException;

public class ProposalDetailsService {
	static Logger logger = Logger.getLogger(ProposalDetailsService.class.getName());
	
	public List<ProposalDetailsDO> persist(List<ProposalDetailsDO> proposalDetailsList) throws AppException {
		return new ProposalDetailsDAO().persist(proposalDetailsList);
	}
	
	public boolean delete(Long proposalId) throws AppException {
		return new ProposalDetailsDAO().delete(proposalId);
	}

	public List<ProposalDetailsDO> retrieveById(Long proposalId) throws AppException {
		return new ProposalDetailsDAO().retrieveById(proposalId);
	}
}
