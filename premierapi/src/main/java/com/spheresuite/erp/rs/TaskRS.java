package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.service.TaskService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TaskUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/task")
public class TaskRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaskRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TaskDO taskDO = new TaskDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.LEAD_ID).toString().isEmpty()){
			 			taskDO.setLeadId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEAD_ID).toString() : ""));
			 		}
			 		taskDO.setTaskDetails(!inputJSON.get(CommonConstants.TASKDETAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.TASKDETAIL).toString() : null);
			 		taskDO.setTaskDate(!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : null);
			 		taskDO.setTaskTime(!inputJSON.get(CommonConstants.TIME).toString().isEmpty() ? inputJSON.get(CommonConstants.TIME).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taskDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taskDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		taskDO.setStatus(CommonConstants.PENDING_STATUS);
			 		taskDO.setUpdatedon(new Date());
			 	}
				new TaskService().persist(taskDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Task Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadid(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
		 	if (inputJSON != null){
		 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 			List<TaskDO> taskList = new TaskService().retrieveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 			respJSON = TaskUtil.getTaskList(taskList).toString();
		 		}
		 		
		 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody String updateStatus(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
			TaskDO taskDO  = new TaskDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TaskDO> taskList = new TaskService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(taskList != null && taskList.size() > 0){
			 			taskDO = taskList.get(0);
			 			taskDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty() ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : CommonConstants.PENDING_STATUS);
			 			//taskDO.setStatus(inputJSON.get(CommonConstants.STATUS).toString().charAt(0));
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			taskDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
					 		taskDO.setUpdatedon(new Date());
				 		new TaskService().update(taskDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Task Updated");
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrievePendingTask", method = RequestMethod.GET)
	public @ResponseBody String retrievePendingTask(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			//JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
		 			List<TaskDO> taskList = new TaskService().retrievePendingTask();
		 			respJSON = TaskUtil.getTaskList(taskList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
