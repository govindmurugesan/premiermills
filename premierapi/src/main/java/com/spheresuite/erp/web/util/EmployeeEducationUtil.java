package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EducationLevelDO;
import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EducationLevelService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeEducationUtil {
	
	private EmployeeEducationUtil() {}
	
	public static JSONObject getEmployeeEducationList(List<EmployeeEducationDO> employeeEducationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeEducationDO employeeSkillsDO : employeeEducationList) {
				resultJSONArray.put(getEmployeeEducationDetailObject(employeeSkillsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeEducationDetailObject(EmployeeEducationDO employeeSkillsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeSkillsDO.getId()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(employeeSkillsDO.getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(employeeSkillsDO.getStateId()));
		CountryDAO obj = new CountryDAO();
		CountryDO workLocationdo = obj.retrieveByID(employeeSkillsDO.getCountryId());
		if(workLocationdo != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(workLocationdo.getName()));
		}
		StateService stateObj = new StateService();
		List<StateDO> stateDO = stateObj.retrieveById(employeeSkillsDO.getStateId());
		if(stateDO != null && stateDO.size() > 0){
			result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
		}
		result.put(CommonConstants.CITY, String.valueOf(employeeSkillsDO.getCity()));
		
		if(employeeSkillsDO.getEducationLevelId() != null){
			result.put(CommonConstants.EDUCATIONLEVELID, String.valueOf(employeeSkillsDO.getEducationLevelId()));
			List<EducationLevelDO> educationLevelList = new EducationLevelService().retrieveById(employeeSkillsDO.getEducationLevelId());
			if(educationLevelList != null && educationLevelList.size() > 0){
				result.put(CommonConstants.EDUCATIONLEVELNAME, String.valueOf(educationLevelList.get(0).getName()));
			}else{
				result.put(CommonConstants.EDUCATIONLEVELNAME, "");
			}
		}else{
			result.put(CommonConstants.EDUCATIONLEVELID, "");
			
			result.put(CommonConstants.EDUCATIONLEVELNAME, "");
		}
		result.put(CommonConstants.UNIVERSITY, String.valueOf(employeeSkillsDO.getUniversity()));
		result.put(CommonConstants.AREA, String.valueOf(employeeSkillsDO.getArea()));
		result.put(CommonConstants.YEAR, String.valueOf(employeeSkillsDO.getYearPassed()));
		return result;
	}
}
