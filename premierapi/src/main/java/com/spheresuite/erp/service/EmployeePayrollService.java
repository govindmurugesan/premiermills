package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeePayrollDAO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeePayrollService {
	static Logger logger = Logger.getLogger(EmployeePayrollService.class.getName());
	
	public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) throws AppException {
		return new EmployeePayrollDAO().persist(employeePayrollDO);
	}

	public List<EmployeePayrollDO> retrieveActive(Long empId) throws AppException {
		return new EmployeePayrollDAO().retrieveActive(empId);
	}
	
	public List<EmployeePayrollDO> retrieveById(Long id) throws AppException {
		return new EmployeePayrollDAO().retrieveById(id);
	}
	
	public List<EmployeePayrollDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeePayrollDAO().retrieveByEmpId(empId);
	}
	
	public Object[] retrieveByEmpIdBetweenDateSum(Long empId, String fromMnth, String toMonth) throws AppException {
		return new EmployeePayrollDAO().retrieveByEmpIdBetweenDateSum(empId, fromMnth, toMonth);
	}
	
	public Object[] retrieveByEmpIdForMonthSum(Long empId, String fromMnth) throws AppException {
		return new EmployeePayrollDAO().retrieveByEmpIdForMonthSum(empId, fromMnth);
	}
	
	public List<EmployeePayrollDO> retrieveByEmpIdBetweenDate(Long empId, String fromMnth, String toMonth) throws AppException {
		return new EmployeePayrollDAO().retrieveByEmpIdBetweenDate(empId, fromMnth, toMonth);
	}
	
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status) throws AppException {
		return new EmployeePayrollDAO().retrieveByAllEmp(payrollMonth, payrollbatchId, status);
	}
	
	public List<EmployeePayrollDO> retriveByMonth(String month, Long payrollBatchId) throws AppException {
		return new EmployeePayrollDAO().retriveByMonth(month, payrollBatchId);
	}
	
	public List<EmployeePayrollDO> retriveByStatus(String month, Long payrollBatchId) throws AppException {
		return new EmployeePayrollDAO().retriveByStatus(month, payrollBatchId);
	}
	
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, Long empId) throws AppException {
		return new EmployeePayrollDAO().retriveByMonthAndEmp(month, empId);
	}
	
	public List<EmployeePayrollDO> retrieve() throws AppException {
		return new EmployeePayrollDAO().retrieve();
	}
	
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO) throws AppException {
		return new EmployeePayrollDAO().update(employeePayrollDO);
	}
	
	public List<Object[]> retrieveForSummary() throws AppException {
		return new EmployeePayrollDAO().retrieveForSummary();
	}
	
	public List<EmployeePayrollDO> retriveForReportMonth(String month, Long batchId) throws AppException {
		return new EmployeePayrollDAO().retriveForReportMonth(month, batchId);
	}
	
	public List<EmployeePayrollDO> retriveForReportQuarter(String fromMonth, String toMonth, Long batchId) throws AppException {
		return new EmployeePayrollDAO().retriveForReportQuarter(fromMonth, toMonth, batchId);
	}
}
