package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class HolidaysUtil {
	
	private HolidaysUtil() {}
	
	public static JSONObject getHoildaysList(List<HolidaysDO> hoildayList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HolidaysDO hoildayDetail : hoildayList) {
				resultJSONArray.put(getHoildayDetailObject(hoildayDetail));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHoildayDetailObject(HolidaysDO holidayDetail)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(holidayDetail.getId()));
		result.put(CommonConstants.DESC, String.valueOf(holidayDetail.getDescription()));
		if(holidayDetail.getLeavedate() != null){
			String array1[]= holidayDetail.getLeavedate().split(" ");
			result.put(CommonConstants.DATE, String.valueOf(array1[1]+" "+ array1[0]));
		}else{
			result.put(CommonConstants.DATE, String.valueOf(""));
		}
		
		result.put(CommonConstants.YEAR, String.valueOf(holidayDetail.getYear()));
		//result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(holidayDetail.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(holidayDetail.getStatus()));
		if(holidayDetail.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(holidayDetail.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
