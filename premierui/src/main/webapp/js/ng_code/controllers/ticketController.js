var app = angular.module('spheresuite')
app.controller('ticketController', ticketController);

ticketController.$inject = ['$scope', '$rootScope', '$location', '$localStorage', 'ticketService'];

function ticketController($scope, $rootScope, $location, $localStorage, ticketService) {
    var ticketControllerScope = this;
    $rootScope.headerMenu = "Ticket";
    
    ticketControllerScope.isReply = false;
    
    ticketControllerScope.showReply = showReply;
    ticketControllerScope.getMyViewTickets = getMyViewTickets;
    ticketControllerScope.addNewTicket = addNewTicket;
    ticketControllerScope.addNewViewTickets = addNewViewTickets;
    ticketControllerScope.getNewViewTickets = getNewViewTickets;

    if ($location.path() == '/myticket') {
        getMyViewTickets();
    }
    
    if ($location.path() == '/ticketassigned') {
        getMyViewTickets();
    }

    if ($location.path() == '/assignticket') {
        getMyViewTickets();
    }
    if ($location.path() == '/viewAssignTicket') {
        getMyViewTickets();
        getNewViewTickets();
    }
    if ($location.path() == '/viewAssignedTicket') {
        getMyViewTickets();
        getNewViewTickets();
        getViewTicketsData();
    }

    if ($location.path() == '/viewTicket') {
        getViewTicketsData();
        getNewViewTickets();
    }
    
    function showReply(){
    	ticketControllerScope.isReply = true;
    }

    function getMyViewTickets(data){
    	console.log('ticketCtrl');
    	ticketControllerScope.spinner = true;
        ticketService.getMyViewTickets(data).then(function(res) {
            if (res.length > 0) {
                ticketControllerScope.viewTicketList = res;
            }
            ticketControllerScope.spinner = false;
        }, function(err) {
            ticketControllerScope.spinner = false;
        });
    }

    function getViewTicketsData(data){
    	console.log('ticketCtrl');
    	ticketControllerScope.spinner = true;
        ticketService.getViewTicketsData(data).then(function(res) {
            if (res.length > 0) {
                ticketControllerScope.viewDataTicketList = res;
                console.log('viewDataTicketList', ticketControllerScope.viewDataTicketList)
            }
            ticketControllerScope.spinner = false;
        }, function(err) {
            ticketControllerScope.spinner = false;
        });
    }

    function addNewTicket(){
    	ticketControllerScope.spinner = true;
        if (ticketControllerScope.newTicket) {
            ticketControllerScope.newTicket.updatedBy = $localStorage.spheresuite.id;
            ticketService.addNewTicket(ticketControllerScope.newTicket).then(function(res) {
                console.log('addNewTicket', res);
                if (res.successflag === 'true') {
                    ticketControllerScope.spinner = false;
                    //getEmpSkills($localStorage.spheresuite.viewEmployeeId);
                    ticketControllerScope.newTicket= {};
                    //$('#submitModal').modal('hide');
                } else {
                    ticketControllerScope.spinner = false;
                    ticketControllerScope.msg = res.errors;
                }

            }, function(err) {
                ticketControllerScope.spinner = false;
            });
        }
    }

    function addNewViewTickets(){
    	ticketControllerScope.spinner = true;
        if (ticketControllerScope.newViewTicket) {
            ticketControllerScope.newViewTicket.updatedBy = $localStorage.spheresuite.id;
            ticketService.addNewViewTickets(ticketControllerScope.newViewTicket).then(function(res) {
                console.log('addNewViewTickets', res);
                if (res.successflag === 'true') {
                    ticketControllerScope.spinner = false;
                    getNewViewTickets(data);
                    ticketControllerScope.newViewTicket= {};
                    //$('#submitModal').modal('hide');
                } else {
                    ticketControllerScope.spinner = false;
                    ticketControllerScope.msg = res.errors;
                }

            }, function(err) {
                ticketControllerScope.spinner = false;
            });
        }
    }

    function getNewViewTickets(data) { 
        ticketControllerScope.spinner = true; 
        ticketService.getNewViewTickets(data).then(function(res) { 
            if (res.length > 0) {
                ticketControllerScope.spinner = false;
                ticketControllerScope.showResponseTicketList = res; 
                console.log('getNewViewTickets', ticketControllerScope.showResponseTicketList); 
            } else
            ticketControllerScope.spinner = false; 
        }, function(err) { 
        });
    }
    
    $(".select1").select2();
    
}