package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeePayrollDAO {
	static Logger logger = Logger.getLogger(EmployeePayrollDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeePayrollDO persist(EmployeePayrollDO employeePayrollDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeePayrollDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeePayrollDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeePayrollDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieve() throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_ALL);
				q.setParameter(CommonConstants.STATUS, 'p');
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveActive(Long empId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, 'p');
				q.setParameter(CommonConstants.EMPID, empId);
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveById(Long id) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_EMP_ID);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.STATUS, 'p');
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	public Object[] retrieveByEmpIdBetweenDateSum(Long empId, String fromMnth, String toMonth) throws AppException {
		Object[] empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_EMP_BEWTEENDATE_SUM);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter("fromMonth", fromMnth);
				q.setParameter("toMonth", toMonth);
				q.setParameter(CommonConstants.STATUS,'p');
				empPayrollList = (Object[]) q.getSingleResult();
			}
		} catch (Exception eException) {
			return null;
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	public Object[] retrieveByEmpIdForMonthSum(Long empId, String fromMnth) throws AppException {
		Object[] empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_EMP_DATE_SUM);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter("fromMonth", fromMnth);
				q.setParameter(CommonConstants.STATUS,'p');
				empPayrollList = (Object[]) q.getSingleResult();
			}
		} catch (Exception eException) {
			return null;
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO>retrieveByEmpIdBetweenDate(Long empId, String fromMnth, String toMonth) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_EMP_BEWTEENDATE);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter("fromMonth", fromMnth);
				q.setParameter("toMonth", toMonth);
				q.setParameter(CommonConstants.STATUS,'p');
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retrieveByAllEmp(String payrollMonth, Long payrollbatchId, char status) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_ALL_EMP);
				q.setParameter(CommonConstants.PAYROLLMONTH, payrollMonth);
				q.setParameter(CommonConstants.STATUS, status);
				q.setParameter(CommonConstants.PAYROLLBATCHID, payrollbatchId);
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonth(String month, Long payrollBatchId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_MONTH);
				q.setParameter(CommonConstants.MONTHLY, month);
				q.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId);
				q.setParameter(CommonConstants.STATUS, CommonConstants.DRAFT);
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForReportMonth(String month, Long batchId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(batchId == null){
					Query q = em.createNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT);
					q.setParameter(CommonConstants.MONTHLY, month);
					empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
				}
				if(batchId != null){
					Query q = em.createNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BATCH);
					q.setParameter(CommonConstants.MONTHLY, month);
					q.setParameter(CommonConstants.PAYROLLBATCHID, batchId);
					empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveForReportQuarter(String fromMonth,String toMonth, Long batchId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(batchId == null){
					Query q = em.createNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BEWTEENDATE);
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
				}
				if(batchId != null){
					Query q = em.createNamedQuery(EmployeePayrollDO.FIND_FOR_REPORT_BEWTEENDATE_BATCH);
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					q.setParameter(CommonConstants.PAYROLLBATCHID, batchId);
					empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByStatus(String month, Long payrollBatchId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_MONTH);
				q.setParameter(CommonConstants.MONTHLY, month);
				q.setParameter(CommonConstants.PAYROLLBATCHID, payrollBatchId);
				q.setParameter(CommonConstants.STATUS, 'p');
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeePayrollDO> retriveByMonthAndEmp(String month, Long empId) throws AppException {
		List<EmployeePayrollDO> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_BY_MONTH_EMP);
				q.setParameter(CommonConstants.MONTHLY, month);
				q.setParameter(CommonConstants.EMPID, empId);
				empPayrollList = (List<EmployeePayrollDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
	
	public EmployeePayrollDO update(EmployeePayrollDO employeePayrollDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeePayrollDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeePayrollDO;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> retrieveForSummary() throws AppException {
		List<Object[]> empPayrollList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeePayrollDO.FIND_FOR_SUMMARY);
				empPayrollList = (List<Object[]>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empPayrollList;
	}
}
