package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="fy")
@TableGenerator(name ="fy", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "fy.findAll", query = "SELECT r FROM FyDO r"),
    @NamedQuery(name = "fy.findById", query = "SELECT r FROM FyDO r where r.id =:id"),
    @NamedQuery(name = "fy.findByStatus", query = "SELECT r FROM FyDO r where r.status =:status"),
})
public class FyDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "fy.findAll";
	
	public static final String FIND_BY_ID = "fy.findById";
	
	public static final String FIND_BY_STATUS = "fy.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "fy")
	private Long id;
	private Long fromyear;
	private Long toyear;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFromyear() {
		return fromyear;
	}

	public void setFromyear(Long fromyear) {
		this.fromyear = fromyear;
	}

	public Long getToyear() {
		return toyear;
	}

	public void setToyear(Long toyear) {
		this.toyear = toyear;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}