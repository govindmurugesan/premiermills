package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class PtUtil {
	
	private PtUtil() {}
	
	public static JSONObject getPtList(List<PtDO> ptDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (PtDO ptDO : ptDOList) {
				resultJSONArray.put(getPtDetailObject(ptDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getPtDetailObject(PtDO ptDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ptDO.getId()));
		result.put(CommonConstants.FROMAMOUNT, String.valueOf(ptDO.getFromamount()));
		result.put(CommonConstants.TOAMOUNT, String.valueOf(ptDO.getToamount()));
		result.put(CommonConstants.PTAMOUNT, String.valueOf(ptDO.getPtamount()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(ptDO.getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(ptDO.getStateId()));
		result.put(CommonConstants.STATUS, String.valueOf(ptDO.getStatus()));
		CountryDAO obj = new CountryDAO();
		CountryDO ptDOdo = obj.retrieveByID(ptDO.getCountryId());
		if(ptDOdo != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(ptDOdo.getName()));
		}
		StateService stateObj = new StateService();
		List<StateDO> stateDO = stateObj.retrieveById(ptDO.getStateId());
		if(stateDO != null && stateDO.size() > 0){
			result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
		}
		
		if(ptDO.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(ptDO.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(ptDOdo.getUpdatedon())));

		return result;
	}
}
