package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeTypeDAO;
import com.spheresuite.erp.domainobject.EmployeeTypeDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeTypeService {
	static Logger logger = Logger.getLogger(EmployeeTypeService.class.getName());
	
	public EmployeeTypeDO persist(EmployeeTypeDO countryDO) throws AppException {
		return new EmployeeTypeDAO().persist(countryDO);
	}

	public List<EmployeeTypeDO> retrieveActive() throws AppException {
		return new EmployeeTypeDAO().retrieveActive();
	}
	
	public List<EmployeeTypeDO> retrieveById(Long id) throws AppException {
		return new EmployeeTypeDAO().retrieveById(id);
	}
	
	public List<EmployeeTypeDO> retrieve() throws AppException {
		return new EmployeeTypeDAO().retrieve();
	}
	
	public EmployeeTypeDO update(EmployeeTypeDO countryDO) throws AppException {
		return new EmployeeTypeDAO().update(countryDO);
	}
}
