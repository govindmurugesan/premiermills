package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeBankInformationDAO;
import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeBankInformationService {
	static Logger logger = Logger.getLogger(EmployeeBankInformationService.class.getName());
	
	public EmployeeBankInformationDO persist(EmployeeBankInformationDO employeeBankInformationDO) throws AppException {
		return new EmployeeBankInformationDAO().persist(employeeBankInformationDO);
	}

	public List<EmployeeBankInformationDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeBankInformationDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeBankInformationDO> retrieveById(Long id) throws AppException {
		return new EmployeeBankInformationDAO().retrieveById(id);
	}
	
	public EmployeeBankInformationDO update(EmployeeBankInformationDO employeeBankInformationDO) throws AppException {
		return new EmployeeBankInformationDAO().update(employeeBankInformationDO);
	}
}
