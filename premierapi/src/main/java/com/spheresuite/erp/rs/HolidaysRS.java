package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.service.HolidaysService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HolidaysUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/holidays")
public class HolidaysRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HolidaysRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		List<HolidaysDO> holidayList = new HolidaysService().retrieveByHoliday(inputJSON.get(CommonConstants.DESC).toString(), inputJSON.get(CommonConstants.YEAR).toString(), inputJSON.get(CommonConstants.DATE).toString());
			 		if(holidayList != null && holidayList.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Holiday Already Exist").toString();
			 		}else{
			 			HolidaysDO holidayDO = new HolidaysDO(); 
				 		holidayDO.setYear(inputJSON.get(CommonConstants.YEAR).toString());
				 		holidayDO.setLeavedate(inputJSON.get(CommonConstants.DATE).toString());
				 		holidayDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
				 		holidayDO.setUpdatedon(new Date());
			 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 				holidayDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 				holidayDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
				 		}
			 			holidayDO.setStatus(CommonConstants.ACTIVE);
			 			new HolidaysService().persist(holidayDO);
			 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Holiday created");
			 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"holiday","holiday");
			 		}
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HolidaysDO> hoildayList = new HolidaysService().retrieve();
				respJSON = HolidaysUtil.getHoildaysList(hoildayList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					 		List<HolidaysDO> holidayList = new HolidaysService().retrieveByHolidayForUpdate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),
					 				inputJSON.get(CommonConstants.DESC).toString(), inputJSON.get(CommonConstants.YEAR).toString(), 
					 				inputJSON.get(CommonConstants.DATE).toString());
					 		if(holidayList != null && holidayList.size() > 0){
					 			return CommonWebUtil.buildErrorResponse("Holiday Already Exist").toString();
					 		}
			 				HolidaysDO hoildayDO = new HolidaysDO(); 
			 				List<HolidaysDO> hoildayList = new HolidaysService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 				if(hoildayList != null && hoildayList.size() > 0){
			 					hoildayDO = hoildayList.get(0);
			 				}
			 				hoildayDO.setYear(inputJSON.get(CommonConstants.YEAR).toString());
			 				hoildayDO.setLeavedate(inputJSON.get(CommonConstants.DATE).toString());
			 				hoildayDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
			 				hoildayDO.setUpdatedon(new Date());
			 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 					hoildayDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
			 				hoildayDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 				new HolidaysService().update(hoildayDO);
			 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Holiday Updated");
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
