package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.ItSavingDocService;
import com.spheresuite.erp.service.ItSavingsSettingsService;
import com.spheresuite.erp.util.CommonConstants;

public class ItSavingsUtil {
	
	private ItSavingsUtil() {}
	
	public static JSONObject getITSavingsSettingsList(List<ItSavingsDO> itSavingsDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSavingsDO itSavingsDO : itSavingsDOList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSavingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSavingsDO itSavingsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSavingsDO.getId()));
		result.put(CommonConstants.EMPID, String.valueOf(itSavingsDO.getEmpId()));
		result.put(CommonConstants.FROMDATE, String.valueOf(itSavingsDO.getFromYear()));
		result.put(CommonConstants.TODATE, String.valueOf(itSavingsDO.getToYear()));
		result.put(CommonConstants.COMMENT, String.valueOf(itSavingsDO.getComments()));
		if(itSavingsDO.getItSavingsSettingId() != null){
			result.put(CommonConstants.ITSAVINGSETTINGID, String.valueOf(itSavingsDO.getItSavingsSettingId()));
			
			List<ItSavingsSettingsDO> itSavingSettingList = new ItSavingsSettingsService().retrieveById(itSavingsDO.getItSavingsSettingId());
			if(itSavingSettingList != null && itSavingSettingList.size() > 0){
				result.put(CommonConstants.ITSAVINGSETTINGNAME, String.valueOf(itSavingSettingList.get(0).getName()));
				result.put(CommonConstants.SECTION, String.valueOf(itSavingSettingList.get(0).getSection()));
				result.put(CommonConstants.MAXLIMIT, String.valueOf(itSavingSettingList.get(0).getMaxLimit()));	
			}else{
				result.put(CommonConstants.SECTION,"");
				result.put(CommonConstants.MAXLIMIT,"");
				result.put(CommonConstants.ITSAVINGSETTINGNAME, "");
			}
		}else{
			result.put(CommonConstants.ITSAVINGSETTINGID, "");
			result.put(CommonConstants.SECTION, "");
			result.put(CommonConstants.MAXLIMIT, "");
		}
		
		result.put(CommonConstants.AMOUNT, String.valueOf(itSavingsDO.getAmount()));
		result.put(CommonConstants.STATUS, String.valueOf(itSavingsDO.getStatus()));
		if(itSavingsDO.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(itSavingsDO.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, "");
		}
		if(itSavingsDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(itSavingsDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSavingsDO.getUpdatedon())));
		JSONArray resultJSONArray = new JSONArray();
		
		List<ItSavingsDocDO> itSavingsDocList = new ItSavingDocService().retrieveByOppId(itSavingsDO.getId());
		if(itSavingsDocList != null && itSavingsDocList.size()  > 0){
			for (ItSavingsDocDO doc : itSavingsDocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	

	public static JSONObject getDocDetailObject(ItSavingsDocDO doc)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
