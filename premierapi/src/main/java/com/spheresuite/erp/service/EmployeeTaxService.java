package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeTaxDAO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeTaxService {
	static Logger logger = Logger.getLogger(EmployeeTaxService.class.getName());
	
	public List<EmployeeTaxDO> persist(List<EmployeeTaxDO> employeetaxDO) throws AppException {
		return new EmployeeTaxDAO().persist(employeetaxDO);
	}

	public List<EmployeeTaxDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new EmployeeTaxDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<EmployeeTaxDO> retrieve() throws AppException {
		return new EmployeeTaxDAO().retrieve();
	}
	
	public EmployeeTaxDO update(EmployeeTaxDO employeetaxDO) throws AppException {
		return new EmployeeTaxDAO().update(employeetaxDO);
	}
}
