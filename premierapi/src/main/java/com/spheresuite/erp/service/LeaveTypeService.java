package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeaveTypeDAO;
import com.spheresuite.erp.domainobject.LeaveTypeDO;
import com.spheresuite.erp.exception.AppException;

public class LeaveTypeService {
	static Logger logger = Logger.getLogger(HrRequestTypeService.class.getName());
	
	public LeaveTypeDO persist(LeaveTypeDO requestType) throws AppException {
		return new LeaveTypeDAO().persist(requestType);
	}

	public List<LeaveTypeDO> retrieve() throws AppException {
		return new LeaveTypeDAO().retrieve();
	}
	
	public List<LeaveTypeDO> retrieveByType(String type) throws AppException {
		return new LeaveTypeDAO().retrieveByType(type);
	}
	
	public List<LeaveTypeDO> retrieveByTypeUpdate(String type, Long id) throws AppException {
		return new LeaveTypeDAO().retrieveByTypeUpdate(type, id);
	}
	
	public List<LeaveTypeDO> retrieveById(Long Id) throws AppException {
		return new LeaveTypeDAO().retrieveById(Id);
	}
	
	
	public LeaveTypeDO update(LeaveTypeDO requestType) throws AppException {
		return new LeaveTypeDAO().update(requestType);
	}
}
