package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="department")
@TableGenerator(name ="department", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "department.findAll", query = "SELECT r FROM DepartmentDO r"),
    @NamedQuery(name = "department.findById", query = "SELECT r FROM DepartmentDO r where r.id =:id"),
    @NamedQuery(name = "department.findByStatus", query = "SELECT r FROM DepartmentDO r where r.status =:status"),
    @NamedQuery(name = "department.findisManager", query = "SELECT r.id FROM DepartmentDO r where r.isManager =:isManager and r.status =:status"),
    
})
public class DepartmentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "department.findAll";
	
	public static final String FIND_BY_ID = "department.findById";
	
	public static final String FIND_BY_STATUS = "department.findByStatus";
	
	public static final String FIND_IS_MANAGER = "department.findisManager";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "department")
	private Long id;
	private String name;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private Long isManager;
	private String fromTime;
	private String toTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public Long getIsManager() {
		return isManager;
	}

	public void setIsManager(Long isManager) {
		this.isManager = isManager;
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
}