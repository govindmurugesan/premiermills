package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.SalesPoliciesDAO;
import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.exception.AppException;

public class SalesPoliciesService {
	static Logger logger = Logger.getLogger(SalesPoliciesService.class.getName());
	
	public SalesPoliciesDO persist(SalesPoliciesDO salesPoliciesDO) throws AppException {
		return new SalesPoliciesDAO().persist(salesPoliciesDO);
	}

	public List<SalesPoliciesDO> retrieveById(Long Id) throws AppException {
		return new SalesPoliciesDAO().retrieveById(Id);
	}
	
	public List<SalesPoliciesDO> retrieve(List<Long> id) throws AppException {
		return new SalesPoliciesDAO().retrieve(id);
	}
	
	public List<SalesPoliciesDO> retrieveAll() throws AppException {
		return new SalesPoliciesDAO().retrieveAll();
	}
	
	public SalesPoliciesDO update(SalesPoliciesDO salesPoliciesDO) throws AppException {
		return new SalesPoliciesDAO().update(salesPoliciesDO);
	}
}
