package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="timesheet")
@TableGenerator(name ="timesheet", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "timesheet.findAll", query = "SELECT r FROM TimesheetDO r"),
    @NamedQuery(name = "timesheet.findById", query = "SELECT r FROM TimesheetDO r where r.id =:id"),
    @NamedQuery(name = "timesheet.findByWeekendDate", query = "SELECT r FROM TimesheetDO r where r.weekenddate =:weekenddate and r.empId =:empId"),
})
public class TimesheetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "timesheet.findAll";
	public static final String FIND_BY_DATE = "timesheet.findByWeekendDate";
	public static final String FIND_BY_ID = "timesheet.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "timesheet")
	private Long id;
	private Long timesheetType;
	private Long hours;
	private String date;
	private String weekenddate;
	private String description;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	//private char status;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTimesheetType() {
		return timesheetType;
	}
	public void setTimesheetType(Long timesheetType) {
		this.timesheetType = timesheetType;
	}
	public Long getHours() {
		return hours;
	}
	public void setHours(Long hours) {
		this.hours = hours;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getWeekenddate() {
		return weekenddate;
	}
	public void setWeekenddate(String weekenddate) {
		this.weekenddate = weekenddate;
	}
	
}