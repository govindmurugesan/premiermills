package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeOtherInformationDAO {
	static Logger logger = Logger.getLogger(EmployeeOtherInformationDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeOtherInformationDO persist(EmployeeOtherInformationDO employeeOtherInformationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeOtherInformationDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeOtherInformationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOtherInformationDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOtherInformationDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeOtherInformationDO> employeeOtherInformationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeOtherInformationDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empId);
				employeeOtherInformationList = (List<EmployeeOtherInformationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOtherInformationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOtherInformationDO> retrieveById(Long id) throws AppException {
		List<EmployeeOtherInformationDO> employeeOtherInformationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeOtherInformationDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeOtherInformationList = (List<EmployeeOtherInformationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOtherInformationList;
	}
	
	public EmployeeOtherInformationDO update(EmployeeOtherInformationDO employeeOtherInformationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeOtherInformationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOtherInformationDO;
	}

}
