package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class PayrollTaxDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollTaxDetailsDAO.class.getName());
	private EntityManager em = null;
	
	public List<PayrollTaxDetailsDO> persist(List<PayrollTaxDetailsDO> payrollTaxDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && payrollTaxDetailsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (PayrollTaxDetailsDO payrollTaxDetailsDO2 : payrollTaxDetailsDO) {
					em.persist(payrollTaxDetailsDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollTaxDetailsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PayrollTaxDetailsDO> retrieve() throws AppException {
		List<PayrollTaxDetailsDO> payrollTaxDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollTaxDetailsDO.FIND_ALL);
				payrollTaxDetailsList = (List<PayrollTaxDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollTaxDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		List<PayrollTaxDetailsDO> payrollTaxDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollTaxDetailsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				payrollTaxDetailsList = (List<PayrollTaxDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollTaxDetailsList;
	}
	
	public PayrollTaxDetailsDO update(PayrollTaxDetailsDO payrollTaxDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(payrollTaxDetailsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollTaxDetailsDO;
	}

	@SuppressWarnings("unchecked")
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) throws AppException {
		List<PayrollTaxDetailsDO> employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollTaxDetailsDO.FIND_BY_IDS);
				q.setParameter(CommonConstants.ID, ids);
				employeeEarningsList = (List<PayrollTaxDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}
}
