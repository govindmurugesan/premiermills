package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="itsavingdoc")
@TableGenerator(name ="itsavingdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "itsavingdoc.findAll", query = "SELECT r FROM ItSavingsDocDO r"),
    @NamedQuery(name = "itsavingdoc.findBySavingId", query = "SELECT r FROM ItSavingsDocDO r where r.itsavingId =:id"),
})
public class ItSavingsDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "itsavingdoc.findAll";
	
	public static final String FIND_BY_ITSAVING_ID = "itsavingdoc.findBySavingId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "itsavingdoc")
	private Long id;
	private Long itsavingId;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getItsavingId() {
		return itsavingId;
	}
	public void setItsavingId(Long itsavingId) {
		this.itsavingId = itsavingId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}