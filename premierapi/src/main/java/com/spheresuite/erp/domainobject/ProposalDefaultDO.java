package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="proposalDefault")
@TableGenerator(name ="proposalDefault", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "proposalDefault.findByProposalId", query = "SELECT r FROM ProposalDefaultDO r where r.proposalId =:proposalId"),
})
public class ProposalDefaultDO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_BY_PROPOSAL_ID = "proposalDefault.findByProposalId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "proposalDefault")
	private Long id;
	private Long proposalId;
	private String proposalKey;
	private String proposalValue;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProposalId() {
		return proposalId;
	}
	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}
	public String getProposalKey() {
		return proposalKey;
	}
	public void setProposalKey(String proposalKey) {
		this.proposalKey = proposalKey;
	}
	public String getProposalValue() {
		return proposalValue;
	}
	public void setProposalValue(String proposalValue) {
		this.proposalValue = proposalValue;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}