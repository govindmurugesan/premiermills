package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="assignedmenu")
@TableGenerator(name ="assignedmenu", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "assignedmenu.findAll", query = "SELECT r FROM AssignedMenuDO r"),
    @NamedQuery(name = "assignedmenu.findById", query = "SELECT r FROM AssignedMenuDO r where r.id =:id"),
    @NamedQuery(name = "assignedmenu.findByroleId", query = "SELECT r FROM AssignedMenuDO r where r.roleId =:roleId")
})
public class AssignedMenuDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "assignedmenu.findAll";
	
	public static final String FIND_BY_ID = "assignedmenu.findById";
	
	public static final String FIND_BY_ROLE_ID = "assignedmenu.findByroleId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "assignedmenu")
	private Long id;
	private Long roleId;
	private Long menuId;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}