package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeadEmailDAO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.exception.AppException;

public class LeadEmailService {
	static Logger logger = Logger.getLogger(LeadEmailService.class.getName());
	
	public LeadEmailDO persist(LeadEmailDO leadEmailDO) throws AppException {
		return new LeadEmailDAO().persist(leadEmailDO);
	}
	
	public boolean persistList(List<LeadEmailDO> leadEmailDO) throws AppException {
		return new LeadEmailDAO().persistList(leadEmailDO);
	}

	public List<LeadEmailDO> retrieveByLead(Long leadId) throws AppException {
		return new LeadEmailDAO().retrieveByLead(leadId);
	}
	
	public List<LeadEmailDO> retrieveById(Long Id) throws AppException {
		return new LeadEmailDAO().retrieveById(Id);
	}
	
	public List<LeadEmailDO> retrieveLast(Long leadId) throws AppException {
		return new LeadEmailDAO().retrieveByLead(leadId);
	}
	
	
	public List<LeadEmailDO> retrieveAllMail() throws AppException {
		return new LeadEmailDAO().retrieveAllMail();
	}

	public boolean delete(Long Id) throws AppException {
		return new LeadEmailDAO().delete(Id);
	}
	
	
}
