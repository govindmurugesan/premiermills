package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProposalDocumentsDAO;
import com.spheresuite.erp.domainobject.ProposalDocumentsDO;
import com.spheresuite.erp.exception.AppException;

public class ProposalDocumentsService {
	static Logger logger = Logger.getLogger(ProposalDocumentsService.class.getName());
	
	public List<ProposalDocumentsDO> persist(List<ProposalDocumentsDO> proposalDocumentsList) throws AppException {
		return new ProposalDocumentsDAO().persist(proposalDocumentsList);
	}

	public List<ProposalDocumentsDO> retrieveById(Long proposalId) throws AppException {
		return new ProposalDocumentsDAO().retrieveById(proposalId);
	}
}
