package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="logacivity")
@TableGenerator(name ="logacivity", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "logacivity.findAll", query = "SELECT r FROM LogActivityDO r where r.type =:type"),
    @NamedQuery(name = "logacivity.findById", query = "SELECT r FROM LogActivityDO r where r.id =:id"),
    @NamedQuery(name = "logacivity.findByLeadId", query = "SELECT r FROM LogActivityDO r where r.leadId =:id"),
})
public class LogActivityDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "logacivity.findAll";
	
	public static final String FIND_BY_ID = "logacivity.findById";
	
	public static final String FIND_BY_LEAD_ID = "logacivity.findByLeadId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "logacivity")
	private Long id;
	private Long leadId;
	private String type;
	private String status;
	private String logDetails;
	private String logDate;
	private String logTime;
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLogDate() {
		return logDate;
	}

	public void setLogDate(String logDate) {
		this.logDate = logDate;
	}

	public String getLogTime() {
		return logTime;
	}

	public void setLogTime(String logTime) {
		this.logTime = logTime;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getLogDetails() {
		return logDetails;
	}

	public void setLogDetails(String logDetails) {
		this.logDetails = logDetails;
	}

	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}