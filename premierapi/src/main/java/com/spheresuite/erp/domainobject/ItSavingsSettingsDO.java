package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="itsavingssetting")
@TableGenerator(name ="itsavingssetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "itsavingssetting.findAll", query = "SELECT r FROM ItSavingsSettingsDO r"),
    @NamedQuery(name = "itsavingssetting.findById", query = "SELECT r FROM ItSavingsSettingsDO r where r.id =:id"),
    @NamedQuery(name = "itsavingssetting.findByStatus", query = "SELECT r FROM ItSavingsSettingsDO r where r.status =:status"),
    @NamedQuery(name = "itsavingssetting.findByDate", query = "SELECT r FROM ItSavingsSettingsDO r where r.status =:status and (r.fromYear =:fromDate and r.toYear =:toDate)"),
})
public class ItSavingsSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "itsavingssetting.findAll";
	
	public static final String FIND_BY_ID = "itsavingssetting.findById";
	
	public static final String FIND_BY_STATUS = "itsavingssetting.findByStatus";
	
	public static final String FIND_BY_DATE = "itsavingssetting.findByDate";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "itsavingssetting")
	private Long id;
	private String name;
	private String fromYear;
	private String toYear;
	private String section;
	private Long maxLimit;
	private char status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFromYear() {
		return fromYear;
	}
	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}
	public String getToYear() {
		return toYear;
	}
	public void setToYear(String toYear) {
		this.toYear = toYear;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Long getMaxLimit() {
		return maxLimit;
	}
	public void setMaxLimit(Long maxLimit) {
		this.maxLimit = maxLimit;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
}