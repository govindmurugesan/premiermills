package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="lead")
@TableGenerator(name ="lead", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "lead.findAll", query = "SELECT r FROM LeadDO r where r.type=:type and r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findAllDate", query = "SELECT r FROM LeadDO r where r.type=:type  and r.isDeleted =:status"),
    @NamedQuery(name = "lead.find", query = "SELECT r FROM LeadDO r where r.empId IN :id and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findById", query = "SELECT r FROM LeadDO r where r.id =:id and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findByDate", query = "SELECT r FROM LeadDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findCustomerByDate", query = "SELECT r FROM LeadDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findByDateByEmp", query = "SELECT r FROM LeadDO r where r.createdon BETWEEN :fromDate AND :toDate AND r.type =:type and r.isDeleted =:status and r.empId =:empId"),
    @NamedQuery(name = "lead.findByByEmp", query = "SELECT r FROM LeadDO r where r.type =:type AND r.empId =:empId and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findByTransferType", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findByAllTransferType", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType is not null and r.isDeleted =:status"),
    @NamedQuery(name = "lead.findByTransferTypeById", query = "SELECT r FROM LeadDO r where r.type =:type AND r.transferType =:transferType and r.isDeleted =:status and r.transferfrom =:empId"),
    @NamedQuery(name = "lead.findAllLead", query = "SELECT r FROM LeadDO r  where r.isDeleted =:status"),
})
public class LeadDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "lead.findAll";
	
	public static final String FIND_ALL_LEAD = "lead.findAllLead";
	
	public static final String FIND_ALL_DATE = "lead.findAllDate";
	
	public static final String FIND = "lead.find";
	
	public static final String FIND_BY_ID = "lead.findById";

	public static final String FIND_BY_DATE = "lead.findByDate";
	
	public static final String FIND_BY_DATE_BY_EMP = "lead.findByDateByEmp";
	
	public static final String FIND_CUSTOMER_BY_DATE = "lead.findCustomerByDate";
	
	public static final String FIND_BY_EMP = "lead.findByByEmp";
	
	public static final String FIND_BY_TRANSFERTYPE = "lead.findByTransferType";
	
	public static final String FIND_BY_ALL_TRANSFERTYPE = "lead.findByAllTransferType";
	
	public static final String FIND_BY_TRANSFERTYPE_BY_ID = "lead.findByTransferTypeById";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "lead")
	private Long id;
	private String name;
	private Long status;
	private String source;
	private Long industryID;
	private Long mobile;
	private Long phone;
	private String email;
	private String address;
	private String comment;
	private String updatedby;
	private Long leadType;
	private String type;
	private String convertCustomerComments;
	@Temporal(TemporalType.TIMESTAMP)
	private Date convertedDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private Long transferfrom;
	@Temporal(TemporalType.DATE)
	private Date transferFromDate;
	@Temporal(TemporalType.DATE)
	private Date transferToDate;
	private String transferType;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String isDeleted;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getStatus() {
		return status;
	}
	public void setStatus(Long status) {
		this.status = status;
	}
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Long getIndustryID() {
		return industryID;
	}
	public void setIndustryID(Long industryID) {
		this.industryID = industryID;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getLeadType() {
		return leadType;
	}
	public void setLeadType(Long leadType) {
		this.leadType = leadType;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getConvertCustomerComments() {
		return convertCustomerComments;
	}
	public void setConvertCustomerComments(String convertCustomerComments) {
		this.convertCustomerComments = convertCustomerComments;
	}
	public Date getConvertedDate() {
		return convertedDate;
	}
	public void setConvertedDate(Date convertedDate) {
		this.convertedDate = convertedDate;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getTransferfrom() {
		return transferfrom;
	}
	public void setTransferfrom(Long transferfrom) {
		this.transferfrom = transferfrom;
	}
	public Date getTransferFromDate() {
		return transferFromDate;
	}
	public void setTransferFromDate(Date transferFromDate) {
		this.transferFromDate = transferFromDate;
	}
	public Date getTransferToDate() {
		return transferToDate;
	}
	public void setTransferToDate(Date transferToDate) {
		this.transferToDate = transferToDate;
	}
	public String getTransferType() {
		return transferType;
	}
	public void setTransferType(String transferType) {
		this.transferType = transferType;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
}