package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.RequirementsDAO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.exception.AppException;

public class RequirementsService {
	static Logger logger = Logger.getLogger(RequirementsService.class.getName());
	
	public RequirementsDO persist(RequirementsDO requirementsDO) throws AppException {
		return new RequirementsDAO().persist(requirementsDO);
	}
	
	public RequirementsDO update(RequirementsDO requirementsDO) throws AppException {
		return new RequirementsDAO().update(requirementsDO);
	}

	public List<RequirementsDO> retrieveById(Long Id) throws AppException {
		return new RequirementsDAO().retrieveById(Id);
	}

	public List<RequirementsDO> retrieve() throws AppException {
		return new RequirementsDAO().retrieve();
	}
	
	public List<RequirementsDO> retrieveByStatus(char status) throws AppException {
		return new RequirementsDAO().retrieveByStatus(status);
	}
	
	public List<RequirementsDO> retrieveByProjectId(Long id) throws AppException {
		return new RequirementsDAO().retrieveByProjectId(id);
	}
	
	public List<RequirementsDO> retrieveByCustomerId(Long id) throws AppException {
		return new RequirementsDAO().retrieveByCustomerId(id);
	}
	
	
}
