package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class LeaveRequestsDAO {
	static Logger logger = Logger.getLogger(LeaveRequestsDAO.class.getName());
	private EntityManager em = null;
	
	public LeaveRequestsDO persist(LeaveRequestsDO leaverequestDetail) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leaverequestDetail != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(leaverequestDetail);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaverequestDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieve() throws AppException {
		List<LeaveRequestsDO> leaveRequestsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveRequestsDO.FIND_ALL);
				leaveRequestsList = (List<LeaveRequestsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveRequestsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveById(Long Id) throws AppException {
		List<LeaveRequestsDO> leaveRequestList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveRequestsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				leaveRequestList = (List<LeaveRequestsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveRequestList;
	}
	
	public LeaveRequestsDO update(LeaveRequestsDO leaveRequest) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(leaveRequest);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveRequest;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveByEmpId(Long id) throws AppException {
		List<LeaveRequestsDO> requestList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveRequestsDO.FINDBY_EMP_ID);
				q.setParameter(CommonConstants.ID, id);
				requestList = (List<LeaveRequestsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return requestList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveAvailableLeavesByEmpId(Long id, String leaveTypeFromDate, String leaveTypeToDate, Long type) throws AppException {
		List<LeaveRequestsDO> requestList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveRequestsDO.FIND_LEAVE_BY_EMP_ID);
				q.setParameter(CommonConstants.ID, id);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				q.setParameter(CommonConstants.LEAVETYPETODATE, leaveTypeToDate );
				q.setParameter(CommonConstants.LEAVETYPEFROMDATE, leaveTypeFromDate);
				q.setParameter(CommonConstants.TYPE, type);
				requestList = (List<LeaveRequestsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return requestList;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveRequestsDO> retrieveActive() throws AppException {
		List<LeaveRequestsDO> leaveRequestsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveRequestsDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				leaveRequestsList = (List<LeaveRequestsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveRequestsList;
	}*/
}
