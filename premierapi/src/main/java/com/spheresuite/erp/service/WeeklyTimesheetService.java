package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.WeeklyTimesheetDAO;
import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.exception.AppException;

public class WeeklyTimesheetService {
	static Logger logger = Logger.getLogger(WeeklyTimesheetService.class.getName());
	
	public WeeklyTimesheetDO persist(WeeklyTimesheetDO timesheet) throws AppException {
		return new WeeklyTimesheetDAO().persist(timesheet);
	}

	public List<WeeklyTimesheetDO> retrieve() throws AppException {
		return new WeeklyTimesheetDAO().retrieve();
	}
	
	public List<WeeklyTimesheetDO> retrieveByDept(List<Long> ids) throws AppException {
		return new WeeklyTimesheetDAO().retrieveByDept(ids);
	}
	
	public List<WeeklyTimesheetDO> retrieveById(Long Id) throws AppException {
		return new WeeklyTimesheetDAO().retrieveById(Id);
	}
	
	public List<WeeklyTimesheetDO> retrieveByDate(String date, long empid) throws AppException {
		return new WeeklyTimesheetDAO().retrieveByDate(date, empid);
	}
	
	public List<WeeklyTimesheetDO> retrieveByEmpId(long empid) throws AppException {
		return new WeeklyTimesheetDAO().retrieveByEmpId(empid);
	}
	
	public List<WeeklyTimesheetDO> retrieveSubmittedTimesheet() throws AppException {
		return new WeeklyTimesheetDAO().retrieveSubmittedTimesheet();
	}
	
	
	public WeeklyTimesheetDO update(WeeklyTimesheetDO timesheet) throws AppException {
		return new WeeklyTimesheetDAO().update(timesheet);
	}
}
