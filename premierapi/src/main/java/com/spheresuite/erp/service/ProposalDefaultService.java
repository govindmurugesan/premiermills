package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProposalDefaultDAO;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.exception.AppException;

public class ProposalDefaultService {
	static Logger logger = Logger.getLogger(ProposalDefaultService.class.getName());
	
	public List<ProposalDefaultDO> persist(List<ProposalDefaultDO> proposalDetailsList) throws AppException {
		return new ProposalDefaultDAO().persist(proposalDetailsList);
	}
	
	public boolean delete(Long proposalId) throws AppException {
		return new ProposalDefaultDAO().delete(proposalId);
	}

	public List<ProposalDefaultDO> retrieveById(Long proposalId) throws AppException {
		return new ProposalDefaultDAO().retrieveById(proposalId);
	}
}
