package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="recentactivity")
@TableGenerator(name ="recentactivity", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "recentactivity.findAll", query = "SELECT r FROM RecentActivityDO r"),
    @NamedQuery(name = "recentactivity.findByEmpId", query = "SELECT r FROM RecentActivityDO r where r.empId =:id order by r.id desc"),
})
public class RecentActivityDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "recentactivity.findAll";
	
	public static final String FIND_BY_EMP_ID = "recentactivity.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "recentactivity")
	private Long id;
	private String descripetion;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripetion() {
		return descripetion;
	}
	public void setDescripetion(String descripetion) {
		this.descripetion = descripetion;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}