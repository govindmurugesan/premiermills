package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class TaxPayeeTypeDAO {
	static Logger logger = Logger.getLogger(TaxPayeeTypeDAO.class.getName());
	private EntityManager em = null;
	
	public TaxPayeeTypeDO persist(TaxPayeeTypeDO taxPayeeType) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && taxPayeeType != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(taxPayeeType);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxPayeeType;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<TaxPayeeTypeDO> retrieve() throws AppException {
		List<TaxPayeeTypeDO> requestTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxPayeeTypeDO.FIND_ALL);
				requestTypeList = (List<TaxPayeeTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return requestTypeList;
	}
	
	public TaxPayeeTypeDO update(TaxPayeeTypeDO requestType) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(requestType);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return requestType;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxPayeeTypeDO> retrieveById(Long Id) throws AppException {
		List<TaxPayeeTypeDO> taxPayeeTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxPayeeTypeDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				taxPayeeTypeList = (List<TaxPayeeTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxPayeeTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxPayeeTypeDO> retrieveActive(char status) throws AppException {
		List<TaxPayeeTypeDO> taxPayeeTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxPayeeTypeDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, status);
				taxPayeeTypeList = (List<TaxPayeeTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxPayeeTypeList;
	}
	

}
