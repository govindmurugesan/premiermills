package com.spheresuite.erp.domainobject;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="sendmailattachments")
@TableGenerator(name ="sendmailattachments", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "sendmailattachments.findAll", query = "SELECT r FROM AttachmentsDO r")
})
public class AttachmentsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "sendmailattachments.findAll";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "sendmailattachments")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String attachFile;
	private String fileName;
	private String filetype;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAttachFile() {
		return attachFile;
	}
	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFiletype() {
		return filetype;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
}