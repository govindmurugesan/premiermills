package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leavetype")
@TableGenerator(name ="leavetype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "leavetype.findAll", query = "SELECT r FROM LeaveTypeDO r"),
    @NamedQuery(name = "leavetype.findById", query = "SELECT r FROM LeaveTypeDO r where r.id =:id"),
    @NamedQuery(name = "leavetype.findByType", query = "SELECT r FROM LeaveTypeDO r where r.type =:type and r.status =:status"),
    @NamedQuery(name = "leavetype.findByTypeForUpdate", query = "SELECT r FROM LeaveTypeDO r where r.type =:type and r.status NOT IN :id and r.status =:status"),
})
public class LeaveTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "leavetype.findAll";
	public static final String FIND_BY_ID = "leavetype.findById";
	public static final String FIND_BY_TYPE = "leavetype.findByType";
	public static final String FIND_BY_TYPE_FOR_UPDATE = "leavetype.findByTypeForUpdate";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "leavetype")
	
	private Long id;
	private String type;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}