package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.WorkLocationDAO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.exception.AppException;

public class WorkLocationService {
	static Logger logger = Logger.getLogger(WorkLocationService.class.getName());
	
	public WorkLocationDO persist(WorkLocationDO countryDO) throws AppException {
		return new WorkLocationDAO().persist(countryDO);
	}

	public List<WorkLocationDO> retrieveActive() throws AppException {
		return new WorkLocationDAO().retrieveActive();
	}
	
	public List<WorkLocationDO> retrieveActiveById(Long Id) throws AppException {
		return new WorkLocationDAO().retrieveActiveById(Id);
	}
	
	public List<WorkLocationDO> retrieve() throws AppException {
		return new WorkLocationDAO().retrieve();
	}
	
	public WorkLocationDO update(WorkLocationDO countryDO) throws AppException {
		return new WorkLocationDAO().update(countryDO);
	}
}
