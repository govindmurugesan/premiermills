package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveManagementUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leavemanagement")
public class LeaveManagementRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveManagementRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		List<LeaveManagementDO> leaveRecord = new LeaveManagementService().retrieveByType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 		if(leaveRecord != null && leaveRecord.size() > 0){
			 			LeaveManagementDO leaveMangamentDOToUpdate = new LeaveManagementDO();
			 			leaveMangamentDOToUpdate = leaveRecord.get(0);
			 			leaveMangamentDOToUpdate.setStatus(CommonConstants.INACTIVE);
			 			new LeaveManagementService().update(leaveMangamentDOToUpdate);
			 		}
			 		LeaveManagementDO leaveManagementDO = new LeaveManagementDO(); 
			 		leaveManagementDO.setLeaveType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 		leaveManagementDO.setNumberOfDays(Long.parseLong(inputJSON.get(CommonConstants.DAYS).toString()));
			 		leaveManagementDO.setUpdatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				leaveManagementDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				leaveManagementDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
		 			leaveManagementDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
		 			
		 			if(inputJSON.get(CommonConstants.FROMDATE) != null){
		 				leaveManagementDO.setFromDate(inputJSON.get(CommonConstants.FROMDATE).toString());
		 			}else{
		 				leaveManagementDO.setFromDate(null);
		 			}
		 			
		 			if(inputJSON.get(CommonConstants.TODATE) != null){
		 				leaveManagementDO.setToDate(inputJSON.get(CommonConstants.TODATE).toString());
		 			}else{
		 				leaveManagementDO.setToDate(null);
		 			}
		 			new LeaveManagementService().persist(leaveManagementDO);
		 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Management Created");
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<LeaveManagementDO> leaveList = new LeaveManagementService().retrieve();
					respJSON = LeaveManagementUtil.getLeaveList(leaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		LeaveManagementDO leaveMangamentDO = new LeaveManagementDO(); 
		 				List<LeaveManagementDO> leaveList = new LeaveManagementService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(leaveList != null && leaveList.size() > 0){
		 					leaveMangamentDO = leaveList.get(0);
		 				}
		 				leaveMangamentDO.setLeaveType((Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString())));
		 				leaveMangamentDO.setUpdatedon(new Date());
		 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 					leaveMangamentDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
		 				leaveMangamentDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
		 				
		 				if(inputJSON.get(CommonConstants.DAYS) != null){
		 					leaveMangamentDO.setNumberOfDays((Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.DAYS).toString() : "")));
				 		}
			 			
			 			if(inputJSON.get(CommonConstants.FROMDATE) != null){
			 				leaveMangamentDO.setFromDate((!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null));
			 			}
			 			
			 			if(inputJSON.get(CommonConstants.TODATE) != null){
			 				leaveMangamentDO.setToDate((!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null));
			 			}
		 				new LeaveManagementService().update(leaveMangamentDO);
		 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Management Updated");
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveManagementDO> leaveDetail = new LeaveManagementService().retriveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveManagementUtil.getLeaveList(leaveDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/retrieveByType/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByType(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveManagementDO> leaveRequestsList = new LeaveManagementService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveManagementUtil.getLeaveList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}*/
	
	@RequestMapping(value = "/retrieveAllActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveAllActive(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveManagementDO> leaveList = new LeaveManagementService().retrieveAllActive();
				respJSON = LeaveManagementUtil.getLeaveList(leaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
