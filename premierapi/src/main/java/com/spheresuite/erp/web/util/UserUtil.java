package com.spheresuite.erp.web.util;

import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.AssignedMenuDAO;
import com.spheresuite.erp.dao.RolesDAO;
import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class UserUtil {
	
	private UserUtil() {}
	
	public static JSONObject getUserList(List<UserDO> userList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (UserDO user : userList) {
				resultJSONArray.put(getUserDetailObject(user));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserDetailObject(UserDO user)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(user.getEmpId() != null ? user.getEmpId() : ""));
		//result.put(CommonConstants.NAME, String.valueOf(user.getName() != null ? user.getName() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(user.getStatus()));
		result.put(CommonConstants.EMAIL, String.valueOf(user.getEmail() != null ? user.getEmail() : ""));
		result.put(CommonConstants.ROLE_ID, String.valueOf(user.getRoleId() != null ? user.getRoleId() : ""));
		result.put(CommonConstants.PASSWORD, String.valueOf(user.getPassword() != null ? user.getPassword(): ""));
		result.put(CommonConstants.TEMP_PASSWORD, String.valueOf(user.getPassword() != null ? user.getPassword() : ""));
		result.put(CommonConstants.ISDELETED, String.valueOf(user.getIsDeleted() != null ? user.getIsDeleted() : ""));
		RolesDAO obj = new RolesDAO();
		List<RolesDO> rolesList = obj.retriveById(user.getRoleId());
		if(rolesList != null && rolesList.size() > 0){
			result.put(CommonConstants.ROLE_NAME, String.valueOf(rolesList.get(0).getName()));
		}else{
			result.put(CommonConstants.ROLE_NAME,"");
		}
		
		if(user.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(user.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, ""); 
			}
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(user.getUpdatedon())));
		if(user.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(user.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}

		return result;
	}
	
	public static JSONObject getUserForLogin(List<UserDO> userList, String token)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (UserDO user : userList) {
				resultJSONArray.put(getUserForLoginDetailObject(user));
			}
			resultJSON.put(CommonConstants.TOKEN, token);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserForLoginDetailObject(UserDO user)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(user.getEmpId()));
		//result.put(CommonConstants.EMPID, String.valueOf(user.getEmpId()));
		result.put(CommonConstants.EMAIL, String.valueOf(user.getEmail()));
		result.put(CommonConstants.ROLE_ID, String.valueOf(user.getRoleId()));
		if(user.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(user.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.NAME, ""); 
			}
		}
		RolesDAO obj = new RolesDAO();
		List<RolesDO> rolesList = obj.retriveById(user.getRoleId());
		if(rolesList != null && rolesList.size() > 0){
			result.put(CommonConstants.ROLE_NAME, String.valueOf(rolesList.get(0).getName()));
		}
		AssignedMenuDAO  assignedMenu = new AssignedMenuDAO();
		List<AssignedMenuDO> list = assignedMenu.retrieveByRoleId(user.getRoleId());
		JSONArray resultJSONArray = new JSONArray();
		if(list.size() > 0){
			for (AssignedMenuDO l : list) {
				resultJSONArray.put(getMenuObject(l));
			}
		}
		result.put(CommonConstants.MENU, resultJSONArray);
		return result;
	}
	
	public static JSONObject getMenuObject(AssignedMenuDO menu)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.SUB_MENU, String.valueOf(menu.getMenuId()));
		return result;
	}
	
	public static JSONObject generateUrl(String email, String tempPassword )throws AppException {
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.EMAIL, email);
			resultJSON.put(CommonConstants.TEMP_PASSWORD, tempPassword);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return resultJSON;
	}
	
	public static char[] geek_Password(int len)
    {
        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
 
        }
        return password;
    }
}
