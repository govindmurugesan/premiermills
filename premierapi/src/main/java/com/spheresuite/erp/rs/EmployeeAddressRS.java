package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.service.EmployeeAddressService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeAddressUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeaddress")
public class EmployeeAddressRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeAddressRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeAddressDO employeeEducationDO = new EmployeeAddressDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeEducationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		employeeEducationDO.setCountryId(!inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()) : null);
			 		employeeEducationDO.setStateId(!inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()) : null);
			 		employeeEducationDO.setAddressTypelId(!inputJSON.get(CommonConstants.ADDRESSTYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ADDRESSTYPEID).toString()) : null);
			 		employeeEducationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeEducationDO.setNotes(!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : null);
			 		employeeEducationDO.setZip(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()) : null);
			 		employeeEducationDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : null);
			 		employeeEducationDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
			 		employeeEducationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeEducationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeEducationDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeAddressService().persist(employeeEducationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeAddressDO> employeeAddressList = new EmployeeAddressService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeAddressUtil.getEmployeeAddressList(employeeAddressList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeAddressDO employeeEducationDO = new EmployeeAddressDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeAddressDO> employeeAddressList = new EmployeeAddressService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeEducationDO = employeeAddressList.get(0);
			 		employeeEducationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		employeeEducationDO.setCountryId(!inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()) : null);
			 		employeeEducationDO.setStateId(!inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()) : null);
			 		employeeEducationDO.setAddressTypelId(!inputJSON.get(CommonConstants.ADDRESSTYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ADDRESSTYPEID).toString()) : null);
			 		employeeEducationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeEducationDO.setNotes(!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : null);
			 		employeeEducationDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : null);
			 		employeeEducationDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
			 		employeeEducationDO.setUpdatedon(new Date());
			 		employeeEducationDO.setZip(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()) : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeEducationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeAddressService().update(employeeEducationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
