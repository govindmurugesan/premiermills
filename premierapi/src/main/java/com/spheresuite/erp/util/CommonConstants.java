package com.spheresuite.erp.util;

public final class CommonConstants {

	private CommonConstants() {

	}
	
	public static final String ID = "id";
	public static final String IDS = "ids";
	
	public static final String CREATED_ON = "createdon";
	
	public static final String UPDATED_ON = "updatedon";
	
	public static final String SUCCESS_FLAG = "successflag";
	
	public static final String NAME = "name";
	
	public static final String TEMP_PASSWORD="temppassword";

	public static final String ISDELETED="idDeleted";
	
	public static final String DESCRIPTION = "description";
	
	public static final String SESSION = "session";

	public static final String SUB_MENU="submenu";
	
	public static final String URL="url";
	
	public static final String CODE="code";
	
	public static final String CALLING_CODE="callingCode";
	
	public static final String CURRENCY_TYPE="currencyType";
	
	public static final String COUNTRY_ID="countryId";
	
	public static final String STATE_ID="stateId";
	
	public static final String CITY="city";
	
	public static final String COUNTRY_NAME="countryName";
	
	public static final String STATE_NAME="stateName";
	
	public static final String DESC="desc";
	
	public static final String ROLE_NAME="roleName";
	
	public static final String ROLE_ID="roleId";
	
	public static final String EMAIL="email";
	
	public static final String TANNO="tanno";
	public static final String PANNO="panno";
	public static final String ADDRESS1="address1";
	public static final String ADDRESS2="address2";
	public static final String STATE="state";
	public static final String COUNTRY="country";
	public static final String ZIP="zip";
	public static final String MOBILE="mobile";
	public static final String INDUSTRY="industry";
	public static final String WEBSITE="website";
	public static final String CREATEDON="createdon";
	public static final String UPDATEDON="updatedon";
	public static final String EMAIL_SEEN="emailseen";
	public static final String REASON="reason";
	
	public static final String WORKLOCATION="workLocation";
	public static final String EMPID="empId";
	public static final String FIRSTNAME="firstName";
	public static final String LASTNAME="lastName";
	public static final String MIDDLENAME="middleName";
	public static final String PERSONALCONTACT="personalContact";
	public static final String PRIMARYCONTACT="primaryContact";
	public static final String COMPANYEMAIL="companyEmail";
	public static final String PERSONALEMAIL="personalEmail";
	public static final String AADHAR="aadhar";
	public static final String DATEOFBIRTH="dateOfBirth";
	public static final String EMPTYPE="empType";
	public static final String REPORTTO="reportTo";
	public static final String DEPT="dept";
	public static final String TITLE="title";
	public static final String JOBDESC="jobDesc";
	public static final String STARTDATE="startDate";
	public static final String GENDER="gender";
	public static final String SOURCE="source";
	public static final String PHONE="phone";
	public static final String COMMENT="comment";
	public static final String COMMENT2="comment2";
	public static final String ADDRESS="address";
	public static final String CUSTOMERID="customerId";
	public static final String PROJECTNAME="projectName";
	public static final String DISPLAYNAME="displayName";
	public static final String PROJECTCODE="projectCode";
	public static final String ENDDATE="enddate";
	public static final String LEADTYPE="leadType";
	public static final String SALUTATION="salutation";
	public static final String PROJECTDURATION="projectDuration";
	public static final String SECONDARYEMAIL="secondaryEmail";
	public static final String PRIMARYEMAIL="primaryEmail";
	public static final String DESIGNATION="designation";
	public static final String MOBILE1="mobile1";
	public static final String MOBILE2="mobile2";
	public static final String PHONE1="phone1";
	public static final String PHONE2="phone2";
	public static final String PHONE3="phone3";
	public static final String PHONE4="phone4";
	public static final String FAX="fax";
	public static final String NOTE="note";
	public static final String REQUIREMENT="requirement";
	public static final String CONTACTTYPE="contacttype";
	public static final String MENU="menu";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	
	public static final String MAIL_SMTP_STARTTLS = "mail.smtp.starttls.enable";
	
	public static final String TRUE = "true";
	
	public static final String FALSE = "false";
	
	public static final String SUCCESS_MSG = "The e-mail was sent successfully";
	
	public static final String FAILURE_MSG = "Exception occured, kindly try after some time";
	
	public static final String INPUT_ERROR = "Invalid input error occurred, kindly chec your input";
	
	public static final String STATUS = "status";
	
	public static final String ERRORS = "errors";
	
	public static final String RESULTS = "results";
	
	public static final String RESPONSE = "response";
	
	public static final String COMMA = ",";
	
	public static final String HOST = "host";
	
	public static final String PORT = "port";
	
	public static final String FROMEMAIL = "FromEmail";
	
	public static final String PASSWORD = "password";
	
	public static final String TOEMAIL = "ToEmail";
	
	public static final String CCEMAIL = "CcEmail";
	
	public static final String BCCMAIL = "BccEmail";
	
	public static final String EMAILBODY = "EmailBody";
	
	public static final String LABEL="label";
	
	public static final String RETURNURL = "ReturnURL";
	
	public static final String REQUESTTYPE= "requestType";
	
	public static final String HTMLCONTENT= "htmlContent";
	
	public static final String ACCESS_CONTROL_ALLOW_ORIGIN  = "Access-Control-Allow-Origin";
	
	public static final String CACHE_CONTROL   = "Cache-Control";
	
	public static final String NO_CACHE   = "no-cache";
	
	public static final String  STAR = "*";
	
	public static final String EMPTY = "";
	
	public static final String AJAX = "ajax";
	
	public static final String TEXT_HTML = "text/html";
	
	public static final String REG_EXP = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-_]+\\.)+[\\w]+[\\w]$";

	public static final String NEWPASSWORD="newpassword";

	public static final String WORKLOCATION_CITY="workLocationCity";
	
	public static final String EMPTYPE_NAME="emptypeName";
	
	public static final String DEPT_NAME="deptName";
	
	public static final String STATUS_NAME="statusName";
	
	public static final String INDUSTRY_NAME="industryName";
	
	public static final String LEAD_TYPE_NAME="leadTypeName";
	
	public static final String CREATED_BY="createdBy";
	
	public static final String UPDATED_BY="updatedBy";
	
	public static final String SALUTATION_NAME="salutationName";
	
	public static String CONTACTTYPE_NAME="contactTypeName";
	
	public static String CONTACT_ID="contactId";
	
	public static String SUBJECT="subject";
	public static String CONTENT="content";
	public static String ATTACH_FILE="attachFile";
	public static String SENTDATE="sentDate";
	public static String HOST_POP = "imap.gmail.com";
	public static String POP_PORT = "993";
	public static String USERNAME = "pramesh@saptalabs.com"; //username for the mail you want to read
	public static String EMAILPASSWORD = "ramesh123"; //password
	public static String saveDirectory = "D:/software";
	public static final String MAX_COUNT="maxCount";
	public static final String START_iNDEX="startIndex";
	public static final String COUNT = "count";
	public static final String REPORTTO_NAME = "reportToName";
	public static final String TYPE= "type";
	public static final String FILENAME= "fileName";	
	public static final String TYPENAME = "typeName";
	public static final String PROJECTTYPE="projectType";
	public static final String PROJECT_TYPE_NAME="projectTypeName";
	public static final String LEAD_ID="leadId";
	public static final String NOTES="notes";
	public static final String  LEAD_NAME="leadName";
	public static final String  BODY="body";
	public static final String TIME = "time";
	public static final String DATE = "date";
	public static final String TASKDETAIL = "taskDetail";
	public static final String CONTACTLIST = "contactList";
	public static final String ICON = "icon";
	public static final String ACTIVEICON="activeicon";
	public static final String EMPNAME= "empName";
	public static final String DURATION= "duration";
	public static final String CURRENCYTYPE= "currencyType";
	public static final String RATE= "rate";
	public static final String PROJECTID= "projectId";
	public static final String PAYMENTTERM = "paymentTerm";
	public static final String PAYMENTTERM_NAME = "PaymentTermName";
	public static final String EXPENSE="expense";
	public static final String EXPENSE_TERM="expenseTerm";
	public static final String SCOPE="scope";
	public static final String TERM="term";
	public static final String INVOICETERM="invoiceTerm";
	public static final String INVOICETERM_LIST = "invoiceTermList";
	public static final String PERCENTAGE = "percentage";
	public static final String CUSTOMERNAME="cutomerName";
	public static final String EMAIL_NOT_REGISTER = "Entered email is not registered";
	public static final String EMAIL_NOT_SENT = "Mail not sent";
	public static final char   PENDING_STATUS = 'P';
	public static final String FIELD_LIST="fieldList";
	public static final String KEY="key";
	public static final String VALUE="value";
	public static final String FILE="file";
	public static final String BILLNUMBER="billNumber";
	public static final String BILLDATE="billDate";
	public static final String DUEDATE="dueDate";
	public static final String FROMDATE="fromDate";
	public static final String TODATE="toDate";
	public static final String PROPOSAL_DETAILS="proposalDetails";
	public static final String PROPOSAL_ID="proposalId";
	public static final String QUANTITY ="quantity";
	public static final String INVOICETERM_TYPE="invoiceTermType";
	public static final String HOT="hot";
	public static final String WARM="warm";
	public static final String COLD="cold";
	public static final String PAIDDATE="paidDate";
	public static final String CURRENCYTYPE_NAME ="currencyTypeName";
	public static final String CUSTOMERLIST="totalCustomer";
	public static final String NEWCUSTOMER="newCustomer";
	public static final String SKIP="skip";
	
	public static final String TOKEN = "token";
	
	public static final String ERROR_CODE = "error_code";
	
	public static final String AUTHENTICATION_ERROECODE = "1000";
	public static final String Q1="q1";
	public static final String Q2="q2";
	public static final String Q3="q3";
	public static final String Q4="q4";
	public static final String ENV="env";
	public static final String HTTP="http://";
	public static final String HTTPS="https://";
	public static final String PHOTO="photo";
	public static final String HRPOLICY="hrPolicy";
	public static final String SALESPOLICY="salesPolicy";
	public static final char ACTIVE='a';
	public static final String USERID="userid";
	public static final String PERSONAL="personal";
	public static final String SECONDARY="secondary";
	public static final String CURRENT_TIME="currentTime";
	public static final String CURRENT_DATE="currentDate";
	public static final String SUPERMENU="supermenu";
	public static final String L="L";
	public static final String C="C";
	public static final char PEDING='p';
	public static final String IMAPS="imaps";
	public static final String EXCHANGE_IMAPS="imap";
	public static final String POP3="pop3";
	public static final String ATTACHMENT="attachment";
	public static final String CONTENT_TYPE="contentType";
	public static String EXCHANGE_PORT = "143";	
	public static String EXCHANGE_HOST_IMAP = "mail.premiermills.com";
	public static String GMAIL_PORT = "993";
	public static String GMAIL_HOST_POP = "imap.gmail.com";
	public static String POP3_HOST = "mail.premiermills.com";
	public static String INBOX="INBOX";
	public static String SENT_MAIL="[Gmail]/Sent Mail";
	public static String GMAIL="gmail";
	public static String TO="to";
	public static String NO_DUPLICATE="noDuplicate";
	public static String EMPID_FOUND="Employee Id Already Found";
	public static String COMPANYEMAIL_FOUND="Company Email Already Found";
	public static final String DEFAULT_LIST="defaultList";
	public static final String SUB_SUB_MENU="subSubMenu";
	public static final String TRANSFER_FORM="transferFrom";
	public static final String TRANSFER_TO="transferTo";
	public static final String TRANSFER_TYPE="transferType";
	public static final String FILEDATA = "fileData";
	public static final String COLUMFIELDS = "columFields";
	
	public static final char STATUS_OPEN = 'O';
	public static final String HOURS = "hours";
	public static final String LEAVEDAY = "leaveday";
	public static final String DAYS = "days";
	public static final char INACTIVE = 'i';
	
	public static final String LEAVETYPEFROMDATE = "leaveTypeFromDate";
	public static final String LEAVETYPETODATE = "leaveTypeToDate";
	public static final String USEDLEAVES = "usedLeaves";
	public static final String UID = "uId";
	public static final String  YEAR = "year";	
	public static final String WEEKENDDATE = "weekenddate";
	public static final char DRAFT = 'd';
	public static final String TIMESHEETLIST = "timeSheetList";
	public static final String TOTAl = "total";
	public static final char SUBMITTED = 's';
	public static final String EMPCTC = "empctc";
	public static final String TAXABLE = "taxable";
	public static final String MAXLIMIT = "maxLimit";
	public static final String SECTION = "section";
	public static final String ALLOWANCE_TYPE_ID="allowanceTypeId";
	public static final String ALLOWANCETYPE_NAME="allowanceTypeName";
	public static final String DEDUCTION_TYPE_ID="deductionTypeId";
	public static final String DEDUCTIONTYPE_NAME="deductionTypeName";
	public static final String PREPOST_TAX="prepostTax";
	public static final String FIXEDAMOUNT="fixedAmount";
	public static final String PERCENTAGEAMOUNT="percentageAmount";
	public static final String BASICGROSS_PAY="basicGrossPay";
	public static final String WAREHOUSEID="wareHouseId";
	public static final String PROPERTYTYPEID="propertyTypeId";
	public static final String STORAGETYPEID="storageTypeId";
	public static final String PROPERTYTYPE_NAME="propertyTypeName";
	public static final String STORAGETYPE_NAME="storageTypeName";
	public static final String MINEXP="minExp";
	public static final String MAXEXP="maxExp";
	public static final String COST="cost";
	public static final String NOTICE_PEROID="noticePeriod";
	public static final String NOOFPOSITION="noofPosititon";
	public static final String JOININGDATE="joiningDate";
	public static final String REQUIREMENT_ID="requirementId";
	public static final String OFFERDATE="offerDate";
	public static final String APPROVER_ID="approverId";
	public static final String APPROVER="approver";
	public static final String SUPERADMIN="Super Admin";
	public static final String SENDMAIL="noreply@saptalabs.com";
	public static final String SENDPASSWORD="spheresuite@789";
	public static final String CONTACT_NAME="contactName";
	public static final String PROBOBILITY="probability";
	public static final String UPDATED_BY_NAME="updatedByName";
	public static final String OPPORTUNITY_ID="opportunityId";
	public static final String OPPORTUNITY_NAME="opportunityName";
	public static final String SALESSTAGE="salesStage";
	public static final String SALESSTAGE_NAME="salesStageName";
	public static final String NUMBER="number";
	public static final String AMOUNT = "amount";
	public static final String PAYTO = "payto";
	public static final String CLEARED = "c";
	public static final String PAYMENTMODE = "paymentmode";
	public static final String CHEQUENUMBER = "chequenumber";
	public static final String NOTCLEARED = "n";
	public static final String AMOUNTTYPE = "amountype";
	public static final String ORDERNUMBER = "ordernumber";
	public static final String GST="gst";
	public static final String GST_PERCENTAGE="gstPercentage";
	public static final String BACSICPAY="50";
	public static final String MONTHLY="monthly";
	public static final String YTD="ytd";
	public static final String NETMONTHLY="netMonth";
	public static final String NETYEAR="netYear";
	public static final String EARNINGMONTHLY="earningMonth";
	public static final String EARNINGYEAR="earningYear";
	public static final String DEDUCTIONMONTHLY="deductionMonth";
	public static final String DEDUCTIONYEAR="deductionYear";
	public static final String DEDUCTIONLIST="employeeCompensationDeductionList";
	public static final String EARNINGLIST="employeeCompensationEarningList";
	public static final String EARNINGID="allowanceId";
	public static final String DEDUCTIONID="deductionId";
	public static final String EFFECTIVETO="effectiveTo";
	public static final String EFFECTIVEFROM="effectiveFrom";
	public static final String BACSICPAYLABEL="basicPay";
	public static final String EARNINGNAME="earningName";
	public static final String DEDUCTIONNAME="deductionName";
	public static final String PAYROLLMONTH="payrollMonth";
	public static final String PAYROLLLIST="payrollList";
	public static final String PAYROLLBATCHID="payrollBatchId";
	public static final String PAYROLLBATCHNAME="payrollBatchName";
	public static final String PAYROLLTYPE="payrollType";
	public static final String EDUCATIONLEVELID="educationLevelId";
	public static final String EDUCATIONLEVELNAME="educationLevelName";
	public static final String UNIVERSITY="university";
	public static final String AREA="area";
	public static final String FATHERNAME="fatherName";
	public static final String MOTHERNAME="motherName";
	public static final String ADDRESSTYPEID="addressTypeId";
	public static final String ADDRESSTYPENAME="addressTypeName";
	public static final String TERMINATETYPEID="terminateTypeId";
	public static final String TERMINATEDATE="terminateDate";
	public static final String TERMINATEREASON="terminateReason";
	public static final String BANKNAME="bankName";
	public static final String IFSCCODE="ifscCode";
	public static final String BRANCHNAME="branchName";
	public static final String ACCOUNTNAME="accountName";
	public static final String ACCOUNTNUMBER="accountNumber";
	public static final String VOTERID="voterId";
	public static final String RATIONCARD="rationCard";
	public static final String MARITALSTATUS="maritalStatus";
	public static final String SPOUSENAME="spouseName";
	public static final String CHILDREN="children";
	public static final String BLOODGROUP="bloodGroup";
	public static final String LICENSENUMBER="licenseNumber";
	public static final String DATEISSUED="dateIssued";
	public static final String EXPIRYDATE="expiryDate";
	public static final String DATEISSUEDNAME="dateIssuedName";
	public static final String EXPIRYDATENAME="expiryDateName";
	public static final String HOMEPHONE="homePhone";
	public static final String CELLPHONE="cellPhone";
	public static final String WORKPHONE="workPhone";
	public static final String OTHEREMAIL="otherEmail";
	public static final String BONUSMONTH="bonusMonth";
	public static final String PAIDON="paidOn";
	public static final String TAXMONTHLY="taxMonth";
	public static final String TAXYEAR="taxYear";
	public static final String TAXLIST="employeeCompensationTaxList";
	public static final String TAXID="taxId";
	public static final String TAXNAME="taxName";
	public static final String BONUSYEAR="bonusYear";
	public static final String HRREQUEST="hrRequest";
	public static final String PAYROLL="payroll";
	public static final String LOSSOFPAYMONTHLY="lossOfPayMonth";
	public static final String LOSSOFPAYYEAR="lossOfPayYear";
	public static final String SETTINGNAME="settingName";
	public static final String ITSAVINGSETTINGID="itSavingSettingId";
	public static final String ITSAVINGSETTINGNAME="itSavingSettingName";
	
	public static final String CC = "cc";
	
	public static final String FROM = "from";
	
	public static final String BCC = "bcc";
	
	public static final String PAYROLLSTATUS = "payrollStatus";
	public static final char NEW = 'n';
	public static final String FROMTIME = "fromTime";
	public static final String TOTIME = "toTime";
	public static final String FROMTIMEDISPLAY="fromTimeDisplay";
	public static final String TOTIMEDISPLAY="toTimeDisplay";
	public static final String FROMAMOUNT="fromAmount";
	public static final String TOAMOUNT="toAmount";
	public static final String TAXPAYEETYPE= "taxpayeeType";
	public static final String PTAMOUNT="ptAmount";
	public static final String FYFROM="fyFrom";
	public static final String FYTO="fyTo";
	public static final String TAXPAYEEID="taxpayeeId";
	public static final String TAXPAYEENAME="taxpayeeName";
	public static final String ENGAGED="Engaged";
	
	public static final String DEPTMNGR_ID ="deptMngrId";
	public static final String DEPTMNGR_NAME = "deptMngrName";
	public static final String ADMIN = "adminId";
	public static final String ADMINID ="100101";
	public static final String REPORTTO_EMAIL ="reportingEmail";
	public static final String DEPHEAD_EMAIL ="deptHeadEmail";
	public static final String REQUESTBY = "requestBy";
	
	public static final String RESIGNEDDATE="resignedDate";
	public static final String QUARTERLY="quarterly";
	public static final String FYID="fyid";
	public static final String FREQUENCY="frequency";
	public static final String PERIOD="period";
	
	public static final String MONTHLYC="Monthly";
	public static final String QUARTERLYC="Quarterly";
	public static final String YEARLYC="Yearly";
}
