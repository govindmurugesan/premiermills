package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.MailboxDAO;
import com.spheresuite.erp.domainobject.MailboxDO;
import com.spheresuite.erp.exception.AppException;

public class MailboxService {
	static Logger logger = Logger.getLogger(MailboxService.class.getName());
	
	public MailboxDO persist(MailboxDO mailboxDO) throws AppException {
		return new MailboxDAO().persist(mailboxDO);
	}
	
	public boolean persistList(List<MailboxDO> mailboxDO) throws AppException {
		return new MailboxDAO().persistList(mailboxDO);
	}

	/*public List<MailboxDO> retrieveByLead(Long leadId) throws AppException {
		return new MailboxDAO().retrieveByLead(leadId);
	}*/
	
	public List<MailboxDO> retrieveById(Long Id) throws AppException {
		return new MailboxDAO().retrieveById(Id);
	}
	
	/*public List<MailboxDO> retrieveLast(Long leadId) throws AppException {
		return new MailboxDAO().retrieveByLead(leadId);
	}*/
	
	
	public List<MailboxDO> retrieveAllMail() throws AppException {
		return new MailboxDAO().retrieveAllMail();
	}
	
	
}
