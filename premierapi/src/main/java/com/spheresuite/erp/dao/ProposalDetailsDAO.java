package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ProposalDetailsDAO {
	static Logger logger = Logger.getLogger(ProposalDetailsDAO.class.getName());
	private EntityManager em = null;
	
	public List<ProposalDetailsDO> persist(List<ProposalDetailsDO> proposalDetailsList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalDetailsList != null && proposalDetailsList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				if(proposalDetailsList != null && proposalDetailsList.size() > 0){
					Query q =  em.createQuery("delete from ProposalDetailsDO e where e.proposalId =:proposalId");
					q.setParameter(CommonConstants.PROPOSAL_ID, proposalDetailsList.get(0).getProposalId());
					q.executeUpdate();
				}
				for (ProposalDetailsDO proposalDetailsDO : proposalDetailsList) {
					em.persist(proposalDetailsDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalDetailsList;
	}
	
	public boolean delete(Long proposalId) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalId != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q =  em.createQuery("delete from ProposalDetailsDO e where e.proposalId =:proposalId");
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				q.executeUpdate();
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<ProposalDetailsDO> retrieveById(Long proposalId) throws AppException {
		List<ProposalDetailsDO> proposalList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ProposalDetailsDO.FIND_BY_PROPOSAL_ID);
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				proposalList = (List<ProposalDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalList;
	}
}
