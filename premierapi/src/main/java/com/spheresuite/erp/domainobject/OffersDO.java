package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="offers")
@TableGenerator(name ="offers", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "offers.findAll", query = "SELECT r FROM OffersDO r"),
    @NamedQuery(name = "offers.findById", query = "SELECT r FROM OffersDO r where r.id =:id"),
    @NamedQuery(name = "offers.findByStatus", query = "SELECT r FROM OffersDO r where r.status =:status"),
    @NamedQuery(name = "offers.findByProjectId", query = "SELECT r FROM OffersDO r where r.projectid =:id"),
})
public class OffersDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "offers.findAll";
	public static final String FIND_BY_ID = "offers.findById";
	public static final String FIND_BY_STATUS = "offers.findByStatus";
	public static final String FIND_BY_PROJECTID = "offers.findByProjectId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "offers")
	
	private Long id;
	private Long customerid;
	private Long projectid;
	private Long requirementid;
	private String offerDate;
	private String name;
	private String desg;
	private String joiningDate;
	private Long ctc;
	private String comments;
	private Long phone;
	private String addresss;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdby;
	private char status;
	private Long approverid;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Long customerid) {
		this.customerid = customerid;
	}
	public Long getProjectid() {
		return projectid;
	}
	public void setProjectid(Long projectid) {
		this.projectid = projectid;
	}
	public Long getRequirementid() {
		return requirementid;
	}
	public void setRequirementid(Long requirementid) {
		this.requirementid = requirementid;
	}
	public String getOfferDate() {
		return offerDate;
	}
	public void setOfferDate(String offerDate) {
		this.offerDate = offerDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesg() {
		return desg;
	}
	public void setDesg(String desg) {
		this.desg = desg;
	}
	public String getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}
	public Long getCtc() {
		return ctc;
	}
	public void setCtc(Long ctc) {
		this.ctc = ctc;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getPhone() {
		return phone;
	}
	public void setPhone(Long phone) {
		this.phone = phone;
	}
	public String getAddresss() {
		return addresss;
	}
	public void setAddresss(String addresss) {
		this.addresss = addresss;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getApproverid() {
		return approverid;
	}
	public void setApproverid(Long approverid) {
		this.approverid = approverid;
	}
}