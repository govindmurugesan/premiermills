package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeCertificationDO;
import com.spheresuite.erp.service.EmployeeCertificationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCertificationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeecertification")
public class EmployeeCertificationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCertificationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCertificationDO employeeCertificationDO = new EmployeeCertificationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeCertificationDO.setMonth(!inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() ? inputJSON.get(CommonConstants.MONTHLY).toString() : null);
			 		employeeCertificationDO.setTitle(!inputJSON.get(CommonConstants.TITLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TITLE).toString() : null);
			 		employeeCertificationDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		employeeCertificationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeCertificationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeCertificationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeCertificationDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeCertificationService().persist(employeeCertificationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCertificationDO> employeeCertificationList = new EmployeeCertificationService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCertificationUtil.getEmployeeCertificationList(employeeCertificationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCertificationDO employeeCertificationDO = new EmployeeCertificationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeCertificationDO> employeeCertificationList = new EmployeeCertificationService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeCertificationDO = employeeCertificationList.get(0);
			 		employeeCertificationDO.setMonth(!inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() ? inputJSON.get(CommonConstants.MONTHLY).toString() : null);
			 		employeeCertificationDO.setTitle(!inputJSON.get(CommonConstants.TITLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TITLE).toString() : null);
			 		employeeCertificationDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		employeeCertificationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeCertificationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeCertificationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeCertificationService().update(employeeCertificationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
