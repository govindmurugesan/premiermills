package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeCompensationDAO {
	static Logger logger = Logger.getLogger(EmployeeCompensationDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeCompensationDO persist(EmployeeCompensationDO EmloyeeCompensationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && EmloyeeCompensationDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(EmloyeeCompensationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmloyeeCompensationDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieve() throws AppException {
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCompensationDO.FIND_ALL);
				/*q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);*/
				empCompensationList = (List<EmployeeCompensationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveActive(Long empId) throws AppException {
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCompensationDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				q.setParameter(CommonConstants.EMPID, empId);
				empCompensationList = (List<EmployeeCompensationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveById(Long id) throws AppException {
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCompensationDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				empCompensationList = (List<EmployeeCompensationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCompensationDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeCompensationDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCompensationDO.FIND_BY_EMP_ID);
				q.setParameter(CommonConstants.EMPID, empId);
				//q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				empCompensationList = (List<EmployeeCompensationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	public EmployeeCompensationDO update(EmployeeCompensationDO EmloyeeCompensationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(EmloyeeCompensationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmloyeeCompensationDO;
	}

}
