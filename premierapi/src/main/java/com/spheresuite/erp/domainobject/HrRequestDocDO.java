package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrrequestdoc")
@TableGenerator(name ="hrrequestdoc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrrequestdoc.findAll", query = "SELECT r FROM HrRequestDocDO r"),
    @NamedQuery(name = "hrrequestdoc.findBySavingId", query = "SELECT r FROM HrRequestDocDO r where r.hrrequestId =:id"),
})
public class HrRequestDocDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrrequestdoc.findAll";
	
	public static final String FIND_BY_ITSAVING_ID = "hrrequestdoc.findBySavingId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "hrrequestdoc")
	private Long id;
	private Long hrrequestId;
	@Column(columnDefinition="LONGTEXT")
	private String photo;
	private String fileName;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getHrrequestId() {
		return hrrequestId;
	}
	public void setHrrequestId(Long hrrequestId) {
		this.hrrequestId = hrrequestId;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}