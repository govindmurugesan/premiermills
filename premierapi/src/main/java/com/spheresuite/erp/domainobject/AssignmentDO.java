package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="assignment")
@TableGenerator(name ="assignment", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "assignment.findAll", query = "SELECT r FROM AssignmentDO r"),
    @NamedQuery(name = "assignment.findAllData", query = "SELECT r FROM AssignmentDO r"),
    @NamedQuery(name = "assignment.findByStaus", query = "SELECT r FROM AssignmentDO r where r.status =:status"),
    @NamedQuery(name = "assignment.findById", query = "SELECT r FROM AssignmentDO r where r.id =:id"),
    @NamedQuery(name = "assignment.findByEmId", query = "SELECT r FROM AssignmentDO r where r.empId =:id and r.status =:status"),
    @NamedQuery(name = "assignment.findByOppId", query = "SELECT r FROM AssignmentDO r where r.opportunityId =:id"),
})
public class AssignmentDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "assignment.findAll";
	public static final String FIND_BY_ID = "assignment.findById";
	public static final String FIND_BY_EMPID = "assignment.findByEmId";
	public static final String FIND_BY_STATUS = "assignment.findByStaus";
	public static final String FIND_BY_OPP_ID = "assignment.findByOppId";
	public static final String FIND_ALL_DATA = "assignment.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "project")
	private Long id;
	private Long opportunityId;
	private Long projectId;
	private Long customerId;
	private String projectType;
	private String startdate;
	private String enddate;
	private String orderNumber;
	private Long amountType;
	private Long amount;
	private Long paymentTerms;
	private String status;
	private String comments;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public Long getAmountType() {
		return amountType;
	}
	public void setAmountType(Long amountType) {
		this.amountType = amountType;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(Long paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	
	
}