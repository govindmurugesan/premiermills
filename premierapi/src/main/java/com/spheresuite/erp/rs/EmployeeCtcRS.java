package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.service.EmployeeCtcService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCtcUtil;
import com.spheresuite.erp.web.util.EmployeeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empctc")
public class EmployeeCtcRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCtcRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCtcDO empCompensationDO = new EmployeeCtcDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		List<EmployeeCtcDO> empCompensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
		 			if(empCompensationList != null && empCompensationList.size() > 0){
		 				return CommonWebUtil.buildErrorResponse("Employee already have active CTC").toString();
		 			}else{
		 				empCompensationDO.setPayrollbatchId(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()) : null);
		 				empCompensationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
				 		empCompensationDO.setEmpctc(!inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()) : null);
				 		empCompensationDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
				 		empCompensationDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
				 		empCompensationDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			empCompensationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			empCompensationDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
				 		}
				 		empCompensationDO.setStatus('a');
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Componsation Created");
		 			}
			 		
			 	}
				new EmployeeCtcService().persist(empCompensationDO);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeeCtcUtil.getempCompensationList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveActive/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveActive(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeeCtcUtil.getempCompensationList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByEmpIdWithEarnings/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpIdWithEarnings(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActiveWithDate(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()),CommonUtil.convertStringToSqlDate(CommonUtil.convertDateToYearWithOutTime(new Date())));
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarnings(compensationList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarnings/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarnings(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarnings(compensationList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarningsDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarningsDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarningsDateYtd(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveWithEarningsDateYtd/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveWithEarningsDateYtd(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCtcDO> compensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCtcUtil.getempCompensationListWithEarningsDateYtd(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCtcDO> empCompensationList = new EmployeeCtcService().retrieve();
				respJSON = EmployeeCtcUtil.getempCompensationList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/empwithoutctc", method = RequestMethod.GET)
	public @ResponseBody String empWithOutCtc(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCtcDO> empCompensationList = new EmployeeCtcService().retrieve();
				List<Long> empIds = new ArrayList<Long>();
				for (EmployeeCtcDO employeeCtcDO : empCompensationList) {
					empIds.add(employeeCtcDO.getEmpId());
				}
				List<EmployeeDO> employeeList = new EmployeeService().retrieveActiveNoCtc(empIds);
				respJSON = EmployeeUtil.getEmployeeListWithOutPhoto(employeeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCtcDO empCompensationDO = new EmployeeCtcDO();
				int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeCtcDO> empCompensationList = new EmployeeCtcService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				EmployeeCtcDO empCompensation = new EmployeeCtcDO();
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStatus('i');
			 				if(empCompensation.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensation.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							empCompensation.setEnddate(dateBefore1Days);
			 						}else{
			 							empCompensation.setEnddate(empCompensation.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				if(i == 0){
			 					new EmployeeCtcService().update(empCompensation);
			 				}
			 			}
			 		}
			 		
			 		empCompensationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		empCompensationDO.setEmpctc(!inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()) : null);
			 		empCompensationDO.setPayrollbatchId(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()) : null);
			 		empCompensationDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		empCompensationDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		empCompensationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		empCompensationDO.setStatus('a');
			 	}
			 	if(i == 0){
			 		new EmployeeCtcService().update(empCompensationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Compensation Updated");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateDate", method = RequestMethod.POST)
	public @ResponseBody String updateDate(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				EmployeeCtcDO empCompensation = new EmployeeCtcDO();
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<EmployeeCtcDO> empCompensationList = new EmployeeCtcService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 				empCompensation.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 				empCompensation.setPayrollbatchId(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()) : null);
			 				empCompensation.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			empCompensation.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			empCompensation.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
			 			}
			 		}
			 	}
		 		new EmployeeCtcService().update(empCompensation);
		 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Compensation Updated");
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
