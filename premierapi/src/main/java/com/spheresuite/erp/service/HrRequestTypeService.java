package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HrRequestTypeDAO;
import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.exception.AppException;

public class HrRequestTypeService {
	static Logger logger = Logger.getLogger(HrRequestTypeService.class.getName());
	
	public HrRequestTypeDO persist(HrRequestTypeDO requestType) throws AppException {
		return new HrRequestTypeDAO().persist(requestType);
	}

	public List<HrRequestTypeDO> retrieve() throws AppException {
		return new HrRequestTypeDAO().retrieve();
	}
	
	public List<HrRequestTypeDO> retrieveById(Long Id) throws AppException {
		return new HrRequestTypeDAO().retrieveById(Id);
	}
	
	
	public HrRequestTypeDO update(HrRequestTypeDO requestType) throws AppException {
		return new HrRequestTypeDAO().update(requestType);
	}
}
