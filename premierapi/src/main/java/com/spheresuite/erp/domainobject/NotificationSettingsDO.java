package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="notificationsettings")
@TableGenerator(name ="notificationsettings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "notificationsettings.findAll", query = "SELECT r FROM NotificationSettingsDO r"),
    @NamedQuery(name = "notificationsettings.findById", query = "SELECT r FROM NotificationSettingsDO r where r.id =:id"),
})
public class NotificationSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "notificationsettings.findAll";
	
	public static final String FIND_BY_ID = "notificationsettings.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "notificationsettings")
	private Long id;
	private String hrRequest;
	private String payroll;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHrRequest() {
		return hrRequest;
	}
	public void setHrRequest(String hrRequest) {
		this.hrRequest = hrRequest;
	}
	public String getPayroll() {
		return payroll;
	}
	public void setPayroll(String payroll) {
		this.payroll = payroll;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
}