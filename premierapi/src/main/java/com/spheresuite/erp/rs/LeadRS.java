package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.ContactService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.ProposalService;
import com.spheresuite.erp.service.RequirementsService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeadUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/lead")
public class LeadRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		LeadDO leadDetails	= new LeadDO();
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		leadDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			leadDO.setStatus(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : ""));
			 		}
			 		if(!inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			leadDO.setIndustryID(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null));
			 		}
			 		leadDO.setSource(!inputJSON.get(CommonConstants.SOURCE).toString().isEmpty() ? inputJSON.get(CommonConstants.SOURCE).toString() : null);
			 		
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			leadDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			leadDO.setPhone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		}
			 		
			 		leadDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : null);
			 		leadDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
			 		leadDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			leadDO.setLeadType(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTYPE).toString() : ""));
			 		}
			 		
			 		leadDO.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : null);
			 		leadDO.setCreatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			leadDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		leadDO.setIsDeleted("n");
			 		leadDO.setUpdatedon(new Date());
			 		
			 	}
			 	leadDetails	= new LeadService().persist(leadDO);
			 	if(!inputJSON.get(CommonConstants.SKIP).toString().isEmpty()){
			 		if(inputJSON.get(CommonConstants.SKIP).toString().equalsIgnoreCase("no")){
			 			if(leadDetails.getId() != null){
					 		ContactDO contactDO = new ContactDO();
					 		contactDO.setLeadtype(leadDetails.getId());
					 		if(!inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
						 		contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
					 		}
					 		contactDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : null);
					 		contactDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : null);
					 		contactDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : null);
					 		contactDO.setDesignation((!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : null));
					 		if(!inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
					 			contactDO.setContattype(Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString()));
					 		}
					 		contactDO.setPrimaryemail((!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : null));
					 		contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : null));
					 		contactDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : null);
					 		contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
					 		if(!inputJSON.get(CommonConstants.MOBILE1).toString().isEmpty()){
						 		contactDO.setMobile1(Long.parseLong(inputJSON.get(CommonConstants.MOBILE1).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
					 			contactDO.setMobile2(Long.parseLong(inputJSON.get(CommonConstants.MOBILE2).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
						 		contactDO.setPhone1(Long.parseLong(inputJSON.get(CommonConstants.PHONE1).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
						 		contactDO.setPhone2(Long.parseLong(inputJSON.get(CommonConstants.PHONE2).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
						 		contactDO.setPhone3(Long.parseLong(inputJSON.get(CommonConstants.PHONE3).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.PHONE4).toString().isEmpty()){
						 		contactDO.setPhone4(Long.parseLong(inputJSON.get(CommonConstants.PHONE4).toString()));
					 		}
					 		if(!inputJSON.get(CommonConstants.FAX).toString().isEmpty()){
						 		contactDO.setFax(Long.parseLong(inputJSON.get(CommonConstants.FAX).toString()));
					 		}
					 		contactDO.setWebsite((!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : null));
					 		contactDO.setRequirements((!inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : null));
					 		contactDO.setNotes((!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : null));
					 		contactDO.setComments((!inputJSON.get(CommonConstants.COMMENT2).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT2).toString() : null));
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			contactDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		contactDO.setIsDeleted("n");
					 		 new ContactService().persist(contactDO);
					 	}else{
					 		return CommonWebUtil.buildErrorResponse("").toString();
					 	}
			 		}
			 	}
			 	CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Lead Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(leadDetails.getId()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					int i = 0;
					List<Long> deptIds = new ArrayList<Long>();
					if(request.getSession().getAttribute("deptIds") != null){
						deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
					}
					List<Long> empIds = new ArrayList<Long>();
					if(deptIds != null && deptIds.size() > 0){
						empIds = new EmployeeService().retrieveByDeptIds(deptIds);
					}else if(request.getSession().getAttribute("empIds") != null){
						empIds = new ArrayList<Long>();
						Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
						empIds.add(empid);
						List<UserDO> userList = new UserService().retriveByEmpId(empid);
						if(userList.get(0).getRoleId() != null){
							List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
							if(roleList != null && roleList.size() > 0){
								if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
									i=1;
								}
							}else{
								empIds = new ArrayList<Long>();
							}
						}else{
							empIds = new ArrayList<Long>();
						}
						//empIds = new EmployeeService().retrieveByDeptIds(deptIds);
					}
					if(i == 1){
						List<LeadDO> leadList = new LeadService().retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadListWithOutPic(leadList).toString();
					}else{
						List<LeadDO> leadList = new LeadService().retrieve(inputJSON.get(CommonConstants.TYPE).toString(), empIds);//retrieveAll(inputJSON.get(CommonConstants.TYPE).toString())/*inputJSON.get(CommonConstants.TYPE).toString(), empIds*/;
						respJSON = LeadUtil.getLeadList(leadList).toString();
						/*List<LeadDO> leadList = new LeadService().retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadListWithOutPic(leadList).toString();*/
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				//List<Long> deptIds = new ArrayList<Long>();
				List<LeadDO> leadList = new ArrayList<LeadDO>();
				/*if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empId") != null){
					empIds = new ArrayList<Long>();
					empIds.add((Long)request.getSession().getAttribute("empId"));
				}*/
				leadList = new LeadService().retrieveAll();
				respJSON = LeadUtil.getLeadLimitData(leadList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByType/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByType(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
					if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
						List<LeadDO> leadList = new LeadService().retrieveAll(inputJSON.get(CommonConstants.TYPE).toString());
						respJSON = LeadUtil.getLeadList(leadList).toString();
				}else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<LeadDO> LeadList = new LeadService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
			 		leadDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			leadDO.setStatus(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : ""));
			 		}
			 		if(!inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty()){
			 			leadDO.setIndustryID(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null));
			 		}
			 		leadDO.setSource(!inputJSON.get(CommonConstants.SOURCE).toString().isEmpty() ? inputJSON.get(CommonConstants.SOURCE).toString() : null);
			 		
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			leadDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			leadDO.setPhone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		}
			 		
			 		leadDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString() : null);
			 		leadDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
			 		leadDO.setComment(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty()){
			 			leadDO.setLeadType(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.LEADTYPE).toString() : ""));
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		leadDO.setCreatedon(new Date());
			 		leadDO.setUpdatedon(new Date());
			 		new LeadService().update(leadDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Lead Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/convertAsCustomer", method = RequestMethod.POST)
	public @ResponseBody String convertAsCustomer(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(LeadList != null && LeadList.size() > 0){
						LeadDO leadDO = new LeadDO();
						leadDO = LeadList.get(0);
						if(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
							leadDO.setConvertCustomerComments(inputJSON.get(CommonConstants.COMMENT).toString());
						}
						leadDO.setUpdatedon(new Date());
						leadDO.setConvertedDate(new Date());
						leadDO.setType("C");
						new LeadService().update(leadDO);
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();
					fromDate = fromDate.substring(0, 10)+" 00:00:00";
					toDate = toDate.substring(0, 10)+" 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> LeadList = new LeadService().retrieveBetweenDate(formattedDate,formattedDate1);
					respJSON = LeadUtil.getLeadListForChart(LeadList).toString();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDateByEmp/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDateByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();
					String empId = "";
					if(inputJSON.get(CommonConstants.EMPID).toString() != null){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					fromDate = fromDate.substring(0, 10)+" 00:00:00";
					toDate = toDate.substring(0, 10)+" 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> LeadList = new ArrayList<LeadDO>();
					if(empId != null && !empId.isEmpty()){
						LeadList = new LeadService().retrieveBetweenDateByEmp(formattedDate,formattedDate1, empId);
					}else{
						LeadList = new LeadService().retrieveBetweenDate(formattedDate,formattedDate1);
					}
					respJSON = LeadUtil.getLeadListForChart(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildSuccessResponse().toString();
	}
	

	@RequestMapping(value = "/importLeads/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY);
				Object type =  inputJSON.get(CommonConstants.TYPE); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<LeadDO> leadDOlist = new ArrayList<LeadDO>();
				
				for (int i=0; i < fileData.size(); i++){
					LeadDO leadDO = new LeadDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.NAME) != null){
						leadDO.setName(!rowJSON.get(colName.get(CommonConstants.NAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.NAME)).toString() : null);
					}
					if(colName.get(CommonConstants.STATUS) != null){
						if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
				 			leadDO.setStatus(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.STATUS)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.INDUSTRY) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString().isEmpty()){
				 			leadDO.setIndustryID(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.INDUSTRY)).toString() : null));
				 		}
					}
					if(colName.get(CommonConstants.SOURCE) != null){
						leadDO.setSource(!rowJSON.get(colName.get(CommonConstants.SOURCE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.SOURCE)).toString() : null);
					}
					if(colName.get(CommonConstants.MOBILE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
				 			leadDO.setMobile(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE)).toString().isEmpty()){
				 			leadDO.setPhone(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.EMAIL) != null){
						leadDO.setEmail(!rowJSON.get(colName.get(CommonConstants.EMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.EMAIL)).toString() : null);
					}
					if(colName.get(CommonConstants.ADDRESS) != null){
						leadDO.setAddress(!rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS)).toString() : null);
					}
					if(colName.get(CommonConstants.COMMENT) != null){
						leadDO.setComment(!rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : null);
					}
					if(colName.get(CommonConstants.LEADTYPE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty()){
				 			leadDO.setLeadType(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString() : ""));
				 		}
					}
			 		leadDO.setType(!type.toString().isEmpty() ? type.toString() : null);
			 		leadDO.setCreatedon(new Date());
			 		if(updatedBy.toString() != null && !updatedBy.toString().isEmpty()){
			 			leadDO.setUpdatedby(updatedBy.toString());
			 			leadDO.setEmpId(Long.parseLong(updatedBy.toString()));
			 		}
			 		leadDO.setUpdatedon(new Date());
			 		leadDO.setIsDeleted("n");
			 		leadDOlist.add(leadDO);
				}
				
				new LeadService().persistList(leadDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}

	@RequestMapping(value = "/retrieveLeadByEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeadByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveLeadByEmp(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()),inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/transferLead", method = RequestMethod.POST)
	public @ResponseBody String transferLead(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
					Long transferTo=!inputJSON.get(CommonConstants.TRANSFER_TO).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.TRANSFER_TO).toString()): null;
					Long transferFrom=!inputJSON.get(CommonConstants.TRANSFER_FORM).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.TRANSFER_FORM).toString()): null;
					String transferType=!inputJSON.get(CommonConstants.TRANSFER_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TRANSFER_TYPE).toString(): null;
					Date fromDate = null;
					Date toDate = null;
					if(transferType != null && !transferType.isEmpty()){
						leadDO.setTransferfrom(transferFrom);
						leadDO.setEmpId(transferTo);
						leadDO.setTransferType(transferType);
						if(transferType.equalsIgnoreCase("T")){
							fromDate = !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.FROMDATE).toString()): null;
							toDate = !inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDate(inputJSON.get(CommonConstants.TODATE).toString()): null;
							leadDO.setTransferFromDate(fromDate);
							leadDO.setTransferToDate(toDate);
						}
						if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		leadDO.setUpdatedon(new Date());
				 		new LeadService().update(leadDO);
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveTransferLead/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeadByTransferType(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty()){
					/*List<LeadDO> LeadList = new LeadService().retrieveLeadByTransferType("T",inputJSON.get(CommonConstants.TYPE).toString());*/
					List<LeadDO> LeadList = new LeadService().retrieveLeadByAllTransferType(inputJSON.get(CommonConstants.TYPE).toString());
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveTransferLeadById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveTransferLeadById(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.TYPE).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveTransferLeadById("T",inputJSON.get(CommonConstants.TYPE).toString(),Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
					respJSON = LeadUtil.getLeadList(LeadList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/revokeLead", method = RequestMethod.POST)
	public @ResponseBody String revokeLead(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadDO> LeadList = new LeadService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		leadDO = LeadList.get(0);
			 		leadDO.setEmpId(leadDO.getTransferfrom());
			 		leadDO.setTransferfrom(null);
					leadDO.setTransferType(null);
					leadDO.setTransferFromDate(null);
					leadDO.setTransferToDate(null);
					leadDO.setUpdatedon(new Date());
					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			leadDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new LeadService().update(leadDO);
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeadDO leadDO = new LeadDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("id") != null){
			 		List<LeadDO> leadList = new LeadService().retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(request.getParameter("file") != null && !request.getParameter("file").equals("")){
			 			if(leadList != null && leadList.size() > 0){
				 			leadDO = leadList.get(0);
				 			leadDO.setPhoto(request.getParameter("file"));
				 			leadDO.setUpdatedon(new Date());
					 		new LeadService().update(leadDO);
				 		}
			 		}else{
			 			if(leadList != null && leadList.size() > 0){
				 			leadDO = leadList.get(0);
				 			leadDO.setPhoto(null);
				 			leadDO.setUpdatedon(new Date());
					 		new LeadService().update(leadDO);
				 		}
			 		}
			 		
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeadDO> leadList = new ArrayList<LeadDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.IDS) != null){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		String updatedBy = "";
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				updatedBy = inputJSON.get(CommonConstants.UPDATED_BY).toString();
			 		}
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			boolean flag = true;
			 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
			 				List<ContactDO> contactList = new ContactService().retriveByLeadId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(contactList != null && contactList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<OpportunitiesDO> opportunitiesList = new OpportunitiesService().retrieveByCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(opportunitiesList != null && opportunitiesList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<ProposalDO> proposalList = new ProposalService().retrieveCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(proposalList != null && proposalList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				List<RequirementsDO> requirementList = new RequirementsService().retrieveByCustomerId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(requirementList != null && requirementList.size() > 0){
			 					flag = false;
			 				}
			 				if(resultJSONArray1.size() == 1){
			 					if(flag){
				 					List<LeadDO> leadDoList = new LeadService().retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(leadDoList != null && leadDoList.size() > 0){
				 						leadDoList.get(0).setIsDeleted("y");
				 						leadDoList.get(0).setUpdatedby(updatedBy);
				 						leadList.add(leadDoList.get(0));
				 					}
				 				}else{
				 					return CommonWebUtil.buildErrorResponse("Cannot Delete").toString();
			 					}
			 				}else{
			 					if(flag){
				 					List<LeadDO> leadDoList = new LeadService().retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(leadDoList != null && leadDoList.size() > 0){
				 						leadDoList.get(0).setIsDeleted("y");
				 						leadDoList.get(0).setUpdatedby(updatedBy);
				 						leadList.add(leadDoList.get(0));
				 					}
				 				}
			 				}
			 				
			 			}
			 		}
			 		new LeadService().update(leadList);
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
