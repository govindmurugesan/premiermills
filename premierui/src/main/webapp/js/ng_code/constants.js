(function() {
    'use strict';

    var apiRoutesUrl = {
        //address summary
        addAddressNewType:'addresstype/persist',
        addressTypeRetrieve:'addresstype/retrieve',
        addressTypeUpdate : 'addresstype/update',
    	//payroll summary
    	payrollsummary: 'empPayroll/retrieveSummary',
    	payrollsummaryRetrieveAllEmployee: 'empPayroll/retrieveByAllEmp/',
    	payrollsummaryDetails: 'empPayroll/retriveById/',
    	//terminate Reason
    	addterminateReason : 'terminatereasontype/persist',
        updateTermianteReason : 'terminatereasontype/update',
        getTerminateReason : 'terminatereasontype/retrieve',
    	//Education level
    	educationlevelRetrieve: 'educationlevel/retrieve',
    	educationlevelPersist: 'educationlevel/persist',
    	educationlevelUpdate: 'educationlevel/update',
    	educationlevelRetrieveActive: 'educationlevel/retrieveActive',
    	
        // bill
        billPersist: 'bills/persist',
        billRetrieve: 'bills/retrieve/',
        billRetrieveById: 'bills/retriveById/',
        billUpdate: 'bills/update',
        billImport: 'bills/importBills/',
        billNumber: 'bills/getBillNumber/',
  
        
        //check 
        ChequePersist: 'chequemanagement/persist',
        ChequeRetrieve: 'chequemanagement/retrieve/',
        ChequeRetrieveById: 'chequemanagement/retriveById/',
        ChequeUpdate: 'chequemanagement/update', 

        // category
        categoryUpdate: 'category/update',

        // customer
        /*customerPersist: 'customer/persist',
        customerRetrieve: 'customer/retrieve/',
        customerUpdate: 'customer/update',
        customerDelete: 'customer/delete/',*/

        // proposal
        proposalPersist: 'proposals/persist',
        proposalRetrieve: 'proposals/retrieve/',
        proposalUpdate: 'proposals/update',
        proposalRetrieveById: 'proposals/retriveById/',
        proposalRetrieveByCustomerId: 'proposals/retrieveByCustomerId/',

        // contact
        contactDelete: 'contact/delete',
        contactPersist: 'contact/persist',
        contactRetrieve: 'contact/retrieve/',
        contactRetrieveById: 'contact/retriveById/',
        contactRetrieveByLeadId: 'contact/retrieveByLeadId/',
        contactUpdate: 'contact/update',
      //  contactDelete: 'contact/delete/',
        calendarUpcoming: 'schedule/retrieveUpcomingCalender/',
        importContact: 'contact/importContact/',
        
        //Employee onboard
        employeeOnboardPersist: 'employeeonboard/persist',
        employeeOnboardRetrieveById: 'employeeonboard/retrieveByOnboardId/',
        employeeOnboardUpdate: 'employeeonboard/update/',
        employeeOnboardRetrieveAll: 'employeeonboard/retrieve/',


        //Configuration
        configCountryDelete: 'category/delete/',
        configCountryPersist: 'country/persist',
        configCountryRetrieve: 'country/retrieve/',
        configStateRetrieveId: 'state/retrieveActive/',
        configStatePersist: 'state/persist',
        configWorkLocationPersist: 'worklocation/persist',
        configStateDelete: 'category/delete/',
        configStateUpdate: 'state/update',
        configStateRetrieve: 'state/retrieve/',
        configStateRetrieveById: 'state/retrieveByCountryId/',
        configWorkLocationRetrieve: 'worklocation/retrieve/',
        configWorkLocationRetrieveStatus: 'worklocation/retrieveActive/',
        configWorkLocationUpdate: 'worklocation/update',

        // configuration Category
        configCategoryPersist: 'category/persist',
        configCategoryRetrieve: 'category/retrieve/',
        configCategoryRetrieveById: 'category/RetrieveById/',
        configCategoryUpdate: 'category/update',

        // configuration Contact
        configContactPersist: 'contacttype/persist',
        configContactRetrieve: 'contacttype/retrieve/',
        configContactRetrieveById: 'contacttype/RetrieveById/',
        configContactUpdate: 'contacttype/update',

        // configuration Department
        configDepartmentPersist: 'department/persist',
        configDepartmentRetrieve: 'department/retrieve/',
        configDepartmentRetrieveActive: 'department/retrieveActive/',
        configDepartmentRetrieveById: 'department/retrieveById/',
        configDepartmentUpdate: 'department/update',
        configDepartmentRetrieveStatus: 'department/retrieveActive/',

        // configuration Employee
        configEmployeePersist: 'employeetype/persist',
        configEmployeeRetrieve: 'employeetype/retrieve/',
        configEmployeeRetrieveById: 'employeetype/retrieveById/',
        configEmployeeUpdate: 'employeetype/update',
        configEmployeeRetrieveStatus: 'employeetype/retrieveActive/',

        // configuration hrRequestType
        confighrRequestTypePersist: 'hrRequestType/persist',
        confighrRequestTypeRetrieve: 'hrRequestType/retrieve/',
        confighrRequestTypeRetrieveById: 'hrrequests/retrieveById/',
        confighrRequestTypeUpdate: 'hrRequestType/update',
        
       // configuration taxpayeetype
        configTaxPayeeTypePersist: 'taxpayeetype/persist',
        configTaxPayeeTypeRetrieve: 'taxpayeetype/retrieve/',
        configTaxPayeeTypeRetrieveById: 'taxpayeetype/retrieveById/',
        configTaxPayeeTypeRetrieveActive: 'taxpayeetype/retrieveActive/',
        configTaxPayeeTypeUpdate: 'taxpayeetype/update',
        
     // configuration professional Tax
        configPTPersist: 'pt/persist',
        configPTRetrieve: 'pt/retrieve/',
        configPTRetrieveById: 'pt/retrieveById/',
        configPTUpdate: 'pt/update',
        PTDetailsByStateId: 'pt/retriveByState/',
        
     // configuration Financial Year
        configFYPersist: 'fy/persist',
        configFYRetrieve: 'fy/retrieve/',
        configFYRetrieveActive: 'fy/retrieveActive/',
        configFYRetrieveById: 'fy/retrieveById/',
        configFYUpdate: 'fy/update',
        FYRetrieveActivePayee: 'taxpayeetype/retrieveActivePayee/',
        
        // configuration Industry
        configIndustryPersist: 'industry/persist',
        configIndustryRetrieve: 'industry/retrieve/',
        configIndustryRetrieveById: 'industry/retrieveById/',
        configIndustryUpdate: 'industry/update',

        // configuration leadStatus
        configLeadStatusPersist: 'leadstatus/persist',
        configLeadStatusRetrieve: 'leadstatus/retrieve/',
        configLeadStatusRetrieveById: 'leadstatus/retrieveById/',
        configLeadStatusUpdate: 'leadstatus/update',

        // configuration leadType
        configLeadTypePersist: 'leadtype/persist',
        configLeadTypeRetrieve: 'leadtype/retrieve/',
        configLeadTypeRetrieveById: 'leadtype/retrieveById/',
        configLeadTypeUpdate: 'leadtype/update',
        
        // configuration manage leaves
        configmanageLeavesPersist: 'leaveType/persist',
        configmanageLeavesRetrieve: 'leaveType/retrieve/', 
        configmanageLeavesUpdate: 'leaveType/update',
        
        configleaveTypePersist: 'leavemanagement/persist',
        configleaveTypeRetrieve: 'leavemanagement/retrieve', 
        configleaveTypeUpdate: 'leavemanagement/update',
         

        // configuration Salution
        configSalutionPersist: 'salutation/persist',
        configSalutionRetrieve: 'salutation/retrieve/',
        configSalutionRetrieveById: 'salutation/retrieveById/',
        configSalutionUpdate: 'salutation/update',

        // configuration projectTYpe
        configProjectPersist: 'projecttype/persist',
        configProjectRetrieve: 'projecttype/retrieve/',
        configProjectRetrieveById: 'projecttype/retrieveById/',
        configProjectUpdate: 'projecttype/update',

        // configuration deductaionType
        configDeductionTypePersist: 'deductiontype/persist',
        configDeductionTypeRetrieve: 'deductiontype/retrieve/',
        configDeductionTypeRetrieveById: 'deductiontype/retrieveById/',
        configDeductionTypeUpdate: 'deductiontype/update',

        // configuration taxType
        configtaxTypePersist: 'taxtype/persist',
        configtaxTypeRetrieve: 'taxtype/retrieve/',
        configtaxTypeRetrieveById: 'taxtype/retrieveById/',
        
        // configuration allowanceType
        configAllowanceTypePersist: 'allowancetype/persist',
        configAllowanceTypeRetrieve: 'allowancetype/retrieve/',
        configAllowanceTypeRetrieveById: 'allowancetype/retrieveById/',
        configAllowanceTypeUpdate: 'allowancetype/update',
        
        // configuration payrollBatch
        configPayrollBatchPersist: 'payrollbatch/persist',
        configPayrollBatchRetrieve: 'payrollbatch/retrieve/',
        configPayrollBatchRetrieveById: 'payrollbatch/retrieveById/',
        configPayrollBatchUpdate: 'payrollbatch/update',
        configPayrollBatchRetrieveActive: 'payrollbatch/retrieveActive',
        
        // configuration property
        configPropertyTypePersist: 'propertytype/persist',
        configPropertyTypeRetrieve: 'propertytype/retrieve/',
        configPropertyTypeRetrieveById: 'propertytype/retrieveById/',
        configPropertyTypeUpdate: 'propertytype/update',
        
        // configuration storageType
        configStorageTypePersist: 'storagetype/persist',
        configStorageTypeRetrieve: 'storagetype/retrieve/',
        configStorageTypeRetrieveById: 'storagetype/retrieveById/',
        configStorageTypeUpdate: 'storagetype/update',
        
        // configuration SalesStageType
        configSalesStagePersist: 'salestage/persist',
        configSalesStageRetrieve: 'salestage/retrieve/',
        configSalesStageRetrieveById: 'salestage/retrieveById/',
        configSalesStageUpdate: 'salestage/update',
        
        // configuration Payment Mode
        configPaymentModePersist: 'paymentmode/persist',
        configPaymentModeRetrieve: 'paymentmode/retrieve/',
        configPaymentModeRetrieveById: 'paymentmode/retrieveById/',
        configPaymentModeUpdate: 'paymentmode/update',
        
     // configuration Invoicing terms
        configInvoicingTermsPersist: 'invoiceTerms/persist',
        configInvoicingTermsRetrieve: 'invoiceTerms/retrieve/',
        configInvoicingTermsRetrieveById: 'invoiceTerms/retrieveById/',
        configInvoicingTermsUpdate: 'invoiceTerms/update', 
        // configuration Gst Tax Slab
        configGstTaxPersist: 'gstTaxSlab/persist',
        configGstTaxRetrieve: 'gstTaxSlab/retrieve/',
        configGstTaxRetrieveById: 'gstTaxSlab/retrieveById/',
        configGstTaxUpdate: 'gstTaxSlab/update',
        
        
        // configuration payment
        configPaymentPersist: 'paymentterm/persist',
        configPaymentRetrieve: 'paymentterm/retrieve/',
        configPaymentRetrieveById: 'paymentterm/retrieveById/',
        configPaymentUpdate: 'paymentterm/update',

        //assignment
        assignmentPaymentRetrieve : 'paymentterm/retrieveActive/',
        newAddProjectPersist : 'assignment/persist',
        newAddProjectUpdate : 'assignment/update',
        getAssignmentRetrieve : 'assignment/retrieve',
        getAssignmentRetrieveById : 'assignment/retrieveById/',
        
        configtimeSheetPersist: 'timesheetType/persist',
        configtimeSheetRetrieve: 'timesheetType/retrieve/', 
        configtimeSheetUpdate: 'timesheetType/update',

        //IT savings Config
        addItSavingsPersist : 'itsavingssettings/persist',
        getItSavingsRetrieve: 'itsavingssettings/retrieve',
        itSavingsUpdate : 'itsavingssettings/update', 
        retrieveItSavings : 'itsavingssettings/retrieveByDate/',
        itSavingsPersist : 'itsavings/persist',
        itSavingsRetrieveById : 'itsavings/retrieveByEmpId/',
        itSavingsUploadDocument : 'itsavings/updatepic',


        //companyInformation
        companyInformationDelete: 'company/delete/',
        companyInformationChangePhoto: 'company/updatepic',
        companyInformationChangeicon: 'company/updateicon',
        companyInformationPersist: 'company/persist',
        companyInformationRetrieve: 'company/retrieve/',
        companyInformationRetrieveById: 'company/retrieveById/',
        companyInformationUpdate: 'company/update',

        // country
        countryPersist: 'country/persist',
        countryRetrieve: 'country/retrieve/',
        countryRetrieveById: 'country/retrieveById/',
        countryUpdate: 'country/update',
        countryDelete: 'country/delete/',

        // email
        emailLogin: 'employee/updateEmail/',
        emailLogout: 'employee/resetEmail/',
        emailRetrieveInbox: 'email/retrieveInbox/',
        emailRetrieveSent: 'email/retrieveSent/',
        emailRetrieveMessage: 'email/retrieveMessage',
		emailCompose: 'email/sendEmail/',	
	    emailAttachments: 'email/attachments/',

        // employee
        employeeCompensationUpdateDate : 'empctc/updateDate',
        employeecompensationPersist: 'empctc/persist',
        employeeCompensationRetrieve: 'empctc/retrieve/',
        employeeCompensationRetrieveById:  'empctc/retriveByEmpId/',
        employeeCompensationUpdate: 'empctc/update',
        employeePersist: 'employee/persist',
        employeeSkillsPersist: 'employeeskills/persist',
        employeeSkillsUpdate: 'employeeskills/update',
        employeeUpdateProfilePic :'employee/updatepic',
        employeeRetrieve: 'employee/retrieve/',
        employeeRetrieveById: 'employee/retriveById/',
        employeeRetrieveSkills: 'employeeskills/retrieveByEmpId/',
        employeeRetrieveByMail: 'employee/retriveByEmail/',
        EmployeeIdRetrieve:  'employee/retrieveId',
        employeeUpdate: 'employee/update',
        employeeDelete: 'employee/delete/',
        importEmployee: 'employee/importEmp/',
        userRetrieveActive: 'employee/retrieveActive',
        employeeEducationDetails : 'employeeeducation/persist',
        employeeEducationRetrieve : 'employeeeducation/retrieveByEmpId/',
        employeeEditEducation : 'employeeeducation/update',
        empFamilyInformation : 'employeefamily/persist',
        getEmployeeRetrieveFamilyDetails : 'employeefamily/retrieveByEmpId/',
        updateFamilyInformation : 'employeefamily/update',
        employeeAddressPersist : 'employeeaddress/persist',
        employeeRetrieveAddress:'employeeaddress/retrieveByEmpId/',
        employeeAddressUpdate:'employeeaddress/update',
        employeeBankPersist:'employeebankinformation/persist',
        employeeRetrieveBankDetails:'employeebankinformation/retrieveByEmpId/',
        employeeBankDetailsUpdate:'employeebankinformation/update',
        employeeTerminatePersist:'employeeterminate/persist',
        employeeRetrieveTerminate:'employeeterminate/retrieveByEmpId/',
        employeeCertificatePersist:'employeecertification/persist',
        employeeRetrieveCertificate : 'employeecertification/retrieveByEmpId/',
        employeeCertificateUpdate : 'employeecertification/update',
        employeeAdditionalInfoPersist : 'employeeotherinformation/persist',
        employeeRetrieveAdditionalInfo : 'employeeotherinformation/retrieveByEmpId/',
        employeeAdditionalInfoUpdate : 'employeeotherinformation/update',
        employeeRetrieveUserInformation : 'user/retriveById/',
        empFileUploadDocument : 'employeedocument/persist/',
        employeeRetrieveUploadFiles : 'employeedocument/retrieveById/',
        employeeDriverLicensePersist : 'employeedriverlicense/persist',
        employeeRetrieveDriverLicense : 'employeedriverlicense/retrieveByEmpId/',
        employeeDriverLicenseDetailsUpdate : 'employeedriverlicense/update',
        employeeRetrieveCompensationDetails : 'empcompensation/retriveByEmpId/',
        employeeContactInfoPersist : 'employeecontactinfo/persist',
        employeeRetrieveContactInfo : 'employeecontactinfo/retrieveByEmpId/',
        employeeContactDetailsUpdate : 'employeecontactinfo/update',
        empCompensationShowData : 'empcompensation/retrieveAll',
        getRemainingEmpDetails : 'empctc/empwithoutctc',
        getEmployeeAssignment: 'assignment/retrieveByEmp/',
        retrieveEmpReportingById: 'employee/retrieveEmpReporting/',

        getEmployeeItSavingsRetrieve : 'itsavings/retrieve',
        updateEmployeeItSavingsVerifiedPersist : 'itsavings/update',

        updateTerminateDetailsInformation : 'employeeterminate/update',
        cancelTerminatePersist : 'employeeterminate/delete/',

        // HR policy
        hrPolicyPersist: 'hrpolicy/persist',
        hrPolicyRetrieve: 'hrpolicy/retrieve/',
        hrPolicyUpdate: 'hrpolicy/update',
        hrPolicyRetrieveById: 'hrpolicy/retrieveById/',
        hrPoliciesUploadDocument : 'hrpolicy/updatepic',
        
        // Holiday
        holidayPersist: 'holidays/persist',
        holidayRetrieve: 'holidays/retrieve/',
        holidayUpdate: 'holidays/update',
        
        // HR request
//        hrAcceptRequest: '',
        hrRequestPersist: 'hrrequests/persist',
        hrRequestRetrieve: 'hrrequests/retrieve/',
        hrRequestUpdate: 'hrrequests/update',
        hrRequestRetrieveByEmpId: 'hrrequests/retrieveByEmpId/',
        hrRequestRetrieveById: 'hrrequests/retrieveById/',
        hrRequestUploadDocument : 'hrrequests/updatepic',


        // lead
        leadDelete: 'lead/delete',
        leadPersist: 'lead/persist',
        leadRetrieve: 'lead/retrieve/',
        leadRetrieveById: 'lead/retrieveById/',
        leadRetrieveByType: 'lead/retrieveByType/',
        leadUpdate: 'lead/update',
        leadEmailPersist: 'leademail/persist',
        leadRetrieveLeadByEmp : 'lead/retrieveLeadByEmp/',
        leadEmailRetrieveByLeadId: 'leademail/retrieveByLeadId/',
        leadEmailRetrieveById: 'leademail/retrieveById/',
        leadLogActivityPersist: 'logactivity/persist',
        leadLogActivityRetrieve: 'logactivity/retrieveByLeadId/',
        leadNotesPersist: 'notes/persist',
        leadNotesRetrieve: 'notes/retrieveByLeadId/',
        leadRevokeTransfer: 'lead/revokeLead/',
        leadRetrieveTransfer: 'lead/retrieveTransferLead/',
        leadRetrieveTransferById: 'lead/retrieveTransferLeadById/',
        leadSchedulePersist: 'schedule/persist',
        leadScheduleRetrieve: 'schedule/retrieveByLeadId/',
        leadTaskPersist: 'task/persist',
        leadTaskRetrieve: 'task/retrieveByLeadId/',
        leadTaskRetrievePending: 'task/retrievePendingTask/',
        leadTaskUpdate: 'task/updateStatus/',
        leadTransfer: 'lead/transferLead',
        leadretrieveBetweenDate: 'lead/retrieveBetweenDate/',
        ConvertCustomerUpdate: 'lead/convertAsCustomer',
        customerretrieveBetweenDate: 'customer/retrieveBetweenDate/',
        myLeadsChartDetailsByEmp: 'lead/retrieveBetweenDateByEmp/',
        myCustomersChartDetailsByEmp: 'customer/retrieveBetweenDateByEmp/',
        mySalesTargetChartDetailsByEmp: 'salestarget/retrieveBetweenDate/',
        leadImport: 'lead/importLeads/',
        leadupdatepic: 'lead/updatepic',
        leadRetrieveInbox: 'email/retrieveLeadInbox/',
        
        // leave Management
        leaveStatusRetrieve: 'leaverequests/retrieveLeaveStatus/',
        leaveManagementPersist: 'leaverequests/persist',
        leaveManagementRetrieve: 'leaverequests/retrieve/',
        leaveManagementUpdate: 'leaverequests/update',
        leaveManagementRetrieveByEmpId: 'leaverequests/retrieveByEmpId/',
        leaveManagementRetrieveById: 'leaverequests/retrieveById/',
        leaveManagementRetrieveAllActive: 'leavemanagement/retrieveAllActive/',
        leaveManagementRetrieveAvailableLeavesByEmpId: 'leaverequests/retrieveAvailableLeavesByEmpId/',     

        // Menu   
        menuRetrieve: 'menus/retrieve/',

        //Notification
        notificationSettingsPersist : 'notificationsettings/persist',
        notificationSettingsRetrieve : 'notificationsettings/retrieve',
        notificationSettingsUpdate : 'notificationsettings/update',
        
        // Offers
        offerPersist: 'offers/persist',
        offerRetrieve: 'offers/retrieve/',
        offerRetrieveById: 'offers/retriveById/',
        offerUpdate: 'offers/update',

        // Payroll
        payrollAllowancePersist:'allowancesettings/persist',
        payrollAllowanceRetrieve:'allowancesettings/retrieve/',
        payrollAllowanceUpdate:'allowancesettings/update',
        payrollDeductionPersist:'deductionsettings/persist',
        payrollDeductionRetrieve:'deductionsettings/retrieve/',
        payrollDeductionUpdate:'deductionsettings/update',
        payrollgetLeadByEmpRetrieveById: 'empctc/retriveByEmpIdWithEarnings/',
        payrollgetEmpDateRetrieveById : 'empctc/retriveWithEarningsDateYtd/',
        payrollgetEmpRetrieveById: 'empctc/retriveWithEarnings/',
        payrollGetAllowanceById: 'allowancesettings/retriveAmount/',
        payrollGetDeductionById: 'deductionsettings/retriveAmount/',
        payrollGetTaxById : 'taxsettings/retriveAmount/',
        empcompensation: 'empcompensation/persist',
        runPayrollRetrieve: 'empcompensation/retrieve/',
        payrollProcess: 'empPayroll/persist',
        payrollProcessRetrieveByMonth: 'empPayroll/retriveByMonth/',
        payrollProcessRetrieveByStatus: 'empPayroll/retriveByStatus/',
        payrollProcessUpdate: 'empPayroll/update',
        getpayrollDetailById: 'empPayroll/retriveByEmpId/',
        runPayrollRetrieveWithDate: 'empcompensation/retrieveWithDate/',
        runPayrollRetrieveWithDateForStatus: 'empcompensation/retrieveWithDateForStatus/',
        
        employeeLopPersist : 'employeelop/persist',
        employeeGetLopDetail : 'employeelop/retrieve',
        updateEmpLopDetails : 'employeelop/update',
        cancelLopEmpDetails : 'employeelop/delete',
        empBonusPersist : 'employeebonus/persist',
        employeeGetBonusDetail : 'employeebonus/retrieve',
        updateEmpBonusDetails : 'employeebonus/update',
        cancelBonusEmpDetails : 'employeebonus/delete',
        payrollTaxPersist : 'taxsettings/persist',
        payrollTaxRetrieve : 'taxsettings/retrieve',
        payrollTaxUpdate : 'taxsettings/update',
        empCompensationRetrieveById : 'empcompensation/retrieveById/',
        payrollSummaryProcessUpdate : 'empPayroll/update',
        payrollSummaryDeclineUpdate : 'empPayroll/decline',
        payrollTaxSlabsPersist : 'taxslabssettings/persist',
        payrollTaxSlabsRetrieve : 'taxslabssettings/retrieve',
        payrollTaxSlabsUpdate : 'taxslabssettings/update',

        // Opportunity
        opportunityPersist: 'opportunity/persist',
        opportunityRetrieve: 'opportunity/retrieve/',
        opportunityRetrieveById: 'opportunity/retrieveById/',
        opportunityRetrieveByCustomerId: 'opportunity/retrieveByCustomerId/',
        opportunityUpdate: 'opportunity/update',
        opportunityDelete: 'opportunity/delete/',
        opportunityUploadDocument: 'opportunity/updatepic/',
        opportunityRetrieveSalesStageReportChartDetails: 'opportunity/retrieveAll/',
        opportunityRetrieveProject: 'opportunity/retrieveopp/',
        
        // Project
        projectPersist: ' project/persist',
        projectRetrieve: ' project/retrieve/',
        projectUpdate: ' project/update',
        projectDelete: ' project/delete/',
        projectRetrieveById: ' project/retrieveByProjectId/',
        projectRetrieveByOpportunityId: 'project/retrieveByOpportunityId/',
        projectUploadDocument: ' project/updatepic/',
        projectRetrieveSalesStageReportChartDetails: ' project/retrieveAll/',
        
        // Requirement
        requirementPersist : 'requirements/persist',
        requirementRetrieve: 'requirements/retrieve/',
        requirementRetrieveById: 'requirements/retriveById/',
        requirementRetrieveByProjectId: 'requirements/retrieveByProjectId/',
        requirementUpdate: 'requirements/update',

        // role
        rolePersist: 'roles/persist',
        roleRetrieve: 'roles/retrieve/',
        roleUpdate: 'assignedmenu/persist',
        roleDelete: 'roles/delete/',

        //report
        viewReportPersist : 'empPayroll/retriveForReport/',
        viewTaxReportPersist : 'empPayroll/retriveForTaxReport/',
        viewBonusReportPersist : 'employeebonus/retriveForBonusReport/',
        viewLopReportPersist : 'employeelop/retriveForLopReport/',
        viewYtdReportPersist : 'empPayroll/retriveForYtdReport/',
        
        // sales policy
        salesPolicyPersist: 'salespolicy/persist',
        salesPolicyRetrieve: 'salespolicy/retrieve/',
        salesPolicyUpdate: 'salespolicy/update',
        salesPolicyRetrieveById: 'salespolicy/retrieveById/',
        
        // sales target
        salesPersist: 'salestarget/persist',
        salesRetrieve: 'salestarget/retrieve/',
//        salesRetrieveSalesByDate: 'retrieveBetweenDateByEmp/fromDate/toDate/empId',
        salesUpdate: 'salestarget/update',

        // state
        statePersist: 'state/persist',
        stateRetrieve: 'state/retrieve/',
        stateUpdate: 'state/update',
        stateDelete: 'state/delete/',
        
        // timesheet
        timeSheetCreate: 'timesheet/persist',
        timeSheetRetrieve: 'timesheet/retrieve/',
        timeSheetRetrieveEmpTimesheet: 'timesheet/retrieveEmpTimesheet/',
        timeSheetRetrieveEmpWeekelyTimesheet: 'timesheet/retrieveEmpWeekelyTimesheet/',
        timeSheetUpdate: 'timesheet/update',
        
        // user
        userPersist: 'user/persist',
        userRetrieve: 'user/retrieve/',
        userRetrieveById: 'user/retriveById/',
        userUpdate: 'user/update',
        userDelete: 'user/delete/',
        userLogin: 'user/retrieveForLogin',
        userChangePassword: 'user/resetpassword',
        userResendInvite: 'user/resendInvite/',
        userSendPassword: 'user/sendPassword',

        // warehouse
        warehousePersist: 'warehouse/persist',
        warehouseRetrieve: 'warehouse/retrieve/',
        warehouseRetrieveById: 'warehouse/retriveById/',
        warehouseUpdate: 'warehouse/update',

        //tickets
        viewTicketsRetrieve : '',
        addNewTicketPersist : '',
        viewTicketsDataRetrieve : '',
        addNewViewTicketPersist : '',
        getNewViewTicketsRetrieve : '',

    };

    angular.module('spheresuite').constant('constants', apiRoutesUrl);

})();