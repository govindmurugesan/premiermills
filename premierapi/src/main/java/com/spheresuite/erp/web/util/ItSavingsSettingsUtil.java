package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class ItSavingsSettingsUtil {
	
	private ItSavingsSettingsUtil() {}
	
	public static JSONObject getITSavingsSettingsList(List<ItSavingsSettingsDO> itSavingsSettingsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ItSavingsSettingsDO itSavingsSettings : itSavingsSettingsList) {
				resultJSONArray.put(getITSavingsSettingsObject(itSavingsSettings));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getITSavingsSettingsObject(ItSavingsSettingsDO itSavingsSettings)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(itSavingsSettings.getId()));
		result.put(CommonConstants.NAME, String.valueOf(itSavingsSettings.getName()));
		result.put(CommonConstants.FROMDATE, String.valueOf(itSavingsSettings.getFromYear()));
		result.put(CommonConstants.TODATE, String.valueOf(itSavingsSettings.getToYear()));
		result.put(CommonConstants.SECTION, String.valueOf(itSavingsSettings.getSection()));
		result.put(CommonConstants.MAXLIMIT, String.valueOf(itSavingsSettings.getMaxLimit()));
		result.put(CommonConstants.STATUS, String.valueOf(itSavingsSettings.getStatus()));
		if(itSavingsSettings.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(itSavingsSettings.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(itSavingsSettings.getUpdatedon())));
		return result;
	}
}
