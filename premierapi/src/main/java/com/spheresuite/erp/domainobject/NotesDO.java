package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="notes")
@TableGenerator(name ="notes", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "notes.findAll", query = "SELECT r FROM NotesDO r"),
    @NamedQuery(name = "notes.findById", query = "SELECT r FROM NotesDO r where r.id =:id"),
    @NamedQuery(name = "notes.findByLeadId", query = "SELECT r FROM NotesDO r where r.leadId =:id ORDER BY r.id DESC"),
    @NamedQuery(name = "notes.findByLeadRecentNote", query = "SELECT r FROM NotesDO r where r.leadId =:id ORDER BY r.id DESC "),
})
public class NotesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "notes.findAll";
	
	public static final String FIND_BY_ID = "notes.findById";
	
	public static final String FIND_BY_LEAD_ID = "notes.findByLeadId";
	
	public static final String FIND_BY_LEAD_RECENT_NOTE = "notes.findByLeadRecentNote";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "notes")
	private Long id;
	private Long leadId;
	@Column(columnDefinition="LONGTEXT")
	private String note;
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;

	private Long empId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}