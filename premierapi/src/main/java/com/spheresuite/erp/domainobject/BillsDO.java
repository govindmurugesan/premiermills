package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bills")
@TableGenerator(name ="bills", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "bills.findAll", query = "SELECT r FROM BillsDO r"),
    @NamedQuery(name = "bills.findById", query = "SELECT r FROM BillsDO r where r.id =:id"),
    @NamedQuery(name = "bills.findBillNumber", query = "SELECT r FROM BillsDO r ORDER BY r.id DESC"),
})
public class BillsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "bills.findAll";
	
	public static final String FIND_BY_ID = "bills.findById";
	
	public static final String FIND_BILLNUMBER = "bills.findBillNumber";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "bills")
	private Long id;
	private Long vendorId;
	private String billNumber;
	private String billDate;
	private String dueDate;
	private Double rate;
	private String currencyType;
	private String comments;
	private String updatedby;
	private String status;
	private String paidDate;
	private String paymentMode;
	private String chequeNumbers;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public String getBillNumber() {
		return billNumber;
	}
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	public String getChequeNumbers() {
		return chequeNumbers;
	}
	public void setChequeNumbers(String chequeNumbers) {
		this.chequeNumbers = chequeNumbers;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(String paidDate) {
		this.paidDate = paidDate;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}