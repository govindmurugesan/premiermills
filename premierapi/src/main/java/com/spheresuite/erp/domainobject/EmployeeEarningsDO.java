package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeearnings")
@TableGenerator(name ="employeeearnings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeearnings.findAll", query = "SELECT r FROM EmployeeEarningsDO r"),
    @NamedQuery(name = "employeeearnings.findByEmpCompensationId", query = "SELECT r FROM EmployeeEarningsDO r where r.empcompensationid =:id"),
    @NamedQuery(name = "employeeearnings.findByEmpCompensationIds", query = "SELECT sum(r.monthly) FROM EmployeeEarningsDO r where r.empcompensationid IN :id"),
})
public class EmployeeEarningsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeearnings.findAll";
	
	public static final String FIND_BY_ID = "employeeearnings.findByEmpCompensationId";
	
	public static final String FIND_BY_IDS = "employeeearnings.findByEmpCompensationIds";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeearnings")
	private Long id;
	private Long earningsId;
	private Long empcompensationid;
	private Double monthly;
	private Double ytd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpcompensationid() {
		return empcompensationid;
	}
	public void setEmpcompensationid(Long empcompensationid) {
		this.empcompensationid = empcompensationid;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEarningsId() {
		return earningsId;
	}
	public void setEarningsId(Long earningsId) {
		this.earningsId = earningsId;
	}
}