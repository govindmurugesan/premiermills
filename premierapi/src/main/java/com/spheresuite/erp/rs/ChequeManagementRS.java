package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ChequeManagementDO;
import com.spheresuite.erp.service.ChequeManagementService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.ChequeManagementUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/chequemanagement")
public class ChequeManagementRS {

	String validation = null;
	static Logger logger = Logger.getLogger(BillsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ChequeManagementDO chequeManagementDO = new ChequeManagementDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.NUMBER).toString().isEmpty()){
			 			chequeManagementDO.setChequeNumber(Long.parseLong(inputJSON.get(CommonConstants.NUMBER).toString()));
			 		}
			 		chequeManagementDO.setChequeAmount(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()? inputJSON.get(CommonConstants.AMOUNT).toString() : null);
			 		chequeManagementDO.setChequeDate(!inputJSON.get(CommonConstants.DATE).toString().isEmpty()? inputJSON.get(CommonConstants.DATE).toString() : null);
			 		chequeManagementDO.setBankName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		chequeManagementDO.setPayTo(!inputJSON.get(CommonConstants.PAYTO).toString().isEmpty()? inputJSON.get(CommonConstants.PAYTO).toString() : null);
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			chequeManagementDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			chequeManagementDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		chequeManagementDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()? inputJSON.get(CommonConstants.STATUS).toString() : null);
			 		chequeManagementDO.setUpdatedon(new Date());
			 		new ChequeManagementService().persist(chequeManagementDO);
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ChequeManagementDO> chequeList = new ChequeManagementService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ChequeManagementUtil.getChequeList(chequeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ChequeManagementDO> chequeList = new ChequeManagementService().retrieve();
				respJSON = ChequeManagementUtil.getChequeList(chequeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ChequeManagementDO> chequeList = new ChequeManagementService().retrieveActive();
				respJSON = ChequeManagementUtil.getChequeList(chequeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ChequeManagementDO chequeManagementDO = new ChequeManagementDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<ChequeManagementDO> chequeList = new ChequeManagementService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(chequeList != null && chequeList.size() > 0){
			 			chequeManagementDO = chequeList.get(0);
				 		
			 			if(!inputJSON.get(CommonConstants.NUMBER).toString().isEmpty()){
				 			chequeManagementDO.setChequeNumber(Long.parseLong(inputJSON.get(CommonConstants.NUMBER).toString()));
				 		}
				 		chequeManagementDO.setChequeAmount(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty()? inputJSON.get(CommonConstants.AMOUNT).toString() : null);
				 		chequeManagementDO.setChequeDate(!inputJSON.get(CommonConstants.DATE).toString().isEmpty()? inputJSON.get(CommonConstants.DATE).toString() : null);
				 		chequeManagementDO.setBankName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()? inputJSON.get(CommonConstants.NAME).toString() : null);
				 		chequeManagementDO.setPayTo(!inputJSON.get(CommonConstants.PAYTO).toString().isEmpty()? inputJSON.get(CommonConstants.PAYTO).toString() : null);
				 		
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			chequeManagementDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			chequeManagementDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
				 		}
				 		chequeManagementDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()? inputJSON.get(CommonConstants.STATUS).toString() : null);
				 		chequeManagementDO.setUpdatedon(new Date());
			 			
			 			
				 		new ChequeManagementService().update(chequeManagementDO);
				 		
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	/*@RequestMapping(value = "/importBills/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<BillsDO> billDOlist = new ArrayList<BillsDO>();
				
				for (int i=0; i < fileData.size(); i++){
					BillsDO billsDO = new BillsDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.CUSTOMERID) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CUSTOMERID)).toString().isEmpty()){
				 			billsDO.setVendorId(Long.parseLong(rowJSON.get(colName.get(CommonConstants.CUSTOMERID)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.CURRENCY_TYPE) != null){
						billsDO.setCurrencyType(!rowJSON.get(colName.get(CommonConstants.CURRENCY_TYPE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.CURRENCY_TYPE)).toString() : null);
					}
					if(colName.get(CommonConstants.RATE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.RATE)).toString().isEmpty()){
				 			billsDO.setRate(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.RATE)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.BILLNUMBER) != null){
						billsDO.setBillNumber(!rowJSON.get(colName.get(CommonConstants.BILLNUMBER)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.BILLNUMBER)).toString() : null);
					}
					if(colName.get(CommonConstants.BILLDATE) != null){
						billsDO.setBillDate(!rowJSON.get(colName.get(CommonConstants.BILLDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.BILLDATE)).toString() : null);
					}
					if(colName.get(CommonConstants.DUEDATE) != null){
						billsDO.setDueDate(!rowJSON.get(colName.get(CommonConstants.DUEDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.DUEDATE)).toString() : null);
					}
					if(colName.get(CommonConstants.DESC) != null){
						billsDO.setComments(!rowJSON.get(colName.get(CommonConstants.DESC)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.DESC)).toString() : null);
					}
			 		if(updatedBy != null && ! updatedBy.toString().isEmpty()){
			 			billsDO.setUpdatedby(updatedBy.toString());
			 			billsDO.setEmpId(Long.parseLong(updatedBy.toString()));
			 		}
			 		if(colName.get(CommonConstants.STATUS) != null){
			 			billsDO.setStatus(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.STATUS)).toString() : null);
			 		}
			 		if(colName.get(CommonConstants.STATUS) != null){
			 			if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
				 			String status = rowJSON.get(colName.get(CommonConstants.STATUS)).toString();
				 			if(colName.get(CommonConstants.PAIDDATE) != null){
					 			if(status.equalsIgnoreCase("Paid")){
					 				billsDO.setPaidDate(!rowJSON.get(colName.get(CommonConstants.PAIDDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.PAIDDATE)).toString() : null);
					 			}else{
					 				billsDO.setPaidDate(null);
					 			}
				 			}
			 			}
			 		}
			 		billsDO.setUpdatedon(new Date());
					billDOlist.add(billsDO);
				}
				
				new BillsService().persistList(billDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	*/
	
}
