package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.WareHouseDO;
import com.spheresuite.erp.service.WareHouseService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WareHouseUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/warehouse")
public class WareHouseRS {

	String validation = null;
	static Logger logger = Logger.getLogger(WareHouseRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				WareHouseDO wareHouseDO = new WareHouseDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		wareHouseDO.setWarehouseId(!inputJSON.get(CommonConstants.WAREHOUSEID).toString().isEmpty() ? inputJSON.get(CommonConstants.WAREHOUSEID).toString() : "");
			 		wareHouseDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		wareHouseDO.setPropertyTypeId(!inputJSON.get(CommonConstants.PROPERTYTYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PROPERTYTYPEID).toString()) : null);
			 		wareHouseDO.setStorageTypeId(!inputJSON.get(CommonConstants.STORAGETYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STORAGETYPEID).toString()) : null);
			 		wareHouseDO.setCountry(!inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY).toString()) : null);
			 		wareHouseDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE).toString()) :null);
			 		wareHouseDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() :null);
			 		wareHouseDO.setPinCode(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()) :null);
			 		wareHouseDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() :null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			wareHouseDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			wareHouseDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		wareHouseDO.setUpdatedon(new Date());
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "WareHouse Created");
			 	}
				new WareHouseService().persist(wareHouseDO);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WareHouseDO> wareHouseList = new WareHouseService().retrieve();
				respJSON = WareHouseUtil.getWareHouseList(wareHouseList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<WareHouseDO> wareHouseList = new WareHouseService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = WareHouseUtil.getWareHouseList(wareHouseList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				WareHouseDO wareHouseDO = new WareHouseDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<WareHouseDO> wareHouseList = new WareHouseService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(wareHouseList != null && wareHouseList.size() > 0){
			 				wareHouseDO = wareHouseList.get(0);

			 				wareHouseDO.setWarehouseId(!inputJSON.get(CommonConstants.WAREHOUSEID).toString().isEmpty() ? inputJSON.get(CommonConstants.WAREHOUSEID).toString() : "");
					 		wareHouseDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
					 		wareHouseDO.setPropertyTypeId(!inputJSON.get(CommonConstants.PROPERTYTYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PROPERTYTYPEID).toString()) : null);
					 		wareHouseDO.setStorageTypeId(!inputJSON.get(CommonConstants.STORAGETYPEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STORAGETYPEID).toString()) : null);
					 		wareHouseDO.setCountry(!inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY).toString()) : null);
					 		wareHouseDO.setState(!inputJSON.get(CommonConstants.STATE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE).toString()) :null);
					 		wareHouseDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() :null);
					 		wareHouseDO.setPinCode(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ZIP).toString()) :null);
					 		wareHouseDO.setAddress(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() :null);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			wareHouseDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			wareHouseDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		wareHouseDO.setUpdatedon(new Date());
					 		new WareHouseService().update(wareHouseDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "WareHouse Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
