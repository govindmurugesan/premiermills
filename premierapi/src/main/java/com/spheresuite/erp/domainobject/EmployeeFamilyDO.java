package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeefamily")
@TableGenerator(name ="employeefamily", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeefamily.findAll", query = "SELECT r FROM EmployeeFamilyDO r"),
    @NamedQuery(name = "employeefamily.findById", query = "SELECT r FROM EmployeeFamilyDO r where r.id =:id"),
    @NamedQuery(name = "employeefamily.findByEmpId", query = "SELECT r FROM EmployeeFamilyDO r where r.empId =:empId"),
})
public class EmployeeFamilyDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeefamily.findAll";
	
	public static final String FIND_BY_ID = "employeefamily.findById";
	
	public static final String FIND_BY_EMPID = "employeefamily.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeefamily")
	private Long id;
	private String fathername;
	private String mothername;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFathername() {
		return fathername;
	}
	public void setFathername(String fathername) {
		this.fathername = fathername;
	}
	public String getMothername() {
		return mothername;
	}
	public void setMothername(String mothername) {
		this.mothername = mothername;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}