package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeecompensation")
@TableGenerator(name ="employeecompensation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeecompensation.findAll", query = "SELECT r FROM EmployeeCompensationDO r"),
    @NamedQuery(name = "employeecompensation.findById", query = "SELECT r FROM EmployeeCompensationDO r where r.id =:id"),
    @NamedQuery(name = "employeecompensation.findByStatus", query = "SELECT r FROM EmployeeCompensationDO r where r.status =:status and r.empId =:empId"),
    @NamedQuery(name = "employeecompensation.findByEmpId", query = "SELECT r FROM EmployeeCompensationDO r where r.empId =:empId"),
})
public class EmployeeCompensationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeecompensation.findAll";
	
	public static final String FIND_BY_ID = "employeecompensation.findById";
	
	public static final String FIND_BY_STATUS = "employeecompensation.findByStatus";
	
	public static final String FIND_BY_EMP_ID = "employeecompensation.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeecompensation")
	private Long id;
	private Long empId;
	@Temporal(TemporalType.DATE)
	private Date effectivefrom;
	@Temporal(TemporalType.DATE)
	private Date effectiveto;
	private Double netmonth;
	private Double netyear;
	private Double earningmonth;
	private Double earningyear;
	private Double deductionmonth;
	private Double deductionyear;
	private Double taxmonth;
	private Double taxyear;
	private char status;
	private Double grossPay;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdby;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Date getEffectivefrom() {
		return effectivefrom;
	}
	public void setEffectivefrom(Date effectivefrom) {
		this.effectivefrom = effectivefrom;
	}
	public Date getEffectiveto() {
		return effectiveto;
	}
	public void setEffectiveto(Date effectiveto) {
		this.effectiveto = effectiveto;
	}
	public Double getNetmonth() {
		return netmonth;
	}
	public void setNetmonth(Double netmonth) {
		this.netmonth = netmonth;
	}
	public Double getNetyear() {
		return netyear;
	}
	public void setNetyear(Double netyear) {
		this.netyear = netyear;
	}
	public Double getEarningmonth() {
		return earningmonth;
	}
	public void setEarningmonth(Double earningmonth) {
		this.earningmonth = earningmonth;
	}
	public Double getEarningyear() {
		return earningyear;
	}
	public void setEarningyear(Double earningyear) {
		this.earningyear = earningyear;
	}
	public Double getDeductionmonth() {
		return deductionmonth;
	}
	public void setDeductionmonth(Double deductionmonth) {
		this.deductionmonth = deductionmonth;
	}
	public Double getDeductionyear() {
		return deductionyear;
	}
	public void setDeductionyear(Double deductionyear) {
		this.deductionyear = deductionyear;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public Double getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(Double grossPay) {
		this.grossPay = grossPay;
	}
	public Double getTaxmonth() {
		return taxmonth;
	}
	public void setTaxmonth(Double taxmonth) {
		this.taxmonth = taxmonth;
	}
	public Double getTaxyear() {
		return taxyear;
	}
	public void setTaxyear(Double taxyear) {
		this.taxyear = taxyear;
	}
}