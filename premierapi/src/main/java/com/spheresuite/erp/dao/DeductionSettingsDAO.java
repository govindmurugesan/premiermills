package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.DeductionSettingDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class DeductionSettingsDAO {
	static Logger logger = Logger.getLogger(DeductionSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public DeductionSettingDO persist(DeductionSettingDO deductionSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && deductionSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(deductionSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return deductionSettingDO;
	}
	
	public DeductionSettingDO update(DeductionSettingDO deductionSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && deductionSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(deductionSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return deductionSettingDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSettingDO> retrieve() throws AppException {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeductionSettingDO.FIND_ALL);
				/*q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);*/
				deductionSettingList = (List<DeductionSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return deductionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSettingDO> retrieveById(Long Id) throws AppException {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeductionSettingDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				deductionSettingList = (List<DeductionSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return deductionSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeductionSettingDO> retrieveActive(String name) throws AppException {
		List<DeductionSettingDO> deductionSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeductionSettingDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.NAME, name);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				deductionSettingList = (List<DeductionSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return deductionSettingList;
	}
}
