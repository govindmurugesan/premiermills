package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class TaxSettingsDAO {
	static Logger logger = Logger.getLogger(TaxSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public TaxSettingsDO persist(TaxSettingsDO taxSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && taxSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(taxSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSettingDO;
	}
	
	public TaxSettingsDO update(TaxSettingsDO taxSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && taxSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(taxSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSettingDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSettingsDO> retrieve() throws AppException {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSettingsDO.FIND_ALL);
				/*q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);*/
				taxSettingList = (List<TaxSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSettingsDO> retrieveById(Long Id) throws AppException {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSettingsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				taxSettingList = (List<TaxSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSettingsDO> retrieveActive(String name) throws AppException {
		List<TaxSettingsDO> taxSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSettingsDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.NAME, name);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				taxSettingList = (List<TaxSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSettingList;
	}
}
