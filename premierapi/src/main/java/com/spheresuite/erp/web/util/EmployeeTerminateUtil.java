package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.TerminateReasonTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeTerminateUtil {
	
	private EmployeeTerminateUtil() {}
	
	public static JSONObject getEmployeeTerminateList(List<EmployeeTerminateDO> employeeTerminateList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeTerminateDO employeeTerminateDO : employeeTerminateList) {
				resultJSONArray.put(getEmployeeTerminateDetailObject(employeeTerminateDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeTerminateDetailObject(EmployeeTerminateDO employeeTerminateDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeTerminateDO.getId()));
		result.put(CommonConstants.COMMENT, String.valueOf(employeeTerminateDO.getComments()));
		if(employeeTerminateDO.getTerminateTypeId() != null ){
			result.put(CommonConstants.TERMINATETYPEID, String.valueOf(employeeTerminateDO.getTerminateTypeId()));
			List<TerminateReasonTypeDO> listTerminateTypeList = new TerminateReasonTypeService().retrieveById(employeeTerminateDO.getTerminateTypeId());
			if(listTerminateTypeList != null && listTerminateTypeList.size() > 0){
				result.put(CommonConstants.TERMINATEREASON, String.valueOf(listTerminateTypeList.get(0).getName()));
			}else{
				result.put(CommonConstants.TERMINATEREASON, "");
			}
		}else{
			result.put(CommonConstants.TERMINATETYPEID, "");
		}
		
		result.put(CommonConstants.TERMINATEDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeTerminateDO.getTerminateDate())));
		result.put(CommonConstants.RESIGNEDDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeTerminateDO.getResignDate())));
		result.put(CommonConstants.EMPID, String.valueOf(employeeTerminateDO.getEmpId()));
		if(employeeTerminateDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employeeTerminateDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeTerminateDO.getUpdatedon())));
		return result;
	}
}
