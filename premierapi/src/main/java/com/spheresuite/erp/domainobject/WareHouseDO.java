package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="warehouse")
@TableGenerator(name ="warehouse", initialValue =100101, allocationSize =1)
@NamedQueries({
	 @NamedQuery(name = "warehouse.findAll", query = "SELECT r FROM WareHouseDO r"),
	 @NamedQuery(name = "warehouse.findById", query = "SELECT r FROM WareHouseDO r where r.id =:id"),
})
public class WareHouseDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "warehouse.findAll";
	public static final String FIND_BY_ID = "warehouse.findById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "warehouse")
	private Long id;
	private String warehouseId;
	private String name;
	private Long propertyTypeId;
	private Long storageTypeId;
	private Long country;
	private Long state;
	private String city;
	private Long pinCode;
	private String address;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(String warehouseId) {
		this.warehouseId = warehouseId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getPropertyTypeId() {
		return propertyTypeId;
	}
	public void setPropertyTypeId(Long propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	public Long getStorageTypeId() {
		return storageTypeId;
	}
	public void setStorageTypeId(Long storageTypeId) {
		this.storageTypeId = storageTypeId;
	}
	public Long getCountry() {
		return country;
	}
	public void setCountry(Long country) {
		this.country = country;
	}
	public Long getState() {
		return state;
	}
	public void setState(Long state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Long getPinCode() {
		return pinCode;
	}
	public void setPinCode(Long pinCode) {
		this.pinCode = pinCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}