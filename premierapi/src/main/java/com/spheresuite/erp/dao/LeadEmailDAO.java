package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class LeadEmailDAO {
	static Logger logger = Logger.getLogger(LeadEmailDAO.class.getName());
	private EntityManager em = null;
	
	public LeadEmailDO persist(LeadEmailDO leadEmailDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leadEmailDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(leadEmailDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailDO;
	}
	
	public boolean persistList(List<LeadEmailDO> leadEmailDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leadEmailDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (LeadEmailDO leadEmailDO2 : leadEmailDO) {
					em.persist(leadEmailDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeadEmailDO> retrieveByLead(Long leadId) throws AppException {
		List<LeadEmailDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadEmailDO.FIND_BY_LEAD_ID);
				q.setParameter(CommonConstants.LEAD_ID,leadId);
				leadEmailList = (List<LeadEmailDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadEmailDO> retrieveById(Long Id) throws AppException {
		List<LeadEmailDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadEmailDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID,Id);
				leadEmailList = (List<LeadEmailDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}
	

	public Boolean delete(Long Id) throws AppException {
		boolean flag = false;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q = em.createNamedQuery(LeadEmailDO.DELETE_BY_ID);
				q.setParameter(CommonConstants.ID,Id);
				q.executeUpdate();
				TransactionManager.commitTransaction();
				flag = true;
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return flag;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeadEmailDO> retrieveAllMail() throws AppException {
		List<LeadEmailDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadEmailDO.FIND_ALL);
				leadEmailList = (List<LeadEmailDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}
	
}
