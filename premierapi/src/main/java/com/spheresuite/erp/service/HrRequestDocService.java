package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HrRequestDocDAO;
import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.exception.AppException;

public class HrRequestDocService {
	static Logger logger = Logger.getLogger(HrRequestDocService.class.getName());
	
	public HrRequestDocDO persist(HrRequestDocDO hrRequestDoc) throws AppException {
		return new HrRequestDocDAO().persist(hrRequestDoc);
	}

	public List<HrRequestDocDO> retrieveByOppId(Long id) throws AppException {
		return new HrRequestDocDAO().retrieveByOppId(id);
	}
	
	public boolean delete(Long id) throws AppException {
		return new HrRequestDocDAO().delete(id);
	}
	
	public List<HrRequestDocDO> retrieve() throws AppException {
		return new HrRequestDocDAO().retrieve();
	}
	
	public HrRequestDocDO update(HrRequestDocDO hrRequestDoc) throws AppException {
		return new HrRequestDocDAO().update(hrRequestDoc);
	}
}
