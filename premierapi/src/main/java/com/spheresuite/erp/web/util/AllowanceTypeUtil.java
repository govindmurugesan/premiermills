package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AllowanceTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class AllowanceTypeUtil {
	
	private AllowanceTypeUtil() {}
	
	public static JSONObject getAllowanceTypeList(List<AllowanceTypeDO> allowanceTypeList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AllowanceTypeDO allowanceType : allowanceTypeList) {
				resultJSONArray.put(getAllowanceDetailObject(allowanceType));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getAllowanceDetailObject(AllowanceTypeDO allowanceType)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(allowanceType.getId()));
		result.put(CommonConstants.NAME, String.valueOf(allowanceType.getName()));
		if(allowanceType.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(allowanceType.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME, "");
		}
		
		result.put(CommonConstants.STATUS, String.valueOf(allowanceType.getStatus()));
		if(allowanceType.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(allowanceType.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(allowanceType.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(allowanceType.getUpdatedon())));
		return result;
	}
}
