package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class AssignedMenuDAO {
	
	static Logger logger = Logger.getLogger(AssignedMenuDAO.class.getName());
	
	private EntityManager em = null;

		
	public List<AssignedMenuDO> persist(List<AssignedMenuDO> assignedMenuList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && assignedMenuList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q =  em.createQuery("delete from AssignedMenuDO e where e.roleId=:roleId");
				q.setParameter(CommonConstants.ROLE_ID, assignedMenuList.get(0).getRoleId());
				q.executeUpdate();
				for (AssignedMenuDO assignedMenuDO : assignedMenuList) {
					em.persist(assignedMenuDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return assignedMenuList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveByRoleId(Long roleId) throws AppException {
		List<AssignedMenuDO> assignedMenuList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(AssignedMenuDO.FIND_BY_ROLE_ID);
				q.setParameter(CommonConstants.ROLE_ID, roleId);
				assignedMenuList = (List<AssignedMenuDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return assignedMenuList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AssignedMenuDO> retrieveById(Long id) throws AppException {
		List<AssignedMenuDO> assignedMenuList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(AssignedMenuDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				assignedMenuList = (List<AssignedMenuDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return assignedMenuList;
	}
	
	public List<AssignedMenuDO> update(List<AssignedMenuDO> assignedMenuList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			TransactionManager.beginTransaction(em);
			em.createQuery("delete from AssignedMenuDO e where e.roleId=:"+assignedMenuList.get(0).getRoleId());
			TransactionManager.commitTransaction();
			if (em != null && assignedMenuList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (AssignedMenuDO assignedMenuDO : assignedMenuList) {
					em.persist(assignedMenuDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return assignedMenuList;
	}
}	
