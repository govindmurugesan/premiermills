package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="opportunities")
@TableGenerator(name ="opportunities", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "opportunities.findAll", query = "SELECT r FROM OpportunitiesDO r where r.empId IN :id"),
    @NamedQuery(name = "opportunities.findAllData", query = "SELECT r FROM OpportunitiesDO r"),
    @NamedQuery(name = "opportunities.findById", query = "SELECT r FROM OpportunitiesDO r where r.id =:id"),
    @NamedQuery(name = "opportunities.findBySalesStageId", query = "SELECT sum(r.amount) FROM OpportunitiesDO r where r.salesStage =:id"),
    @NamedQuery(name = "opportunities.findByCustomer", query = "SELECT r FROM OpportunitiesDO r where r.customerId =:id"),
    @NamedQuery(name = "opportunities.findByContact", query = "SELECT r FROM OpportunitiesDO r where r.contactId =:id"),
})
public class OpportunitiesDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "opportunities.findAll";
	
	public static final String FIND_BY_ID = "opportunities.findById";
	
	public static final String FIND_BY_SALES_STAGE_ID = "opportunities.findBySalesStageId";
	
	public static final String FIND_BY_CUSTOMER = "opportunities.findByCustomer";
	
	public static final String FIND_ALL_DATA = "opportunities.findAllData";
	
	public static final String FIND_BY_CONTACT = "opportunities.findByContact";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "opportunities")
	private Long id;
	private Long customerId;
	private String projectname;
	private String displayname;
	private String projectcode;
	private String startdate;
	private String enddate;
	private String projectduration;
	private String comment;
	private String updatedby;
	private Long projectType;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private Long contactId;
	private Long probability;
	private Long amount;
	private Long salesStage;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getProjectcode() {
		return projectcode;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getProjectduration() {
		return projectduration;
	}
	public void setProjectduration(String projectduration) {
		this.projectduration = projectduration;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getProjectType() {
		return projectType;
	}
	public void setProjectType(Long projectType) {
		this.projectType = projectType;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getContactId() {
		return contactId;
	}
	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}
	
	public Long getProbability() {
		return probability;
	}
	public void setProbability(Long probability) {
		this.probability = probability;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public Long getSalesStage() {
		return salesStage;
	}
	public void setSalesStage(Long salesStage) {
		this.salesStage = salesStage;
	}
}