package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ItSavingsSettingsDAO;
import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.exception.AppException;

public class ItSavingsSettingsService {
	static Logger logger = Logger.getLogger(ItSavingsSettingsService.class.getName());
	
	public ItSavingsSettingsDO persist(ItSavingsSettingsDO itSavingsSettingsDO) throws AppException {
		return new ItSavingsSettingsDAO().persist(itSavingsSettingsDO);
	}

	public List<ItSavingsSettingsDO> retrieveActive() throws AppException {
		return new ItSavingsSettingsDAO().retrieveActive();
	}
	
	public List<ItSavingsSettingsDO> retrieveById(Long id) throws AppException {
		return new ItSavingsSettingsDAO().retrieveById(id);
	}
	
	public List<ItSavingsSettingsDO> retrieve() throws AppException {
		return new ItSavingsSettingsDAO().retrieve();
	}
	
	public List<ItSavingsSettingsDO> retrieveByDate(String fromDate, String toDate) throws AppException {
		return new ItSavingsSettingsDAO().retrieveByDate(fromDate, toDate);
	}
	
	
	
	public ItSavingsSettingsDO update(ItSavingsSettingsDO itSavingsSettingsDO) throws AppException {
		return new ItSavingsSettingsDAO().update(itSavingsSettingsDO);
	}
}
