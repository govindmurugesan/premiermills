package com.spheresuite.erp.domainobject;

public class PayrollSummaryDO {

	private String payrolltype;
	private String rundate;
	private String payperiod;
	private String batchname;
	private String empcount;
	private String gross;
	private String earnings;
	private String deduction;
	private String netpayment;
	private char status;
	public String getPayrolltype() {
		return payrolltype;
	}
	public void setPayrolltype(String payrolltype) {
		this.payrolltype = payrolltype;
	}
	public String getRundate() {
		return rundate;
	}
	public void setRundate(String rundate) {
		this.rundate = rundate;
	}
	public String getPayperiod() {
		return payperiod;
	}
	public void setPayperiod(String payperiod) {
		this.payperiod = payperiod;
	}
	public String getBatchname() {
		return batchname;
	}
	public void setBatchname(String batchname) {
		this.batchname = batchname;
	}
	public String getEmpcount() {
		return empcount;
	}
	public void setEmpcount(String empcount) {
		this.empcount = empcount;
	}
	public String getGross() {
		return gross;
	}
	public void setGross(String gross) {
		this.gross = gross;
	}
	public String getEarnings() {
		return earnings;
	}
	public void setEarnings(String earnings) {
		this.earnings = earnings;
	}
	public String getDeduction() {
		return deduction;
	}
	public void setDeduction(String deduction) {
		this.deduction = deduction;
	}
	public String getNetpayment() {
		return netpayment;
	}
	public void setNetpayment(String netpayment) {
		this.netpayment = netpayment;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
}
