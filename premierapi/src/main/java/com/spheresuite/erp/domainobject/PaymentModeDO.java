package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="paymentmode")
@TableGenerator(name ="paymentmode", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "paymentmode.findAll", query = "SELECT r FROM PaymentModeDO r"),
    @NamedQuery(name = "paymentmode.findById", query = "SELECT r FROM PaymentModeDO r where r.id =:id"),
    @NamedQuery(name = "paymentmode.findByStatus", query = "SELECT r FROM PaymentModeDO r where r.status =:status"),
})
public class PaymentModeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "paymentmode.findAll";
	public static final String FIND_BY_ID = "paymentmode.findById";
	public static final String FIND_BY_STATUS = "paymentmode.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "leavetype")
	
	private Long id;
	private String name;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	
	
}