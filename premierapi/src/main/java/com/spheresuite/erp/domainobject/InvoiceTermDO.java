package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="invoiceterm")
@TableGenerator(name ="invoiceterm", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "invoiceterm.findByProposalId", query = "SELECT r FROM InvoiceTermDO r where r.proposalId =:proposalId"),
})
public class InvoiceTermDO implements Serializable {
	private static final long serialVersionUID = 1L;

	
	public static final String FIND_BY_PROPOSAL_ID = "invoiceterm.findByProposalId";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "invoiceterm")
	private Long id;
	private Long proposalId;
	private String descripetion;
	private String projectPercentage;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getProposalId() {
		return proposalId;
	}
	public void setProposalId(Long proposalId) {
		this.proposalId = proposalId;
	}
	public String getDescripetion() {
		return descripetion;
	}
	public void setDescripetion(String descripetion) {
		this.descripetion = descripetion;
	}
	public String getProjectPercentage() {
		return projectPercentage;
	}
	public void setProjectPercentage(String projectPercentage) {
		this.projectPercentage = projectPercentage;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}