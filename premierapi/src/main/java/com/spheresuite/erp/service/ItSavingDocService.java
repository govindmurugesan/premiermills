package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ItSavingDocDAO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.exception.AppException;

public class ItSavingDocService {
	static Logger logger = Logger.getLogger(ItSavingDocService.class.getName());
	
	public ItSavingsDocDO persist(ItSavingsDocDO itsavingdoc) throws AppException {
		return new ItSavingDocDAO().persist(itsavingdoc);
	}

	public List<ItSavingsDocDO> retrieveByOppId(Long id) throws AppException {
		return new ItSavingDocDAO().retrieveByOppId(id);
	}
	
	public boolean delete(Long id) throws AppException {
		return new ItSavingDocDAO().delete(id);
	}
	
	public List<ItSavingsDocDO> retrieve() throws AppException {
		return new ItSavingDocDAO().retrieve();
	}
	
	public ItSavingsDocDO update(ItSavingsDocDO itsavingdoc) throws AppException {
		return new ItSavingDocDAO().update(itsavingdoc);
	}
}
