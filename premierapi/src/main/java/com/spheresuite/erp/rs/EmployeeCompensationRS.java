package com.spheresuite.erp.rs;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.service.EmployeeCompensationService;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeCompensationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empcompensation")
public class EmployeeCompensationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeCompensationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompensationDO empCompensationDO = new EmployeeCompensationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
		 			if(empCompensationList != null && empCompensationList.size() > 0){
		 				EmployeeCompensationDO empCompensationUpdateDO = new EmployeeCompensationDO();
		 				empCompensationUpdateDO = empCompensationList.get(0);
		 				empCompensationUpdateDO.setStatus('p');
		 					if(!inputJSON.get(CommonConstants.EFFECTIVEFROM).toString().isEmpty()){
		 						Calendar cal = Calendar.getInstance();
		 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.EFFECTIVEFROM).toString()));
		 						cal.add(Calendar.DATE, -1);
		 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensationUpdateDO.getEffectivefrom());
		 						Calendar cal1 = Calendar.getInstance();
		 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
		 						Date dateBefore1Days = cal.getTime();
		 						if(cal1.before(cal)){
		 							empCompensationUpdateDO.setEffectiveto(dateBefore1Days);
		 						}else{
		 							empCompensationUpdateDO.setEffectiveto(empCompensationUpdateDO.getEffectivefrom());
		 						}
		 					}
			 				new EmployeeCompensationService().update(empCompensationUpdateDO);
		 				}
		 				
	 				empCompensationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		empCompensationDO.setEffectivefrom(!inputJSON.get(CommonConstants.EFFECTIVEFROM).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.EFFECTIVEFROM).toString()) : null);
			 		empCompensationDO.setEffectiveto(!inputJSON.get(CommonConstants.EFFECTIVETO).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.EFFECTIVETO).toString()) : null);
			 		empCompensationDO.setNetmonth(!inputJSON.get(CommonConstants.NETMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.NETMONTHLY).toString()) : null);
			 		empCompensationDO.setNetyear(!inputJSON.get(CommonConstants.NETYEAR).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.NETYEAR).toString()) : null);
			 		empCompensationDO.setEarningmonth(!inputJSON.get(CommonConstants.EARNINGMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.EARNINGMONTHLY).toString()) : null);
			 		empCompensationDO.setEarningyear(!inputJSON.get(CommonConstants.EARNINGYEAR).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.EARNINGYEAR).toString()) : null);
			 		empCompensationDO.setDeductionmonth(!inputJSON.get(CommonConstants.DEDUCTIONMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.DEDUCTIONMONTHLY).toString()) : null);
			 		empCompensationDO.setDeductionyear(!inputJSON.get(CommonConstants.DEDUCTIONYEAR).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.DEDUCTIONYEAR).toString()) : null);
			 		empCompensationDO.setTaxmonth(!inputJSON.get(CommonConstants.TAXMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.TAXMONTHLY).toString()) : null);
			 		empCompensationDO.setTaxyear(!inputJSON.get(CommonConstants.TAXYEAR).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.TAXYEAR).toString()) : null);
			 		empCompensationDO.setGrossPay(!inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.BASICGROSS_PAY).toString()) : null);
			 		empCompensationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		empCompensationDO.setStatus('a');
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Componsation Created");
			 	}
				new EmployeeCompensationService().persist(empCompensationDO);
				if(!inputJSON.get(CommonConstants.EARNINGLIST).toString().isEmpty()){
		 			List<EmployeeEarningsDO> earningDetailsList = new ArrayList<EmployeeEarningsDO>(); 
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.EARNINGLIST));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			EmployeeEarningsDO earningDetails = new EmployeeEarningsDO(); 
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			earningDetails.setEmpcompensationid(empCompensationDO.getId());
			 			earningDetails.setEarningsId(Long.parseLong(inputJSON1.get(CommonConstants.EARNINGID).toString()));
			 			earningDetails.setMonthly(Double.parseDouble(inputJSON1.get(CommonConstants.MONTHLY).toString()));
			 			earningDetails.setYtd(Double.parseDouble(inputJSON1.get(CommonConstants.YTD).toString()));
			 			earningDetails.setUpdatedon(new Date());
			 			earningDetailsList.add(earningDetails);
			 			
			 		}
			 		new EmployeeEarningsService().persist(earningDetailsList);
		 		}
				
				if(!inputJSON.get(CommonConstants.DEDUCTIONLIST).toString().isEmpty()){
		 			List<EmployeeDeductionDO> deductionDetailsList = new ArrayList<EmployeeDeductionDO>(); 
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.DEDUCTIONLIST));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			EmployeeDeductionDO deductionDetails = new EmployeeDeductionDO(); 
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			deductionDetails.setEmpcompensationid(empCompensationDO.getId());
			 			deductionDetails.setEmpdeductionid(Long.parseLong(inputJSON1.get(CommonConstants.DEDUCTIONID).toString()));
			 			deductionDetails.setMonthly(Double.parseDouble(inputJSON1.get(CommonConstants.MONTHLY).toString()));
			 			deductionDetails.setYtd(Double.parseDouble(inputJSON1.get(CommonConstants.YTD).toString()));
			 			deductionDetails.setUpdatedon(new Date());
			 			deductionDetailsList.add(deductionDetails);
			 			
			 		}
			 		new EmployeeDeductionService().persist(deductionDetailsList);
		 		}
				
				if(!inputJSON.get(CommonConstants.TAXLIST).toString().isEmpty()){
		 			List<EmployeeTaxDO> taxDetailsList = new ArrayList<EmployeeTaxDO>(); 
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.TAXLIST));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			EmployeeTaxDO taxDetails = new EmployeeTaxDO(); 
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			taxDetails.setEmpcompensationid(empCompensationDO.getId());
			 			taxDetails.setEmptaxid(Long.parseLong(inputJSON1.get(CommonConstants.TAXID).toString()));
			 			taxDetails.setMonthly(Double.parseDouble(inputJSON1.get(CommonConstants.MONTHLY).toString()));
			 			 taxDetails.setYtd(Double.parseDouble(inputJSON1.get(CommonConstants.YTD).toString()));
			 			taxDetails.setUpdatedon(new Date());
			 			taxDetailsList.add(taxDetails);
			 			
			 		}
			 		new EmployeeTaxService().persist(taxDetailsList);		 		}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeeCompensationDO> compensationList = new EmployeeCompensationService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeeCompensationUtil.getempCompensationList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieve();
					respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeCompensationUtil.getempCompensationListWithEarningList(empCompensationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAll", method = RequestMethod.GET)
	public @ResponseBody String retrieveAll(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieve();
				respJSON = EmployeeCompensationUtil.getempCompensationList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveWithDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieve();
					respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	/*@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeCompensationDO empCompensationDO = new EmployeeCompensationDO();
				int i =0;
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieveActive(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 			if(empCompensationList != null && empCompensationList.size() > 0){
			 				EmployeeCompensationDO empCompensation = new EmployeeCompensationDO();
			 				empCompensation = empCompensationList.get(0);
			 				empCompensation.setStatus('i');
			 				if(empCompensation.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(empCompensation.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							empCompensation.setEnddate(dateBefore1Days);
			 						}else{
			 							empCompensation.setEnddate(empCompensation.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				if(i == 0){
			 					new EmployeeCompensationService().update(empCompensation);
			 				}
			 			}
			 		}
			 		
			 		empCompensationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		empCompensationDO.setEmpctc(!inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPCTC).toString()) : null);
			 		empCompensationDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		empCompensationDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		empCompensationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			empCompensationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			empCompensationDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		empCompensationDO.setStatus('a');
			 	}
			 	if(i == 0){
			 		new EmployeeCompensationService().update(empCompensationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Compensation Updated");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}*/
	
	@RequestMapping(value = "/retrieveWithDateForStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveWithDateForStatus(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					List<EmployeeCompensationDO> empCompensationList = new EmployeeCompensationService().retrieve();
					respJSON = EmployeeCompensationUtil.getempCompensationListWithBatch(empCompensationList, Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
