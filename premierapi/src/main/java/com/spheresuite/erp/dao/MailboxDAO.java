package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.MailboxDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class MailboxDAO {
	static Logger logger = Logger.getLogger(MailboxDAO.class.getName());
	private EntityManager em = null;
	
	public MailboxDO persist(MailboxDO mailboxDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && mailboxDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(mailboxDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return mailboxDO;
	}
	
	public boolean persistList(List<MailboxDO> mailboxDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && mailboxDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (MailboxDO mailboxDO2 : mailboxDO) {
					em.persist(mailboxDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	
	/*@SuppressWarnings("unchecked")
	public List<MailboxDO> retrieveByLead(Long leadId) throws AppException {
		List<MailboxDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				//Query q = em.createNamedQuery(MailboxDO.FIND_BY_LEAD_ID);
				q.setParameter(CommonConstants.LEAD_ID,leadId);
				leadEmailList = (List<MailboxDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<MailboxDO> retrieveById(Long Id) throws AppException {
		List<MailboxDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(MailboxDO.FIND_BY_ID);
				q.setParameter("empId",Id);
				leadEmailList = (List<MailboxDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MailboxDO> retrieveAllMail() throws AppException {
		List<MailboxDO> leadEmailList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(MailboxDO.FIND_ALL);
				leadEmailList = (List<MailboxDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadEmailList;
	}
	
}
