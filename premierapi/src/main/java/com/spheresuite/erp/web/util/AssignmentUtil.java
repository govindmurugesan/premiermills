package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AssignmentDO;
import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.CustomerService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.PaymentTermService;
import com.spheresuite.erp.service.ProjectService;
import com.spheresuite.erp.util.CommonConstants;

public class AssignmentUtil {
	
	private AssignmentUtil() {}
	
	public static JSONObject getAllowanceList(List<AssignmentDO> assignmentList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AssignmentDO assignment : assignmentList) {
				resultJSONArray.put(getAllowanceDetailObject(assignment));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAllowanceDetailObject(AssignmentDO assignment)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(assignment.getId()));
		
		if(assignment.getOpportunityId() != null){
			List<OpportunitiesDO> opportunityList = new OpportunitiesService().retrieveById(assignment.getOpportunityId());
			result.put(CommonConstants.OPPORTUNITY_ID, String.valueOf(assignment.getOpportunityId()));
			if(opportunityList != null && opportunityList.size() > 0){
				result.put(CommonConstants.OPPORTUNITY_NAME, String.valueOf(opportunityList.get(0).getProjectname()));
			}else{
				result.put(CommonConstants.OPPORTUNITY_NAME, "");
			}
		}else{
			result.put(CommonConstants.OPPORTUNITY_ID, "");
			result.put(CommonConstants.OPPORTUNITY_NAME, "");
		}
		
		if(assignment.getProjectId() != null){
			List<ProjectDO> projectList = new ProjectService().retrieveById(assignment.getProjectId());
			result.put(CommonConstants.PROJECTID, String.valueOf(assignment.getProjectId()));
			if(projectList != null && projectList.size() > 0){
				result.put(CommonConstants.PROJECTNAME, String.valueOf(projectList.get(0).getProjectname()));
			}else{
				result.put(CommonConstants.PROJECTNAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTID, "");
			result.put(CommonConstants.PROJECTNAME, "");
		}
		
		if(assignment.getCustomerId() != null){
			List<CustomerDO> customerList = new CustomerService().retrieveById(assignment.getCustomerId());
			result.put(CommonConstants.CUSTOMERID, String.valueOf(assignment.getCustomerId()));
			if(customerList != null && customerList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, String.valueOf(customerList.get(0).getName()));
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");
			}
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		
		if(assignment.getPaymentTerms() != null){
			List<PaymentTermDO> paymentTermList = new PaymentTermService().retrieveById(assignment.getPaymentTerms());
			result.put(CommonConstants.PAYMENTTERM, String.valueOf(assignment.getPaymentTerms()));
			if(paymentTermList != null && paymentTermList.size() > 0){
				result.put(CommonConstants.PAYMENTTERM_NAME, String.valueOf(paymentTermList.get(0).getName()));
			}else{
				result.put(CommonConstants.PAYMENTTERM_NAME, "");
			}
		}else{
			result.put(CommonConstants.PAYMENTTERM, "");
			result.put(CommonConstants.PAYMENTTERM_NAME, "");
		}
		
		if(assignment.getProjectType() != null){
			result.put(CommonConstants.PROJECTTYPE,String.valueOf(assignment.getProjectType()));
		}else{
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(assignment.getOrderNumber() != null){
			result.put(CommonConstants.ORDERNUMBER,String.valueOf(assignment.getOrderNumber()));
		}else{
			result.put(CommonConstants.ORDERNUMBER, "");
		}
		
		if(assignment.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(assignment.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(assignment.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(assignment.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(assignment.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(assignment.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		result.put(CommonConstants.STATUS, String.valueOf(assignment.getStatus()));
		
		if(assignment.getComments() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(assignment.getComments()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(assignment.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(assignment.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(assignment.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(assignment.getEmpId());
			result.put(CommonConstants.EMPID,String.valueOf(assignment.getEmpId()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
				result.put(CommonConstants.EMPID, "");
			}
			
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		if(assignment.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(assignment.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		
		if(assignment.getAmount() != null){
			result.put(CommonConstants.AMOUNT, String.valueOf(assignment.getAmount()));
		}else{
			result.put(CommonConstants.AMOUNT, "");
		}
		
		return result;
	}
}
