package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeebonus")
@TableGenerator(name ="employeebonus", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeebonus.findAll", query = "SELECT r FROM EmployeeBonusDO r"),
    @NamedQuery(name = "employeebonus.findById", query = "SELECT r FROM EmployeeBonusDO r where r.id =:id"),
    @NamedQuery(name = "employeebonus.findByEmpId", query = "SELECT r FROM EmployeeBonusDO r where r.empId =:empId"),
    @NamedQuery(name = "employeebonus.findByEmpIdAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.empId =:empId and r.bonusMonth = :monthly and r.status =:status"),
    @NamedQuery(name = "employeebonus.findByEmpIdAndMonthStatus", query = "SELECT r FROM EmployeeBonusDO r where r.empId =:empId and r.bonusMonth = :monthly"),
    @NamedQuery(name = "employeebonus.findByEmpIdBetweenMonth", query = "SELECT sum(r.amount) FROM EmployeeBonusDO r where r.empId =:empId and (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "employeebonus.findByEmpIdsAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.empId IN :empId and r.bonusMonth = :monthly and r.status =:status"),
    @NamedQuery(name = "employeebonus.findByMonth", query = "SELECT r FROM EmployeeBonusDO r where r.bonusMonth = :monthly and r.status =:status"),
    
    @NamedQuery(name = "employeebonus.findByEmpIdsBetweenMonth", query = "SELECT r FROM EmployeeBonusDO r where r.empId IN :empId and (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status"),
    @NamedQuery(name = "employeebonus.findBetweenMonth", query = "SELECT r FROM EmployeeBonusDO r where (r.bonusMonth >= :fromMonth and r.bonusMonth <= :toMonth) and r.status =:status "),
})
public class EmployeeBonusDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeebonus.findAll";
	
	public static final String FIND_BY_ID = "employeebonus.findById";
	
	public static final String FIND_BY_EMPID = "employeebonus.findByEmpId";
	
	public static final String FIND_BY_EMPID_DATE = "employeebonus.findByEmpIdAndMonth";
	
	public static final String FIND_BY_EMPID_DATE_WITHOUT_STATUS = "employeebonus.findByEmpIdAndMonthStatus";
	
	public static final String FIND_BY_EMPID_BETWEEN_DATE = "employeebonus.findByEmpIdBetweenMonth";
	
	public static final String FIND_BY_EMPID_MONTH = "employeebonus.findByEmpIdsAndMonth";
	
	public static final String FIND_BY_MONTH = "employeebonus.findByMonth";
	
	public static final String FIND_BY_EMPID_BETWEENMONTH = "employeebonus.findByEmpIdsBetweenMonth";
	
	public static final String FIND_BETWEENMONTH = "employeebonus.findBetweenMonth";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeebonus")
	private Long id;
	private Long empId;
	private Long amount;
	private String bonusMonth;
	private String comment;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private char status;
	private String paidOn;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getBonusMonth() {
		return bonusMonth;
	}
	public void setBonusMonth(String bonusMonth) {
		this.bonusMonth = bonusMonth;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getPaidOn() {
		return paidOn;
	}
	public void setPaidOn(String paidOn) {
		this.paidOn = paidOn;
	}
}