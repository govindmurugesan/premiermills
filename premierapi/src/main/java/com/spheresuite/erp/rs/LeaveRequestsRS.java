package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.LeaveManagementService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveRequestsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leaverequests")
public class LeaveRequestsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveRequestsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		LeaveRequestsDO leaveRequestDO = new LeaveRequestsDO(); 
			 		leaveRequestDO.setRequestType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
			 		leaveRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
			 		leaveRequestDO.setUpdatedon(new Date());
			 		leaveRequestDO.setCreatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				leaveRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				leaveRequestDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
		 			leaveRequestDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : null));
		 			if(inputJSON.get(CommonConstants.HOURS) != null){
		 				leaveRequestDO.setNumberofhours(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.HOURS).toString() : ""));
			 		}
		 			leaveRequestDO.setLeaveDay(!inputJSON.get(CommonConstants.LEAVEDAY).toString().isEmpty() ? inputJSON.get(CommonConstants.LEAVEDAY).toString() : null);
		 			leaveRequestDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
		 			if(inputJSON.get(CommonConstants.DATE) != null ){
		 				leaveRequestDO.setDate(inputJSON.get(CommonConstants.DATE).toString());
		 			}else{
		 				leaveRequestDO.setDate(null);
		 			}
		 			if(inputJSON.get(CommonConstants.FROMDATE) != null){
		 				leaveRequestDO.setFromDate(inputJSON.get(CommonConstants.FROMDATE).toString());
		 			}else{
		 				leaveRequestDO.setFromDate(null);
		 			}
		 			
		 			if(inputJSON.get(CommonConstants.TODATE) != null){
		 				leaveRequestDO.setToDate(inputJSON.get(CommonConstants.TODATE).toString());
		 			}else{
		 				leaveRequestDO.setToDate(null);
		 			}
		 			if(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE) != null){
		 				leaveRequestDO.setLeavetypefromDate(inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString());
		 			}else{
		 				leaveRequestDO.setLeavetypefromDate(null);
		 			}
		 			if(inputJSON.get(CommonConstants.LEAVETYPETODATE) != null){
		 				leaveRequestDO.setLeavetypeToDate(inputJSON.get(CommonConstants.LEAVETYPETODATE).toString());
		 			}else{
		 				leaveRequestDO.setLeavetypeToDate(null);
		 			}
		 			new LeaveRequestsService().persist(leaveRequestDO);
		 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Request Created");
		 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"leaves","leaveRequest");
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveRequestsDO> leaveRequestsList = new LeaveRequestsService().retrieve();
				respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			LeaveRequestsDO leaveRequestsDO = new LeaveRequestsDO(); 
		 				List<LeaveRequestsDO> leaveRequestList = new LeaveRequestsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(leaveRequestList != null && leaveRequestList.size() > 0){
		 					leaveRequestsDO = leaveRequestList.get(0);
		 				}
		 				//leaveRequestsDO.setRequestType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
		 				//leaveRequestsDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
		 				if(inputJSON.get(CommonConstants.COMMENT).toString() != null ){
		 					leaveRequestsDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
		 				}
		 				leaveRequestsDO.setUpdatedon(new Date());
		 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 					leaveRequestsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
		 				leaveRequestsDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
		 				
		 				/*if(inputJSON.get(CommonConstants.HOURS) != null){
		 					leaveRequestsDO.setNumberofhours(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.HOURS).toString() : ""));
				 		}*/
		 				//leaveRequestsDO.setLeaveDay(!inputJSON.get(CommonConstants.LEAVEDAY).toString().isEmpty() ? inputJSON.get(CommonConstants.LEAVEDAY).toString() : null);
		 				//leaveRequestsDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 			/*if(inputJSON.get(CommonConstants.DATE) != null){
			 				leaveRequestsDO.setDate((!inputJSON.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON.get(CommonConstants.DATE).toString() : null));
			 			}*/
			 			
			 			/*if(inputJSON.get(CommonConstants.FROMDATE) != null){
			 				leaveRequestsDO.setFromDate((!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null));
			 			}
			 			
			 			if(inputJSON.get(CommonConstants.TODATE) != null){
			 				leaveRequestsDO.setToDate((!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null));
			 			}*/
		 				new LeaveRequestsService().update(leaveRequestsDO);
		 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Request Updated");
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = new LeaveRequestsService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
					respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = new LeaveRequestsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = LeaveRequestsUtil.getLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAvailableLeavesByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveAvailableLeavesByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<LeaveRequestsDO> leaveRequestsList = new LeaveRequestsService().retrieveAvailableLeavesByEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()),inputJSON.get(CommonConstants.LEAVETYPEFROMDATE).toString(),inputJSON.get(CommonConstants.LEAVETYPETODATE).toString(),Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
					respJSON = LeaveRequestsUtil.getEmpLeaveRequestsList(leaveRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	

	@RequestMapping(value = "/retrieveLeaveStatus", method = RequestMethod.GET)
	public @ResponseBody String retrieveLeaveStatus(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveManagementDO> leaveList = new LeaveManagementService().retrieveAllActive();
				List<UserDO> activeUsersList = new UserService().retrieveActive();
				respJSON = LeaveRequestsUtil.getLeaveStatus(activeUsersList,leaveList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
}
