package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeDAO {
	static Logger logger = Logger.getLogger(EmployeeDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeDO persist(EmployeeDO employeeDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieve() throws AppException {
		List<EmployeeDO> userList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_ALL);
				userList = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveActive() throws AppException {
		List<EmployeeDO> userList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				userList = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveActiveNoCtc(List<Long> empIds) throws AppException {
		List<EmployeeDO> userList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_EMP_NOCTC);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				q.setParameter(CommonConstants.ID, empIds);
				userList = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> isEmpActive(Long empid) throws AppException {
		List<EmployeeDO> userList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_IS_ACTIVE);
				q.setParameter(CommonConstants.ID, empid);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				userList = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveById(Long id) throws AppException {
		List<EmployeeDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				user = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> retrieveByDeptIds(List<Long> deptIds) throws AppException {
		List<Long> employeeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_BY_DEPT);
				q.setParameter(CommonConstants.ID, deptIds);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				employeeList = (List<Long>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveByEmail(String email) throws AppException {
		List<EmployeeDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_BY_EMAIL);
				q.setParameter(CommonConstants.EMAIL, email);
				user = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}
	
	public EmployeeDO update(EmployeeDO employeeDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeDO> retrieveByEmpId(Long id) throws AppException {
		List<EmployeeDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeDO.FIND_EMP_ID);
				q.setParameter(CommonConstants.ID, id);
				user = (List<EmployeeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id) throws AppException {
		List<EmployeeDO> employeeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(companyEmail != null && Id != null && empId != null){
					List<Long> ids = new ArrayList<Long>();
					ids.add(Id);
					Query q = em.createNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE);
					q.setParameter(CommonConstants.EMAIL, companyEmail);
					q.setParameter(CommonConstants.EMPID, empId);
					q.setParameter(CommonConstants.ID, ids);
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					employeeList = (List<EmployeeDO>) q.getResultList();
					for (EmployeeDO employeeDO : employeeList) {
						if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
							return CommonConstants.COMPANYEMAIL;
						}else{
							return CommonConstants.EMPID;
						}
					}
				}else if(companyEmail != null && Id != null){
					List<Long> ids = new ArrayList<Long>();
					ids.add(Id);
					Query q = em.createNamedQuery(EmployeeDO.FIND_DUPLICATE_UPDATE_NOEMP);
					q.setParameter(CommonConstants.EMAIL, companyEmail);
					q.setParameter(CommonConstants.ID, ids);
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					employeeList = (List<EmployeeDO>) q.getResultList();
					for (EmployeeDO employeeDO : employeeList) {
						if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
							return CommonConstants.COMPANYEMAIL;
						}
					}
				}
				
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return CommonConstants.NO_DUPLICATE;
	}
	
	@SuppressWarnings("unchecked")
	public String checkDuplicate(String empId, String companyEmail) throws AppException {
		List<EmployeeDO> employeeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(companyEmail != null && empId != null){
					Query q = em.createNamedQuery(EmployeeDO.FIND_DUPLICATE_INSERT);
					q.setParameter(CommonConstants.EMAIL, companyEmail);
					q.setParameter(CommonConstants.EMPID, empId);
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					employeeList = (List<EmployeeDO>) q.getResultList();
					for (EmployeeDO employeeDO : employeeList) {
						if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
							return CommonConstants.COMPANYEMAIL;
						}else{
							return CommonConstants.EMPID;
						}
					}
				}else if(companyEmail != null){
					Query q = em.createNamedQuery(EmployeeDO.FIND_DUPLICATE_NOEMP);
					q.setParameter(CommonConstants.EMAIL, companyEmail);
					q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
					employeeList = (List<EmployeeDO>) q.getResultList();
					for (EmployeeDO employeeDO : employeeList) {
						if(employeeDO.getCompanyemail() != null && employeeDO.getCompanyemail().equalsIgnoreCase(companyEmail)){
							return CommonConstants.COMPANYEMAIL;
						}
					}
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return CommonConstants.NO_DUPLICATE;
	}
	
	public boolean persistList(List<EmployeeDO> empDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && empDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (EmployeeDO empDO2 : empDO) {
					em.persist(empDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}

}
