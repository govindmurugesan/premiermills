package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;

public class ScheduleUtil {
	
	private ScheduleUtil() {}
	
	public static JSONObject getScheduleList(List<ScheduleDO> scheduleList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ScheduleDO schedule : scheduleList) {
				resultJSONArray.put(getScheduleDetailObject(schedule));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getScheduleDetailObject(ScheduleDO schedule)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(schedule.getId() != null ? schedule.getId() : ""));
		result.put(CommonConstants.DESC, String.valueOf(schedule.getSubject() != null ? schedule.getSubject() : ""));
		result.put(CommonConstants.CONTACTLIST, String.valueOf(schedule.getContactList() != null ? schedule.getContactList() : ""));
		result.put(CommonConstants.TIME, String.valueOf(schedule.getLogTime() != null ? schedule.getLogTime() : ""));
		result.put(CommonConstants.DATE, String.valueOf(schedule.getLogDate() != null ? schedule.getLogDate() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToString(schedule.getUpdatedon())));
		if(schedule.getUpdatedBy() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(schedule.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(schedule.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(schedule.getLeadId() != null ? schedule.getLeadId() : ""));
		result.put(CommonConstants.BODY, String.valueOf(schedule.getDescription() != null ? schedule.getDescription() : ""));
		if(schedule.getLeadId() != null){
			List<LeadDO> leads = new LeadService().retrieveById(schedule.getLeadId());
			if(leads !=null && leads.size() > 0){
				result.put(CommonConstants.LEAD_NAME, String.valueOf(leads.get(0).getName()));
			}else{
				result.put(CommonConstants.LEAD_NAME,"");
			}
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
