package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.TaxPayeeTypeDO;
import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.TaxPayeeTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class TaxSlabsSettingsUtil {
	
	private TaxSlabsSettingsUtil() {}
	
	public static JSONObject getTaxSettingsList(List<TaxSlabsSettingsDO> taxSlabsSettingsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxSlabsSettingsDO taxSlabsSettingsDO : taxSlabsSettingsList) {
				resultJSONArray.put(getTaxSettingsDetailObject(taxSlabsSettingsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTaxSettingsDetailObject(TaxSlabsSettingsDO taxSlabsSettingsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(taxSlabsSettingsDO.getId() != null ? taxSlabsSettingsDO.getId() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(taxSlabsSettingsDO.getPercentageamount() != null ? taxSlabsSettingsDO.getPercentageamount() : ""));
		result.put(CommonConstants.FROMAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountfrom() != null ? taxSlabsSettingsDO.getAmountfrom() : ""));
		result.put(CommonConstants.TOAMOUNT, String.valueOf(taxSlabsSettingsDO.getAmountto() != null ? taxSlabsSettingsDO.getAmountto() : ""));
		if(taxSlabsSettingsDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(taxSlabsSettingsDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		result.put(CommonConstants.STARTDATE,String.valueOf(taxSlabsSettingsDO.getStartdate() != null ? taxSlabsSettingsDO.getStartdate() : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(taxSlabsSettingsDO.getEnddate() != null ? taxSlabsSettingsDO.getEnddate() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(taxSlabsSettingsDO.getStatus()));
		if(taxSlabsSettingsDO.getTaxpayeeId() != null){
			result.put(CommonConstants.TAXPAYEEID, String.valueOf(taxSlabsSettingsDO.getTaxpayeeId()));
			List<TaxPayeeTypeDO> taxpayeeList = new TaxPayeeTypeService().retrieveById(taxSlabsSettingsDO.getTaxpayeeId());
			if(taxpayeeList != null && taxpayeeList.size() > 0){
				result.put(CommonConstants.TAXPAYEENAME, String.valueOf(taxpayeeList.get(0).getTaxpayeeType()));
			}else{
				result.put(CommonConstants.TAXPAYEENAME, String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.TAXPAYEEID, String.valueOf(""));
		}
		if(taxSlabsSettingsDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(taxSlabsSettingsDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		String displayName="";
		if(taxSlabsSettingsDO.getPercentageamount() != null){
			displayName = taxSlabsSettingsDO.getPercentageamount() + "% Of Taxable Amount";
		}
		
		result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));
		return result;
	}
}
