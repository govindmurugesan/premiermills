package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PayrollDeductionDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.exception.AppException;

public class PayrollDeductionDetailsService {
	static Logger logger = Logger.getLogger(PayrollDeductionDetailsService.class.getName());
	
	public List<PayrollDeductionDetailsDO> persist(List<PayrollDeductionDetailsDO> payrollDeductionDetailsDO) throws AppException {
		return new PayrollDeductionDetailsDAO().persist(payrollDeductionDetailsDO);
	}

	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new PayrollDeductionDetailsDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<PayrollDeductionDetailsDO> retrieve() throws AppException {
		return new PayrollDeductionDetailsDAO().retrieve();
	}
	
	public PayrollDeductionDetailsDO update(PayrollDeductionDetailsDO payrollDeductionDetailsDO) throws AppException {
		return new PayrollDeductionDetailsDAO().update(payrollDeductionDetailsDO);
	}
	
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationIds(List<Long> id) throws AppException {
		return new PayrollDeductionDetailsDAO().retrieveByEmpCompensationIds(id);
	}
	
	public Double retrieveAllDeduction(Long id) throws AppException {
		return new PayrollDeductionDetailsDAO().retrieveAllDeduction(id);
	}
}
