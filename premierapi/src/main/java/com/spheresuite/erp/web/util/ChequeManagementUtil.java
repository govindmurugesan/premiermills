package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.ChequeManagementDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class ChequeManagementUtil {
	
	private ChequeManagementUtil() {}
	
	public static JSONObject getChequeList(List<ChequeManagementDO> chequeList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ChequeManagementDO cheques : chequeList) {
				resultJSONArray.put(getChequeObject(cheques));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getChequeObject(ChequeManagementDO ChequeDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(ChequeDO.getId()));
		
		result.put(CommonConstants.NAME, String.valueOf(ChequeDO.getBankName()));
		result.put(CommonConstants.AMOUNT, String.valueOf(ChequeDO.getChequeAmount()));
		result.put(CommonConstants.DATE, String.valueOf(ChequeDO.getChequeDate() != null ? ChequeDO.getChequeDate(): ""));
		result.put(CommonConstants.NUMBER, String.valueOf(ChequeDO.getChequeNumber() != null ? ChequeDO.getChequeNumber(): ""));
		result.put(CommonConstants.PAYTO, String.valueOf(ChequeDO.getPayTo() != null ? ChequeDO.getPayTo(): ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(ChequeDO.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(ChequeDO.getStatus() != null ? ChequeDO.getStatus(): ""));
		if(ChequeDO.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(ChequeDO.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
