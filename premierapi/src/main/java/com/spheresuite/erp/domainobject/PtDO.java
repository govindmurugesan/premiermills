package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="pt")
@TableGenerator(name ="pt", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "pt.findAll", query = "SELECT r FROM PtDO r"),
    @NamedQuery(name = "pt.findById", query = "SELECT r FROM PtDO r where r.id =:id"),
    @NamedQuery(name = "pt.findByStatus", query = "SELECT r FROM PtDO r where r.status =:status"),
    @NamedQuery(name = "pt.findByState", query = "SELECT r FROM PtDO r where r.status =:status and r.stateId =:id"),
})
public class PtDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "pt.findAll";
	
	public static final String FIND_BY_ID = "pt.findById";
	
	public static final String FIND_BY_STATUS = "pt.findByStatus";
	
	public static final String FIND_BY_STATE = "pt.findByState";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "pt")
	private Long id;
	private Long fromamount;
	private Long toamount;
	private Long ptamount;
	private Long stateId;
	private Long countryId;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFromamount() {
		return fromamount;
	}
	public void setFromamount(Long fromamount) {
		this.fromamount = fromamount;
	}
	public Long getToamount() {
		return toamount;
	}
	public void setToamount(Long toamount) {
		this.toamount = toamount;
	}
	public Long getPtamount() {
		return ptamount;
	}
	public void setPtamount(Long ptamount) {
		this.ptamount = ptamount;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}