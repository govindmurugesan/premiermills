package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.OffersDAO;
import com.spheresuite.erp.domainobject.OffersDO;
import com.spheresuite.erp.exception.AppException;

public class OffersService {
	static Logger logger = Logger.getLogger(OffersService.class.getName());
	
	public OffersDO persist(OffersDO offersDO) throws AppException {
		return new OffersDAO().persist(offersDO);
	}
	
	public OffersDO update(OffersDO offersDO) throws AppException {
		return new OffersDAO().update(offersDO);
	}

	public List<OffersDO> retrieveById(Long Id) throws AppException {
		return new OffersDAO().retrieveById(Id);
	}

	public List<OffersDO> retrieve() throws AppException {
		return new OffersDAO().retrieve();
	}
	
	public List<OffersDO> retrieveByStatus(char status) throws AppException {
		return new OffersDAO().retrieveByStatus(status);
	}
	
	public List<OffersDO> retrieveByProjectId(Long id) throws AppException {
		return new OffersDAO().retrieveByProjectId(id);
	}
	
	
}
