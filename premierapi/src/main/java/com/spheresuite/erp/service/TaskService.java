package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TaskDAO;
import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.exception.AppException;

public class TaskService {
	static Logger logger = Logger.getLogger(TaskService.class.getName());
	
	public TaskDO persist(TaskDO taskDO) throws AppException {
		return new TaskDAO().persist(taskDO);
	}

	public List<TaskDO> retrieveById(Long Id) throws AppException {
		return new TaskDAO().retrieveById(Id);
	}

	public List<TaskDO> retrieveByLeadId(Long Id) throws AppException {
		return new TaskDAO().retrieveByLeadId(Id);
	}
	
	public TaskDO update(TaskDO taskDO) throws AppException {
		return new TaskDAO().update(taskDO);
	}
	
	public List<TaskDO> retrievePendingTask() throws AppException {
		return new TaskDAO().retrievePendingTask();
	}
}
