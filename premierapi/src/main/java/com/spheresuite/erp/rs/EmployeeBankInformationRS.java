package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.service.EmployeeBankInformationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeBankInformationUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeebankinformation")
public class EmployeeBankInformationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeBankInformationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBankInformationDO employeeBankInformationDO = new EmployeeBankInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeBankInformationDO.setAccountName(!inputJSON.get(CommonConstants.ACCOUNTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.ACCOUNTNAME).toString() : null);
			 		employeeBankInformationDO.setBankName(!inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.BANKNAME).toString() : null);
			 		employeeBankInformationDO.setIfscCode(!inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.IFSCCODE).toString() : null);
			 		employeeBankInformationDO.setBranchName(!inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.BRANCHNAME).toString() : null);
			 		employeeBankInformationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeBankInformationDO.setAccountNumber(!inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()) : null);
			 		employeeBankInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeBankInformationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeBankInformationDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeBankInformationService().persist(employeeBankInformationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeBankInformationDO> employeeAddressList = new EmployeeBankInformationService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeBankInformationUtil.getEmployeeBankInfoList(employeeAddressList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeBankInformationDO employeeBankInformationDO = new EmployeeBankInformationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeBankInformationDO> employeeAddressList = new EmployeeBankInformationService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeBankInformationDO = employeeAddressList.get(0);
			 		employeeBankInformationDO.setAccountName(!inputJSON.get(CommonConstants.ACCOUNTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.ACCOUNTNAME).toString() : null);
			 		employeeBankInformationDO.setBankName(!inputJSON.get(CommonConstants.BANKNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.BANKNAME).toString() : null);
			 		employeeBankInformationDO.setIfscCode(!inputJSON.get(CommonConstants.IFSCCODE).toString().isEmpty() ? inputJSON.get(CommonConstants.IFSCCODE).toString() : null);
			 		employeeBankInformationDO.setBranchName(!inputJSON.get(CommonConstants.BRANCHNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.BRANCHNAME).toString() : null);
			 		employeeBankInformationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeBankInformationDO.setAccountNumber(!inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.ACCOUNTNUMBER).toString()) : null);
			 		employeeBankInformationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeBankInformationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeBankInformationService().update(employeeBankInformationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
