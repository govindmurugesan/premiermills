package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class TaxSlabsSettingsDAO {
	static Logger logger = Logger.getLogger(TaxSlabsSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public TaxSlabsSettingsDO persist(TaxSlabsSettingsDO taxSlabsSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && taxSlabsSettingsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(taxSlabsSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsDO;
	}
	
	public TaxSlabsSettingsDO update(TaxSlabsSettingsDO taxSlabsSettingsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && taxSlabsSettingsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(taxSlabsSettingsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieve() throws AppException {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSlabsSettingsDO.FIND_ALL);
				/*q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);*/
				taxSlabsSettingsList = (List<TaxSlabsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveById(Long Id) throws AppException {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSlabsSettingsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				taxSlabsSettingsList = (List<TaxSlabsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveBytaxPayee(Long Id, String startDate, String endDate) throws AppException {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSlabsSettingsDO.FIND_BY_TAXPAYEEID);
				q.setParameter(CommonConstants.ID, Id);
				q.setParameter(CommonConstants.STARTDATE, startDate);
				q.setParameter(CommonConstants.ENDDATE, endDate);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				taxSlabsSettingsList = (List<TaxSlabsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<TaxSlabsSettingsDO> retrieveActive() throws AppException {
		List<TaxSlabsSettingsDO> taxSlabsSettingsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(TaxSlabsSettingsDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				taxSlabsSettingsList = (List<TaxSlabsSettingsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return taxSlabsSettingsList;
	}
}
