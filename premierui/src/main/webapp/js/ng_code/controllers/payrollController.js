var app = angular.module('spheresuite')
app.controller('payrollController', payrollController);

payrollController.$inject = ['$scope', '$rootScope', '$location', '$localStorage', '$filter', 'commonService', 'configurationService', 'payrollService', 'userService', 'Upload', '$route', 'companyInformationService'];

function payrollController($scope, $rootScope, $location, $localStorage, $filter, commonService, configurationService, payrollService, userService, Upload, $route, companyInformationService) {
	var payrollControllerScope = this;

	if ($location.path() == '/employee/itsavings') {
        $rootScope.headerMenu = "Employees";
    }  
    
    else{
        $rootScope.headerMenu = "Payroll";
    }

	$scope.format = "MMM dd, yyyy";
	$scope.format1 = "MMM YYYY";
	
	payrollControllerScope.showBackPayroll = true;
	payrollControllerScope.showEmpCompTable = true;
	payrollControllerScope.isEmpCompDetail = false;
	payrollControllerScope.showEmpCompBack = false;
	payrollControllerScope.divValue = 'employeeComp';
	payrollControllerScope.clickToShow = false;
	payrollControllerScope.showBack = false;
	payrollControllerScope.showSummary = false;
	payrollControllerScope.showActualData = true
	payrollControllerScope.allowance;
	payrollControllerScope.minDate = new Date();
	payrollControllerScope.allowanceForm;
	payrollControllerScope.allowanceTypeList;
	payrollControllerScope.allowanceList = [];
	payrollControllerScope.allowanceMsg = '';
	payrollControllerScope.deduction;
	payrollControllerScope.deductionForm;
	payrollControllerScope.deductionTypeList;
	payrollControllerScope.deductionList = [];
	payrollControllerScope.deductionMsg = '';
	payrollControllerScope.taxMsg = '';
	payrollControllerScope.employeeCompensationEarningList = [];
	payrollControllerScope.employeeCompensationEarningListEmpComp = [];
	payrollControllerScope.employeeCompensationDeductionList = [];
	payrollControllerScope.employeeCompensationDeductionListEmpComp = [];
	payrollControllerScope.employeeCompensationTaxList = [];
	payrollControllerScope.employeeCompensationTaxListEmpComp = [];
	payrollControllerScope.isEdit = false;
	payrollControllerScope.limitToShowAllowance = 5;
	payrollControllerScope.limitToShowDeduction = 5;
	payrollControllerScope.isEffectiveFrom = false;
	payrollControllerScope.isEffectiveTo = false;
	payrollControllerScope.isPayrollDetail = false;
	payrollControllerScope.isRunPayroll = false;
	payrollControllerScope.earningMonth = 0;
	payrollControllerScope.earningYear = 0;
	payrollControllerScope.deductionMonth = 0;
	payrollControllerScope.deductionYear = 0;
	payrollControllerScope.taxMonth = 0;
	payrollControllerScope.taxYear = 0;
	payrollControllerScope.runPayroll;
	payrollControllerScope.runPayrollForm;
	payrollControllerScope.runPayrollList;
	payrollControllerScope.taxList = [];
	payrollControllerScope.bonusMonth;
	payrollControllerScope.bonusYear;
	payrollControllerScope.employeeComp;
	payrollControllerScope.compSettings;
	payrollControllerScope.empCompensationShowDataList;
	payrollControllerScope.taxSlabsList = [];
	payrollControllerScope.taxSlabsMsg = '';
	payrollControllerScope.limitToShowTaxSlabs = 5;
	
	payrollControllerScope.goBackToEmpComp = goBackToEmpComp;
	payrollControllerScope.changeDiv = changeDiv;
	payrollControllerScope.goBackToPayrollSummary = goBackToPayrollSummary;
	payrollControllerScope.addAllowance = addAllowance;
	payrollControllerScope.addDeduction = addDeduction;
	payrollControllerScope.addMoreAllowance = addMoreAllowance;
	payrollControllerScope.addMoreDeduction = addMoreDeduction;
	payrollControllerScope.addNewEarningsField = addNewEarningsField;
	payrollControllerScope.addNewEarningsFieldEmpComp = addNewEarningsFieldEmpComp;
	payrollControllerScope.addNewDeductionField = addNewDeductionField;
	payrollControllerScope.addNewDeductionFieldEmpComp = addNewDeductionFieldEmpComp;
	payrollControllerScope.addNewTaxField = addNewTaxField;
	payrollControllerScope.addNewTaxFieldEmpComp = addNewTaxFieldEmpComp;
	payrollControllerScope.closeModal = closeModal;
	payrollControllerScope.declineAllowance = declineAllowance;
	payrollControllerScope.declineMyItSavings = declineMyItSavings;
	payrollControllerScope.declineDeduction = declineDeduction;
	payrollControllerScope.editAllowance = editAllowance;
	payrollControllerScope.editDeduction = editDeduction;
	payrollControllerScope.getActiveUser = getActiveUser;
	payrollControllerScope.getAllowance = getAllowance;
	payrollControllerScope.getAllowanceById = getAllowanceById;
	payrollControllerScope.getAllowanceType = getAllowanceType;
	payrollControllerScope.getDeduction = getDeduction;
	payrollControllerScope.getDeductionType = getDeductionType;
	payrollControllerScope.getDeductionById = getDeductionById;
	payrollControllerScope.getTax = getTax;
	payrollControllerScope.getTaxType = getTaxType;
	payrollControllerScope.getTaxById = getTaxById;
	payrollControllerScope.getEarningsType = getEarningsType;
	payrollControllerScope.getEmpId = getEmpId;
	payrollControllerScope.getPayrollByEmp=getPayrollByEmp;
	payrollControllerScope.getRunPayroll = getRunPayroll;
	payrollControllerScope.hidePayroll = hidePayroll;
	payrollControllerScope.getRunPayrollMonthly= getRunPayrollMonthly;
	payrollControllerScope.gotoRunPayroll = gotoRunPayroll;
	payrollControllerScope.openAllowanceModal = openAllowanceModal;
	payrollControllerScope.openDeductionModal = openDeductionModal;
	payrollControllerScope.openEffectiveFromCalender = openEffectiveFromCalender;
	payrollControllerScope.openEffectiveToCalender = openEffectiveToCalender;
	payrollControllerScope.processPayroll = processPayroll;
	payrollControllerScope.resetAllowanceAmount = resetAllowanceAmount;
	payrollControllerScope.resetDeductionAmount = resetDeductionAmount;
	payrollControllerScope.submitPayroll = submitPayroll;
	payrollControllerScope.updateEmployeeCompensation = updateEmployeeCompensation;
	payrollControllerScope.viewPayrollDetail = viewPayrollDetail;
	payrollControllerScope.goToPayroll=goToPayroll;
	payrollControllerScope.editInCompensation=editInCompensation;
	payrollControllerScope.allowance = {};
	payrollControllerScope.batchActiveList;
	payrollControllerScope.batchId;
	payrollControllerScope.payrolltype;
	payrollControllerScope.batchName;
	payrollControllerScope.payPeriodTo;
	payrollControllerScope.payPeriodFrom;
	payrollControllerScope.payrollsummary = [];
	payrollControllerScope.empPayrollDetailList;
	payrollControllerScope.isDataAvailable = false;
	payrollControllerScope.isBonusAvailable = true;
	payrollControllerScope.goBackToPyroll = goBackToPyroll;
	payrollControllerScope.getEmployeeBonusTypeList = [];
	payrollControllerScope.showEmpCompValue = false;

	$rootScope.limitToShow = 10;
    $rootScope.beginFrom = 0;
    
    payrollControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
    payrollControllerScope.buttonBeginFrom = 0;
	payrollControllerScope.isNextDisabled = false;
    payrollControllerScope.gotoPage = gotoPage;
	payrollControllerScope.showPrevNav = showPrevNav;
    payrollControllerScope.showNextNav = showNextNav;
    payrollControllerScope.payrollList = [];
    payrollControllerScope.runPayrollListBackup = [];
    payrollControllerScope.runPayrollDetailListBackup = [];
    payrollControllerScope.viewAllEmployee = viewAllEmployee;
    payrollControllerScope.addEmployeeLop = addEmployeeLop;
    payrollControllerScope.getEmployeeLop = getEmployeeLop;
    payrollControllerScope.getEmployeeLopTypeList = [];
    payrollControllerScope.gotoUpdateEmployeeLopDetails = gotoUpdateEmployeeLopDetails;
    payrollControllerScope.gotoAddEmployeeLopDetails = gotoAddEmployeeLopDetails;
    payrollControllerScope.updateEmployeeLopDetails = updateEmployeeLopDetails;
    payrollControllerScope.cancelEmployeeLopDetails = cancelEmployeeLopDetails;
    payrollControllerScope.gotoAddEmployeeBonusDetails = gotoAddEmployeeBonusDetails;
    payrollControllerScope.addEmpBonusDetails = addEmpBonusDetails;
    payrollControllerScope.getEmployeeBonusDetails = getEmployeeBonusDetails
    payrollControllerScope.gotoEditEmpBonus = gotoEditEmpBonus;
    payrollControllerScope.updateEmpBonusDetails = updateEmpBonusDetails;
    payrollControllerScope.cancelEmployeeBonusDetails = cancelEmployeeBonusDetails;
    payrollControllerScope.declineEmpLop = declineEmpLop;
    payrollControllerScope.declineEmpBonusDetails = declineEmpBonusDetails;
    payrollControllerScope.addEmployeeTax = addEmployeeTax;
    payrollControllerScope.getTax = getTax;
    payrollControllerScope.declineTax = declineTax;
    payrollControllerScope.editTax = editTax;
    payrollControllerScope.resetTaxAmount = resetTaxAmount;
    payrollControllerScope.openTaxModal = openTaxModal;
    payrollControllerScope.getPayrollByEmpDate = getPayrollByEmpDate;
    payrollControllerScope.netYearShow = 0;
    payrollControllerScope.netMonthShow = 0;
    payrollControllerScope.getPayrollDetails = getPayrollDetails;
    payrollControllerScope.netTotalMonth;
    payrollControllerScope.netTotalYear;
    payrollControllerScope.empBonusShow;
    payrollControllerScope.getAllowanceByIdCompSettings = getAllowanceByIdCompSettings;
    payrollControllerScope.getDeductionByIdCompSettings = getDeductionByIdCompSettings;
    payrollControllerScope.getTaxByIdCompSettings = getTaxByIdCompSettings;
    payrollControllerScope.getEmpIdEmpComp = getEmpIdEmpComp;
    payrollControllerScope.empCompShowData = empCompShowData;
    payrollControllerScope.getEmpCompensation = getEmpCompensation;
    payrollControllerScope.getItSavingsDate = getItSavingsDate;
    payrollControllerScope.getSavingsData = getSavingsData;
    payrollControllerScope.addmyItSavings = addmyItSavings;
    payrollControllerScope.getItSavings = getItSavings;
    payrollControllerScope.gotoEditSavings = gotoEditSavings;
    payrollControllerScope.getEmployeeItSavings = getEmployeeItSavings;
    payrollControllerScope.gotoEmployeeItSavings = gotoEmployeeItSavings;
    payrollControllerScope.updateEmployeeItSavingsReject = updateEmployeeItSavingsReject;
    payrollControllerScope.updateEmployeeItSavingsVerified = updateEmployeeItSavingsVerified;
    payrollControllerScope.updateEmployeeItSavingsApproved = updateEmployeeItSavingsApproved;
    payrollControllerScope.submitForSummary = submitForSummary;
    payrollControllerScope.declineForSummary = declineForSummary;
    payrollControllerScope.addTaxSlabs = addTaxSlabs;
    payrollControllerScope.getTaxSlabs = getTaxSlabs;
    payrollControllerScope.editTaxSlabs = editTaxSlabs;
    payrollControllerScope.declineTaxSlabs = declineTaxSlabs;
    payrollControllerScope.resetTaxSlabsAmount = resetTaxSlabsAmount;
    payrollControllerScope.openTaxSlabs = openTaxSlabs;
    payrollControllerScope.addMoreTaxSlabs = addMoreTaxSlabs;
    payrollControllerScope.fromDate;
    payrollControllerScope.toDate
    payrollControllerScope.getTaxPayeeTypeActive = getTaxPayeeTypeActive;
    payrollControllerScope.getCompanyInformation = getCompanyInformation;
    payrollControllerScope.getState = getState;
    payrollControllerScope.getPTDetails = getPTDetails;
    payrollControllerScope.PTData = true;
    payrollControllerScope.getFYActive = getFYActive;
    payrollControllerScope.getFYActiveDetails = getFYActiveDetails;
    payrollControllerScope.FYData = true;
	
	payrollControllerScope.minDate = moment(payrollControllerScope.minDate).format('MMM YYYY');
	console.log(payrollControllerScope.minDate);
	if ($location.path() == '/payroll/settings') {
		getAllowance();
		getDeduction();
		getAllowanceType();
		getDeductionType();
		getTax();
		getTaxSlabs();
		getTaxPayeeTypeActive();
		getCompanyInformation();
		getFYActive();
	} else if ($location.path() == '/employee/compensation') {
		getAllowance();
		getDeduction();
		getActiveUser();
		getTax();
		empCompShowData();
	} else if ($location.path() == '/payroll/run') {
		getActiveBatch();
		$('body').addClass('run_payrollCalander');
	}else if($location.path() == '/payroll/detail'){
		getRunPayrollMonthly($localStorage.spheresuite.runPayrollDate, 'monthly', $localStorage.spheresuite.payrollBatchId, '');
	}else if($location.path() == '/payroll/summary'){
		getPayrollSummary();
	}else if($location.path() == '/payroll/lop'){
		getActiveUser();
		getEmployeeLop();
	}else if($location.path() == '/payroll/bonus'){
		getActiveUser();
		getEmployeeBonusDetails();
	}else if ($location.path() == '/payroll/itsavings') {
		getItSavings($localStorage.spheresuite.id);

	} else if($location.path() == '/employee/itsavings'){
		getEmployeeItSavings();
	}
	//payrollControllerScope.myItSavings;
	
	$scope.$watch("payrollControllerScope.myItSavings.toDate", function(newValue, oldValue) {
	    getItSavingsDate(payrollControllerScope.myItSavings);
	});
	function goBackToPyroll(){
		payrollControllerScope.isRunPayroll = false;
		payrollControllerScope.isPayrollDetail = false;
		payrollControllerScope.showBack = false;
	}
	
	function getPayrollSummary(){
		payrollControllerScope.spinner = true;
		payrollControllerScope.isDataAvailable = false;
		payrollService.getPayrollSummary().then(function(res){
    		console.log('getActiveBatch res',res)
    		if(res.successflag == 'true' && res.results.length > 0){
    			payrollControllerScope.isDataAvailable = true;
    			payrollControllerScope.payrollsummary = res.results;
    			console.log('payrollControllerScope.payrollsummary   ', payrollControllerScope.payrollsummary )
    			payrollControllerScope.payrollSummaryListBackup = angular.copy(payrollControllerScope.payrollsummary);
	            payrollControllerScope.isDataAvailable = true;

		        	for(var i = 0; i < payrollControllerScope.payrollSummaryListBackup.length; i++){
		            	payrollControllerScope.payrollSummaryListBackup[i].isActiveClass = false;
		            	payrollControllerScope.payrollSummaryListBackup[i].isSelect = false;
		        	}
					payrollControllerScope.payrollsummary =  angular.copy(payrollControllerScope.payrollSummaryListBackup);
		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollsummary.length)
		        		payrollControllerScope.isNextDisabled = true;
					payrollControllerScope.payrollsummary[0].isActiveClass = true;
					console.log('payrollControllerScope.payrollsummary 00000 ',payrollControllerScope.payrollsummary )
    		}else{
    			payrollControllerScope.dataMsg =  "Payroll Summary Not Available";
    		}
    		payrollControllerScope.spinner = false;
    	},function(err){
    		payrollControllerScope.spinner = false;
    		payrollControllerScope.dataMsg =  "Payroll Summary Not Available";
    	});
	}
	
	function viewAllEmployee(data){
		payrollControllerScope.clickToShow = true;
		payrollControllerScope.showSummary=true;
		payrollControllerScope.showActualData = false;
		payrollControllerScope.spinner = true;
		payrollService.viewAllEmployee(data).then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				//payrollControllerScope.allowanceTypeList = res.results;
				console.log("res.results",res.results)
				payrollControllerScope.empPayrollDetailList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = true;
		});
	}
	
	/*function showSummaryData(){
		payrollControllerScope.showSummary=true;
		payrollControllerScope.showActualData = false;
	}*/

	function goBackToPayrollSummary(){
		payrollControllerScope.clickToShow = false;
		payrollControllerScope.showSummary=false;
		payrollControllerScope.showActualData = true;
	}

	function getActiveBatch(){
		payrollControllerScope.spinner = true;
        configurationService.getActiveBatch().then(function(res){
    		console.log('getActiveBatch res',res)
    		if(res.successflag == 'true' && res.results.length > 0){
    			payrollControllerScope.batchActiveList = res.results;
    		}
    		payrollControllerScope.spinner = false;
    	},function(err){
    		payrollControllerScope.spinner = false;
    	});
    }

	function addAllowance() {
		if (payrollControllerScope.allowance) {
			payrollControllerScope.spinner = true;
			commonService.formValNotManditory(payrollControllerScope.allowanceForm, payrollControllerScope.allowance).then(function (data) {
				if (data) {
					data.updatedBy = $localStorage.spheresuite.id;
					console.log('data', data)
					payrollService.addAllowance(data).then(function (res) {
						console.log('addAllowance res', res)
						if (res.successflag == 'true') {
							declineAllowance();
							getAllowance();
							$('#allowance').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					payrollControllerScope.spinner = false;
				}
			});
		}
	}

	function addDeduction() {
		if (payrollControllerScope.deduction) {
			payrollControllerScope.spinner = true;
			commonService.formValNotManditory(payrollControllerScope.deductionForm, payrollControllerScope.deduction).then(function (data) {
				if (data) {
					data.updatedBy = $localStorage.spheresuite.id;
					payrollService.addDeduction(data).then(function (res) {
						if (res.successflag == 'true') {
							declineDeduction();
							getDeduction();
							$('#deduction').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					payrollControllerScope.spinner = false;
				}
			});
		}
	}


	function hidePayroll() {
		payrollControllerScope.isRunPayroll = false;
	}
	
	function addMoreAllowance() {
		payrollControllerScope.limitToShowAllowance += 5;
	}

	function addMoreDeduction() {
		payrollControllerScope.limitToShowDeduction += 5;
	}

	function addNewEarningsField() {
		payrollControllerScope.employeeCompensationEarningList.push({
			allowanceId: "",
			monthly: "",
			ytd: ""
		});
	}

	function addNewEarningsFieldEmpComp() {
		payrollControllerScope.employeeCompensationEarningListEmpComp.push({
			allowanceId: "",
			monthly: "",
			ytd: ""
		});
	}


	function addNewDeductionField() {
		payrollControllerScope.employeeCompensationDeductionList.push({
			deductionId: "",
			monthly: "",
			ytd: ""
		});
	}

	function addNewDeductionFieldEmpComp() {
		payrollControllerScope.employeeCompensationDeductionListEmpComp.push({
			deductionId: "",
			monthly: "",
			ytd: ""
		});
	}

	function addNewTaxField() {
		payrollControllerScope.employeeCompensationTaxList.push({
			taxId: "",
			monthly: "",
			ytd: ""
		});
	}

	function addNewTaxFieldEmpComp() {
		payrollControllerScope.employeeCompensationTaxListEmpComp.push({
			taxId: "",
			monthly: "",
			ytd: ""
		});
	}

	function closeModal() {
		$('#confirmation').modal('hide');
	}

	function editAllowance() {
		if (payrollControllerScope.allowance) {
			console.log('payrollControllerScope.allowance', payrollControllerScope.allowance);
			if(payrollControllerScope.allowance.enddate == undefined){
				console.log('emterinngg')
				payrollControllerScope.allowance.enddate = '';
			}
			console.log('qwerty', payrollControllerScope.allowance);
			payrollControllerScope.spinner = true;
			payrollControllerScope.allowance.updatedBy = $localStorage.spheresuite.id;
			if (payrollControllerScope.allowance.taxable == 'y') {
				payrollControllerScope.allowance.maxLimit = '';
			}
			payrollService.editAllowance(payrollControllerScope.allowance).then(function (res) {
				if (res.successflag == 'true') {
					console.log('editAllowance', res);
					declineAllowance();
					getAllowance();
					$('#allowance').modal('hide');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			});
		}
	}
	
	 function goToPayroll() {
	       // if ($location.path()=="/leads" || $location.path()=="/lead/add" || $location.path()=="/lead/view" || $location.path()=="/lead/edit") {
	            $location.path('/payroll/run');
	        //} 
	    }

	function editDeduction() {
		if (payrollControllerScope.deduction) {
			if(payrollControllerScope.deduction.enddate == undefined){
				console.log('emterinngg')
				payrollControllerScope.deduction.enddate = '';
			}
			payrollControllerScope.spinner = true;
			payrollControllerScope.deduction.updatedBy = $localStorage.spheresuite.id;
			payrollService.editDeduction(payrollControllerScope.deduction).then(function (res) {
				if (res.successflag == 'true') {
					declineDeduction();
					getDeduction();
					$('#deduction').modal('hide');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			});
		}
	}

	function declineAllowance() {
		payrollControllerScope.allowance = null;
		payrollControllerScope.allowanceForm.$setPristine();
		payrollControllerScope.allowanceForm.$setUntouched();
		payrollControllerScope.isEdit = false;
	}
	
	function declineMyItSavings() {
		payrollControllerScope.myItSavings = null;
		payrollControllerScope.myItSavingsForm.$setPristine();
		payrollControllerScope.myItSavingsForm.$setUntouched();
		payrollControllerScope.isEdit = false;
	}

	function declineDeduction() {
		payrollControllerScope.deduction = null;
		payrollControllerScope.deductionForm.$setPristine();
		payrollControllerScope.deductionForm.$setUntouched();
		payrollControllerScope.isEdit = false;
	}

	function declineTax() {
		payrollControllerScope.tax = null;
		payrollControllerScope.taxForm.$setPristine();
		payrollControllerScope.taxForm.$setUntouched();
		payrollControllerScope.isEdit = false;
	}


	function getActiveUser() {
		payrollControllerScope.getActiveUserSpinner = true;
		userService.getActiveUser().then(function (res) {
			if (res.successflag === 'true' && res.results.length > 0) {
				payrollControllerScope.activeUserList = res.results;
				console.log("payrollControllerScope.activeUserList", payrollControllerScope.activeUserList)
				payrollControllerScope.getActiveUserSpinner = false;
				console.log('$localStorage.spheresuite.usrId   ', $localStorage.spheresuite.usrId);
				if($localStorage.spheresuite.usrId){
					payrollControllerScope.allowance.empId = $localStorage.spheresuite.usrId;
					delete $localStorage.spheresuite['usrId'];
					getEmpId(payrollControllerScope.allowance.empId);
				}
				
			} else {
				payrollControllerScope.getActiveUserSpinner = false;
			}
		}, function (err) {
			payrollControllerScope.getActiveUserSpinner = false;
		});
	}

	function getEmpId(empId) { console.log('2');
		//console.log('payrollControllerScope.allowance.empId  ', payrollControllerScope.allowance.empId);
		//console.log('empId  ', empId);		
		//console.log('payrollControllerScope.allowance.empId11 ', payrollControllerScope.allowance.empId)
		if (empId) {
			payrollControllerScope.spinner = true;
			payrollControllerScope.earningshow = false;
			payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
			payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
			payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
			payrollControllerScope.employeeCompensationEarningList = [{
				allowanceId: "",
				monthly: "",
				ytd: ""
			}];
			payrollControllerScope.employeeCompensationDeductionList = [{
				deductionId: "",
				monthly: "",
				ytd: ""
			}];
			payrollControllerScope.employeeCompensationTaxList = [{
				taxId: "",
				monthly: "",
				ytd: ""
			}];
			payrollService.getLeadByEmp(empId).then(function (res) {

				if (res.successflag == 'true' && res.results.length > 0) {
					console.log('qwerty', res.results[0]);
					payrollControllerScope.leadListByEmp = res.results[0];
					if (res.results[0].employeeCompensationEarningList != '' && res.results[0].employeeCompensationEarningList != '[]'){
						payrollControllerScope.employeeCompensationEarningList = res.results[0].employeeCompensationEarningList;
						payrollControllerScope.earningshow = true;
					}
					if (res.results[0].employeeCompensationDeductionList != '' && res.results[0].employeeCompensationDeductionList != '[]')
						payrollControllerScope.employeeCompensationDeductionList = res.results[0].employeeCompensationDeductionList;
					if (res.results[0].employeeCompensationTaxList != '' && res.results[0].employeeCompensationTaxList != '[]')
						payrollControllerScope.employeeCompensationTaxList = res.results[0].employeeCompensationTaxList;
					payrollControllerScope.earningMonth = parseFloat(res.results[0].earningMonth);
					payrollControllerScope.earningYear = parseFloat(res.results[0].earningYear);
					payrollControllerScope.deductionMonth = parseFloat(res.results[0].deductionMonth);
					payrollControllerScope.deductionYear = parseFloat(res.results[0].deductionYear);
					payrollControllerScope.taxMonth = parseFloat(res.results[0].taxMonth);
					payrollControllerScope.taxYear = parseFloat(res.results[0].taxYear);
					payrollControllerScope.earning = true;
					
					payrollControllerScope.spinner = false;
					console.log('truemsg', payrollControllerScope.leadListByEmp);
				} else {
					payrollControllerScope.earning = false;
					payrollControllerScope.leadListByEmp = [];
					payrollControllerScope.ctcMsg = "Employee CTC Not Avaliable";
					payrollControllerScope.spinner = false;
				}
			}, function (err) {
				payrollControllerScope.spinner = false;
			});;
		}
	}

	function getEmpIdEmpComp(empId,month) { 
		//console.log('payrollControllerScope.allowance.empId  ', payrollControllerScope.allowance.empId);
		//console.log('empId  ', empId);		
		//console.log('payrollControllerScope.allowance.empId11 ', payrollControllerScope.allowance.empId)
		if (empId) {
			payrollControllerScope.spinner = true;
			payrollControllerScope.earningshowEmp = false;
			payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
			payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
			payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
			payrollControllerScope.employeeCompensationEarningListEmpComp = [{
				allowanceId: "",
				monthly: "",
				ytd: ""
			}];
			payrollControllerScope.employeeCompensationDeductionListEmpComp = [{
				deductionId: "",
				monthly: "",
				ytd: ""
			}];
			payrollControllerScope.employeeCompensationTaxListEmpComp = [{
				taxId: "",
				monthly: "",
				ytd: ""
			}];
			payrollService.getPayrollByEmpDate(empId,month).then(function (res) {

				if (res.successflag == 'true' && res.results.length > 0) {
					console.log('qwerty1122', res.results[0]);
					payrollControllerScope.leadListByEmpComp = res.results[0];
					if (res.results[0].employeeCompensationEarningList != '' && res.results[0].employeeCompensationEarningList != '[]'){
						payrollControllerScope.employeeCompensationEarningListEmpComp = res.results[0].employeeCompensationEarningList;
						payrollControllerScope.earningshowEmp = true;
					}
					if (res.results[0].employeeCompensationDeductionList != '' && res.results[0].employeeCompensationDeductionList != '[]')
						payrollControllerScope.employeeCompensationDeductionListEmpComp = res.results[0].employeeCompensationDeductionList;
					if (res.results[0].employeeCompensationTaxList != '' && res.results[0].employeeCompensationTaxList != '[]')
						payrollControllerScope.employeeCompensationTaxListEmpComp = res.results[0].employeeCompensationTaxList;
					payrollControllerScope.earningMonth = parseFloat(res.results[0].earningMonth);
					payrollControllerScope.earningYear = parseFloat(res.results[0].earningYear);
					payrollControllerScope.deductionMonth = parseFloat(res.results[0].deductionMonth);
					payrollControllerScope.deductionYear = parseFloat(res.results[0].deductionYear);
					payrollControllerScope.taxMonth = parseFloat(res.results[0].taxMonth);
					payrollControllerScope.taxYear = parseFloat(res.results[0].taxYear);
					payrollControllerScope.earning = true;
					
					payrollControllerScope.spinner = false;
					console.log('truemsg', payrollControllerScope.leadListByEmpComp);
				} else {
					payrollControllerScope.earning = false;
					payrollControllerScope.leadListByEmpComp = [];
					payrollControllerScope.ctcMsg = "Employee CTC Not Avaliable";
					payrollControllerScope.spinner = false;
				}
			}, function (err) {
				payrollControllerScope.spinner = false;
			});;
		}
	}
	
	function getPayrollByEmp(empId) { console.log('2');
	if (empId) {
		payrollControllerScope.isPayrollDetail = true;
		payrollControllerScope.spinner = true;
		payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
		payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
		payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
		payrollControllerScope.employeeCompensationEarningList = [{
			allowanceId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationDeductionList = [{
			deductionId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationTaxList = [{
			taxId: "",
			monthly: "",
			ytd: ""
		}];
		payrollService.getPayrollByEmp(empId).then(function (res) {

			if (res.successflag == 'true' && res.results.length > 0) {
				
				payrollControllerScope.leadListByEmp = res.results[0];
				if (res.results[0].employeeCompensationEarningList != '' && res.results[0].employeeCompensationEarningList != '[]')
					payrollControllerScope.employeeCompensationEarningList = res.results[0].employeeCompensationEarningList;
				if (res.results[0].employeeCompensationDeductionList != '' && res.results[0].employeeCompensationDeductionList != '[]')
					payrollControllerScope.employeeCompensationDeductionList = res.results[0].employeeCompensationDeductionList;
				if (res.results[0].employeeCompensationTaxList != '' && res.results[0].employeeCompensationTaxList != '[]')
					payrollControllerScope.employeeCompensationTaxList = res.results[0].employeeCompensationTaxList;
				payrollControllerScope.earningMonth = parseFloat(res.results[0].earningMonth);
				payrollControllerScope.earningYear = parseFloat(res.results[0].earningYear);
				payrollControllerScope.deductionMonth = parseFloat(res.results[0].deductionMonth);
				payrollControllerScope.deductionYear = parseFloat(res.results[0].deductionYear);
				payrollControllerScope.taxMonth = parseFloat(res.results[0].taxMonth);
				payrollControllerScope.taxYear = parseFloat(res.results[0].taxYear);
				payrollControllerScope.earning = true;
				payrollControllerScope.spinner = false;
				console.log('truemsg', payrollControllerScope.leadListByEmp);
			} else {
				payrollControllerScope.earning = false;
				payrollControllerScope.leadListByEmp = [];
				payrollControllerScope.ctcMsg = "Employee CTC Not Avaliable";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});;
	}
}

function getPayrollByEmpDate(empId,month) {
	if (empId) {
		//payrollControllerScope.isPayrollDetail = true;
		payrollControllerScope.spinner = true;
		payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
		payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
		payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
		payrollControllerScope.employeeCompensationEarningList = [{
			allowanceId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationDeductionList = [{
			deductionId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationTaxList = [{
			taxId: "",
			monthly: "",
			ytd: ""
		}];
		payrollService.getPayrollByEmpDate(empId,month).then(function (res) {
			console.log('payrollControllerScope.runPayrollList  ', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				console.log('22==', res.results[0]);
				payrollControllerScope.bonusMonth = res.results[0].bonusMonth,
				payrollControllerScope.bonusYear = res.results[0].bonusYear,
				payrollControllerScope.leadListByEmp = res.results[0];
				console.log('payrollControllerScope.leadListByEmp', payrollControllerScope.leadListByEmp);
				if (res.results[0].employeeCompensationEarningList != '' && res.results[0].employeeCompensationEarningList != '[]')
					payrollControllerScope.employeeCompensationEarningList = res.results[0].employeeCompensationEarningList;
				if (res.results[0].employeeCompensationDeductionList != '' && res.results[0].employeeCompensationDeductionList != '[]')
					payrollControllerScope.employeeCompensationDeductionList = res.results[0].employeeCompensationDeductionList;
				if (res.results[0].employeeCompensationTaxList != '' && res.results[0].employeeCompensationTaxList != '[]')
					payrollControllerScope.employeeCompensationTaxList = res.results[0].employeeCompensationTaxList;
				payrollControllerScope.earningMonth = parseFloat(res.results[0].earningMonth);
				payrollControllerScope.earningYear = parseFloat(res.results[0].earningYear);
				payrollControllerScope.deductionMonth = parseFloat(res.results[0].deductionMonth);
				payrollControllerScope.deductionYear = parseFloat(res.results[0].deductionYear);
				payrollControllerScope.taxMonth = parseFloat(res.results[0].taxMonth);
				payrollControllerScope.taxYear = parseFloat(res.results[0].taxYear);

				payrollControllerScope.netMonthShow = 0;
				if(payrollControllerScope.earningMonth){
					payrollControllerScope.netMonthShow += payrollControllerScope.earningMonth;
				}

				if(payrollControllerScope.deductionMonth){
					payrollControllerScope.netMonthShow -= payrollControllerScope.deductionMonth;
				}

				

				if(payrollControllerScope.bonusMonth){
					console.log('payrollControllerScope.bonusMonth', payrollControllerScope.bonusMonth, payrollControllerScope.netMonthShow);
					payrollControllerScope.netMonthShow = payrollControllerScope.netMonthShow + parseInt(payrollControllerScope.bonusMonth);
					console.log('payrollControllerScope.netMonthShow', payrollControllerScope.netMonthShow);
				}

				payrollControllerScope.netYearShow = 0;

				if(payrollControllerScope.earningYear){
					payrollControllerScope.netYearShow += payrollControllerScope.earningYear;
				}

				if(payrollControllerScope.deductionYear){
					payrollControllerScope.netYearShow -= payrollControllerScope.deductionYear;
				}

							
				
				payrollControllerScope.earning = true;
				payrollControllerScope.spinner = false;
				console.log('truemsg', payrollControllerScope.leadListByEmp);
			} else {
				payrollControllerScope.earning = false;
				payrollControllerScope.leadListByEmp = [];
				payrollControllerScope.ctcMsg = "Employee CTC Not Avaliable";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});;
	}
}

function goBackToEmpComp(){
	payrollControllerScope.showEmpCompTable = true;
	payrollControllerScope.isEmpCompDetail = false;
	payrollControllerScope.showEmpCompBack = false;
}
function getEmpCompensation(empId) {
	
	if (empId) {
		$('#empCompData').modal('show');
	payrollControllerScope.isEmpCompDetail = true;
	payrollControllerScope.showEmpCompBack = true;
		payrollControllerScope.spinner = true;
		payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
		payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
		payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
		payrollControllerScope.employeeCompensationEarningList = [{
			allowanceId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationDeductionList = [{
			deductionId: "",
			monthly: "",
			ytd: ""
		}];
		payrollControllerScope.employeeCompensationTaxList = [{
			taxId: "",
			monthly: "",
			ytd: ""
		}];
		payrollService.getEmpCompensation(empId).then(function (res) {

			if (res.successflag == 'true' && res.results.length > 0) {
				console.log('22==nathan', res.results[0]);
				payrollControllerScope.bonusMonth = res.results[0].bonusMonth,
				payrollControllerScope.bonusYear = res.results[0].bonusYear,
				payrollControllerScope.leadListByEmp = res.results[0];
				if (res.results[0].employeeCompensationEarningList != '' && res.results[0].employeeCompensationEarningList != '[]')
					payrollControllerScope.employeeCompensationEarningList = res.results[0].employeeCompensationEarningList;
				if (res.results[0].employeeCompensationDeductionList != '' && res.results[0].employeeCompensationDeductionList != '[]')
					payrollControllerScope.employeeCompensationDeductionList = res.results[0].employeeCompensationDeductionList;
				if (res.results[0].employeeCompensationTaxList != '' && res.results[0].employeeCompensationTaxList != '[]')
					payrollControllerScope.employeeCompensationTaxList = res.results[0].employeeCompensationTaxList;
				payrollControllerScope.earningMonth = parseFloat(res.results[0].earningMonth);
				payrollControllerScope.earningYear = parseFloat(res.results[0].earningYear);
				payrollControllerScope.deductionMonth = parseFloat(res.results[0].deductionMonth);
				payrollControllerScope.deductionYear = parseFloat(res.results[0].deductionYear);
				payrollControllerScope.taxMonth = parseFloat(res.results[0].taxMonth);
				payrollControllerScope.taxYear = parseFloat(res.results[0].taxYear);

				payrollControllerScope.netMonthShow = 0;
				if(payrollControllerScope.earningMonth){
					payrollControllerScope.netMonthShow += payrollControllerScope.earningMonth;
				}

				if(payrollControllerScope.deductionMonth){
					payrollControllerScope.netMonthShow -= payrollControllerScope.deductionMonth;
				}

				

				if(payrollControllerScope.bonusMonth){
					console.log('payrollControllerScope.bonusMonth', payrollControllerScope.bonusMonth, payrollControllerScope.netMonthShow);
					payrollControllerScope.netMonthShow = payrollControllerScope.netMonthShow + parseInt(payrollControllerScope.bonusMonth);
					console.log('payrollControllerScope.netMonthShow', payrollControllerScope.netMonthShow);
				}

				payrollControllerScope.netYearShow = 0;

				if(payrollControllerScope.earningYear){
					payrollControllerScope.netYearShow += payrollControllerScope.earningYear;
				}

				if(payrollControllerScope.deductionYear){
					payrollControllerScope.netYearShow -= payrollControllerScope.deductionYear;
				}

							
				
				/*payrollControllerScope.earning = true;*/
				payrollControllerScope.spinner = false;
				console.log('truemsg', payrollControllerScope.leadListByEmp);
			} else {
				/*payrollControllerScope.earning = false;*/
				payrollControllerScope.leadListByEmp = [];
				payrollControllerScope.ctcMsg = "Employee CTC Not Avaliable";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});;
	}
}

	function getAllowanceByIdCompSettings(allowance, ctc) {
		if (allowance && ctc) {
			payrollControllerScope.earningMonth = 0;
			payrollControllerScope.earningYear = 0
			payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			var flag = "";
			var temp = 0;
			for (var i = 0; i < payrollControllerScope.employeeCompensationEarningList.length; i++) {
				if (allowance.allowanceId == payrollControllerScope.employeeCompensationEarningList[i].allowanceId) {
					temp++;
				}
			}
			
			if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationEarningList.length; i++) {
					if (allowance.allowanceId == payrollControllerScope.employeeCompensationEarningList[i].allowanceId && payrollControllerScope.employeeCompensationEarningList[i].monthly != '') {
						allowance.monthly = '';
						allowance.ytd = '';
						canEnter = false;						
					}
					if (payrollControllerScope.employeeCompensationEarningList[i].monthly != '') {						
						payrollControllerScope.earningMonth += parseFloat(payrollControllerScope.employeeCompensationEarningList[i].monthly);
						payrollControllerScope.earningYear += parseFloat(payrollControllerScope.employeeCompensationEarningList[i].ytd);
					}
					angular.forEach(payrollControllerScope.allowanceList, function(key){
						if(allowance.allowanceId == key.id){
							flag = key.type;
						}
					});
				}
			} else {
				for (var i = 0; i < payrollControllerScope.allowanceList.length; i++) {
					if (payrollControllerScope.allowanceList[i].id == allowance.allowanceId && payrollControllerScope.allowanceList[i].name.toLowerCase() != 'basic') {
						canEnter = false;
						payrollControllerScope.modalMsg = 'Please ensure basic is selected';
						$('#confirmation').modal('show');
						break;
					}
				}
			}

			if(flag == "" || flag != 'A'){
				if (canEnter) {
					payrollControllerScope.spinner = false;
					payrollService.getAllowanceById(allowance.allowanceId, ctc).then(function (res) {
						if (res.successflag == 'true' && res.results.length > 0) {
							allowance.monthly = res.results[0].monthly;
							allowance.ytd = res.results[0].ytd;							
							payrollControllerScope.earningMonth += parseFloat(res.results[0].monthly);
							payrollControllerScope.earningYear += parseFloat(res.results[0].ytd);
							if(payrollControllerScope.employeeCompensationEarningList.length > 1){
								angular.forEach(payrollControllerScope.employeeCompensationEarningList, function(earning, index){
									angular.forEach(payrollControllerScope.allowanceList, function(key){
										if(earning.allowanceId == key.id && key.type == 'A'){
											var tempMonthly = earning.monthly;
											var tempyearly = earning.ytd;
											payrollControllerScope.earningMonth -= parseFloat(tempMonthly);
											payrollControllerScope.earningYear -= parseFloat(tempyearly);
											var yearly = ctc - payrollControllerScope.earningYear;
											console.log("employeeCompensationEarningList[index]['monthly']",yearly, 'index   ', index);
											payrollControllerScope.employeeCompensationEarningList[index]['monthly'] = yearly/12;
											payrollControllerScope.employeeCompensationEarningList[index]['ytd'] = yearly;
											payrollControllerScope.earningMonth += parseFloat(payrollControllerScope.employeeCompensationEarningList[index]['monthly']);
											payrollControllerScope.earningYear += parseFloat(payrollControllerScope.employeeCompensationEarningList[index]['ytd']);
											console.log("employeeCompensationEarningList====  ",payrollControllerScope.employeeCompensationEarningList);
										}
									});
								});
							}
							payrollControllerScope.spinner = false;
						} else {
							payrollControllerScope.spinner = false;
						}
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
						if(temp > 1){
							payrollControllerScope.modalMsg = 'Allowance is already selected';
							$('#confirmation').modal('show');
							allowance.allowanceId = '';
							allowance.monthly = '';
							allowance.ytd = '';
							$('#confirmation').modal('show');
						}
					}
					payrollControllerScope.spinner = false;
				}
			} else {
				 if ( canEnter) {			
					var yearly = ctc - payrollControllerScope.earningYear;
					allowance.monthly = yearly/12;
					allowance.ytd = yearly;
					payrollControllerScope.earningMonth += parseFloat(allowance.monthly);
					payrollControllerScope.earningYear += parseFloat(allowance.ytd);
					payrollControllerScope.spinner = false;		
				}else {
					if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
						if(temp > 1){
							payrollControllerScope.modalMsg = 'Allowance is already selected';
							$('#confirmation').modal('show');
							allowance.allowanceId = '';
							allowance.monthly = '';
							allowance.ytd = '';
							$('#confirmation').modal('show');
						}
					}
					payrollControllerScope.spinner = false;
				}

			}
		}
	}

	function getAllowanceById(allowance, ctc) {
		if (allowance && ctc) {
			payrollControllerScope.earningMonth = 0;
			payrollControllerScope.earningYear = 0
			payrollControllerScope.hasAllowanceSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			var flag = "";
			var temp = 0;
			for (var i = 0; i < payrollControllerScope.employeeCompensationEarningList.length; i++) {
				if (allowance.allowanceId == payrollControllerScope.employeeCompensationEarningList[i].allowanceId) {
					temp++;
				}
			}
			
			if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationEarningList.length; i++) {
					if (allowance.allowanceId == payrollControllerScope.employeeCompensationEarningList[i].allowanceId && payrollControllerScope.employeeCompensationEarningList[i].monthly != '') {
						allowance.monthly = '';
						allowance.ytd = '';
						canEnter = false;						
					}
					if (payrollControllerScope.employeeCompensationEarningList[i].monthly != '') {						
						payrollControllerScope.earningMonth += parseFloat(payrollControllerScope.employeeCompensationEarningList[i].monthly);
						payrollControllerScope.earningYear += parseFloat(payrollControllerScope.employeeCompensationEarningList[i].ytd);
					}
					angular.forEach(payrollControllerScope.allowanceList, function(key){
						if(allowance.allowanceId == key.id){
							flag = key.type;
						}
					});
				}
			} else {
				for (var i = 0; i < payrollControllerScope.allowanceList.length; i++) {
					if (payrollControllerScope.allowanceList[i].id == allowance.allowanceId && payrollControllerScope.allowanceList[i].name.toLowerCase() != 'basic') {
						canEnter = false;
						payrollControllerScope.modalMsg = 'Please ensure basic is selected';
						$('#confirmation').modal('show');
						break;
					}
				}
			}

			if(flag == "" || flag != 'A'){
				if (canEnter) {
					payrollService.getAllowanceById(allowance.allowanceId, ctc).then(function (res) {
						if (res.successflag == 'true' && res.results.length > 0) {
							allowance.monthly = res.results[0].monthly;
							allowance.ytd = res.results[0].ytd;							
							payrollControllerScope.earningMonth += parseFloat(res.results[0].monthly);
							payrollControllerScope.earningYear += parseFloat(res.results[0].ytd);
							if(payrollControllerScope.employeeCompensationEarningList.length > 1){
								angular.forEach(payrollControllerScope.employeeCompensationEarningList, function(earning, index){
									angular.forEach(payrollControllerScope.allowanceList, function(key){
										if(earning.allowanceId == key.id && key.type == 'A'){
											var tempMonthly = earning.monthly;
											var tempyearly = earning.ytd;
											payrollControllerScope.earningMonth -= parseFloat(tempMonthly);
											payrollControllerScope.earningYear -= parseFloat(tempyearly);
											var yearly = ctc - payrollControllerScope.earningYear;
											console.log("employeeCompensationEarningList[index]['monthly']",yearly, 'index   ', index);
											payrollControllerScope.employeeCompensationEarningList[index]['monthly'] = yearly/12;
											payrollControllerScope.employeeCompensationEarningList[index]['ytd'] = yearly;
											payrollControllerScope.earningMonth += parseFloat(payrollControllerScope.employeeCompensationEarningList[index]['monthly']);
											payrollControllerScope.earningYear += parseFloat(payrollControllerScope.employeeCompensationEarningList[index]['ytd']);
											console.log("employeeCompensationEarningList====  ",payrollControllerScope.employeeCompensationEarningList);
										}
									});
								});
							}
							payrollControllerScope.spinner = false;
						} else {
							payrollControllerScope.spinner = false;
						}
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
						if(temp > 1){
							payrollControllerScope.modalMsg = 'Allowance is already selected';
							$('#confirmation').modal('show');
							allowance.allowanceId = '';
							allowance.monthly = '';
							allowance.ytd = '';
							$('#confirmation').modal('show');
						}
					}
					payrollControllerScope.spinner = false;
				}
			} else {
				 if ( canEnter) {			
					var yearly = ctc - payrollControllerScope.earningYear;
					allowance.monthly = yearly/12;
					allowance.ytd = yearly;
					payrollControllerScope.earningMonth += parseFloat(allowance.monthly);
					payrollControllerScope.earningYear += parseFloat(allowance.ytd);
					payrollControllerScope.spinner = false;		
				}else {
					if (payrollControllerScope.employeeCompensationEarningList.length > 1) {
						if(temp > 1){
							payrollControllerScope.modalMsg = 'Allowance is already selected';
							$('#confirmation').modal('show');
							allowance.allowanceId = '';
							allowance.monthly = '';
							allowance.ytd = '';
							$('#confirmation').modal('show');
						}
					}
					payrollControllerScope.spinner = false;
				}

			}
		}
	}

	function getDeductionByIdCompSettings(deduction, ctc) {
		if (deduction && ctc) {
			payrollControllerScope.deductionMonth = 0;
			payrollControllerScope.deductionYear = 0
			payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			if (payrollControllerScope.employeeCompensationDeductionList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationDeductionList.length; i++) {
					if (deduction.deductionId == payrollControllerScope.employeeCompensationDeductionList[i].deductionId && payrollControllerScope.employeeCompensationDeductionList[i].monthly != '') {
						deduction.monthly = '';
						deduction.ytd = '';
						canEnter = false;
					}
					if (payrollControllerScope.employeeCompensationDeductionList[i].monthly != '') {
						payrollControllerScope.deductionMonth += parseFloat(payrollControllerScope.employeeCompensationDeductionList[i].monthly);
						payrollControllerScope.deductionYear += parseFloat(payrollControllerScope.employeeCompensationDeductionList[i].ytd);
					}
				}
				
			} else if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId == '') {
				canEnter = false;
				payrollControllerScope.modalMsg = 'Please ensure basic is selected in Earning';
				$('#confirmation').modal('show');
			}
			if (canEnter) {
				payrollService.getDeductionById(deduction.deductionId, ctc, payrollControllerScope.employeeCompensationEarningList[0].monthly).then(function (res) {
					console.log('getDeductionById', res)
					if (res.successflag == 'true' && res.results.length > 0) {
						deduction.monthly = res.results[0].monthly;
						deduction.ytd = res.results[0].ytd;
						payrollControllerScope.deductionMonth += parseFloat(res.results[0].monthly);
						payrollControllerScope.deductionYear += parseFloat(res.results[0].ytd);
						payrollControllerScope.spinner = false;
					} else {
						payrollControllerScope.spinner = false;
					}
				}, function (err) {
					payrollControllerScope.spinner = false;
				});
				payrollControllerScope.spinner = false;
			} else {
				console.log('can enter is false')
				if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId != '') {
					payrollControllerScope.modalMsg = 'Deduction is already selected';
					$('#confirmation').modal('show');
				}
				deduction.deductionId = '';
				deduction.ytd = '';
				deduction.monthly = '';
				payrollControllerScope.spinner = false;
			}
		}
	}

	function getDeductionById(deduction, ctc) {
		if (deduction && ctc) {
			payrollControllerScope.deductionMonth = 0;
			payrollControllerScope.deductionYear = 0
			payrollControllerScope.hasDeductionSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			if (payrollControllerScope.employeeCompensationDeductionList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationDeductionList.length; i++) {
					if (deduction.deductionId == payrollControllerScope.employeeCompensationDeductionList[i].deductionId && payrollControllerScope.employeeCompensationDeductionList[i].monthly != '') {
						deduction.monthly = '';
						deduction.ytd = '';
						canEnter = false;
					}
					if (payrollControllerScope.employeeCompensationDeductionList[i].monthly != '') {
						payrollControllerScope.deductionMonth += parseFloat(payrollControllerScope.employeeCompensationDeductionList[i].monthly);
						payrollControllerScope.deductionYear += parseFloat(payrollControllerScope.employeeCompensationDeductionList[i].ytd);
					}
				}
				console.log('enter for')
			} else if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId == '') {
				canEnter = false;
				payrollControllerScope.modalMsg = 'Please ensure basic is selected in Earning';
				$('#confirmation').modal('show');
			}
			if (canEnter) {
				payrollService.getDeductionById(deduction.deductionId, ctc, payrollControllerScope.employeeCompensationEarningList[0].monthly).then(function (res) {
					console.log('getDeductionById', res)
					if (res.successflag == 'true' && res.results.length > 0) {
						deduction.monthly = res.results[0].monthly;
						deduction.ytd = res.results[0].ytd;
						payrollControllerScope.deductionMonth += parseFloat(res.results[0].monthly);
						payrollControllerScope.deductionYear += parseFloat(res.results[0].ytd);
						payrollControllerScope.spinner = false;
					} else {
						payrollControllerScope.spinner = false;
					}
				}, function (err) {
					payrollControllerScope.spinner = false;
				});
			} else {
				console.log('can enter is false')
				if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId != '') {
					payrollControllerScope.modalMsg = 'Deduction is already selected';
					$('#confirmation').modal('show');
				}
				deduction.deductionId = '';
				deduction.ytd = '';
				deduction.monthly = '';
				payrollControllerScope.spinner = false;
			}
		}
	}

	function getTaxByIdCompSettings(tax, ctc) {
		console.log('nathantax', tax);
		console.log('nathancytc', ctc);
		if (tax && ctc) {
			payrollControllerScope.taxMonth = 0;
			payrollControllerScope.taxYear = 0
			payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			if (payrollControllerScope.employeeCompensationTaxList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationTaxList.length; i++) {
					if (tax.taxId == payrollControllerScope.employeeCompensationTaxList[i].taxId && payrollControllerScope.employeeCompensationTaxList[i].monthly != '') {
						tax.monthly = '';
						tax.ytd = '';
						canEnter = false;
					}
					if (payrollControllerScope.employeeCompensationTaxList[i].monthly != '') {
						payrollControllerScope.taxMonth += parseFloat(payrollControllerScope.employeeCompensationTaxList[i].monthly);
						payrollControllerScope.taxYear += parseFloat(payrollControllerScope.employeeCompensationTaxList[i].ytd);
					}
				}
				
			} else if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId == '') {
				canEnter = false;
				payrollControllerScope.modalMsg = 'Please ensure basic is selected in Earning';
				$('#confirmation').modal('show');
			}
			if (canEnter) {
				payrollService.getTaxById(tax.taxId, ctc, payrollControllerScope.employeeCompensationEarningList[0].monthly).then(function (res) {
					console.log('getTaxById', res)
					if (res.successflag == 'true' && res.results.length > 0) {
						tax.monthly = res.results[0].monthly;
						tax.ytd = res.results[0].ytd;
						payrollControllerScope.taxMonth += parseFloat(res.results[0].monthly);
						payrollControllerScope.taxYear += parseFloat(res.results[0].ytd);
						payrollControllerScope.spinner = false;
					} else {
						payrollControllerScope.spinner = false;
					}
				}, function (err) {
					payrollControllerScope.spinner = false;
				});
				payrollControllerScope.spinner = false;
			} else {
				console.log('can enter is false')
				if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId != '') {
					payrollControllerScope.modalMsg = 'tax is already selected';
					$('#confirmation').modal('show');
				}
				tax.taxId = '';
				tax.ytd = '';
				tax.monthly = '';
				payrollControllerScope.spinner = false;
			}
		}
	}

	function getTaxById(tax, ctc) {
		console.log('nathantax', tax);
		console.log('nathancytc', ctc);
		if (tax && ctc) {
			payrollControllerScope.taxMonth = 0;
			payrollControllerScope.taxYear = 0
			payrollControllerScope.hasTaxSelectedAlreadyMsg = '';
			payrollControllerScope.spinner = true;
			var canEnter = true;
			if (payrollControllerScope.employeeCompensationTaxList.length > 1) {
				for (var i = 0; i < payrollControllerScope.employeeCompensationTaxList.length; i++) {
					if (tax.taxId == payrollControllerScope.employeeCompensationTaxList[i].taxId && payrollControllerScope.employeeCompensationTaxList[i].monthly != '') {
						tax.monthly = '';
						tax.ytd = '';
						canEnter = false;
					}
					if (payrollControllerScope.employeeCompensationTaxList[i].monthly != '') {
						payrollControllerScope.taxMonth += parseFloat(payrollControllerScope.employeeCompensationTaxList[i].monthly);
						payrollControllerScope.taxYear += parseFloat(payrollControllerScope.employeeCompensationTaxList[i].ytd);
					}
				}
				console.log('enter tax')
			} else if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId == '') {
				canEnter = false;
				payrollControllerScope.modalMsg = 'Please ensure basic is selected in Earning';
				$('#confirmation').modal('show');
			}
			if (canEnter) {
				payrollService.getTaxById(tax.taxId, ctc, payrollControllerScope.employeeCompensationEarningList[0].monthly).then(function (res) {
					console.log('getTaxById', res)
					if (res.successflag == 'true' && res.results.length > 0) {
						tax.monthly = res.results[0].monthly;
						tax.ytd = res.results[0].ytd;
						payrollControllerScope.taxMonth += parseFloat(res.results[0].monthly);
						payrollControllerScope.taxYear += parseFloat(res.results[0].ytd);
						payrollControllerScope.spinner = false;
					} else {
						payrollControllerScope.spinner = false;
					}
				}, function (err) {
					payrollControllerScope.spinner = false;
				});
			} else {
				console.log('can enter is false')
				if (payrollControllerScope.employeeCompensationEarningList[0].allowanceId != '') {
					payrollControllerScope.modalMsg = 'tax is already selected';
					$('#confirmation').modal('show');
				}
				tax.taxId = '';
				tax.ytd = '';
				tax.monthly = '';
				payrollControllerScope.spinner = false;
			}
		}
	}

	function getAllowanceType() {
		payrollControllerScope.getSpinner = true;
		configurationService.getEarningsType().then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.allowanceTypeList = res.results;
				payrollControllerScope.getSpinner = false;
			} else {
				payrollControllerScope.getSpinner = false;
			}
		}, function (err) {
			payrollControllerScope.getSpinner = false;
		});
	}

	function getDeduction() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.deductionMsg = '';
		payrollService.getDeduction().then(function (res) {
			console.log('getDeduction res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.deductionList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.deductionMsg = "Deduction Not Available";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.deductionMsg = "Deduction Not Available";
			payrollControllerScope.spinner = false;
		});
	}

	/*function getTax() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.taxMsg = '';
		payrollService.getTax().then(function (res) {
			console.log('getTax===res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.taxList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.taxMsg = "tax Not Available";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.taxMsg = "tax Not Available";
			payrollControllerScope.spinner = false;
		});
	}*/

	function getDeductionType() {
		payrollControllerScope.spinner = true;
		configurationService.getDeductionType().then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.deductionTypeList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}

	function getTaxType() {
		payrollControllerScope.spinner = true;
		configurationService.getTaxType().then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.taxTypeList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}


	function getAllowance() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.allowanceMsg = '';
		payrollService.getAllowance().then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.allowanceList = res.results;
				console.log('payrollControllerScope.allowanceList', payrollControllerScope.allowanceList);
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
				payrollControllerScope.allowanceMsg = "Earnings Not Available";
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}

	function getEarningsType(data) {
		payrollControllerScope.spinner = true;
		configurationService.getEarningsType(data).then(function (res) {
			console.log("get allowance", res)
			if (res.successflag == 'true') {
				payrollControllerScope.allowanceTypeList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}

	function getMonthInDigit(monthString){
		var monthInDigit;
		if(monthString == 'Jan'){
			monthInDigit = 0;
		}else if(monthString == 'Feb'){
			monthInDigit = 1;
		}else if(monthString == 'Mar'){
			monthInDigit = 2;
		}else if(monthString == 'Apr'){
			monthInDigit = 3;
		}else if(monthString == 'May'){
			monthInDigit = 4;
		}else if(monthString == 'Jun'){
			monthInDigit = 5;
		}else if(monthString == 'Jul'){
			monthInDigit = 6;
		}
		else if(monthString == 'Aug'){
			monthInDigit = 7;
		}else if(monthString == 'Sep'){
			monthInDigit = 8;
		}else if(monthString == 'Oct'){
			monthInDigit = 9;
		}else if(monthString == 'Nov'){
			monthInDigit = 10;
		}else{
			monthInDigit = 11;
		}
		return monthInDigit;
	}
	function getRunPayroll(data) { console.log('getRunPayroll', data);
		payrollControllerScope.spinner = true;
		payrollControllerScope.isRunPayroll = false;
		payrollControllerScope.showBack = true;
		angular.forEach(payrollControllerScope.batchActiveList,function(piece,index){
			if(piece.id == data.payrollBatchId){
				payrollControllerScope.batchName = piece.name;
			}
		})
		var monthInDigit = getMonthInDigit(payrollControllerScope.runPayroll.payPeriod.substring(0,3));
		var dt = new Date(payrollControllerScope.runPayroll.payPeriod.substring(3),monthInDigit);
        var month = dt.getMonth(),
            year = dt.getFullYear();
        var FirstDay = new Date(year, month, 1);
        var LastDay = new Date(year, month + 1, 0);
		payrollControllerScope.payPeriodFrom = FirstDay;
		payrollControllerScope.payPeriodTo = LastDay;
		/*payrollService.getRunPayroll(data).then(function (res) {
			console.log('getRunPayroll res', res);
			if (res.successflag == 'true') {
				payrollControllerScope.runPayrollList = res.results;
				payrollControllerScope.isRunPayroll = true;
			}
			payrollControllerScope.runPayrollListBackup = angular.copy(res.results);
            payrollControllerScope.isDataAvailable = true;

        	for(var i = 0; i < payrollControllerScope.runPayrollListBackup.length; i++){
            	payrollControllerScope.runPayrollListBackup[i].isActiveClass = false;
            	payrollControllerScope.runPayrollListBackup[i].isSelect = false;
        	}
			payrollControllerScope.payrollList =  angular.copy(payrollControllerScope.runPayrollListBackup);
        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollList.length)
        		payrollControllerScope.isNextDisabled = true;
        	if(payrollControllerScope.payrollList.length > 0){
			payrollControllerScope.payrollList[0].isActiveClass = true;
		}
			payrollControllerScope.spinner = false;
		}, function (err) {
			payrollControllerScope.spinner = false;
		});*/
		payrollService.getRunPayrollWithDate(data).then(function (res) {
			console.log('getRunPayroll res', res);
			if (res.successflag == 'true') {
				payrollControllerScope.runPayrollList = res.results;
				payrollControllerScope.isRunPayroll = true;
			}
			payrollControllerScope.runPayrollListBackup = angular.copy(res.results);
            payrollControllerScope.isDataAvailable = true;

        	for(var i = 0; i < payrollControllerScope.runPayrollListBackup.length; i++){
            	payrollControllerScope.runPayrollListBackup[i].isActiveClass = false;
            	payrollControllerScope.runPayrollListBackup[i].isSelect = false;
            	if(payrollControllerScope.runPayrollListBackup[i].payrollStatus !== 'p'){
            		payrollControllerScope.payrollStatus = true;
            	} else {
            		payrollControllerScope.payrollStatus = false;
            	}
        	}
			payrollControllerScope.payrollList =  angular.copy(payrollControllerScope.runPayrollListBackup);
        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollList.length)
        		payrollControllerScope.isNextDisabled = true;
        	if(payrollControllerScope.payrollList.length > 0){
			payrollControllerScope.payrollList[0].isActiveClass = true;
		}
			payrollControllerScope.spinner = false;
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}
	
	function getRunPayrollMonthly(month, status, batchId, payrolltype){
		if(month && status){
			payrollControllerScope.spinner = true;
			payrollControllerScope.month = month;
			payrollService.getRunPayrollMonthly({monthly:month, payrollBatchId:batchId}, status).then(function(res){
				payrollControllerScope.spinner = false;
				if(res.successflag == 'true' && res.results.length > 0){ 
					payrollControllerScope.batchId = batchId;
					payrollControllerScope.payrolltype = payrolltype;
					delete $localStorage.spheresuite['runPayrollDate'];
					delete $localStorage.spheresuite['payrollBatchId'];
					delete $localStorage.spheresuite['payrollType'];
					payrollControllerScope.runPayrollDetailList = res.results;
					payrollControllerScope.batchName = payrollControllerScope.runPayrollDetailList[0].payrollBatchName;
					payrollControllerScope.runPayrollDetailListBackup = angular.copy(res.results);
		            payrollControllerScope.isDataAvailable = true;
		            var monthInDigit = getMonthInDigit(payrollControllerScope.month.substring(0,3));
		    		var dt = new Date(payrollControllerScope.month.substring(3),monthInDigit);
		            var month = dt.getMonth(),
		                year = dt.getFullYear();
		            var FirstDay = new Date(year, month, 1);
		            var LastDay = new Date(year, month + 1, 0);
		    		payrollControllerScope.payPeriodFrom = FirstDay;
		    		payrollControllerScope.payPeriodTo = LastDay;
		        	for(var i = 0; i < payrollControllerScope.runPayrollDetailListBackup.length; i++){
		            	payrollControllerScope.runPayrollDetailListBackup[i].isActiveClass = false;
		            	payrollControllerScope.runPayrollDetailListBackup[i].isSelect = false;
		        	}
					payrollControllerScope.payrollList =  angular.copy(payrollControllerScope.runPayrollDetailListBackup);
		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollList.length)
		        		payrollControllerScope.isNextDisabled = true;
					payrollControllerScope.payrollList[0].isActiveClass = true;
//					$location.path('/payroll/run');
				} else {
					payrollControllerScope.dataMsg = 'Payroll processed already for this month';
				}
			}, function(err){
				payrollControllerScope.spinner = false;
				payrollControllerScope.dataMsg = 'Payroll processed already for this month';
			});
		}
	}
	
	function gotoRunPayroll(data){
		if(data)
		$location.path('/payroll/run');
		payrollControllerScope.isPayrollDetail = !payrollControllerScope.isPayrollDetail;
		getRunPayroll(data);
	}

	function openAllowanceModal(allowance) {
		if (allowance) {
			payrollControllerScope.allowance = angular.copy(allowance);
			payrollControllerScope.isEdit = true;
		}
	}

	function openDeductionModal(deduction) {
		if (deduction) {
			payrollControllerScope.deduction = angular.copy(deduction);
			payrollControllerScope.isEdit = true;
		}
	}

	function openTaxModal(taxValue) {
		if (taxValue) {
			payrollControllerScope.tax = angular.copy(taxValue);
			payrollControllerScope.isEdit = true;
		}
	}

	function openEffectiveFromCalender($event) {
		$event.preventDefault();
		$event.stopPropagation();
		payrollControllerScope.isEffectiveFrom = !payrollControllerScope.isEffectiveFrom;
	}

	function openEffectiveToCalender($event) {
		$event.preventDefault();
		$event.stopPropagation();
		payrollControllerScope.isEffectiveTo = !payrollControllerScope.isEffectiveTo;
	}

	function processPayroll() {
		if (payrollControllerScope.runPayroll.payPeriod && payrollControllerScope.runPayrollList && payrollControllerScope.runPayroll.payrollBatchId) {
			payrollControllerScope.spinner = true;
			var payrollList = [];
			for (var i = 0; i < payrollControllerScope.runPayrollList.length; i++) {
				//if((payrollControllerScope.runPayrollList[i].payrollStatus == 'n' && payrollControllerScope.runPayrollList[i].isChecked != false) || ( payrollControllerScope.runPayrollList[i].payrollStatus == 'v' && payrollControllerScope.runPayrollList[i].isChecked != false) || payrollControllerScope.runPayrollList[i].payrollStatus == 'd'){
					console.log('enteringpayroll');
					payrollList.push({
						id: payrollControllerScope.runPayrollList[i].id,
						empId: payrollControllerScope.runPayrollList[i].empId,
						empName: payrollControllerScope.runPayrollList[i].name,
						basicGrossPay: payrollControllerScope.runPayrollList[i].basicGrossPay,
						earningMonth: payrollControllerScope.runPayrollList[i].earningMonth,
						deductionMonth: payrollControllerScope.runPayrollList[i].deductionMonth,
						netMonth: payrollControllerScope.runPayrollList[i].netMonth,
						taxMonth: payrollControllerScope.runPayrollList[i].taxMonth
					});
				//}
			}console.log('data123',payrollList);
			var data = { payrollMonth: payrollControllerScope.runPayroll.payPeriod, payrollList: payrollList, updatedBy: $localStorage.spheresuite.id, payrollBatchId: payrollControllerScope.runPayroll.payrollBatchId, payrollType: ''};
			
			payrollService.processPayroll(data).then(function (res) {
				console.log('process payroll111 ', res)
				if (res.successflag == 'true' && res.results.length != 0) {
					console.log("storing now");
					$localStorage.spheresuite.runPayrollDate = payrollControllerScope.runPayroll.payPeriod; 
					$localStorage.spheresuite.payrollBatchId = payrollControllerScope.runPayroll.payrollBatchId;
					$localStorage.spheresuite.payrollType = '';
					payrollControllerScope.runPayrollDetailListBackup = angular.copy(payrollList);
		            payrollControllerScope.isDataAvailable = true;

		        	for(var i = 0; i < payrollControllerScope.runPayrollDetailListBackup.length; i++){
		            	payrollControllerScope.runPayrollDetailListBackup[i].isActiveClass = false;
		            	payrollControllerScope.runPayrollDetailListBackup[i].isSelect = false;
		        	}
					payrollControllerScope.payrollList =  angular.copy(payrollControllerScope.runPayrollDetailListBackup);
		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollList.length)
		        		payrollControllerScope.isNextDisabled = true;
					payrollControllerScope.payrollList[0].isActiveClass = true;
					$location.path('/payroll/detail');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			})
		}
	}

	function resetDeductionAmount() {
		if (payrollControllerScope.deduction.type == 'P') {
			payrollControllerScope.deduction.basicGrossPay = '';
			payrollControllerScope.deduction.fixedAmount = '0';
			payrollControllerScope.deduction.percentageAmount = '';
		} else if (payrollControllerScope.deduction.type == 'F') {
			payrollControllerScope.deduction.basicGrossPay = 'G';
			payrollControllerScope.deduction.fixedAmount = '';
			payrollControllerScope.deduction.percentageAmount = '0';
		}
	}

	function resetTaxAmount() {
		if (payrollControllerScope.tax.type == 'P') {
			payrollControllerScope.tax.basicGrossPay = '';
			payrollControllerScope.tax.fixedAmount = '0';
			payrollControllerScope.tax.percentageAmount = '';
		} else if (payrollControllerScope.tax.type == 'F') {
			payrollControllerScope.tax.basicGrossPay = 'G';
			payrollControllerScope.tax.fixedAmount = '';
			payrollControllerScope.tax.percentageAmount = '0';
		}
	}


	function resetAllowanceAmount() {
		if (payrollControllerScope.allowance.type == 'P') {
			payrollControllerScope.allowance.basicGrossPay = '';
			payrollControllerScope.allowance.fixedAmount = '0';
			payrollControllerScope.allowance.percentageAmount = '';
		} else if (payrollControllerScope.allowance.type == 'F') {
			payrollControllerScope.allowance.basicGrossPay = 'G';
			payrollControllerScope.allowance.fixedAmount = '';
			payrollControllerScope.allowance.percentageAmount = '0';
		} else {
			payrollControllerScope.allowance.basicGrossPay = '';
			payrollControllerScope.allowance.fixedAmount = '0';
			payrollControllerScope.allowance.percentageAmount = '';
		}
	}
	
	function submitPayroll(){
		if(payrollControllerScope.runPayrollDetailList){
			payrollControllerScope.spinner = true;
			var ids = [];
			for(var i = 0; i < payrollControllerScope.runPayrollDetailList.length; i++){
				ids.push({id:payrollControllerScope.runPayrollDetailList[i].id});
			}
			if(ids.length != 0){
				payrollService.submitPayroll({updatedBy: $localStorage.spheresuite.id, ids: ids}).then(function(res){
					console.log('submitPayroll res',res);
					if(res.successflag == 'true'){
						getRunPayrollMonthly(payrollControllerScope.month, 'status', payrollControllerScope.batchId, payrollControllerScope.payrolltype);
					}
					payrollControllerScope.spinner = false;
				},function(err){
					console.log('err',err)
					payrollControllerScope.spinner = false;
				})
			}else{
				payrollControllerScope.spinner = true;
			}
		}
	}

	function updateEmployeeCompensation() {
		payrollControllerScope.spinner = true;
		if (payrollControllerScope.employeeCompensationEarningList[payrollControllerScope.employeeCompensationEarningList.length - 1].allowanceId == '')
			payrollControllerScope.employeeCompensationEarningList.splice(payrollControllerScope.employeeCompensationEarningList.length - 1, 1)
		if (payrollControllerScope.employeeCompensationDeductionList[payrollControllerScope.employeeCompensationDeductionList.length - 1].deductionId == '')
			payrollControllerScope.employeeCompensationDeductionList.splice(payrollControllerScope.employeeCompensationDeductionList.length - 1, 1)
		if (payrollControllerScope.employeeCompensationTaxList[payrollControllerScope.employeeCompensationTaxList.length - 1].taxId == '')
			payrollControllerScope.employeeCompensationTaxList.splice(payrollControllerScope.employeeCompensationTaxList.length - 1, 1)
		if (!payrollControllerScope.leadListByEmp.effectiveTo) {
			payrollControllerScope.leadListByEmp.effectiveTo = '';
		}
		else {
			payrollControllerScope.leadListByEmp.effectiveTo = moment(payrollControllerScope.leadListByEmp.effectiveTo).format('MMM DD YYYY')
		}
		var data = {
			basicGrossPay: Number(payrollControllerScope.leadListByEmp.empctc) / 12,
			empId: payrollControllerScope.allowance.empId,
			effectiveFrom: moment(payrollControllerScope.leadListByEmp.effectiveFrom).format('MMM DD YYYY'),
			effectiveTo: payrollControllerScope.leadListByEmp.effectiveTo,
			employeeCompensationEarningList: payrollControllerScope.employeeCompensationEarningList,
			employeeCompensationDeductionList: payrollControllerScope.employeeCompensationDeductionList,
			employeeCompensationTaxList: payrollControllerScope.employeeCompensationTaxList,
			earningMonth: payrollControllerScope.earningMonth,
			earningYear: payrollControllerScope.earningYear,
			deductionMonth: payrollControllerScope.deductionMonth,
			deductionYear: payrollControllerScope.deductionYear,
			taxMonth: payrollControllerScope.taxMonth,
			taxYear: payrollControllerScope.taxYear,
			netMonth: payrollControllerScope.earningMonth - (payrollControllerScope.deductionMonth + payrollControllerScope.taxMonth),
			netYear: payrollControllerScope.earningYear - (payrollControllerScope.deductionYear + payrollControllerScope.taxYear),
			updatedBy: $localStorage.spheresuite.id
		}
		console.log('data', data)
		if (data) {
			payrollService.updateEmployeeCompensation(data).then(function (res) {
				console.log('res updateEmployeeCompensation', res);
				if (res.successflag == 'true') {
					empCompShowData();
					payrollControllerScope.spinner = false;
					payrollControllerScope.allowance = { empId: '' };
					payrollControllerScope.leadListByEmp = null;
					payrollControllerScope.employeeCompensationEarningList = null;
					payrollControllerScope.employeeCompensationDeductionList = null;
					payrollControllerScope.employeeCompensationTaxList = null;
					payrollControllerScope.earningMonth = 0;
					payrollControllerScope.earningYear = 0;
					payrollControllerScope.deductionMonth = 0;
					payrollControllerScope.deductionYear = 0;
					payrollControllerScope.taxMonth = 0;
					payrollControllerScope.taxYear = 0;
					payrollControllerScope.earning = false;
					$('#updateCompensation').modal('hide');
					payrollControllerScope.modalMsg = "Compensation updated successfully";
					$('#confirmation').modal('show');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			})
		} else {
			payrollControllerScope.spinner = false;
		}

	}

	function viewPayrollDetail(empId) {
		console.log('1');
		if (empId) {
			//getEmpId(empId);
			getPayrollDetails(empId);
			payrollControllerScope.isPayrollDetail = true;

		}
	}
	
	function getPayrollDetails(data) {
		console.log("((((((((((((((((((((((((((((((((((",data)
		payrollControllerScope.spinner = true;
		payrollService.getPayrollDetails(data).then(function (res) {
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.payrollDetails = res.results[0];
				console.log("testet",payrollControllerScope.payrollDetails);
				payrollControllerScope.empBonusShow = payrollControllerScope.payrollDetails.bonusMonth;


				var netEarningMonth =  payrollControllerScope.payrollDetails.earningMonth;
				if(payrollControllerScope.empBonusShow ){
					console.log('payrollControllerScope.empBonusShow', payrollControllerScope.empBonusShow);
					netEarningMonth = parseFloat(netEarningMonth) + parseFloat(payrollControllerScope.empBonusShow);
					console.log('netEarningMonth', netEarningMonth);
				}

 				payrollControllerScope.payrollDetails.earningMonth = netEarningMonth;

				var netEarningYear =  payrollControllerScope.payrollDetails.earningYear;
				
				var netDeductionMonth =  payrollControllerScope.payrollDetails.deductionMonth;
				var netDeductionYear =  payrollControllerScope.payrollDetails.deductionYear;
				
				var netTaxMonth =  payrollControllerScope.payrollDetails.taxMonth;
				var netTaxYear =  payrollControllerScope.payrollDetails.taxYear;

				
				
				payrollControllerScope.netTotalMonth = parseFloat(netEarningMonth) - (parseFloat(netDeductionMonth) + parseFloat(netTaxMonth));
				payrollControllerScope.netTotalYear = parseFloat(netEarningYear) - (parseFloat(netDeductionYear) + parseFloat(netTaxYear));
				payrollControllerScope.spinner = false;

				
			} else {
				payrollControllerScope.payrollDetails = [];
				payrollControllerScope.ctcMsg = "Compensation Details Not Available";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}

	function editInCompensation(empId){
		console.log('editIncompensation', empId);
		$location.path('/employee/compensation');
		$localStorage.spheresuite.usrId = empId;
	}

	function showPrevNav(){
		payrollControllerScope.buttonBeginFrom -= payrollControllerScope.buttonLimitToShow;
    	gotoPage(payrollControllerScope.buttonBeginFrom)
		payrollControllerScope.isNextDisabled = false;
	}
	
	function showNextNav(){
		payrollControllerScope.buttonBeginFrom += payrollControllerScope.buttonLimitToShow;
		gotoPage(payrollControllerScope.buttonBeginFrom)
		if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollList.length
			 || (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.payrollsummary.length
			 || (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeLopTypeList.length
			 || (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeBonusTypeList.length
			 || (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.empCompensationShowDataList.length
			|| (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.empItSavingsList.length
			|| (payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeItSavingsList.length)
			payrollControllerScope.isNextDisabled = true;
	}

	function gotoPage(index, activeIndex){
    	$rootScope.beginFrom = index;
    	if(payrollControllerScope.payrollList.length > 0){
	    	for(var i = 0; i < payrollControllerScope.payrollList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.payrollList[i].isActiveClass);
	        	payrollControllerScope.payrollList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.payrollList[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.payrollsummary.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.payrollsummary.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.payrollsummary[i].isActiveClass);
	        	payrollControllerScope.payrollsummary[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.payrollsummary[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.getEmployeeLopTypeList.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.getEmployeeLopTypeList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.getEmployeeLopTypeList[i].isActiveClass);
	        	payrollControllerScope.getEmployeeLopTypeList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.getEmployeeLopTypeList[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.getEmployeeBonusTypeList.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.getEmployeeBonusTypeList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.getEmployeeBonusTypeList[i].isActiveClass);
	        	payrollControllerScope.getEmployeeBonusTypeList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.getEmployeeBonusTypeList[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.empCompensationShowDataList.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.empCompensationShowDataList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.empCompensationShowDataList[i].isActiveClass);
	        	payrollControllerScope.empCompensationShowDataList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.empCompensationShowDataList[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.empItSavingsList.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.empItSavingsList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.empItSavingsList[i].isActiveClass);
	        	payrollControllerScope.empItSavingsList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.empItSavingsList[activeIndex].isActiveClass = true;
	    } else if(payrollControllerScope.getEmployeeItSavingsList.length > 0) {
	    	for(var i = 0; i < payrollControllerScope.getEmployeeItSavingsList.length; i++){
	        	console.log(' data[i].isActiveClass', payrollControllerScope.getEmployeeItSavingsList[i].isActiveClass);
	        	payrollControllerScope.getEmployeeItSavingsList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payrollControllerScope.getEmployeeItSavingsList[activeIndex].isActiveClass = true;
	    }

    }

    function addEmployeeLop(){
    	if (payrollControllerScope.addLop) {
			payrollControllerScope.spinner = true;
					payrollControllerScope.addLop.updatedBy = $localStorage.spheresuite.id;
					console.log('payrollControllerScope.addLop', payrollControllerScope.addLop)
					payrollService.addEmployeeLop(payrollControllerScope.addLop).then(function (res) {
						console.log('addAllowance res', res)
						if (res.successflag == 'true') {
							payrollControllerScope.addLop = [];
							getEmployeeLop();							
							$('#employeeLOP').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} 
		}

	function getEmployeeLop(){
		payrollControllerScope.spinner = true;
		payrollControllerScope.isDataAvailable = true;
		payrollControllerScope.getEmployeeLopTypeList =[];
		payrollService.getEmployeeLop().then(function (res) {
			console.log("getEmployeeLop", res)
			if (res.successflag == 'true') {
				payrollControllerScope.getEmployeeLopTypeList = res.results;
				console.log('payrollControllerScope.getEmployeeLopTypeList', payrollControllerScope.getEmployeeLopTypeList);
				payrollControllerScope.spinner = false;
				//payrollControllerScope.isDataAvailable = true;
				payrollControllerScope.empLopListBackup = angular.copy(payrollControllerScope.getEmployeeLopTypeList);
	            payrollControllerScope.isDataAvailable = true;

	        	for(var i = 0; i < payrollControllerScope.empLopListBackup.length; i++){
	            	payrollControllerScope.empLopListBackup[i].isActiveClass = false;
	            	payrollControllerScope.empLopListBackup[i].isSelect = false;
	        	}
				payrollControllerScope.getEmployeeLopTypeList =  angular.copy(payrollControllerScope.empLopListBackup);
	        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeLopTypeList.length)
	        		payrollControllerScope.isNextDisabled = true;
				payrollControllerScope.getEmployeeLopTypeList[0].isActiveClass = true;
			} else {
				payrollControllerScope.isDataAvailable = false;
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
	}

	function gotoUpdateEmployeeLopDetails(data){
		getActiveUser();
        payrollControllerScope.addLop = data;        
        payrollControllerScope.saveUpdate = true;
        payrollControllerScope.saveShow = false;
        $('#employeeLOP').modal('show');
    }  

    function gotoAddEmployeeLopDetails(){
        payrollControllerScope.addLop = undefined;
        console.log('addLOP', payrollControllerScope.addLop);
        payrollControllerScope.saveUpdate = false;
        payrollControllerScope.saveShow = true;
    }  

    function updateEmployeeLopDetails(){ console.log('hhhh');
    	payrollControllerScope.spinner = true;
        payrollControllerScope.addLop.updatedBy= $localStorage.spheresuite.id;
        payrollService.updateEmployeeLopDetails(payrollControllerScope.addLop).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmployeeLop()
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
        $('#employeeLOP').modal('hide');
    }

    function cancelEmployeeLopDetails(data){
    	payrollControllerScope.spinner = true;
        data.updatedBy= $localStorage.spheresuite.id;
        payrollService.cancelEmployeeLopDetails(data).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmployeeLop()
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
    }

    function addEmpBonusDetails(){
    	if (payrollControllerScope.addEmpBonus) {
			payrollControllerScope.spinner = true;
					payrollControllerScope.addEmpBonus.updatedBy = $localStorage.spheresuite.id;
					console.log('payrollControllerScope.addLop', payrollControllerScope.addEmpBonus)
					if(payrollControllerScope.addEmpBonus.comment == undefined){
						payrollControllerScope.addEmpBonus.comment = '';
					}
					payrollService.addEmpBonusDetails(payrollControllerScope.addEmpBonus).then(function (res) {
						console.log('addEmpBonus res', res)
						if (res.successflag == 'true') {
							payrollControllerScope.addEmpBonus = [];
							getEmployeeBonusDetails();							
							$('#employeeBonus').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} 
    }

    function getEmployeeBonusDetails(){
    	payrollControllerScope.spinner = true;
		payrollService.getEmployeeBonusDetails().then(function (res) {
			if (res.successflag == 'true') {
				console.log("getEmployeeBonus", res.results.length);
				payrollControllerScope.getEmployeeBonusTypeList = res.results;
				payrollControllerScope.isBonusAvailable = true;
				payrollControllerScope.spinner = false;
				payrollControllerScope.empBonusListBackup = angular.copy(payrollControllerScope.getEmployeeBonusTypeList);
	            payrollControllerScope.isDataAvailable = true;

	        	for(var i = 0; i < payrollControllerScope.empBonusListBackup.length; i++){
	            	payrollControllerScope.empBonusListBackup[i].isActiveClass = false;
	            	payrollControllerScope.empBonusListBackup[i].isSelect = false;
	        	}
				payrollControllerScope.getEmployeeBonusTypeList =  angular.copy(payrollControllerScope.empBonusListBackup);
	        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeBonusTypeList.length)
	        		payrollControllerScope.isNextDisabled = true;
				payrollControllerScope.getEmployeeBonusTypeList[0].isActiveClass = true;
			} else {
				payrollControllerScope.spinner = false;
				payrollControllerScope.isDataAvailable = false;
				payrollControllerScope.isBonusAvailable = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});
		
    }

    function gotoEditEmpBonus(data){
		getActiveUser();
        payrollControllerScope.addEmpBonus = data;        
        payrollControllerScope.saveUpdate = true;
        payrollControllerScope.saveShow = false;
        $('#employeeBonus').modal('show');
    }  

    function gotoAddEmployeeBonusDetails(){
        payrollControllerScope.addEmpBonus = undefined;
        console.log('addEmpBonus', payrollControllerScope.addEmpBonus);
        payrollControllerScope.saveUpdate = false;
        payrollControllerScope.saveShow = true;
    }  

    function updateEmpBonusDetails(){
    	payrollControllerScope.spinner = true;
        payrollControllerScope.addEmpBonus.updatedBy= $localStorage.spheresuite.id;
        payrollService.updateEmployeeBonusDetails(payrollControllerScope.addEmpBonus).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmployeeBonusDetails()
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
        $('#employeeBonus').modal('hide');
    }

    function cancelEmployeeBonusDetails(data){
    	payrollControllerScope.spinner = true;
        data.updatedBy= $localStorage.spheresuite.id;
        payrollService.cancelEmployeeBonusDetails(data).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmployeeBonusDetails()
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
    }

    function declineEmpLop(){
    	 payrollControllerScope.addLop = null;
    }

    function declineEmpBonusDetails(){
    	payrollControllerScope.addEmpBonus = null;
    }

    function addEmployeeTax() {
		if (payrollControllerScope.tax) {
			payrollControllerScope.spinner = true;
			commonService.formValNotManditory(payrollControllerScope.taxForm, payrollControllerScope.tax).then(function (data) {
				if (data) {
					data.updatedBy = $localStorage.spheresuite.id;
					payrollService.addEmpTax(data).then(function (res) {
						if (res.successflag == 'true') {
							declineTax();
							getTax();
							$('#taxModal').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					payrollControllerScope.spinner = false;
				}
			});
		}
	}

	function getTax() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.taxMsg = '';
		payrollService.getTax().then(function (res) {
			console.log('getTax res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.taxList = res.results;
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.taxMsg = "Tax Not Available";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.taxMsg = "Tax Not Available";
			payrollControllerScope.spinner = false;
		});
	}

	function editTax() {
		if (payrollControllerScope.tax) {
			if(payrollControllerScope.tax.enddate == undefined){
				payrollControllerScope.tax.enddate = '';
			}
			payrollControllerScope.spinner = true;
			payrollControllerScope.tax.updatedBy = $localStorage.spheresuite.id;
			payrollService.editTax(payrollControllerScope.tax).then(function (res) {
				if (res.successflag == 'true') {
					declineTax();
					getTax();
					$('#taxModal').modal('hide');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			});
		}
	}

	function empCompShowData() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.isDataAvailable = false;
		payrollControllerScope.taxMsg = '';
		payrollService.empCompShowData().then(function (res) {
			console.log('getTax res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.isDataAvailable = true;
				payrollControllerScope.empCompensationShowDataList = res.results;
				
				console.log('payrollControllerScope.empCompensationShowDataList   ', payrollControllerScope.empCompensationShowDataList )
    			payrollControllerScope.empCompensationShowDataListBackup = angular.copy(payrollControllerScope.empCompensationShowDataList);
	            payrollControllerScope.showEmpCompValue = false;
	            payrollControllerScope.isDataAvailable = true;

		        	for(var i = 0; i < payrollControllerScope.empCompensationShowDataListBackup.length; i++){
		            	payrollControllerScope.empCompensationShowDataListBackup[i].isActiveClass = false;
		            	payrollControllerScope.empCompensationShowDataListBackup[i].isSelect = false;
		        	}
					payrollControllerScope.empCompensationShowDataList =  angular.copy(payrollControllerScope.empCompensationShowDataListBackup);
		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.empCompensationShowDataList.length)
		        		payrollControllerScope.isNextDisabled = true;
					payrollControllerScope.empCompensationShowDataList[0].isActiveClass = true;
					console.log('payrollControllerScope.empCompensationShowDataList 00000 ',payrollControllerScope.empCompensationShowDataList )
			} else {
				payrollControllerScope.spinner = false;
				payrollControllerScope.showEmpCompValue = true;
			}
		}, function (err) {
			payrollControllerScope.empShowData = "Data Not Available";
			payrollControllerScope.spinner = false;
			payrollControllerScope.showEmpCompValue = true;
		});
	}


	function changeDiv(selectedDiv) {
        if (selectedDiv == 'employeeComp') {
        	payrollControllerScope.divValue = 'employeeComp';
        } else if (selectedDiv == 'compSettings') {
	
        	payrollControllerScope.divValue = 'compSettings';
        }
    
	}

	payrollControllerScope.fromEditFile = [];
	function getItSavings(data) {
		payrollControllerScope.spinner = true;
		payrollControllerScope.fromDate = (new Date()).getFullYear();
		payrollControllerScope.toDate = parseFloat(payrollControllerScope.fromDate)+1 ;
		getItSavingsDate(payrollControllerScope.fromDate, payrollControllerScope.toDate);
		payrollService.getItSavings(data).then(function(res) {
            if (res.successflag === 'true') {
                 payrollControllerScope.empItSavingsList = res.results; 	
                 if(payrollControllerScope.empItSavingsList.length > 0) {                 
	                 payrollControllerScope.empItSavingsListBackup = angular.copy(payrollControllerScope.empItSavingsList);
	 	             payrollControllerScope.isDataAvailable = true;
	 		        	for(var i = 0; i < payrollControllerScope.empItSavingsListBackup.length; i++){
	 		            	payrollControllerScope.empItSavingsListBackup[i].isActiveClass = false;
	 		            	payrollControllerScope.empItSavingsListBackup[i].isSelect = false;
	 		        	}
	 					payrollControllerScope.empItSavingsList =  angular.copy(payrollControllerScope.empItSavingsListBackup);
	 					console.log('payrollControllerScope.empItSavingsList    ',  payrollControllerScope.empItSavingsList);
	 		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.empItSavingsList.length)
	 		        		payrollControllerScope.isNextDisabled = true;
	 					payrollControllerScope.empItSavingsList[0].isActiveClass = true;
 				  }

 	             payrollControllerScope.spinner = false;
            }else {
            	payrollControllerScope.spinner = false;
            }
        }, function (err) {
			payrollControllerScope.spinner = false;
		}); 
    }
	
	function getItSavingsDate(fromDate, toDate) {
		payrollControllerScope.spinner = true;		
			payrollService.getItSavingsByDate(fromDate, toDate).then(function(res) {
	            console.log('getItSavings1111  ', res);
	            if (res.successflag === 'true') {
	                 payrollControllerScope.itSavingsList = res.results; 
	                 payrollControllerScope.spinner = false;
	            }
	        });		
    }
	
	function getSavingsData(data) {
		console.log('dataaaaaa  ',data)
		data =  JSON.parse(data);
		payrollControllerScope.myItSavings.section = data.section;
		payrollControllerScope.myItSavings.maxLimit = data.maxLimit;
    }
	
	var filesName = [];
	function addmyItSavings() {
		if (payrollControllerScope.myItSavings) {
			payrollControllerScope.myItSavings.toDate = payrollControllerScope.toDate;
			payrollControllerScope.myItSavings.fromDate = payrollControllerScope.fromDate;
			payrollControllerScope.spinner = true;
			commonService.formValNotManditory(payrollControllerScope.myItSavingsForm, payrollControllerScope.myItSavings).then(function (data) {
				if (data) {
					data.updatedBy = $localStorage.spheresuite.id;
					data.empId = $localStorage.spheresuite.id;
					data.itSavingSettingId = JSON.parse(data.itSavingSettingId).id;
					console.log('data setting', data)
					if(data.file && data.file != ""){
                		for(i in data.file){
                			console.log('i name',data.file[i].name)
                			filesName.push(data.file[i].name)
                		}
            			console.log('files',filesName)
	                	Upload
						.base64DataUrl(data.file)
						.then(function(urls) {
                    		data.file = '';
                    		itSavingFiles(data,urls);
						});
                	}else{
						data.file = "";
						itSavingFiles(data);
                	}  
					/*payrollService.addMyItSavings(data).then(function (res) {
						console.log('addMyItSavings res', res)
						if (res.successflag == 'true') {
							
							declineMyItSavings();
							getItSavings($localStorage.spheresuite.id);
							$('#itsavings').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});*/
				} else {
					payrollControllerScope.spinner = false;
				}
			});
		}
	}
	
	function itSavingFiles(data,files){
		payrollService.addMyItSavings(data).then(function (res) {
        	console.log('addOpportunity res',res);
            if (res.successflag === 'true' && res.results) {
            	if(files && res.results != ''){
            		for(var i = 0; i < files.length; i++){
            			payrollService.uploadDocument({id: res.results, photo: files[i], name: filesName[i]}).then(function(res1){
							console.log('res1',i, files.length)
		            		if(i == files.length){
		            			declineMyItSavings();		            			
								getItSavings($localStorage.spheresuite.id);	
								$('#itsavings').modal('hide');							
								payrollControllerScope.spinner = false;
		            		}
						},function(err){
							console.log('err1',err1);
						});
            		}
            	}else{
            		payrollControllerScope.spinner = false;  
            		getItSavings($localStorage.spheresuite.id);	          		
            		$('#itsavings').modal('hide');
            		declineMyItSavings();
            	}
            } else {
            	payrollControllerScope.spinner = false;
            }
        }, function(err) {
        	payrollControllerScope.spinner = false;
        });
    }
	
	function gotoEditSavings(data){	
		payrollControllerScope.empItSavings = data;
		payrollControllerScope.fromEditFile = data.file;
		$('#viewitsavings').modal('show');
	}

	function getEmployeeItSavings(){
		payrollControllerScope.spinner = true;
		payrollService.getEmployeeItSavings().then(function (res) {
			console.log('getTax res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.isDataAvailable = true;
				payrollControllerScope.getEmployeeItSavingsList = res.results;
				console.log('payrollControllerScope.getEmployeeItSavingsList   ', payrollControllerScope.getEmployeeItSavingsList )
    			payrollControllerScope.getEmployeeItSavingsListListBackup = angular.copy(payrollControllerScope.getEmployeeItSavingsList);
	            payrollControllerScope.isDataAvailable = true;

		        	for(var i = 0; i < payrollControllerScope.getEmployeeItSavingsListListBackup.length; i++){
		            	payrollControllerScope.getEmployeeItSavingsListListBackup[i].isActiveClass = false;
		            	payrollControllerScope.getEmployeeItSavingsListListBackup[i].isSelect = false;
		        	}
					payrollControllerScope.getEmployeeItSavingsList =  angular.copy(payrollControllerScope.getEmployeeItSavingsListListBackup);
		        	if((payrollControllerScope.buttonBeginFrom + payrollControllerScope.buttonLimitToShow) * payrollControllerScope.buttonLimitToShow >= payrollControllerScope.getEmployeeItSavingsList.length)
		        		payrollControllerScope.isNextDisabled = true;
					payrollControllerScope.getEmployeeItSavingsList[0].isActiveClass = true;
					console.log('payrollControllerScope.getEmployeeItSavingsList 00000 ',payrollControllerScope.getEmployeeItSavingsList )
					payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.spinner = false;
		});

	}

	function updateEmployeeItSavingsReject(data){
		 data.updatedBy= $localStorage.spheresuite.id;
		  data.status= 'r';
		console.log('tesssss',data);
		payrollControllerScope.spinner = true;
        data.updatedBy= $localStorage.spheresuite.id;
        console.log('showData', data)
        payrollService.updateEmployeeItSavingsVerified(data).then(function(res){
            console.log('updateEmployeeItSavingsVerified', res);
            if(res.successflag == 'true'){
            	console.log('trueSaving');
            	getEmployeeItSavings();
            	payrollControllerScope.spinner = false;
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
        $('#itsavings').modal('hide');

	}

	function updateEmployeeItSavingsVerified(data){
		 data.updatedBy= $localStorage.spheresuite.id;
		  data.status= 'v';
		payrollControllerScope.spinner = true;
        data.updatedBy= $localStorage.spheresuite.id;
        delete data.file;
        payrollService.updateEmployeeItSavingsVerified(data).then(function(res){
            console.log('updateEmployeeItSavingsVerified', res);
            if(res.successflag == 'true'){
            	console.log('trueSaving');
            	getEmployeeItSavings();
            	payrollControllerScope.spinner = false;
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
        $('#itsavings').modal('hide');
	}

	function updateEmployeeItSavingsApproved(data){
		 data.updatedBy= $localStorage.spheresuite.id;
		  data.status= 'a';
		payrollControllerScope.spinner = true;
        data.updatedBy= $localStorage.spheresuite.id;
        delete data.file;
        payrollService.updateEmployeeItSavingsVerified(data).then(function(res){
            console.log('updateEmployeeItSavingsVerified', res);
            if(res.successflag == 'true'){
            	console.log('trueSaving');
            	getEmployeeItSavings();
            	payrollControllerScope.spinner = false;
            } else {
                payrollControllerScope.spinner = false;
            }
        }, function(err){
                payrollControllerScope.spinner = false;
        });
        $('#itsavings').modal('hide');

	}


	function gotoEmployeeItSavings(data){
		getEmployeeItSavings();
        payrollControllerScope.empItSaving = data;
        $('#itsavings').modal('show');
    }

    function submitForSummary(){
		if(payrollControllerScope.empPayrollDetailList){
			payrollControllerScope.spinner = true;
			var ids = [];
			for(var i = 0; i < payrollControllerScope.empPayrollDetailList.length; i++){
				ids.push({id:payrollControllerScope.empPayrollDetailList[i].id});
			}
			if(ids.length != 0){
				payrollService.submitForSummary({updatedBy: $localStorage.spheresuite.id, ids: ids}).then(function(res){
					console.log('submitForSummary res',res);
					if(res.successflag == 'true'){
						getPayrollSummary();
						$route.reload();
					}
					payrollControllerScope.spinner = false;
				},function(err){
					console.log('err',err)
					payrollControllerScope.spinner = false;
				})
			}else{
				payrollControllerScope.spinner = true;
			}
		}
	} 

	function declineForSummary(){
		if(payrollControllerScope.empPayrollDetailList){
			payrollControllerScope.spinner = true;
			var ids = [];
			for(var i = 0; i < payrollControllerScope.empPayrollDetailList.length; i++){
				ids.push({id:payrollControllerScope.empPayrollDetailList[i].id});
			}
			if(ids.length != 0){
				payrollService.declineForSummary({updatedBy: $localStorage.spheresuite.id, ids: ids}).then(function(res){
					console.log('submitForSummary res',res);
					if(res.successflag == 'true'){
						getPayrollSummary();
						$route.reload();
					}
					payrollControllerScope.spinner = false;
				},function(err){
					console.log('err',err)
					payrollControllerScope.spinner = false;
				})
			}else{
				payrollControllerScope.spinner = true;
			}
		}
	}

	function addTaxSlabs() {
		if (payrollControllerScope.taxSlabs) {
			payrollControllerScope.spinner = true;
			commonService.formValNotManditory(payrollControllerScope.taxSlabsForm, payrollControllerScope.taxSlabs).then(function (data) {
				if (data) {
					data.updatedBy = $localStorage.spheresuite.id;
					payrollService.addTaxSlabs(data).then(function (res) {
						if (res.successflag == 'true') {
							declineTaxSlabs();
							getTaxSlabs();
							$('#taxSlabs').modal('hide');
						}
						payrollControllerScope.spinner = false;
					}, function (err) {
						payrollControllerScope.spinner = false;
					});
				} else {
					payrollControllerScope.spinner = false;
				}
			});
		}
	}

	function getTaxSlabs() {
		payrollControllerScope.spinner = true;
		payrollControllerScope.taxSlabsMsg = '';
		payrollService.getTaxSlabs().then(function (res) {
			console.log('getTaxSlabs res', res);
			if (res.successflag == 'true' && res.results.length > 0) {
				payrollControllerScope.taxSlabsList = res.results;
				console.log('payrollControllerScope.taxSlabsList', payrollControllerScope.taxSlabsList);
				payrollControllerScope.spinner = false;
			} else {
				payrollControllerScope.taxSlabsMsg = "Tax Slabs Not Available";
				payrollControllerScope.spinner = false;
			}
		}, function (err) {
			payrollControllerScope.taxSlabsMsg = "Tax Slabs Not Available";
			payrollControllerScope.spinner = false;
		});
	}

	function editTaxSlabs() {
		if (payrollControllerScope.taxSlabs) {
			if(payrollControllerScope.taxSlabs.enddate == undefined){
				console.log('emterinngg')
				payrollControllerScope.taxSlabs.enddate = '';
			}
			payrollControllerScope.spinner = true;
			payrollControllerScope.taxSlabs.updatedBy = $localStorage.spheresuite.id;
			payrollService.editTaxSlabs(payrollControllerScope.taxSlabs).then(function (res) {
				if (res.successflag == 'true') {
					declineTaxSlabs();
					getTaxSlabs();
					$('#taxSlabs').modal('hide');
				}
				payrollControllerScope.spinner = false;
			}, function (err) {
				payrollControllerScope.spinner = false;
			});
		}
	}

	function declineTaxSlabs() {
		payrollControllerScope.taxSlabs = null;
		payrollControllerScope.taxSlabsForm.$setPristine();
		payrollControllerScope.taxSlabsForm.$setUntouched();
		payrollControllerScope.isEdit = false;
	}

	function resetTaxSlabsAmount() {
		if (payrollControllerScope.taxSlabs.type == 'P') {
			payrollControllerScope.taxSlabs.basicGrossPay = '';
			payrollControllerScope.taxSlabs.fixedAmount = '0';
			payrollControllerScope.taxSlabs.percentageAmount = '';
		} else if (payrollControllerScope.taxSlabs.type == 'F') {
			payrollControllerScope.taxSlabs.basicGrossPay = 'G';
			payrollControllerScope.taxSlabs.fixedAmount = '';
			payrollControllerScope.taxSlabs.percentageAmount = '0';
		}
	}

	function openTaxSlabs(taxSlabs) {
		if (taxSlabs) {
			payrollControllerScope.taxSlabs = angular.copy(taxSlabs);
			payrollControllerScope.isEdit = true;
		}
	}

	function addMoreTaxSlabs() {
		payrollControllerScope.limitToShowTaxSlabs += 5;
	}
	
	function getTaxPayeeTypeActive(data) {
		payrollControllerScope.spinner = true;
        configurationService.getTaxPayeeTypeActive(data).then(function(res) {
            if (res.successflag == 'true' ) {
            	payrollControllerScope.taxPayeeTypeList = res.results;
                payrollControllerScope.spinner = false;
            } else {
            	payrollControllerScope.spinner = false;
            }
        }, function(err) {
        	payrollControllerScope.spinner = false;
        });
    }
	
	function getCompanyInformation(comapanyId) {
		payrollControllerScope.spinner = true;
        companyInformationService.getCompanyInformation(comapanyId).then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
                getState(res.results[0].country);
                payrollControllerScope.companyInfo = res.results[0];    
                payrollControllerScope.Companyspinner = false;
            } else {
            	payrollControllerScope.companyInfo = { photo : '', icon : '' }; 
            }
            payrollControllerScope.spinner = false;
        }, function(err) {
        	payrollControllerScope.spinner = false;
        });
    }
	
	function getState(country) {
        configurationService.getState(country).then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
            	payrollControllerScope.stateList = res.results;
                payrollControllerScope.spinner = false;
            } else {
            	payrollControllerScope.spinner = false;
            }
        }, function(err) {
        	payrollControllerScope.spinner = false;
        });
    }
	
	function getPTDetails(stateId){
		console.log('stateId  ', stateId)
		payrollControllerScope.spinner = true;
		if(stateId){
			payrollService.getPTDetails(stateId).then(function (res) {
				if (res.successflag == 'true' && res.results.length > 0) {
					payrollControllerScope.ptList = res.results;
					payrollControllerScope.PTData = false;
					console.log('payrollControllerScope.ptList   ', payrollControllerScope.ptList);
					payrollControllerScope.spinner = false;
				} else {
					payrollControllerScope.PTData = true;
					payrollControllerScope.spinner = false;
					payrollControllerScope.ptList = [];
				}
			}, function (err) {
				payrollControllerScope.PTData = true;
				payrollControllerScope.spinner = false;
				payrollControllerScope.ptList = [];
			});			
		}
	}
	
	function getFYActive(data) {
		payrollControllerScope.spinner = true;
        configurationService.getFYActive(data).then(function(res) {
            if (res.successflag == 'true' ) {
            	payrollControllerScope.financialYearActiveList = res.results;
                console.log('financialYearActiveList   ',payrollControllerScope.financialYearActiveList);
                payrollControllerScope.spinner = false;
            } else {
            	payrollControllerScope.spinner = false;
            }
        }, function(err) {
        	payrollControllerScope.spinner = false;
        });
    }
	
	function getFYActiveDetails(data) {
		payrollControllerScope.spinner = true;
		if(data){
			 console.log('fydata   ',JSON.parse(data));
			payrollService.getFYActiveDetails(JSON.parse(data)).then(function(res) {
	            if (res.successflag == 'true' ) {
	            	payrollControllerScope.fyList = res.results;
	                console.log('fyList   ',payrollControllerScope.fyList);
	                payrollControllerScope.spinner = false;
	                payrollControllerScope.FYData = false;
	            } else {
	            	payrollControllerScope.spinner = false;
	            	payrollControllerScope.FYData = true;
	            	payrollControllerScope.fyList = [];
	            }
	        }, function(err) {
	        	payrollControllerScope.spinner = false;
	        	payrollControllerScope.fyList = [];
	        	payrollControllerScope.FYData = true;
	        });
		} else {
			payrollControllerScope.spinner = false;
        	payrollControllerScope.fyList = [];
        	payrollControllerScope.FYData = true;
		}
    }


	$('.select1').select2();
	$('#select2').select2();
}