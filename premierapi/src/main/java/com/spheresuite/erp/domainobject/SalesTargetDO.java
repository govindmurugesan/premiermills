package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="salestarget")
@TableGenerator(name ="salestarget", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "salestarget.findAll", query = "SELECT r FROM SalesTargetDO r where r.createdBy IN :id"),
    @NamedQuery(name = "salestarget.findById", query = "SELECT r FROM SalesTargetDO r where r.id =:id"),
    @NamedQuery(name = "salestarget.findByEmpId", query = "SELECT r FROM SalesTargetDO r where r.empId =:empId"),
    @NamedQuery(name = "salestarget.findBetweenYear", query = "SELECT r FROM SalesTargetDO r where (r.fromDate >=:fromDate AND r.toDate <=:toDate)"),
    @NamedQuery(name = "salestarget.findBetweenYearForEmp", query = "SELECT r FROM SalesTargetDO r where (r.fromDate >=:fromDate AND r.toDate <=:toDate) AND r.empId =:empId"),
})
public class SalesTargetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "salestarget.findAll";
	
	public static final String FIND_BY_ID = "salestarget.findById";
	
	public static final String FIND_BY_EMP_ID = "salestarget.findByEmpId";
	
	public static final String FIND_BY_YEAR = "salestarget.findBetweenYear";
	
	public static final String FIND_BY_YEAR_FOR_EMP = "salestarget.findBetweenYearForEmp";
	
	public static final String FIND_ALL_DATA = "salestarget.findAllData";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "salestarget")
	private Long id;
	private Long fromDate;
	private Long toDate;
	private Long empId;
	private Double q1;
	private Double q2;
	private Double q3;
	private Double q4;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private Long createdBy;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getFromDate() {
		return fromDate;
	}
	public void setFromDate(Long fromDate) {
		this.fromDate = fromDate;
	}
	public Long getToDate() {
		return toDate;
	}
	public void setToDate(Long toDate) {
		this.toDate = toDate;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Double getQ1() {
		return q1;
	}
	public void setQ1(Double q1) {
		this.q1 = q1;
	}
	public Double getQ2() {
		return q2;
	}
	public void setQ2(Double q2) {
		this.q2 = q2;
	}
	public Double getQ3() {
		return q3;
	}
	public void setQ3(Double q3) {
		this.q3 = q3;
	}
	public Double getQ4() {
		return q4;
	}
	public void setQ4(Double q4) {
		this.q4 = q4;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
}