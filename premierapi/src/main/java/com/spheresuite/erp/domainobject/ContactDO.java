package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="contact")
@TableGenerator(name ="contact", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "contact.findAll", query = "SELECT r FROM ContactDO r where r.empId IN :id and r.isDeleted=:status"),
    @NamedQuery(name = "contact.findById", query = "SELECT r FROM ContactDO r where r.id =:id and r.isDeleted=:status"),
    @NamedQuery(name = "contact.findByLeadId", query = "SELECT r FROM ContactDO r where r.leadtype =:id and r.isDeleted=:status"),
    @NamedQuery(name = "contact.findAllData", query = "SELECT r FROM ContactDO r  where r.isDeleted=:status"),
    
})
public class ContactDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "contact.findAll";
	
	public static final String FIND_ALL_DATA = "contact.findAllData";
	
	public static final String FIND_BY_ID = "contact.findById";
	
	public static final String FIND_BY_LEADID = "contact.findByLeadId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "contact")
	private Long id;
	private Long leadtype;
	private Long salutation;
	private String firstname;
	private String lastname;
	private String middlename;
	private String designation;
	private Long contattype;
	private Long mobile1;
	private Long mobile2;
	private Long phone1;
	private Long phone2;
	private Long phone3;
	private Long phone4;
	private String address1;
	private String address2;
	private String primaryemail;
	private String secondaryemail;
	private Long fax;
	private String website;
	private String requirements;
	private String notes;
	private String comments;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private String isDeleted;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getLeadtype() {
		return leadtype;
	}
	public void setLeadtype(Long leadtype) {
		this.leadtype = leadtype;
	}
	public Long getSalutation() {
		return salutation;
	}
	public void setSalutation(Long salutation) {
		this.salutation = salutation;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public Long getContattype() {
		return contattype;
	}
	public void setContattype(Long contattype) {
		this.contattype = contattype;
	}
	public Long getMobile1() {
		return mobile1;
	}
	public void setMobile1(Long mobile1) {
		this.mobile1 = mobile1;
	}
	public Long getMobile2() {
		return mobile2;
	}
	public void setMobile2(Long mobile2) {
		this.mobile2 = mobile2;
	}
	public Long getPhone1() {
		return phone1;
	}
	public void setPhone1(Long phone1) {
		this.phone1 = phone1;
	}
	public Long getPhone2() {
		return phone2;
	}
	public void setPhone2(Long phone2) {
		this.phone2 = phone2;
	}
	public Long getPhone3() {
		return phone3;
	}
	public void setPhone3(Long phone3) {
		this.phone3 = phone3;
	}
	public Long getPhone4() {
		return phone4;
	}
	public void setPhone4(Long phone4) {
		this.phone4 = phone4;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getPrimaryemail() {
		return primaryemail;
	}
	public void setPrimaryemail(String primaryemail) {
		this.primaryemail = primaryemail;
	}
	public String getSecondaryemail() {
		return secondaryemail;
	}
	public void setSecondaryemail(String secondaryemail) {
		this.secondaryemail = secondaryemail;
	}
	public Long getFax() {
		return fax;
	}
	public void setFax(Long fax) {
		this.fax = fax;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getRequirements() {
		return requirements;
	}
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
}