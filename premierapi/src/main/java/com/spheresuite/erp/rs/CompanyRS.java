package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CompanyDO;
import com.spheresuite.erp.service.CompanyService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CompanyUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/company")
public class CompanyRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CompanyRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		companyDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		companyDO.setTanno(!inputJSON.get(CommonConstants.TANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.TANNO).toString() : null);
			 		companyDO.setPanno(!inputJSON.get(CommonConstants.PANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.PANNO).toString() : null);
			 		companyDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : null);
			 		companyDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
			 		companyDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		if(!inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
			 			companyDO.setState(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE).toString() : null));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty()){
			 			companyDO.setCountry(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY).toString() : null));
			 		}
			 		
			 		companyDO.setZip(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? inputJSON.get(CommonConstants.ZIP).toString() : null);
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			companyDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : null));
			 		}
			 		companyDO.setIndustry(!inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty() ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null);
			 		companyDO.setWebsite(!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : null);
			 		companyDO.setCallingCode(!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			companyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			companyDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		companyDO.setUpdatedon(new Date());
			 	}
				new CompanyService().persist(companyDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Company Created");
				return CommonWebUtil.buildSuccessResponseId(companyDO.getId()).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
	
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
		public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
			String respJSON = null;
			try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CompanyDO> companyList = new CompanyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = CompanyUtil.getCompanyList(companyList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CompanyDO> categoryList = new CompanyService().retrieve();
				respJSON = CompanyUtil.getCompanyList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CompanyDO> companyList = new CompanyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
				 		companyDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
				 		companyDO.setTanno(!inputJSON.get(CommonConstants.TANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.TANNO).toString() : null);
				 		companyDO.setPanno(!inputJSON.get(CommonConstants.PANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.PANNO).toString() : null);
				 		companyDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : null);
				 		companyDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : null);
				 		companyDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
				 		if(!inputJSON.get(CommonConstants.STATE).toString().isEmpty()){
				 			companyDO.setState(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE).toString() : null));
				 		}
				 		
				 		if(!inputJSON.get(CommonConstants.COUNTRY).toString().isEmpty()){
				 			companyDO.setCountry(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY).toString() : null));
				 		}
				 		
				 		companyDO.setZip(!inputJSON.get(CommonConstants.ZIP).toString().isEmpty() ? inputJSON.get(CommonConstants.ZIP).toString() : null);
				 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
				 			companyDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : null));
				 		}
				 		companyDO.setIndustry(!inputJSON.get(CommonConstants.INDUSTRY).toString().isEmpty() ? inputJSON.get(CommonConstants.INDUSTRY).toString() : null);
				 		companyDO.setWebsite(!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : null);
				 		companyDO.setCallingCode(!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null);
				 		companyDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			companyDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		new CompanyService().update(companyDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Company Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<CompanyDO> companyList = new CompanyService().retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
				 		companyDO.setPhoto(request.getParameter("file"));
				 		companyDO.setUpdatedon(new Date());
				 		new CompanyService().update(companyDO);
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updateicon", method = RequestMethod.POST)
	public @ResponseBody String updateicon(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CompanyDO companyDO = new CompanyDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		List<CompanyDO> companyList = new CompanyService().retrieveById(Long.parseLong(request.getParameter("id")));
			 		if(companyList != null && companyList.size() > 0){
			 			companyDO = companyList.get(0);
				 		companyDO.setCompanyIcon(request.getParameter("file"));
				 		companyDO.setUpdatedon(new Date());
				 		new CompanyService().update(companyDO);
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
