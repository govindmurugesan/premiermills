angular
    .module('spheresuite')
    .controller('hrPolicyController', hrPolicyController);

hrPolicyController.$inject = ['$scope', '$rootScope', '$location', '$filter', '$localStorage', 'hrPolicyService', 'commonService', 'rolesService', 'Upload', 'employeeService'];

function hrPolicyController($scope, $rootScope, $location, $filter, $localStorage, hrPolicyService, commonService, rolesService, Upload, employeeService) {
    var hrPolicyControllerScope = this;
	$rootScope.headerMenu = "HR Policies"; 
	hrPolicyControllerScope.dataMsg = '';
    hrPolicyControllerScope.isUpdate = false;
    hrPolicyControllerScope.hrPolicy;
    hrPolicyControllerScope.hrPolicyList = [];
    hrPolicyControllerScope.spinner = false;
    
    hrPolicyControllerScope.addHrPolicy = addHrPolicy;
    hrPolicyControllerScope.changeStatus = changeStatus;
    hrPolicyControllerScope.decline = decline;
    hrPolicyControllerScope.editHrPolicy = editHrPolicy;
    hrPolicyControllerScope.exportData = exportData;
    hrPolicyControllerScope.getHrPolicy = getHrPolicy;
    hrPolicyControllerScope.gotoEditHrPolicy = gotoEditHrPolicy;
    hrPolicyControllerScope.getRole = getRole;
    
    
    	getHrPolicy();
    	
    
    
    /*function addHrPolicy(){ 
    	if(hrPolicyControllerScope.hrPolicy){
    	    hrPolicyControllerScope.spinner = true;
    	    hrPolicyControllerScope.hrPolicy.updatedBy= $localStorage.spheresuite.id; 
			hrPolicyService.addHrPolicy(hrPolicyControllerScope.hrPolicy).then(function(res){ 
				if(res.successflag == 'true'){
					decline();
					getHrPolicy();
				}
				hrPolicyControllerScope.spinner = false;
			},function(err){
				hrPolicyControllerScope.spinner = false;
			});
    	}
    }*/

    function getHrPolicy(data){ 
    	
        hrPolicyControllerScope.spinner = true;
        hrPolicyControllerScope.dataMsg = '';
         console.log('hrPolicyControllerScope.hrPolicyList')
        hrPolicyService.getHrPolicy(data).then(function(res){
        	console.log('hrPolicyControllerScope.hrPolicyList ===   ', res); 
            if(res.successflag == 'true' && res.results.length > 0){
                hrPolicyControllerScope.hrPolicyList = res.results;
                hrPolicyControllerScope.spinner = false;
                getRole();
                getReportingDetails();
                console.log('hrPolicyControllerScope.hrPolicyList', hrPolicyControllerScope.hrPolicyList);                
                if($location.path() == '/hrpolicies'){
                    var list = [];
                    angular.forEach(hrPolicyControllerScope.hrPolicyList,function(val, key){
                        if(val.status == 'a'){
                            list.push(val);
                            
                        }
                    });
                    hrPolicyControllerScope.hrPolicyList = list;
                    if(list.length === 0){
                        hrPolicyControllerScope.dataMsg = "HR Policies Not Available";
                    }
                }
               
            } else{
                hrPolicyControllerScope.dataMsg = "HR Policies Not Available";
                hrPolicyControllerScope.spinner = false;
                }
            
        },function(err){
            hrPolicyControllerScope.dataMsg = "HR Policies Not Available";
            hrPolicyControllerScope.spinner = false;
        });
    }

    var filesName = [];
    function addHrPolicy() {
        if (hrPolicyControllerScope.hrPolicy) {
            if(hrPolicyControllerScope.hrPolicy.roleId == undefined){
                hrPolicyControllerScope.hrPolicy.roleId = '';
            }
            hrPolicyControllerScope.spinner = true;
          if(hrPolicyControllerScope.reportingList.deptHeadEmail)  hrPolicyControllerScope.hrPolicy.deptHeadEmail = hrPolicyControllerScope.reportingList.deptHeadEmail;
          else hrPolicyControllerScope.hrPolicy.deptHeadEmail = "";
          if(hrPolicyControllerScope.reportingList.reportingEmail)  hrPolicyControllerScope.hrPolicy.reportingEmail = hrPolicyControllerScope.reportingList.reportingEmail;
          else hrPolicyControllerScope.hrPolicy.reportingEmail = "";
          if(hrPolicyControllerScope.reportingList.reportToName)  hrPolicyControllerScope.hrPolicy.reportToName = hrPolicyControllerScope.reportingList.reportToName;
          else hrPolicyControllerScope.hrPolicy.reportToName = "";
          if(hrPolicyControllerScope.reportingList.deptMngrName)  hrPolicyControllerScope.hrPolicy.deptMngrName = hrPolicyControllerScope.reportingList.deptMngrName;
          else hrPolicyControllerScope.hrPolicy.deptMngrName = "";
          hrPolicyControllerScope.hrPolicy.comment =  hrPolicyControllerScope.hrPolicy.value;
            hrPolicyControllerScope.hrPolicy.requestBy =  $localStorage.spheresuite.name;
            commonService.formValNotManditory(hrPolicyControllerScope.hrPolicyForm, hrPolicyControllerScope.hrPolicy).then(function (data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    console.log('data setting', data)
                    if(data.file && data.file != ""){
                        for(i in data.file){
                            console.log('i name',data.file[i].name)
                            filesName.push(data.file[i].name)
                        }
                        console.log('files',filesName)
                        Upload
                        .base64DataUrl(data.file)
                        .then(function(urls) {
                            data.file = '';
                            itSavingFiles(data,urls);
                        });
                    }else{
                        data.file = "";
                        itSavingFiles(data);
                    } 
                } else {
                    hrPolicyControllerScope.spinner = false;
                }
            });
        }
    }

    function itSavingFiles(data,files){
        hrPolicyService.addHrPolicy(data).then(function (res) {
            console.log('addOpportunity res',res);
            if (res.successflag === 'true' && res.results) {
                if(files && res.results != ''){
                    for(var i = 0; i < files.length; i++){
                        hrPolicyService.uploadDocument({id: res.results, photo: files[i], name: filesName[i]}).then(function(res1){
                            console.log('res1',i, files.length)
                            if(i == files.length){
                                decline();
                                $location.path('/hrpolicy'); 
                                getHrPolicy()                     
                                hrPolicyControllerScope.spinner = false;
                            }
                        },function(err){
                            console.log('err1',err1);
                        });
                    }
                }else{
                    hrPolicyControllerScope.spinner = false;  
                    decline();
                    $location.path('/hrpolicy');
                    getHrPolicy();
                   // hrPolicyControllerScope.hrPolicy = {};
                }
            } else {
                hrPolicyControllerScope.spinner = false;
            }
        }, function(err) {
            hrPolicyControllerScope.spinner = false;
        });
    }
    
    function changeStatus(hrPolicy){
    	if(hrPolicy){
    		if(hrPolicy.status && hrPolicy.status == 'i'){
    			hrPolicy.status = 'a'
    		} else {
    			hrPolicy.status = 'i'
    		}
    		hrPolicyControllerScope.hrPolicy = hrPolicy;
    		hrPolicyControllerScope.hrPolicy.value = hrPolicy.name;
    		editHrPolicy();
    	}
    }

    function decline(){
    		hrPolicyControllerScope.hrPolicy = null;
	    	hrPolicyControllerScope.hrPolicyForm.$setPristine();
	    	hrPolicyControllerScope.hrPolicyForm.$setUntouched();
	    	hrPolicyControllerScope.isUpdate = false;
	        $('#hrPolicy').modal('hide');
    }
    
    /*function editHrPolicy(){
    	if(hrPolicyControllerScope.hrPolicy){
    	    hrPolicyControllerScope.spinner = true;
    	    hrPolicyControllerScope.hrPolicy.updatedBy= $localStorage.spheresuite.id;
			hrPolicyService.updateHrPolicy(hrPolicyControllerScope.hrPolicy).then(function(res){ 
				if(res.successflag == 'true'){
					decline();
					getHrPolicy();
				}
				hrPolicyControllerScope.spinner = false;
			},function(err){
				hrPolicyControllerScope.spinner = false;
			});
    	}
    }*/

    function editHrPolicy(){
        if(hrPolicyControllerScope.hrPolicy){
            hrPolicyControllerScope.spinner = true;
            /*commonService.formValNotManditory(hrPolicyControllerScope.hrPolicyForm, hrPolicyControllerScope.hrPolicy).then(function (data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    //data.status = 's';
                    console.log('data setting', data)
                    if(data.file && data.file != ""){
                        for(i in data.file){
                            console.log('i name',data.file[i].name)
                            filesName.push(data.file[i].name)
                        }
                        console.log('files',data)
                        Upload
                        .base64DataUrl(data.file)
                        .then(function(urls) {
                            data.file = '';
                            itSavingEditFiles(data,urls);
                        });
                    }else{
                        data.file = "";
                        itSavingEditFiles(data);
                    } 
                    $('#hrPolicy').modal('hide');
                } else {
                    hrPolicyControllerScope.spinner = false;
                    $('#hrPolicy').modal('hide');
                }
            });*/
            console.log('hrPolicyControllerScope.hrPolicy.file ',hrPolicyControllerScope.hrPolicy)
            if(hrPolicyControllerScope.hrPolicy.file && hrPolicyControllerScope.hrPolicy.file != ""){
                
                console.log('hrPolicyControllerScope.hrPolicy.file',hrPolicyControllerScope.hrPolicy.file)
                for(i in hrPolicyControllerScope.hrPolicy.fromEditFile){
                    filesName.push(hrPolicyControllerScope.hrPolicy.fromEditFile[i].name)
                }
                for(i in hrPolicyControllerScope.hrPolicy.file){
                    console.log('i name',hrPolicyControllerScope.hrPolicy.file[i].name)
                    filesName.push(hrPolicyControllerScope.hrPolicy.file[i].name)
                }
                console.log('files ',filesName)
                Upload
                .base64DataUrl(hrPolicyControllerScope.hrPolicy.file)
                .then(function(urls) {
                    hrPolicyControllerScope.hrPolicy.file = '';

                    for(i in hrPolicyControllerScope.hrPolicy.fromEditFile){
                        urls.push(hrPolicyControllerScope.hrPolicy.fromEditFile[i].file)
                    }
                    console.log('urls',urls)
                    itSavingEditFiles(hrPolicyControllerScope.hrPolicy,urls);
                });
            }else{
                itSavingEditFiles(hrPolicyControllerScope.hrPolicy,hrPolicyControllerScope.hrPolicy.fromEditFile);
            }
        }       
    }

   /* function itSavingEditFiles(data,files){
        console.log('itSavingEditFiles', itSavingEditFiles);
        hrPolicyService.updateHrPolicy(data).then(function (res) {
            console.log('addOpportunity res',res);
            if (res.successflag === 'true' && res.results) {
                if(files && res.results != ''){
                    for(var i = 0; i < files.length; i++){
                        hrPolicyService.uploadDocument({id: data.id, photo: files[i], name: filesName[i]}).then(function(res1){
                            console.log('res1',i, files.length)
                            if(i == files.length){
                                hrPolicyControllerScope.spinner = false;
                    if($location.path() == '/hrpolicy'){
                        $location.path('/hrpolicy');
                        getHrPolicy();
                    }else{
                        //decline();
                    }
                            }
                        },function(err){
                            console.log('err1',err1);
                        });
                    }
                }else{
                    hrPolicyControllerScope.spinner = false;  
                    
                }
            } else {
                hrPolicyControllerScope.spinner = false;
            }
        }, function(err) {
            hrPolicyControllerScope.spinner = false;
        });
    }*/

    function itSavingEditFiles(data,files){
        data.fromEditFile = '';
        data.updatedBy = $localStorage.spheresuite.id;
        hrPolicyService.updateHrPolicy(data).then(function(res) {
            hrPolicyControllerScope.spinner = true;
            if (res.successflag === 'true') {
                if(files){
                    if(files == ''|| files.length == 0){
                        hrPolicyControllerScope.spinner = false;
                        $location.path('/hrpolicy');
                        
                    }else{
                        for(var i = 0; i < files.length; i++){
                            var fileName = filesName.length > 0?filesName[i]:files[i].name;
                            var fileData = (files[i].file!="" && files[i].file != undefined)?files[i].file:files[i];
                            console.log('fileData   ',fileData)
                            console.log('fileName   ',fileName)
                            console.log('id   ',data.id)
                            hrPolicyService.uploadDocument({id: data.id, photo: fileData, name: fileName}).then(function(res1){
                                console.log('res1 ',files.length)
                                if(i == files.length){
                                    hrPolicyControllerScope.spinner = false;
                                    $('#hrPolicy').modal('hide');
                                    getHrPolicy();
                                }
                            },function(err){
                                console.log('err1',err1);
                                hrPolicyControllerScope.spinner = false;
                            });
                        }
                    }
                }else{
                    hrPolicyControllerScope.spinner = false;
                    $('#hrPolicy').modal('hide');
                    getHrPolicy();
                }
            }else{
                hrPolicyControllerScope.spinner = false;
                 $('#hrPolicy').modal('hide');
                    getHrPolicy();
            }
        }, function(err) {
            hrPolicyControllerScope.spinner = false;
        });
    }
    
    function exportData() { 
        $scope.fileName = "Hr Policy";
        $scope.exportData = []; 
        $scope.exportData.push(["Id", "Policy", "Updated By", "Updated On"]); 
        $scope.Filterdata = hrPolicyControllerScope.hrPolicyList;
        var firstFiter = $filter('filter')(hrPolicyControllerScope.hrPolicyList);
        $scope.Filterdata = $filter('filter')(firstFiter, { name: hrPolicyControllerScope.searchName });
        angular.forEach($scope.Filterdata, function(value, key) {
            $scope.exportData.push([value.id, value.name, value.updatedBy, value.updatedon]);
        });

        function datenum(v, date1904) {
            if (date1904) v += 1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
        };

        function getSheet(data, opts) {
            var ws = {};
            var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
            for (var R = 0; R != data.length; ++R) {
                for (var C = 0; C != data[R].length; ++C) {
                    if (range.s.r > R) range.s.r = R;
                    if (range.s.c > C) range.s.c = C;
                    if (range.e.r < R) range.e.r = R;
                    if (range.e.c < C) range.e.c = C;
                    var cell = { v: data[R][C] };
                    if (cell.v == null) continue;
                    var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                    if (typeof cell.v === 'number') cell.t = 'n';
                    else if (typeof cell.v === 'boolean') cell.t = 'b';
                    else if (cell.v instanceof Date) {
                        cell.t = 'n';
                        cell.z = XLSX.SSF._table[14];
                        cell.v = datenum(cell.v);
                    } else cell.t = 's';

                    ws[cell_ref] = cell;
                }
            }
            if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
            return ws;
        };

        function Workbook() {
            if (!(this instanceof Workbook)) return new Workbook();
            this.SheetNames = [];
            this.Sheets = {};
        }

        var wb = new Workbook(),
            ws = getSheet($scope.exportData); 
        wb.SheetNames.push($scope.fileName);
        wb.Sheets[$scope.fileName] = ws;
        var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

        function s2ab(s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        }
        saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), $scope.fileName + '.xlsx');
    }
    
    
    function gotoEditHrPolicy(hrPolicy){
    	if(hrPolicy){
            console.log('hrPolicy', hrPolicy);
    		hrPolicyControllerScope.hrPolicy = hrPolicy;
    		hrPolicyControllerScope.hrPolicy.value = hrPolicy.name;
            hrPolicyControllerScope.hrPolicy.fromEditFile = hrPolicy.file;
            hrPolicyControllerScope.hrPolicy.file = ''; 
    		hrPolicyControllerScope.isUpdate = true;
    	}
    }

    function getRole() {
        hrPolicyControllerScope.spinner = true;
        rolesService.getRole().then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
                hrPolicyControllerScope.roleList = res.results;
                console.log('hrPolicyControllerScope.roleList', hrPolicyControllerScope.roleList);
                hrPolicyControllerScope.spinner = false;
            } else { 
                hrPolicyControllerScope.spinner = false;
            }
        }, function(err) { 
            hrPolicyControllerScope.spinner = false;
        });
    }
    
    function getReportingDetails(){
    	hrPolicyControllerScope.spinner = true;
        var userId = $localStorage.spheresuite.id;
        employeeService.getReportingDetails(userId).then(function(res) {
        	console.log("res",res);
               if (res.successflag === 'true' && res.results.length > 0) {
            	   hrPolicyControllerScope.isReportingMangager = true;
            	   hrPolicyControllerScope.reportingList = res.results[0];
            	   hrPolicyControllerScope.spinner = false;
                } else {
                	hrPolicyControllerScope.spinner = false; 
                }
        }, function(err) {
        	hrPolicyControllerScope.spinner = false;
        });
    }
    
$(".select1").select2();

}