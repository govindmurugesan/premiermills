package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="payrolltaxdetails")
@TableGenerator(name ="payrolltaxdetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "payrolltaxdetails.findAll", query = "SELECT r FROM PayrollTaxDetailsDO r"),
    @NamedQuery(name = "payrolltaxdetails.findByEmpPayrollId", query = "SELECT r FROM PayrollTaxDetailsDO r where r.payrollid =:id"),
    @NamedQuery(name = "payrolltaxdetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollTaxDetailsDO r where r.payrollid IN :id"),
})
public class PayrollTaxDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "payrolltaxdetails.findAll";
	
	public static final String FIND_BY_ID = "payrolltaxdetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "payrolltaxdetails.findByEmpPayrollIds";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "payrolltaxdetails")
	private Long id;
	private Long taxId;
	private Long payrollid;
	private Double monthly;
	private Double ytd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaxId() {
		return taxId;
	}
	public void setTaxId(Long taxId) {
		this.taxId = taxId;
	}
	public Long getPayrollid() {
		return payrollid;
	}
	public void setPayrollid(Long payrollid) {
		this.payrollid = payrollid;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}