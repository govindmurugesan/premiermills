package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.MenusDAO;
import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.exception.AppException;

public class MenuService {
	static Logger logger = Logger.getLogger(MenuService.class.getName());
	
	public List<MenuDO> retrieveMainMenu() throws AppException {
		return new MenusDAO().retrieveMainMenu();
	}
}
