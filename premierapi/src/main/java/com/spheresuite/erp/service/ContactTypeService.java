package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ContactTypeDAO;
import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.exception.AppException;

public class ContactTypeService {
	static Logger logger = Logger.getLogger(ContactTypeService.class.getName());
	
	public ContactTypeDO persist(ContactTypeDO countryDO) throws AppException {
		return new ContactTypeDAO().persist(countryDO);
	}

	public List<ContactTypeDO> retrieveActive() throws AppException {
		return new ContactTypeDAO().retrieveActive();
	}
	
	public List<ContactTypeDO> retrieveById(Long id) throws AppException {
		return new ContactTypeDAO().retrieveById(id);
	}
	
	public List<ContactTypeDO> retrieve() throws AppException {
		return new ContactTypeDAO().retrieve();
	}
	
	public ContactTypeDO update(ContactTypeDO countryDO) throws AppException {
		return new ContactTypeDAO().update(countryDO);
	}
}
