package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeaveTypeDO;
import com.spheresuite.erp.service.LeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.LeaveTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leaveType")
public class LeaveTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeaveTypeRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				LeaveTypeDO leaveTypeDO = new LeaveTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		List<LeaveTypeDO> leaveTypeList = new LeaveTypeService().retrieveByType(inputJSON.get(CommonConstants.TYPE).toString());
			 		if(leaveTypeList != null && leaveTypeList.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Leave Type Already Exist").toString();
			 		}else{
			 			leaveTypeDO.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
				 		leaveTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leaveTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 			leaveTypeDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
				 		}
				 		leaveTypeDO.setStatus(CommonConstants.ACTIVE);
				 		new LeaveTypeService().persist(leaveTypeDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Type Created");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<LeaveTypeDO> leaveTypeList = new LeaveTypeService().retrieve();
				respJSON = LeaveTypeUtil.getLeaveTypeList(leaveTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				LeaveTypeDO leaveTypeDO = new LeaveTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<LeaveTypeDO> leaveTypeListTemp = new LeaveTypeService().retrieveByTypeUpdate(inputJSON.get(CommonConstants.TYPE).toString(),
			 				Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(leaveTypeListTemp != null && leaveTypeListTemp.size() > 0){
			 			return CommonWebUtil.buildErrorResponse("Leave Type Already Exist").toString();
			 		}else{
			 			List<LeaveTypeDO> leaveTypeList = new LeaveTypeService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		leaveTypeDO = leaveTypeList.get(0);
				 		leaveTypeDO.setType(inputJSON != null ? inputJSON.get(CommonConstants.TYPE).toString() : "");
				 		leaveTypeDO.setUpdatedon(new Date());
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			leaveTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		leaveTypeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
				 		new LeaveTypeService().update(leaveTypeDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Leave Type Updated");
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
