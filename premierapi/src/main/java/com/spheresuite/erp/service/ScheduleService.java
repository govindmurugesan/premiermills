package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ScheduleDAO;
import com.spheresuite.erp.domainobject.ScheduleDO;
import com.spheresuite.erp.exception.AppException;

public class ScheduleService {
	static Logger logger = Logger.getLogger(ScheduleService.class.getName());
	
	public ScheduleDO persist(ScheduleDO scheduleDO) throws AppException {
		return new ScheduleDAO().persist(scheduleDO);
	}
	
	public ScheduleDO update(ScheduleDO scheduleDO) throws AppException {
		return new ScheduleDAO().update(scheduleDO);
	}

	public List<ScheduleDO> retrieveById(Long Id) throws AppException {
		return new ScheduleDAO().retrieveById(Id);
	}

	public List<ScheduleDO> retrieveByLeadId(Long Id) throws AppException {
		return new ScheduleDAO().retrieveByLeadId(Id);
	}
	
	public List<ScheduleDO> retrieveUpcomingCalender(Long empid) throws AppException {
		return new ScheduleDAO().retrieveUpcomingCalender(empid, new Date());
	}
	
	
}
