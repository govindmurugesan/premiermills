package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ContactDAO;
import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.exception.AppException;

public class ContactService {
	static Logger logger = Logger.getLogger(ContactService.class.getName());
	
	public ContactDO persist(ContactDO contactDO) throws AppException {
		return new ContactDAO().persist(contactDO);
	}

	public List<ContactDO> retriveById(Long id) throws AppException {
		return new ContactDAO().retrieveById(id);
	}
	
	public List<ContactDO> retriveByLeadId(Long id) throws AppException {
		return new ContactDAO().retrieveByLeadId(id);
	}
	
	public List<ContactDO> retrieve(List<Long> ids) throws AppException {
		return new ContactDAO().retrieve(ids);
	}
	
	public List<ContactDO> retrieveAll() throws AppException {
		return new ContactDAO().retrieveAll();
	}
	
	public ContactDO update(ContactDO contactDO) throws AppException {
		return new ContactDAO().update(contactDO);
	}
	
	public boolean update(List<ContactDO> contactDO) throws AppException {
		return new ContactDAO().update(contactDO);
	}
	
	public boolean persistList(List<ContactDO> contactDO) throws AppException {
		return new ContactDAO().persistList(contactDO);
	}
}
