package com.spheresuite.erp.web.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.util.CommonConstants;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class WebManager {

	public static boolean authenticateSession(HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException{
		boolean authenticationStatus = true;
		try{
			//System.out.println("chk");
			//System.out.println("request.getHeaderAuthorization"+request.getHeader("Authorization"));
			//System.out.println("request.getSession().getAttributetoken"+request.getSession().getAttribute("token"));
			if(request.getSession().getAttribute(CommonConstants.TOKEN) != null){
				if(request.getHeader("Authorization") != null){
					if(request.getHeader("Authorization").equalsIgnoreCase((String) request.getSession().getAttribute("token"))){
						//System.out.println("eq");
						authenticationStatus = true;
						Claims claims = Jwts.parser()         
								   .setSigningKey(DatatypeConverter.parseBase64Binary("ths is secret"))
								   .parseClaimsJws(request.getSession().getAttribute("token").toString()).getBody();
						 String replace = claims.getId().replace("[","");
						 String replace1 = replace.replace("]","");
						 List<String> myList = new ArrayList<String>(Arrays.asList(replace1.split(", ")));
						 List<Long> deptIds = new ArrayList<Long>();
						 if(myList != null && !myList.isEmpty() && myList.size() > 0){
							 for (String string : myList) {
								 if(!string.equalsIgnoreCase("")){
									 deptIds.add(Long.parseLong(string));
								 }
							 }
						 }
						 request.getSession().setAttribute("deptIds", deptIds);
						 request.getSession().setAttribute("empIds", claims.getSubject());
						 /*List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(claims.getSubject()));
					 		if(employeeList != null && employeeList.size() > 0){
					 			if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null
					 					&& employeeList.get(0).getEmailType() != null){
					 				 request.getSession().setAttribute("emailId", employeeList.get(0).getEmailId());
					 				 request.getSession().setAttribute("emailPassword", employeeList.get(0).getEmailPassword());
					 				 request.getSession().setAttribute("emailType", employeeList.get(0).getEmailType());
					 			}
					 		}*/
						//userId = claims.getId();
					}else{
						authenticationStatus = false;
					}
					
				}
			}
		}catch(Exception e) {
	    }
		
		return authenticationStatus;
	}
	
	
	/*public static boolean authenticateFinanceSession(HttpServletRequest request){
		boolean authenticationStatus = false;
		try{
			if(request.getSession().getAttribute(CommonConstants.SESSION) != null){
				UserDO user = (UserDO) request.getSession().getAttribute(CommonConstants.SESSION);
				if(user.getUsername() != null && user.getPassword() != null 
						&& (user.getGroupname().equalsIgnoreCase(CommonConstants.SAPTA_LABS_AD) || user.getGroupname().equalsIgnoreCase(CommonConstants.SAPTA_LABS_FI))){
					authenticationStatus = true;
				}
			}
		}catch(Exception e) {
	    }
		return authenticationStatus;
	}
	
	public static boolean authenticateHRSession(HttpServletRequest request){
		boolean authenticationStatus = false;
		try{
			if(request.getSession().getAttribute(CommonConstants.SESSION) != null){
				UserDO user = (UserDO) request.getSession().getAttribute(CommonConstants.SESSION);
				if(user.getUsername() != null && user.getPassword() != null 
						&& (user.getGroupname().equalsIgnoreCase(CommonConstants.SAPTA_LABS_AD) || user.getGroupname().equalsIgnoreCase(CommonConstants.SAPTA_LABS_HR))){
					authenticationStatus = true;
				}
			}
		}catch(Exception e) {
	    }
		return authenticationStatus;
	}*/

}