package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeEarningsDAO {
	static Logger logger = Logger.getLogger(EmployeeEarningsDAO.class.getName());
	private EntityManager em = null;
	
	public List<EmployeeEarningsDO> persist(List<EmployeeEarningsDO> employeeEarningsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeEarningsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (EmployeeEarningsDO employeeEarningsDO2 : employeeEarningsDO) {
					em.persist(employeeEarningsDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEarningsDO> retrieve() throws AppException {
		List<EmployeeEarningsDO> employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeEarningsDO.FIND_ALL);
				employeeEarningsList = (List<EmployeeEarningsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeEarningsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		List<EmployeeEarningsDO> employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeEarningsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeEarningsList = (List<EmployeeEarningsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}
	
	public Long retrieveByEmpCompensationIds(List<Long> ids) throws AppException {
		Long employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeEarningsDO.FIND_BY_IDS);
				q.setParameter(CommonConstants.ID, ids);
				employeeEarningsList = (Long) q.getSingleResult();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}
	
	public EmployeeEarningsDO update(EmployeeEarningsDO employeeEarningsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeEarningsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsDO;
	}

}
