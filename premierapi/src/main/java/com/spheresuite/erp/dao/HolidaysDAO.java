package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class HolidaysDAO {
	static Logger logger = Logger.getLogger(HolidaysDAO.class.getName());
	private EntityManager em = null;
	
	public HolidaysDO persist(HolidaysDO holidayDetail) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && holidayDetail != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(holidayDetail);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayDetail;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieve() throws AppException {
		List<HolidaysDO> holidayList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HolidaysDO.FIND_ALL);
				holidayList = (List<HolidaysDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveById(Long Id) throws AppException {
		List<HolidaysDO> holidayList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HolidaysDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				holidayList = (List<HolidaysDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByHoliday(String desc, String year, String date) throws AppException {
		List<HolidaysDO> holidayList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HolidaysDO.FIND_BY_HOLIDAY);
				q.setParameter(CommonConstants.DESC, desc);
				q.setParameter(CommonConstants.YEAR, year);
				q.setParameter(CommonConstants.DATE, date);
				holidayList = (List<HolidaysDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HolidaysDO> retrieveByHolidayForUpdate(Long id,String desc, String year, String date) throws AppException {
		List<HolidaysDO> holidayList = null;
		List<Long> ids = new ArrayList<Long>();
		ids.add(id);
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HolidaysDO.FIND_BY_HOLIDAY_UPDATE);
				q.setParameter(CommonConstants.DESC, desc);
				q.setParameter(CommonConstants.YEAR, year);
				q.setParameter(CommonConstants.DATE, date);
				q.setParameter(CommonConstants.ID, ids);
				holidayList = (List<HolidaysDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayList;
	}
	
	public HolidaysDO update(HolidaysDO holidayDetail) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(holidayDetail);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return holidayDetail;
	}

}
