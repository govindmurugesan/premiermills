package com.spheresuite.erp.rs;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.service.AllowanceSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.AllowanceSettingsUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/allowancesettings")
public class AllowanceSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(AllowanceSettingsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				AllowanceSettingDO allowanceSettingDO = new AllowanceSettingDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString().isEmpty()){
			 			allowanceSettingDO.setAllowancetypeid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString() : ""));
			 		}
			 		allowanceSettingDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		allowanceSettingDO.setTaxable(!inputJSON.get(CommonConstants.TAXABLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TAXABLE).toString() : "");
			 		allowanceSettingDO.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : "");
			 		allowanceSettingDO.setNotes(!inputJSON.get(CommonConstants.NOTES).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTES).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			allowanceSettingDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			allowanceSettingDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		allowanceSettingDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
			 		allowanceSettingDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
			 		allowanceSettingDO.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
			 		allowanceSettingDO.setFixedamount(!inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FIXEDAMOUNT).toString() : "");
			 		allowanceSettingDO.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
			 		allowanceSettingDO.setBasicgrosspay(!inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? inputJSON.get(CommonConstants.BASICGROSS_PAY).toString() : "");
			 		allowanceSettingDO.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
			 		allowanceSettingDO.setUpdatedon(new Date());
			 		allowanceSettingDO.setStatus('a');
			 	}
				new AllowanceSettingsService().persist(allowanceSettingDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<AllowanceSettingDO> allowanceSettingList = new AllowanceSettingsService().retrieve();
				respJSON = AllowanceSettingsUtil.getAllowanceSettingsList(allowanceSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				int i =0;
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.NAME).toString().isEmpty()){
			 			List<AllowanceSettingDO> allowanceSettingList = new AllowanceSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(allowanceSettingList != null && allowanceSettingList.size() > 0){
			 				AllowanceSettingDO allowanceSettingDO = new AllowanceSettingDO();
			 				allowanceSettingDO = allowanceSettingList.get(0);
			 				allowanceSettingDO.setStatus('i');
			 				if(allowanceSettingDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						Calendar cal = Calendar.getInstance();
			 						cal.setTime(CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()));
			 						cal.add(Calendar.DATE, -1);
			 						String enddate = CommonUtil.convertDateToYearWithOutTime(allowanceSettingDO.getStartdate());
			 						Calendar cal1 = Calendar.getInstance();
			 						cal1.setTime(CommonUtil.convertStringToSqlDate(enddate));
			 						Date dateBefore1Days = cal.getTime();
			 						if(cal1.before(cal)){
			 							allowanceSettingDO.setEnddate(dateBefore1Days);
			 						}else{
			 							allowanceSettingDO.setEnddate(allowanceSettingDO.getStartdate());
			 						}
			 					}else{
			 						i++;
			 					}
			 				}
			 				
			 				if(i == 0){
			 					allowanceSettingDO.setUpdatedon(new Date());
			 					new AllowanceSettingsService().update(allowanceSettingDO);
			 				}
			 				
			 				AllowanceSettingDO allowanceSetting = new AllowanceSettingDO();
					 		if(!inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString().isEmpty()){
					 			allowanceSetting.setAllowancetypeid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ALLOWANCE_TYPE_ID).toString() : ""));
					 		}
					 		allowanceSetting.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : "");
					 		allowanceSetting.setTaxable(!inputJSON.get(CommonConstants.TAXABLE).toString().isEmpty() ? inputJSON.get(CommonConstants.TAXABLE).toString() : "");
					 		allowanceSetting.setType(!inputJSON.get(CommonConstants.TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.TYPE).toString() : "");
					 		allowanceSetting.setFixedamount(!inputJSON.get(CommonConstants.FIXEDAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FIXEDAMOUNT).toString() : "");
					 		allowanceSetting.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
					 		allowanceSetting.setBasicgrosspay(!inputJSON.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? inputJSON.get(CommonConstants.BASICGROSS_PAY).toString() : "");
					 		allowanceSetting.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : "");
					 		allowanceSetting.setNotes(!inputJSON.get(CommonConstants.NOTES).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTES).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			allowanceSetting.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		allowanceSetting.setUpdatedon(new Date());
					 		allowanceSetting.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.STARTDATE).toString()) : null);
					 		allowanceSetting.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.ENDDATE).toString()) : null);
					 		allowanceSetting.setDisplayname(!inputJSON.get(CommonConstants.DISPLAYNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.DISPLAYNAME).toString() : "");
					 		allowanceSetting.setStatus('a');
					 		new AllowanceSettingsService().persist(allowanceSetting);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveAmount", method = RequestMethod.POST)
	public @ResponseBody String retriveById(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					
					List<AllowanceSettingDO> allowanceSettingList = new AllowanceSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AllowanceSettingsUtil.getAllowanceSettingsListForAmount(allowanceSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveAmountWithDate", method = RequestMethod.POST)
	public @ResponseBody String retriveByIdWithDate(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.EMPCTC).toString().isEmpty()){
					List<AllowanceSettingDO> allowanceSettingList = new AllowanceSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = AllowanceSettingsUtil.getAllowanceSettingsListForAmount(allowanceSettingList, Double.parseDouble(inputJSON.get(CommonConstants.EMPCTC).toString())).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
