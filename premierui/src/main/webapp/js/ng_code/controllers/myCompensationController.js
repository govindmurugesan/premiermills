var app = angular.module('spheresuite')
app.controller('myCompensationController', myCompensationController);

myCompensationController.$inject = ['$scope', '$rootScope', '$filter', '$location', '$localStorage', 'employeeService', 'payrollService', 'commonService', 'Upload'];

function myCompensationController($scope, $rootScope, $filter, $location, $localStorage, employeeService, payrollService, commonService, Upload) {
	var myCompensationControllerScope = this;
	$rootScope.headerMenu = "My Compensation";
	myCompensationControllerScope.getEmployeeUser = getEmployeeUser;
	myCompensationControllerScope.getEmpId = getEmpId;
	myCompensationControllerScope.getEmployeeAddress = getEmployeeAddress;
	myCompensationControllerScope.getEmpBankDetails = getEmpBankDetails;
	myCompensationControllerScope.numberToStar = numberToStar;
	myCompensationControllerScope.netTotalMonth;
	myCompensationControllerScope.netTotalYear;
	
	getEmployeeUser();	
	getEmployeeAddress();
	getEmpBankDetails();
	getEmpId();

	function getEmployeeUser() {
		var myUser = $localStorage.spheresuite.id;
		employeeService.getEmployee(myUser).then(function (res) {
			if (res.successflag === 'true' && res.results.length > 0) {
				myCompensationControllerScope.noEmployeeDetail = true;
				myCompensationControllerScope.userEmployeeList = res.results[0];
			} else {
				myCompensationControllerScope.spinner = false;
				myCompensationControllerScope.noEmployeeDetail = false;
				myCompensationControllerScope.dataMsg = "Information Not Avalible";
			}
		}, function (err) {
			myCompensationControllerScope.spinner = false;
		});
	}
	
	function getEmpBankDetails(){
		var myUser = $localStorage.spheresuite.id;
		employeeService.getEmpBankDetails(myUser).then(function(res) { 
            if (res.successflag === 'true' && res.results.length > 0) {
            	myCompensationControllerScope.employeeBankDetailsList = res.results[0];
            	console.log("testststss111  ",myCompensationControllerScope.employeeBankDetailsList);
            	myCompensationControllerScope.employeeBankDetailsList.ifscCode = numberToStar(res.results[0].ifscCode);
            	myCompensationControllerScope.employeeBankDetailsList.accountNumber = numberToStar(res.results[0].accountNumber);
            	myCompensationControllerScope.employeeBankDetailsList.aadhar = numberToStar(res.results[0].aadhar);
            	myCompensationControllerScope.employeeBankDetailsList.panno = numberToStar(res.results[0].panno);
            	console.log("testststss",myCompensationControllerScope.employeeBankDetailsList);
            } else
            	myCompensationControllerScope.spinner = false;
        }, function(err) { 
        });
    }

    function numberToStar(data){
    	trailingCharsIntactCount = 4;
    	if(data){
	    	data = new Array(data.length - trailingCharsIntactCount + 1).join('*') + data.slice( -trailingCharsIntactCount);
	    }
	    return data;
    }
	
	function getEmployeeAddress() {
		var myUser = $localStorage.spheresuite.id;
		employeeService.getEmpAddress(myUser).then(function (res) {
			if (res.successflag === 'true' && res.results.length > 0) {
				myCompensationControllerScope.noEmployeeDetail = true;
				myCompensationControllerScope.employeeAddressList = res.results[0];
			} else {
				myCompensationControllerScope.spinner = false;
			}
		}, function (err) {
			myCompensationControllerScope.spinner = false;
		});
	}

	function getEmpId() {		
		var myUser = $localStorage.spheresuite.id;
		myCompensationControllerScope.CtcShow = true;
		myCompensationControllerScope.spinner = true;		
		if (myUser) {
			payrollService.getLeadByEmp(myUser).then(function (res) {
				if (res.successflag == 'true' && res.results.length > 0) {
					myCompensationControllerScope.CtcShow = true;
					myCompensationControllerScope.leadListByEmp = res.results[0];
					var earlength = myCompensationControllerScope.leadListByEmp.employeeCompensationEarningList.length;
					var dudlength = myCompensationControllerScope.leadListByEmp.employeeCompensationDeductionList.length;
					var taxlength = myCompensationControllerScope.leadListByEmp.employeeCompensationTaxList.length
					
					var netEarningMonth =  myCompensationControllerScope.leadListByEmp.earningMonth;
					var netEarningYear =  myCompensationControllerScope.leadListByEmp.earningYear;
					
					var netDeductionMonth =  myCompensationControllerScope.leadListByEmp.deductionMonth;
					var netDeductionYear =  myCompensationControllerScope.leadListByEmp.deductionYear;
					
					var netTaxMonth =  myCompensationControllerScope.leadListByEmp.taxMonth;
					var netTaxYear =  myCompensationControllerScope.leadListByEmp.taxYear;
					
					myCompensationControllerScope.netTotalMonth = parseFloat(netEarningMonth) - (parseFloat(netDeductionMonth) + parseFloat(netTaxMonth));
					myCompensationControllerScope.netTotalYear = parseFloat(netEarningYear) - (parseFloat(netDeductionYear) + parseFloat(netTaxYear));
					if(earlength > 0 && dudlength >0 && taxlength > 0){
						
					}else{
						//myCompensationControllerScope.CtcShow = false;
					}
					/*var arr = [];
					var len;
					if (earlength > dudlength) {
						arr = myCompensationControllerScope.leadListByEmp.employeeCompensationDeductionList;
						len = earlength;
					} else if (earlength < dudlength) {
						arr = myCompensationControllerScope.leadListByEmp.employeeCompensationEarningList;
						len = dudlength;
					} if (arr.length > 0) {
						for (var i = arr.length - 1; i < dudlength; i++) {
							arr.push({
								allowanceId: "-",
								monthly: "0",
								deductionId: "-",
								ytd: "0"
							});
						}
					}*/
					myCompensationControllerScope.spinner = false;
				} else {
					myCompensationControllerScope.CtcShow = false;
					myCompensationControllerScope.leadListByEmp = [];
					myCompensationControllerScope.ctcMsg = "Compensation Details Not Available";
					myCompensationControllerScope.spinner = false;
				}
			}, function (err) {
				myCompensationControllerScope.spinner = false;
			});
		}
	}





}