package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="itsavings")
@TableGenerator(name ="itsavings", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "itsavings.findAll", query = "SELECT r FROM ItSavingsDO r"),
    @NamedQuery(name = "itsavings.findById", query = "SELECT r FROM ItSavingsDO r where r.id =:id"),
    @NamedQuery(name = "itsavings.findByEmpId", query = "SELECT r FROM ItSavingsDO r where r.empId =:empId"),
    @NamedQuery(name = "itsavings.findByDate", query = "SELECT r FROM ItSavingsDO r where (r.fromYear =:fromDate and r.toYear =:toDate)"),
})
public class ItSavingsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "itsavings.findAll";
	
	public static final String FIND_BY_ID = "itsavings.findById";
	
	public static final String FIND_BY_EMPID = "itsavings.findByEmpId";
	
	public static final String FIND_BY_DATE = "itsavings.findByDate";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "itsavings")
	private Long id;
	private Long itSavingsSettingId;
	private String fromYear;
	private String toYear;
	private String comments;
	private Long empId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Double amount;
	private char status;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getItSavingsSettingId() {
		return itSavingsSettingId;
	}
	public void setItSavingsSettingId(Long itSavingsSettingId) {
		this.itSavingsSettingId = itSavingsSettingId;
	}
	public String getFromYear() {
		return fromYear;
	}
	public void setFromYear(String fromYear) {
		this.fromYear = fromYear;
	}
	public String getToYear() {
		return toYear;
	}
	public void setToYear(String toYear) {
		this.toYear = toYear;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
}