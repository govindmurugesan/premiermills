package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HrPoliciesDAO;
import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.exception.AppException;

public class HrPoliciesService {
	static Logger logger = Logger.getLogger(HrPoliciesService.class.getName());
	
	public HrPoliciesDO persist(HrPoliciesDO hrPoliciesDOList) throws AppException {
		return new HrPoliciesDAO().persist(hrPoliciesDOList);
	}
	
	public List<HrPoliciesDO> retrieveById(Long Id) throws AppException {
		return new HrPoliciesDAO().retrieveById(Id);
	}
	
	public List<HrPoliciesDO> retrieveByRoleId(Long Id) throws AppException {
		return new HrPoliciesDAO().retrieveByRoleId(Id);
	}
	
	public List<HrPoliciesDO> retrieve() throws AppException {
		return new HrPoliciesDAO().retrieve();
	}
	
	public HrPoliciesDO update(HrPoliciesDO hrPoliciesDO) throws AppException {
		return new HrPoliciesDAO().update(hrPoliciesDO);
	}
}
