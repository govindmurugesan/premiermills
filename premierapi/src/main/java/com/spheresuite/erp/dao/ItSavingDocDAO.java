package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class ItSavingDocDAO {
	static Logger logger = Logger.getLogger(ItSavingDocDAO.class.getName());
	private EntityManager em = null;
	
	public boolean delete(Long id) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && id != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q =  em.createQuery("delete from ItSavingsDocDO e where e.itsavingId=:id");
				q.setParameter(CommonConstants.ID, id);
				q.executeUpdate();
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	
	public ItSavingsDocDO persist(ItSavingsDocDO itsavingdoc) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && itsavingdoc != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(itsavingdoc);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itsavingdoc;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDocDO> retrieve() throws AppException {
		List<ItSavingsDocDO> OpportunitiesDocList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDocDO.FIND_ALL);
				OpportunitiesDocList = (List<ItSavingsDocDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return OpportunitiesDocList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ItSavingsDocDO> retrieveByOppId(Long id) throws AppException {
		List<ItSavingsDocDO> OpportunitiesDocList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(ItSavingsDocDO.FIND_BY_ITSAVING_ID);
				q.setParameter(CommonConstants.ID, id);
				OpportunitiesDocList = (List<ItSavingsDocDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return OpportunitiesDocList;
	}
	
	public ItSavingsDocDO update(ItSavingsDocDO itsavingdoc) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(itsavingdoc);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return itsavingdoc;
	}

}
