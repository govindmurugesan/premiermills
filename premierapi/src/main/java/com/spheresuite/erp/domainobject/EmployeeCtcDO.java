package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeectc")
@TableGenerator(name ="employeectc", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeectc.findAll", query = "SELECT r FROM EmployeeCtcDO r where r.status =:status"),
    @NamedQuery(name = "employeectc.findById", query = "SELECT r FROM EmployeeCtcDO r where r.id =:id"),
    @NamedQuery(name = "employeectc.findByStatus", query = "SELECT r FROM EmployeeCtcDO r where r.status =:status and r.empId =:empId"),
    @NamedQuery(name = "employeectc.findByStatusAndDate", query = "SELECT r FROM EmployeeCtcDO r where r.empId =:empId and (r.startdate <=:date and r.enddate >=:date or r.startdate <=:date and r.enddate IS NULL)"),
    @NamedQuery(name = "employeectc.findByEmpId", query = "SELECT r FROM EmployeeCtcDO r where r.empId =:empId"),
    @NamedQuery(name = "employeectc.findByBatchId", query = "SELECT r.empId FROM EmployeeCtcDO r where r.payrollbatchId =:payrollbatchId"),
})
public class EmployeeCtcDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeectc.findAll";
	
	public static final String FIND_BY_ID = "employeectc.findById";
	
	public static final String FIND_BY_STATUS = "employeectc.findByStatus";
	
	public static final String FIND_BY_EMP_ID = "employeectc.findByEmpId";
	
	public static final String FIND_BY_STATUS_DATE = "employeectc.findByStatusAndDate";
	
	public static final String FIND_BY_BATCH_ID = "employeectc.findByBatchId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeectc")
	private Long id;
	private Long empId;
	private Long empctc;
	private char status;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdby;
	private Long payrollbatchId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getEmpctc() {
		return empctc;
	}
	public void setEmpctc(Long empctc) {
		this.empctc = empctc;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public Long getPayrollbatchId() {
		return payrollbatchId;
	}
	public void setPayrollbatchId(Long payrollbatchId) {
		this.payrollbatchId = payrollbatchId;
	}
}