package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="taxslabssetting")
@TableGenerator(name ="taxslabssetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "taxslabssetting.findAll", query = "SELECT r FROM TaxSlabsSettingsDO r"),
    @NamedQuery(name = "taxslabssetting.findById", query = "SELECT r FROM TaxSlabsSettingsDO r where r.id =:id"),
    @NamedQuery(name = "taxslabssetting.findActive", query = "SELECT r FROM TaxSlabsSettingsDO r where r.status =:status"),
    @NamedQuery(name = "taxslabssetting.findByTaxPayee", query = "SELECT r FROM TaxSlabsSettingsDO r where r.status =:status and r.taxpayeeId =:id and r.startdate =:startDate and r.enddate =:enddate"),
})
public class TaxSlabsSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "taxslabssetting.findAll";
	public static final String FIND_BY_ID = "taxslabssetting.findById";
	public static final String FIND_ACTIVE = "taxslabssetting.findActive";
	public static final String FIND_BY_TAXPAYEEID = "taxslabssetting.findByTaxPayee";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "taxslabssetting")
	private Long id;
	private String amountfrom;
	private String amountto;
	private String percentageamount;
	private String startdate;
	private String enddate;
	private char status;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdby;
	private Long taxpayeeId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAmountfrom() {
		return amountfrom;
	}
	public void setAmountfrom(String amountfrom) {
		this.amountfrom = amountfrom;
	}
	public String getAmountto() {
		return amountto;
	}
	public void setAmountto(String amountto) {
		this.amountto = amountto;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public Long getTaxpayeeId() {
		return taxpayeeId;
	}
	public void setTaxpayeeId(Long taxpayeeId) {
		this.taxpayeeId = taxpayeeId;
	}
}