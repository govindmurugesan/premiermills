angular
    .module('spheresuite')
    .service('employeeService', employeeService);

employeeService.$inject = ['constants', 'commonService'];

function employeeService(constants, commonService) {

   var employeeService = {
    addEmployee: addEmployee,
    addEmployeeCompensation : addEmployeeCompensation,
    deleteEmployee: deleteEmployee,
    editEmployee: editEmployee,
    getEmployee: getEmployee,
    getEmployeeCompensation: getEmployeeCompensation,
    getEmployeeByEmail: getEmployeeByEmail,
    getEmployeeId: getEmployeeId,
    importEmployee: importEmployee,
    updateCompensation: updateCompensation,
    updateCompensationDate: updateCompensationDate,
    updateProfilePic: updateProfilePic,
    getEmpSkills: getEmpSkills,
    addSkills: addSkills,
    reqEmpEducation : reqEmpEducation,
    retrieveEmployeeEducation : retrieveEmployeeEducation,
    empFamilyDetail : empFamilyDetail,
    getEmpFamilyDetails : getEmpFamilyDetails,
    updateFamilyDetails : updateFamilyDetails,
    updateempEducationDetails : updateempEducationDetails,
    updateEmpSkillDetails : updateEmpSkillDetails,
    addEmpAddress : addEmpAddress,
    getEmpAddress : getEmpAddress,
    updateEmpAddressDetails : updateEmpAddressDetails,
    addEmpBankDetails : addEmpBankDetails,
    getEmpBankDetails : getEmpBankDetails,
    updateEmpBankDetails : updateEmpBankDetails,
    addEmpTerminateDetails : addEmpTerminateDetails,
    getEmpTerminateDetails : getEmpTerminateDetails,
    addEmpCertificateDetails : addEmpCertificateDetails,
    getEmpCertificateDetails : getEmpCertificateDetails,
    updateEmpCertificateDetails : updateEmpCertificateDetails,
    addEmpAdditionalInformation : addEmpAdditionalInformation,
    getEmpAdditionalInfoDetails : getEmpAdditionalInfoDetails,
    updateEmpAdditionalInfoDetails : updateEmpAdditionalInfoDetails,
    getUserInformationDetails : getUserInformationDetails,
    addUpdateFile : addUpdateFile,
    changeicon : changeicon,
    uploadDocument : uploadDocument,
    getEmpFileUpload : getEmpFileUpload,
    addEmpDriversLicense : addEmpDriversLicense,
    getEmpDriverLicenseDetails : getEmpDriverLicenseDetails,
    updateEmpDriverLicenseDetails : updateEmpDriverLicenseDetails,
    getEmpCompensationDetails : getEmpCompensationDetails,
    addEmpContactInformation : addEmpContactInformation,
    getEmpContactInfoDetails : getEmpContactInfoDetails,
    updateEmpContactInfoDetails : updateEmpContactInfoDetails,
    getRemainingEmployees : getRemainingEmployees,
    sendOnboardRequest : sendOnboardRequest,
    getEmployeeOnboardById : getEmployeeOnboardById,
    getEmployeeOnboard : getEmployeeOnboard,
    updateEmployeeOnboard : updateEmployeeOnboard,
    getEmployeeAssignment : getEmployeeAssignment,
    updateTerminateDetails : updateTerminateDetails,
    gotoCancelTerminateDetails : gotoCancelTerminateDetails,
    getReportingDetails : getReportingDetails
  };

  return employeeService;

  function addEmployee(data) {
    return commonService.apiCall(constants.employeePersist, data);
  }

  function addUpdateFile(data) {
    console.log('addUpdateFile', data);
    return commonService.apiCall(constants.updateFilePersist, data);
  }

  function changeicon(data){
      return commonService.httpCallForUploadingImage(constants.uploadFileChangeicon, data);
  }

  function addSkills(data) {
    return commonService.apiCall(constants.employeeSkillsPersist, data);
  }

  function addEmpContactInformation(data) {
    return commonService.apiCall(constants.employeeContactInfoPersist, data);
  }

  function addEmpDriversLicense(data) {
    return commonService.apiCall(constants.employeeDriverLicensePersist, data);
  }

  function addEmpAdditionalInformation(data) {
    console.log('addEmpAdditionalInformation', data);
    return commonService.apiCall(constants.employeeAdditionalInfoPersist, data);
  }

  function addEmpCertificateDetails(data) {
    return commonService.apiCall(constants.employeeCertificatePersist, data);
  }

  function addEmpTerminateDetails(data) {
    console.log('addEmpTerminateDetails', data);
    return commonService.apiCall(constants.employeeTerminatePersist, data);
  }

  function addEmpBankDetails(data) {
    console.log('addEmpBankDetails', data)
    return commonService.apiCall(constants.employeeBankPersist, data);
  }

  function addEmpAddress(data) {
    console.log('addEmpAddress', data)
    return commonService.apiCall(constants.employeeAddressPersist, data);
  }
 
  function addEmployeeCompensation(data) {
	    return commonService.apiCall(constants.employeecompensationPersist, data);
	  }
  function deleteEmployee(data) {
    return commonService.apiCall(constants.employeeDelete, data);
  }

  function editEmployee(data) {
    return commonService.apiCall(constants.employeeUpdate, data);
  }
  
  function getEmployee(data) {
	    if (!data)
	      return commonService.httpCallGetAll(constants.employeeRetrieve);
	    else {
	    	data = { id : data };
	    	return commonService.httpCallGetById(constants.employeeRetrieveById, data);
	    }
	  }

    function getEmpSkills(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveSkills, data);      
    }

    function getEmpCompensationDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveCompensationDetails, data);      
    }

    function getEmpDriverLicenseDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveDriverLicense, data);      
    }

    function getEmpFileUpload(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveUploadFiles, data);      
    }

    function getUserInformationDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveUserInformation, data);      
    }

    function getEmpAdditionalInfoDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveAdditionalInfo, data);      
    }

    function getEmpCertificateDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveCertificate, data);      
    }

    function getEmpTerminateDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveTerminate, data);      
    }

    function getEmpBankDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveBankDetails, data);      
    }

    function getEmpContactInfoDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveContactInfo, data);      
    }

    function getEmpAddress(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.employeeRetrieveAddress, data);
      
    }

  
  	function getEmployeeCompensation(data)  {
	    if (!data)
		      return commonService.httpCallGetAll(constants.employeeCompensationRetrieve);
		    else {
		    	data = { id : data };
		    	return commonService.httpCallGetById(constants.employeeCompensationRetrieveById, data);
		    }
  }
  
  function getEmployeeByEmail(data) {
	  if(data){
	    	data = { email : data };
	    	return commonService.httpCallGetById(constants.employeeRetrieveByMail, data);
	  }
  }

  function getEmployeeId( ) {  
      return commonService.httpCallGetAll(constants.EmployeeIdRetrieve); 
  }
 
  function importEmployee(data){
	  if(data){
		  return commonService.httpCallGetById(constants.importEmployee, data);
	  }
  }
  

  function updateCompensation(data){ 
		    return commonService.apiCall(constants.employeeCompensationUpdate, data); 
  }

  function updateEmpSkillDetails(data){
     return commonService.apiCall(constants.employeeSkillsUpdate, data); 
  }

  function updateEmpAdditionalInfoDetails(data){
     return commonService.apiCall(constants.employeeAdditionalInfoUpdate, data); 
  }

  function updateEmpCertificateDetails(data){
     return commonService.apiCall(constants.employeeCertificateUpdate, data); 
  }

  function updateEmpBankDetails(data){
     return commonService.apiCall(constants.employeeBankDetailsUpdate, data); 
  }

  function updateEmpContactInfoDetails(data){
     return commonService.apiCall(constants.employeeContactDetailsUpdate, data); 
  }

  function updateEmpDriverLicenseDetails(data){
     return commonService.apiCall(constants.employeeDriverLicenseDetailsUpdate, data); 
  }

  function updateEmpAddressDetails(data){
     return commonService.apiCall(constants.employeeAddressUpdate, data); 
  }
  
  function updateCompensationDate(data){ 
	    return commonService.apiCall(constants.employeeCompensationUpdateDate, data); 
  }

  function updateempEducationDetails(data){ 
      return commonService.apiCall(constants.employeeEditEducation, data); 
  }
  
  function updateProfilePic(data){
	  if(data){
		    return commonService.httpCallForUploadingImage(constants.employeeUpdateProfilePic, data);
	  }
  }

  function uploadDocument(data){
          return commonService.httpCallForUploadingImage(constants.empFileUploadDocument, data);
      }

  function reqEmpEducation(data){
    return commonService.apiCall(constants.employeeEducationDetails, data)
  }

  function retrieveEmployeeEducation(data){
    data = {id: data};
    return commonService.httpCallGetById(constants.employeeEducationRetrieve, data);
  }

  function empFamilyDetail(data){
    return commonService.apiCall(constants.empFamilyInformation, data);
  }

  function updateFamilyDetails(data){
    return commonService.apiCall(constants.updateFamilyInformation, data);
  }

  function getEmpFamilyDetails(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.getEmployeeRetrieveFamilyDetails, data);
      
    }

  function getRemainingEmployees() {
      return commonService.httpCallGetAll(constants.getRemainingEmpDetails);
  }
  
  function sendOnboardRequest(data) {
	  return commonService.apiCall(constants.employeeOnboardPersist, data);
  }
  
  function getEmployeeOnboardById(data) {
	data = { id : data };
	return commonService.httpCallGetById(constants.employeeOnboardRetrieveById, data);
  }
  
  function updateEmployeeOnboard(data){
    return commonService.apiCall(constants.employeeOnboardUpdate, data);
  }
  function getEmployeeOnboard() {
      return commonService.httpCallGetAll(constants.employeeOnboardRetrieveAll);
  }
  
  function getEmployeeAssignment(data) {
      data = { id : data };
      return commonService.httpCallGetById(constants.getEmployeeAssignment, data);      
    }

    function updateTerminateDetails(data){
      console.log('daata', data);
    return commonService.apiCall(constants.updateTerminateDetailsInformation, data);
  }

  function gotoCancelTerminateDetails(data) {
    
    data = { id : data };
    console.log('gotoCancelTerminateDetails', data);
    return commonService.apiCall(constants.cancelTerminatePersist, data);
  }
  
  function getReportingDetails(empId){
	  data = { id : empId };
  	return commonService.httpCallGetById(constants.retrieveEmpReportingById, data);
 
	  
  }
}