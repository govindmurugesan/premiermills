package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OffersDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.RequirementsService;
import com.spheresuite.erp.util.CommonConstants;

public class OffersUtil {
	
	private OffersUtil() {}
	
	public static JSONObject getOffersList(List<OffersDO> offersList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OffersDO offersDO : offersList) {
				resultJSONArray.put(getOffersDetailObject(offersDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getOffersDetailObject(OffersDO offersDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(offersDO.getId() != null ? offersDO.getId() : ""));
		result.put(CommonConstants.PROJECTID, String.valueOf(offersDO.getProjectid() != null ? offersDO.getProjectid() : ""));
		result.put(CommonConstants.CUSTOMERID, String.valueOf(offersDO.getCustomerid() != null ? offersDO.getCustomerid() : ""));
		result.put(CommonConstants.REQUIREMENT_ID, String.valueOf(offersDO.getRequirementid() != null ? offersDO.getRequirementid() : ""));
		result.put(CommonConstants.OFFERDATE, String.valueOf(offersDO.getOfferDate() != null ? offersDO.getOfferDate() : ""));
		result.put(CommonConstants.NAME, String.valueOf(offersDO.getName() != null ? offersDO.getName() : ""));
		result.put(CommonConstants.DESIGNATION, String.valueOf(offersDO.getDesg() != null ? offersDO.getDesg() : ""));
		result.put(CommonConstants.JOININGDATE, String.valueOf(offersDO.getJoiningDate() != null ? offersDO.getJoiningDate() : ""));
		result.put(CommonConstants.COST, String.valueOf(offersDO.getCtc() != null ? offersDO.getCtc() : ""));
		result.put(CommonConstants.COMMENT, String.valueOf(offersDO.getComments() != null ? offersDO.getComments() : ""));
		result.put(CommonConstants.PHONE, String.valueOf(offersDO.getPhone() != null ? offersDO.getPhone() : ""));
		result.put(CommonConstants.ADDRESS, String.valueOf(offersDO.getAddresss() != null ? offersDO.getAddresss() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(offersDO.getStatus()));
		result.put(CommonConstants.APPROVER_ID, String.valueOf(offersDO.getApproverid() != null ? offersDO.getApproverid() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(offersDO.getUpdatedon())));
		if(offersDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(offersDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(offersDO.getProjectid() != null){
			List<OpportunitiesDO> opportunitiesList = new OpportunitiesService().retrieveById(offersDO.getProjectid());
			if(opportunitiesList != null && opportunitiesList.size() > 0){
				result.put(CommonConstants.PROJECTNAME, opportunitiesList.get(0).getProjectname());
			}else{
				result.put(CommonConstants.PROJECTNAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTNAME, "");
		}
		
		if(offersDO.getRequirementid() != null){
			List<RequirementsDO> requirementList = new RequirementsService().retrieveById(offersDO.getRequirementid());
			if(requirementList != null && requirementList.size() > 0){
				result.put(CommonConstants.REQUIREMENT, requirementList.get(0).getName());
			}else{
				result.put(CommonConstants.REQUIREMENT, "");
			}
		}else{
			result.put(CommonConstants.REQUIREMENT, "");
		}
		
		if(offersDO.getCustomerid() != null){
			List<LeadDO> leadList = new LeadService().retrieveById(offersDO.getCustomerid());
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, leadList.get(0).getName());	
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");	
			}
		}else{
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		
		if(offersDO.getApproverid() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(offersDO.getApproverid());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.APPROVER, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.APPROVER, ""); 
			}
		}else{
			result.put(CommonConstants.APPROVER, ""); 
		}
		
		return result;
	}
}
