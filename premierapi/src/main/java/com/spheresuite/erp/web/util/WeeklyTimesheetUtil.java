package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.ProjectService;
import com.spheresuite.erp.service.TimesheetService;
import com.spheresuite.erp.service.TimesheetTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class WeeklyTimesheetUtil {
	
	private WeeklyTimesheetUtil() {}
	
	public static JSONObject WeeklyTimesheetList(List<WeeklyTimesheetDO> timesheetList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WeeklyTimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObject(timesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTimesheetDetailObject(WeeklyTimesheetDO timesheet)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		result.put(CommonConstants.STATUS, String.valueOf(timesheet.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(timesheet.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	
	public static JSONObject EmpWeeklyTimesheetList(List<WeeklyTimesheetDO> weeklyTimesheetList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WeeklyTimesheetDO weeklytimesheet : weeklyTimesheetList) {
				resultJSONArray.put(getEmpTimesheetDetailObject(weeklytimesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmpTimesheetDetailObject(WeeklyTimesheetDO weeklytimesheet)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		List<TimesheetDO> timesheetList = new TimesheetService().retrieveByWeekendDate(weeklytimesheet.getDate(), weeklytimesheet.getEmpId());
		int totalHours = 0;
		JSONArray resultJSONArray = new JSONArray();
		if(timesheetList != null && timesheetList.size() > 0){
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getEmpTimesheetObj(timesheet));
				totalHours = (int) (totalHours + timesheet.getHours());
			}
		}
		
		result.put(CommonConstants.TIMESHEETLIST, resultJSONArray);
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(weeklytimesheet.getDate()));
		result.put(CommonConstants.TOTAl, totalHours);
		result.put(CommonConstants.STATUS, String.valueOf(weeklytimesheet.getStatus()));
		if(weeklytimesheet.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(weeklytimesheet.getEmpId());
			if(empList != null && empList.size() > 0){
				result.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getId()));
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPID, "");
				result.put(CommonConstants.CREATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.CREATED_BY, ""); 
			result.put(CommonConstants.EMPID, "");
		}
		if(timesheetList.size() > 0 &&  timesheetList.get(0).getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(timesheetList.get(0).getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
		
		
	}
	
	public static JSONObject getEmpTimesheetObj(TimesheetDO timesheet)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		/*List<TimesheetTypeDO> timesheetTypeList = new TimesheetTypeService().retrieveById(timesheet.getTimesheetType());
		if(timesheetTypeList != null && timesheetTypeList.size() > 0){
			result.put(CommonConstants.TYPENAME, String.valueOf(timesheetTypeList.get(0).getName()));
		}else{
			result.put(CommonConstants.TYPENAME, String.valueOf(""));
		}*/
		List<ProjectDO> projectList = new ProjectService().retrieveById(timesheet.getTimesheetType());
		if(projectList != null && projectList.size() > 0){
			result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
		}else{
			result.put(CommonConstants.TYPENAME, String.valueOf(""));
		}
 		
 		
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeekenddate()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(timesheet.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
