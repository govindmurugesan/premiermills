package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeBonusUtil {
	
	private EmployeeBonusUtil() {}
	
	public static JSONObject getEmployeeBonusList(List<EmployeeBonusDO> employeeBonusList, String frequency, String period)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBonusDO employeeBonusDO : employeeBonusList) {
				resultJSONArray.put(getEmployeeBonusDetailObject(employeeBonusDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject MultipleMonthBonuReport(List<EmployeeBonusDO> bonusList, String frequency, String period)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBonusDO bonus : bonusList) {
				 /*Map<Long,Long> map=new HashMap<Long,Long>();
				 if(!map.containsKey(bonus.getEmpId())){*/
					 resultJSONArray.put(getEmployeeBonusDetailObject(bonus));
				 /*}*/
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getMultipleMonthBonusDetailObject(List<EmployeeBonusDO> bonusList, Long empId)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		if(empId != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(empId);
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPNAME, ""); 
		}
		Double amount = 0D;
		for(EmployeeBonusDO bonus : bonusList){
			if(bonus.getEmpId().equals(empId)){
				amount = amount+bonus.getAmount();
			}
		}
		result.put(CommonConstants.AMOUNT,String.valueOf(amount)); 
		return result;
	}

	public static JSONObject getEmployeeBonusDetailObject(EmployeeBonusDO employeeBonusDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeBonusDO.getId()));
		if(employeeBonusDO.getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(employeeBonusDO.getEmpId()));
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(employeeBonusDO.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		result.put(CommonConstants.AMOUNT, String.valueOf(employeeBonusDO.getAmount()));
		result.put(CommonConstants.BONUSMONTH, String.valueOf(employeeBonusDO.getBonusMonth()));
		if(employeeBonusDO.getPaidOn() != null){
			result.put(CommonConstants.PAIDON, String.valueOf(employeeBonusDO.getPaidOn()));
		}else{
			result.put(CommonConstants.PAIDON, "");
		}
		result.put(CommonConstants.COMMENT, String.valueOf(employeeBonusDO.getComment() != null ? employeeBonusDO.getComment() : ""));
		result.put(CommonConstants.STATUS, String.valueOf(employeeBonusDO.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeBonusDO.getUpdatedon())));
		if(employeeBonusDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employeeBonusDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		return result;
	}
}
