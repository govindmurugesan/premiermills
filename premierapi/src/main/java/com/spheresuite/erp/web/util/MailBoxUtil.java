package com.spheresuite.erp.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.UIDFolder.FetchProfileItem;
import javax.mail.URLName;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;

import com.auth0.jwt.internal.org.apache.commons.lang3.ArrayUtils;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;

public class MailBoxUtil {
	public static List<LeadEmailDO> getAllInbox(String userName, String password, Long empId) throws Exception {
		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	                props.put("mail.imap.partialfetch","false");
	                props.put("mail.imap.fetchsize", "1048576"); 
	                props.put("mail.imaps.partialfetch", "false"); 
	                props.put("mail.imaps.fetchsize", "1048576");
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			 System.out.println("user   "+userName);
			 System.out.println("Start Time :=tttt=" + new Date());
			 List<LeadEmailDO> emailList = new LeadEmailService().retrieveByLead(empId);
			 for (LeadEmailDO leadEmailDO : emailList) {
				 InputStream is = new ByteArrayInputStream(leadEmailDO.getMsgObject());
				 MimeMessage message = new MimeMessage(session, is);
				 
				 Date date = null;
			        date = CommonUtil.convertEmailDate(message.getSentDate().toString());
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
				   Address[] fromAddress = message.getFrom();
		            leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(fromAddress[0].toString());
			 		leadEmailDO.setSubject(message.getSubject());
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 		leadEmailDO.setUpdatedon(new Date());
			 		leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(fromAddress[0].toString());
			 		leadEmailDO.setSubject(message.getSubject());
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 		leadEmailDO.setUpdatedon(new Date());
					String messageContent = "";
					Object content = message.getContent();
					if (content instanceof String) {
		                messageContent = content.toString();
					}else if (content instanceof Multipart){
			                Multipart multiPart = (Multipart) content;
			                for (int j = 0; j < multiPart.getCount(); j++) {
			                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
			                    Object o = bodyPart.getContent();
			                    String msgContent = getText(multiPart.getBodyPart(j));
		                    	if(msgContent != null ){
		                    		messageContent = msgContent;
		                    	}
			                    
			                    if (o instanceof String) {
			                        messageContent = o.toString();
			                    } else if (null != bodyPart.getDisposition()
			                            && bodyPart.getDisposition().equalsIgnoreCase(
			                                    Part.ATTACHMENT)) {
			                    	
			                        String fileName = bodyPart.getFileName();
			                        leadEmailDO.setAttachFiles(fileName);
			                        InputStream inStream = bodyPart.getInputStream();
			                        byte[] bytes = IOUtils.toByteArray(inStream);
			                        leadEmailDO.setContentType(bodyPart.getContentType());
			                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
		                            inStream.close();
			                    }
			                }
			           }
						leadEmailDO.setMessageContent(messageContent);
				 		leadEmailDOlist.add(leadEmailDO);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		System.out.println("End Time :=tttt=" + new Date());
		return leadEmailDOlist;
	}
	
public static List<LeadEmailDO> addInbox(String userName, String password, Long empId) throws Exception {		
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			Session session = null;
			if (session == null) {
				Properties props = null;
	            try {
	            	props = System.getProperties();
	                props.put("mail.imap.partialfetch","false");
	                props.put("mail.imap.fetchsize", "1048576"); 
	                props.put("mail.imaps.partialfetch", "false"); 
	                props.put("mail.imaps.fetchsize", "1048576");
	            } catch (SecurityException sex) {
	            }
	            session = Session.getInstance(props, null);
	        }
			 System.out.println("user   "+userName);
			 System.out.println("Start Time :=tttt=" + new Date());	 
			 
			Store store = session.getStore(new URLName(CommonConstants.IMAPS,CommonConstants.GMAIL_HOST_POP,Integer.parseInt(CommonConstants.GMAIL_PORT),CommonConstants.INBOX,userName, password));
	        
	        store.connect();
	        
	        Folder inbox = store.getDefaultFolder();
	      
            inbox = inbox.getFolder("INBOX");
            UIDFolder uf = (UIDFolder)inbox; // cast folder to UIDFolder interface
            inbox.open(Folder.READ_WRITE);
            Message[] arrayMessages = inbox.getMessages();
            ArrayUtils.reverse(arrayMessages);
            System.out.println("Start Time :=tttt=" + new Date());    
            FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.ENVELOPE);
            fp.add(FetchProfileItem.FLAGS);
            fp.add(FetchProfileItem.CONTENT_INFO);
            fp.add("X-mailer");
            if (inbox instanceof UIDFolder) {
            	fp.add(UIDFolder.FetchProfileItem.UID);
            }
            inbox.fetch(arrayMessages, fp);
            
          for (Message message : arrayMessages) {
              //Message message = arrayMessages[7];        	  
        	  ByteArrayOutputStream baos = new ByteArrayOutputStream();
              message.writeTo(baos);
             
        	  LeadEmailDO leadEmailDO = new LeadEmailDO();
				Date date = null;
		        date = CommonUtil.convertEmailDate(message.getSentDate().toString());
			    String formattedDate = "";
			    if( date != null ) {
			    	formattedDate = CommonUtil.convertEmailDateToDate( date );
			    }
			 // Address[] fromAddress = message.getFrom();
	 			//leadEmailDO.setContactId(null);
	 			leadEmailDO.setLeadId(empId);
	 			//leadEmailDO.setFromAddress(fromAddress[0].toString());
		 		//leadEmailDO.setSubject(message.getSubject());
		 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
		 		leadEmailDO.setUpdatedon(new Date());
				//leadEmailDO.setuId(uf.getUID(message));
				leadEmailDO.setMsgObject(baos.toByteArray());
				leadEmailDOlist.add(leadEmailDO);				
         }
         
          System.out.println("End Time :=tttt=" + new Date()); 
          new LeadEmailService().persistList(leadEmailDOlist);
		  //return leadEmailDOlist;
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			Object content = p.getContent();
			if (content instanceof String){  
				String s = (String)p.getContent();
				return s;  
			}
			
		}

		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
				}
		}

		return null;
	}


}
