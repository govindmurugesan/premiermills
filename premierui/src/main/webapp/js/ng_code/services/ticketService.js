angular
    .module('spheresuite')
    .service('ticketService', ticketService);

ticketService.$inject = ['constants', 'commonService'];

function ticketService(constants, commonService) {

   var ticketService = {
    getMyViewTickets : getMyViewTickets,
    addNewTicket : addNewTicket,
    getViewTicketsData : getViewTicketsData,
    addNewViewTickets : addNewViewTickets,
    getNewViewTickets : getNewViewTickets
  };

  return ticketService;

  function getMyViewTickets(data){
    console.log('ticketService');
    return commonService.httpCallGetTicket(constants.viewTicketsRetrieve);
  }

  function getViewTicketsData(data){
    console.log('ticketService');
    return commonService.httpCallGetTicketData(constants.viewTicketsDataRetrieve);
  }

  function addNewTicket(data) {
    console.log('addNewTicket', data);
    return commonService.apiCall(constants.addNewTicketPersist, data);
  }

  function addNewViewTickets(data) {
    console.log('addNewTicket', data);
    return commonService.apiCall(constants.addNewViewTicketPersist, data);
  }

  function getNewViewTickets(data){
    return commonService.httpCallGetNewViewTickets(constants.getNewViewTicketsRetrieve);
  }
}