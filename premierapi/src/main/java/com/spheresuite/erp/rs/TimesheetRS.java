package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.TimesheetService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.service.WeeklyTimesheetService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TimesheetUtil;
import com.spheresuite.erp.web.util.WebManager;
import com.spheresuite.erp.web.util.WeeklyTimesheetUtil;

@Controller
@RequestMapping(value = "/timesheet")
public class TimesheetRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TimesheetRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				WeeklyTimesheetDO weeklyTimesheetDO = new WeeklyTimesheetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty()){
						List<WeeklyTimesheetDO> weeklyTimeSheet = new WeeklyTimesheetService().retrieveByDate(inputJSON.get(CommonConstants.WEEKENDDATE).toString(), Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
						if(weeklyTimeSheet != null && weeklyTimeSheet.size() == 0){
							weeklyTimesheetDO.setDate(inputJSON.get(CommonConstants.WEEKENDDATE).toString());
							weeklyTimesheetDO.setUpdatedon(new Date());
							weeklyTimesheetDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
							if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								weeklyTimesheetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
								weeklyTimesheetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
							new WeeklyTimesheetService().persist(weeklyTimesheetDO);
						}
					}
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.TIMESHEETLIST ).toString().isEmpty()){
			 			org.json.simple.JSONArray timeSheetList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.TIMESHEETLIST).toString());
			 			for (int i = 0; i < timeSheetList.size(); i++) {
			 				TimesheetDO timesheetDO = new TimesheetDO();
				 			JSONObject inputJSON1 = (JSONObject) timeSheetList.get(i);
				 			timesheetDO.setDescription(!inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty() ? inputJSON1.get(CommonConstants.DESCRIPTION).toString() : null);
					 		timesheetDO.setDate(!inputJSON1.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON1.get(CommonConstants.DATE).toString() : null);
					 		
					 		if(!inputJSON1.get(CommonConstants.HOURS).toString().isEmpty()){
					 			timesheetDO.setHours(Long.parseLong(inputJSON1 != null ? inputJSON1.get(CommonConstants.HOURS).toString() : ""));
					 		}
					 		//if(inputJSON1.get(CommonConstants.TYPE).toString().isEmpty()){
					 			timesheetDO.setTimesheetType(Long.parseLong(inputJSON != null ? inputJSON1.get(CommonConstants.TYPE).toString() : ""));
					 		//}
					 			
					 		timesheetDO.setWeekenddate(!inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEEKENDDATE).toString() : null);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			timesheetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			timesheetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		timesheetDO.setUpdatedon(new Date());
					 		new TimesheetService().persist(timesheetDO);
			 		}
			 		}
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "TimeSheet Created");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveEmpWeekelyTimesheet/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty()){
					List<WeeklyTimesheetDO> weeklyTimeSheetDetail = new WeeklyTimesheetService().retrieveByDate(inputJSON.get(CommonConstants.WEEKENDDATE).toString(), Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
					if(weeklyTimeSheetDetail != null && weeklyTimeSheetDetail.size() > 0){
						List<TimesheetDO> TimeSheetList = new TimesheetService().retrieveByWeekendDate(weeklyTimeSheetDetail.get(0).getDate(), Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
						
						respJSON = TimesheetUtil.getTimesheetListWithStatus(TimeSheetList, weeklyTimeSheetDetail.get(0).getStatus()).toString();
					}else{
						respJSON = WeeklyTimesheetUtil.WeeklyTimesheetList(weeklyTimeSheetDetail).toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveEmpTimesheet/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveEmpTimesheet(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<WeeklyTimesheetDO> weeklyTimeSheetDetail = new WeeklyTimesheetService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
					if(weeklyTimeSheetDetail != null && weeklyTimeSheetDetail.size() > 0){
						respJSON = WeeklyTimesheetUtil.EmpWeeklyTimesheetList(weeklyTimeSheetDetail).toString();
					}else{
						respJSON = WeeklyTimesheetUtil.WeeklyTimesheetList(weeklyTimeSheetDetail).toString();
					}
					
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> deptIds = new ArrayList<Long>();
				int i = 0;
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
				}
				if(i == 1){
					List<WeeklyTimesheetDO> weeklyTimeSheetDetail = new WeeklyTimesheetService().retrieve();
					respJSON = WeeklyTimesheetUtil.EmpWeeklyTimesheetList(weeklyTimeSheetDetail).toString();
				}else{
					List<WeeklyTimesheetDO> weeklyTimeSheetDetail = new WeeklyTimesheetService().retrieveByDept(empIds);
					respJSON = WeeklyTimesheetUtil.EmpWeeklyTimesheetList(weeklyTimeSheetDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty()){
						List<WeeklyTimesheetDO> weeklyTimeSheet = new WeeklyTimesheetService().retrieveByDate(inputJSON.get(CommonConstants.WEEKENDDATE).toString(), Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
						WeeklyTimesheetDO weeklyTimesheetDO = new WeeklyTimesheetDO();
						if(weeklyTimeSheet != null && weeklyTimeSheet.size() > 0){
							weeklyTimesheetDO = weeklyTimeSheet.get(0);
							//weeklyTimesheetDO.setDate(inputJSON.get(CommonConstants.WEEKENDDATE).toString());
							weeklyTimesheetDO.setUpdatedon(new Date());
							if(inputJSON.get(CommonConstants.COMMENT) != null){
								weeklyTimesheetDO.setComments(CommonConstants.COMMENT.toString());
							}
							weeklyTimesheetDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
							if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
								weeklyTimesheetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
							new WeeklyTimesheetService().update(weeklyTimesheetDO);
						}
					}
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.TIMESHEETLIST ).toString().isEmpty()){
			 			org.json.simple.JSONArray timeSheetList = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.TIMESHEETLIST).toString());
			 			for (int i = 0; i < timeSheetList.size(); i++) {
				 			JSONObject inputJSON1 = (JSONObject) timeSheetList.get(i);
				 			TimesheetDO timesheetDO = new TimesheetDO();
				 			if(inputJSON1.get(CommonConstants.ID) != null){
				 				List<TimesheetDO> TimeSheetList = new TimesheetService().retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 				if(TimeSheetList != null && TimeSheetList.size() > 0){
				 				timesheetDO = TimeSheetList.get(0);
				 				timesheetDO.setDescription(!inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty() ? inputJSON1.get(CommonConstants.DESCRIPTION).toString() : null);
						 		timesheetDO.setDate(!inputJSON1.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON1.get(CommonConstants.DATE).toString() : null);
						 		if(!inputJSON1.get(CommonConstants.HOURS).toString().isEmpty()){
						 			timesheetDO.setHours(Long.parseLong(inputJSON1 != null ? inputJSON1.get(CommonConstants.HOURS).toString() : ""));
						 		}
						 		timesheetDO.setTimesheetType(Long.parseLong(inputJSON != null ? inputJSON1.get(CommonConstants.TYPE).toString() : ""));
						 		timesheetDO.setWeekenddate(!inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEEKENDDATE).toString() : null);
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			timesheetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			timesheetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
						 		}
						 		timesheetDO.setUpdatedon(new Date());
						 		new TimesheetService().update(timesheetDO);
				 				}
				 			}else{
				 				timesheetDO.setDescription(!inputJSON1.get(CommonConstants.DESCRIPTION).toString().isEmpty() ? inputJSON1.get(CommonConstants.DESCRIPTION).toString() : null);
						 		timesheetDO.setDate(!inputJSON1.get(CommonConstants.DATE).toString().isEmpty() ? inputJSON1.get(CommonConstants.DATE).toString() : null);
						 		
						 		
				 				if(!inputJSON1.get(CommonConstants.HOURS).toString().isEmpty()){
						 			timesheetDO.setHours(Long.parseLong(inputJSON1 != null ? inputJSON1.get(CommonConstants.HOURS).toString() : ""));
						 		}
						 		if(inputJSON1.get(CommonConstants.TYPE).toString().isEmpty()){
						 			timesheetDO.setTimesheetType(Long.parseLong(inputJSON != null ? inputJSON1.get(CommonConstants.TYPE).toString() : ""));
						 		}
						 			
						 		timesheetDO.setWeekenddate(!inputJSON.get(CommonConstants.WEEKENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEEKENDDATE).toString() : null);
						 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			timesheetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 			timesheetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
						 		}
						 		timesheetDO.setUpdatedon(new Date());
						 		new TimesheetService().persist(timesheetDO);
				 			}
				 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "TimeSheet Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveSubmittedTimesheet/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveSubmittedTimesheet(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
					List<WeeklyTimesheetDO> weeklyTimeSheetDetail = new WeeklyTimesheetService().retrieveSubmittedTimesheet();
					if(weeklyTimeSheetDetail != null && weeklyTimeSheetDetail.size() > 0){
						respJSON = WeeklyTimesheetUtil.EmpWeeklyTimesheetList(weeklyTimeSheetDetail).toString();
					}else{
						respJSON = WeeklyTimesheetUtil.WeeklyTimesheetList(weeklyTimeSheetDetail).toString();
					}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
