package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.service.ContactTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ContactTypeUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/contacttype")
public class ContactTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ContactTypeRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ContactTypeDO contactTypeDO = new ContactTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		contactTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			contactTypeDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		contactTypeDO.setUpdatedon(new Date());
			 		contactTypeDO.setStatus('a');
			 	}
				new ContactTypeService().persist(contactTypeDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ContactTypeDO> contactTypeList = new ContactTypeService().retrieveActive();
				respJSON = ContactTypeUtil.getContactTypeList(contactTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ContactTypeDO> contactTypeList = new ContactTypeService().retrieve();
				respJSON = ContactTypeUtil.getContactTypeList(contactTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContactTypeDO contactTypeDO = new ContactTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ContactTypeDO> contactTypeList = new ContactTypeService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		contactTypeDO = contactTypeList.get(0);
			 		contactTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		contactTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactTypeDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		contactTypeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new ContactTypeService().update(contactTypeDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
