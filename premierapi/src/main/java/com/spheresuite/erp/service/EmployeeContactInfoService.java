package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeContactInfoDAO;
import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeContactInfoService {
	static Logger logger = Logger.getLogger(EmployeeContactInfoService.class.getName());
	
	public EmployeeContactInfoDO persist(EmployeeContactInfoDO employeeContactInfoDO) throws AppException {
		return new EmployeeContactInfoDAO().persist(employeeContactInfoDO);
	}

	public List<EmployeeContactInfoDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeContactInfoDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeContactInfoDO> retrieveById(Long id) throws AppException {
		return new EmployeeContactInfoDAO().retrieveById(id);
	}
	
	public EmployeeContactInfoDO update(EmployeeContactInfoDO employeeContactInfoDO) throws AppException {
		return new EmployeeContactInfoDAO().update(employeeContactInfoDO);
	}
}
