
var app = angular.module('spheresuite')
app.controller('assignmentController', assignmentController);

assignmentController.$inject = ['$scope', '$rootScope', 'assignmentService', '$location', 'commonService', '$localStorage'];

function assignmentController($scope, $rootScope, assignmentService, $location, commonService, $localStorage) {
    var assignmentControllerScope = this;
    $scope.format = "MMM dd, yyyy";
    
    $rootScope.limitToShow = 10;
    $rootScope.beginFrom = 0;
    
    assignmentControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
	assignmentControllerScope.isNextDisabled = false;
	assignmentControllerScope.showPrevNav = showPrevNav;
    assignmentControllerScope.showNextNav = showNextNav;
    assignmentControllerScope.gotoPage = gotoPage;
    assignmentControllerScope.buttonBeginFrom = 0;
    
    
    assignmentControllerScope.startDateCalender = false;
    assignmentControllerScope.endDateCalender = false;
    assignmentControllerScope.showAssignment;
    assignmentControllerScope.isDataAvailable = false;
    assignmentControllerScope.getAssignmentList = [];
    
     $rootScope.headerMenu = "Project";
    assignmentControllerScope.openStartDateCalender = openStartDateCalender;
    assignmentControllerScope.openEndDateCalender = openEndDateCalender;
    assignmentControllerScope.getActiveUser = getActiveUser;
    assignmentControllerScope.getLeadType = getLeadType;
    assignmentControllerScope.getOpportunityForProject = getOpportunityForProject;
    assignmentControllerScope.getProjectName = getProjectName;
    assignmentControllerScope.addAssignment = addAssignment;
    assignmentControllerScope.updateAssignment = updateAssignment;
    assignmentControllerScope.decline = decline;
    assignmentControllerScope.getAssignment = getAssignment;
    assignmentControllerScope.gotoView = gotoView;
    assignmentControllerScope.gotoEditAssignment = gotoEditAssignment;
    assignmentControllerScope.getAssignmentById = getAssignmentById;
    assignmentControllerScope.goBackToAssignments = goBackToAssignments;
    
    if ($location.path() == '/assignment/add') {
    	assignmentControllerScope.isUpdate = false;
        getActiveUser();
        getLeadType();
        getPaymentTerms();
    }else if($location.path() == '/assignments'){
        getAssignment();
    }else if($location.path() == '/assignment/view'){
        getAssignment($localStorage.spheresuite.assignId);
    }else if($location.path() == '/assignment/edit'){
    	assignmentControllerScope.isUpdate = true;
    	getActiveUser();
        getLeadType();
        getPaymentTerms();
        getOpportunityForProject($localStorage.spheresuite.customerId);
    	getAssignmentById($localStorage.spheresuite.assignId);
    	getProjectName($localStorage.spheresuite.opportunityId);
    }
    
    function openStartDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        assignmentControllerScope.startDateCalender = !assignmentControllerScope.startDateCalender
    };

    function openEndDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        assignmentControllerScope.endDateCalender = !assignmentControllerScope.endDateCalender
    };
    
    function goBackToAssignments(){
    	$location.path('/assignments');
    }

    function showPrevNav(){
		assignmentControllerScope.buttonBeginFrom -= assignmentControllerScope.buttonLimitToShow;
    	gotoPage(assignmentControllerScope.buttonBeginFrom)
		assignmentControllerScope.isNextDisabled = false;
	}
	
	function showNextNav(){
		assignmentControllerScope.buttonBeginFrom += assignmentControllerScope.buttonLimitToShow;
		gotoPage(assignmentControllerScope.buttonBeginFrom)
		if((assignmentControllerScope.buttonBeginFrom + assignmentControllerScope.buttonLimitToShow) * assignmentControllerScope.buttonLimitToShow >= assignmentControllerScope.getAssignmentList.length)
			assignmentControllerScope.isNextDisabled = true;
	}
    
	function gotoPage(index, activeIndex){
    	$rootScope.beginFrom = index;
        if(assignmentControllerScope.getAssignmentList.length > 0){
    	for(var i = 0; i < assignmentControllerScope.getAssignmentList.length; i++){
        	assignmentControllerScope.getAssignmentList[i].isActiveClass = false;
    	}
    	if(!activeIndex)
    		activeIndex = index
    	assignmentControllerScope.getAssignmentList[activeIndex].isActiveClass = true;
    }
    }
	
    function getActiveUser(){
        assignmentControllerScope.spinner = true;
        assignmentService.getActiveUser().then(function(res){
            console.log('getActiveUser res',res)
            if(res.successflag == 'true' && res.results.length > 0){
                assignmentControllerScope.employeeAssignmentActiveList = res.results;
            }
            assignmentControllerScope.spinner = false;
        },function(err){
            assignmentControllerScope.spinner = false;
        });
    }

    function getLeadType() {
        assignmentControllerScope.spinner = true;
        assignmentService.getLeadType().then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
                assignmentControllerScope.leadTypeList = res.results;                
            }
        }, function(err) {
            assignmentControllerScope.spinner = false;
        });
    }


    function getOpportunityForProject(data){
        assignmentControllerScope.spinner = true;
        assignmentService.getOpportunityByCustomerId(data).then(function(res){
            console.log('getOpportunityForProject',res);
            if(res.successflag == 'true' && res.results.length > 0){

               assignmentControllerScope.opportunityForProjectList = res.results;
            }
            assignmentControllerScope.spinner = false;
        },function(err){
            assignmentControllerScope.spinner = false;
        });
    }

    function getProjectName(data){
        assignmentControllerScope.spinner = true;
        if(data){
        assignmentService.getProjectName(data).then(function(res){
            console.log('getProjectName',res);
            if(res.successflag == 'true' && res.results.length > 0){
               assignmentControllerScope.getValueForProjectList = res.results;
            }
            assignmentControllerScope.spinner = false;
        },function(err){
            assignmentControllerScope.spinner = false;
        });
    }else{
        assignmentControllerScope.spinner = false;
    }
    }

    function getPaymentTerms(){
      assignmentControllerScope.spinner = true;
        assignmentService.getPaymentTerms().then(function(res){
            console.log('getOpportunityForProject',res);
            if(res.successflag == 'true' && res.results.length > 0){
                console.log('getPaymentTerms', res)
               assignmentControllerScope.paymentTermsList = res.results;
            }
        },function(err){
            assignmentControllerScope.spinner = false;
        });  
    }

    function addAssignment() {
        assignmentControllerScope.spinner = true;
        if (assignmentControllerScope.employee) {
            commonService.formValNotManditory(assignmentControllerScope.employeeForm, assignmentControllerScope.employee).then(function(data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    delete $localStorage.spheresuite.assignId;
                	delete $localStorage.spheresuite.customerId;
                	delete $localStorage.spheresuite.opportunityId;
                    assignmentService.addProject(data).then(function(res) {
                        if (res.successflag === 'true') {                            
                            assignmentControllerScope.spinner = false;
                            $('#assignmentModal').modal('show');
                        } else {
                            assignmentControllerScope.spinner = false;
                            assignmentControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        assignmentControllerScope.spinner = false;
                    });
                } else {
                    assignmentControllerScope.spinner = false;
                }
            });
        } else
            assignmentControllerScope.spinner = false;
    }
    
    function updateAssignment() {
        assignmentControllerScope.spinner = true;
        if (assignmentControllerScope.employee) {
            commonService.formValNotManditory(assignmentControllerScope.employeeForm, assignmentControllerScope.employee).then(function(data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    assignmentService.updateAssignment(data).then(function(res) {
                        if (res.successflag === 'true') {                            
                            assignmentControllerScope.spinner = false;
                            //$('#assignmentModal').modal('show');
                            $location.path('/assignments')
                        } else {
                            assignmentControllerScope.spinner = false;
                            assignmentControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        assignmentControllerScope.spinner = false;
                    });
                } else {
                    assignmentControllerScope.spinner = false;
                }
            });
        } else
            assignmentControllerScope.spinner = false;
    }

    function decline() {         
            assignmentControllerScope.employee = null;
            assignmentControllerScope.employeeForm.$setPristine();
            assignmentControllerScope.employeeForm.$setUntouched();
            assignmentControllerScope.isUpdate = false;
    }

    function getAssignment(data){
        assignmentControllerScope.spinner = true;
        assignmentControllerScope.isDataAvailable = true;
        assignmentControllerScope.isNextDisabled = false;
        assignmentService.getAssignment(data).then(function(res){
            console.log('getAssignment res',res)
            if(res.successflag == 'true' && res.results.length > 0){
                assignmentControllerScope.getAssignmentList = res.results;
                assignmentControllerScope.spinner = false;

                assignmentControllerScope.assignmentListBackup = angular.copy(assignmentControllerScope.getAssignmentList);
                assignmentControllerScope.isDataAvailable = true;

                for(var i = 0; i < assignmentControllerScope.assignmentListBackup.length; i++){
                    assignmentControllerScope.assignmentListBackup[i].isActiveClass = false;
                    assignmentControllerScope.assignmentListBackup[i].isSelect = false;
                }

                assignmentControllerScope.getAssignmentList =  angular.copy(assignmentControllerScope.assignmentListBackup);
                if((assignmentControllerScope.buttonBeginFrom + assignmentControllerScope.buttonLimitToShow) * assignmentControllerScope.buttonLimitToShow >= assignmentControllerScope.getAssignmentList.length)
                    assignmentControllerScope.isNextDisabled = true;
                
                assignmentControllerScope.getAssignmentList[0].isActiveClass = true;
                assignmentControllerScope.spinner = false;
            }
            
            else {          	
           	
           	assignmentControllerScope.spinner = false;
           }
        },function(err){
            assignmentControllerScope.spinner = false;
        });
    }
    
    function getAssignmentById(data){
        assignmentControllerScope.spinner = true;
        assignmentService.getAssignment(data).then(function(res){
            console.log('getAssignment res',res)
            if(res.successflag == 'true' && res.results.length > 0){
                assignmentControllerScope.employee = res.results[0];
                assignmentControllerScope.spinner = false;
            }
        },function(err){
            assignmentControllerScope.spinner = false;
        });
    }

    function gotoView(assignment) {
        if (assignment) {
        	$localStorage.spheresuite.assignId = assignment.id;
            $location.path('/assignment/view');
        }
    }
    
    function gotoEditAssignment(assignment) {
        if (assignment) {
        	assignmentControllerScope.isUpdate = true;
        	$localStorage.spheresuite.assignId = assignment.id;
        	$localStorage.spheresuite.customerId = assignment.customerId;
        	$localStorage.spheresuite.opportunityId = assignment.opportunityId;
            $location.path('/assignment/edit');
        }
    }
    
    $(".select1").select2();
}