var app = angular.module('spheresuite')
app.controller('payslipController', payslipController);

payslipController.$inject = ['$scope', '$rootScope', 'payrollService', '$localStorage', 'employeeService', '$q', 'payrollService', 'companyInformationService', '$filter', '$location', 'employeeService'];

function payslipController($scope, $rootScope, payrollService, $localStorage, employeeService, $q, payrollService, companyInformationService, $filter, $location, employeeService) {
	var payslipControllerScope = this;

	$rootScope.headerMenu = "Payroll";
	payslipControllerScope.downloadPDF = downloadPDF;
	payslipControllerScope.showPrevNav = showPrevNav;
	payslipControllerScope.showNextNav = showNextNav;
	payslipControllerScope.gotoPage = gotoPage;
	payslipControllerScope.payslipListBackup = [];
	payslipControllerScope.getEmpPayslip = getEmpPayslip;
	payslipControllerScope.goBackToSearchSection = goBackToSearchSection;
	payslipControllerScope.isDataAvailable = true;
	var employeeId;
	if ($location.path() == '/payroll/mypayslips') { 
		employeeId = $localStorage.spheresuite.id;
		data = {id: $localStorage.spheresuite.id};
		getPayDetails(data);
	}
	
	if ($location.path() == '/payroll/employeepayslips') { 
		getEmpList();
	}
	
	payslipControllerScope.searchSection = true;
	
	$rootScope.limitToShow = 10;
    $rootScope.beginFrom = 0;
    payslipControllerScope.isNextDisabled = false;
    payslipControllerScope.buttonBeginFrom = 0;
    payslipControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
	
	function getEmpList(){
		payslipControllerScope.spinner = true;
		employeeService.getEmployee().then(function(res) {
			  if (res.successflag == 'true' && res.results.length > 0) {
				  payslipControllerScope.empList = res.results;
			  }else {
				  payslipControllerScope.isDataAvailable = false;
				  payslipControllerScope.dataMsg = "Employee Not available";
		      }
			  payslipControllerScope.spinner = false;
		}, function(err) {
			payslipControllerScope.dataMsg =  "Something went wrong try after sometime";
			payslipControllerScope.spinner = false;
		});
	}
	
	function getEmpPayslip(empInfo){
		employeeId = payslipControllerScope.employee.ID;
		data = {id: payslipControllerScope.employee.ID};
		getPayDetails(data);
		
	}
	
	function goBackToSearchSection(){
		payslipControllerScope.searchSection = true;
	}
	
	function getPayDetails(data){
		payslipControllerScope.isDataAvailable = true;
		payslipControllerScope.spinner = true;
		payrollService.getpayrollDetailByEmpId(data).then(function (res) {
			if (res.successflag == 'true') {
				if(res.results.length > 0){
					payslipControllerScope.searchSection = false;
					angular.forEach(res.results,function(piece,index){
						if(piece.payrollMonth){
							var monthInDigit = getMonthInDigit(piece.payrollMonth.substring(0,3));
							var dt = new Date(piece.payrollMonth.substring(3),monthInDigit);
					        var month = dt.getMonth(),
					            year = dt.getFullYear();
					        piece.payPeriodFrom = new Date(year, month, 1);
					        piece.payPeriodTo = new Date(year, month + 1, 0);
						}
					})
					payslipControllerScope.payrollDetailList = res.results;
					payslipControllerScope.payslipListBackup = angular.copy(payslipControllerScope.payrollDetailList);
		            payslipControllerScope.isDataAvailable = true;

		        	for(var i = 0; i < payslipControllerScope.payslipListBackup.length; i++){
		            	payslipControllerScope.payslipListBackup[i].isActiveClass = false;
		            	payslipControllerScope.payslipListBackup[i].isSelect = false;
		        	}
					payslipControllerScope.payrollDetailList =  angular.copy(payslipControllerScope.payslipListBackup);
		        	if((payslipControllerScope.buttonBeginFrom + payslipControllerScope.buttonLimitToShow) * payslipControllerScope.buttonLimitToShow >= payslipControllerScope.payrollDetailList.length)
		        		payslipControllerScope.isNextDisabled = true;
					payslipControllerScope.payrollDetailList[0].isActiveClass = true;
				}else{
					payslipControllerScope.isDataAvailable = false;
					payslipControllerScope.dataMsg = "Payslips not available";
				}
			}
			payslipControllerScope.spinner = false;
		}, function (err) {
			payslipControllerScope.spinner = false;
			payslipControllerScope.isDataAvailable = false;
			payslipControllerScope.dataMsg = "Something went wrong try after sometime";
		});
	}
	
	function getMonthInDigit(monthString){
		var monthInDigit;
		if(monthString == 'Jan'){
			monthInDigit = 0;
		}else if(monthString == 'Feb'){
			monthInDigit = 1;
		}else if(monthString == 'Mar'){
			monthInDigit = 2;
		}else if(monthString == 'Apr'){
			monthInDigit = 3;
		}else if(monthString == 'May'){
			monthInDigit = 4;
		}else if(monthString == 'Jun'){
			monthInDigit = 5;
		}else if(monthString == 'Jul'){
			monthInDigit = 6;
		}
		else if(monthString == 'Aug'){
			monthInDigit = 7;
		}else if(monthString == 'Sep'){
			monthInDigit = 8;
		}else if(monthString == 'Oct'){
			monthInDigit = 9;
		}else if(monthString == 'Nov'){
			monthInDigit = 10;
		}else{
			monthInDigit = 11;
		}
		return monthInDigit;
	}
	
	function toDataURL(url, callback) {
	  var xhr = new XMLHttpRequest();
	  xhr.onload = function() {
	    var reader = new FileReader();
	    reader.onloadend = function() {
	      callback(reader.result);
	    }
	    reader.readAsDataURL(xhr.response);
	  };
	  xhr.open('GET', url);
	  xhr.responseType = 'blob';
	  xhr.send();
	}
	
	var empBankDetails;
	var empDetails;
	var empAddress;
	var companyInfo;
	var logo;
	
		
	function downloadPDF(payslipDetail){
		toDataURL('images/i_logo.png', function(dataUrl) {
			  if(dataUrl) logo = dataUrl;
			  else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
		var doc = new jsPDF('a3');// a4 220*300
	    payslipControllerScope.spinner = true; 
		var promises = [];
		var deferred = $q.defer();
		promises.push(getEmpBankInfo());
		promises.push(getEmployee());  
		promises.push(getEmpAddress());
		promises.push(getCompanyInformation());
		
		$q.all(promises).then(function (data) {
			//header
			doc.setFont("Poppins");
			doc.setFontSize(15);
			doc.setFontType("bold");
			doc.text(10, 13,'PAYSLIP');
			doc.addImage(logo, 'PNG', 138, 3, 65, 20);
			doc.setFontSize(10);
			if(payslipDetail.payrollMonth){
				var monthInDigit = getMonthInDigit(payslipDetail.payrollMonth.substring(0,3));
				var dt = new Date(payslipDetail.payrollMonth.substring(3),monthInDigit);
		        var month = dt.getMonth(),
		            year = dt.getFullYear();
		        var payPeriodFrom = new Date(year, month, 1);
		        var payPeriodTo = new Date(year, month + 1, 0);
		        payPeriodFrom = $filter('date')(payPeriodFrom, "dd MMM yyyy");
		        payPeriodTo = $filter('date')(payPeriodTo, "dd MMM yyyy");
		        doc.setFontType("normal");
		        doc.text(10, 19, "( "+payPeriodFrom+" - "+ payPeriodTo+" )");
			}
	        
			doc.setFontType("normal");
			doc.setDrawColor(0,37,77);
			//doc.setLineWidth(0.1);
			doc.line(10, 24, 200, 24);
			//emp info
			doc.setFontSize(12);
			doc.setFontType("bold");
			doc.text(10, 33, 'Employee');
			doc.setFontType("normal");
			doc.setDrawColor(232,232,232);
			doc.setLineWidth(0.3);
			doc.line(10, 35, 80, 35);
			doc.setFontSize(9);
			doc.setFontType("bold");
			doc.text(10, 42, payslipDetail.name);
			
			if(empAddress){
				doc.setFontSize(8);
				doc.setFontType("normal");
				doc.text(10, 47, empAddress.address1);
				
				if(empAddress.address2){
					doc.text(10, 52, empAddress.address2);
					doc.text(10, 57, empAddress.city+" "+empAddress.zip);
					doc.text(10, 62, empAddress.stateName+" "+empAddress.countryName);
				}else{
					doc.text(10, 52, empAddress.city+" "+empAddress.zip);
					doc.text(10, 57, empAddress.stateName+" "+empAddress.countryName);
				}
			}
			
			//basic
			doc.setFontSize(12);
			doc.setFontType("bold");
			doc.text(10, 72, "Basic Information");
			doc.setFontType("normal");
			doc.setDrawColor(232,232,232);
			doc.setLineWidth(0.3);
			doc.line(10, 74, 80, 74);
	    	doc.setFontSize(8);
			doc.setFontType("normal");
			doc.text(10, 81, "Emp Id");
			if(empDetails.empId) doc.text(80, 81, empDetails.empId, null, null, 'right');
			else doc.text(80, 81, "-", null, null, 'right');
			
			
			doc.text(10, 87, "Designation");
			if(empDetails.title) doc.text(80, 87, empDetails.title, null, null, 'right');
			else doc.text(80, 87, "-", null, null, 'right');
			if(!empBankDetails){
				doc.text(10, 93, "Bank Name");
				doc.text(80, 93, "-", null, null, 'right');
				doc.text(10, 100, "Bank IFSC");
				doc.text(80, 100, "-", null, null, 'right')
				doc.text(10, 106, "Bank A/c");
				doc.text(80, 106, "-", null, null, 'right')
			}else{
				doc.text(10, 93, "Bank Name");
				if(empBankDetails.bankName) doc.text(80, 93, empBankDetails.bankName, null, null, 'right')
				else doc.text(80, 93, "", null, null, 'right')
				
				doc.text(10, 100, "Bank IFSC");
				if(empBankDetails.ifscCode) doc.text(80, 100, empBankDetails.ifscCode, null, null, 'right')
				else doc.text(80, 100, "", null, null, 'right')
				
				doc.text(10, 106, "Bank A/c");
				if(empBankDetails.accountNumber) doc.text(80, 106, empBankDetails.accountNumber, null, null, 'right')
				else doc.text(80, 106, "", null, null, 'right')
			}
			
			doc.text(10, 112, "ADHAAR #");
			if(empDetails.aadhar) doc.text(80, 112, empDetails.aadhar, null, null, 'right')
			else doc.text(80, 112, "-", null, null, 'right')
			
			doc.text(10, 118, "PAN #");
			if(empDetails.panno) doc.text(80, 118, empDetails.panno, null, null, 'right')
			else doc.text(80, 118, "-", null, null, 'right')
			
			//earning
			doc.setFontSize(12);
			doc.setFontType("bold");
			doc.text(92, 33, 'Earnings / Allowances');
			doc.setFontType("normal");
			doc.setDrawColor(232,232,232);
			doc.setLineWidth(0.3);
			doc.line(92, 35, 200, 35);
			doc.setFontSize(8);
			var lastLineSection1 = 42;
			for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
				doc.setFontSize(8);
				doc.setFontType("normal");
				doc.text(92, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].earningName);
				doc.text(200, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
				lastLineSection1 = lastLineSection1+ 6;
			}
			doc.setDrawColor(232,232,232);
			doc.setLineWidth(0.3);
			doc.line(170, lastLineSection1, 200, lastLineSection1);
			doc.setFontType("bold");
			doc.setFontSize(9);
			doc.text(92, lastLineSection1+6, 'Total ( A )');
			doc.text(200, lastLineSection1+6, payslipDetail.earningMonth, null, null, 'right');
			
			//Deductions
			if(payslipDetail.employeeCompensationDeductionList){
				doc.setFontSize(12);
				doc.setFontType("bold");
				doc.text(92, lastLineSection1+16, 'Deductions');
				doc.setFontType("normal");
				doc.setDrawColor(232,232,232);
				doc.setLineWidth(0.3);
				doc.line(92, lastLineSection1+18, 200, lastLineSection1+18);
				var lastLineSection2 = lastLineSection1+25;
				for(var i=0; i< payslipDetail.employeeCompensationDeductionList.length; i++){
					doc.setFontSize(8);
					doc.setFontType("normal");
					doc.text(92, lastLineSection2, payslipDetail.employeeCompensationDeductionList[i].deductionName);
					doc.text(200, lastLineSection2, payslipDetail.employeeCompensationDeductionList[i].monthly, null, null, 'right');
					lastLineSection2 = lastLineSection2+6;
				}
				
				doc.setDrawColor(232,232,232);
				doc.setLineWidth(0.3);
				doc.line(170, lastLineSection2, 200, lastLineSection2);
				doc.setFontType("bold");
				doc.setFontSize(9);
				doc.text(92, lastLineSection2+6, 'Total ( B )');
				if(payslipDetail.deductionMonth) doc.text(200, lastLineSection2+6, payslipDetail.deductionMonth, null, null, 'right');
			}
			
			//Tax
			if(payslipDetail.employeeCompensationTaxList){
				if(!payslipDetail.employeeCompensationDeductionList) lastLineSection2 = lastLineSection1+16; 
				doc.setFontSize(12);
				doc.setFontType("bold");
				doc.text(92, lastLineSection2+16, 'Taxes');
				doc.setFontType("normal");
				doc.setDrawColor(232,232,232);
				doc.setLineWidth(0.3);
				doc.line(92, lastLineSection2+18, 200, lastLineSection2+18);
				var lastLineSection3 = lastLineSection2+25;
				for(var i=0; i< payslipDetail.employeeCompensationTaxList.length; i++){
					doc.setFontSize(8);
					doc.setFontType("normal");
					doc.text(92, lastLineSection3, payslipDetail.employeeCompensationTaxList[i].taxName);
					doc.text(200, lastLineSection3, payslipDetail.employeeCompensationTaxList[i].monthly, null, null, 'right');
					lastLineSection3 = lastLineSection3+6 ;
				}
				doc.setDrawColor(232,232,232);
				doc.setLineWidth(0.3);
				doc.line(170, lastLineSection3, 200, lastLineSection3);
				doc.setFontType("bold");
				doc.setFontSize(9);
				if(payslipDetail.employeeCompensationDeductionList)  doc.text(92, lastLineSection3+6, 'Total ( C )');
				else  doc.text(92, lastLineSection3+6, 'Total ( B )');
				if(payslipDetail.taxMonth) doc.text(200, lastLineSection3+6, payslipDetail.taxMonth, null, null, 'right');
			}
			var finalSection;
			if(payslipDetail.employeeCompensationDeductionList && payslipDetail.employeeCompensationTaxList) finalSection = lastLineSection3+22;
			if(!payslipDetail.employeeCompensationDeductionList && payslipDetail.employeeCompensationTaxList) finalSection = lastLineSection1+22;
			if(payslipDetail.employeeCompensationDeductionList && !payslipDetail.employeeCompensationTaxList) finalSection = lastLineSection3+16;
			if(!payslipDetail.employeeCompensationDeductionList && !payslipDetail.employeeCompensationTaxList) finalSection = lastLineSection1+22;
				doc.setDrawColor(232,232,232);
				doc.setLineWidth(0.3);
				doc.line(170, finalSection, 200, finalSection);
				doc.setFontType("bold");
				doc.setFontSize(12);
				if(payslipDetail.employeeCompensationEarningList && payslipDetail.employeeCompensationDeductionList && payslipDetail.employeeCompensationEarningList) doc.text(92, finalSection+6, 'Net Amount ( A - ( B+C ) )');
				if((payslipDetail.employeeCompensationEarningList && !payslipDetail.employeeCompensationDeductionList && payslipDetail.employeeCompensationTaxList) || (payslipDetail.employeeCompensationEarningList && payslipDetail.employeeCompensationDeductionList && !payslipDetail.employeeCompensationTaxList)) doc.text(92, finalSection+6, 'Net Amount ( A - B )');
				if(payslipDetail.employeeCompensationEarningList && !payslipDetail.employeeCompensationDeductionList && !payslipDetail.employeeCompensationTaxList) doc.text(92, finalSection+6, 'Net Amount ( A )');
				if(payslipDetail.taxMonth) doc.text(200, finalSection+6, payslipDetail.netMonth, null, null, 'right');
			
			//footer
			doc.setFontType("normal");
			doc.setDrawColor(0,37,77);
			doc.line(10, 268, 200, 268);
			doc.setFontType("bold");
			doc.setFontSize(12);
			
			//companyInfo
			
			if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
		    else doc.text(110, 274, companyInfo.name , null, null, 'center');
			
			doc.setFontType("normal");
			doc.setFontSize(10);
			
			if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
			if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
			if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
			if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
			if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
			if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
			
			if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
			if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
			payslipControllerScope.spinner = false;
			doc.save("Payslip_"+payslipDetail.name+"_"+payslipDetail.payrollMonth);
		})
		
		})
		
	}
	
	function getEmpBankInfo(){
		var deferred = $q.defer();
		employeeService.getEmpBankDetails(employeeId).then(function(res) { 
	        if (res.successflag === 'true') empBankDetails = res.results[0];
	        else empBankDetails = "";
        	deferred.resolve();
		}, function(err) { 
			deferred.reject();
		});
		 return deferred.promise;
	}
	
	function getEmployee() {
		var deferred = $q.defer();
        employeeService.getEmployee(employeeId).then(function(res) {
        	if (res.successflag === 'true') empDetails = res.results[0];
	        else empDetails = ""; 
        	deferred.resolve();
        }, function(err) {
        	deferred.reject();
        });
        return deferred.promise;
	}
    
	function getEmpAddress() { 
		var deferred = $q.defer();
        employeeService.getEmpAddress(employeeId).then(function(res) { 
            if (res.successflag === 'true') {
                empAddress = res.results[0];
            }
            deferred.resolve();
        }, function(err) { 
        	deferred.reject();
        });
        return deferred.promise;
    }
	
	function getCompanyInformation() {
		var deferred = $q.defer();
        companyInformationService.getCompanyInformation().then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
            	companyInfo = res.results[0];    
            } 
            deferred.resolve();
        }, function(err) {
        	deferred.reject();
        });
        return deferred.promise;
    }
	
	 function showPrevNav(){
  		payslipControllerScope.buttonBeginFrom -= payslipControllerScope.buttonLimitToShow;
      	gotoPage(payslipControllerScope.buttonBeginFrom)
  		payslipControllerScope.isNextDisabled = false;
  }
  
  function showNextNav(type){
 	 if(type == 'compo') {
 		 list =  payslipControllerScope.employeeCompensationList;
 	 }else{
 		 list =  payslipControllerScope.payrollDetailList;
 	 }
 	payslipControllerScope.buttonBeginFrom += payslipControllerScope.buttonLimitToShow;
  	gotoPage(payslipControllerScope.buttonBeginFrom)
  	if((payslipControllerScope.buttonBeginFrom + payslipControllerScope.buttonLimitToShow) * payslipControllerScope.buttonLimitToShow >= list.length)
  		payslipControllerScope.isNextDisabled = true;
  } 
  
  function gotoPage(index, activeIndex){
  	$rootScope.beginFrom = index;
  	if(payslipControllerScope.payrollDetailList.length > 0){
	    	for(var i = 0; i < payslipControllerScope.payrollDetailList.length; i++){
	        	console.log(' data[i].isActiveClass', payslipControllerScope.payrollDetailList[i].isActiveClass);
	        	payslipControllerScope.payrollDetailList[i].isActiveClass = false;
	    	}
	    	if(!activeIndex)
	    		activeIndex = index
	    	payslipControllerScope.payrollDetailList[activeIndex].isActiveClass = true;
	    } 

  }
	
	$('.select1').select2();
	
}