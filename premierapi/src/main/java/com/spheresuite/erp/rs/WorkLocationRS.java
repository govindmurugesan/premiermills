package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;
import com.spheresuite.erp.web.util.WorkLocationUtil;

@Controller
@RequestMapping(value = "/worklocation")
public class WorkLocationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(WorkLocationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				WorkLocationDO workLocationDO = new WorkLocationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		workLocationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty()? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		workLocationDO.setCountryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY_ID).toString() : ""));
			 		workLocationDO.setStateId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE_ID).toString() : ""));
			 		workLocationDO.setUpdatedon(new Date());
			 		workLocationDO.setStatus('a');
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			workLocationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			workLocationDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new WorkLocationService().persist(workLocationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WorkLocationDO> workLocationList = new WorkLocationService().retrieveActive();
				respJSON = WorkLocationUtil.getworkLocationList(workLocationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<WorkLocationDO> workLocationList = new WorkLocationService().retrieve();
				respJSON = WorkLocationUtil.getworkLocationList(workLocationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				WorkLocationDO workLocationDO = new WorkLocationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<WorkLocationDO> workLocation = new WorkLocationService().retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		workLocationDO = workLocation.get(0);
			 		workLocationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty()? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		workLocationDO.setCountryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY_ID).toString() : ""));
			 		workLocationDO.setStateId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE_ID).toString() : ""));
			 		workLocationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			workLocationDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		workLocationDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new WorkLocationService().update(workLocationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
