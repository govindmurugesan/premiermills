package com.spheresuite.erp.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.LeaveTypeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class LeaveTypeDAO {
	static Logger logger = Logger.getLogger(LeaveTypeDAO.class.getName());
	private EntityManager em = null;
	
	public LeaveTypeDO persist(LeaveTypeDO leaveType) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leaveType != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(leaveType);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveType;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeaveTypeDO> retrieve() throws AppException {
		List<LeaveTypeDO> leaveTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveTypeDO.FIND_ALL);
				leaveTypeList = (List<LeaveTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveTypeList;
	}
	
	public LeaveTypeDO update(LeaveTypeDO requestType) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(requestType);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return requestType;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveTypeDO> retrieveById(Long Id) throws AppException {
		List<LeaveTypeDO> leaveTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveTypeDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				leaveTypeList = (List<LeaveTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveTypeDO> retrieveByType(String type) throws AppException {
		List<LeaveTypeDO> leaveTypeList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveTypeDO.FIND_BY_TYPE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				leaveTypeList = (List<LeaveTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveTypeList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveTypeDO> retrieveByTypeUpdate(String type, Long Id) throws AppException {
		List<LeaveTypeDO> leaveTypeList = null;
		List<Long> ids = new ArrayList<Long>();
		ids.add(Id);
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveTypeDO.FIND_BY_TYPE_FOR_UPDATE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				q.setParameter(CommonConstants.ID, ids);
				leaveTypeList = (List<LeaveTypeDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveTypeList;
	}
	

}
