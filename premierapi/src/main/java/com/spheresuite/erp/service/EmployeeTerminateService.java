package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeTerminateDAO;
import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeTerminateService {
	static Logger logger = Logger.getLogger(EmployeeTerminateService.class.getName());
	
	public EmployeeTerminateDO persist(EmployeeTerminateDO employeeTerminateDO) throws AppException {
		return new EmployeeTerminateDAO().persist(employeeTerminateDO);
	}

	public List<EmployeeTerminateDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeTerminateDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeTerminateDO> retrieveById(Long id) throws AppException {
		return new EmployeeTerminateDAO().retrieveById(id);
	}
	
	public List<EmployeeTerminateDO> delete(Long id) throws AppException {
		return new EmployeeTerminateDAO().delete(id);
	}
	
	public List<EmployeeTerminateDO> retrieve() throws AppException {
		return new EmployeeTerminateDAO().retrieve();
	}
	
	public EmployeeTerminateDO update(EmployeeTerminateDO EmployeeTerminateDO) throws AppException {
		return new EmployeeTerminateDAO().update(EmployeeTerminateDO);
	}
}
