package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeeducation")
@TableGenerator(name ="employeeeducation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeeducation.findAll", query = "SELECT r FROM EmployeeEducationDO r"),
    @NamedQuery(name = "employeeeducation.findById", query = "SELECT r FROM EmployeeEducationDO r where r.id =:id"),
    @NamedQuery(name = "employeeeducation.findByEmpId", query = "SELECT r FROM EmployeeEducationDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeEducationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeeducation.findAll";
	
	public static final String FIND_BY_ID = "employeeeducation.findById";
	
	public static final String FIND_BY_EMPID = "employeeeducation.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeeducation")
	private Long id;
	private Long educationLevelId;
	private String area;
	private String university;
	private Long countryId;
	private Long stateId;
	private String city;
	private String yearPassed;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEducationLevelId() {
		return educationLevelId;
	}
	public void setEducationLevelId(Long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getUniversity() {
		return university;
	}
	public void setUniversity(String university) {
		this.university = university;
	}
	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getYearPassed() {
		return yearPassed;
	}
	public void setYearPassed(String yearPassed) {
		this.yearPassed = yearPassed;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}