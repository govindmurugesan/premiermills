package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AssignedMenuDAO;
import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.exception.AppException;

public class AssignedMenuService {
	static Logger logger = Logger.getLogger(AssignedMenuService.class.getName());
	
	public List<AssignedMenuDO> persist(List<AssignedMenuDO> assignedMenuDO) throws AppException {
		return new AssignedMenuDAO().persist(assignedMenuDO);
	}

	public List<AssignedMenuDO> retrieveByRoleId(Long id) throws AppException {
		return new AssignedMenuDAO().retrieveByRoleId(id);
	}
	
	public List<AssignedMenuDO> update(List<AssignedMenuDO> assignedMenuDO) throws AppException {
		return new AssignedMenuDAO().update(assignedMenuDO);
	}
}
