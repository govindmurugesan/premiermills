package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeaveManagementDAO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.exception.AppException;

public class LeaveManagementService {
	static Logger logger = Logger.getLogger(LeaveManagementService.class.getName());
	
	public LeaveManagementDO persist(LeaveManagementDO leaveDetail) throws AppException {
		return new LeaveManagementDAO().persist(leaveDetail);
	}
	
	public List<LeaveManagementDO> retrieve() throws AppException {
		return new LeaveManagementDAO().retrieve();
	}
	
	public List<LeaveManagementDO> retrieveByDept(List<Long> ids) throws AppException {
		return new LeaveManagementDAO().retrieveByDept(ids);
	}
	
	public List<LeaveManagementDO> retrieveById(Long Id) throws AppException {
		return new LeaveManagementDAO().retrieveById(Id);
	}
	
	public LeaveManagementDO update(LeaveManagementDO leaveDetail) throws AppException {
		return new LeaveManagementDAO().update(leaveDetail);
	}
	
	/*public List<LeaveManagementDO> retriveByEmpId(Long id) throws AppException {
		return new LeaveManagementDAO().retrieveByEmpId(id);
	}*/
	
	public List<LeaveManagementDO> retriveActiveById(Long id) throws AppException {
		return new LeaveManagementDAO().retriveActiveById(id);
	}
	
	public List<LeaveManagementDO> retrieveByType(Long id) throws AppException {
		return new LeaveManagementDAO().retrieveByType(id);
	}
	
	public List<LeaveManagementDO> retrieveAllActive() throws AppException {
		return new LeaveManagementDAO().retrieveAllActive();
	}
	
	
	
	
	
}
