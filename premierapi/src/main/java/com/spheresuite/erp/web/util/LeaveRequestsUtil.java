package com.spheresuite.erp.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.domainobject.LeaveTypeDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeaveRequestsService;
import com.spheresuite.erp.service.LeaveTypeService;
import com.spheresuite.erp.util.CommonConstants;

public class LeaveRequestsUtil {
	
	private LeaveRequestsUtil() {}
	
	public static JSONObject getLeaveRequestsList(List<LeaveRequestsDO> leaveRequestsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
				resultJSONArray.put(getLeaveRequestsDetailObject(leaveRequest));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getLeaveRequestsDetailObject(LeaveRequestsDO leaveRequest)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(leaveRequest.getId()));
		result.put(CommonConstants.DESC, String.valueOf(leaveRequest.getDescription()));
		result.put(CommonConstants.LEAVEDAY, String.valueOf(leaveRequest.getLeaveDay()));
		
		if(leaveRequest.getRequestType() != null){
			List<LeaveTypeDO> leaveType = new LeaveTypeService().retrieveById(leaveRequest.getRequestType());
			if(leaveType != null && leaveType.size() > 0){
				if(leaveType.get(0).getType() != null){
					result.put(CommonConstants.TYPE, String.valueOf(leaveType.get(0).getId()));
					result.put(CommonConstants.TYPENAME, String.valueOf(leaveType.get(0).getType()));
				}
				else{
					result.put(CommonConstants.TYPE, "");
					result.put(CommonConstants.TYPENAME, "");
				}
			}else{
				result.put(CommonConstants.TYPE, "");
				result.put(CommonConstants.TYPENAME, "");
			}
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getUpdatedon())));
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithtime(leaveRequest.getCreatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(leaveRequest.getStatus()));
		if(leaveRequest.getComments() != null ){
			result.put(CommonConstants.COMMENT, String.valueOf(leaveRequest.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(leaveRequest.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(leaveRequest.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.UPDATED_BY, "");
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(leaveRequest.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(leaveRequest.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.CREATED_BY, "");
			}
		}else{
			result.put(CommonConstants.CREATED_BY, "");
		}
		if(leaveRequest.getDate() != null){
			result.put(CommonConstants.DATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDateWithLocale(leaveRequest.getDate()))));
		}else{
			result.put(CommonConstants.DATE, "");
		}
		if(leaveRequest.getFromDate()!= null){
			result.put(CommonConstants.FROMDATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDateWithLocale(leaveRequest.getFromDate()))));
		}else{
			result.put(CommonConstants.FROMDATE, "");
		}
		if(leaveRequest.getToDate() != null){
			result.put(CommonConstants.TODATE, String.valueOf(CommonUtil.convertDateToStringWithdatetime(CommonUtil.convertStringToSqlDateWithLocale(leaveRequest.getToDate()))));
		}else{
			result.put(CommonConstants.TODATE, "");
		}
		if(leaveRequest.getNumberofhours() != null){
			result.put(CommonConstants.HOURS, String.valueOf(leaveRequest.getNumberofhours()));
		}else{
			result.put(CommonConstants.HOURS, "");
		}
		return result;
	}
	
	public static JSONObject getEmpLeaveRequestsList(List<LeaveRequestsDO> leaveRequestsList)throws AppException, ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			JSONObject result = new JSONObject();
			int leaveCount = 0;
			for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
				if(String.valueOf(leaveRequest.getLeaveDay()).equals("Single")){
					leaveCount = leaveCount+1;
				}
				if(String.valueOf(leaveRequest.getLeaveDay()).equals("Multiple")){
					SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
					 /*String dateBeforeString = leaveRequest.getFromDate();
					 String dateAfterString = leaveRequest.getToDate();
					 Date dateBefore = myFormat.parse(dateBeforeString);
				     Date dateAfter = myFormat.parse(dateAfterString);
				     long difference = dateAfter.getTime() - dateBefore.getTime();
				     int daysBetween = (int) (difference / (1000*60*60*24));*/
					 String dateBeforeString = leaveRequest.getFromDate();
					 String dateAfterString = leaveRequest.getToDate();
					 Date dateBefore = myFormat.parse(dateBeforeString);
				     Date dateAfter = myFormat.parse(dateAfterString);
					 Calendar start = Calendar.getInstance();
					 start.setTime(dateBefore);
					 Calendar end = Calendar.getInstance();
					 end.setTime(dateAfter);
					
					 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
						Calendar weekend = Calendar.getInstance();
						weekend.setTime(date);
						if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
							weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
						     /*long difference = dateAfter.getTime() - dateBefore.getTime();
						     int daysBetween = (int) (difference / (1000*60*60*24));*/
						     leaveCount = leaveCount++;
						}
					}
				     
				}
			}
			result.put(CommonConstants.COUNT, leaveCount);
			resultJSONArray.put(result);
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getLeaveStatus(List<UserDO> activeUserList,List<LeaveManagementDO> leaveLists)throws AppException, ParseException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for(LeaveManagementDO leaveDetail : leaveLists){
				for (UserDO user : activeUserList) {
					resultJSONArray.put(getUserDetailObject(user,leaveDetail));
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getUserDetailObject(UserDO user, LeaveManagementDO leaveDetail)throws JSONException, AppException, ParseException {
		JSONObject result = new JSONObject();
		List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(user.getEmpId());
		if(empList != null && empList.size() > 0){
			if(empList.get(0).getMiddlename() != null){
				result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
			}
			else{
				result.put(CommonConstants.NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}
		}
		if(empList.get(0).getTitle() != null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(empList.get(0).getTitle()));
		}else{
			result.put(CommonConstants.DESIGNATION,"");
		}
		result.put(CommonConstants.EMPID, String.valueOf(empList.get(0).getId()));
		List<LeaveRequestsDO> leaveRequestsList = new LeaveRequestsService().retrieveAvailableLeavesByEmpId(user.getEmpId(),leaveDetail.getFromDate().toString(),leaveDetail.getToDate().toString(),leaveDetail.getLeaveType());
		int leaveCount = 0;
		for (LeaveRequestsDO leaveRequest : leaveRequestsList) {
			if(String.valueOf(leaveRequest.getLeaveDay()).equals("Single")){
				leaveCount = leaveCount+1;
			}
			if(String.valueOf(leaveRequest.getLeaveDay()).equals("Multiple")){
				SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
				/* String dateBeforeString = leaveRequest.getFromDate();
				 String dateAfterString = leaveRequest.getToDate();
				 Date dateBefore = myFormat.parse(dateBeforeString);
			     Date dateAfter = myFormat.parse(dateAfterString);
			     long difference = dateAfter.getTime() - dateBefore.getTime();
			     int daysBetween = (int) (difference / (1000*60*60*24));
			     leaveCount = leaveCount + daysBetween + 1;*/
				String dateBeforeString = leaveRequest.getFromDate();
				 String dateAfterString = leaveRequest.getToDate();
				 Date dateBefore = myFormat.parse(dateBeforeString);
			     Date dateAfter = myFormat.parse(dateAfterString);
				 Calendar start = Calendar.getInstance();
				 start.setTime(dateBefore);
				 Calendar end = Calendar.getInstance();
				 end.setTime(dateAfter);
				
				 for (Date date = start.getTime(); start.before(end) || start.equals(end); start.add(Calendar.DATE, 1), date = start.getTime()) {
					Calendar weekend = Calendar.getInstance();
					weekend.setTime(date);
					if (weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && 
						weekend.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY){
					     /*long difference = dateAfter.getTime() - dateBefore.getTime();
					     int daysBetween = (int) (difference / (1000*60*60*24));*/
					     leaveCount = leaveCount++;
					}
				}
			}
		}
		result.put(CommonConstants.USEDLEAVES, leaveCount);
		result.put(CommonConstants.TYPE, leaveDetail.getLeaveType());
		result.put(CommonConstants.DAYS, leaveDetail.getNumberOfDays());
		List<LeaveTypeDO> leaveTypeList = new LeaveTypeService().retrieveById(leaveDetail.getLeaveType());
		result.put(CommonConstants.TYPENAME, leaveTypeList.get(0).getType());
		return result;
	}
}
