package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="addresstype")
@TableGenerator(name ="addresstype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "addresstype.findAll", query = "SELECT r FROM AddressTypeDO r"),
    @NamedQuery(name = "addresstype.findById", query = "SELECT r FROM AddressTypeDO r where r.id =:id"),
    @NamedQuery(name = "addresstype.findByStatus", query = "SELECT r FROM AddressTypeDO r where r.status =:status"),
})
public class AddressTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "addresstype.findAll";
	
	public static final String FIND_BY_ID = "addresstype.findById";
	
	public static final String FIND_BY_STATUS = "addresstype.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "addresstype")
	private Long id;
	private String name;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}