package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.RecentActivityDAO;
import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.exception.AppException;

public class RecentActivityService {
	static Logger logger = Logger.getLogger(RecentActivityService.class.getName());
	
	public RecentActivityDO persist(RecentActivityDO recentActivityDO) throws AppException {
		return new RecentActivityDAO().persist(recentActivityDO);
	}

	public List<RecentActivityDO> retrieveById(Long id) throws AppException {
		return new RecentActivityDAO().retrieveById(id);
	}
	
	public List<RecentActivityDO> retrieve() throws AppException {
		return new RecentActivityDAO().retrieve();
	}
	
	public RecentActivityDO update(RecentActivityDO recentActivityDO) throws AppException {
		return new RecentActivityDAO().update(recentActivityDO);
	}
}
