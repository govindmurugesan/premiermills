package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PayrollEarningsDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.exception.AppException;

public class PayrollEarningsDetailsService {
	static Logger logger = Logger.getLogger(PayrollEarningsDetailsService.class.getName());
	
	public List<PayrollEarningsDetailsDO> persist(List<PayrollEarningsDetailsDO> employeeEarningsDO) throws AppException {
		return new PayrollEarningsDetailsDAO().persist(employeeEarningsDO);
	}

	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new PayrollEarningsDetailsDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<PayrollEarningsDetailsDO> retrieve() throws AppException {
		return new PayrollEarningsDetailsDAO().retrieve();
	}
	
	public PayrollEarningsDetailsDO update(PayrollEarningsDetailsDO employeeEarningsDO) throws AppException {
		return new PayrollEarningsDetailsDAO().update(employeeEarningsDO);
	}
	
	public List<PayrollEarningsDetailsDO> retrieveByEmpCompensationIds(List<Long> id) throws AppException {
		return new PayrollEarningsDetailsDAO().retrieveByEmpCompensationIds(id);
	}
}
