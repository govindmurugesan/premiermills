package com.spheresuite.erp.rs;

import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.AttachmentsDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.AttachmentsService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.GmailUtil;
import com.spheresuite.erp.web.util.LeadEmailUtil;
import com.spheresuite.erp.web.util.MailBoxUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/email")
public class EmailRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmailRS.class.getName());

	@RequestMapping(value = "/retrieveInbox", method = RequestMethod.POST)
	public @ResponseBody String retrieveInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			/*Long empId = null;
			 			String emailType = null;
			 			String emailId = null;
			 			String emailPassword = null;
			 			empId = request.getSession().getAttribute("empIds") != null ? Long.parseLong(request.getSession().getAttribute("empIds").toString()) : null;
		 				if(empId != null && empId == Long.parseLong(inputJSON.get(CommonConstants.ID).toString())){
		 					emailType = request.getSession().getAttribute("emailType") != null ? request.getSession().getAttribute("emailType").toString() : null;
		 					emailPassword = request.getSession().getAttribute("emailPassword") != null ? request.getSession().getAttribute("emailPassword").toString() : null;
		 					emailId = request.getSession().getAttribute("emailId") != null ? request.getSession().getAttribute("emailId").toString() : null;
			 				if(emailType != null && emailType.equalsIgnoreCase(CommonConstants.GMAIL)){
			 					if(emailId != null && emailPassword != null){
			 						List<LeadEmailDO> LeadList = GmailUtil.getByMessageUid(emailId, emailPassword,561);
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
			 					}
			 				}
			 			}*/
				 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().equalsIgnoreCase(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				List<LeadEmailDO> LeadList = GmailUtil.getAllInbox(employeeList.get(0).getEmailId(), stringDecode.toString());
					 				System.out.println("leadList  ==  "+LeadList.size());
					 				//List<LeadEmailDO> LeadList = MailBoxUtil.getAllInbox(employeeList.get(0).getEmailId(), employeeList.get(0).getEmailPassword(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 				if(LeadList.size() > 0){
					 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
					 				}
					 				//resultJSON.put(CommonConstants.ERRORS, "");
					 				//JSONArray resultJSONArray = new JSONArray(arrayMessages);
					 				//for (LeadEmailDO leadEmail : leadEmailEmailList) {
					 					//resultJSONArray.put(getLeadEmailDetailObject(leadEmail));
					 				//}
					 				
					 				//resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
					 				//responseJSON.put(CommonConstants.RESPONSE, resultJSON);
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/addInbox", method = RequestMethod.POST)
	public @ResponseBody String addInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){			 			
				 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeList != null && employeeList.size() > 0){
				 			if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().equalsIgnoreCase(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				MailBoxUtil.addInbox(employeeList.get(0).getEmailId(), stringDecode.toString(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					 				respJSON = CommonWebUtil.buildSuccessResponse().toString();
				 				}
				 			}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveMessage", method = RequestMethod.POST)
	public @ResponseBody String retrieveMessage(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty() && !inputJSON.get(CommonConstants.ID).toString().isEmpty() && !inputJSON.get(CommonConstants.LABEL).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().equalsIgnoreCase(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				List<LeadEmailDO> LeadList = GmailUtil.getByMessageUid(employeeList.get(0).getEmailId(), stringDecode.toString(), Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON.get(CommonConstants.LABEL).toString());
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadEmailDO> emailList = new LeadEmailService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					List<LeadEmailDO> emailListNew = GmailUtil.getSingleMessage(emailList);
					/*List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeUtil.getEmployeeList(employeeList).toString();*/
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSent", method = RequestMethod.POST)
	public @ResponseBody String retrieveSent(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeList != null && employeeList.size() > 0){
				 			//if(employeeList.get(0).getEmailType() != null && employeeList.get(0).getEmailType().equalsIgnoreCase(CommonConstants.GMAIL)){
				 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
					 				List<LeadEmailDO> LeadList = GmailUtil.getAllSentMail(employeeList.get(0).getEmailId(), stringDecode.toString());
					 				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				 				}
				 			//}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public @ResponseBody String sendEmail(Model model, HttpServletRequest request,HttpServletResponse response) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.FROM).toString().isEmpty()){
			 			String host = "mail.premiermills.com";
			 			String fromEmail = !inputJSON.get(CommonConstants.FROM).toString().isEmpty()?inputJSON.get(CommonConstants.FROM).toString():"";
			 			String toEmails = !inputJSON.get(CommonConstants.TO).toString().isEmpty()?inputJSON.get(CommonConstants.TO).toString():"";
			 			String emailBody =  !inputJSON.get(CommonConstants.BODY).toString().isEmpty()?inputJSON.get(CommonConstants.BODY).toString():"";
			 			String ccMails =  !inputJSON.get(CommonConstants.CC).toString().isEmpty()?inputJSON.get(CommonConstants.CC).toString():"";
			 			String subject =  !inputJSON.get(CommonConstants.SUBJECT).toString().isEmpty()?inputJSON.get(CommonConstants.SUBJECT).toString():"";
			 			String bccMails =  !inputJSON.get(CommonConstants.BCC).toString().isEmpty()?inputJSON.get(CommonConstants.BCC).toString():"";
			 			byte[] decodedBytes = Base64.getDecoder().decode(inputJSON.get(CommonConstants.PASSWORD).toString());
						String password = new String(decodedBytes, "UTF-8");
						String fileList = !inputJSON.get(CommonConstants.FILE).toString().isEmpty()?inputJSON.get(CommonConstants.FILE).toString():"";
						
						System.out.println("password    "+fromEmail);
			 			boolean mailStatus = EmailProxyUtil.sendEmails(request, host, fromEmail, toEmails, ccMails, bccMails, subject, emailBody, password.toString(), fileList); 
						if(mailStatus){
							return CommonWebUtil.buildSuccessResponse().toString();
						}else{
							return CommonWebUtil.buildErrorResponse("").toString();
						}
			 		}
			 	}*/
				if (request.getParameter("from") != null && request.getParameter("to") != null){
					String host = "mail.premiermills.com";
		 			String fromEmail = request.getParameter(CommonConstants.FROM);
		 			String toEmails = request.getParameter(CommonConstants.TO);
		 			String emailBody =  request.getParameter(CommonConstants.BODY);
		 			String ccMails =  request.getParameter(CommonConstants.CC);
		 			String subject = request.getParameter(CommonConstants.SUBJECT);
		 			String bccMails =  request.getParameter(CommonConstants.BCC);
		 			byte[] decodedBytes = Base64.getDecoder().decode(request.getParameter(CommonConstants.PASSWORD).toString());
					String password = new String(decodedBytes, "UTF-8");
					/*String fileList = request.getParameter(CommonConstants.FILE);
					String mimeType = request.getParameter(CommonConstants.TYPE);
					String fileName = request.getParameter(CommonConstants.FILENAME);*/
					System.out.println("password    "+password);
		 			boolean mailStatus = EmailProxyUtil.sendEmails(request, host, fromEmail, toEmails, ccMails, bccMails, subject, emailBody, password.toString()); 
					if(mailStatus){
						new AttachmentsService().delete();
						return CommonWebUtil.buildSuccessResponse().toString();
					}else{
						return CommonWebUtil.buildErrorResponse("").toString();
					}
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/attachments", method = RequestMethod.POST)
	public @ResponseBody String attachments(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				AttachmentsDO attachmentsDO = new AttachmentsDO();
				System.out.println("inside attachments ");
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter(CommonConstants.FILE) != null){
			 		System.out.println("inside if " +request.getParameter(CommonConstants.FILE));
			 		attachmentsDO.setAttachFile(request.getParameter(CommonConstants.FILE));
			 		attachmentsDO.setFiletype(request.getParameter(CommonConstants.TYPE));
			 		attachmentsDO.setFileName(request.getParameter(CommonConstants.FILENAME));
			 		new AttachmentsService().persist(attachmentsDO);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveLeadInbox", method = RequestMethod.POST)
	public @ResponseBody String retrieveLeadInbox(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				 		List<EmployeeDO> employeeList = new EmployeeService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeList != null && employeeList.size() > 0){
			 				if(employeeList.get(0).getEmailId() != null && employeeList.get(0).getEmailPassword() != null){
			 					if(!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty()){
				 					byte[] decodedBytes = Base64.getDecoder().decode(employeeList.get(0).getEmailPassword());
									String stringDecode = new String(decodedBytes, "UTF-8");
									String primaryEmail = inputJSON.get(CommonConstants.PRIMARYEMAIL).toString();
									List<LeadEmailDO> LeadList = GmailUtil.getLeadMails(employeeList.get(0).getEmailId(), stringDecode.toString(),primaryEmail);
					 				System.out.println("leadList  ==  "+LeadList.size());
					 				if(LeadList.size() > 0){
					 					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
					 				}
			 					}	
			 				}
				 		}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
