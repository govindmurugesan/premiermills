package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeDocDAO;
import com.spheresuite.erp.domainobject.EmployeeDocDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeDocService {
	static Logger logger = Logger.getLogger(EmployeeDocService.class.getName());
	
	public EmployeeDocDO persist(EmployeeDocDO employeeDocDO) throws AppException {
		return new EmployeeDocDAO().persist(employeeDocDO);
	}

	public List<EmployeeDocDO> retrieveByEmpId(Long id) throws AppException {
		return new EmployeeDocDAO().retrieveByEmpId(id);
	}
	
	public boolean delete(Long id) throws AppException {
		return new EmployeeDocDAO().delete(id);
	}
	
	public List<EmployeeDocDO> retrieve() throws AppException {
		return new EmployeeDocDAO().retrieve();
	}
	
	public EmployeeDocDO update(EmployeeDocDO employeeDocDO) throws AppException {
		return new EmployeeDocDAO().update(employeeDocDO);
	}
}
