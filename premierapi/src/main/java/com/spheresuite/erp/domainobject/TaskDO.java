package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="task")
@TableGenerator(name ="task", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "task.findAll", query = "SELECT r FROM TaskDO r"),
    @NamedQuery(name = "task.findById", query = "SELECT r FROM TaskDO r where r.id =:id"),
    @NamedQuery(name = "task.findByLeadId", query = "SELECT r FROM TaskDO r where r.leadId =:id"),
    @NamedQuery(name = "task.findPendingTask", query = "SELECT r FROM TaskDO r where r.status =:status order by r.taskDate asc"),
})
public class TaskDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "task.findAll";
	
	public static final String FIND_BY_ID = "task.findById";
	
	public static final String FIND_BY_LEAD_ID = "task.findByLeadId";
	
	public static final String FIND_PENDING_TASK = "task.findPendingTask";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "task")
	private Long id;
	private Long leadId;
	private String taskDetails;
	private String taskDate;
	private String taskTime;
	private char status;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public String getTaskDetails() {
		return taskDetails;
	}

	public void setTaskDetails(String taskDetails) {
		this.taskDetails = taskDetails;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getTaskTime() {
		return taskTime;
	}

	public void setTaskTime(String taskTime) {
		this.taskTime = taskTime;
	}

	public String getTaskDate() {
		return taskDate;
	}

	public void setTaskDate(String taskDate) {
		this.taskDate = taskDate;
	}
	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}