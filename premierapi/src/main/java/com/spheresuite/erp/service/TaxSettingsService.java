package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TaxSettingsDAO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;

public class TaxSettingsService {
	static Logger logger = Logger.getLogger(TaxSettingsService.class.getName());
	
	public TaxSettingsDO persist(TaxSettingsDO taxSettingDO) throws AppException {
		return new TaxSettingsDAO().persist(taxSettingDO);
	}
	
	public TaxSettingsDO update(TaxSettingsDO taxSettingDO) throws AppException {
		return new TaxSettingsDAO().update(taxSettingDO);
	}

	public List<TaxSettingsDO> retrieveById(Long Id) throws AppException {
		return new TaxSettingsDAO().retrieveById(Id);
	}

	public List<TaxSettingsDO> retrieve() throws AppException {
		return new TaxSettingsDAO().retrieve();
	}
	
	public List<TaxSettingsDO> retrieveActive(String name) throws AppException {
		return new TaxSettingsDAO().retrieveActive(name);
	}
}
