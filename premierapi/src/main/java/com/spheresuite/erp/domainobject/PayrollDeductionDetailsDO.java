package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="payrolldeductiondetails")
@TableGenerator(name ="payrolldeductiondetails", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "payrolldeductiondetails.findAll", query = "SELECT r FROM PayrollDeductionDetailsDO r"),
    @NamedQuery(name = "payrolldeductiondetails.findByEmpPayrollId", query = "SELECT r FROM PayrollDeductionDetailsDO r where r.payrollid =:id"),
    @NamedQuery(name = "payrolldeductiondetails.findByEmpPayrollIds", query = "SELECT r FROM PayrollDeductionDetailsDO r where r.payrollid IN :id"),
    @NamedQuery(name = "payrolldeductiondetails.findAllDeduction", query = "SELECT sum(r.monthly) FROM PayrollDeductionDetailsDO r where r.payrollid =:id"),
})
public class PayrollDeductionDetailsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "payrolldeductiondetails.findAll";
	
	public static final String FIND_BY_ID = "payrolldeductiondetails.findByEmpPayrollId";
	
	public static final String FIND_BY_IDS = "payrolldeductiondetails.findByEmpPayrollIds";
	
	public static final String FIND_ALL_DEDUCTION = "payrolldeductiondetails.findAllDeduction";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "payrolldeductiondetails")
	private Long id;
	private Long deductionId;
	private Long payrollid;
	private Double monthly;
	private Double ytd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDeductionId() {
		return deductionId;
	}
	public void setDeductionId(Long deductionId) {
		this.deductionId = deductionId;
	}
	public Long getPayrollid() {
		return payrollid;
	}
	public void setPayrollid(Long payrollid) {
		this.payrollid = payrollid;
	}
	public Double getMonthly() {
		return monthly;
	}
	public void setMonthly(Double monthly) {
		this.monthly = monthly;
	}
	public Double getYtd() {
		return ytd;
	}
	public void setYtd(Double ytd) {
		this.ytd = ytd;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}