package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EducationLevelDAO;
import com.spheresuite.erp.domainobject.EducationLevelDO;
import com.spheresuite.erp.exception.AppException;

public class EducationLevelService {
	static Logger logger = Logger.getLogger(EducationLevelService.class.getName());
	
	public EducationLevelDO persist(EducationLevelDO EducationLevelDO) throws AppException {
		return new EducationLevelDAO().persist(EducationLevelDO);
	}

	public List<EducationLevelDO> retrieveActive() throws AppException {
		return new EducationLevelDAO().retrieveActive();
	}
	
	public List<EducationLevelDO> retrieveById(Long id) throws AppException {
		return new EducationLevelDAO().retrieveById(id);
	}
	
	public List<EducationLevelDO> retrieve() throws AppException {
		return new EducationLevelDAO().retrieve();
	}
	
	public EducationLevelDO update(EducationLevelDO EducationLevelDO) throws AppException {
		return new EducationLevelDAO().update(EducationLevelDO);
	}
}
