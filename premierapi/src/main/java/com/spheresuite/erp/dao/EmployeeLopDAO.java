package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeLopDAO {
	static Logger logger = Logger.getLogger(EmployeeLopDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeLopDO persist(EmployeeLopDO employeeLopDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeLopDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeLopDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeLopDO> employeeLopList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeLopDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				employeeLopList = (List<EmployeeLopDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieve() throws AppException {
		List<EmployeeLopDO> employeeLopList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeLopDO.FIND_ALL);
				employeeLopList = (List<EmployeeLopDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveById(Long id) throws AppException {
		List<EmployeeLopDO> employeeLopList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeLopDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeLopList = (List<EmployeeLopDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopList;
	}
	
	public EmployeeLopDO update(EmployeeLopDO employeeLopDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeLopDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopDO;
	}
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpWithDate(List<Long> empIds, String fromMonth) throws AppException {
		List<EmployeeLopDO> employeeLopList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(empIds != null && empIds.size() > 0){
					TransactionManager.beginTransaction(em);
					//Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_MONTH);
					/*Query q =  em.createQuery("SELECT r FROM EmployeeLopDO r where (SUBSTR(r.startdate,1,7) >=:fromMonth and SUBSTR(r.startdate,1,7) <=:toMonth) and r.empId IN :empId and r.status =:status");*/
					Query q =  em.createQuery("SELECT r FROM EmployeeLopDO r where SUBSTRING(r.startdate,1,7) =:fromMonth and r.empId IN :empId and r.status =:status");
					q.setParameter(CommonConstants.EMPID, empIds);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					employeeLopList = (List<EmployeeLopDO>) q.getResultList();
					TransactionManager.commitTransaction();
				}else{
					TransactionManager.beginTransaction(em);
					Query q =  em.createQuery("SELECT r FROM EmployeeLopDO r where SUBSTRING(r.startdate,1,7) =:fromMonth and r.status =:status");
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					employeeLopList = (List<EmployeeLopDO>) q.getResultList();
					TransactionManager.commitTransaction();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeLopDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) throws AppException {
		List<EmployeeLopDO> employeeLopList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(empIds != null && empIds.size() > 0){
					//Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_BETWEENMONTH);
					Query q =  em.createQuery("SELECT r FROM EmployeeLopDO r where (SUBSTR(r.startdate,1,7) >=:fromMonth and SUBSTR(r.startdate,1,7) <=:toMonth) and r.empId IN :empId and r.status =:status");
					q.setParameter(CommonConstants.EMPID, empIds);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeLopList = (List<EmployeeLopDO>) q.getResultList();
				}else{
					Query q =  em.createQuery("SELECT r FROM EmployeeLopDO r where (SUBSTR(r.startdate,1,7) >=:fromMonth and SUBSTR(r.startdate,1,7) <=:toMonth) and r.status =:status");
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeLopList = (List<EmployeeLopDO>) q.getResultList();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeLopList;
	}
}
