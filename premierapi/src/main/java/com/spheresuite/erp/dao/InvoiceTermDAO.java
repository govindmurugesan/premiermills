package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class InvoiceTermDAO {
	static Logger logger = Logger.getLogger(InvoiceTermDAO.class.getName());
	private EntityManager em = null;
	
	public List<InvoiceTermDO> persist(List<InvoiceTermDO> invoiceTermList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && invoiceTermList != null && invoiceTermList.size() > 0) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				if(invoiceTermList != null && invoiceTermList.size() > 0){
					Query q =  em.createQuery("delete from InvoiceTermDO e where e.proposalId =:proposalId");
					q.setParameter(CommonConstants.PROPOSAL_ID, invoiceTermList.get(0).getProposalId());
					q.executeUpdate();
				}
				for (InvoiceTermDO invoiceTermDO : invoiceTermList) {
					em.persist(invoiceTermDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return invoiceTermList;
	}
	
	public boolean delete(Long proposalId) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && proposalId != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				Query q =  em.createQuery("delete from InvoiceTermDO e where e.proposalId =:proposalId");
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				q.executeUpdate();
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<InvoiceTermDO> retrieveById(Long proposalId) throws AppException {
		List<InvoiceTermDO> proposalList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(InvoiceTermDO.FIND_BY_PROPOSAL_ID);
				q.setParameter(CommonConstants.PROPOSAL_ID, proposalId);
				proposalList = (List<InvoiceTermDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return proposalList;
	}
}
