angular
    .module('spheresuite')
    .controller('reportController', reportController);

reportController.$inject = ['$scope', '$rootScope','$location', 'reportService', '$localStorage', '$q', 'companyInformationService'];

function reportController($scope, $rootScope, $location, reportService, $localStorage, $q, companyInformationService) {
    var reportControllerScope = this;
    $rootScope.headerMenu= "Reports"; 
    
    reportControllerScope.viewReport = viewReport;
    reportControllerScope.viewTaxReport = viewTaxReport;
    reportControllerScope.viewBonusReport = viewBonusReport;
    reportControllerScope.viewLopReport = viewLopReport;
    reportControllerScope.viewYtdReport = viewYtdReport;
    reportControllerScope.goBackToReports = goBackToReports;
    reportControllerScope.getActiveFinancialYear = getActiveFinancialYear;
    reportControllerScope.getActiveBatch = getActiveBatch;
    reportControllerScope.getDeductionTypes = getDeductionTypes;
    reportControllerScope.showMonth = showMonth;
    //reportControllerScope.mapImageAndName = mapImageAndName;
    reportControllerScope.downloadBonusPDF = downloadBonusPDF;
    reportControllerScope.downloadLopPDF = downloadLopPDF;
    reportControllerScope.downloadDeductionPDF = downloadDeductionPDF;
    reportControllerScope.downloadYtdPDF = downloadYtdPDF;
    reportControllerScope.downloadTaxPDF = downloadTaxPDF;
    
    reportControllerScope.deductionReport = false;
    reportControllerScope.taxReport = false;
    reportControllerScope.bonusReport = false;
    reportControllerScope.ytdReport = false;
    reportControllerScope.isDataAvailable = false;
    reportControllerScope.getActiveFinancialYearList;
    reportControllerScope.deductionList = [];
    reportControllerScope.deductionDetailsList = [];
    reportControllerScope.taxDetailsList = [];
    reportControllerScope.bonusDetailsList = [];
    reportControllerScope.lopDetailsList = [];
    reportControllerScope.ytdDetailsList = [];
    reportControllerScope.empNameDeduction = [];
    reportControllerScope.newEmpCompDeductionList = [];
    reportControllerScope.mappedArr = [];
    reportControllerScope.newArray = [];
    reportControllerScope.bonusDetailsListFrequency;
    reportControllerScope.lopDetailsListFrequency;
    reportControllerScope.ytdDetailsListFrequency;
    reportControllerScope.deductionDetailsListFrequency;
    reportControllerScope.taxDetailsListFrequency;

    $rootScope.limitToShow = 10;
    $rootScope.beginFrom = 0;
    
    reportControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
    reportControllerScope.buttonBeginFrom = 0;
    reportControllerScope.isNextDisabled = false;
    reportControllerScope.gotoPage = gotoPage;
    reportControllerScope.showPrevNav = showPrevNav;
    reportControllerScope.showNextNav = showNextNav;
    

    if($location.path() == '/deductionReport') { 
        getActiveFinancialYear();
        getActiveBatch();
        getDeductionTypes();
        //mapImageAndName();
    }else if($location.path() == '/taxReport'){
        getActiveFinancialYear();
        getActiveBatch();
        getTaxType();
       // mapImageAndName();
    }else if($location.path() == '/bonusReport'){
        getActiveFinancialYear();
        getActiveBatch();
       // mapImageAndName();
    }else if($location.path() == '/LOPReport'){
        getActiveFinancialYear();
        getActiveBatch();
       // mapImageAndName();
    }else if($location.path() == '/payrollYTDReport'){
        getActiveFinancialYear();
        getActiveBatch();
       // mapImageAndName();
    }
    
    function viewReport(){
    	reportControllerScope.deductionReport = true;
        reportControllerScope.spinner = true;
        reportControllerScope.isDataAvailable = false;
        
        if (reportControllerScope.deductionreport) {
            if(reportControllerScope.deductionreport.payrollBatchId == undefined){
                reportControllerScope.deductionreport.payrollBatchId = '';
            }            
            var newfyid = JSON.parse(reportControllerScope.deductionreport.fyid);
            if(reportControllerScope.deductionreport.fyid){
                 reportControllerScope.deductionreport.fyid = newfyid.id;
            }

            if(reportControllerScope.deductionreport.frequency == "Y" || (reportControllerScope.deductionreport.quarterly == undefined && reportControllerScope.deductionreport.monthly == undefined)){
                reportControllerScope.deductionreport.quarterly = '';
                reportControllerScope.deductionreport.monthly = '';
            }else if(reportControllerScope.deductionreport.frequency == "Q" ||  reportControllerScope.deductionreport.monthly == undefined){
                reportControllerScope.deductionreport.monthly = '';
            }else if(reportControllerScope.deductionreport.frequency == "M" ||  reportControllerScope.deductionreport.quarterly == undefined){
                reportControllerScope.deductionreport.quarterly = '';
            }
            reportService.viewReport(reportControllerScope.deductionreport).then(function(res) {
                console.log('viewReportResponse', res);
                if (res.successflag === 'true' && res.results.length > 0) {
                    reportControllerScope.deductionDetailsList = res.results;
                    reportControllerScope.deductionDetailsListFrequency = res;
                    console.log('reportControllerScope.deductionDetailsList  ', reportControllerScope.deductionDetailsList)
                   reportControllerScope.deductionDetailsListBackup = angular.copy(reportControllerScope.deductionDetailsList);
                    reportControllerScope.isDataAvailable = true;

                    for(var i = 0; i < reportControllerScope.deductionDetailsListBackup.length; i++){
                        reportControllerScope.deductionDetailsListBackup[i].isActiveClass = false;
                        reportControllerScope.deductionDetailsListBackup[i].isSelect = false;
                    }
                    reportControllerScope.deductionDetailsList =  angular.copy(reportControllerScope.deductionDetailsListBackup);
                    if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.deductionDetailsList.length)
                        reportControllerScope.isNextDisabled = true;
                    reportControllerScope.deductionDetailsList[0].isActiveClass = true;
                    console.log('reportControllerScope.deductionDetailsList 00000 ',reportControllerScope.deductionDetailsList )
                    reportControllerScope.spinner = false;
                    //reportControllerScope.deductionreport= {};
                } else {
                    reportControllerScope.spinner = false;
                    reportControllerScope.msg = res.errors;
                    reportControllerScope.deductionDetailsList = [];
                    reportControllerScope.deductionDetailsListBackup  = [];
                }

            }, function(err) {
                reportControllerScope.spinner = false;
            });
        } 
    }

    function viewTaxReport(){
        reportControllerScope.taxReport = true;
        reportControllerScope.spinner = true;
        reportControllerScope.isDataAvailable = false;
        if (reportControllerScope.taxreport) {
            if(reportControllerScope.taxreport.payrollBatchId == undefined){
                reportControllerScope.taxreport.payrollBatchId = '';
            }            
            var newfyid = JSON.parse(reportControllerScope.taxreport.fyid);
            if(reportControllerScope.taxreport.fyid){
                 reportControllerScope.taxreport.fyid = newfyid.id;
            }

            if(reportControllerScope.taxreport.frequency == "Y" || (reportControllerScope.taxreport.quarterly == undefined && reportControllerScope.taxreport.monthly == undefined)){
                reportControllerScope.taxreport.quarterly = '';
                reportControllerScope.taxreport.monthly = '';
            }else if(reportControllerScope.taxreport.frequency == "Q" ||  reportControllerScope.taxreport.monthly == undefined){
                reportControllerScope.taxreport.monthly = '';
            }else if(reportControllerScope.taxreport.frequency == "M" ||  reportControllerScope.taxreport.quarterly == undefined){
                reportControllerScope.taxreport.quarterly = '';
            }
            reportService.viewTaxReport(reportControllerScope.taxreport).then(function(res) {
                console.log('viewTaxReportResponse', res);
                if (res.successflag === 'true' && res.results.length > 0) {
                    reportControllerScope.taxDetailsList = res.results;
                    reportControllerScope.taxDetailsListFrequency = res;
                    console.log('reportControllerScope.taxDetailsList  ', reportControllerScope.taxDetailsList)
                    reportControllerScope.taxDetailsListBackup = angular.copy(reportControllerScope.taxDetailsList);
                    reportControllerScope.isDataAvailable = true;

                    for(var i = 0; i < reportControllerScope.taxDetailsListBackup.length; i++){
                        reportControllerScope.taxDetailsListBackup[i].isActiveClass = false;
                        reportControllerScope.taxDetailsListBackup[i].isSelect = false;
                    }
                    reportControllerScope.taxDetailsList =  angular.copy(reportControllerScope.taxDetailsListBackup);
                    if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.taxDetailsList.length)
                        reportControllerScope.isNextDisabled = true;
                    reportControllerScope.taxDetailsList[0].isActiveClass = true;
                    console.log('reportControllerScope.taxDetailsList 00000 ',reportControllerScope.taxDetailsList )
                    reportControllerScope.spinner = false;
                    //reportControllerScope.taxreport= {};
                } else {
                    reportControllerScope.spinner = false;
                    reportControllerScope.msg = res.errors;
                    reportControllerScope.taxDetailsList = [];
                    reportControllerScope.taxDetailsListBackup = [];
                }

            }, function(err) {
                reportControllerScope.spinner = false;
            });
        } 
    }

    function viewBonusReport(){
        reportControllerScope.bonusReport = true;
        reportControllerScope.spinner = true;
        reportControllerScope.isDataAvailable = false;
        if (reportControllerScope.bonusreport) {
            if(reportControllerScope.bonusreport.payrollBatchId == undefined){
                reportControllerScope.bonusreport.payrollBatchId = '';
            }            
            var newfyid = JSON.parse(reportControllerScope.bonusreport.fyid);
            if(reportControllerScope.bonusreport.fyid){
                 reportControllerScope.bonusreport.fyid = newfyid.id;
            }

            if(reportControllerScope.bonusreport.frequency == "Y" || (reportControllerScope.bonusreport.quarterly == undefined && reportControllerScope.bonusreport.monthly == undefined)){
                reportControllerScope.bonusreport.quarterly = '';
                reportControllerScope.bonusreport.monthly = '';
            }else if(reportControllerScope.bonusreport.frequency == "Q" ||  reportControllerScope.bonusreport.monthly == undefined){
                reportControllerScope.bonusreport.monthly = '';
            }else if(reportControllerScope.bonusreport.frequency == "M" ||  reportControllerScope.bonusreport.quarterly == undefined){
                reportControllerScope.bonusreport.quarterly = '';
            }
            reportService.viewBonusReport(reportControllerScope.bonusreport).then(function(res) {
                console.log('viewBonusReportResponse', res);
                if (res.successflag === 'true' && res.results.length > 0) {
                    reportControllerScope.bonusDetailsList = res.results;
                    reportControllerScope.bonusDetailsListFrequency = res;
                    console.log('reportControllerScope.bonusDetailsList  ', reportControllerScope.bonusDetailsList)
                    reportControllerScope.bonusDetailsListBackup = angular.copy(reportControllerScope.bonusDetailsList);
                    reportControllerScope.isDataAvailable = true;

                    for(var i = 0; i < reportControllerScope.bonusDetailsListBackup.length; i++){
                        reportControllerScope.bonusDetailsListBackup[i].isActiveClass = false;
                        reportControllerScope.bonusDetailsListBackup[i].isSelect = false;
                    }
                    reportControllerScope.bonusDetailsList =  angular.copy(reportControllerScope.bonusDetailsListBackup);
                    if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.bonusDetailsList.length)
                        reportControllerScope.isNextDisabled = true;
                    reportControllerScope.bonusDetailsList[0].isActiveClass = true;
                    console.log('reportControllerScope.bonusDetailsList 00000 ',reportControllerScope.bonusDetailsList )
                    reportControllerScope.spinner = false;
                    //reportControllerScope.bonusreport= {};
                } else {
                    reportControllerScope.spinner = false;
                    reportControllerScope.msg = res.errors;
                    reportControllerScope.bonusDetailsList = [];
                    reportControllerScope.bonusDetailsListBackup = []
                }

            }, function(err) {
                reportControllerScope.spinner = false;
            });
        }

    }

    function viewLopReport(){
        reportControllerScope.lopReport = true;
        reportControllerScope.spinner = true;
        var fyMonth, fyYear;
        if (reportControllerScope.lopreport) {
        	var str =  reportControllerScope.lopreport.monthly.split(' ');
        	console.log('str  ',str);
        	monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        	               "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
           for(var i = 0; i < 12; i++) {
        	   if(monthNames[i]==str[0]){
        		   fyMonth = i;
        	   }        	                   
           }
        	fyMonth = fyMonth+1;
        	fyYear = str[1];
        	fyMonth = (fyMonth<10)?('0'+fyMonth):fyMonth;
        	reportControllerScope.lopreport.monthly = fyYear+'-'+fyMonth;
        	console.log('reportControllerScope.lopreport.monthly  ', fyMonth)
            if(reportControllerScope.lopreport.payrollBatchId == undefined){
                reportControllerScope.lopreport.payrollBatchId = '';
            }            
            var newfyid = JSON.parse(reportControllerScope.lopreport.fyid);
            if(reportControllerScope.lopreport.fyid){
                 reportControllerScope.lopreport.fyid = newfyid.id;
            }

            if(reportControllerScope.lopreport.frequency == "Y" || (reportControllerScope.lopreport.quarterly == undefined && reportControllerScope.lopreport.monthly == undefined)){
                reportControllerScope.lopreport.quarterly = '';
                reportControllerScope.lopreport.monthly = '';
            }else if(reportControllerScope.lopreport.frequency == "Q" ||  reportControllerScope.lopreport.monthly == undefined){
                reportControllerScope.lopreport.monthly = '';
            }else if(reportControllerScope.lopreport.frequency == "M" ||  reportControllerScope.lopreport.quarterly == undefined){
                reportControllerScope.lopreport.quarterly = '';
            }
            reportService.viewLopReport(reportControllerScope.lopreport).then(function(res) {
                console.log('viewLopReportResponse', res);
                if (res.successflag === 'true' && res.results.length > 0) {
                    reportControllerScope.lopDetailsList = res.results;
                    reportControllerScope.lopDetailsListFrequency = res;
                    console.log('reportControllerScope.lopDetailsList  ', reportControllerScope.lopDetailsList)
                    reportControllerScope.lopDetailsListBackup = angular.copy(reportControllerScope.lopDetailsList);
                    reportControllerScope.isDataAvailable = true;

                    for(var i = 0; i < reportControllerScope.lopDetailsListBackup.length; i++){
                        reportControllerScope.lopDetailsListBackup[i].isActiveClass = false;
                        reportControllerScope.lopDetailsListBackup[i].isSelect = false;
                    }
                    reportControllerScope.lopDetailsList =  angular.copy(reportControllerScope.lopDetailsListBackup);
                    if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.lopDetailsList.length)
                        reportControllerScope.isNextDisabled = true;
                    reportControllerScope.lopDetailsList[0].isActiveClass = true;
                    console.log('reportControllerScope.lopDetailsList 00000 ',reportControllerScope.lopDetailsList )
                    reportControllerScope.spinner = false;
                    //reportControllerScope.lopreport= {};
                } else {
                    reportControllerScope.spinner = false;
                    reportControllerScope.msg = res.errors;
                    reportControllerScope.lopDetailsList = [];
                    reportControllerScope.lopDetailsListBackup = []
                }

            }, function(err) {
                reportControllerScope.spinner = false;
            });
        }

    }

    function viewYtdReport(){
        reportControllerScope.ytdReport = true;
        reportControllerScope.spinner = true;
        reportControllerScope.isDataAvailable = false;
        if (reportControllerScope.ytdreport) {
            if(reportControllerScope.ytdreport.payrollBatchId == undefined){
                reportControllerScope.ytdreport.payrollBatchId = '';
            }            
            var newfyid = JSON.parse(reportControllerScope.ytdreport.fyid);
            if(reportControllerScope.ytdreport.fyid){
                 reportControllerScope.ytdreport.fyid = newfyid.id;
            }

            if(reportControllerScope.ytdreport.frequency == "Y" || (reportControllerScope.ytdreport.quarterly == undefined && reportControllerScope.ytdreport.monthly == undefined)){
                reportControllerScope.ytdreport.quarterly = '';
                reportControllerScope.ytdreport.monthly = '';
            }else if(reportControllerScope.ytdreport.frequency == "Q" ||  reportControllerScope.ytdreport.monthly == undefined){
                reportControllerScope.ytdreport.monthly = '';
            }else if(reportControllerScope.ytdreport.frequency == "M" ||  reportControllerScope.ytdreport.quarterly == undefined){
                reportControllerScope.ytdreport.quarterly = '';
            }
            reportService.viewYtdReport(reportControllerScope.ytdreport).then(function(res) {
                console.log('viewTaxReportResponse', res);
                if (res.successflag === 'true' && res.results.length > 0) {
                    reportControllerScope.ytdDetailsList = res.results;
                    reportControllerScope.ytdDetailsListFrequency = res;
                    console.log('reportControllerScope.ytdDetailsList  ', reportControllerScope.ytdDetailsList)
                    reportControllerScope.ytdDetailsListBackup = angular.copy(reportControllerScope.ytdDetailsList);
                    reportControllerScope.isDataAvailable = true;

                    for(var i = 0; i < reportControllerScope.ytdDetailsListBackup.length; i++){
                        reportControllerScope.ytdDetailsListBackup[i].isActiveClass = false;
                        reportControllerScope.ytdDetailsListBackup[i].isSelect = false;
                    }
                    reportControllerScope.ytdDetailsList =  angular.copy(reportControllerScope.ytdDetailsListBackup);
                    if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.ytdDetailsList.length)
                        reportControllerScope.isNextDisabled = true;
                    reportControllerScope.ytdDetailsList[0].isActiveClass = true;
                    console.log('reportControllerScope.ytdDetailsList 00000 ',reportControllerScope.ytdDetailsList )
                    reportControllerScope.spinner = false;
                    //reportControllerScope.ytdreport= {};
                } else {
                    reportControllerScope.spinner = false;
                    reportControllerScope.msg = res.errors;
                    reportControllerScope.ytdDetailsList = [];
                    reportControllerScope.ytdDetailsListBackup = []
                }

            }, function(err) {
                reportControllerScope.spinner = false;
            });
        } 
    }

    /*$scope.intrArr = mapImageAndName();
    function mapImageAndName() {        
        for (var i = 0; i < reportControllerScope.newEmpCompDeductionList.length; i++) {
          reportControllerScope.mappedArr.push({
            //'list': reportControllerScope.newEmpCompDeductionList[i],
            'name': reportControllerScope.empNameDeduction[i]

          });
        }console.log('name', name);
        return reportControllerScope.mappedArr;
    }*/

    function goBackToReports(){
    	$location.path('/reports');
    }

    function getActiveFinancialYear(){
        reportService.getActiveFinancialYear().then(function(res) {
            console.log('getActiveFinancialYear', res);
            if (res.successflag === 'true') {
                 reportControllerScope.getActiveFinancialYearList = res.results;                  
            }
        });
    }

    function showMonth(data){
        data =  JSON.parse(data);
        var date = new Date(data.fyFrom, 3);
        var months = [],
        monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
    for(var i = 0; i < 12; i++) {
        months.push(monthNames[date.getMonth()] + ' ' + date.getFullYear());
        
        // Subtract a month each time
        date.setMonth(date.getMonth() + 1);
    }
    reportControllerScope.months = months;
    }

    function getActiveBatch(){
        reportControllerScope.spinner = true;
        reportService.getActiveBatch().then(function(res){
            console.log('getActiveBatch res',res)
            if(res.successflag == 'true' && res.results.length > 0){
                reportControllerScope.batchActiveList = res.results;
            }
            reportControllerScope.spinner = false;
        },function(err){
            reportControllerScope.spinner = false;
        });
    }

    function getDeductionTypes() {
        reportControllerScope.spinner = true;
        reportControllerScope.deductionMsg = '';
        reportService.getDeduction().then(function (res) {
            console.log('getDeduction res', res);
            if (res.successflag == 'true' && res.results.length > 0) {
                reportControllerScope.deductionList = res.results;
                reportControllerScope.spinner = false;
            } else {
                reportControllerScope.deductionMsg = "Deduction Not Available";
                reportControllerScope.spinner = false;
            }
        }, function (err) {
            reportControllerScope.deductionMsg = "Deduction Not Available";
            reportControllerScope.spinner = false;
        });
    }

    function getTaxType() {
        reportControllerScope.spinner = true;
        reportControllerScope.taxMsg = '';
        reportService.getTax().then(function (res) {
            console.log('getTax res', res);
            if (res.successflag == 'true' && res.results.length > 0) {
                reportControllerScope.taxList = res.results;
                reportControllerScope.spinner = false;
            } else {
                reportControllerScope.taxMsg = "Tax Not Available";
                reportControllerScope.spinner = false;
            }
        }, function (err) {
            reportControllerScope.taxMsg = "Tax Not Available";
            reportControllerScope.spinner = false;
        });
    }


    function showPrevNav(){
        reportControllerScope.buttonBeginFrom -= reportControllerScope.buttonLimitToShow;
        gotoPage(reportControllerScope.buttonBeginFrom)
        reportControllerScope.isNextDisabled = false;
    }

    function showNextNav(){
        reportControllerScope.buttonBeginFrom += reportControllerScope.buttonLimitToShow;
        gotoPage(reportControllerScope.buttonBeginFrom)
        if((reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.deductionDetailsList.length ||
            (reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.taxDetailsList.length || 
            (reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.bonusDetailsList.length || 
            (reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.lopDetailsList.length ||
            (reportControllerScope.buttonBeginFrom + reportControllerScope.buttonLimitToShow) * reportControllerScope.buttonLimitToShow >= reportControllerScope.ytdDetailsList.length)
            reportControllerScope.isNextDisabled = true;
    }

    function gotoPage(index, activeIndex){
        $rootScope.beginFrom = index;
        if(reportControllerScope.deductionDetailsList.length > 0){
            for(var i = 0; i < reportControllerScope.deductionDetailsList.length; i++){
                console.log(' data[i].isActiveClass', reportControllerScope.deductionDetailsList[i].isActiveClass);
                reportControllerScope.deductionDetailsList[i].isActiveClass = false;
            }
            if(!activeIndex)
                activeIndex = index
            reportControllerScope.deductionDetailsList[activeIndex].isActiveClass = true;
        } else if(reportControllerScope.taxDetailsList.length > 0) {
            for(var i = 0; i < reportControllerScope.taxDetailsList.length; i++){
                console.log(' data[i].isActiveClass', reportControllerScope.taxDetailsList[i].isActiveClass);
                reportControllerScope.taxDetailsList[i].isActiveClass = false;
            }
            if(!activeIndex)
                activeIndex = index
            reportControllerScope.taxDetailsList[activeIndex].isActiveClass = true;
        } else if(reportControllerScope.bonusDetailsList.length > 0) {
            for(var i = 0; i < reportControllerScope.bonusDetailsList.length; i++){
                console.log(' data[i].isActiveClass', reportControllerScope.bonusDetailsList[i].isActiveClass);
                reportControllerScope.bonusDetailsList[i].isActiveClass = false;
            }
            if(!activeIndex)
                activeIndex = index
            reportControllerScope.bonusDetailsList[activeIndex].isActiveClass = true;
        } else if(reportControllerScope.lopDetailsList.length > 0) {
            for(var i = 0; i < reportControllerScope.lopDetailsList.length; i++){
                console.log(' data[i].isActiveClass', reportControllerScope.lopDetailsList[i].isActiveClass);
                reportControllerScope.lopDetailsList[i].isActiveClass = false;
            }
            if(!activeIndex)
                activeIndex = index
            reportControllerScope.lopDetailsList[activeIndex].isActiveClass = true;
        } else if(reportControllerScope.ytdDetailsList.length > 0) {
            for(var i = 0; i < reportControllerScope.ytdDetailsList.length; i++){
                console.log(' data[i].isActiveClass', reportControllerScope.ytdDetailsList[i].isActiveClass);
                reportControllerScope.ytdDetailsList[i].isActiveClass = false;
            }
            if(!activeIndex)
                activeIndex = index
            reportControllerScope.ytdDetailsList[activeIndex].isActiveClass = true;
        }
    }

    function toDataURL(url, callback) {
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
          callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
    }

    var companyInfo;


    function downloadBonusPDF(bonusReportDetail){
        toDataURL('images/i_logo.png', function(dataUrl) {
              if(dataUrl) logo = dataUrl;
              else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
        var doc = new jsPDF('a3');// a4 220*300
        reportControllerScope.spinner = true; 
        var promises = [];
        var deferred = $q.defer();
        promises.push(getActiveFinancialYear());
        promises.push(getActiveBatch());  
        promises.push(getCompanyInformation());
        
        $q.all(promises).then(function (data) {
            //header
            doc.setFont("Poppins");
            doc.setFontSize(15);
            doc.setFontType("bold");
            doc.text(10, 15,'BONUS REPORT');
            doc.addImage(logo, 'PNG', 138, 3, 65, 20);
            doc.setFontSize(10);
            
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            //doc.setLineWidth(0.1);
            doc.line(10, 24, 200, 24);


            //frequency
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 33, 'Report Filters');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 35, 80, 35);
            doc.setFontSize(9);
            doc.setFontType("bold");
            if(!reportControllerScope.bonusDetailsListFrequency){
                doc.text(10, 42, "Frequency");
                doc.text(80, 42, "-", null, null, 'right');
                doc.text(10, 47, "Period");
                doc.text(80, 47, "-", null, null, 'right');
            }else{
                doc.text(10, 42, "Frequency");
                if(reportControllerScope.bonusDetailsListFrequency.frequency) doc.text(80, 42, reportControllerScope.bonusDetailsListFrequency.frequency, null, null, 'right')
                else doc.text(80, 42, "", null, null, 'right')
                
                doc.text(10, 47, "Period");
                if(reportControllerScope.bonusDetailsListFrequency.period) doc.text(80, 47, reportControllerScope.bonusDetailsListFrequency.period, null, null, 'right')
                else doc.text(80, 47, "", null, null, 'right')
            }

            //reporttable
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 72, "Report");
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 74, 80, 74);
            doc.setFontSize(9);
            doc.setFontType("bold");
            var lastLineSection1 = 30;
            if(!reportControllerScope.bonusDetailsList){
                doc.text(10, 77, "Employee Name");
                doc.text(70, 77, "Bonus Amount");
                doc.text(130, 77, "Bonus Month");
                doc.text(185, 77, "Paid On");
            }else{

                for(var i=0; i< reportControllerScope.bonusDetailsList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(10, 82, "Employee Name");
                if(reportControllerScope.bonusDetailsList[i].empName) doc.text(20,  lastLineSection1+60, reportControllerScope.bonusDetailsList[i].empName, null, null, 'center')
                else doc.text(27, 103, "", null, null, 'right')
                
                doc.text(70, 82, "Bonus Amount");
                if(reportControllerScope.bonusDetailsList[i].amount) doc.text(87,  lastLineSection1+60, parseFloat(reportControllerScope.bonusDetailsList[i].amount).toFixed(2), null, null, 'right')
                else doc.text(87, 103, "", null, null, 'right')
                
                doc.text(130, 82, "Bonus Month");
                if(reportControllerScope.bonusDetailsList[i].bonusMonth) doc.text(143,  lastLineSection1+60, reportControllerScope.bonusDetailsList[i].bonusMonth, null, null, 'right')
                else doc.text(143, 103, "", null, null, 'right')

                doc.text(185, 82, "Paid On");
                if(reportControllerScope.bonusDetailsList[i].paidOn) doc.text(193,  lastLineSection1+60, reportControllerScope.bonusDetailsList[i].paidOn, null, null, 'right')
                else doc.text(193, 103, "", null, null, 'right')
                lastLineSection1 = lastLineSection1+ 6;
                }

                
            }
            
            //total
            /*doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(92, 33, 'Earnings / Allowances');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(92, 35, 200, 35);
            doc.setFontSize(8);
            var lastLineSection1 = 42;
            for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(92, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].earningName);
                doc.text(200, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
                lastLineSection1 = lastLineSection1+ 6;
            }
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(170, lastLineSection1, 200, lastLineSection1);
            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text(92, lastLineSection1+6, 'Total ( A )');
            doc.text(200, lastLineSection1+6, payslipDetail.earningMonth, null, null, 'right');*/
            //footer
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            doc.line(10, 268, 200, 268);
            doc.setFontType("bold");
            doc.setFontSize(12);
            
            //companyInfo
            
            if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
            else doc.text(110, 274, companyInfo.name , null, null, 'center');
            
            doc.setFontType("normal");
            doc.setFontSize(10);
            
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
            
            if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
            if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
            reportControllerScope.spinner = false;
            doc.save("Bonus_Report");
        })
        
        })
        
    }

    function downloadLopPDF(lopReportDetail){
        toDataURL('images/i_logo.png', function(dataUrl) {
              if(dataUrl) logo = dataUrl;
              else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
        var doc = new jsPDF('a3');// a4 220*300
        reportControllerScope.spinner = true; 
        var promises = [];
        var deferred = $q.defer();
        promises.push(getActiveFinancialYear());
        promises.push(getActiveBatch());  
        promises.push(getCompanyInformation());
        
        $q.all(promises).then(function (data) {
            //header
            doc.setFont("Poppins");
            doc.setFontSize(15);
            doc.setFontType("bold");
            doc.text(10, 15,'LOP REPORT');
            doc.addImage(logo, 'PNG', 138, 3, 65, 20);
            doc.setFontSize(10);
            
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            //doc.setLineWidth(0.1);
            doc.line(10, 24, 200, 24);


            //frequency
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 33, 'Report Filters');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 35, 80, 35);
            doc.setFontSize(9);
            doc.setFontType("bold");
            if(!reportControllerScope.lopDetailsListFrequency){
                doc.text(10, 42, "Frequency");
                doc.text(80, 42, "-", null, null, 'right');
                doc.text(10, 47, "Period");
                doc.text(80, 47, "-", null, null, 'right');
            }else{
                doc.text(10, 42, "Frequency");
                if(reportControllerScope.lopDetailsListFrequency.frequency) doc.text(80, 42, reportControllerScope.lopDetailsListFrequency.frequency, null, null, 'right')
                else doc.text(80, 42, "", null, null, 'right')
                
                doc.text(10, 47, "Period");
                if(reportControllerScope.lopDetailsListFrequency.period) doc.text(80, 47, reportControllerScope.lopDetailsListFrequency.period, null, null, 'right')
                else doc.text(80, 47, "", null, null, 'right')
                
            }

            //reporttable
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 72, "Report");
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 74, 80, 74);
            doc.setFontSize(9);
            doc.setFontType("bold");
            var lastLineSection2 = 30;
            if(!reportControllerScope.lopDetailsList){
                doc.text(10, 77, "Employee Name");
                doc.text(80, 77, "Start Date");
                doc.text(150, 77, "End Date");
                //doc.text(185, 77, "Paid On");
            }else{

                for(var i=0; i< reportControllerScope.lopDetailsList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(10, 82, "Employee Name");
                if(reportControllerScope.lopDetailsList[i].empName) doc.text(20,  lastLineSection2+60, reportControllerScope.lopDetailsList[i].empName, null, null, 'center')
                else doc.text(27, 103, "", null, null, 'right')
                
                doc.text(100, 82, "Start Date");
                if(reportControllerScope.lopDetailsList[i].startDate) doc.text(112,  lastLineSection2+60, reportControllerScope.lopDetailsList[i].startDate, null, null, 'right')
                else doc.text(84, 103, "", null, null, 'right')
                
                doc.text(177, 82, "End Date");
                if(reportControllerScope.lopDetailsList[i].enddate) doc.text(188,  lastLineSection2+60, reportControllerScope.lopDetailsList[i].enddate, null, null, 'right')
                else doc.text(141, 103, "", null, null, 'right')
                lastLineSection2 = lastLineSection2+ 6;
                }

                
            }
            
            //total
            /*doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(92, 33, 'Earnings / Allowances');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(92, 35, 200, 35);
            doc.setFontSize(8);
            var lastLineSection2 = 42;
            for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(92, lastLineSection2, payslipDetail.employeeCompensationEarningList[i].earningName);
                doc.text(200, lastLineSection2, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
                lastLineSection2 = lastLineSection2+ 6;
            }
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(170, lastLineSection2, 200, lastLineSection2);
            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text(92, lastLineSection2+6, 'Total ( A )');
            doc.text(200, lastLineSection2+6, payslipDetail.earningMonth, null, null, 'right');*/
            //footer
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            doc.line(10, 268, 200, 268);
            doc.setFontType("bold");
            doc.setFontSize(12);
            
            //companyInfo
            
            if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
            else doc.text(110, 274, companyInfo.name , null, null, 'center');
            
            doc.setFontType("normal");
            doc.setFontSize(10);
            
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
            
            if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
            if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
            reportControllerScope.spinner = false;
            doc.save("Lop_Report");
        })
        
        })
        
    }

    function downloadYtdPDF(ytdReportDetail){
        toDataURL('images/i_logo.png', function(dataUrl) {
              if(dataUrl) logo = dataUrl;
              else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
        var doc = new jsPDF('a3');// a4 220*300
        reportControllerScope.spinner = true; 
        var promises = [];
        var deferred = $q.defer();
        promises.push(getActiveFinancialYear());
        promises.push(getActiveBatch());  
        promises.push(getCompanyInformation());
        
        $q.all(promises).then(function (data) {
            //header
            doc.setFont("Poppins");
            doc.setFontSize(15);
            doc.setFontType("bold");
            doc.text(10, 15,'YTD REPORT');
            doc.addImage(logo, 'PNG', 138, 3, 65, 20);
            doc.setFontSize(10);
            
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            //doc.setLineWidth(0.1);
            doc.line(10, 24, 200, 24);


            //frequency
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 33, 'Report Filters');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 35, 80, 35);
            doc.setFontSize(9);
            doc.setFontType("bold");
            if(!reportControllerScope.ytdDetailsListFrequency){
                doc.text(10, 42, "Frequency");
                doc.text(80, 42, "-", null, null, 'right');
                doc.text(10, 47, "Period");
                doc.text(80, 47, "-", null, null, 'right');
            }else{
                doc.text(10, 42, "Frequency");
                if(reportControllerScope.ytdDetailsListFrequency.frequency) doc.text(80, 42, reportControllerScope.ytdDetailsListFrequency.frequency, null, null, 'right')
                else doc.text(80, 42, "", null, null, 'right')
                
                doc.text(10, 47, "Period");
                if(reportControllerScope.ytdDetailsListFrequency.period) doc.text(80, 47, reportControllerScope.ytdDetailsListFrequency.period, null, null, 'right')
                else doc.text(80, 47, "", null, null, 'right')
            }

            //reporttable
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 72, "Report");
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 74, 80, 74);
            doc.setFontSize(9);
            doc.setFontType("bold");
            var lastLineSection1 = 30;
            if(!reportControllerScope.ytdDetailsList){
                doc.text(10, 77, "Employee Name");
                doc.text(70, 77, "Bonus Amount");
                doc.text(130, 77, "Bonus Month");
                doc.text(185, 77, "Paid On");
            }else{

                for(var i=0; i< reportControllerScope.ytdDetailsList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(10, 82, "Employee Name");
                if(reportControllerScope.ytdDetailsList[i].empName) doc.text(20,  lastLineSection1+60, reportControllerScope.ytdDetailsList[i].empName, null, null, 'center')
                else doc.text(20, 103, "", null, null, 'right')
                
                doc.text(55, 82, "Earning Year");
                if(reportControllerScope.ytdDetailsList[i].earningYear) doc.text(70,  lastLineSection1+60, parseFloat(reportControllerScope.ytdDetailsList[i].earningYear).toFixed(2), null, null, 'right')
                else doc.text(70, 103, "", null, null, 'right')
                
                doc.text(103, 82, "Net Year");
                if(reportControllerScope.ytdDetailsList[i].netYear) doc.text(114,  lastLineSection1+60, parseFloat(reportControllerScope.ytdDetailsList[i].netYear).toFixed(2), null, null, 'right')
                else doc.text(114, 103, "", null, null, 'right')

                doc.text(140, 82, "Deduction Year");
                if(reportControllerScope.ytdDetailsList[i].deductionYear) doc.text(158,  lastLineSection1+60, parseFloat(reportControllerScope.ytdDetailsList[i].deductionYear).toFixed(2), null, null, 'right')
                else doc.text(158, 103, "", null, null, 'right')

                doc.text(185, 82, "Tax Year");
                if(reportControllerScope.ytdDetailsList[i].taxYear) doc.text(195,  lastLineSection1+60, parseFloat(reportControllerScope.ytdDetailsList[i].taxYear).toFixed(2), null, null, 'right')
                else doc.text(195, 103, "", null, null, 'right')
                lastLineSection1 = lastLineSection1+ 6;
                }

                
            }
            
            //total
            /*doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(92, 33, 'Earnings / Allowances');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(92, 35, 200, 35);
            doc.setFontSize(8);
            var lastLineSection1 = 42;
            for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(92, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].earningName);
                doc.text(200, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
                lastLineSection1 = lastLineSection1+ 6;
            }
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(170, lastLineSection1, 200, lastLineSection1);
            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text(92, lastLineSection1+6, 'Total ( A )');
            doc.text(200, lastLineSection1+6, payslipDetail.earningMonth, null, null, 'right');*/
            //footer
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            doc.line(10, 268, 200, 268);
            doc.setFontType("bold");
            doc.setFontSize(12);
            
            //companyInfo
            
            if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
            else doc.text(110, 274, companyInfo.name , null, null, 'center');
            
            doc.setFontType("normal");
            doc.setFontSize(10);
            
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
            
            if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
            if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
            reportControllerScope.spinner = false;
            doc.save("Ytd_Report");
        })
        
        })
        
    }


    function downloadDeductionPDF(deductionReportDetail){
        toDataURL('images/i_logo.png', function(dataUrl) {
              if(dataUrl) logo = dataUrl;
              else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
        var doc = new jsPDF('landscape');// a4 220*300
        reportControllerScope.spinner = true; 
        var promises = [];
        var deferred = $q.defer();
        promises.push(getActiveFinancialYear());
        promises.push(getActiveBatch());  
        promises.push(getCompanyInformation());
        promises.push(getDeductionTypes());
        
        $q.all(promises).then(function (data) {
            //header
            doc.setFont("Poppins");
            doc.setFontSize(15);
            doc.setFontType("bold");
            doc.text(10, 15,'DEDUCTION REPORT');
            doc.addImage(logo, 'PNG', 225, 3, 65, 20);
            doc.setFontSize(10);
            
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            //doc.setLineWidth(0.1);
            doc.line(10, 24, 286.5, 24);


            //frequency
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 33, 'Report Filters');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 35, 80, 35);
            doc.setFontSize(9);
            doc.setFontType("bold");
            if(!reportControllerScope.deductionDetailsListFrequency){
                doc.text(10, 42, "Frequency");
                doc.text(80, 42, "-", null, null, 'right');
                doc.text(10, 47, "Period");
                doc.text(80, 47, "-", null, null, 'right');
            }else{
                doc.text(10, 42, "Frequency");
                if(reportControllerScope.deductionDetailsListFrequency.frequency) doc.text(80, 42, reportControllerScope.deductionDetailsListFrequency.frequency, null, null, 'right')
                else doc.text(80, 42, "", null, null, 'right')
                
                doc.text(10, 47, "Period");
                if(reportControllerScope.deductionDetailsListFrequency.period) doc.text(80, 47, reportControllerScope.deductionDetailsListFrequency.period, null, null, 'right')
                else doc.text(80, 47, "", null, null, 'right')
            }

            //reporttable
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 72, "Report");
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 74, 80, 74);
            doc.setFontSize(9);
            doc.setFontType("bold");
            var lastLineSection3 = 30;
            var lastLineSection4 = 20;
            if(!reportControllerScope.deductionDetailsList){
                doc.text(10, 77, "Employee Name");
                doc.text(70, 77, "Bonus Amount");
                doc.text(130, 77, "Bonus Month");
                doc.text(185, 77, "Paid On");
            }else{

                for(var i=0; i< 1; i++){
                    doc.setFontSize(8);
                    doc.setFontType("normal");
                    doc.text(10, 82, "Employee Name");
                    
                    for(var j=0; j< reportControllerScope.deductionDetailsList[0].employeeCompensationDeductionList.length; j++){
                        doc.setFontSize(8);
                        doc.setFontType("normal");
                        if(reportControllerScope.deductionDetailsList[0].employeeCompensationDeductionList[j].deductionName) 
                            doc.text(lastLineSection4+30, 82,  reportControllerScope.deductionDetailsList[0].employeeCompensationDeductionList[j].deductionName, null, null, 'center')
                        else doc.text(27, 103, "", null, null, 'right')
                        lastLineSection4 = lastLineSection4+ 19;
                    }
                }
                var tempHgt = 88;
                for(var i=0; i< reportControllerScope.deductionDetailsList.length; i++){
                    doc.setFontSize(8);
                    doc.setFontType("normal");
                    lastLineSection4 = 20;
                    if(reportControllerScope.deductionDetailsList[i].empName) 
                        doc.text(20,  lastLineSection3+60, reportControllerScope.deductionDetailsList[i].empName, null, null, 'center')
                    else doc.text(27, 103, "", null, null, 'right')
                    lastLineSection3 = lastLineSection3+ 6;
                    if(i>0){
                    	tempHgt = tempHgt + 8;
                    }
                    console.log('tempHgt   ',tempHgt);
                    for(var j=0; j< reportControllerScope.deductionDetailsList[i].employeeCompensationDeductionList.length; j++){
                        doc.setFontSize(8);
                        doc.setFontType("normal");
                        
                        doc.text(lastLineSection4+30, tempHgt, parseFloat(reportControllerScope.deductionDetailsList[i].employeeCompensationDeductionList[j].amount).toFixed(2), null, null, 'center')
                        lastLineSection4 = lastLineSection4+ 19;
                    }
                }

               /* for(var i=0; i< reportControllerScope.deductionDetailsList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                if(reportControllerScope.deductionDetailsList[i].employeeCompensationDeductionList.toString()){
                //JSON.parse(reportControllerScope.deductionDetailsList[i].employeeCompensationDeductionList)
                doc.text(80,  lastLineSection3+60, reportControllerScope.deductionDetailsList[i].employeeCompensationDeductionList, null, null, 'center')
                }
                else doc.text(27, 103, "", null, null, 'right')
                lastLineSection3 = lastLineSection3+ 6;
                }*/

                
            }
            
            //total
            /*doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(92, 33, 'Earnings / Allowances');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(92, 35, 200, 35);
            doc.setFontSize(8);
            var lastLineSection1 = 42;
            for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(92, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].earningName);
                doc.text(200, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
                lastLineSection1 = lastLineSection1+ 6;
            }
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(170, lastLineSection1, 200, lastLineSection1);
            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text(92, lastLineSection1+6, 'Total ( A )');
            doc.text(200, lastLineSection1+6, payslipDetail.earningMonth, null, null, 'right');*/
            //footer
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            doc.line(10, 268, 200, 268);
            doc.setFontType("bold");
            doc.setFontSize(12);
            
            //companyInfo
            
            if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
            else doc.text(110, 274, companyInfo.name , null, null, 'center');
            
            doc.setFontType("normal");
            doc.setFontSize(10);
            
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
            
            if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
            if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
            reportControllerScope.spinner = false;
            doc.save("Deduction_Report");
        })
        
        })
        
    }

     function downloadTaxPDF(taxReportDetail){
        toDataURL('images/i_logo.png', function(dataUrl) {
              if(dataUrl) logo = dataUrl;
              else logo = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABQCAYAAAAwa2i1AAAACXBIWXMAACTpAAAk6QFQJOf4AAABOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarZGxSsNQFIa/G0XFoVYI4uBwJ1FQbNXBjElbiiBYq0OSrUlDldIk3NyqfQhHtw4u7j6Bk6PgoPgEvoHi1MEhSHASwW/6zs/hcOAHo2LXnYZRhkGsVbvpSNfz5ewTM0wBQCfMUrvVOgCIkzjiJwI+XxEAz5t23WnwN+bDVGlgAmx3oywEUQH6FzrVIMaAGfRTDeIOMNVJuwbiASj1cn8BSkHub0BJuZ4P4gMwe67ngzEHmEHuK4Cpo0sNUEvSkTrrnWpZtSxL2t0kiOTxKNPRIJP7cZioNFEdHXWB/D8AFvPFdtORa1XL2lvnn3E9X+b2foQAxNJjkRWEQ3X+3YWx8/tc3Bgvw+EtTE+KbPcKbjZg4brIVqtQ3oL78RfCs0/+HAmzJwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAXlklEQVR42uyde2wcR37nP9Xd8+JwOBySEsWHJD5kvR9eUGuvs2s4yVLOY5EXFnL+yV32kJx9uM0lfwRYKRfsJsDdIlZwt0ASbBALSTYBLne3UjZA4mzuEMvrtRPLu1rJtujV26TepPgckkPOcKa7q/JHd0utyZAccqiHnfoAjeFwpqurq+tbVb9f/apGKKXQaDQfbwxdBBqNFrpGo9FC12g0WugajUYLXaPRaKFrNBotdI1Go4Wu0Wi00DUaLXSNRqOFrtFotNA1Go0Wukaj0ULXaDRa6BqNRgtdo9FUwFrtiWLrz67mtDjwX4DngBb/aPD/L4ECMA/kgBngDnAJeB84j1KXvIsLsB1SDUn2793KxcGbrGtOE7UsbMdhYmqWuVwBR0jm88X7MqCuvKqfukYL/QFTAmLAODAIZIFpYMp/LQAOYAJ1QCPQDvSj1M8DY8CbwGtAUT8+jebxE3oMWAcc93vxeiANJHxRb/S/E+TJBhaAURTXTCGk67oNKHb45/2N3zBoNJrHQOitwH8CfgTo8cXeAIiqU3BdtvR20r5h3UxuvnDtzujEWKohuffW8PjfAN/Xj1GjebRC7wG+CeyvKRXTYDKbI2JZ6e5Nbft29HaC4MC27o7/PJPLvwn8PvCWfpwaTWUetNf9N1cscqnAdqBsd9qJ8SlSyQQNqQSO6+K6LnWJeP3GtpbPPfvJ3f8f+JJ+nBrNoxF6Z/UCl7BQIpmI8pmn97Ktu8MTvZRQLLF1yyZ6utuRUqGUwnEkriuxHZeIZSb2bOs6Avy2fqQazcMX+v+iGoeZVDTUJ9nS08G2no3k8wXGJ6bZ2t1BS7qe3bt62buzFwEIIUAIhPCsfFdKpFSAoLer/b/7/gCNRrMWNrrhuJ5GLXOprx0HRoGfADYAUU+euChclHSQ0kEJOxa1nMbmtHP12rA7OTHjfuqp3e7Q1dvEYjFr+xObEvV18aTtuo1As1JqnWmabbbj1BvCqHOVRDoKyzLZta3ra2++M3Ad+H/68Wo0HmK1v9Ri9nwOgFgsQiGXh0TcT9H/guPCYo2AlKSSCbZ0ddC9cQPnL13j4sUhiEW9YBilMC2LX//ln+H8lZtEIybFko10JQYQq4uLfdu7W7539lJXNGJt39DSuMeVss+23b3xeLTlztjU2NvfP/dpR8gPdcCMRvOw5tEV4LpgGiCEgRcQI/zXiH8Y/hE0FdI/bP9w/Pduc2NK/d9Xvzu+Z1v3+KkPrvygvm8nDXUJpMnW6zdHD8zlF361sSn1jYnszEG86DqNRgv9AbIe2A08CfTizaE3olQqEY/WdWxoiQ9cHIrdHpmIEolauMoAV+zc3sPWrg51ZyzrpBvqSigWhBAFYNYUYso3B+4AI8BtvEi7se+ePHttXUvj1y98ePPrG9vX/Ypjuz8J/G+8iDyNRg/d13jonsZxvwi8gGnsMYRhrG9uYHI6h+3b9vGIRSIWZTo3j7JdPrl/J5NTM6xrStPb1YZhGHe/K4TAMg0EAseVuNIlFo1iOw6GEBiGKCipJsanZkYmsrnLFz68+U5hbv5bRCMFYBb8EQWgBr+9qvvdsOtnH9Uz6vGPLHDm31j9zAB9/t8nyj/89uypf3OC7bu5ugHqg+jRn0C6f9nYUPfM3h1bSNbFsCIWg9eHGZvIgvAc/Qu2w8JCESTs2/MEW7vbEd0dCENgOy6u7QBgGAYKsG0XKSXC8FqSkm17HnjAdWVCSrWxOZPeuLlj/VP1idgvvfX2e19Fqb9CiN/ze/214BDQX8P5Z4DDK/j+i8ArofdHgZeqPPegf/5qGfLzmq3w2TFfhLVwAjiyVJ3GW9OQCZXdgUXyA/Cy/3xewHMCr5aD/v3hX+9Eld+ttjyPV0ijf5lndaKK8nqoQk9QLH0j1VD/zPPPfZK6uhiOIxHA6HgW6SjPGvcxLJOezg3s3LYZhMB1JMpVIASGYWCaBlIqT+BCYFkmKmTECwFSKlzXRSpFzDTIzRXIFxbo3bKp8cbw2Bdt2/kspnkQ+GGN9xZUpJoa5BUIva9M5IHwh6p84D01NkpBHvYvUsFZg7SPLNGTlzcmQXm88IA7zZfL/t5fZaN1ZplncdBP7/giDcVSaQQdzNBqG7E1E3p91KJYsv/dxu6OT+/Z0UMkYpEvFDGEQSxqsb13Iyez5wgbCgaQaUzhOC6zs/OkUnUYhoHw5VxYKGEaAiEMpFIIgdejC4FA4boKhcQyDSReMF0uX+D8pWt8+uk97Nq2mRNvvbs9P1f4Cyzjx/CWv9bSmwe96lANPXq1Q9ZjZT3Zy77QX66iYgX5rHX00udXsPJe7UBoSF2pUge901KN2lIV9pWQuXIgJPKDfr6OAHyu4am1Hr4f8q973H/t8+/laBXPdbkG/LSfXiY0KukPldORZUYDr/jnP1KhPwv8WG9P53NPf2KbF7UmXQwhUEphOy71yQQK4U27md7wXUhFPBIhlUxwZ2yKQrHEpo51FEs2SkE0aiFdhQJM00tLIEBJSo7EEALLtEAoIqaJkop4LIIwDU6+e4Ent3ezoaWRfDLeZyt+fXpq9qtrcK9HahB6tYQr+kuh1z7/OOb3NNkl0sjWMtTzRdXn52OxoWQlwsPQI6sU28GQAM74R4//WbUN3Wp8IYf8cjvsv3/N/9/RB/ScM4v5Hyo0ij213HOtkXEG8OXn+z/1pV9+4fnXE5HIV0tF21Z4gvSi1iQIQSadoiVdT2dbC6m6OFHTZOvWLkzLZGxymrp4jLbWJkq2i2VZJOJRcrk8SikilokQAoFASolpmsSiFgiBVArbdjl/4RrZmRyzuQKO7dK+vhlXKnZt72bn9m62dLX/KrCpRpsVvHX0aoXHlF9p+ldZ0QNe8CtiT4Vh/WJifW0V+VVLOcEepK8pNHQ+WiawcFmshY+g0pA9E2rIT4QEVqvJdtBPZ6iscc6GnnlmmUb7cC2+h1UL/Wf6n6a5qeG3f+6nPvP805/Y/h+At4HvnPngyu+bhoErJQLPrjaF4MrQLXq62mlvbcYt2jQ1pljf0sjZcx8yfGeCTCaFACKmgQAuXbnB8J1JTNP0RgW2ixICYQiGRyaQUpGsi7FQKvGdk+/z7plznLt0jXg8SmdbC+ub07Stz9CcaWBmZh7Hcbv27uypxWZ9YZkedLmWu98XXd8qK3rQ2LwUqjxLVcB+XxC13PNLD2H0spi5cniJZ3C3oftcw1Nrce2D/lHu/zhSpRAPLdNoBg3T0UVGhwf9zmCx80/X2tjUMnTf++SeJ754/cadn35ic/sEwMjYFMBX5uYXzsVjkd8wDGOX67r1U9M5Lly5jhWPuaViyV0oFu1IMSpbm9LquWf23Z3gk1IJ23GIRkyxobWZdCopHNcVjiOFaQljeiZnnL9y3bp9a8zYtbOHlkwD61saka6EaITZuQIoxd5dvYxOTHPt5hip+hyWZdDZth5QnwX+vAb7uncZoS7XYwQ230uLVPRXqrD5jvsV5kX/4S82jO2rwnu+3AjmYYk8KJ9yc2Uxz3Vgr1djP6/EAXe4wjM/4pfzUo7AakyJSmbHkF+nDi5RrzL+ffbVYA6tXugbO1tfmJzJqVu3Rt/7679/M/yRBP5PKpn4piHEvqmZuR0DF6825BeKBYqlWQxjDtOctl250JRJOaWiI2fm8momNy8KCyVxY2RCPPPkNrO1JWPM5ObNufyC1dnWYt0YHrNe/6d3423t6xrMqJUZ+ODDjBG1WttbmzvjdfENzOXbs7n5zldf/36iob6O9c2NZKdzTE5mefaZT5CIR5mdz++psUJkaxjKnljC5g03BEtV9PAwti/kpFpq2ukotU03PQxeLHPgnVnGydgXckyeqdFePxR6Jseq8FmcWaUzLnCo9VdI4/gyz+hErebKqoU+cH7ok5Mzc029XR17m5vTZy8P3mR+foGSH+TiC/49/3igjIxnkwjRDKxHiF2zufwzszNz/Qh6E8kkDfUJEIJUsq65RmfNadZm/rhSJaq2ohNqDAJT4BArm59/nOhZxlyppqHbX8O1D1XRUwbTY7Vcq9YOpiZWLfRbY1PdhlJRqdQfAr+IH1P+0599iqbGFCNjWS5fu82ebZseRkHM+8cNX4x/ScSqw3F+Pp2q+61MY/1uyzS4PTJRi6nSvwYiP7pIhepZYUUP9yKv1GBOPAiGVlg5M/6xkmCi8oauVgfc0SquPUj1022LjQjKy+WY//+Xlkgz3AmsWvCrDoHd81NfHOlsa9lgOw6d7evPXh68+dXbt8df7X+ub6Fc6FLCtdujvHP6PEXbBePednGWgM0bN7BryyasiMX1D29gJGJ0tK1jYaFE96YNTM/MkZsvYBgGI6NT/ODMORLpFLbt4Djuv959zpXeFJ7j0JRJt6XrE99sb2t59vLQ7atjp/6qZzX364fA1uLYWi6EtX+VZkFfBW9urWnWSmCCDK3BPSzXI/Pt2VNDVB/QdNy3tYNglaxvJy937cDGDuxqWFlkXNA4h82s8sjH5UaCBx56COwT3R2OKyVNmQZs296XSiWP1dXPXQD+ETgJXMZbfDLJw15UYt43mTAyMjX7a1dv3DmFadx4AMPutWK1aZ95RPldbZ7W4vvlo4ew46zacwIP+5kqG5gjoRHISq8ZXO9ohRHeUe6Pxlvs2jUN31ct9FR9YsJ23E6BYHp2nrffGSCfL+xIp5I7tnR3/IbjOjOtLY03SyVn2LKsYeAm3iqzKe7t576At/w0OIKlqeXTC/5+Mph4m1c04u0k24i3VfQMcBE4f//dWUzl5kGpASxzDCF+gObjynIOrUqNy90Gppppum/PnjpSQbwr9o1UiOg7XM05sPo116sW+sjY1AfrmtJPgiIWjbBj2yamZ/NkZ+coLJRIxCPpxob6tOO4u8GlsaHeC3/1htne+nKlpFTIja0tKpVMKMeVdPVuVNGoBQgVsSymsjmEgHg0SjQaEQ2phNHUmIps3dZlvvfBlYijlEAICcwBA8AfAN/yG4iAnQgRx9uRVvMxIxBBIJ41mltf9DoVGoAVf7aatGph1QEzfbu3vGH7mzNGoxG2dHfydN8Odm/rAhRSKhzHwTS91WfFok3IHxABEgiRlEqlBs4PNszO59ORiJlOxGONN26NN37/vYuZaNTMxGJWRikypmlkXNdpbG3JNLSsyySaGxuijQ31AqmC+2gAPuO36r8bymoaIf4A+DPfUae538YdfATXHWTxacaahPigRL5Unpe65krzs8T31SMRelND8oRlGFelUkgliUYsHNslGokQj0V9Vx84rotCUSyWsG3HW3J2Xw4MpmbmeOPkWYZHJxkZz3LyzHmGrg7zxj+/z+C1YQxTeLHuQMSyUEpx8coNHCkXu4PfAn4U+ATwDV/gX37MBXea+yOhgqmfF8scQsGc8+ky8+a1kANuqsL/g3OCzwYrOJvC6R0KVfBwGO0U9xZnnF7knMXSCnOsLM1+7i2kCZfLoSrKqb/se8H7Hj/twQrnLJZWpXLqKUu7Uvm+wr8ON34x9NlKnlVfhTw/mh4duDk2Of1nsWiEiGVyfXgc27axLBOUtyWzkhCLRIiYFol4DNOyKrdLlsnc/ALfOXmWN743gDIERC2Gb43x4dBtopEI4C1ddaUkVV/H4NVbTE7nwKh4CxHgK8AvAH/iC995jIUeTNkEvojj3FtMEQg7WKp4NOQUEqEjw72lkPtD/8+WNRZNoWv0h9J6MXROk/++J+TUC+ctmPIZCv2/l3uhokulFXZEBd95yc93tswxdajKcjrBvbDYfv/1eOjeDoTOOREq00ppLVVOLFO+2bJ7CiLa+kPpCe6tjFssrUNleTv8yIQ+Mj7N3m1df9TUkHz30uUblIolYrEoruNSsp27onx34BIzuTmiUcs3zxcZgZgGGMbdRTAAxKJMzxd4/4dXMA0DJRWO6zIylvWm6MSSv+r0t8Dv4M0CPO70lbX64XDQA9xb6HIgdM6JCs6lTIXe4CD3b94QdgCdCF2/p6yH7QmJ83iF65SnN+RX5uXSqpT/IM2joR46aLiOVlFO+MIK4uUPlHnXh8rymVkmrfJyKs/DYuV7ouz+ghiB8qnDXv8ai6WVKcvDiUcmdJ/ZH39m3xc2btpwZUtXB66UONIlmUwwPDrJqfcukp9fwDRNrt0YwZlf8LZ0cvzDdrwj/N6VodwJikWbgfNXOT1wmUjERElJoVi6u1PNIrzr2+SKjwZBAEi4NwjPtQ6F/g4PIcsbiyz35oWDtI5Wef2hshFCNfPa5cPsqRWk1V8h74FgAxOlfFppqXLq516Iavlwv6fsfXaZtJYrq5WUb7AIJ1Nm7/ctkVa2rGxr3UBkTdajf+A4bv9EdvbL61vSv+i6MjU6OsX5y9dxXcm2LRv54YWrTOfm2ffkViLRCJGIhW07LCyUiMUsYtEowhCM3JnEdlxuj0/5P8rg9fRKwYXL13GlJDudY3pm7t5cuVR+Md3t3d8H/r3vhf+ocMTviV4JPegXuLcu/YBfUYKtlYKKOlXWmxz33w+W9ZZHlqksZ/zzVVkvslzM+Wtl5xxeQVovVrjf4LuH/KO3ynIKhrtBTx7Yy4F4j4WEc8Y/J7NIWss5CQ8vUr6HlinbqQrlsVhaQWz7y2vVo686Mu4//tc/uu/9/r1P1L93fqh5fqH4+ZPvDPxIZ2drrzCMtgtXrje3NKasvie3IoRA+bqUUmGZJgiQSqIURCyTkbEs//jWGdxKFw2cecF+8UqRiMdwHBfbdS8Cfw38Id58fWXX5eW/W9X9PsLNIRez6fuofv+4jxqBvX6kxnSCzSN6Py4Fc+fc6urvWu0wE/mHN0//wsXBW+Ly+atfSzenv/apjnX18WhkY108un77lk3tl4ZutqdTyTZXyg1SqtZEPNqULxZTAuoEIi6VtKRrGZZhCMMQuK5S/p5Sru9IKxGx8ng/8TQHTKPUhGWZg1Kqd3Hds3hBOZqPNop7oaKaNWKthG5/9+TAidmZ3DHg14BXxqdm3zDgwrrm9IWZ3Dxbuzt56wc/ZMvmNkzDMACrvi5uJuKxuGM70cvXhq3uztZIb1e7aPogzejopMIypS/y4AccioR+yOFh2uCrbUkfEEc/xnVSrGFaQx+n3vxxEDrACJb5K0Qj/zybL/xpsVgaScSiA8ApvB1YLwLXI5Y1A0qWSk7JcVwWSnYhN5dny+Z2xrMzDJwfZHRkHKKRKlyJBrn5gn6KGs1DFDrAZaT8QjwW/VZrc2PbbC7fdmno1k9s7e5Uw2NT85vaWqbxfl1lDJjAi3ef84fjwTy34efL9F8tvHnx4NXk3mxBCS/89wTwjn6cGs3DETrAPwghfrOwUPy6VJJN7etxHFcIqE831NfPzRc6AeKxCEqBklCfSJDLF0jX1+G2rcO6eM2LehNVj+IOAV/Ac8ZpNJrywe8DSvePh26OfiXdkCRimRiGwLIMbty8w8Ur1++aYo7rIqWLYQhys/OUbIdEPIppGiu1vpO+0A39SDWah9OjB/w315FOXTz+OwvFUiwWi5FOJ9nQ2owQgpLtYBre64UrNzl/6Sq7d/UigGLJXiy0dSlG+egEyGg0HxuhA/ze2QtDp5Lx6JccV34mWRevSyTiuK5LPBpBoXjn9DnGszmwLM6eG0RJyjeOqIZzwP/QQtdoHo3QAV4HXj/3wYfPFmz785/90f37Wlsy62fm5jcn6xLJfNEmP5f3vOxCgCmqmWAp4DnzhoDvAX+Mt1+cRqN5REIP+CcpjNNvn7nw+bpkvEM6bnbXE5sLsWjUEBErobz16XV+ngT35s3zoSOHF0o4ExL7nH6MGs3jI3SAQj6/cDw/n38exLOn37+0fvvWzbMj41M3XKWGfQHn8YJhLCABpIBmoA1v66gkcB34n/73NBrNYyZ0/F76VeDVgm3vfG/g8i4i1pPAU8AGvJ1ikkC9L3QLLzIu6M0n8RxvMb9R0Gg0y7DqRS0ajeajg5531mi00DUajRa6RqPRQtdoNFroGo1GC12j0WihazQaLXSNRqOFrtFooWs0Gi10jUajha7RaLTQNRqNFrpGo9FC12g0WugajaYS/zIAJw2Ex0UIpVoAAAAASUVORK5CYII="
        var doc = new jsPDF('landscape');// a4 220*300
        reportControllerScope.spinner = true; 
        var promises = [];
        var deferred = $q.defer();
        promises.push(getActiveFinancialYear());
        promises.push(getActiveBatch());  
        promises.push(getCompanyInformation());
        promises.push(getTaxType());
        
        $q.all(promises).then(function (data) {
            //header
            doc.setFont("Poppins");
            doc.setFontSize(15);
            doc.setFontType("bold");
            doc.text(10, 15,'TAX REPORT');
            doc.addImage(logo, 'PNG', 225, 3, 65, 20);
            doc.setFontSize(10);
            
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            //doc.setLineWidth(0.1);
            doc.line(10, 24, 286.5, 24);


            //frequency
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 33, 'Report Filters');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 35, 80, 35);
            doc.setFontSize(9);
            doc.setFontType("bold");
            if(!reportControllerScope.taxDetailsListFrequency){
                doc.text(10, 42, "Frequency");
                doc.text(80, 42, "-", null, null, 'right');
                doc.text(10, 47, "Period");
                doc.text(80, 47, "-", null, null, 'right');
            }else{
                doc.text(10, 42, "Frequency");
                if(reportControllerScope.taxDetailsListFrequency.frequency) doc.text(80, 42, reportControllerScope.taxDetailsListFrequency.frequency, null, null, 'right')
                else doc.text(80, 42, "", null, null, 'right')
                
                doc.text(10, 47, "Period");
                if(reportControllerScope.taxDetailsListFrequency.period) doc.text(80, 47, reportControllerScope.taxDetailsListFrequency.period, null, null, 'right')
                else doc.text(80, 47, "", null, null, 'right')
            }

            //reporttable
            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(10, 72, "Report");
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(10, 74, 80, 74);
            doc.setFontSize(9);
            doc.setFontType("bold");
            var lastLineSection3 = 30;
            var lastLineSection4 = 20;
            if(!reportControllerScope.taxDetailsList){
                doc.text(10, 77, "Employee Name");
                doc.text(70, 77, "Bonus Amount");
                doc.text(130, 77, "Bonus Month");
                doc.text(185, 77, "Paid On");
            }else{

                for(var i=0; i< 1; i++){
                    doc.setFontSize(8);
                    doc.setFontType("normal");
                    doc.text(10, 82, "Employee Name");
                    
                    for(var j=0; j< reportControllerScope.taxDetailsList[0].employeeCompensationTaxList.length; j++){
                        doc.setFontSize(8);
                        doc.setFontType("normal");
                        if(reportControllerScope.taxDetailsList[0].employeeCompensationTaxList[j].taxName) 
                            doc.text(lastLineSection4+30, 82,  reportControllerScope.taxDetailsList[0].employeeCompensationTaxList[j].taxName, null, null, 'center')
                        else doc.text(27, 103, "", null, null, 'right')
                        lastLineSection4 = lastLineSection4+ 19;
                    }
                }
                var tempHgt = 88;
                for(var i=0; i< reportControllerScope.taxDetailsList.length; i++){
                    doc.setFontSize(8);
                    doc.setFontType("normal");
                    lastLineSection4 = 20;
                    if(reportControllerScope.taxDetailsList[i].empName) 
                        doc.text(20,  lastLineSection3+60, reportControllerScope.taxDetailsList[i].empName, null, null, 'center')
                    else doc.text(27, 103, "", null, null, 'right')
                    lastLineSection3 = lastLineSection3+ 6;
                    if(i>0){
                        tempHgt = tempHgt + 8;
                    }
                    console.log('tempHgt   ',tempHgt);
                    for(var j=0; j< reportControllerScope.taxDetailsList[i].employeeCompensationTaxList.length; j++){
                        doc.setFontSize(8);
                        doc.setFontType("normal");
                        
                        doc.text(lastLineSection4+30, tempHgt, parseFloat(reportControllerScope.taxDetailsList[i].employeeCompensationTaxList[j].amount).toFixed(2), null, null, 'center')
                        lastLineSection4 = lastLineSection4+ 19;
                        //parseFloat(reportControllerScope.ytdDetailsList[i].taxYear).toFixed(2)
                    }
                }

               /* for(var i=0; i< reportControllerScope.taxDetailsList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                if(reportControllerScope.taxDetailsList[i].employeeCompensationTaxList.toString()){
                //JSON.parse(reportControllerScope.taxDetailsList[i].employeeCompensationTaxList)
                doc.text(80,  lastLineSection3+60, reportControllerScope.taxDetailsList[i].employeeCompensationTaxList, null, null, 'center')
                }
                else doc.text(27, 103, "", null, null, 'right')
                lastLineSection3 = lastLineSection3+ 6;
                }*/

                
            }
            
            //total
            /*doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(92, 33, 'Earnings / Allowances');
            doc.setFontType("normal");
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(92, 35, 200, 35);
            doc.setFontSize(8);
            var lastLineSection1 = 42;
            for(var i=0; i< payslipDetail.employeeCompensationEarningList.length; i++){
                doc.setFontSize(8);
                doc.setFontType("normal");
                doc.text(92, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].earningName);
                doc.text(200, lastLineSection1, payslipDetail.employeeCompensationEarningList[i].monthly, null, null, 'right');
                lastLineSection1 = lastLineSection1+ 6;
            }
            doc.setDrawColor(232,232,232);
            doc.setLineWidth(0.3);
            doc.line(170, lastLineSection1, 200, lastLineSection1);
            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text(92, lastLineSection1+6, 'Total ( A )');
            doc.text(200, lastLineSection1+6, payslipDetail.earningMonth, null, null, 'right');*/
            //footer
            doc.setFontType("normal");
            doc.setDrawColor(0,37,77);
            doc.line(10, 268, 200, 268);
            doc.setFontType("bold");
            doc.setFontSize(12);
            
            //companyInfo
            
            if(companyInfo.name && companyInfo.website) doc.text(110, 274, companyInfo.name + " "+"|"+" "+companyInfo.website, null, null, 'center');
            else doc.text(110, 274, companyInfo.name , null, null, 'center');
            
            doc.setFontType("normal");
            doc.setFontSize(10);
            
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && !companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 +", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && !companyInfo.city &&  companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  !companyInfo.zip && companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+", "+companyInfo.stateName+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && !companyInfo.stateName && companyInfo.countryName ) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.countryName, null, null, 'center');
            if(companyInfo.address1 && companyInfo.address2 && companyInfo.city &&  companyInfo.zip && companyInfo.stateName && !companyInfo.countryName) doc.text(105, 280, companyInfo.address1 + ", "+companyInfo.address2+", "+companyInfo.city+" - "+ companyInfo.zip+", "+companyInfo.stateName, null, null, 'center');
            
            if(companyInfo.callingCode && companyInfo.mobile && companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile+" | "+'Email: '+companyInfo.email, null, null, 'center');
            if(companyInfo.callingCode && companyInfo.mobile && !companyInfo.email )doc.text(108, 285, 'Off: '+companyInfo.callingCode.substring(0,2)+" "+companyInfo.mobile, null, null, 'center');
            reportControllerScope.spinner = false;
            doc.save("Tax_Report");
        })
        
        })
        
    }


    function getCompanyInformation() {
        var deferred = $q.defer();
        companyInformationService.getCompanyInformation().then(function(res) {
            if (res.successflag == 'true' && res.results.length > 0) {
                companyInfo = res.results[0];    
            } 
            deferred.resolve();
        }, function(err) {
            deferred.reject();
        });
        return deferred.promise;
    }

    $(".select1").select2();
}