package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeonboard")
@TableGenerator(name ="employeeonboard", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeonboard.findAll", query = "SELECT r FROM EmployeeOnboardDO r where r.status <>:status"),
    @NamedQuery(name = "employeeonboard.findById", query = "SELECT r FROM EmployeeOnboardDO r where r.status=:status and r.id =:id")
})
public class EmployeeOnboardDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeonboard.findAll";
	
	public static final String FIND_BY_ID = "employeeonboard.findById";
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeonboard")
	private Long id;
	private String firstname;
	private String lastname;
	private String middlename;
	private String personalcontact;
	private String primarycontact;
	private String personalemail;
	private String aadhar;
	private String panno;
	private String dateofbirth;
	private char gender;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdBy;
	private char status;
	@Column(columnDefinition="LONGTEXT")
	private String reason;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMiddlename() {
		return middlename;
	}
	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}
	public String getPersonalcontact() {
		return personalcontact;
	}
	public void setPersonalcontact(String personalcontact) {
		this.personalcontact = personalcontact;
	}
	public String getPrimarycontact() {
		return primarycontact;
	}
	public void setPrimarycontact(String primarycontact) {
		this.primarycontact = primarycontact;
	}
	public String getPersonalemail() {
		return personalemail;
	}
	public void setPersonalemail(String personalemail) {
		this.personalemail = personalemail;
	}
	public String getAadhar() {
		return aadhar;
	}
	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}
	public String getPanno() {
		return panno;
	}
	public void setPanno(String panno) {
		this.panno = panno;
	}
	public String getDateofbirth() {
		return dateofbirth;
	}
	public void setDateofbirth(String dateofbirth) {
		this.dateofbirth = dateofbirth;
	}
	public char getGender() {
		return gender;
	}
	public void setGender(char gender) {
		this.gender = gender;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
}