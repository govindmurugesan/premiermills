package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class AllowanceSettingsDAO {
	static Logger logger = Logger.getLogger(AllowanceSettingsDAO.class.getName());
	private EntityManager em = null;
	
	public AllowanceSettingDO persist(AllowanceSettingDO allowanceSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && allowanceSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(allowanceSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return allowanceSettingDO;
	}
	
	public AllowanceSettingDO update(AllowanceSettingDO allowanceSettingDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && allowanceSettingDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(allowanceSettingDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return allowanceSettingDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<AllowanceSettingDO> retrieve() throws AppException {
		List<AllowanceSettingDO> allowanceSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(AllowanceSettingDO.FIND_ALL);
				/*q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);*/
				allowanceSettingList = (List<AllowanceSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return allowanceSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AllowanceSettingDO> retrieveById(Long Id) throws AppException {
		List<AllowanceSettingDO> allowanceSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(AllowanceSettingDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				allowanceSettingList = (List<AllowanceSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return allowanceSettingList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AllowanceSettingDO> retrieveActive(String name) throws AppException {
		List<AllowanceSettingDO> allowanceSettingList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(AllowanceSettingDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.NAME, name);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				allowanceSettingList = (List<AllowanceSettingDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return allowanceSettingList;
	}
}
