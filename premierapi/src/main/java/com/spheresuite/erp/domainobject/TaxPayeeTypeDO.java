package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="taxpayeetype")
@TableGenerator(name ="taxpayeetype", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "taxpayeetype.findAll", query = "SELECT r FROM TaxPayeeTypeDO r"),
    @NamedQuery(name = "taxpayeetype.findById", query = "SELECT r FROM TaxPayeeTypeDO r where r.id =:id"),
    @NamedQuery(name = "taxpayeetype.findByStatus", query = "SELECT r FROM TaxPayeeTypeDO r where r.status =:status"),
})
public class TaxPayeeTypeDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "taxpayeetype.findAll";
	public static final String FIND_BY_ID = "taxpayeetype.findById";
	public static final String FIND_BY_STATUS = "taxpayeetype.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "paymentterm")
	
	private Long id;
	private String taxpayeeType;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTaxpayeeType() {
		return taxpayeeType;
	}
	public void setTaxpayeeType(String taxpayeeType) {
		this.taxpayeeType = taxpayeeType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}