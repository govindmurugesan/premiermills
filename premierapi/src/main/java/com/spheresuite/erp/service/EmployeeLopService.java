package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeLopDAO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeLopService {
	static Logger logger = Logger.getLogger(EmployeeLopService.class.getName());
	
	public EmployeeLopDO persist(EmployeeLopDO employeeLopDO) throws AppException {
		return new EmployeeLopDAO().persist(employeeLopDO);
	}

	public List<EmployeeLopDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeLopDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeLopDO> retrieve() throws AppException {
		return new EmployeeLopDAO().retrieve();
	}
	
	public List<EmployeeLopDO> retrieveById(Long id) throws AppException {
		return new EmployeeLopDAO().retrieveById(id);
	}
	
	public EmployeeLopDO update(EmployeeLopDO employeeLopDO) throws AppException {
		return new EmployeeLopDAO().update(employeeLopDO);
	}
	public List<EmployeeLopDO> retrieveByEmpWithDate(List<Long> empIds, String fromMonth) throws AppException{
		return new EmployeeLopDAO().retrieveByEmpWithDate(empIds, fromMonth);
	}
	
	public List<EmployeeLopDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) throws AppException {
		return new EmployeeLopDAO().retrieveByEmpIdBetweenDate(empIds, fromMonth, toMonth);
	}
}
