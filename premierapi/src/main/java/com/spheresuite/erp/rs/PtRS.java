package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.PtDO;
import com.spheresuite.erp.service.PtService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.PtUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/pt")
public class PtRS {

	String validation = null;
	static Logger logger = Logger.getLogger(PtRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				PtDO ptDODO = new PtDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		ptDODO.setFromamount(!inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()) : null);
			 		ptDODO.setToamount(!inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()) : null);
			 		ptDODO.setPtamount(!inputJSON.get(CommonConstants.PTAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.PTAMOUNT).toString()) : null);
			 		ptDODO.setCountryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY_ID).toString() : ""));
			 		ptDODO.setStateId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE_ID).toString() : ""));
			 		ptDODO.setUpdatedon(new Date());
			 		ptDODO.setStatus('a');
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ptDODO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			ptDODO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new PtService().persist(ptDODO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveByState/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<PtDO> ptDOList = new PtService().retrieveByState(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = PtUtil.getPtList(ptDOList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PtDO> ptDOList = new PtService().retrieveActive();
				respJSON = PtUtil.getPtList(ptDOList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<PtDO> ptDOList = new PtService().retrieve();
				respJSON = PtUtil.getPtList(ptDOList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				PtDO ptDODO = new PtDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<PtDO> ptDO = new PtService().retrieveActiveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		ptDODO = ptDO.get(0);
			 		ptDODO.setFromamount(!inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.FROMAMOUNT).toString()) : null);
			 		ptDODO.setToamount(!inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.TOAMOUNT).toString()) : null);
			 		ptDODO.setPtamount(!inputJSON.get(CommonConstants.PTAMOUNT).toString().isEmpty()? Long.parseLong(inputJSON.get(CommonConstants.PTAMOUNT).toString()) : null);
			 		ptDODO.setCountryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.COUNTRY_ID).toString() : ""));
			 		ptDODO.setStateId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.STATE_ID).toString() : ""));
			 		ptDODO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			ptDODO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		ptDODO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new PtService().update(ptDODO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Wrok Location Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
