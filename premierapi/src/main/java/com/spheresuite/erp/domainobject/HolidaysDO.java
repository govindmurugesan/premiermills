package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="holidays")
@TableGenerator(name ="holidays", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "holidays.findAll", query = "SELECT r FROM HolidaysDO r"),
    @NamedQuery(name = "holidays.findById", query = "SELECT r FROM HolidaysDO r where r.id =:id"),
    @NamedQuery(name = "holidays.findByHoliday", query = "SELECT r FROM HolidaysDO r where r.description =:desc and r.year =:year and r.leavedate =:date"),
    @NamedQuery(name = "holidays.findByHolidayUpdate", query = "SELECT r FROM HolidaysDO r where r.description =:desc and r.year =:year and r.leavedate =:date and r.id NOT IN :id"),
})
public class HolidaysDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "holidays.findAll";
	public static final String FIND_BY_ID = "holidays.findById";
	public static final String FIND_BY_HOLIDAY = "holidays.findByHoliday";
	public static final String FIND_BY_HOLIDAY_UPDATE = "holidays.findByHolidayUpdate";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "holidays")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String leavedate;
	private String year;
	private String description;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLeavedate() {
		return leavedate;
	}
	public void setLeavedate(String leavedate) {
		this.leavedate = leavedate;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}