package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class SalutationDAO {
	static Logger logger = Logger.getLogger(SalutationDAO.class.getName());
	private EntityManager em = null;
	
	public SalutationDO persist(SalutationDO salutationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && salutationDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(salutationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salutationDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SalutationDO> retrieve() throws AppException {
		List<SalutationDO> salutationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalutationDO.FIND_ALL);
				salutationList = (List<SalutationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salutationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalutationDO> retrieveActive() throws AppException {
		List<SalutationDO> salutationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalutationDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				salutationList = (List<SalutationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salutationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalutationDO> retrieveById(Long Id) throws AppException {
		List<SalutationDO> salutationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalutationDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				salutationList = (List<SalutationDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salutationList;
	}
	
	public SalutationDO update(SalutationDO salutationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(salutationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salutationDO;
	}

}
