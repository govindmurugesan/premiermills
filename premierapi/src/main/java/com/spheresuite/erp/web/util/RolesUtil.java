package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.AssignedMenuDAO;
import com.spheresuite.erp.domainobject.AssignedMenuDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;


public class RolesUtil {
	
	private RolesUtil() {}
	

	public static JSONObject getRolesList(List<RolesDO> rolesList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (RolesDO role : rolesList) {
				resultJSONArray.put(getRoleDetailObject(role));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getRoleDetailObject(RolesDO role)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(role.getId()));
		result.put(CommonConstants.NAME, String.valueOf(role.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(role.getStatus()));
		result.put(CommonConstants.DESCRIPTION, String.valueOf(role.getDescription() != null ? role.getDescription() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(role.getUpdatedon())));
		if(role.getUpdatedby() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(role.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(role.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		AssignedMenuDAO  obj = new AssignedMenuDAO();
		List<AssignedMenuDO> list = obj.retrieveByRoleId(role.getId());
		JSONArray resultJSONArray = new JSONArray();
		if(list.size() > 0){
			for (AssignedMenuDO l : list) {
				resultJSONArray.put(getMenuObject(l));
			}
		}
		result.put(CommonConstants.MENU, resultJSONArray);
		return result;
	}
	
	public static JSONObject getMenuObject(AssignedMenuDO menu)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.SUB_MENU, String.valueOf(menu.getMenuId()));
		return result;
	}
	
}
