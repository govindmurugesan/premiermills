package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.InvoiceTermDAO;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.exception.AppException;

public class InvoiceTermService {
	static Logger logger = Logger.getLogger(InvoiceTermService.class.getName());
	
	public List<InvoiceTermDO> persist(List<InvoiceTermDO> invoiceTermList) throws AppException {
		return new InvoiceTermDAO().persist(invoiceTermList);
	}

	public List<InvoiceTermDO> retrieveById(Long proposalId) throws AppException {
		return new InvoiceTermDAO().retrieveById(proposalId);
	}
	
	public boolean delete(Long proposalId) throws AppException {
		return new InvoiceTermDAO().delete(proposalId);
	}
}
