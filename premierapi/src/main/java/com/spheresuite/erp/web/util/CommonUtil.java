package com.spheresuite.erp.web.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.json.simple.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.domainobject.RecentActivityDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.service.RecentActivityService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;

public final class CommonUtil {

	private CommonUtil() {

	}

	public static String convertDateToString(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToYearWithOutTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithdatetime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutComma(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static String convertDateToStringWithOutDate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertDateToStringWithOutDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM yyyy");
		if(date != null){
			try {
				return formatter.parse(date);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static String convertDateToStringWithtime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertEmailDate(String sentDate){
		DateFormat readFormat = new SimpleDateFormat( "EEE MMM dd HH:mm:ss z yyyy");
		Date date = null;
	    try {
	    	if(sentDate != null){
	    		date = readFormat.parse( sentDate );
	    		return date;
	    	}else{
	    		return null;
	    	}
	    } catch ( ParseException e ) {
	        e.printStackTrace();
	    }
		return null;
	}
	
	public static String convertEmailDateToDate(Date date){
		DateFormat writeFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		if( date != null ) {
			return writeFormat.format( date );
	    }
		return null;
	}

	public static String convertDateToOnlyTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		if(date != null){
			return formatter.format(date);
		}else{
			return null;
		}
	}
	
	public static Date convertStringToDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToSqlDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToSqlDateWithLocale(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat parser = new SimpleDateFormat("dd MM yyyy");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertStringToSqlDateMonth(String dateStr){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat parser = new SimpleDateFormat("MMM dd yyyy");
		if(dateStr != null){
			try{
				Date thedate = parser.parse(dateStr);
				String dateStrReformatted = formatter.format(thedate);
				return convertStringToSqlDate(dateStrReformatted);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			return null;
		}
		return null;
	}
	
	public static Date convertStringToDateTime(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateTimeReturnDate(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToDateTimeFmt(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dateResp = null;
		if(date != null){
			try{
				dateResp = formatter.parse(date);
			}catch(ParseException e){
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	
	public static Date convertStringToTime(String date){
		SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
		Date dateResp = null;
		if(date != null){
			try {
				dateResp = formatter.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else{
			dateResp =  null;
		}
		return dateResp;
	}
	public static String convertDoubleValueWithTwoDecimalPoints(Double number){
		DecimalFormat format = new DecimalFormat("0.00");
	    String formatted = format.format(number);
	    return formatted;
	}
	
	public static String convertfloatValueWithTwoDecimalPoints(double number){
        DecimalFormat dec = new DecimalFormat("#.00");
        double rawPercent = ((double)(number)) * 1.00;
        String formatted = dec.format(rawPercent);
	    return formatted;
	}
	
	public static String convertnumberValueWithcomma(double value) {
	    if(value < 1000) {
	    	return new DecimalFormat("###.00").format(value);
	    } else {
	        double hundreds = value % 1000;
	        int other = (int)(value / 1000);
	        return new DecimalFormat(",##").format(other) + ',' + new DecimalFormat("000.00").format(hundreds);
	    }
	}
	
	public static float convertFloatWithDoublePrecision(float number){
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.format(number);
	    return number;
	}
	
	public static double convertFloatWithDoublePrecision(double number){
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		df.format(number);
	    return number;
	}
	
	
	 public static Double findExchangeRateAndConvert(String from, String to, double amount) {
        try {
            URL url = new URL("http://finance.yahoo.com/d/quotes.csv?f=l1&s="+ from + to + "=X");
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();
            if (line.length() > 0) {
                return Double.parseDouble(line) * amount;
            }
            reader.close();
        } catch (Exception e) {
        }
        return null;
    }
	 
	 public static long convertDatesToDaysCount(Date startdate, Date enddate){
			long noofdays = 0;
			if(startdate != null && enddate != null){
				noofdays =  (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24) + 1;
			}else{
				noofdays =  0;
			}
			return noofdays;
		}

	public static String encrypt( String salt, String plainText) throws Exception {
		Base64 base64 = new Base64();
		String Concatpassword = salt + plainText;
        String encoded = new String(base64.encode(Concatpassword.getBytes()));
        return encoded;
	}
	 
	public static String decrypt(String encryptedText) throws Exception {
		Base64 decoder = new Base64();
		String decoded = new String(decoder.decode(encryptedText.getBytes()));
		return decoded;
	}
	
	public static String getEnvironment(HttpServletRequest request) throws NamingException{
		return request.getServletContext().getInitParameter(CommonConstants.ENV);
	}
	
	 public static String encryptText(String seed,String password) throws Exception{
		 StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		 encryptor.setPassword(seed);
		 String encrypted= encryptor.encrypt(password);
        return encrypted;
     }
    public static String decryptText(String seed,String password) throws Exception {
    	StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    	encryptor.setPassword(seed);
    	String decrypted = encryptor.decrypt(password);
        return decrypted;
    }
    
    public static void persistRecentAcvtivity(Long empId, String desc){
    	try {
	    	RecentActivityDO recentActivity = new RecentActivityDO();
	    	recentActivity.setEmpId(empId);
	    	recentActivity.setDescripetion(desc);
	    	recentActivity.setUpdatedBy(empId.toString());
	    	recentActivity.setUpdatedon(new Date());
			new RecentActivityService().persist(recentActivity);
		} catch (AppException e) {
			e.printStackTrace();
		}
    }
    
	public static void sendMailToReportingHeads(HttpServletRequest request, JSONObject inputJSON, String pageUrl, String mailType) throws Exception{
    	try {
	    	String userName = null;
	    	String toEmails = null;
	    	//String[] CCList = new String[ 100 ];
	    	//List<String> ccEmailList ;
	    	
	    	ArrayList<String> emailslist=new ArrayList<String>();  
	    	
	    	String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
			String fromEmail = CommonConstants.SENDMAIL;
			
			if(inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString() != null && !inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString().isEmpty()){
				toEmails = inputJSON.get(CommonConstants.REPORTTO_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.REPORTTO_NAME).toString(); 
			}else{
				toEmails = inputJSON.get(CommonConstants.DEPHEAD_EMAIL).toString();
				userName = inputJSON.get(CommonConstants.DEPTMNGR_NAME).toString(); 
			}
			List<UserDO> adminList = new UserService().getSuperAdmins();
			for(int i=0; i< adminList.size(); i++){
				List<EmployeeDO> adminList2 = new EmployeeService().retriveByEmpId(adminList.get(i).getEmpId() );
				if(adminList2 != null && adminList2.size() > 0){
					emailslist.add(adminList2.get(0).getCompanyemail());
				}
			}
			
			List<NotificationSettingsDO> notificationSettingsList = new NotificationSettingsService().retrieve();
		    List<String> ccEmailList = Arrays.asList(notificationSettingsList.get(0).getHrRequest().split(CommonConstants.COMMA));
		    for(String ccEmail : ccEmailList ){
				emailslist.add(ccEmail);
			}
		    
			String URL = CommonUtil.getEnvironment(request);
			URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/"+pageUrl;
			
			String url = URL;  
			String requestBy = "";
			
			if(inputJSON != null && !inputJSON.get(CommonConstants.REQUESTBY).toString().isEmpty()){
				requestBy = inputJSON.get(CommonConstants.REQUESTBY).toString();
			}
			String emailBody = "";
			//String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getEmail()+"/N";
			if(inputJSON != null && !inputJSON.get(CommonConstants.COMMENT).toString().isEmpty()){
				emailBody = inputJSON.get(CommonConstants.COMMENT).toString();
			}
	 		boolean mailStatus = EmailProxyUtil.notificationEmail(request,host,  fromEmail,  toEmails, emailslist, "", emailBody, false, url, mailType, userName, requestBy);
	    	
	    	} catch (AppException e) {
			e.printStackTrace();
		}
    }

	 
    
    
}


