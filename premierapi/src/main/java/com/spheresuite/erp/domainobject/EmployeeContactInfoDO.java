package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeecontactinfo")
@TableGenerator(name ="employeecontactinfo", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeecontactinfo.findAll", query = "SELECT r FROM EmployeeContactInfoDO r"),
    @NamedQuery(name = "employeecontactinfo.findById", query = "SELECT r FROM EmployeeContactInfoDO r where r.id =:id"),
    @NamedQuery(name = "employeecontactinfo.findByEmpId", query = "SELECT r FROM EmployeeContactInfoDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeContactInfoDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeecontactinfo.findAll";
	
	public static final String FIND_BY_ID = "employeecontactinfo.findById";
	
	public static final String FIND_BY_EMPID = "employeecontactinfo.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeecontactinfo")
	private Long id;
	private Long homePhone;
	private Long cellPhone;
	private Long workPhone;
	private String personalEmail;
	private String companyEmail;
	private String otherEmail;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getHomePhone() {
		return homePhone;
	}
	public void setHomePhone(Long homePhone) {
		this.homePhone = homePhone;
	}
	public Long getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(Long cellPhone) {
		this.cellPhone = cellPhone;
	}
	public Long getWorkPhone() {
		return workPhone;
	}
	public void setWorkPhone(Long workPhone) {
		this.workPhone = workPhone;
	}
	public String getPersonalEmail() {
		return personalEmail;
	}
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getOtherEmail() {
		return otherEmail;
	}
	public void setOtherEmail(String otherEmail) {
		this.otherEmail = otherEmail;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}