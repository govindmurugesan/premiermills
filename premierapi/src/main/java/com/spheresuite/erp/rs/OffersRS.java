package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.OffersDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.OffersService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.OffersUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/offers")
public class OffersRS {

	String validation = null;
	static Logger logger = Logger.getLogger(OffersRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				OffersDO offersDO = new OffersDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			offersDO.setProjectid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PROJECTID).toString() : null));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			offersDO.setCustomerid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CUSTOMERID).toString() : null));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.REQUIREMENT_ID).toString().isEmpty()){
			 			offersDO.setRequirementid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.REQUIREMENT_ID).toString() : null));
			 		}
			 		
			 		offersDO.setOfferDate(!inputJSON.get(CommonConstants.OFFERDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.OFFERDATE).toString() : null);
			 		offersDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		offersDO.setDesg(!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : null);
			 		offersDO.setJoiningDate(!inputJSON.get(CommonConstants.JOININGDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.JOININGDATE).toString() : null);
			 		
			 		offersDO.setCtc(!inputJSON.get(CommonConstants.COST).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.COST).toString()) : null);
			 		offersDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		offersDO.setPhone(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PHONE).toString()) : null);
			 		offersDO.setAddresss(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
			 		offersDO.setApproverid(!inputJSON.get(CommonConstants.APPROVER_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.APPROVER_ID).toString()) : null);
			 		
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			offersDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			offersDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		offersDO.setStatus('P');
			 		offersDO.setUpdatedon(new Date());
			 	}
				new OffersService().persist(offersDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Offer Created");
				if(offersDO.getApproverid() != null){
					List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(offersDO.getApproverid());
					if(empList != null && empList.size() > 0){
						String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
						String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
						String URL = CommonUtil.getEnvironment(request);
						String toEmails = empList.get(0).getCompanyemail();
						URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/offer/edit/";
						String url = URL+empList.get(0).getId()+"/"+offersDO.getId();
						String emailBody = "Go to offer : " +URL+empList.get(0).getId()+"/"+offersDO.getId();
						List<EmployeeDO> loginEmpList = new EmployeeService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
						String userName = "";
						String ccEmails = "";
						if(loginEmpList != null && loginEmpList.size() > 0){
							if(loginEmpList.get(0).getMiddlename() != null) userName = String.valueOf(loginEmpList.get(0).getFirstname() + " " +loginEmpList.get(0).getMiddlename() + " " + loginEmpList.get(0).getLastname());
							else userName = String.valueOf(loginEmpList.get(0).getFirstname() + " " + loginEmpList.get(0).getLastname());
						}
						if(empList.get(0).getMiddlename() != null) ccEmails = String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname());
						else ccEmails = String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname());
				 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, ccEmails, "", emailBody, false, url, "inviteUser", userName); 
						if(mailStatus){
							return CommonWebUtil.buildSuccessResponse().toString();
						}else{
							return CommonWebUtil.buildErrorResponse("").toString();
						}
					}else{
					}
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<OffersDO> offersList = new OffersService().retrieve();
				respJSON = OffersUtil.getOffersList(offersList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/retrieveByStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					List<OffersDO> offersList = new OffersService().retrieveByStatus((char)inputJSON.get(CommonConstants.STATUS));
					respJSON = OffersUtil.getOffersList(offersList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByProjectId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProjectId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
					List<OffersDO> offersList = new OffersService().retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
					respJSON = OffersUtil.getOffersList(offersList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<OffersDO> offersList = new OffersService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = OffersUtil.getOffersList(offersList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				OffersDO offersDO = new OffersDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<OffersDO> offersList = new OffersService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(offersList != null && offersList.size() > 0){
			 				offersDO = offersList.get(0);

			 				if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
					 			offersDO.setProjectid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PROJECTID).toString() : null));
					 		}
					 		
					 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
					 			offersDO.setCustomerid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CUSTOMERID).toString() : null));
					 		}
					 		
					 		if(!inputJSON.get(CommonConstants.REQUIREMENT_ID).toString().isEmpty()){
					 			offersDO.setRequirementid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.REQUIREMENT_ID).toString() : null));
					 		}
					 		
					 		offersDO.setOfferDate(!inputJSON.get(CommonConstants.OFFERDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.OFFERDATE).toString() : null);
					 		offersDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					 		offersDO.setDesg(!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : null);
					 		offersDO.setJoiningDate(!inputJSON.get(CommonConstants.JOININGDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.JOININGDATE).toString() : null);
					 		
					 		offersDO.setCtc(!inputJSON.get(CommonConstants.COST).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.COST).toString()) : null);
					 		offersDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
					 		offersDO.setPhone(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PHONE).toString()) : null);
					 		offersDO.setAddresss(!inputJSON.get(CommonConstants.ADDRESS).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS).toString() : null);
					 		offersDO.setApproverid(!inputJSON.get(CommonConstants.APPROVER_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.APPROVER_ID).toString()) : null);
					 		
					 		
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			offersDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			offersDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		offersDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
					 		new OffersService().update(offersDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Offer Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
