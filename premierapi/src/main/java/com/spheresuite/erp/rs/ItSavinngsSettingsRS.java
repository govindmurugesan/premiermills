package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ItSavingsSettingsDO;
import com.spheresuite.erp.service.ItSavingsSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSavingsSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itsavingssettings")
public class ItSavinngsSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSavinngsSettingsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsSettingsDO allowanceTypeDO = new ItSavingsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		allowanceTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		allowanceTypeDO.setFromYear(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null);
			 		allowanceTypeDO.setToYear(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null);
			 		allowanceTypeDO.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : null);
			 		allowanceTypeDO.setMaxLimit(!inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()) : null);
			 		allowanceTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			allowanceTypeDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			allowanceTypeDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		allowanceTypeDO.setStatus('a');
			 	}
				new ItSavingsSettingsService().persist(allowanceTypeDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsSettingsDO> itSavingsSettingsList = new ItSavingsSettingsService().retrieveActive();
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsSettingsDO> itSavingsSettingsList = new ItSavingsSettingsService().retrieve();
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByDate/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieve(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSavingsSettingsDO> itSavingsSettingsList = new ItSavingsSettingsService().retrieveByDate(inputJSON.get(CommonConstants.FROMDATE).toString(),inputJSON.get(CommonConstants.TODATE).toString());
				respJSON = ItSavingsSettingsUtil.getITSavingsSettingsList(itSavingsSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsSettingsDO allowanceTypeDO = new ItSavingsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSavingsSettingsDO> itSavingsSettingsList = new ItSavingsSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		allowanceTypeDO = itSavingsSettingsList.get(0);
			 		allowanceTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		allowanceTypeDO.setFromYear(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null);
			 		allowanceTypeDO.setToYear(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null);
			 		allowanceTypeDO.setSection(!inputJSON.get(CommonConstants.SECTION).toString().isEmpty() ? inputJSON.get(CommonConstants.SECTION).toString() : null);
			 		allowanceTypeDO.setMaxLimit(!inputJSON.get(CommonConstants.MAXLIMIT).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MAXLIMIT).toString()) : null);
			 		allowanceTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			allowanceTypeDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		allowanceTypeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new ItSavingsSettingsService().update(allowanceTypeDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
