package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeAddressDAO;
import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeAddressService {
	static Logger logger = Logger.getLogger(EmployeeAddressService.class.getName());
	
	public EmployeeAddressDO persist(EmployeeAddressDO employeeAddressDO) throws AppException {
		return new EmployeeAddressDAO().persist(employeeAddressDO);
	}

	public List<EmployeeAddressDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeAddressDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeAddressDO> retrieveById(Long id) throws AppException {
		return new EmployeeAddressDAO().retrieveById(id);
	}
	
	public EmployeeAddressDO update(EmployeeAddressDO employeeAddressDO) throws AppException {
		return new EmployeeAddressDAO().update(employeeAddressDO);
	}
}
