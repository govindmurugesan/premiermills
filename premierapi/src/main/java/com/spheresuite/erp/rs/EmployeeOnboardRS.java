package com.spheresuite.erp.rs;

import io.jsonwebtoken.SignatureException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.auth0.jwt.JWTVerifyException;
import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.service.EmployeeOnboardService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.EmployeeOnboardUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeonboard")
public class EmployeeOnboardRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeOnboardRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		EmployeeOnboardDO employeeOnboardDO = new EmployeeOnboardDO();
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeOnboardDO.setFirstname(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		employeeOnboardDO.setPersonalcontact(!inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()? inputJSON.get(CommonConstants.PERSONALCONTACT).toString() : null);
			 		employeeOnboardDO.setPersonalemail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty()? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeOnboardDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeOnboardDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		employeeOnboardDO.setUpdatedon(new Date());
			 		employeeOnboardDO.setStatus('p');
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "EmployeeOnboard Created");
		 			}else{
		 				return CommonWebUtil.buildErrorResponse("").toString();
		 			}
			 		new EmployeeOnboardService().persist(employeeOnboardDO);
			 		String URL = CommonUtil.getEnvironment(request);
					URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/employee/onboardform";
					String url = URL+"/"+employeeOnboardDO.getId();
					String emailBody = "Cick Here to Onboard : " +url;
					String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
					String fromEmail = CommonConstants.SENDMAIL;
					String toEmails = employeeOnboardDO.getPersonalemail();
			 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "onboardEmployee", employeeOnboardDO.getFirstname()); 
					if(mailStatus){
						return CommonWebUtil.buildSuccessResponseId(employeeOnboardDO.getId()).toString();
					}else{
						return CommonWebUtil.buildErrorResponse("").toString();
					}				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}		
	}
	
	
	@RequestMapping(value = "/retrieveByOnboardId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByOnboardId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeOnboardDO> employeeOnboardList = new EmployeeOnboardService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeOnboardUtil.getEmployeeList(employeeOnboardList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeOnboardDO employeeOnboardDO = new EmployeeOnboardDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 				List<EmployeeOnboardDO> employeeOnboardList = new EmployeeOnboardService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 		if(employeeOnboardList != null && employeeOnboardList.size() > 0){
				 			employeeOnboardDO = employeeOnboardList.get(0);
					 		employeeOnboardDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : null);
					 		employeeOnboardDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : null);
					 		employeeOnboardDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : null);
					 		employeeOnboardDO.setPersonalcontact(!inputJSON.get(CommonConstants.PERSONALCONTACT).toString().isEmpty()? inputJSON.get(CommonConstants.PERSONALCONTACT).toString() : null);
					 		employeeOnboardDO.setPrimarycontact(!inputJSON.get(CommonConstants.PRIMARYCONTACT).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYCONTACT).toString() : null);
					 		employeeOnboardDO.setPersonalemail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
					 		employeeOnboardDO.setAadhar(!inputJSON.get(CommonConstants.AADHAR).toString().isEmpty() ? inputJSON.get(CommonConstants.AADHAR).toString() : null);
					 		employeeOnboardDO.setPanno(!inputJSON.get(CommonConstants.PANNO).toString().isEmpty() ? inputJSON.get(CommonConstants.PANNO).toString() : null);
					 		employeeOnboardDO.setDateofbirth(!inputJSON.get(CommonConstants.DATEOFBIRTH).toString().isEmpty() ? inputJSON.get(CommonConstants.DATEOFBIRTH).toString() : null);
					 		employeeOnboardDO.setGender((char) (inputJSON != null ? inputJSON.get(CommonConstants.GENDER).toString().charAt(0) : ""));
					 		employeeOnboardDO.setReason((inputJSON != null ? inputJSON.get(CommonConstants.REASON).toString() : ""));
					 		employeeOnboardDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeeOnboardDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		employeeOnboardDO.setUpdatedon(new Date());
					 		new EmployeeOnboardService().update(employeeOnboardDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee OnBoard Updated");
				 		}
		 		}else{
		 			return CommonWebUtil.buildErrorResponse(CommonConstants.COMPANYEMAIL_FOUND).toString();
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeOnboardDO> employeeOnboardList = new EmployeeOnboardService().retrieve();
				respJSON = EmployeeOnboardUtil.getEmployeeList(employeeOnboardList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
