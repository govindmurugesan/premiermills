package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeadDAO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.exception.AppException;

public class LeadService {
	static Logger logger = Logger.getLogger(LeadService.class.getName());
	
	public LeadDO persist(LeadDO leadDO) throws AppException {
		return new LeadDAO().persist(leadDO);
	}

	public List<LeadDO> retrieveById(Long Id) throws AppException {
		return new LeadDAO().retrieveById(Id);
	}

	public List<LeadDO> retrieveBetweenDate(Date fromDate, Date toDate) throws AppException {
		return new LeadDAO().retrieveBetweenDate(fromDate, toDate);
	}
	
	public List<LeadDO> retrieveBetweenDateByEmp(Date fromDate, Date toDate, String empId) throws AppException {
		return new LeadDAO().retrieveBetweenDateByEmp(fromDate, toDate, empId);
	}
	
	public List<LeadDO> retrieve(String type, List<Long> id) throws AppException {
		return new LeadDAO().retrieve(type, id);
	}
	
	public boolean update(List<LeadDO> leadList) throws AppException {
		return new LeadDAO().update(leadList);
	}
	
	public List<LeadDO> retrieveAll(String type) throws AppException {
		return new LeadDAO().retrieveAll(type);
	}
	
	public List<LeadDO> retrieve(List<Long> id) throws AppException {
		return new LeadDAO().retrieve(id);
	}
	
	public List<LeadDO> retrieveAll() throws AppException {
		return new LeadDAO().retrieveAll();
	}
	
	public LeadDO update(LeadDO leadDO) throws AppException {
		return new LeadDAO().update(leadDO);
	}
	
	public List<LeadDO> retrieveCustomerBetweenDate(Date fromDate, Date toDate) throws AppException {
		return new LeadDAO().retrieveCustomerBetweenDate(fromDate, toDate);
	}
	
	public List<LeadDO> retrieveCustomerBetweenDateByEmp(Date fromDate, Date toDate, String empId) throws AppException {
		return new LeadDAO().retrieveCustomerBetweenDateByEmp(fromDate, toDate, empId);
	}
	

	public boolean persistList(List<LeadDO> leadDO) throws AppException {
		return new LeadDAO().persistList(leadDO);
	}

	public List<LeadDO> retrieveLeadByEmp(Long empId, String type) throws AppException {
		return new LeadDAO().retrieveLeadByEmp(empId, type);
	}
	
	public List<LeadDO> retrieveLeadByTransferType(String transferType, String type) throws AppException {
		return new LeadDAO().retrieveLeadByTransferType(transferType, type);
	}
	
	public List<LeadDO> retrieveLeadByAllTransferType(String type) throws AppException {
		return new LeadDAO().retrieveLeadByAllTransferType(type);
	}
	
	public List<LeadDO> retrieveTransferLeadById(String transferType, String type, Long empID) throws AppException {
		return new LeadDAO().retrieveTransferLeadById(transferType, type, empID);
	}
	
	public List<LeadDO> retrieveByEmp(String type, Long empId) throws AppException {
		return new LeadDAO().retrieveByEmp(type, empId);
	}

}
