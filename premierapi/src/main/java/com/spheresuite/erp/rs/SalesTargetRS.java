package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.SalesTargetService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.SalesTargetUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/salestarget")
public class SalesTargetRS {

	String validation = null;
	static Logger logger = Logger.getLogger(SalesTargetRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				SalesTargetDO salesTargetDO = new SalesTargetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
			 			salesTargetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
			 			salesTargetDO.setFromDate(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
			 			salesTargetDO.setToDate(Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.Q1).toString().isEmpty()){
			 			salesTargetDO.setQ1(Double.parseDouble(inputJSON.get(CommonConstants.Q1).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.Q2).toString().isEmpty()){
			 			salesTargetDO.setQ2(Double.parseDouble(inputJSON.get(CommonConstants.Q2).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.Q3).toString().isEmpty()){
			 			salesTargetDO.setQ3(Double.parseDouble(inputJSON.get(CommonConstants.Q3).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.Q4).toString().isEmpty()){
			 			salesTargetDO.setQ4(Double.parseDouble(inputJSON.get(CommonConstants.Q4).toString()));
			 		}
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			salesTargetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			salesTargetDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		salesTargetDO.setUpdatedon(new Date());
			 		new SalesTargetService().persist(salesTargetDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Taraget Created");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<SalesTargetDO> salesTargetList = new SalesTargetService().retrieveByYear(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()), Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByYear/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByYear(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() && !inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					List<SalesTargetDO> salesTargetList = new SalesTargetService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
				}
				if(i==1){
					List<SalesTargetDO> salesTargetList = new SalesTargetService().retrieveAll();
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}else{
					List<SalesTargetDO> salesTargetList = new SalesTargetService().retrieve(empIds);
					respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
				}
				System.out.println("respJson"+respJSON);
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				SalesTargetDO salesTargetDO = new SalesTargetDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<SalesTargetDO> salesTargetList = new SalesTargetService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(salesTargetList != null && salesTargetList.size() > 0){
			 			salesTargetDO = salesTargetList.get(0);
				 		
			 			if(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
				 			salesTargetDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
				 		}
				 		
				 		if(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty()){
				 			salesTargetDO.setFromDate(Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString()));
				 		}
				 		
				 		if(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
				 			salesTargetDO.setToDate(Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString()));
				 		}
				 		
				 		if(!inputJSON.get(CommonConstants.Q1).toString().isEmpty()){
				 			salesTargetDO.setQ1(Double.parseDouble(inputJSON.get(CommonConstants.Q1).toString()));
				 		}
				 		if(!inputJSON.get(CommonConstants.Q2).toString().isEmpty()){
				 			salesTargetDO.setQ2(Double.parseDouble(inputJSON.get(CommonConstants.Q2).toString()));
				 		}
				 		if(!inputJSON.get(CommonConstants.Q3).toString().isEmpty()){
				 			salesTargetDO.setQ3(Double.parseDouble(inputJSON.get(CommonConstants.Q3).toString()));
				 		}
				 		if(!inputJSON.get(CommonConstants.Q4).toString().isEmpty()){
				 			salesTargetDO.setQ4(Double.parseDouble(inputJSON.get(CommonConstants.Q4).toString()));
				 		}
				 		
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			salesTargetDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		salesTargetDO.setUpdatedon(new Date());
				 		new SalesTargetService().update(salesTargetDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Target Updated");
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					Long fromDate = Long.parseLong(inputJSON.get(CommonConstants.FROMDATE).toString());
					Long toDate = Long.parseLong(inputJSON.get(CommonConstants.TODATE).toString());
					Long empId = null;
					if(inputJSON.get(CommonConstants.EMPID).toString() != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
						empId = Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString());
					}
					if(empId != null){
						List<SalesTargetDO> salesTargetList = new SalesTargetService().retrieveByYearForEmp(fromDate, toDate,empId);
						respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
					}else{
						List<SalesTargetDO> salesTargetList = new SalesTargetService().retrieveByYear(fromDate, toDate);
						respJSON = SalesTargetUtil.getSalesTargetList(salesTargetList).toString();
					}
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
