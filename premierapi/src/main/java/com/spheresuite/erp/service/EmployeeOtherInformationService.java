package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeOtherInformationDAO;
import com.spheresuite.erp.domainobject.EmployeeOtherInformationDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeOtherInformationService {
	static Logger logger = Logger.getLogger(EmployeeOtherInformationService.class.getName());
	
	public EmployeeOtherInformationDO persist(EmployeeOtherInformationDO employeeOtherInformationDO) throws AppException {
		return new EmployeeOtherInformationDAO().persist(employeeOtherInformationDO);
	}

	public List<EmployeeOtherInformationDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeOtherInformationDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeOtherInformationDO> retrieveById(Long id) throws AppException {
		return new EmployeeOtherInformationDAO().retrieveById(id);
	}
	
	public EmployeeOtherInformationDO update(EmployeeOtherInformationDO employeeOtherInformationDO) throws AppException {
		return new EmployeeOtherInformationDAO().update(employeeOtherInformationDO);
	}
}
