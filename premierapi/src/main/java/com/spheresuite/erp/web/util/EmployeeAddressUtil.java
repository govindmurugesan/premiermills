package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.AddressTypeDO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeAddressDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.AddressTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeAddressUtil {
	
	private EmployeeAddressUtil() {}
	
	public static JSONObject getEmployeeAddressList(List<EmployeeAddressDO> employeeAddressList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeAddressDO employeeAddressDO : employeeAddressList) {
				resultJSONArray.put(getEmployeeAddressDetailObject(employeeAddressDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeAddressDetailObject(EmployeeAddressDO employeeAddressDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeAddressDO.getId()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(employeeAddressDO.getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(employeeAddressDO.getStateId()));
		CountryDAO obj = new CountryDAO();
		CountryDO workLocationdo = obj.retrieveByID(employeeAddressDO.getCountryId());
		if(workLocationdo != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(workLocationdo.getName()));
		}
		
		StateService stateObj = new StateService();
		List<StateDO> stateDO = stateObj.retrieveById(employeeAddressDO.getStateId());
		if(stateDO != null && stateDO.size() > 0){
			result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
		}
		result.put(CommonConstants.CITY, String.valueOf(employeeAddressDO.getCity()));
		
		if(employeeAddressDO.getAddressTypelId() != null){
			result.put(CommonConstants.ADDRESSTYPEID, String.valueOf(employeeAddressDO.getAddressTypelId()));
			List<AddressTypeDO> addressTypeList = new AddressTypeService().retrieveById(employeeAddressDO.getAddressTypelId());
			if(addressTypeList != null && addressTypeList.size() > 0){
				result.put(CommonConstants.ADDRESSTYPENAME, String.valueOf(addressTypeList.get(0).getName()));
			}else{
				result.put(CommonConstants.ADDRESSTYPENAME, "");
			}
		}else{
			result.put(CommonConstants.ADDRESSTYPEID, "");
			
			result.put(CommonConstants.ADDRESSTYPENAME, "");
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeAddressDO.getUpdatedon())));
		if(employeeAddressDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employeeAddressDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.ADDRESS1, String.valueOf(employeeAddressDO.getAddress1()));
		result.put(CommonConstants.ADDRESS2, String.valueOf(employeeAddressDO.getAddress2()!=null?employeeAddressDO.getAddress2():""));
		result.put(CommonConstants.NOTE, String.valueOf(employeeAddressDO.getNotes()!=null?employeeAddressDO.getNotes():""));
		if(employeeAddressDO.getZip() != null){
			result.put(CommonConstants.ZIP, String.valueOf(employeeAddressDO.getZip()));
		}else{
			result.put(CommonConstants.ZIP, "");
		}
		
		return result;
	}
}
