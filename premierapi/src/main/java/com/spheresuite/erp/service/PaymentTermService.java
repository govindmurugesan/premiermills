package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PaymentTermDAO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.exception.AppException;

public class PaymentTermService {
	static Logger logger = Logger.getLogger(PaymentTermService.class.getName());
	
	public PaymentTermDO persist(PaymentTermDO PaymentTermDO) throws AppException {
		return new PaymentTermDAO().persist(PaymentTermDO);
	}

	public List<PaymentTermDO> retrieveActive() throws AppException {
		return new PaymentTermDAO().retrieveActive();
	}
	
	public List<PaymentTermDO> retrieveById(Long Id) throws AppException {
		return new PaymentTermDAO().retrieveById(Id);
	}
	
	public List<PaymentTermDO> retrieve() throws AppException {
		return new PaymentTermDAO().retrieve();
	}
	
	public PaymentTermDO update(PaymentTermDO PaymentTermDO) throws AppException {
		return new PaymentTermDAO().update(PaymentTermDO);
	}
}
