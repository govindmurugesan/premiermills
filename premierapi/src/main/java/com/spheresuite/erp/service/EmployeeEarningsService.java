package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeEarningsDAO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeEarningsService {
	static Logger logger = Logger.getLogger(EmployeeEarningsService.class.getName());
	
	public List<EmployeeEarningsDO> persist(List<EmployeeEarningsDO> employeeEarningsDO) throws AppException {
		return new EmployeeEarningsDAO().persist(employeeEarningsDO);
	}

	public List<EmployeeEarningsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new EmployeeEarningsDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<EmployeeEarningsDO> retrieve() throws AppException {
		return new EmployeeEarningsDAO().retrieve();
	}
	
	public EmployeeEarningsDO update(EmployeeEarningsDO employeeEarningsDO) throws AppException {
		return new EmployeeEarningsDAO().update(employeeEarningsDO);
	}
}
