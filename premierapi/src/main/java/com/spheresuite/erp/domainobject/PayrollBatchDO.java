package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="payrollbatch")
@TableGenerator(name ="batch", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "batch.findAll", query = "SELECT r FROM PayrollBatchDO r"),
    @NamedQuery(name = "batch.findById", query = "SELECT r FROM PayrollBatchDO r where r.id =:id"),
    @NamedQuery(name = "batch.findByStatus", query = "SELECT r FROM PayrollBatchDO r where r.status =:status"),
})
public class PayrollBatchDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "batch.findAll";
	
	public static final String FIND_BY_ID = "batch.findById";
	
	public static final String FIND_BY_STATUS = "batch.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "batch")
	private Long id;
	private String name;
	private String displayname;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayname() {
		return displayname;
	}

	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}