package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeTaxDAO {
	static Logger logger = Logger.getLogger(EmployeeTaxDAO.class.getName());
	private EntityManager em = null;
	
	public List<EmployeeTaxDO> persist(List<EmployeeTaxDO> employeetaxDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeetaxDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (EmployeeTaxDO employeetaxDO2 : employeetaxDO) {
					em.persist(employeetaxDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeetaxDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTaxDO> retrieve() throws AppException {
		List<EmployeeTaxDO> employeeDeductionList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeTaxDO.FIND_ALL);
				employeeDeductionList = (List<EmployeeTaxDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDeductionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeTaxDO> retrieveByEmpCompensationId(Long id) throws AppException {
		List<EmployeeTaxDO> employeeDeductionList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeTaxDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeDeductionList = (List<EmployeeTaxDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeDeductionList;
	}
	
	public EmployeeTaxDO update(EmployeeTaxDO employeetaxDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeetaxDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeetaxDO;
	}

}
