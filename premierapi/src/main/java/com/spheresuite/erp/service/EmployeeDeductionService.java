package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeDeductionDAO;
import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeDeductionService {
	static Logger logger = Logger.getLogger(EmployeeDeductionService.class.getName());
	
	public List<EmployeeDeductionDO> persist(List<EmployeeDeductionDO> employeeDeductionDO) throws AppException {
		return new EmployeeDeductionDAO().persist(employeeDeductionDO);
	}

	public List<EmployeeDeductionDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new EmployeeDeductionDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<EmployeeDeductionDO> retrieve() throws AppException {
		return new EmployeeDeductionDAO().retrieve();
	}
	
	public EmployeeDeductionDO update(EmployeeDeductionDO employeeDeductionDO) throws AppException {
		return new EmployeeDeductionDAO().update(employeeDeductionDO);
	}
}
