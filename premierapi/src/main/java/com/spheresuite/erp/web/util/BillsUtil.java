package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.BillsDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;

public class BillsUtil {
	
	private BillsUtil() {}
	
	public static JSONObject getBillsList(List<BillsDO> billsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (BillsDO bills : billsList) {
				resultJSONArray.put(getBillsDetailObject(bills));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getBillsDetailObject(BillsDO bills)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(bills.getId()));
		if(bills.getVendorId() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(bills.getVendorId()));
			List<LeadDO> customerList = new LeadService().retrieveById(bills.getVendorId());
			if(customerList != null && customerList.size() > 0){
				result.put(CommonConstants.NAME, String.valueOf(customerList.get(0).getName()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.NAME, "");
		}
		result.put(CommonConstants.CURRENCYTYPE, String.valueOf(bills.getCurrencyType()));
		result.put(CommonConstants.PAYMENTMODE, String.valueOf(bills.getPaymentMode()));
		result.put(CommonConstants.CHEQUENUMBER, String.valueOf(bills.getChequeNumbers()));
		result.put(CommonConstants.RATE, String.valueOf(bills.getRate()));
		result.put(CommonConstants.BILLNUMBER, String.valueOf(bills.getBillNumber() != null ? bills.getBillNumber(): ""));
		result.put(CommonConstants.BILLDATE, String.valueOf(bills.getBillDate() != null ? bills.getBillDate(): ""));
		result.put(CommonConstants.DUEDATE, String.valueOf(bills.getDueDate() != null ? bills.getDueDate(): ""));
		result.put(CommonConstants.DESC, String.valueOf(bills.getComments() != null ? bills.getComments(): ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(bills.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(bills.getStatus() != null ? bills.getStatus(): ""));
		result.put(CommonConstants.PAIDDATE, String.valueOf(bills.getPaidDate() != null ? bills.getPaidDate(): ""));
		if(bills.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(bills.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(bills.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
