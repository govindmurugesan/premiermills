angular
  .module('spheresuite')
  .service('assignmentService', assignmentService);

assignmentService.$inject = ['constants', 'commonService','$q','$http'];

function assignmentService(constants, commonService,$q,$http) {

  var assignmentService = {
	getActiveUser: getActiveUser,
  getLeadType : getLeadType,
  getOpportunityByCustomerId : getOpportunityByCustomerId,
  getProjectName : getProjectName,
  getPaymentTerms : getPaymentTerms,
  addProject : addProject,
  getAssignment : getAssignment,
  updateAssignment: updateAssignment
  };

  return assignmentService;

  function getActiveUser() {
      return commonService.httpCallGetAll(constants.userRetrieveActive);
  }

  function getLeadType(data){
      if (!data)
        return commonService.httpCallGetAll(constants.configLeadTypeRetrieve);
      else {
        data = { id : data };
        return commonService.httpCallGetById(constants.configLeadTypeRetrieveById, data);
      }
  }

  function getOpportunityByCustomerId(data){

        if(data){
          console.log('data', data);
          data = {id : data};
          return commonService.httpCallGetById(constants.opportunityRetrieveByCustomerId, data);
        }
      }

  function getProjectName(data){
        if(data){
          data = {id : data};
          return commonService.httpCallGetById(constants.projectRetrieveByOpportunityId, data);
        }
      }

  function getPaymentTerms() {
      return commonService.httpCallGetAll(constants.assignmentPaymentRetrieve);
  }
  
  function addProject(data) {
    console.log('data', data);
    return commonService.apiCall(constants.newAddProjectPersist, data);
  }
  
  function updateAssignment(data) {
    console.log('uodate  ', data);
    return commonService.apiCall(constants.newAddProjectUpdate, data);
  }

  function getAssignment(data) {
	  if(!data){
		  return commonService.httpCallGetAll(constants.getAssignmentRetrieve);
	  } else {
		  data = {id: data}
		  return commonService.httpCallGetById(constants.getAssignmentRetrieveById, data);
	  }
      
  }
}
