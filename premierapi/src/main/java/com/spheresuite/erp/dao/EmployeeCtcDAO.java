package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeCtcDAO {
	static Logger logger = Logger.getLogger(EmployeeCtcDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeCtcDO persist(EmployeeCtcDO EmloyeeCompensationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && EmloyeeCompensationDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(EmloyeeCompensationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmloyeeCompensationDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieve() throws AppException {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_ALL);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				empCompensationList = (List<EmployeeCtcDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveActive(Long empId) throws AppException {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				q.setParameter(CommonConstants.EMPID, empId);
				empCompensationList = (List<EmployeeCtcDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveActiveWithDate(Long empId, Date date) throws AppException {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_BY_STATUS_DATE);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.DATE, date);
				empCompensationList = (List<EmployeeCtcDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveById(Long id) throws AppException {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				empCompensationList = (List<EmployeeCtcDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Long> retrieveByBatchId(Long id) throws AppException {
		List<Long> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_BY_BATCH_ID);
				q.setParameter(CommonConstants.ID, id);
				empCompensationList = (List<Long>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeCtcDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeCtcDO> empCompensationList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeCtcDO.FIND_BY_EMP_ID);
				q.setParameter(CommonConstants.EMPID, empId);
				empCompensationList = (List<EmployeeCtcDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return empCompensationList;
	}
	
	public EmployeeCtcDO update(EmployeeCtcDO EmloyeeCompensationDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(EmloyeeCompensationDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return EmloyeeCompensationDO;
	}

}
