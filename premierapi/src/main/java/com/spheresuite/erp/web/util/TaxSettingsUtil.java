package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.TaxSettingsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class TaxSettingsUtil {
	
	private TaxSettingsUtil() {}
	
	public static JSONObject getTaxSettingsList(List<TaxSettingsDO> taxSettingList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxSettingsDO taxSettingDO : taxSettingList) {
				resultJSONArray.put(getTaxSettingsDetailObject(taxSettingDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getTaxSettingsListForAmount(List<TaxSettingsDO> taxSettingList, Double ctc,Double basic)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaxSettingsDO taxSettingDO : taxSettingList) {
				resultJSONArray.put(getTaxSettingsAmount(taxSettingDO, ctc, basic));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getTaxSettingsAmount(TaxSettingsDO taxSettingDO, Double ctc, Double basic)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		Double basicPay = basic;
		Double monthlypay=0D;
		Double ytd=0D;
		if(taxSettingDO.getType() != null && taxSettingDO.getType().equalsIgnoreCase("P")){
			if(taxSettingDO.getBasicgrosspay() != null && taxSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
				if(taxSettingDO.getPercentageamount() != null){
					monthlypay = (basicPay * Double.parseDouble(taxSettingDO.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}else if(taxSettingDO.getBasicgrosspay() != null && taxSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
				if(taxSettingDO.getPercentageamount() != null){
					monthlypay = (grossPay * Double.parseDouble(taxSettingDO.getPercentageamount()))/100;
					ytd = monthlypay*12;
				}
			}
		}else if(taxSettingDO.getType() != null && taxSettingDO.getType().equalsIgnoreCase("F")){
			if(taxSettingDO.getFixedamount() != null){
				ytd = Double.parseDouble(taxSettingDO.getFixedamount());
				monthlypay = ytd/12;
			}
		}
		
		result.put(CommonConstants.MONTHLY, String.valueOf(monthlypay));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		
		return result;
	}

	public static JSONObject getTaxSettingsDetailObject(TaxSettingsDO taxSettingDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(taxSettingDO.getId() != null ? taxSettingDO.getId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(taxSettingDO.getName() != null ? taxSettingDO.getName() : ""));
		result.put(CommonConstants.NOTES, String.valueOf(taxSettingDO.getNotes() != null ? taxSettingDO.getNotes() : ""));
		result.put(CommonConstants.PREPOST_TAX, String.valueOf(taxSettingDO.getPreposttax() != null ? taxSettingDO.getPreposttax() : ""));
		result.put(CommonConstants.TYPE, String.valueOf(taxSettingDO.getType() != null ? taxSettingDO.getType() : ""));
		result.put(CommonConstants.SECTION, String.valueOf(taxSettingDO.getSection() != null ? taxSettingDO.getSection() : ""));
		result.put(CommonConstants.FIXEDAMOUNT, String.valueOf(taxSettingDO.getFixedamount() != null ? taxSettingDO.getFixedamount() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(taxSettingDO.getPercentageamount() != null ? taxSettingDO.getPercentageamount() : ""));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(taxSettingDO.getBasicgrosspay() != null ? taxSettingDO.getBasicgrosspay() : ""));
		if(taxSettingDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(taxSettingDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		String displayName="";
		if(taxSettingDO.getType().equalsIgnoreCase("P") ){
			String name="";
			if(taxSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
				name = "Gross";
			}else if(taxSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
				name = "Basic";
			}
			displayName = taxSettingDO.getName() +"-"+ taxSettingDO.getPercentageamount()+"% Of "+name;
		}else{
			displayName = taxSettingDO.getName() +" Rs "+ taxSettingDO.getFixedamount();
		}
		
		result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));
		result.put(CommonConstants.DISPLAYNAME, String.valueOf(taxSettingDO.getDisplayname() != null ? taxSettingDO.getDisplayname() : ""));
		result.put(CommonConstants.STARTDATE,String.valueOf(taxSettingDO.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(taxSettingDO.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(taxSettingDO.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(taxSettingDO.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(taxSettingDO.getStatus()));
		if(taxSettingDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(taxSettingDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
