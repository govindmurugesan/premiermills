package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeEducationDO;
import com.spheresuite.erp.service.EmployeeEducationService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.EmployeeEducationUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeeducation")
public class EmployeeEducationRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeEducationRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeEducationDO employeeEducationDO = new EmployeeEducationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeEducationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		employeeEducationDO.setCountryId(!inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()) : null);
			 		employeeEducationDO.setStateId(!inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()) : null);
			 		employeeEducationDO.setEducationLevelId(!inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString()) : null);
			 		employeeEducationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeEducationDO.setArea(!inputJSON.get(CommonConstants.AREA).toString().isEmpty() ? inputJSON.get(CommonConstants.AREA).toString() : null);
			 		employeeEducationDO.setUniversity(!inputJSON.get(CommonConstants.UNIVERSITY).toString().isEmpty() ? inputJSON.get(CommonConstants.UNIVERSITY).toString() : null);
			 		employeeEducationDO.setYearPassed(!inputJSON.get(CommonConstants.YEAR).toString().isEmpty() ? inputJSON.get(CommonConstants.YEAR).toString() : null);
			 		employeeEducationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeEducationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeEducationDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeEducationService().persist(employeeEducationDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeEducationDO> employeeEducationList = new EmployeeEducationService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeEducationUtil.getEmployeeEducationList(employeeEducationList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeEducationDO employeeEducationDO = new EmployeeEducationDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeEducationDO> employeeEducationList = new EmployeeEducationService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeEducationDO = employeeEducationList.get(0);
			 		employeeEducationDO.setCity(!inputJSON.get(CommonConstants.CITY).toString().isEmpty() ? inputJSON.get(CommonConstants.CITY).toString() : null);
			 		employeeEducationDO.setCountryId(!inputJSON.get(CommonConstants.COUNTRY_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.COUNTRY_ID).toString()) : null);
			 		employeeEducationDO.setStateId(!inputJSON.get(CommonConstants.STATE_ID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.STATE_ID).toString()) : null);
			 		employeeEducationDO.setEducationLevelId(!inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EDUCATIONLEVELID).toString()) : null);
			 		employeeEducationDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeEducationDO.setArea(!inputJSON.get(CommonConstants.AREA).toString().isEmpty() ? inputJSON.get(CommonConstants.AREA).toString() : null);
			 		employeeEducationDO.setUniversity(!inputJSON.get(CommonConstants.UNIVERSITY).toString().isEmpty() ? inputJSON.get(CommonConstants.UNIVERSITY).toString() : null);
			 		employeeEducationDO.setYearPassed(!inputJSON.get(CommonConstants.YEAR).toString().isEmpty() ? inputJSON.get(CommonConstants.YEAR).toString() : null);
			 		employeeEducationDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeEducationDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeEducationService().update(employeeEducationDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
