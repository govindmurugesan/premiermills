package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="hrrequests")
@TableGenerator(name ="hrrequests", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "hrrequests.findAll", query = "SELECT r FROM HrRequestsDO r"),
    @NamedQuery(name = "hrrequests.findById", query = "SELECT r FROM HrRequestsDO r where r.id =:id"),
    @NamedQuery(name = "hrrequests.findByEmpId", query = "SELECT r FROM HrRequestsDO r where r.empId =:id"),
})
public class HrRequestsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "hrrequests.findAll";
	public static final String FIND_BY_ID = "hrrequests.findById";
	public static final String FINDBY_EMP_ID = "hrrequests.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "hrrequests")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String description;
	private Long requestType;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	private String Comments;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getRequestType() {
		return requestType;
	}
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
}