package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeBankInformationDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeBankInformationUtil {
	
	private EmployeeBankInformationUtil() {}
	
	public static JSONObject getEmployeeBankInfoList(List<EmployeeBankInformationDO> employeeBankInformationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeBankInformationDO employeeBankInformationDO : employeeBankInformationList) {
				resultJSONArray.put(getEmployeeBankInfoDetailObject(employeeBankInformationDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeBankInfoDetailObject(EmployeeBankInformationDO employeeBankInformationDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeBankInformationDO.getId()));
		result.put(CommonConstants.BANKNAME, String.valueOf(employeeBankInformationDO.getBankName()));
		result.put(CommonConstants.IFSCCODE, String.valueOf(employeeBankInformationDO.getIfscCode()));
		result.put(CommonConstants.BRANCHNAME, String.valueOf(employeeBankInformationDO.getBranchName()));
		result.put(CommonConstants.ACCOUNTNAME, String.valueOf(employeeBankInformationDO.getAccountName()));
		result.put(CommonConstants.ACCOUNTNUMBER, String.valueOf(employeeBankInformationDO.getAccountNumber()));
		result.put(CommonConstants.EMPID, String.valueOf(employeeBankInformationDO.getEmpId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employeeBankInformationDO.getUpdatedon())));
		if(employeeBankInformationDO.getUpdatedBy() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employeeBankInformationDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
