package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.StorageTypeDAO;
import com.spheresuite.erp.domainobject.StorageTypeDO;
import com.spheresuite.erp.exception.AppException;

public class StorageTypeService {
	static Logger logger = Logger.getLogger(StorageTypeService.class.getName());
	
	public StorageTypeDO persist(StorageTypeDO storageTypeDO) throws AppException {
		return new StorageTypeDAO().persist(storageTypeDO);
	}

	public List<StorageTypeDO> retrieveActive() throws AppException {
		return new StorageTypeDAO().retrieveActive();
	}
	
	public List<StorageTypeDO> retrieveById(Long id) throws AppException {
		return new StorageTypeDAO().retrieveById(id);
	}
	
	public List<StorageTypeDO> retrieve() throws AppException {
		return new StorageTypeDAO().retrieve();
	}
	
	public StorageTypeDO update(StorageTypeDO storageTypeDO) throws AppException {
		return new StorageTypeDAO().update(storageTypeDO);
	}
}
