package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeCertificationDAO;
import com.spheresuite.erp.domainobject.EmployeeCertificationDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeCertificationService {
	static Logger logger = Logger.getLogger(EmployeeCertificationService.class.getName());
	
	public EmployeeCertificationDO persist(EmployeeCertificationDO employeeCertificationDO) throws AppException {
		return new EmployeeCertificationDAO().persist(employeeCertificationDO);
	}

	public List<EmployeeCertificationDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeCertificationDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeCertificationDO> retrieveById(Long id) throws AppException {
		return new EmployeeCertificationDAO().retrieveById(id);
	}
	
	public EmployeeCertificationDO update(EmployeeCertificationDO employeeCertificationDO) throws AppException {
		return new EmployeeCertificationDAO().update(employeeCertificationDO);
	}
}
