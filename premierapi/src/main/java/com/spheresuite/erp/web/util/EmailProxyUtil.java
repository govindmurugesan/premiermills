package com.spheresuite.erp.web.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;

import com.spheresuite.erp.domainobject.AttachmentsDO;
import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.AttachmentsService;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;

public class EmailProxyUtil {
	public static boolean sendEmail(final HttpServletRequest request, String host, final String fromEmail, String toEmails,
			String ccEmails, String bccEmails, String emailBody, boolean htmlContent, String url, String mailType, String userName) throws AddressException,
			MessagingException, IOException, TemplateException{
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put(CommonConstants.MAIL_SMTP_HOST, host);
		properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		properties.put(CommonConstants.MAIL_SMTP_PORT, request.getServletContext().getInitParameter(CommonConstants.PORT));
		properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		// creates a new session with an authenticator
					Authenticator auth = new Authenticator() {
						
						public PasswordAuthentication getPasswordAuthentication() {
							PasswordAuthentication passwordAuthentication = null;
							try {
								passwordAuthentication = new PasswordAuthentication(CommonConstants.SENDMAIL,CommonConstants.SENDPASSWORD);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return passwordAuthentication;
						}
					};
			
					Session session = Session.getInstance(properties, auth);

		//Session session = Session.getInstance(properties,null);

		// Create Message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(fromEmail));
		
		msg.setSentDate(new Date());
        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(request.getServletContext().getRealPath("resources/templates")));
        freemarker.template.Template template = null;
        if(mailType.equalsIgnoreCase("inviteUser")){
        	msg.setSubject("Welcome to SphereSuite");
        	template = cfg.getTemplate("inviteUser.ftl");
        }
        
        if(mailType.equalsIgnoreCase("onboardEmployee")){
        	msg.setSubject("Welcome to SphereSuite");
        	template = cfg.getTemplate("inviteUser.ftl");
        }
        
        if(mailType.equalsIgnoreCase("resetPassword")) {
        	msg.setSubject("Reset Password");
        	template = cfg.getTemplate("resetPassword.ftl");
        }
        
        if(mailType.equalsIgnoreCase("reInvite")) {
        	msg.setSubject("Welcome to SphereSuite");
        	template = cfg.getTemplate("reInvite.ftl");
        }
        Map<String, String> rootMap = new HashMap<String, String>();
        rootMap.put("link",url);
        rootMap.put("userName",userName);
        
        if(mailType.equalsIgnoreCase("offerApproval")){
        	msg.setSubject("New offer from "+userName+" is pending for approval");
        	template = cfg.getTemplate("offerApproval.ftl");
        	rootMap.put("userName",ccEmails);
        }
        
        Writer out = new StringWriter();
        template.process(rootMap, out);
        messageBodyPart.setContent(out.toString(), "text/html");
       
        // Create a multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
		// To Email List
		List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
		if(toEmailList != null){
			for(String toEmail : toEmailList ){
				if(emailValidation(toEmail.trim())){
					msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
				}
			}
		}
		

		// CC Email List
		/*	List<String> ccEmailList = Arrays.asList(ccEmails.split(CommonConstants.COMMA));
		if(toEmailList != null){
			for(String ccEmail : ccEmailList ){
				if(emailValidation(ccEmail.trim())){
					msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
				}
			}
		}

		//BCC Email List
		List<String> bccEmailList = Arrays.asList(bccEmails.split(CommonConstants.COMMA));
		if(toEmailList != null){
			for(String bccEmail : bccEmailList ){
				if(emailValidation(bccEmail.trim())){
					msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccEmail.trim()));			
				}
			}
		}
		*/

		// sends the e-mail
		Transport.send(msg);
		
		return true;
	}
	
	public static boolean  emailValidation(String email){
		
		String EMAIL_REGEX = CommonConstants.REG_EXP;
		return email.matches(EMAIL_REGEX)? true : false;
	}
	
	@SuppressWarnings("resource")
	public static boolean sendEmails(final HttpServletRequest request, String host, final String fromEmail, String toEmails,
			String ccEmails, String bccEmails, String subject, String emailBody, final String password) throws IOException, NumberFormatException, MessagingException {
		
			// sets SMTP server properties
			Properties properties = new Properties();
			/*properties.put(CommonConstants.MAIL_SMTP_HOST, host);  
			properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);*/  
			properties.put(CommonConstants.MAIL_SMTP_HOST, "smtp.gmail.com");
			properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
			properties.put(CommonConstants.MAIL_SMTP_PORT, request.getServletContext().getInitParameter(CommonConstants.PORT));
			properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
	        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
	        
		   Session session = Session.getDefaultInstance(properties,  
		    new javax.mail.Authenticator() {  
		      protected PasswordAuthentication getPasswordAuthentication() {  
		    return new PasswordAuthentication("skarthikkannan@saptalabs.com","karthik1988");  
		      }  
		    });
		   
			try{
				
				List<AttachmentsDO> attachmentsList = new  AttachmentsService().retrieve();
				// creates a new e-mail message
				Message msg = new MimeMessage(session);
		
				msg.setFrom(new InternetAddress("skarthikkannan@saptalabs.com"));
				
				// To Email List
				List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
				if(toEmailList != null){
					for(String toEmail : toEmailList ){
						if(emailValidation(toEmail.trim())){
							msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
						}
					}
				}
				
				// CC Email List
				List<String> ccEmailList = Arrays.asList(ccEmails.split(CommonConstants.COMMA));
				if(toEmailList != null){
					for(String ccEmail : ccEmailList ){
						if(emailValidation(ccEmail.trim())){
							msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
						}
					}
				}
	
				//BCC Email List
				List<String> bccEmailList = Arrays.asList(bccEmails.split(CommonConstants.COMMA));
				if(toEmailList != null){
					for(String bccEmail : bccEmailList ){
						if(emailValidation(bccEmail.trim())){
							msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccEmail.trim()));			
						}
					}
				}
				
				
				msg.setSubject(subject);
				msg.setSentDate(new Date());
				
				// Create Multipart E-Mail.
				Multipart multipart = new MimeMultipart();
				
				// Set the email msg text.
				/*BodyPart messagePart = new MimeBodyPart();
	            messagePart.setText(emailBody);*/
	            BodyPart messagePart = new MimeBodyPart();
	            messagePart.setText(emailBody);
	            multipart.addBodyPart(messagePart);
				if(attachmentsList.size() > 0){
					for(AttachmentsDO attachments:attachmentsList){
				        // Create the message part
						MimeBodyPart attachmentPart = new MimeBodyPart();
				            
				        // Part two is attachment
				        DataSource source;
				        InputStream in = new ByteArrayInputStream(attachments.getAttachFile().getBytes("UTF-8"));
				        try {
				            in = MimeUtility.decode(in, "base64");
				            System.out.println("mimeType  2 "+attachments.getFiletype());
				            try {
				            	source = new ByteArrayDataSource(in , attachments.getFiletype());
				            } finally {
				                in.close();
				            }
				        } catch (IOException ioe) {
				            throw new MessagingException(attachments.getAttachFile(), ioe);
				        }
		
				        // "Attachment"
				        attachmentPart.setDataHandler(new DataHandler(source));
			            attachmentPart.setFileName(attachments.getFileName());			        
			         // Create Multipart E-Mail.
			            //multipart.addBodyPart(messagePart);
			            multipart.addBodyPart(attachmentPart);
	
					}
					msg.setContent(multipart);
				}
		
		        // Send message
		        Transport.send(msg);
			}catch(Exception ex){
				ex.printStackTrace();
				System.out.println("Exception   " + ex.getMessage());
			}
		return true;
	}
	
	@SuppressWarnings("unused")
	public static List<LeadEmailDO> getAllEmails(JSONObject inputJSON, Message[] arrayMessages) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				String from = fromAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                        byte[] attachment = outStream.toByteArray();
                                if(attachment.length > 1){
                                     leadEmailDO.setAttachmentFile(attachment);
                                }
                                inStream.close();
		                        outStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
			new LeadEmailService().persistList(leadEmailDOlist);
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	
	
	@SuppressWarnings("unused")
	public static List<LeadEmailDO> getEmailBasedOnDate(JSONObject inputJSON, Message[] arrayMessages
			,java.util.Date utilDate,String email) throws ParseException, IOException{
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
		        String dt=message.getSentDate().toString();
		        java.util.Date dDate = CommonUtil.convertEmailDate(dt);
				if (dDate.after(utilDate)){
					Address[] fromAddress = message.getFrom();
					String from = fromAddress[0].toString();
					String subject = message.getSubject();
					String sentDate = message.getSentDate().toString();
					String messageContent = "";
					String attachFiles = "";
					Object content = message.getContent();
					LeadEmailDO leadEmailDO = new LeadEmailDO();
					if (content instanceof String) {
		                messageContent = content.toString();
	            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    		
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                       
		                         byte[] attachment = outStream.toByteArray();
	                                if(attachment.length > 1){
	                                     leadEmailDO.setAttachmentFile(attachment);
	                                }
	                                inStream.close();
			                        outStream.close();
		                         
		                    }
		                }
		            }
					
				 	Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
				
		}
	} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 		return leadEmailDOlist;
}
	
	public static List<LeadEmailDO> getAllInbox(Message[] arrayMessages) throws Exception {
		List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();
		try {
			for (int i = arrayMessages.length-1; i >= 0 ; i--) {
				Message message = arrayMessages[i];
				Address[] fromAddress = message.getFrom();
				String from = fromAddress[0].toString();
				String subject = message.getSubject();
				String sentDate = message.getSentDate().toString();
				String messageContent = "";
				String attachFiles = "";
				Object content = message.getContent();
				LeadEmailDO leadEmailDO = new LeadEmailDO();
				
				 if (content instanceof String) {
		                messageContent = content.toString();
		            } else if (content instanceof Multipart) {
		                Multipart multiPart = (Multipart) content;
		                for (int j = 0; j < multiPart.getCount(); j++) {
		                    MimeBodyPart bodyPart = (MimeBodyPart) multiPart.getBodyPart(j);
		                    Object o = bodyPart.getContent();
		                    String msgContent = getText(multiPart.getBodyPart(j));
	                    	if(msgContent != null ){
	                    		messageContent = msgContent;
	                    	}
		                    
		                    if (o instanceof String) {
		                        messageContent = o.toString();
		                    } else if (null != bodyPart.getDisposition()
		                            && bodyPart.getDisposition().equalsIgnoreCase(
		                                    Part.ATTACHMENT)) {
		                    	
		                        String fileName = bodyPart.getFileName();
		                        leadEmailDO.setAttachFiles(fileName);
		                        InputStream inStream = bodyPart.getInputStream();
		                       /* ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		                        
		                        byte[] tempBuffer = new byte[4096];// 4 KB
		                        int numRead;
		                        while ((numRead = inStream.read(tempBuffer)) != -1) {
		                            outStream.write(tempBuffer);
		                        }
		                        */
		                        byte[] bytes = IOUtils.toByteArray(inStream);
		                        leadEmailDO.setContentType(bodyPart.getContentType());
		                        leadEmailDO.setAttachment(DatatypeConverter.printBase64Binary(bytes));
                                inStream.close();
		                    }
		                }
		            }
				    
				    Date date = null;
			        date = CommonUtil.convertEmailDate(sentDate);
				    String formattedDate = "";
				    if( date != null ) {
				    	formattedDate = CommonUtil.convertEmailDateToDate( date );
				    }
		 			leadEmailDO.setContactId(null);
		 			leadEmailDO.setFromAddress(from);
			 		
			 		leadEmailDO.setSubject(subject);
			 		
			 		leadEmailDO.setMessageContent(messageContent);
			 		if(!attachFiles.isEmpty() && attachFiles.length() > 0){
			 			leadEmailDO.setAttachFiles(attachFiles);
			 		}
			 		
			 		
			 		leadEmailDO.setSentDate(CommonUtil.convertStringToDateTimeFmt(formattedDate));
			 					 		
			 		leadEmailDO.setUpdatedon(new Date());
			 		
			 		leadEmailDOlist.add(leadEmailDO);
			}
			return leadEmailDOlist;
		} catch (NoSuchProviderException ex) {
			ex.printStackTrace();
		} catch (MessagingException ex) {
			ex.printStackTrace();
		} 
		return leadEmailDOlist;
	}
	private static String getText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String)p.getContent();
			return s;
		}

		if (p.isMimeType("multipart/alternative")) {
			Multipart mp = (Multipart)p.getContent();
			String text = null;
			for (int i = 0; i < mp.getCount(); i++) {
			    Part bp = mp.getBodyPart(i);
			    if (bp.isMimeType("text/plain")) {
			        if (text == null)
			            text = getText(bp);
			        continue;
			    } else if (bp.isMimeType("text/html")) {
			        String s = getText(bp);
			        if (s != null)
			            return s;
			    } else {
			        return getText(bp);
			    }
			}
		return text;
		} else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart)p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getText(mp.getBodyPart(i));
				if (s != null)
					return s;
				}
		}

		return null;
	}



	public static boolean notificationEmail(final HttpServletRequest request, String host, final String fromEmail, String toEmails,
			ArrayList<String> ccEmails, String bccEmails, String emailBody, boolean htmlContent, String url, String mailType, String userName, String requestBy) throws AddressException,
			MessagingException, IOException, TemplateException{
		// sets SMTP server properties
		Properties properties = new Properties();
		properties.put(CommonConstants.MAIL_SMTP_HOST, host);
		properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		properties.put(CommonConstants.MAIL_SMTP_PORT, request.getServletContext().getInitParameter(CommonConstants.PORT));
		properties.put(CommonConstants.MAIL_SMTP_AUTH, CommonConstants.TRUE);
        properties.put(CommonConstants.MAIL_SMTP_STARTTLS, CommonConstants.TRUE);
		// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			
			public PasswordAuthentication getPasswordAuthentication() {
				PasswordAuthentication passwordAuthentication = null;
				try {
					passwordAuthentication = new PasswordAuthentication(CommonConstants.SENDMAIL,CommonConstants.SENDPASSWORD);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return passwordAuthentication;
			}
		};

		Session session = Session.getInstance(properties, auth);

		//Session session = Session.getInstance(properties,null);

		// Create Message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(fromEmail));
		
		msg.setSentDate(new Date());
        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        Configuration cfg = new Configuration();
        cfg.setDirectoryForTemplateLoading(new File(request.getServletContext().getRealPath("resources/templates")));
        freemarker.template.Template template = null;
        if(mailType.equalsIgnoreCase("hrRequest")){
        	msg.setSubject("New HR Request created by "+requestBy);
        	template = cfg.getTemplate("hrRequestNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("leaveRequest")){
        	msg.setSubject("New leave request from "+requestBy);
        	template = cfg.getTemplate("leaveRequestNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("hrpolicies")) {
        	msg.setSubject("New HR Policy Created By "+requestBy);
        	template = cfg.getTemplate("hrPoliciesNotification.ftl");
        }
        
        if(mailType.equalsIgnoreCase("holiday")) {
        	msg.setSubject("New Holiday added to calender");
        	template = cfg.getTemplate("holidayNotification.ftl");
        }
        
        Map<String, String> rootMap = new HashMap<String, String>();
        rootMap.put("link",url);
        rootMap.put("userName",userName);
        rootMap.put("comments",emailBody);
        rootMap.put("requestBy",requestBy);
        /*if(mailType.equalsIgnoreCase("offerApproval")){
        	msg.setSubject("New offer from "+userName+" is pending for approval");
        	template = cfg.getTemplate("offerApproval.ftl");
        	rootMap.put("userName",ccEmails);
        }*/
        
        Writer out = new StringWriter();
        template.process(rootMap, out);
        messageBodyPart.setContent(out.toString(), "text/html");
       
        // Create a multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
		// To Email List
		List<String> toEmailList = Arrays.asList(toEmails.split(CommonConstants.COMMA));
		if(toEmailList != null){
			for(String toEmail : toEmailList ){
				if(emailValidation(toEmail.trim())){
					msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail.trim()));			
				}
			}
		}
		
		// CC Email List
			for(String ccEmail : ccEmails ){
				if(ccEmail != null){
					if(emailValidation(ccEmail.trim())){
						msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccEmail.trim()));			
					}
				}
			}

		// sends the e-mail
		Transport.send(msg);
		
		return true;
	}
}
