package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="project")
@TableGenerator(name ="project", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "project.findAll", query = "SELECT r FROM ProjectDO r where r.empId IN :id"),
    @NamedQuery(name = "project.findAllData", query = "SELECT r FROM ProjectDO r"),
    @NamedQuery(name = "project.findById", query = "SELECT r FROM ProjectDO r where r.id =:id"),
    @NamedQuery(name = "project.findByOppId", query = "SELECT r FROM ProjectDO r where r.opportunityId =:id"),
})
public class ProjectDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "project.findAll";
	public static final String FIND_BY_ID = "project.findById";
	public static final String FIND_BY_OPP_ID = "project.findByOppId";
	public static final String FIND_ALL_DATA = "project.findAllData";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "project")
	private Long id;
	private Long opportunityId;
	private String projectname;
	private String displayname;
	private String startdate;
	private String enddate;
	private String projectduration;
	private Long probability;
	private Long amount;
	private String comment;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private Long gstId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOpportunityId() {
		return opportunityId;
	}
	public void setOpportunityId(Long opportunityId) {
		this.opportunityId = opportunityId;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getProjectduration() {
		return projectduration;
	}
	public void setProjectduration(String projectduration) {
		this.projectduration = projectduration;
	}
	public Long getProbability() {
		return probability;
	}
	public void setProbability(Long probability) {
		this.probability = probability;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getGstId() {
		return gstId;
	}
	public void setGstId(Long gstId) {
		this.gstId = gstId;
	}
}