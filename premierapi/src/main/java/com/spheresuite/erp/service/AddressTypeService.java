package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AddressTypeDAO;
import com.spheresuite.erp.domainobject.AddressTypeDO;
import com.spheresuite.erp.exception.AppException;

public class AddressTypeService {
	static Logger logger = Logger.getLogger(AddressTypeService.class.getName());
	
	public AddressTypeDO persist(AddressTypeDO AddressTypeDO) throws AppException {
		return new AddressTypeDAO().persist(AddressTypeDO);
	}

	public List<AddressTypeDO> retrieveActive() throws AppException {
		return new AddressTypeDAO().retrieveActive();
	}
	
	public List<AddressTypeDO> retrieveById(Long id) throws AppException {
		return new AddressTypeDAO().retrieveById(id);
	}
	
	public List<AddressTypeDO> retrieve() throws AppException {
		return new AddressTypeDAO().retrieve();
	}
	
	public AddressTypeDO update(AddressTypeDO AddressTypeDO) throws AppException {
		return new AddressTypeDAO().update(AddressTypeDO);
	}
}
