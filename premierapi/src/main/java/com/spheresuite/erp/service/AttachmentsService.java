package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AttachmentsDAO;
import com.spheresuite.erp.domainobject.AttachmentsDO;
import com.spheresuite.erp.exception.AppException;

public class AttachmentsService {
	static Logger logger = Logger.getLogger(AttachmentsService.class.getName());
	
	public AttachmentsDO persist(AttachmentsDO attachmentsDO) throws AppException {
		return new AttachmentsDAO().persist(attachmentsDO);
	}
	
	public boolean delete() throws AppException {
		return new AttachmentsDAO().delete();
	}
	
	public List<AttachmentsDO> retrieve() throws AppException {
		return new AttachmentsDAO().retrieve();
	}
}
