package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.NotificationSettingsDO;
import com.spheresuite.erp.service.NotificationSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.NotificationSettingsUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/notificationsettings")
public class NotificationSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(NotificationSettingsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotificationSettingsDO notificationSettingsDO = new NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		notificationSettingsDO.setHrRequest(!inputJSON.get(CommonConstants.HRREQUEST).toString().isEmpty() ? inputJSON.get(CommonConstants.HRREQUEST).toString() : "");
			 		notificationSettingsDO.setPayroll(!inputJSON.get(CommonConstants.PAYROLL).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLL).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			notificationSettingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			notificationSettingsDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		notificationSettingsDO.setUpdatedon(new Date());
			 	}
				new NotificationSettingsService().persist(notificationSettingsDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<NotificationSettingsDO> notificationSettingsList = new NotificationSettingsService().retrieve();
				respJSON = NotificationSettingsUtil.getNotificationSettingsList(notificationSettingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				NotificationSettingsDO notificationSettingsDO = new NotificationSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<NotificationSettingsDO> notificationSettingsList = new NotificationSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(notificationSettingsList != null && notificationSettingsList.size() > 0){
			 				notificationSettingsDO = notificationSettingsList.get(0);
			 				notificationSettingsDO.setHrRequest(!inputJSON.get(CommonConstants.HRREQUEST).toString().isEmpty() ? inputJSON.get(CommonConstants.HRREQUEST).toString() : "");
					 		notificationSettingsDO.setPayroll(!inputJSON.get(CommonConstants.PAYROLL).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLL).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			notificationSettingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
					 		notificationSettingsDO.setUpdatedon(new Date());
					 		new NotificationSettingsService().update(notificationSettingsDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
