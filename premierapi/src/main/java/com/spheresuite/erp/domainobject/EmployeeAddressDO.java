package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeaddress")
@TableGenerator(name ="employeeaddress", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeaddress.findAll", query = "SELECT r FROM EmployeeAddressDO r"),
    @NamedQuery(name = "employeeaddress.findById", query = "SELECT r FROM EmployeeAddressDO r where r.id =:id"),
    @NamedQuery(name = "employeeaddress.findByEmpId", query = "SELECT r FROM EmployeeAddressDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeAddressDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeaddress.findAll";
	
	public static final String FIND_BY_ID = "employeeaddress.findById";
	
	public static final String FIND_BY_EMPID = "employeeaddress.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeaddress")
	private Long id;
	private Long addressTypelId;
	private String address1;
	private String address2;
	private Long countryId;
	private Long stateId;
	private String city;
	private String notes;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	private Long zip;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAddressTypelId() {
		return addressTypelId;
	}
	public void setAddressTypelId(Long addressTypelId) {
		this.addressTypelId = addressTypelId;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public Long getCountryId() {
		return countryId;
	}
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	public Long getStateId() {
		return stateId;
	}
	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getZip() {
		return zip;
	}
	public void setZip(Long zip) {
		this.zip = zip;
	}
}