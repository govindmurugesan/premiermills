package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.WeeklyTimesheetDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class WeeklyTimesheetDAO {
	static Logger logger = Logger.getLogger(WeeklyTimesheetDAO.class.getName());
	private EntityManager em = null;
	
	public WeeklyTimesheetDO persist(WeeklyTimesheetDO weeklyTimesheetDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && weeklyTimesheetDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(weeklyTimesheetDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieve() throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_ALL);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveByDept(List<Long> ids) throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_ALL_BY_DEPT);
				q.setParameter(CommonConstants.ID, ids);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveSubmittedTimesheet() throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_SUBMITTED);
				q.setParameter(CommonConstants.STATUS, CommonConstants.SUBMITTED);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveById(Long Id) throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveByDate(String date, long empid) throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_BY_DATE);
				q.setParameter(CommonConstants.WEEKENDDATE, date);
				q.setParameter(CommonConstants.EMPID, empid);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<WeeklyTimesheetDO> retrieveByEmpId(long empid) throws AppException {
		List<WeeklyTimesheetDO> weeklyTimesheetList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(WeeklyTimesheetDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empid);
				weeklyTimesheetList = (List<WeeklyTimesheetDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetList;
	}
	
	public WeeklyTimesheetDO update(WeeklyTimesheetDO weeklyTimesheetDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(weeklyTimesheetDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return weeklyTimesheetDO;
	}

}
