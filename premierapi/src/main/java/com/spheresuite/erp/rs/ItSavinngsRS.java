package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.domainobject.ItSavingsDocDO;
import com.spheresuite.erp.service.ItSavingDocService;
import com.spheresuite.erp.service.ItSavingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ItSavingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/itsavings")
public class ItSavinngsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ItSavinngsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		ItSavingsDO itSavingsDO = new ItSavingsDO();
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		itSavingsDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		itSavingsDO.setItSavingsSettingId(!inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString()) : null);
			 		itSavingsDO.setFromYear(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null);
			 		itSavingsDO.setToYear(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null);
			 		itSavingsDO.setAmount(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()) : null);
			 		itSavingsDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		itSavingsDO.setUpdatedon(new Date());
			 		itSavingsDO.setStatus('s');
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			itSavingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			itSavingsDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new ItSavingsService().persist(itSavingsDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Itsaving  Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(itSavingsDO.getId()).toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				List<ItSavingsDO> itSavingsList = new ItSavingsService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = ItSavingsUtil.getITSavingsSettingsList(itSavingsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<ItSavingsDO> itSavingsList = new ItSavingsService().retrieve();
				respJSON = ItSavingsUtil.getITSavingsSettingsList(itSavingsList).toString(); 
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsDO itSavingsDO = new ItSavingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ItSavingsDO> itSavingsList = new ItSavingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		itSavingsDO = itSavingsList.get(0);
			 		itSavingsDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		itSavingsDO.setItSavingsSettingId(!inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.ITSAVINGSETTINGID).toString()) : null);
			 		itSavingsDO.setFromYear(!inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMDATE).toString() : null);
			 		itSavingsDO.setToYear(!inputJSON.get(CommonConstants.TODATE).toString().isEmpty() ? inputJSON.get(CommonConstants.TODATE).toString() : null);
			 		itSavingsDO.setAmount(!inputJSON.get(CommonConstants.AMOUNT).toString().isEmpty() ? Double.parseDouble(inputJSON.get(CommonConstants.AMOUNT).toString()) : null);
			 		itSavingsDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		itSavingsDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new ItSavingsService().update(itSavingsDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Itsaving Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ItSavingsDocDO itsavingdoc = new ItSavingsDocDO();
				/*JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("data").toString());*/
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		itsavingdoc.setItsavingId(Long.parseLong(request.getParameter("id")));
			 		itsavingdoc.setPhoto(request.getParameter("file"));
			 		itsavingdoc.setFileName(request.getParameter("name"));
			 		new ItSavingDocService().persist(itsavingdoc);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
