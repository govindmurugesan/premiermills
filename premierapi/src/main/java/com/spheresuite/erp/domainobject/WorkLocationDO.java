package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="worklocation")
@TableGenerator(name ="worklocation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "worklocation.findAll", query = "SELECT r FROM WorkLocationDO r"),
    @NamedQuery(name = "worklocation.findById", query = "SELECT r FROM WorkLocationDO r where r.id =:id"),
    @NamedQuery(name = "worklocation.findByStatus", query = "SELECT r FROM WorkLocationDO r where r.status =:status"),
})
public class WorkLocationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "worklocation.findAll";
	
	public static final String FIND_BY_ID = "worklocation.findById";
	
	public static final String FIND_BY_STATUS = "worklocation.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "worklocation")
	private Long id;
	private String city;
	private Long stateId;
	private Long countryId;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}