package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="chequeManagement")
@TableGenerator(name ="chequeManagement", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "chequeManagement.findAll", query = "SELECT r FROM ChequeManagementDO r"),
    @NamedQuery(name = "chequeManagement.findById", query = "SELECT r FROM ChequeManagementDO r where r.id =:id"),
    @NamedQuery(name = "chequeManagement.findActive", query = "SELECT r FROM ChequeManagementDO r where r.status =:status"),
})
public class ChequeManagementDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "chequeManagement.findAll";
	
	public static final String FIND_BY_ID = "chequeManagement.findById";
	
	public static final String FIND_ACTIVE = "chequeManagement.findActive";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "chequeManagement")
	private Long id;
	private Long chequeNumber;
	private String chequeDate;
	private String chequeAmount;
	private String bankName;
	private String payTo;
	private String updatedby;
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(Long chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(String chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getPayTo() {
		return payTo;
	}
	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	
	
	
}