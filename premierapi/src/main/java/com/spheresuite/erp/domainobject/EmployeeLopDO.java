package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeelop")
@TableGenerator(name ="employeelop", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeelop.findAll", query = "SELECT r FROM EmployeeLopDO r"),
    @NamedQuery(name = "employeelop.findById", query = "SELECT r FROM EmployeeLopDO r where r.id =:id"),
    @NamedQuery(name = "employeelop.findByEmpId", query = "SELECT r FROM EmployeeLopDO r where r.empId =:empId and r.status =:status"),
    /*@NamedQuery(name = "employeebonus.findByEmpIdsAndMonth", query = "SELECT r FROM EmployeeBonusDO r where r.empId IN :empId and SUBSTRING(r.startdate,1,7) >=:fromMonth and SUBSTRING(r.startdate,1,7) <=:toMonth"),
    @NamedQuery(name = "employeebonus.findByMonth", query = "SELECT r FROM EmployeeBonusDO r where r.bonusMonth = :monthly and r.status =:status"),*/
    /*SELECT * FROM spheresuite.employeelop where (SUBSTR(startdate,1,7) >= '2017-04' and SUBSTR(startdate,1,7) <='2018-03')*/
})
public class EmployeeLopDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeelop.findAll";
	
	public static final String FIND_BY_ID = "employeelop.findById";
	
	public static final String FIND_BY_EMPID = "employeelop.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeelop")
	private Long id;
	private Long empId;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private char status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
}