package com.spheresuite.erp.rs;

import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.JwtTokenGenerator;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.UserUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/user")
public class UserRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(UserRS.class.getName());

	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				UserDO userDO = new UserDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		userDO.setEmpId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.EMPID).toString() :  ""));
			 		userDO.setRoleId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ROLE_ID).toString() :  ""));
					userDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString().toString() : null);
					userDO.setIsDeleted(0L);
					userDO.setUpdatedon(new Date());
					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			userDO.setCreatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
					char[] temppasssword = UserUtil.geek_Password(5);
					//Base64.Encoder encoder = Base64.getEncoder(); 
					//byte[] enString = encoder.encode(temppasssword.toString().getBytes());  
					//String enString = CommonUtil.encryptText("testseed",temppasssword.toString());
					String pwd = temppasssword.toString();
					userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
					userDO.setStatus(CommonConstants.PEDING);
			 	}
			 	userDO = new UserService().persist(userDO);
			 	CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "User Created");
			 	if(userDO != null && userDO.getId() !=null){
					String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
					String fromEmail = CommonConstants.SENDMAIL;
					String toEmails = userDO.getEmail();
					String userName = null;
					List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(userDO.getEmpId());
					if(empList != null && empList.size() > 0){
						userName =  String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname());
					}
					String URL = CommonUtil.getEnvironment(request);
					URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/changepassword/";
					String url = URL+userDO.getTemppassowrd()+"/"+userDO.getEmail()+"/N";
					String emailBody = "Cick Here Login : " +URL+userDO.getTemppassowrd()+"/"+userDO.getEmail()+"/N";
			 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "inviteUser", userName); 
					if(mailStatus){
						return CommonWebUtil.buildSuccessResponse().toString();
					}else{
						return CommonWebUtil.buildErrorResponse("").toString();
					}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/resendInvite/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String resendInvite(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<UserDO> userList = new UserService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					if(userList != null && userList.size() > 0){
						if(userList.get(0).getTemppassowrd() != null && userList.get(0).getEmail() != null){
							String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
							String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
							String toEmails = userList.get(0).getEmail();
							String URL = CommonUtil.getEnvironment(request);
							URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/changepassword/";
							String emailBody = "Cick Here Login : " +URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getEmail()+"/N";
							String url = URL+userList.get(0).getTemppassowrd()+"/"+userList.get(0).getEmail()+"/N";

							String userName = null;
							List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(userList.get(0).getEmpId());
							if(empList != null && empList.size() > 0){
								userName =  String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname());
							}
	
							boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "reInvite", userName);
							
							if(mailStatus){
								return CommonWebUtil.buildSuccessResponse().toString();
							}else{
								return CommonWebUtil.buildErrorResponse("").toString();
							}
						}
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UserDO> userList = new UserService().retrieve();
				respJSON = UserUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<UserDO> userList = new UserService().retrieveActive();
				respJSON = UserUtil.getUserList(userList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<UserDO> userList = new UserService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = UserUtil.getUserList(userList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = new UserService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));;
				 	if (userDOList != null){
				 		userDO = userDOList.get(0);
						userDO.setRoleId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ROLE_ID).toString() :  ""));
						userDO.setEmail(inputJSON != null ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
						userDO.setUpdatedon(new Date());
						if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			userDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
						userDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
						if(userDO.getStatus() != 'd'){
							userDO.setIsDeleted(0L);
						}else{
							userDO.setIsDeleted(1L);
						}
						
						new UserService().update(userDO);
						CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "User Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/updatepassword", method = RequestMethod.POST)
	public @ResponseBody String updatepassword(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() && !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = new UserService().retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				 	if (userDOList != null){
				 		userDO = userDOList.get(0);
				 		/*Base64.Encoder encoder = Base64.getEncoder(); 
						byte[] enString = encoder.encode(inputJSON.get(CommonConstants.PASSWORD).toString().getBytes());*/
						String pwd = inputJSON.get(CommonConstants.PASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
				 		//String enString = CommonUtil.encryptText("testseed",inputJSON != null ? CommonConstants.PASSWORD.toString() : "");
						userDO.setUpdatedon(new Date());
						new UserService().update(userDO);
						CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Password Updated");
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/delete/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String delete(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO> userList = new UserService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				 	if (userList != null){
				 		userDO = userList.get(0);
				 		userDO.setIsDeleted(1L);
				 		userDO.setStatus('d');
						userDO.setUpdatedon(new Date());
						new UserService().update(userDO);
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveForLogin", method = RequestMethod.POST)
	public @ResponseBody String retrieveForLogin(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
					&& !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				List<UserDO> userList = new UserService().retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList.size() > 0) {
					byte[] decodedBytes = Base64.getDecoder().decode(userList.get(0).getPassword());
					String stringDecode = new String(decodedBytes, "UTF-8");
					if(stringDecode.toString().equals(inputJSON.get(CommonConstants.PASSWORD).toString())){
						String token = new JwtTokenGenerator().createJWT(userList);
						HttpSession session = request.getSession();
						session.setAttribute("token", token);
						//session.setMaxInactiveInterval(60*60);
						respJSON = UserUtil.getUserForLogin(userList,token).toString();
					}
					
				}
			}
			//}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
			
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public @ResponseBody String resetpassword(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() 
			 			&& !inputJSON.get(CommonConstants.TEMP_PASSWORD).toString().isEmpty()
			 			&& !inputJSON.get(CommonConstants.NEWPASSWORD).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = new UserService().retrieveByTempPassword(inputJSON.get(CommonConstants.TEMP_PASSWORD).toString(),
			 				inputJSON.get(CommonConstants.EMAIL).toString());
				 	if (userDOList != null && userDOList.size() > 0){
				 		userDO = userDOList.get(0);
				 		String pwd = inputJSON.get(CommonConstants.NEWPASSWORD).toString();
						userDO.setPassword(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
						userDO.setTemppassowrd(null);
						userDO.setUpdatedon(new Date());
						userDO.setStatus(CommonConstants.ACTIVE);
						new UserService().update(userDO);
				 	}else{
				 		return CommonWebUtil.buildErrorResponse("").toString();
				 	}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/sendPassword", method = RequestMethod.POST)
	public @ResponseBody String sendPassword(Model model, HttpServletRequest request) {
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()){
				List<UserDO> userList = new UserService().retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList != null && userList.size() > 0){
					String host = request.getServletContext().getInitParameter(CommonConstants.HOST);
					String fromEmail = request.getServletContext().getInitParameter(CommonConstants.FROMEMAIL);
					String toEmails = userList.get(0).getEmail();
					if(userList.get(0).getPassword() != null){
						UserDO userDO = new UserDO();
						userDO = userList.get(0);
						/*char[] temppasssword = UserUtil.geek_Password(5);
						userDO.setTemppassowrd(temppasssword.toString());*/
						char[] temppasssword = UserUtil.geek_Password(5);
						//Base64.Encoder encoder = Base64.getEncoder(); 
						//byte[] enString = encoder.encode(temppasssword.toString().getBytes());
						//String enString = CommonUtil.encryptText("testseed",temppasssword.toString());
						//userDO.setTemppassowrd(enString.toString());
						
						String pwd = temppasssword.toString();
						userDO.setTemppassowrd(Base64.getEncoder().encodeToString(pwd.getBytes("UTF-8")));
						userDO.setPassword(null);
						userDO = new UserService().update(userDO);
						String URL = CommonUtil.getEnvironment(request);
						URL = CommonConstants.HTTP+URL+".spheresuite.com/ui/#!/changepassword/";
						String emailBody = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getEmail()+"/F";
						String userName = null;
						List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(userList.get(0).getEmpId());
						if(empList != null && empList.size() > 0){
							userName =  String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname());
						}
						
						String url = URL+userDO.getTemppassowrd()+"/"+userList.get(0).getEmail()+"/F";
				 		boolean mailStatus = EmailProxyUtil.sendEmail(request,host,  fromEmail,  toEmails, "", "", emailBody, false, url, "resetPassword", userName); 

						if(mailStatus){
							return CommonWebUtil.buildSuccessResponse().toString();
						}else{
							return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_SENT).toString();
						}
					}else{
						return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_REGISTER).toString();
					}
				}else{
					return CommonWebUtil.buildErrorResponse(CommonConstants.EMAIL_NOT_REGISTER).toString();
				}
			}else{
				return CommonWebUtil.buildErrorResponse("").toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
	}
	
}