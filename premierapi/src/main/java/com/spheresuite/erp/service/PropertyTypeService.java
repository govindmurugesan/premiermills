package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PropertyTypeDAO;
import com.spheresuite.erp.domainobject.PropertyTypeDO;
import com.spheresuite.erp.exception.AppException;

public class PropertyTypeService {
	static Logger logger = Logger.getLogger(PropertyTypeService.class.getName());
	
	public PropertyTypeDO persist(PropertyTypeDO propertyTypeDO) throws AppException {
		return new PropertyTypeDAO().persist(propertyTypeDO);
	}

	public List<PropertyTypeDO> retrieveActive() throws AppException {
		return new PropertyTypeDAO().retrieveActive();
	}
	
	public List<PropertyTypeDO> retrieveById(Long id) throws AppException {
		return new PropertyTypeDAO().retrieveById(id);
	}
	
	public List<PropertyTypeDO> retrieve() throws AppException {
		return new PropertyTypeDAO().retrieve();
	}
	
	public PropertyTypeDO update(PropertyTypeDO propertyTypeDO) throws AppException {
		return new PropertyTypeDAO().update(propertyTypeDO);
	}
}
