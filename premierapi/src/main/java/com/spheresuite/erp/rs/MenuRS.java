package com.spheresuite.erp.rs;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.service.MenuService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.MenuUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/menus")
public class MenuRS {
	
	static Logger logger = Logger.getLogger(MenuRS.class.getName());
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<MenuDO> menuList = new MenuService().retrieveMainMenu();
				respJSON = MenuUtil.getMenuList(menuList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

}
