package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TimesheetTypeDAO;
import com.spheresuite.erp.domainobject.TimesheetTypeDO;
import com.spheresuite.erp.exception.AppException;

public class TimesheetTypeService {
	static Logger logger = Logger.getLogger(TimesheetTypeService.class.getName());
	
	public TimesheetTypeDO persist(TimesheetTypeDO  timesheetTypeDO) throws AppException {
		return new TimesheetTypeDAO().persist(timesheetTypeDO);
	}
	
	public List<TimesheetTypeDO> retrieveById(Long Id) throws AppException {
		return new TimesheetTypeDAO().retrieveById(Id);
	}
	
	public List<TimesheetTypeDO> retrieve() throws AppException {
		return new TimesheetTypeDAO().retrieve();
	}
	
	public TimesheetTypeDO update(TimesheetTypeDO timesheetTypeDO) throws AppException {
		return new TimesheetTypeDAO().update(timesheetTypeDO);
	}
}
