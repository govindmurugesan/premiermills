package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bankinformation")
@TableGenerator(name ="bankinformation", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "bankinformation.findAll", query = "SELECT r FROM EmployeeBankInformationDO r"),
    @NamedQuery(name = "bankinformation.findById", query = "SELECT r FROM EmployeeBankInformationDO r where r.id =:id"),
    @NamedQuery(name = "bankinformation.findByEmpId", query = "SELECT r FROM EmployeeBankInformationDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeBankInformationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "bankinformation.findAll";
	
	public static final String FIND_BY_ID = "bankinformation.findById";
	
	public static final String FIND_BY_EMPID = "bankinformation.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "bankinformation")
	private Long id;
	private String bankName;
	private String ifscCode;
	private String branchName;
	private String accountName;
	private Long accountNumber;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}