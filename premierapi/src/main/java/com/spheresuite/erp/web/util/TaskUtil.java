package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.TaskDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;

public class TaskUtil {
	
	private TaskUtil() {}
	
	public static JSONObject getTaskList(List<TaskDO> taskList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TaskDO task : taskList) {
				resultJSONArray.put(getCategoryDetailObject(task));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCategoryDetailObject(TaskDO task)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(task.getId() != null ? task.getId() : ""));
		result.put(CommonConstants.TASKDETAIL, String.valueOf(task.getTaskDetails() != null ? task.getTaskDetails() : ""));
		result.put(CommonConstants.DATE, String.valueOf(task.getTaskDate() != null ? task.getTaskDate() : ""));
		result.put(CommonConstants.TIME, String.valueOf(task.getTaskTime() != null ? task.getTaskTime() : ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToString(task.getUpdatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(task.getStatus()));
		if(task.getUpdatedBy() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(task.getUpdatedBy()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(task.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.LEAD_ID, String.valueOf(task.getLeadId() != null ? task.getLeadId() : ""));
		
		if(task.getLeadId() != null){
			List<LeadDO> leads = new LeadService().retrieveById(task.getLeadId());
			if(leads !=null && leads.size() > 0){
				result.put(CommonConstants.LEAD_NAME, String.valueOf(leads.get(0).getName()));
			}else{
				result.put(CommonConstants.LEAD_NAME,"");
			}
		}else{
			result.put(CommonConstants.LEAD_NAME,"");
		}
		return result;
	}
}
