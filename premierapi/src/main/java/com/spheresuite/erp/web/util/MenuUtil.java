package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.MenusDAO;
import com.spheresuite.erp.domainobject.MenuDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.util.CommonConstants;

public class MenuUtil {
	
	private MenuUtil() {}
	

	public static JSONObject getMenuList(List<MenuDO> menuList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (MenuDO menu : menuList) {
				resultJSONArray.put(getMenuDetailObject(menu));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getMenuDetailObject(MenuDO menu)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(menu.getId()));
		result.put(CommonConstants.NAME, String.valueOf(menu.getName()));
		if(menu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(menu.getUrl()));
		}
		if(menu.getIcon() != null){
			result.put(CommonConstants.ICON, String.valueOf(menu.getIcon()));
		}
		
		if(menu.getActiveicon() != null){
			result.put(CommonConstants.ACTIVEICON, String.valueOf(menu.getActiveicon()));
		}else{
			result.put(CommonConstants.ACTIVEICON, "");
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
		MenusDAO obj = new  MenusDAO();
		List<MenuDO> submenuList = obj.retrieveSubMenu(menu.getId());
		for(MenuDO subMenu : submenuList){
			resultJSONArraySubMenu.put(getSubMenuDetailObject(subMenu));
		}
		result.put(CommonConstants.SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
	
	public static JSONObject getSubMenuDetailObject(MenuDO subMenu)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(subMenu.getId()));
		result.put(CommonConstants.NAME, String.valueOf(subMenu.getName()));
		if(subMenu.getUrl() != null){
			result.put(CommonConstants.URL, String.valueOf(subMenu.getUrl()));
		}
		JSONArray resultJSONArraySubMenu = new JSONArray();
		MenusDAO obj = new  MenusDAO();
		List<MenuDO> submenuList = obj.retrieveSubSubMenu(subMenu.getId());
		for(MenuDO subMenu1 : submenuList){
			resultJSONArraySubMenu.put(getSubMenuDetailObject(subMenu1));
		}
		result.put(CommonConstants.SUB_SUB_MENU,resultJSONArraySubMenu);
		return result;
	}
}
