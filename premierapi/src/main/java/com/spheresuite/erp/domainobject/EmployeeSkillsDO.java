package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeskills")
@TableGenerator(name ="employeeskills", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeskills.findAll", query = "SELECT r FROM EmployeeSkillsDO r"),
    @NamedQuery(name = "employeeskills.findById", query = "SELECT r FROM EmployeeSkillsDO r where r.id =:id"),
    @NamedQuery(name = "employeeskills.findByEmpId", query = "SELECT r FROM EmployeeSkillsDO r where r.empId =:empId"),
})
public class EmployeeSkillsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeskills.findAll";
	
	public static final String FIND_BY_ID = "employeeskills.findById";
	
	public static final String FIND_BY_EMPID = "employeeskills.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeskills")
	private Long id;
	private String name;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}