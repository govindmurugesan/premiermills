package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ItSavingsDAO;
import com.spheresuite.erp.domainobject.ItSavingsDO;
import com.spheresuite.erp.exception.AppException;

public class ItSavingsService {
	static Logger logger = Logger.getLogger(ItSavingsService.class.getName());
	
	public ItSavingsDO persist(ItSavingsDO itSavingsDO) throws AppException {
		return new ItSavingsDAO().persist(itSavingsDO);
	}

	public List<ItSavingsDO> retrieveByEmpId(Long empId) throws AppException {
		return new ItSavingsDAO().retrieveByEmpId(empId);
	}
	
	public List<ItSavingsDO> retrieveById(Long id) throws AppException {
		return new ItSavingsDAO().retrieveById(id);
	}
	
	public List<ItSavingsDO> retrieve() throws AppException {
		return new ItSavingsDAO().retrieve();
	}
	
	public List<ItSavingsDO> retrieveByDate(String fromDate, String toDate) throws AppException {
		return new ItSavingsDAO().retrieveByDate(fromDate, toDate);
	}
	
	
	
	public ItSavingsDO update(ItSavingsDO itSavingsDO) throws AppException {
		return new ItSavingsDAO().update(itSavingsDO);
	}
}
