package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.service.CustomerService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CustomerUtil;
import com.spheresuite.erp.web.util.LeadUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/customer")
public class CustomerRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CustomerRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CustomerDO customerDO = new CustomerDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		customerDO.setId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ID).toString() : ""));
			 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		customerDO.setStatus(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : "");
			 		customerDO.setSourceId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SOURCE).toString() : ""));
			 		customerDO.setIndustryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : ""));
			 		customerDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		customerDO.setPone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		customerDO.setEmail(inputJSON != null ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		customerDO.setComment(inputJSON != null ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		customerDO.setAddress(inputJSON != null ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		customerDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			customerDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			customerDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new CustomerService().persist(customerDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Customer Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<CustomerDO> customerList = new CustomerService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = CustomerUtil.getCustomerList(customerList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerDO> categoryList = new CustomerService().retrieve();
				respJSON = CustomerUtil.getCustomerList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveByActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CustomerDO> categoryList = new CustomerService().retrieve();
				respJSON = CustomerUtil.getCustomerList(categoryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				CustomerDO customerDO = new CustomerDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CustomerDO> customerList = new CustomerService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		customerDO = customerList.get(0);
			 		customerDO.setId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ID).toString() : ""));
			 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		customerDO.setStatus(inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString() : "");
			 		customerDO.setSourceId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.SOURCE).toString() : ""));
			 		customerDO.setIndustryId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.INDUSTRY).toString() : ""));
			 		customerDO.setMobile(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		customerDO.setPone(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		customerDO.setEmail(inputJSON != null ? inputJSON.get(CommonConstants.EMAIL).toString() : "");
			 		customerDO.setComment(inputJSON != null ? inputJSON.get(CommonConstants.COMMENT).toString() : "");
			 		customerDO.setAddress(inputJSON != null ? inputJSON.get(CommonConstants.ADDRESS).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			customerDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		customerDO.setUpdatedon(new Date());
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Customer Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDate/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDate(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();
					fromDate = fromDate.substring(0, 10)+" 00:00:00";
					toDate = toDate.substring(0, 10)+" 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					List<LeadDO> newcustomerList = new LeadService().retrieveCustomerBetweenDate(formattedDate,formattedDate1);
					List<LeadDO> customerList = new LeadService().retrieveAll("C");
					respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveBetweenDateByEmp/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String retrieveBetweenDateByEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.FROMDATE).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.TODATE).toString().isEmpty()){
					String fromDate = inputJSON.get(CommonConstants.FROMDATE).toString();
					String toDate = inputJSON.get(CommonConstants.TODATE).toString();
					String empId = "";
					if(inputJSON.get(CommonConstants.EMPID).toString() != null){
						empId = inputJSON.get(CommonConstants.EMPID).toString();
					}
					fromDate = fromDate.substring(0, 10)+" 00:00:00";
					toDate = toDate.substring(0, 10)+" 23:59:59";
					Date formattedDate = CommonUtil.convertStringToDateTimeFmt(fromDate);
					Date formattedDate1 = CommonUtil.convertStringToDateTimeFmt(toDate);
					if(!empId.isEmpty() && empId.length() > 0){
						List<LeadDO> newcustomerList = new LeadService().retrieveCustomerBetweenDateByEmp(formattedDate,formattedDate1, empId);
						List<LeadDO> customerList = new LeadService().retrieveByEmp("C",Long.parseLong(empId));
						respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
					}else{
						List<LeadDO> newcustomerList = new LeadService().retrieveCustomerBetweenDate(formattedDate,formattedDate1);
						List<LeadDO> customerList = new LeadService().retrieveAll("C");
						respJSON = LeadUtil.getCustomerListForChart(customerList,newcustomerList).toString();
					}
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
