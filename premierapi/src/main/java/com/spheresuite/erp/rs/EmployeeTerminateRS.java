package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeTerminateDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTerminateService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.EmployeeTerminateUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeeterminate")
public class EmployeeTerminateRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeTerminateRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTerminateDO employeeTerminateDO = new EmployeeTerminateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeTerminateDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		employeeTerminateDO.setTerminateDate(!inputJSON.get(CommonConstants.TERMINATEDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.TERMINATEDATE).toString()) : null);
			 		employeeTerminateDO.setResignDate(!inputJSON.get(CommonConstants.RESIGNEDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.RESIGNEDDATE).toString()) : null); 
			 		employeeTerminateDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeTerminateDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(!inputJSON.get(CommonConstants.TERMINATETYPEID).toString().isEmpty()){
			 			employeeTerminateDO.setTerminateTypeId(Long.parseLong(inputJSON.get(CommonConstants.TERMINATETYPEID).toString()));
			 		}
			 		employeeTerminateDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 	}
				new EmployeeTerminateService().persist(employeeTerminateDO);
				List<EmployeeDO> employeeList = new EmployeeService().retriveById(employeeTerminateDO.getEmpId());
				if(employeeList != null && employeeList.size() > 0){
					EmployeeDO employee = employeeList.get(0);
					employee.setStatus('t');
					new EmployeeService().update(employee);
				}
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee  Terminated");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeTerminateDO> addressTypeList = new EmployeeTerminateService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeTerminateUtil.getEmployeeTerminateList(addressTypeList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeeTerminateDO> addressTypeList = new EmployeeTerminateService().retrieve();
				respJSON = EmployeeTerminateUtil.getEmployeeTerminateList(addressTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeTerminateDO employeeTerminateDO = new EmployeeTerminateDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeTerminateDO> addressTypeList = new EmployeeTerminateService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeTerminateDO = addressTypeList.get(0);
			 		employeeTerminateDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		employeeTerminateDO.setTerminateDate(!inputJSON.get(CommonConstants.TERMINATEDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.TERMINATEDATE).toString()) : null);
			 		employeeTerminateDO.setResignDate(!inputJSON.get(CommonConstants.RESIGNEDDATE).toString().isEmpty() ? CommonUtil.convertStringToSqlDateMonth(inputJSON.get(CommonConstants.RESIGNEDDATE).toString()) : null); 
			 		employeeTerminateDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeTerminateDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		if(!inputJSON.get(CommonConstants.TERMINATETYPEID).toString().isEmpty()){
			 			employeeTerminateDO.setTerminateTypeId(Long.parseLong(inputJSON.get(CommonConstants.TERMINATETYPEID).toString()));
			 		}
			 		employeeTerminateDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
			 		new EmployeeTerminateService().update(employeeTerminateDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Employee Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		new EmployeeTerminateService().delete(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		//CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Termination Updated");
			 		return CommonWebUtil.buildSuccessResponse().toString();
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
