package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.ProjectService;
import com.spheresuite.erp.util.CommonConstants;

public class TimesheetUtil {
	
	private TimesheetUtil() {}
	
	public static JSONObject getTimesheetList(List<TimesheetDO> timesheetList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObject(timesheet));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getTimesheetListWithStatus(List<TimesheetDO> timesheetList, char status)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (TimesheetDO timesheet : timesheetList) {
				resultJSONArray.put(getTimesheetDetailObjectWithStatus(timesheet, status));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getTimesheetDetailObject(TimesheetDO timesheet)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		
		if(timesheet.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, "");
		}
		
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		/*List<TimesheetTypeDO> timesheetTypeList = new TimesheetTypeService().retrieveById(timesheet.getTimesheetType());*/
		List<ProjectDO> projectList = new ProjectService().retrieveById(timesheet.getTimesheetType());
 		result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
 		/*result.put(CommonConstants.TYPENAME, String.valueOf(timesheetTypeList.get(0).getName()));*/
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeekenddate()));
		//result.put(CommonConstants.STATUS, String.valueOf(timesheet.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(timesheet.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
	
	public static JSONObject getTimesheetDetailObjectWithStatus(TimesheetDO timesheet, char status)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(timesheet.getId()));
		result.put(CommonConstants.DATE, String.valueOf(timesheet.getDate()));
		result.put(CommonConstants.HOURS, String.valueOf(timesheet.getHours()));
		
		if(timesheet.getDescription() != null){
			result.put(CommonConstants.DESCRIPTION, String.valueOf(timesheet.getDescription()));
		}else{
			result.put(CommonConstants.DESCRIPTION, "");
		}
		
		result.put(CommonConstants.TYPE, String.valueOf(timesheet.getTimesheetType()));
		List<ProjectDO> projectList = new ProjectService().retrieveById(timesheet.getTimesheetType());
 		result.put(CommonConstants.TYPENAME, String.valueOf(projectList.get(0).getProjectname()));
		result.put(CommonConstants.WEEKENDDATE, String.valueOf(timesheet.getWeekenddate()));
		result.put(CommonConstants.STATUS, String.valueOf(status));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(timesheet.getUpdatedon())));
		if(timesheet.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(timesheet.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		return result;
	}
}
