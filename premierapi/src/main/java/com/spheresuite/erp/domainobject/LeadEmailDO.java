package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="leademail")
@TableGenerator(name ="leademail", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "leademail.findAll", query = "SELECT r FROM LeadEmailDO r"),
    @NamedQuery(name = "leademail.findByLeadId", query = "SELECT r FROM LeadEmailDO r where r.leadId =:leadId"),
    @NamedQuery(name = "leademail.findById", query = "SELECT r FROM LeadEmailDO r where r.id =:id"),
    @NamedQuery(name = "leademail.deleteById", query = "DELETE FROM LeadEmailDO r where r.leadId =:id"),
})
public class LeadEmailDO implements Serializable {

	private static final long serialVersionUID = -7730147181503448729L;

	public static final String FIND_ALL = "leademail.findAll";
	
	public static final String FIND_BY_LEAD_ID = "leademail.findByLeadId";
	
	public static final String FIND_BY_ID = "leademail.findById";
	
	public static final String DELETE_BY_ID = "leademail.deleteById";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "leademail")
	private Long id;
	private Long leadId;
	private Long contactId;
	private String updatedby;
	@Column(columnDefinition="LONGTEXT")
	private String fromAddress;
	@Column(columnDefinition="LONGTEXT")
	private String subject;
	@Temporal(TemporalType.TIMESTAMP)
	private Date sentDate;
	@Column(columnDefinition="LONGTEXT")
	private String messageContent;
	@Column(columnDefinition="LONGTEXT")
	private String attachFiles;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	@Lob
    private byte[] attachmentFile;
	
	@Transient
	private String attachment;
	
	@Transient
	private String contentType;
	
	@Transient
	private String toAddress;
	
	@Transient
	private Long uId;
	
	@Transient
	private String pop3UId;
	
	@Transient
	private boolean seenMsg;
	
	@Column(columnDefinition="LONGBLOB")
    private byte[] msgObject;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLeadId() {
		return leadId;
	}

	public void setLeadId(Long leadId) {
		this.leadId = leadId;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getAttachFiles() {
		return attachFiles;
	}

	public void setAttachFiles(String attachFiles) {
		this.attachFiles = attachFiles;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public byte[] getAttachmentFile() {
		return attachmentFile;
	}

	public void setAttachmentFile(byte[] attachmentFile) {
		this.attachmentFile = attachmentFile;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public Long getuId() {
		return uId;
	}

	public void setuId(Long uId) {
		this.uId = uId;
	}

	public String getPop3UId() {
		return pop3UId;
	}

	public void setPop3UId(String pop3uId) {
		pop3UId = pop3uId;
	}

	public byte[] getMsgObject() {
		return msgObject;
	}

	public void setMsgObject(byte[] msgObject) {
		this.msgObject = msgObject;
	}
	
	public boolean isSeenMsg() {
		return seenMsg;
	}

	public void setSeenMsg(boolean seenMsg) {
		this.seenMsg = seenMsg;
	}
}