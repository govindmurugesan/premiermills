package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.CategoryDAO;
import com.spheresuite.erp.domainobject.CategoryDO;
import com.spheresuite.erp.exception.AppException;

public class CategoryService {
	static Logger logger = Logger.getLogger(CategoryService.class.getName());
	
	public CategoryDO persist(CategoryDO countryDO) throws AppException {
		return new CategoryDAO().persist(countryDO);
	}

	public List<CategoryDO> retrieveActive() throws AppException {
		return new CategoryDAO().retrieveActive();
	}
	
	public List<CategoryDO> retrieveById(Long id) throws AppException {
		return new CategoryDAO().retrieveById(id);
	}
	
	public List<CategoryDO> retrieve() throws AppException {
		return new CategoryDAO().retrieve();
	}
	
	public CategoryDO update(CategoryDO countryDO) throws AppException {
		return new CategoryDAO().update(countryDO);
	}
}
