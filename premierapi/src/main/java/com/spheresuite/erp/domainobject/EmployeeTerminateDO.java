package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeeterminate")
@TableGenerator(name ="employeeterminate", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeeterminate.findAll", query = "SELECT r FROM EmployeeTerminateDO r"),
    @NamedQuery(name = "employeeterminate.findById", query = "SELECT r FROM EmployeeTerminateDO r where r.id =:id"),
    @NamedQuery(name = "employeeterminate.findByEmpId", query = "SELECT r FROM EmployeeTerminateDO r where r.empId =:empId"),
})
public class EmployeeTerminateDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeeterminate.findAll";
	
	public static final String FIND_BY_ID = "employeeterminate.findById";
	
	public static final String FIND_BY_EMPID = "employeeterminate.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeeterminate")
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date terminateDate;
	@Temporal(TemporalType.DATE)
	private Date resignDate;
	private String comments;
	private Long terminateTypeId;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getTerminateDate() {
		return terminateDate;
	}
	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getTerminateTypeId() {
		return terminateTypeId;
	}
	public void setTerminateTypeId(Long terminateTypeId) {
		this.terminateTypeId = terminateTypeId;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Date getResignDate() {
		return resignDate;
	}
	public void setResignDate(Date resignDate) {
		this.resignDate = resignDate;
	}
}