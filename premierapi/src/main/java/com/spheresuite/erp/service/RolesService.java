package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.RolesDAO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.exception.AppException;

public class RolesService {
	static Logger logger = Logger.getLogger(RolesService.class.getName());
	
	public RolesDO persist(RolesDO rolesDO) throws AppException {
		return new RolesDAO().persist(rolesDO);
	}

	public List<RolesDO> retrieve() throws AppException {
		return new RolesDAO().retrieve();
	}

	public List<RolesDO> retriveById(long id) throws AppException {
		return new RolesDAO().retriveById(id);
	}

	public boolean delete(RolesDO rolesDO) throws AppException {
		return new RolesDAO().delete(rolesDO);
	}

	public RolesDO update(RolesDO rolesDO) throws AppException {
		return new RolesDAO().update(rolesDO);
	}

}
