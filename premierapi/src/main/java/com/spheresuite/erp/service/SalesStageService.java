package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.SalesStageDAO;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.exception.AppException;

public class SalesStageService {
	static Logger logger = Logger.getLogger(SalesStageService.class.getName());
	
	public SalesStageDO persist(SalesStageDO salesStageDO) throws AppException {
		return new SalesStageDAO().persist(salesStageDO);
	}

	public List<SalesStageDO> retrieveActive() throws AppException {
		return new SalesStageDAO().retrieveActive();
	}
	
	public List<SalesStageDO> retrieveById(Long id) throws AppException {
		return new SalesStageDAO().retrieveById(id);
	}
	
	public List<SalesStageDO> retrieve() throws AppException {
		return new SalesStageDAO().retrieve();
	}
	
	public SalesStageDO update(SalesStageDO salesStageDO) throws AppException {
		return new SalesStageDAO().update(salesStageDO);
	}
}
