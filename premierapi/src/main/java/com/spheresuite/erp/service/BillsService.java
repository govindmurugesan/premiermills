package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.BillsDAO;
import com.spheresuite.erp.domainobject.BillsDO;
import com.spheresuite.erp.exception.AppException;

public class BillsService {
	static Logger logger = Logger.getLogger(BillsService.class.getName());
	
	public BillsDO persist(BillsDO billsDO) throws AppException {
		return new BillsDAO().persist(billsDO);
	}

	public List<BillsDO> retriveById(Long id) throws AppException {
		return new BillsDAO().retrieveById(id);
	}
	
	public List<BillsDO> retrieve() throws AppException {
		return new BillsDAO().retrieve();
	}
	
	public List<BillsDO> retrieveBillNumber() throws AppException {
		return new BillsDAO().retrieveBillNumber();
	}
	
	public BillsDO update(BillsDO billsDO) throws AppException {
		return new BillsDAO().update(billsDO);
	}
	
	public boolean persistList(List<BillsDO> billsDO) throws AppException {
		return new BillsDAO().persistList(billsDO);
	}
}
