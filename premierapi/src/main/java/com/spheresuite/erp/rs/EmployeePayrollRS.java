package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeDeductionDO;
import com.spheresuite.erp.domainobject.EmployeeEarningsDO;
import com.spheresuite.erp.domainobject.EmployeePayrollDO;
import com.spheresuite.erp.domainobject.EmployeeTaxDO;
import com.spheresuite.erp.domainobject.FyDO;
import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.domainobject.PayrollEarningsDetailsDO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.service.EmployeeDeductionService;
import com.spheresuite.erp.service.EmployeeEarningsService;
import com.spheresuite.erp.service.EmployeePayrollService;
import com.spheresuite.erp.service.EmployeeTaxService;
import com.spheresuite.erp.service.FyService;
import com.spheresuite.erp.service.PayrollDeductionDetailsService;
import com.spheresuite.erp.service.PayrollEarningsDetailsService;
import com.spheresuite.erp.service.PayrollTaxDetailsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeePayrollUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/empPayroll")
public class EmployeePayrollRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeePayrollRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.PAYROLLLIST).toString().isEmpty()){
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.PAYROLLLIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
				 			List<EmployeePayrollDO> payrollList = new EmployeePayrollService().retriveByMonthAndEmp(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(), Long.parseLong(inputJSON1.get(CommonConstants.EMPID).toString()));
				 			if(payrollList == null || payrollList.size() <= 0){
				 			employeePayrollDO.setPayrollbatchId(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()) : null);
			 				employeePayrollDO.setPayrollMonth(!inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLLMONTH).toString() : null);
			 				/*employeePayrollDO.setPayrollType(!inputJSON.get(CommonConstants.PAYROLLTYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYROLLTYPE).toString() : null);*/
				 			employeePayrollDO.setEmpId(!inputJSON1.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON1.get(CommonConstants.EMPID).toString()) : null);
				 			employeePayrollDO.setEmpName(!inputJSON1.get(CommonConstants.EMPNAME).toString().isEmpty() ? inputJSON1.get(CommonConstants.EMPNAME).toString() : null);
				 			employeePayrollDO.setNetmonth(!inputJSON1.get(CommonConstants.NETMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON1.get(CommonConstants.NETMONTHLY).toString()) : null);
				 			if(!inputJSON1.get(CommonConstants.TAXMONTHLY).toString().isEmpty() && !inputJSON1.get(CommonConstants.TAXMONTHLY).toString().equalsIgnoreCase("null")){
				 				employeePayrollDO.setTaxMonth(Double.parseDouble(inputJSON1.get(CommonConstants.TAXMONTHLY).toString()));
				 			}
				 			employeePayrollDO.setEarningmonth(!inputJSON1.get(CommonConstants.EARNINGMONTHLY).toString().isEmpty() ? Double.parseDouble(inputJSON1.get(CommonConstants.EARNINGMONTHLY).toString()) : null);
				 			if(!inputJSON1.get(CommonConstants.DEDUCTIONMONTHLY).toString().isEmpty()){
				 				employeePayrollDO.setDeductionmonth(Double.parseDouble(inputJSON1.get(CommonConstants.DEDUCTIONMONTHLY).toString()));
				 			}
					 		employeePayrollDO.setGrossPay(!inputJSON1.get(CommonConstants.BASICGROSS_PAY).toString().isEmpty() ? Double.parseDouble(inputJSON1.get(CommonConstants.BASICGROSS_PAY).toString()) : null);
					 		employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		employeePayrollDO.setStatus('d');
					 		new EmployeePayrollService().persist(employeePayrollDO);
					 		
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollEarningsDetailsDO> earningDetailsList = new ArrayList<PayrollEarningsDetailsDO>(); 
					 			List<EmployeeEarningsDO> earningsList = new EmployeeEarningsService().retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeEarningsDO employeeEarningsDO : earningsList) {
					 				PayrollEarningsDetailsDO earningDetails = new PayrollEarningsDetailsDO(); 
					 				earningDetails.setPayrollid(employeePayrollDO.getId());
					 				earningDetails.setEarningsId(employeeEarningsDO.getEarningsId());
					 				earningDetails.setMonthly(employeeEarningsDO.getMonthly());
					 				earningDetails.setYtd(employeeEarningsDO.getYtd());
					 				earningDetails.setUpdatedon(new Date());
						 			earningDetailsList.add(earningDetails);
								}
						 		new PayrollEarningsDetailsService().persist(earningDetailsList);
					 		}
				 			
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollDeductionDetailsDO> deductionDetailsList = new ArrayList<PayrollDeductionDetailsDO>(); 
								List<EmployeeDeductionDO> deductionList = new EmployeeDeductionService().retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeDeductionDO employeeDeductionDO : deductionList) {
					 				PayrollDeductionDetailsDO deductionDetails = new PayrollDeductionDetailsDO();
					 				deductionDetails.setPayrollid(employeePayrollDO.getId());
						 			deductionDetails.setDeductionId(employeeDeductionDO.getEmpdeductionid());
						 			deductionDetails.setMonthly(employeeDeductionDO.getMonthly());
						 			deductionDetails.setYtd(employeeDeductionDO.getYtd());
						 			deductionDetails.setUpdatedon(new Date());
						 			deductionDetailsList.add(deductionDetails);
								}
						 		new PayrollDeductionDetailsService().persist(deductionDetailsList);
				 			}
				 			
				 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
					 			List<PayrollTaxDetailsDO> payDetailsList = new ArrayList<PayrollTaxDetailsDO>(); 
								List<EmployeeTaxDO> taxList = new EmployeeTaxService().retrieveByEmpCompensationId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
					 			for (EmployeeTaxDO employeeDeductionDO : taxList) {
					 				PayrollTaxDetailsDO deductionDetails = new PayrollTaxDetailsDO();
					 				deductionDetails.setPayrollid(employeePayrollDO.getId());
						 			deductionDetails.setTaxId(employeeDeductionDO.getEmptaxid());
						 			deductionDetails.setMonthly(employeeDeductionDO.getMonthly());
						 			deductionDetails.setYtd(employeeDeductionDO.getYtd());
						 			deductionDetails.setUpdatedon(new Date());
						 			payDetailsList.add(deductionDetails);
								}
						 		new PayrollTaxDetailsService().persist(payDetailsList);
				 			}
			 			}else{
			 				employeePayrollDO = payrollList.get(0);
			 				employeePayrollDO.setStatus('d');
					 		new EmployeePayrollService().update(employeePayrollDO);
			 			}
			 		}
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Run Payroll");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/retrieveByAllEmp/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByAllEmp(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PAYROLLMONTH).toString().isEmpty() &&
						!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
					
				List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retrieveByAllEmp(inputJSON.get(CommonConstants.PAYROLLMONTH).toString(),
						Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()), (char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByMonth/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty() && !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveByMonth(inputJSON.get(CommonConstants.MONTHLY).toString(), Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveByStatus(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()&& !inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
				List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveByStatus(inputJSON.get(CommonConstants.MONTHLY).toString(),  Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString()));
				respJSON = EmployeePayrollUtil.getempPayrollList(compensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<EmployeePayrollDO> empCompensationList = new EmployeePayrollService().retrieve();
				respJSON = EmployeePayrollUtil.getempPayrollList(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveSummary", method = RequestMethod.GET)
	public @ResponseBody String retrieveSummary(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Object[]> empCompensationList = new EmployeePayrollService().retrieveForSummary();
				respJSON = EmployeePayrollUtil.ObjectArray(empCompensationList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			List<EmployeePayrollDO> empPayrollList = new EmployeePayrollService().retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			if(empPayrollList != null && empPayrollList.size() > 0){
			 				employeePayrollDO = empPayrollList.get(0);
			 				employeePayrollDO.setStatus('p');
			 				employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
			 				new EmployeePayrollService().update(employeePayrollDO);
			 			}
			 		}
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/decline", method = RequestMethod.POST)
	public @ResponseBody String decline(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 		if(!inputJSON.get(CommonConstants.IDS).toString().isEmpty()){
			 		JSONArray resultJSONArray = new JSONArray();
		 			resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			List<EmployeePayrollDO> empPayrollList = new EmployeePayrollService().retrieveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 			EmployeePayrollDO employeePayrollDO = new EmployeePayrollDO();
			 			if(empPayrollList != null && empPayrollList.size() > 0){
			 				employeePayrollDO = empPayrollList.get(0);
			 				employeePayrollDO.setStatus('v');
			 				employeePayrollDO.setUpdatedon(new Date());
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			employeePayrollDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			employeePayrollDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
			 				new EmployeePayrollService().update(employeePayrollDO);
			 			}
			 		}
		 		}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retriveForReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForReportMonth(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.SingleMonthDeductionReport(compensationList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthDeductionReport(compensationList, fromMonth, toMonth,CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveForTaxReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForTaxReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.SingleMonthTaxReport(compensationList, CommonConstants.MONTHLYC, inputJSON.get(CommonConstants.MONTHLY).toString()).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.MultipleMonthTaxReport(compensationList, fromMonth, toMonth,CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveForYtdReport/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveForYtdReport(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.MONTHLY).toString().isEmpty()){
					Long batchId = null;
					if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
						batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
						if(batchId == 0){
							batchId = null;
						}
					}
					List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportMonth(inputJSON.get(CommonConstants.MONTHLY).toString(),
						batchId);
					if(compensationList != null && compensationList.size() > 0){
						respJSON = EmployeePayrollUtil.YtdReport(compensationList, inputJSON.get(CommonConstants.MONTHLY).toString(), CommonConstants.MONTHLYC).toString();
					}
				}else{
					if(inputJSON != null && !inputJSON.get(CommonConstants.QUARTERLY).toString().isEmpty()){
						String fromMonth = null;
						String toMonth = null;
						Long batchId = null;
						List<FyDO> fyList = new ArrayList<FyDO>();
						if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
							fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
							if(fyList != null && fyList.size() > 0){
								if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
									batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
									if(batchId == 0){
										batchId = null;
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q1")){
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Jun"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q2")){
									fromMonth = "Jul"+" "+fyList.get(0).getFromyear();
									toMonth = "Sep"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								
								
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q3")){
									fromMonth = "Oct"+" "+fyList.get(0).getFromyear();
									toMonth = "Dec"+" "+fyList.get(0).getFromyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
								if(inputJSON.get(CommonConstants.QUARTERLY).toString().equalsIgnoreCase("Q4")){
									fromMonth = "Jan"+" "+fyList.get(0).getToyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.QUARTERLYC).toString();
									}
								}
							}
							
						}
					}else{
						if(inputJSON != null && !inputJSON.get(CommonConstants.FREQUENCY).toString().isEmpty()
								&& inputJSON.get(CommonConstants.FREQUENCY).toString().equalsIgnoreCase("Y")){
							String fromMonth = null;
							String toMonth = null;
							Long batchId = null;
							List<FyDO> fyList = new ArrayList<FyDO>();
							if(!inputJSON.get(CommonConstants.FYID).toString().isEmpty()){
								fyList = new FyService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.FYID).toString()));
								if(fyList != null && fyList.size() > 0){
									if(!inputJSON.get(CommonConstants.PAYROLLBATCHID).toString().isEmpty()){
										batchId = Long.parseLong(inputJSON.get(CommonConstants.PAYROLLBATCHID).toString());
										if(batchId == 0){
											batchId = null;
										}
									}
									fromMonth = "Apr"+" "+fyList.get(0).getFromyear();
									toMonth = "Mar"+" "+fyList.get(0).getToyear(); 
									List<EmployeePayrollDO> compensationList = new EmployeePayrollService().retriveForReportQuarter(fromMonth, toMonth,	batchId);
									if(compensationList != null && compensationList.size() > 0){
										respJSON = EmployeePayrollUtil.YtdForMultipleMonth(compensationList, fromMonth, toMonth, CommonConstants.YEARLYC).toString();
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
