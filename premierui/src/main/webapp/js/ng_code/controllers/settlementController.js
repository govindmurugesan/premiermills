var app = angular.module('spheresuite')
app.controller('settlementController', settlementController);

settlementController.$inject = ['$scope', '$rootScope', '$location'];

function settlementController($scope, $rootScope, $location) {
    var settlementControllerScope = this;
    
    $rootScope.headerMenu = "Settlements";
    
    settlementControllerScope.viewSettlement = viewSettlement;
    
    
    function viewSettlement(){
    	 $location.path('/viewSettlement');
    }
}