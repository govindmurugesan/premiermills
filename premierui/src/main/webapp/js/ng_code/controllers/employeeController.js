var app = angular.module('spheresuite')
app.controller('employeeController', employeeController);

employeeController.$inject = ['$scope', '$rootScope', '$filter', '$location', '$localStorage', 'employeeService', 'userService', 'configurationService', 'commonService', 'Upload', '$routeParams', '$route'];

function employeeController($scope, $rootScope, $filter, $location, $localStorage, employeeService, userService, configurationService, commonService, Upload, $routeParams, $route) {
    var employeeControllerScope = this;

    if ($location.path() == '/myinfo') {
        $rootScope.headerMenu = "My Info";
    }  
    
    else{
        $rootScope.headerMenu = "Employees";
    }

    $scope.format = "MMM dd, yyyy";
    $scope.format1 = "MMM YYYY";
    $scope.arrowPosition = false;
    $scope.arrowPosition1 = false;
    $scope.arrowPosition2 = false;
    $scope.arrowPosition3 = false;
    $scope.arrowPosition4 = false;
    $scope.arrowPosition5 = false;
    $scope.arrowPosition6 = false;
    $scope.arrowPosition7 = false;
    $scope.arrowPosition8 = false;
    $scope.arrowPosition9 = false;
    $scope.arrowPosition10 = false;
    $scope.arrowPosition11 = false;
    $scope.arrowPosition12 = false;
    $scope.arrowPosition13 = false;
    $scope.arrowPosition14 = false;
    $scope.arrowPosition15 = false;
    $scope.arrowPosition16 = false;

    employeeControllerScope.selectShowDays = false;
    employeeControllerScope.showDays = showDays
    employeeControllerScope.arrowRotate = arrowRotate;
    employeeControllerScope.arrowRotate1 = arrowRotate1;
    employeeControllerScope.arrowRotate2 = arrowRotate2;
    employeeControllerScope.arrowRotate3 = arrowRotate3;
    employeeControllerScope.arrowRotate4 = arrowRotate4;
    employeeControllerScope.arrowRotate5 = arrowRotate5;
    employeeControllerScope.arrowRotate6 = arrowRotate6;
    employeeControllerScope.arrowRotate7 = arrowRotate7;
    employeeControllerScope.arrowRotate8 = arrowRotate8;
    employeeControllerScope.arrowRotate9 = arrowRotate9;
    employeeControllerScope.arrowRotate10 = arrowRotate10;
    employeeControllerScope.arrowRotate11 = arrowRotate11;
    employeeControllerScope.arrowRotate12 = arrowRotate12;
    employeeControllerScope.arrowRotate13 = arrowRotate13;
    employeeControllerScope.arrowRotate14 = arrowRotate14;
    employeeControllerScope.arrowRotate15 = arrowRotate15;
    employeeControllerScope.arrowRotate16 = arrowRotate16;
    employeeControllerScope.dummyItems = [];
    employeeControllerScope.addMoreItems = addMoreItems;
    employeeControllerScope.limitToShow = 5;
    employeeControllerScope.addMoreItems = addMoreItems;
    employeeControllerScope.limitToShowAllowance = 5;
    employeeControllerScope.allowanceList = [];
    employeeControllerScope.limitToShowDeduction = 5;
    employeeControllerScope.deductionList = [];

    employeeControllerScope.dateOfBirthCalender = false;
    employeeControllerScope.issueCalender = false;
    employeeControllerScope.expiryCalender = false;
    employeeControllerScope.terminateCalender = false;
    employeeControllerScope.terminateResignCalender = false;
	employeeControllerScope.icon = false;
    employeeControllerScope.employee;
    employeeControllerScope.employeeActiveList;
    employeeControllerScope.batchActiveList;
    employeeControllerScope.employeeForm;
    employeeControllerScope.compensation;
    employeeControllerScope.compensationForm;
    employeeControllerScope.employeeList = [];
    employeeControllerScope.employeeSkillsList = [];
    employeeControllerScope.items;
    employeeControllerScope.msg;
    employeeControllerScope.pager = {};
    employeeControllerScope.searchName;
    employeeControllerScope.startCalender = false;
    employeeControllerScope.employeeSearchList = [];
    employeeControllerScope.isDataAvailable = false;
    employeeControllerScope.isUpdate = false;
    employeeControllerScope.isSearchResultFound = false;
    employeeControllerScope.chooseFileSection = true;
    employeeControllerScope.employeeSelected;
    employeeControllerScope.stateList;
    employeeControllerScope.countryList;
    employeeControllerScope.educationLevelList;
    employeeControllerScope.empEducationDetailList;
    employeeControllerScope.empFamilyDetailsList;
    employeeControllerScope.addressTypeLevelList;
    employeeControllerScope.employeeAddressList;
    employeeControllerScope.employeeBankDetailsList;
    employeeControllerScope.terminateReasonList;
    employeeControllerScope.employeeTerminateDetailsList;
    employeeControllerScope.employeeCertificateDetailsList;
    employeeControllerScope.employeeAdditionalInfoDetailsList;
    employeeControllerScope.employeeUserInformationDetailsList;
    employeeControllerScope.uploadFileList; 
    employeeControllerScope.employeeDriverLicenseDetailsList;
    employeeControllerScope.employeeCompensationDetailsList;
    employeeControllerScope.employeeContactDetailsList;
    employeeControllerScope.empIdAcceptPopup;
    
    employeeControllerScope.addAllowance = addAllowance;
    employeeControllerScope.addNew = addNew;
    employeeControllerScope.goToEditEmployee = goToEditEmployee;
    employeeControllerScope.addDeduction = addDeduction;
    employeeControllerScope.addEmployee = addEmployee;
    employeeControllerScope.addEmployeeCompensation = addEmployeeCompensation;
    employeeControllerScope.clearimage =clearimage;
    employeeControllerScope.decline = decline;
    employeeControllerScope.declineCompensation = declineCompensation;
    employeeControllerScope.editEmployee = editEmployee;
    employeeControllerScope.exportData = exportData;
    employeeControllerScope.getEmployee = getEmployee;
    employeeControllerScope.getEmployeeCompensation = getEmployeeCompensation;
    employeeControllerScope.getEmployeeId = getEmployeeId;
    employeeControllerScope.getEmployeeStatus = getEmployeeStatus;
    employeeControllerScope.getEmployeeUser = getEmployeeUser;
    employeeControllerScope.getDepartmentStatus = getDepartmentStatus;
    employeeControllerScope.getWorkLocationStatus = getWorkLocationStatus;
    employeeControllerScope.goBackToEmpoyee = goBackToEmpoyee;
    employeeControllerScope.goBackToEmpoyeeCompensation = goBackToEmpoyeeCompensation;
    employeeControllerScope.updateCompensationDate = updateCompensationDate;
    employeeControllerScope.updatePic = updatePic;
    employeeControllerScope.updateProfilePic = updateProfilePic;
    employeeControllerScope.viewEmployee = viewEmployee;
    employeeControllerScope.editEmployeeCompensation = editEmployeeCompensation;
    employeeControllerScope.updateCompensationCtc = updateCompensationCtc;
    employeeControllerScope.viewEmployeeCompensation = viewEmployeeCompensation;
    employeeControllerScope.opendateOfBirthCalender = opendateOfBirthCalender;
    employeeControllerScope.openImportPage = openImportPage;
    employeeControllerScope.openstartCalender = openstartCalender;
    employeeControllerScope.issueDateCalender = issueDateCalender;
    employeeControllerScope.expiryDateCalender = expiryDateCalender;
    employeeControllerScope.terminateDateCalender = terminateDateCalender;
    employeeControllerScope.importEmployee = importEmployee;
    employeeControllerScope.searchByName = searchByName;
    employeeControllerScope.showSelectedEmployee = showSelectedEmployee;
    employeeControllerScope.getEmpSkills = getEmpSkills;
    employeeControllerScope.addSkills = addSkills;
    employeeControllerScope.getState = getState;
    employeeControllerScope.getCountry = getCountry;
    employeeControllerScope.getEducationLevel = getEducationLevel;
    employeeControllerScope.sendEmpEducation = sendEmpEducation;
    employeeControllerScope.getActiveEducationLevel = getActiveEducationLevel;
    employeeControllerScope.getEmployeeEducationDetails = getEmployeeEducationDetails;
    employeeControllerScope.empFamilyDetails = empFamilyDetails;
    employeeControllerScope.getEmpFamilyDetails = getEmpFamilyDetails;
    employeeControllerScope.goToAddFamilyDetails = goToAddFamilyDetails;
    employeeControllerScope.goToEditFamilyDetails = goToEditFamilyDetails;
    employeeControllerScope.gotoEditTerminateDetails = gotoEditTerminateDetails;
    employeeControllerScope.updateFamilyDetails = updateFamilyDetails;
    employeeControllerScope.gotoEditEmpEducation  = gotoEditEmpEducation;
    employeeControllerScope.updateEmpEducation = updateEmpEducation;
    employeeControllerScope.gotoAddEmpEducation = gotoAddEmpEducation;
    employeeControllerScope.gotoEditSkills = gotoEditSkills;
    employeeControllerScope.gotoAddEmpSkills = gotoAddEmpSkills;
    employeeControllerScope.updateEmpSkills = updateEmpSkills;
    employeeControllerScope.getAddressType = getAddressType;
    employeeControllerScope.addEmpAddress = addEmpAddress;
    employeeControllerScope.getEmpAddress = getEmpAddress;
    employeeControllerScope.goToEditEmpAddress = goToEditEmpAddress;
    employeeControllerScope.updateEmpAddress = updateEmpAddress;
    employeeControllerScope.gotoAddEmpAddress = gotoAddEmpAddress;
    employeeControllerScope.addEmpBankDetails = addEmpBankDetails;
    employeeControllerScope.getEmpBankDetails = getEmpBankDetails;
    employeeControllerScope.goToEditBankDetails = goToEditBankDetails;
    employeeControllerScope.gotoAddEmpBankDetails = gotoAddEmpBankDetails;
    employeeControllerScope.updateEmpBankDetails = updateEmpBankDetails;
    employeeControllerScope.getTerminateReason = getTerminateReason;
    employeeControllerScope.addEmpTerminate = addEmpTerminate;
    employeeControllerScope.getEmpTerminateDetails = getEmpTerminateDetails;
    employeeControllerScope.addEmpCertification = addEmpCertification;
    employeeControllerScope.getEmpCertificateDetails = getEmpCertificateDetails;
    employeeControllerScope.hidePayroll = hidePayroll;
    employeeControllerScope.gotoEditEmpCetificate = gotoEditEmpCetificate;
    employeeControllerScope.gotoAddEmpCertificate = gotoAddEmpCertificate;
    employeeControllerScope.updateEmpCertification = updateEmpCertification;
    employeeControllerScope.addEmpAdditionalInformation = addEmpAdditionalInformation;
    employeeControllerScope.gotoAddEmpAdditionalInfo = gotoAddEmpAdditionalInfo;
    employeeControllerScope.getEmpAdditionalInfoDetails = getEmpAdditionalInfoDetails;
    employeeControllerScope.UpdateAdditionalInfo = UpdateAdditionalInfo;
    employeeControllerScope.gotoEditEmpAdditionalInfo = gotoEditEmpAdditionalInfo;
    employeeControllerScope.getUserInformation = getUserInformation;
    employeeControllerScope.addUploadedFile = addUploadedFile;
    employeeControllerScope.getEmpFileUpload = getEmpFileUpload;
    employeeControllerScope.addEmpDriversLicense = addEmpDriversLicense;
    employeeControllerScope.getEmpDriverLicenseDetails = getEmpDriverLicenseDetails;
    employeeControllerScope.updateEmpDriverLicenseDetails = updateEmpDriverLicenseDetails;
    employeeControllerScope.gotoAddEmpDriverLicense = gotoAddEmpDriverLicense;
    employeeControllerScope.goToEditEmpDriverLicense = goToEditEmpDriverLicense;
    employeeControllerScope.getEmpCompensation = getEmpCompensation;
    employeeControllerScope.addEmpContactDetails = addEmpContactDetails;
    employeeControllerScope.updateEmpContactDetails = updateEmpContactDetails;
    employeeControllerScope.goToEditContactDetails = goToEditContactDetails;
    employeeControllerScope.gotoAddEmpContactDetails = gotoAddEmpContactDetails;
    employeeControllerScope.getEmpContactInformationDetails = getEmpContactInformationDetails;
    employeeControllerScope.updateEmpContactDetails = updateEmpContactDetails;
    employeeControllerScope.getUserInformationMyInfo = getUserInformationMyInfo;
    employeeControllerScope.familyInfoDecline = familyInfoDecline;
    employeeControllerScope.getRemainingEmployees = getRemainingEmployees;
    employeeControllerScope.sendOnboardRequest = sendOnboardRequest;
    employeeControllerScope.getEmployeeOnboardById = getEmployeeOnboardById;
    employeeControllerScope.updateEmployeeOnboard = updateEmployeeOnboard;
    employeeControllerScope.getEmployeeOnboard = getEmployeeOnboard;
    employeeControllerScope.getEmployeeOnboard = getEmployeeOnboard;
    employeeControllerScope.rejectPopup = rejectPopup;
    employeeControllerScope.empOnboardData = {};
    employeeControllerScope.updateRejectOnboard = updateRejectOnboard;
    employeeControllerScope.onboardRejectDecline = onboardRejectDecline;
    employeeControllerScope.addEmployeeonBoard = addEmployeeonBoard;
    employeeControllerScope.updateAcceptOnboard = updateAcceptOnboard;
    employeeControllerScope.rejectStatus = false;
    employeeControllerScope.acceptPopup = acceptPopup;
    employeeControllerScope.getEmployeeAssignment = getEmployeeAssignment;
    employeeControllerScope.gotoViewProject = gotoViewProject;
    employeeControllerScope.terminateResignDateCalender = terminateResignDateCalender;
    employeeControllerScope.updateTerminateDetails = updateTerminateDetails;
    employeeControllerScope.gotoCancelTerminateDetails = gotoCancelTerminateDetails;
    
    employeeControllerScope.isDisabled = true;
    
    if ($location.path().indexOf('/employee/onboardform') != -1){
    	$rootScope.headerMenu = "";
    	if($routeParams.onboardId){
    		getEmployeeOnboardById($routeParams.onboardId);    		
    	}
    }
    
     function showDays(){
    	 employeeControllerScope.selectShowDays = true;
     }
    function hidePayroll() {
        employeeControllerScope.isRunPayroll = false;
    }
    
    function getEmployeeOnboardById(data) {
    	employeeControllerScope.spinner = true;
    	employeeControllerScope.employeeonboard = {};
        employeeService.getEmployeeOnboardById(data).then(function(res) {
        	console.log('onboarddd   ', data)
            if(res.successflag === 'true' && res.results.length > 0){
            	employeeControllerScope.employeeOnboard = res.results[0];
            	employeeControllerScope.employeeonboard.personalEmail = employeeControllerScope.employeeOnboard.personalEmail;
            	employeeControllerScope.employeeonboard.personalContact = employeeControllerScope.employeeOnboard.personalContact;
            	$localStorage.spheresuite.employeeOnboardId = employeeControllerScope.employeeOnboard.id;
            } 
        	employeeControllerScope.spinner = false;
	    }, function(err) { 
	    	employeeControllerScope.spinner = false;
	    });
    }
    
   function getEmployeeId() {  
        employeeService.getEmployeeId().then(function(res) { 
            employeeControllerScope.employee = { empId :  res.results }; 
	    }, function(err) { 
	    });
    } 

    function goToAddFamilyDetails(){
        employeeControllerScope.saveShow = true;
        employeeControllerScope.saveUpdate = false;
    } 
    
    
	function editEmployeeCompensation(employee , type){
		if(type && employee){
		employeeControllerScope.compensation = angular.copy(employee);
		if(type == 'ctc')
			employeeControllerScope.isDisabled = false;
		else if(type == 'date')
			employeeControllerScope.isDisabled = true;
		}
	}
	
    function addMoreItems() {
        /*employeeControllerScope.spinner = true;*/
        employeeControllerScope.limitToShow += 5;
//        $rootScope.limitToShow += 5;
        /*employeeControllerScope.spinner = false;*/
    }

    employeeControllerScope.format = "MMM DD YYYY"; 
    employeeControllerScope.saveShow = true;
    employeeControllerScope.saveupdate = false;
    employeeControllerScope.isUpdate = false;

    function addNew() {
    	employeeControllerScope.optionsShow = false;
    	employeeControllerScope.saveShow = true;
    	employeeControllerScope.saveupdate = false;
    }

    
    if($location.path() == '/employee/add'){
    	getEmployeeId();
        getEmployee();
    }
    
    if($location.path() == '/employee/onboard'){
    	getEmployeeId();
    	getEmployee();
    	getWorkLocationStatus();
        getDepartmentStatus();
        getEmployeeStatus();
        getEmployeeOnboard();
    }
    
    if ($location.path() == '/myinfo') {
    	getEmployeeUser();
        getEmployee($localStorage.spheresuite.id);
        getEmpSkills($localStorage.spheresuite.id);
        getCountry();
        getActiveEducationLevel();
        getEmployeeEducationDetails($localStorage.spheresuite.id);
        getEmpFamilyDetails($localStorage.spheresuite.id);
        getAddressType();
        getEmpAddress($localStorage.spheresuite.id);
        getEmpBankDetails($localStorage.spheresuite.id);
        getTerminateReason();
        getEmpTerminateDetails($localStorage.spheresuite.id);
        getEmpCertificateDetails($localStorage.spheresuite.id);
        getEmpAdditionalInfoDetails($localStorage.spheresuite.id);
        getUserInformationMyInfo($localStorage.spheresuite.id);
        getEmpFileUpload($localStorage.spheresuite.id);
        getEmpDriverLicenseDetails($localStorage.spheresuite.id);
        getEmpCompensation($localStorage.spheresuite.id);
        getEmpContactInformationDetails($localStorage.spheresuite.id);
        getEmployeeAssignment($localStorage.spheresuite.viewEmployeeId);
    } else if ($localStorage.spheresuite && $localStorage.spheresuite.id && $localStorage.spheresuite.id != '' && $location.path() != '/employee/view' && $location.path() != '/employees' &&  $location.path() != '/employee/directory') {
        getWorkLocationStatus();
        getDepartmentStatus();
        getEmployeeStatus();
        /*getEmployee();*/
    } else if ($location.path() == '/employees') {
        getEmployeeStatus();
    }

    if ($location.path() == '/employee/view' || $location.path() == '/employee/edit') {
        getEmployee($localStorage.spheresuite.viewEmployeeId);
        getEmpSkills($localStorage.spheresuite.viewEmployeeId);
        getCountry();
        getActiveEducationLevel();
        getEmployeeEducationDetails($localStorage.spheresuite.viewEmployeeId);
        getEmpFamilyDetails($localStorage.spheresuite.viewEmployeeId);
        getAddressType();
        getEmpAddress($localStorage.spheresuite.viewEmployeeId);
        getEmpBankDetails($localStorage.spheresuite.viewEmployeeId);
        getTerminateReason();
        getEmpTerminateDetails($localStorage.spheresuite.viewEmployeeId);
        getEmpCertificateDetails($localStorage.spheresuite.viewEmployeeId);
        getEmpAdditionalInfoDetails($localStorage.spheresuite.viewEmployeeId);
        getUserInformation($localStorage.spheresuite.viewEmployeeId);
        getEmpFileUpload($localStorage.spheresuite.viewEmployeeId);
        getEmpDriverLicenseDetails($localStorage.spheresuite.viewEmployeeId);
        getEmpCompensation($localStorage.spheresuite.viewEmployeeId);
        getEmpContactInformationDetails($localStorage.spheresuite.viewEmployeeId);
        getEmployeeAssignment($localStorage.spheresuite.viewEmployeeId);


        if ($location.path() == '/employee/edit') {
            employeeControllerScope.isUpdate = true;
            getEmployee($localStorage.spheresuite.viewEmployeeId);
            getEmployee1();
        }
    }  else if($location.path() == '/employees') {
        delete $localStorage.spheresuite['viewEmployeeId'];
        getEmployee();

    }
    
    
    
    if ($location.path() == '/employeeCompensation/view') { 
    	getEmployeeCompensation($localStorage.spheresuite.viewEmployeecomId); 
    	getActiveBatch();
    }  else if($location.path() == '/employee/ctc') {
        delete $localStorage.spheresuite['viewEmployeeId'];
        getEmployeeCompensation();
        //getActiveUser();
        getActiveBatch();
        getRemainingEmployees();
    } else if($location.path() == '/employee/directory'){
    	console.log('asdasd')
        getActiveUser();
    	getActiveBatch();
    }
//    getActiveUser();

    if ($localStorage.spheresuite && $localStorage.spheresuite.name != "" && $localStorage.spheresuite.id != "" &&  $location.path() != '/employee/add' &&  $location.path() != '/employee/directory'){
    	employeeControllerScope.userName = $localStorage.spheresuite.name;
    	employeeControllerScope.icon = false;
	    	employeeService.getEmployee($localStorage.spheresuite.id).then(function(res) { 
	            if (res.successflag === 'true' && res.results.length > 0 && res.results[0].photo != '') {
	            	employeeControllerScope.photo = res.results[0].photo;
	                } else {
	                	employeeControllerScope.icon = true;
	                }
	             
	        }, function(err) {
	        	employeeControllerScope.icon = true;
	        });
    }
    
    function addAllowance(){	
    }

    function addSkills(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.empSkill) {
            employeeControllerScope.empSkill.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empSkill.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addSkills(employeeControllerScope.empSkill).then(function(res) {
                console.log('addSkills', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpSkills($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empSkill= {};
                    $('#skillsModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }        
    }
    
    function addEmployee() {
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.employee) {
            commonService.formValNotManditory(employeeControllerScope.employeeForm, employeeControllerScope.employee).then(function(data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    employeeService.addEmployee(data).then(function(res) {
                        if (res.successflag === 'true') {
                            employeeControllerScope.spinner = false;
                            $('#employeeOnboardAccept').modal('hide');
                            $location.path('/employees');

                        } else {
                            employeeControllerScope.spinner = false;
                            employeeControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        employeeControllerScope.spinner = false;
                    });
                } else {
                    employeeControllerScope.spinner = false;
                }
            });
        } else
            employeeControllerScope.spinner = false;
    }
    
    function addEmployeeonBoard() {
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.employee) {
            commonService.formValNotManditory(employeeControllerScope.employeeForm, employeeControllerScope.employee).then(function(data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    console.log('entering123', data);
                    employeeService.addEmployee(data).then(function(res) {
                        if (res.successflag === 'true') {
                        	updateAcceptOnboard(data);
                            employeeControllerScope.spinner = false;
                            $location.path('/employees');

                        } else {
                            employeeControllerScope.spinner = false;
                            employeeControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        employeeControllerScope.spinner = false;
                    });
                } else {
                    employeeControllerScope.spinner = false;
                }
            });
        } else
            employeeControllerScope.spinner = false;
    }
 
    function addEmployeeCompensation() {
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.compensation) {
            commonService.formValNotManditory(employeeControllerScope.compensationForm, employeeControllerScope.compensation).then(function(data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    employeeService.addEmployeeCompensation(data).then(function(res) {
                    	console.log("empcompensation",res)
                        if (res.successflag === 'true') {
                            employeeControllerScope.spinner = false; 
                            $("#employeeComp").modal('hide');
                            declineCompensation();
                            getEmployeeCompensation();
                        } else if(res.successflag === 'false') {
                        	employeeControllerScope.empCompansation = res.errors; 
                            employeeControllerScope.spinner = false;
                        } else {
                            employeeControllerScope.spinner = false;
                            employeeControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        employeeControllerScope.spinner = false;
                    });
                } else {
                    employeeControllerScope.spinner = false;
                }
            });
        } else
            employeeControllerScope.spinner = false;
    }
    
    function decline() {
        if ($location.path() === '/employee/edit') {
            $location.path('/employee/view');
        } else {
        	employeeControllerScope.employee = '';
            employeeControllerScope.empDriverLicense = null;
            employeeControllerScope.driversLicenseForm.$setPristine();
            employeeControllerScope.driversLicenseForm.$setUntouched();
            employeeControllerScope.isUpdate = false;
            
        }
    }
    
    function onboardRejectDecline() {
    	employeeControllerScope.employeeonboardReject = null;
        employeeControllerScope.employeeonboardRejectForm.$setPristine();
        employeeControllerScope.employeeonboardRejectForm.$setUntouched();
    }
  
    
    function declineCompensation() { 
    	getEmployeeCompensation($localStorage.spheresuite.viewEmployeecomId); 
    	console.log("employeeControllerScope decline", employeeControllerScope.compensation)
            employeeControllerScope.compensation = null; 

    	employeeControllerScope.empCompansation = null;
        employeeControllerScope.compensation = { empctc : ''};
            employeeControllerScope.compensationForm.$setPristine();
            employeeControllerScope.compensationForm.$setUntouched();
    }
    
    function editEmployee() {
        employeeControllerScope.spinner = true;
         delete employeeControllerScope.employee['photo'];
        if (employeeControllerScope.employee) {
        	commonService.formValNotManditory(employeeControllerScope.employeeForm, employeeControllerScope.employee).then(function(data) {
            employeeControllerScope.employee.updatedBy = $localStorage.spheresuite.id;
            employeeService.editEmployee(employeeControllerScope.employee).then(function(res) {
                if (res.successflag === 'true') {
                    $location.path('/employee/view');
                    employeeControllerScope.spinner = false;
                } else
                employeeControllerScope.spinner = false;
            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        });
        }
    }
    function updateCompensationCtc() {
    	console.log("new update compensation",employeeControllerScope.compensation)
        if (employeeControllerScope.compensation) {
            employeeControllerScope.spinner = true; 
        	commonService.formValNotManditory(employeeControllerScope.compensationForm, employeeControllerScope.compensation).then(function(data) {
                if(data){
		        	data.updatedBy = $localStorage.spheresuite.id;
		            employeeService.updateCompensation(data).then(function(res) {
		            	console.log('updateCompensationCtc res',res)
		                if (res.successflag === 'true') {
		                    employeeControllerScope.spinner = false;
		                    $("#employeeComp").modal('hide');
		                    getEmployeeCompensation($localStorage.spheresuite.viewEmployeecomId);  
		                } else
		                employeeControllerScope.spinner = false;
		            }, function(err) {
		                employeeControllerScope.spinner = false;
		            });
                }
            });
        }
    }
    
    function getActiveUser(){
        employeeControllerScope.spinner = true;
    	userService.getActiveUser().then(function(res){
    		console.log('getActiveUser res',res)
    		if(res.successflag == 'true' && res.results.length > 0){
    			employeeControllerScope.employeeActiveList = res.results;
    		}
            employeeControllerScope.spinner = false;
    	},function(err){
            employeeControllerScope.spinner = false;
    	});
    }

    function getRemainingEmployees(){
        employeeControllerScope.spinner = true;
        employeeService.getRemainingEmployees().then(function(res){
            console.log('getRemainingEmployees',res)
            if(res.successflag == 'true' && res.results.length > 0){
                employeeControllerScope.employeeActiveList = res.results;
            }
            employeeControllerScope.spinner = false;
        },function(err){
            employeeControllerScope.spinner = false;
        });
    }
    
    function getActiveBatch(){
        employeeControllerScope.spinner = true;
        configurationService.getActiveBatch().then(function(res){
    		console.log('getActiveBatch res',res)
    		if(res.successflag == 'true' && res.results.length > 0){
    			employeeControllerScope.batchActiveList = res.results;
    		}
            employeeControllerScope.spinner = false;
    	},function(err){
            employeeControllerScope.spinner = false;
    	});
    }

    function getEmployeeUser() {
        employeeControllerScope.spinner = true;
        var myUser = $localStorage.spheresuite.id;
        employeeService.getEmployee(myUser).then(function(res) {
        	console.log('getEmployee(myUser)', res)
               if (res.successflag === 'true' && res.results.length > 0) {
                    employeeControllerScope.noEmployeeDetail = true;
                    employeeControllerScope.userEmployeeList = res.results[0];
                    employeeControllerScope.spinner = false;
                }   else {
                    employeeControllerScope.spinner = false; 
                    employeeControllerScope.noEmployeeDetail = false; 
                    employeeControllerScope.dataMsg = "Information Not Avalible";
                }
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }

    
    function exportData() {
        $scope.fileName = "Employees";
        $scope.exportData = []; 
        $scope.exportData.push(["Id", "First Name", "Middle Name", "Last Name", "Emp Id", "Emp Type", "EmptypeName", "Start Date", "Gender", "Dept", "Dept Name", "Job Desc", "company Email", "Personal Email", "Personal Contact", "Primary Contact", "WorkLocation Id", "WorkLocation City", "Pan No", "Aadhar", "DateOfBirth", "Job Desc", "ReportTo", "Title", "Createdon", "CreatedBy", "Updatedon", "UpdatedBy"]);
        $scope.Filterdata = employeeControllerScope.employeeList;
        var firstFiter = $filter('filter')(employeeControllerScope.employeeList, { empType: employeeControllerScope.empType });
        $scope.Filterdata = $filter('filter')(firstFiter, employeeControllerScope.searchName);
        angular.forEach($scope.Filterdata, function(value, key) {
            $scope.exportData.push([value.id, value.firstName, value.middleName, value.lastName, value.empId, value.empType, value.emptypeName, value.startDate, value.gender, value.dept, value.deptName, value.jobDesc, value.companyEmail, value.personalEmail, value.personalContact, value.primaryContact, value.workLocation, value.workLocationCity, value.panno, value.aadhar, value.dateOfBirth, value.jobDesc, value.reportTo, value.title, value.createdon, value.createdBy, value.updatedon, value.updatedBy]);
        });
        function datenum(v, date1904) {
            if (date1904) v += 1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
        };

        function getSheet(data, opts) {
            var ws = {};
            var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
            for (var R = 0; R != data.length; ++R) {
                for (var C = 0; C != data[R].length; ++C) {
                    if (range.s.r > R) range.s.r = R;
                    if (range.s.c > C) range.s.c = C;
                    if (range.e.r < R) range.e.r = R;
                    if (range.e.c < C) range.e.c = C;
                    var cell = { v: data[R][C] };
                    if (cell.v == null) continue;
                    var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                    if (typeof cell.v === 'number') cell.t = 'n';
                    else if (typeof cell.v === 'boolean') cell.t = 'b';
                    else if (cell.v instanceof Date) {
                        cell.t = 'n';
                        cell.z = XLSX.SSF._table[14];
                        cell.v = datenum(cell.v);
                    } else cell.t = 's';

                    ws[cell_ref] = cell;
                }
            }
            if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
            return ws;
        };

        function Workbook() {
            if (!(this instanceof Workbook)) return new Workbook();
            this.SheetNames = [];
            this.Sheets = {};
        }

        var wb = new Workbook(),
            ws = getSheet($scope.exportData); 
        wb.SheetNames.push($scope.fileName);
        wb.Sheets[$scope.fileName] = ws;
        var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

        function s2ab(s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        }

        saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), $scope.fileName + '.xlsx');

    }
    
    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) { 
    	employeeControllerScope.viewempspinner = false;
    });

    
    function getEmployee(data) {
        employeeControllerScope.viewempspinner = true;
        employeeControllerScope.isDataAvailable = false;
        employeeService.getEmployee(data).then(function(res) {
        	console.log('get employee',res)
            	   if (res.successflag == 'true' && res.results.length > 0) {
                    if ($location.path() == '/employee/edit' || $location.path() == '/employee/directory' && data != undefined) { 
                        employeeControllerScope.employee = res.results[0]; 
                        employeeControllerScope.Directoryphoto = res.results[0].photo ;

                    	console.log(" employeeControllerScope.Directoryphoto111", employeeControllerScope.employee)
                        employeeControllerScope.viewempspinner = false;
                    }  else if ($location.path() == '/employee/view' ) {
                        employeeControllerScope.employee = res.results[0]; 
                        employeeControllerScope.viewempspinner = false;
                    }else {
                        employeeControllerScope.employeeListBackup = angular.copy(res.results);
                        employeeControllerScope.isDataAvailable = true;

                    	for(var i = 0; i < employeeControllerScope.employeeListBackup.length; i++){
                        	employeeControllerScope.employeeListBackup[i].isActiveClass = false;
                        	employeeControllerScope.employeeListBackup[i].isSelect = false;
                    	}
                        employeeControllerScope.employeeList = angular.copy(employeeControllerScope.employeeListBackup);
                        console.log('employeeControllerScope.employeeList', employeeControllerScope.employeeList)
                    	if((employeeControllerScope.buttonBeginFrom + employeeControllerScope.buttonLimitToShow) * employeeControllerScope.buttonLimitToShow >= employeeControllerScope.employeeList.length)
                    		employeeControllerScope.isNextDisabled = true;
                    	
                    	employeeControllerScope.employeeList[0].isActiveClass = true;
                        employeeControllerScope.viewempspinner = false;
                    }
                }  else {
                    employeeControllerScope.viewempspinner = false;
             		employeeControllerScope.dataMsg =  "Employee Not Available";
                }
        }, function(err) {
     		employeeControllerScope.dataMsg =  "Employee Not Available";
            employeeControllerScope.viewempspinner = false;
        });
    }

    function getEmployee1(data) {
        employeeControllerScope.viewempspinner = true;
        employeeControllerScope.isDataAvailable = false;
        employeeService.getEmployee(data).then(function(res) {
            console.log('get employee',res)
                   if (res.successflag == 'true' && res.results.length > 0) {
                    
                        employeeControllerScope.employeeListBackup = angular.copy(res.results);
                        employeeControllerScope.isDataAvailable = true;

                        for(var i = 0; i < employeeControllerScope.employeeListBackup.length; i++){
                            employeeControllerScope.employeeListBackup[i].isActiveClass = false;
                            employeeControllerScope.employeeListBackup[i].isSelect = false;
                        }
                        employeeControllerScope.employeeList = angular.copy(employeeControllerScope.employeeListBackup);
                        console.log('employeeControllerScope.employeeList', employeeControllerScope.employeeList)
                                          
                }  else {
                    employeeControllerScope.viewempspinner = false;
                    employeeControllerScope.dataMsg =  "Employee Not Available";
                }
        }, function(err) {
            employeeControllerScope.dataMsg =  "Employee Not Available";
            employeeControllerScope.viewempspinner = false;
        });
    }
 
    function getEmployeeCompensation(data) {
    	employeeControllerScope.dataMsgCompensation = '';
        employeeControllerScope.viewempspinner = true;
        employeeControllerScope.isDataAvailable = true;
        employeeService.getEmployeeCompensation(data).then(function(res) {
        	console.log('getEmployeeCompensation',res)
        	   if (res.successflag == 'true' && res.results.length > 0) {
                 employeeControllerScope.isDataAvailable = true;
        		   if(data){
                    employeeControllerScope.employeeCompensation = res.results; 
                    employeeControllerScope.viewempspinner = false;
                } else {

                    employeeControllerScope.employeeCompensationListBackup = angular.copy(res.results);
                   

                	for(var i = 0; i < employeeControllerScope.employeeCompensationListBackup.length; i++){
                    	employeeControllerScope.employeeCompensationListBackup[i].isActiveClass = false;
                    	employeeControllerScope.employeeCompensationListBackup[i].isSelect = false;
                	}
                    employeeControllerScope.employeeCompensationList = angular.copy(employeeControllerScope.employeeCompensationListBackup);
                	if((employeeControllerScope.buttonBeginFrom + employeeControllerScope.buttonLimitToShow) * employeeControllerScope.buttonLimitToShow >= employeeControllerScope.employeeCompensationList.length)
                		employeeControllerScope.isNextDisabled = true;
                	
                	employeeControllerScope.employeeCompensationList[0].isActiveClass = true;
                	
                	
//                    employeeControllerScope.employeeCompensationList = res.results;
//                    console.log("two data",employeeControllerScope.employeeCompensationList)
                    employeeControllerScope.viewempspinner = false;
                }
            }  else { 
                employeeControllerScope.viewempspinner = false;
                employeeControllerScope.isDataAvailable = false;
         		employeeControllerScope.dataMsgCompensation =  "Employee Compensation Not Available";
            }
        }, function(err) {
     		employeeControllerScope.dataMsgCompensation =  "Employee Compensation Not Available";
            employeeControllerScope.viewempspinner = false;
        });
    }
    
    function getWorkLocationStatus(data) {
        employeeControllerScope.viewempspinner = true;
        configurationService.getWorkLocationByStatus(data).then(function(res) {
            if (res.successflag === 'true' && res.results.length > 0) {
                employeeControllerScope.worklocationList = res.results;
            }
            employeeControllerScope.spinner = false;
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }

    function goBackToEmpoyee() {
        $location.path('/employees');
    }

    function goBackToEmpoyeeCompensation() {
        $location.path('/employee/ctc');
    }
    
    function getDepartmentStatus(data) {
        employeeControllerScope.spinner = true;
        configurationService.getDepartmentByStatus(data).then(function(res) {
            if (res.successflag === 'true') {
                if (res.successflag && res.results.length > 0) {
                    employeeControllerScope.DepartmentList = res.results;
                }
                employeeControllerScope.spinner = false;
            }
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }

    function getEmployeeStatus(data) {
        employeeControllerScope.spinner = true;
        configurationService.getEmployeeByStatus(data).then(function(res) {
            if (res.successflag === 'true') {
                employeeControllerScope.EmployeeTypeList = res.results;
            }
            employeeControllerScope.spinner = false;
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }
    
    function updateCompensationDate(comp){
    	console.log('comp',comp)
    	if(comp && employeeControllerScope.compensationForm){
        	console.log("new update compensation",employeeControllerScope.compensation)
            employeeControllerScope.spinner = true; 
            	commonService.formValNotManditory(employeeControllerScope.compensationForm, comp).then(function(data) {
                    if(data){
		            	data.updatedBy = $localStorage.spheresuite.id;
		            	console.log(data)
		                employeeService.updateCompensationDate(data).then(function(res) {
		                    if (res.successflag === 'true') {
		                        employeeControllerScope.spinner = false;
		                        $("#employeeComp").modal('hide');
		                        getEmployeeCompensation($localStorage.spheresuite.viewEmployeecomId);  
		                    } else
		                    employeeControllerScope.spinner = false;
		                }, function(err) {
		                    employeeControllerScope.spinner = false;
		                });
		            }
              });
    	}
    }
    
    function updatePic(myCroppedImage) {
    	if(myCroppedImage)
    		employeeControllerScope.icon = false;
        if (myCroppedImage) {
            employeeControllerScope.spinner = true;  
                var data = {
                    id: $localStorage.spheresuite.id,
                    photo: myCroppedImage
                } 
                $('#modal').modal('hide');
                clearimage();
                  employeeService.updateProfilePic(data).then(function(res) {
                    if (res) {
                        getEmployeeUser();
                        employeeService.getEmployee($localStorage.spheresuite.id).then(function(res) { 
            	            if (res.successflag === 'true') {
            	            	employeeControllerScope.photo = res.results[0].photo;
            	                }
            	            else{
            	            	employeeControllerScope.photo = 'images/User.png'
            	            }
            	        }, function(err) {
            	            employeeControllerScope.photo = 'images/User.png'
            	        });
                        
                    }
                    employeeControllerScope.spinner = false;
                }, function(err) {
                    employeeControllerScope.spinner = false;
                });
            
        }
    }

    function updateProfilePic() {
        if (employeeControllerScope.userEmployeeList.photo && $localStorage.spheresuite && $localStorage.spheresuite.id != '') {
            employeeControllerScope.spinner = true;
            updatePic();
        }
    }

    function opendateOfBirthCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.dateOfBirthCalender = !employeeControllerScope.dateOfBirthCalender;
    };

    function openstartCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.startCalender = !employeeControllerScope.startCalender
    };
    function issueDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.issueCalender = !employeeControllerScope.issueCalender
    };
    function expiryDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.expiryCalender = !employeeControllerScope.expiryCalender
    };
    
    function terminateDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.terminateCalender = !employeeControllerScope.terminateCalender
    };

    function terminateResignDateCalender($event) {
        $event.preventDefault();
        $event.stopPropagation();
        employeeControllerScope.terminateResignCalender = !employeeControllerScope.terminateResignCalender
    };

    function viewEmployee(data) {
        if (data) {
            $localStorage.spheresuite.viewEmployeeId = data;            
            $location.path('/employee/view');
            getEmpSkills(data);
        }
    }

    function getEmpSkills(data) { 
        employeeControllerScope.spinner = true; 
        employeeService.getEmpSkills(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeSkillsList = res.results; 
                console.log('getEmpSkills', employeeControllerScope.employeeSkillsList); 
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
        });
    }

    function viewEmployeeCompensation(data) {
    	console.log("get com,p", data);
        if (data) {
            $localStorage.spheresuite.viewEmployeecomId = data; 
            $location.path('/employeeCompensation/view');
        }
    }

    function goToEditEmployee(employeeId) {
        if (employeeId) {
            $localStorage.spheresuite.viewEmployeeId = employeeId;
            $location.path('/employee/edit');
        }
    }
    
    function addDeduction(){
    	
    }

    function searchByName(empName) {
        employeeControllerScope.employeeSelected = '';
        if (empName && empName != '' && employeeControllerScope.employeeActiveList) {
            empName = empName.toLowerCase();
            employeeControllerScope.employeeSearchList = [];
            angular.forEach(employeeControllerScope.employeeActiveList, function(val) {
                if (val.firstName.toLowerCase().indexOf(empName) > -1 || val.middleName.toLowerCase().indexOf(empName) > -1 || val.lastName.toLowerCase().indexOf(empName) > -1) {
                    employeeControllerScope.employeeSearchList.push(val);
                }
            });
            employeeControllerScope.isSearchResultFound = true;
        } else {
            employeeControllerScope.employeeSearchList = [];
            employeeControllerScope.isSearchResultFound = false;
        }
    }

    function showSelectedEmployee(employee) {
        if (employee) {
        	console.log("get id employee",employee)
        	getEmployee(employee.id);
        	employeeControllerScope.employeeSelected = employee;
        	console.log(" employeeControllerScope.Directoryphoto", employeeControllerScope.Directoryphoto)
            employeeControllerScope.employeeSelected.photo =  employeeControllerScope.Directoryphoto;
            employeeControllerScope.isSearchResultFound = false;
            employeeControllerScope.searchName = employee.firstName + " " + employee.middleName;
            if (employee.middleName != "") {
                employeeControllerScope.searchName += " " + employee.lastName;
            } else {
                employeeControllerScope.searchName += employee.lastName;
            }
        }
    }
   

    var formdata = new FormData();
    $scope.getTheFiles = function($files) {
        angular.forEach($files, function(value, key) {
            formdata.append(key, value);
        });
    };


    $(".select1").select2();
    $(".test").select2();
    $("#select4").select2();

    function openImportPage() {

        $location.path('/employee/import');

    }

    var importedDataList;
    employeeControllerScope.declineImport = declineImport;

 	function declineImport(){
 		employeeControllerScope.feilds = null;
 		employeeControllerScope.employeeImportForm.$setPristine();
 		employeeControllerScope.employeeImportForm.$setUntouched();
 	}

    function importEmployee() {
    	employeeControllerScope.spinner = true;
        var dataToSend = {
            fileData: importedDataList,
            columFields: employeeControllerScope.feilds,
            updatedBy: $localStorage.spheresuite.id
        }
        console.log('dataToImport ',JSON.stringify(dataToSend));
        employeeService.importEmployee(dataToSend).then(function(res) {
        	employeeControllerScope.spinner = false;
        	if(res.successflag == 'true'){
        		declineImport();
        		$location.path('/employees');
        	}
        }, function(err) {
        	employeeControllerScope.spinner = false;
        });


    }

    $scope.selectedFile = null;
    $scope.loadFile = function(files) {
        $scope.selectedFile = files[0];
    }
    
    $scope.handleFile = function() {
        if ($scope.selectedFile) {
            var reader = new FileReader();
            var name = $scope.selectedFile.name;
            reader.onload = function(e) {
                var data = e.target.result; 
               
                var arr = String.fromCharCode.apply(null, new Uint8Array(data));
                var wb = XLSX.read(btoa(arr), { type: 'base64' });
                process_wb(wb);
            }; 
            reader.readAsArrayBuffer($scope.selectedFile);
        }


        function process_wb(wb) { 
        	console.log("wb",wb);
            var output = to_json(wb)
            console.log("output",output);
            var keySet;
            for (var fieldName in output){
                keySet = output[fieldName][0];
                importedDataList = output[fieldName];
                break;
            }
            employeeControllerScope.fileInputFeilds = [];
            console.log("keySet"+keySet);
            angular.forEach(keySet, function(value, key) {
            	//console.log("key",key);
                employeeControllerScope.fileInputFeilds.push(key)
            })
            $scope.$apply(function() {
                employeeControllerScope.chooseFileSection = false;
            })

        }

        function to_json(workbook) {
        	console.log("workbook",workbook)
            var result = {};
            workbook.SheetNames.forEach(function(sheetName) {
                var roa = XLS.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                console.log("roa",roa);
                if (roa.length > 0) {
                    result[sheetName] = roa;
                }
            });
            console.log("result",result)
            return result;
        }
 
    } 
   

    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
        	employeeControllerScope.myImage = evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

    employeeControllerScope.myImage='';
    employeeControllerScope.myCroppedImage='';
     function clearimage() {
    	 employeeControllerScope.myImage='';
    	 employeeControllerScope.myCroppedImage = ''; 
    	 $("#fileInput").val('');
    }
     
     
     $rootScope.limitToShow = 10;
     $rootScope.beginFrom = 0;
     
     employeeControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
     employeeControllerScope.buttonBeginFrom = 0;
 	employeeControllerScope.isNextDisabled = false;

     employeeControllerScope.deleteEmployee = deleteEmployee;
     employeeControllerScope.gotoPage = gotoPage;
 	employeeControllerScope.searchMe = searchMe;
 	employeeControllerScope.showPrevNav = showPrevNav;
     employeeControllerScope.showNextNav = showNextNav;
     employeeControllerScope.toggleSelect = toggleSelect;
     
     function deleteEmployee(){
    	 var filteredData = $filter('orderBy')(employeeControllerScope.employeeList, '-id');
      	filteredData = $filter('limitTo')(filteredData, $rootScope.limitToShow, $rootScope.beginFrom);
      	if(employeeControllerScope.search)
      		filteredData = $filter('filter')(filteredData, employeeControllerScope.search);
      	if(employeeControllerScope.searchName)
      		filteredData = $filter('filter')(filteredData, employeeControllerScope.searchName);
      	var id = [];
 		
     	for(var i = 0; i < filterData.length; i++){
     		if(filterData[i].isSelect){
     			id.push({id:filterData[i].id});
     		}
     	}
 		console.log('id',JSON.stringify(id));
     }
     
     function gotoPage(type, index, activeIndex){
    	 var list, backupList;
    	 if(type == 'compo') {
    		 list =  employeeControllerScope.employeeCompensationList;
    	 }else{
    		 list =  employeeControllerScope.employeeList;
    	 }
     	$rootScope.beginFrom = index;
     	for(var i = 0; i < list.length; i++){
         	list[i].isActiveClass = false;
     	}
     	if(!activeIndex)
     		activeIndex = index
     		list[activeIndex].isActiveClass = true;
     }
     
     function searchMe(type, ifMobile){
    	 var list, backupList;
    	 if(type == 'compo') {
    		 list =  employeeControllerScope.employeeCompensationList;
    		 listBackup =  employeeControllerScope.employeeCompensationListBackup;
    	 }else{
    		 list =  employeeControllerScope.employeeList;
    		 listBackup =  employeeControllerScope.employeeListBackup;
    	 }
     	var canEnter = false;
     	employeeControllerScope.isNextDisabled = false;
     	for (var item in employeeControllerScope.search){
     		if(employeeControllerScope.search[item] != ''){
     			canEnter = true;
     		} 
     	}
     	if(ifMobile && employeeControllerScope.searchName != ''){
     		list = $filter('filter')(employeeControllerScope.listBackup, employeeControllerScope.searchName)
     	}else if(canEnter){
     		list = $filter('filter')(employeeControllerScope.listBackup, employeeControllerScope.search);
     	}else {
     		list = angular.copy(employeeControllerScope.listBackup);
     	}
     	$rootScope.beginFrom = 0;
    	for(var i = 0; i < list.length; i++){
    		list[i].isActiveClass = false;
    		list[i].isSelect = employeeControllerScope.isSelect;
    	}
    	if(list[0])
    		list[0].isActiveClass = true;
     	if((employeeControllerScope.buttonBeginFrom + employeeControllerScope.buttonLimitToShow) * employeeControllerScope.buttonLimitToShow >= list.length)
     		employeeControllerScope.isNextDisabled = true;
     	
     }
     
     function showPrevNav(){
     		employeeControllerScope.buttonBeginFrom -= employeeControllerScope.buttonLimitToShow;
         	gotoPage(employeeControllerScope.buttonBeginFrom)
     		employeeControllerScope.isNextDisabled = false;
     }
     
     function showNextNav(type){
    	 if(type == 'compo') {
    		 list =  employeeControllerScope.employeeCompensationList;
    	 }else{
    		 list =  employeeControllerScope.employeeList;
    	 }
     	employeeControllerScope.buttonBeginFrom += employeeControllerScope.buttonLimitToShow;
     	gotoPage(employeeControllerScope.buttonBeginFrom)
     	if((employeeControllerScope.buttonBeginFrom + employeeControllerScope.buttonLimitToShow) * employeeControllerScope.buttonLimitToShow >= list.length)
     		employeeControllerScope.isNextDisabled = true;
     }
     

     function toggleSelect(isSelect){
    	employeeControllerScope.spinner = true;
     	var filteredData = $filter('orderBy')(employeeControllerScope.employeeList, '-id');
     	filteredData = $filter('limitTo')(filteredData, $rootScope.limitToShow, $rootScope.beginFrom);
     	if(employeeControllerScope.search)
     		filteredData = $filter('filter')(filteredData, employeeControllerScope.search);
     	if(employeeControllerScope.searchName)
     		filteredData = $filter('filter')(filteredData, employeeControllerScope.searchName);
     	for(var i = 0; i < filteredData.length; i++){
     		filteredData[i].isSelect = isSelect;
     	}
     	console.log('toggleSelect',filteredData)
    	 
     	employeeControllerScope.spinner = false; 		
     }
     
     function arrowRotate(arrowPosition) {
         arrowPosition.arrowPosition = !arrowPosition.arrowPosition;
     }
     function arrowRotate1(arrowPosition1) {
         arrowPosition1.arrowPosition1 = !arrowPosition1.arrowPosition1;
     }
     function arrowRotate2(arrowPosition2) {
         arrowPosition2.arrowPosition2 = !arrowPosition2.arrowPosition2;
     }
     function arrowRotate3(arrowPosition3) {
         arrowPosition3.arrowPosition3 = !arrowPosition3.arrowPosition3;
     }
     function arrowRotate4(arrowPosition4) {
         arrowPosition4.arrowPosition4 = !arrowPosition4.arrowPosition4;
     }
     function arrowRotate5(arrowPosition5) {
         arrowPosition5.arrowPosition5 = !arrowPosition5.arrowPosition5;
     }
     function arrowRotate6(arrowPosition6) {
         arrowPosition6.arrowPosition6 = !arrowPosition6.arrowPosition6;
     }
     function arrowRotate7(arrowPosition7) {
         arrowPosition7.arrowPosition7 = !arrowPosition7.arrowPosition7;
     }
     function arrowRotate8(arrowPosition8) {
         arrowPosition8.arrowPosition8 = !arrowPosition8.arrowPosition8;
     }
     function arrowRotate9(arrowPosition9) {
         arrowPosition9.arrowPosition9 = !arrowPosition9.arrowPosition9;
     }
     function arrowRotate10(arrowPosition10) {
         arrowPosition.arrowPosition = !arrowPosition.arrowPosition;
     }
     function arrowRotate10(arrowPosition10) {
         arrowPosition10.arrowPosition10 = !arrowPosition10.arrowPosition10;
     }
     function arrowRotate11(arrowPosition11) {
         arrowPosition11.arrowPosition11 = !arrowPosition11.arrowPosition11;
     }
     function arrowRotate12(arrowPosition12) {
         arrowPosition12.arrowPosition12 = !arrowPosition12.arrowPosition12;
     }
     function arrowRotate13(arrowPosition13) {
         arrowPosition13.arrowPosition13 = !arrowPosition13.arrowPosition13;
     }
     function arrowRotate14(arrowPosition14) {
         arrowPosition14.arrowPosition14 = !arrowPosition14.arrowPosition14;
     }
     function arrowRotate15(arrowPosition15) {
         arrowPosition15.arrowPosition15 = !arrowPosition15.arrowPosition15;
     }
     function arrowRotate16(arrowPosition16) {
         arrowPosition16.arrowPosition16 = !arrowPosition16.arrowPosition16;
     }

     function getCountry() {
        employeeControllerScope.spinner = true;
        configurationService.getCountry().then(function(res) {
            if (res.successflag == 'true') {
                employeeControllerScope.countryList = res.results;
                console.log('getCountry', employeeControllerScope.countryList);
                employeeControllerScope.spinner = false;
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }

     function getState(data) {
        employeeControllerScope.spinner = true;
        configurationService.getState(data).then(function(res) {
            if (res.successflag == 'true') {
                employeeControllerScope.stateList = res.results;
                employeeControllerScope.spinner = false;
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err) {
            employeeControllerScope.spinner = false;
        });
    }

    function getEducationLevel() {
        configurationService.getEducationLevel().then(function(res) {
            console.log('getEducationLevel', res);
            if (res.successflag === 'true') {
                 employeeControllerScope.educationLevelList = res.results; 
                 console.log('educationLevelList', employeeControllerScope.educationLevelList);                 
            }
        }); 
    }

     function getActiveEducationLevel() {
        configurationService.getActiveEducationLevel().then(function(res) {
            console.log('getEducationLevel', res);
            if (res.successflag === 'true') {
                 employeeControllerScope.educationLevelList = res.results; 
                 console.log('educationLevelList', employeeControllerScope.educationLevelList);                 
            }
        }); 
    }

    function sendEmpEducation(){
        employeeControllerScope.empEducation.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empEducation.updatedBy= $localStorage.spheresuite.id;
        employeeControllerScope.spinner = true;
        employeeService.reqEmpEducation(employeeControllerScope.empEducation).then(function(res){
            if(res.successflag == 'true'){
                console.log('sendEmpEducation', res);
                getEmployeeEducationDetails($localStorage.spheresuite.viewEmployeeId);
            } else{
                employeeControllerScope.spinner = false;
            }
        },  function(err){
                employeeControllerScope.spinner = false;
            });
        $('#educationModal').modal('hide');
    }


    function getEmployeeEducationDetails(data){
        employeeService.retrieveEmployeeEducation(data).then(function(res) {
            if (res.successflag === 'true') {                
             employeeControllerScope.empEducationDetailList = res.results; 
             console.log('getEmployeeEducationDetails11===', employeeControllerScope.empEducationDetailList);                   
            }
        }); 
    }    

    function empFamilyDetails(){
        employeeControllerScope.spinner = true;
        employeeControllerScope.empFamily.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empFamily.updatedBy= $localStorage.spheresuite.id;
        employeeService.empFamilyDetail(employeeControllerScope.empFamily).then(function(res){
            console.log('empFamilyDetails', res);
            if(res.successflag == 'true'){
                employeeControllerScope.empFamily = [];
                getEmpFamilyDetails($localStorage.spheresuite.viewEmployeeId);

            } else{
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#familyModal').modal('hide');
    }

    function updateFamilyDetails(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empFamily.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empFamily.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateFamilyDetails(employeeControllerScope.empFamily).then(function(res){
            console.log('empFamilyDetails', res);
            if(res.successflag == 'true'){
                getEmpFamilyDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#familyModal').modal('hide');
    }

    function getEmpFamilyDetails(data){
        employeeControllerScope.spinner = true;
        employeeService.getEmpFamilyDetails(data).then(function(res){
            if (res.successflag === 'true'){
              employeeControllerScope.empFamilyDetailsList = res.results;

              console.log('empFamilyDetailsList', employeeControllerScope.empFamilyDetailsList);  
            }else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });

    }   

    function goToEditFamilyDetails(){
        employeeControllerScope.empFamily=[];
        employeeControllerScope.empFamily = employeeControllerScope.empFamilyDetailsList[0];
        
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }  

    function gotoEditEmpEducation(data){
        employeeControllerScope.empEducation = data;
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
        getState(data.countryId);
        employeeControllerScope.empEducation.educationLevelId = data.educationLevelId;
    }    

    function gotoEditTerminateDetails(){
        employeeControllerScope.empTerminate=[];
        employeeControllerScope.empTerminate = employeeControllerScope.employeeTerminateDetailsList[0];
        
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    } 

    function updateEmpEducation(){

        employeeControllerScope.spinner = true;
       employeeControllerScope.empEducation.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empEducation.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateempEducationDetails(employeeControllerScope.empEducation).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmployeeEducationDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#educationModal').modal('hide');

    } 


    function gotoAddEmpEducation(){
        employeeControllerScope.empEducation;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }  

    function gotoEditSkills(data){
        employeeControllerScope.empSkill = data;
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }  

    function gotoAddEmpSkills(){
        employeeControllerScope.empSkill;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }      

    function updateEmpSkills(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empSkill.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empSkill.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpSkillDetails(employeeControllerScope.empSkill).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmpSkills($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#skillsModal').modal('hide');
    } 


    function getAddressType() {
        configurationService.getAddressType().then(function(res) {
            console.log('getAddressTypenathan====', res);
            if (res.successflag === 'true') {
                 employeeControllerScope.addressTypeLevelList = res.results; 
                 console.log('addressTypeLevelList', employeeControllerScope.addressTypeLevelList);
                 /*angular.forEach(configurationControllerScope.addressTypeLevelList, function(piece,index){                    
                    configurationControllerScope.addressLevelListName.push(piece.name);
                    console.log('addressLevelListName', configurationControllerScope.addressLevelListName);
                 });*/
                 
            }
        }); 
    }

    function addEmpAddress(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.empAddress) {
            if(employeeControllerScope.empAddress.note == undefined){
                employeeControllerScope.empAddress.note = '';
            }
            if(employeeControllerScope.empAddress.address2 == undefined){
                employeeControllerScope.empAddress.address2 = '';
            }
            employeeControllerScope.empAddress.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empAddress.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpAddress(employeeControllerScope.empAddress).then(function(res) {
                console.log('addEmpAddress====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpAddress($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empAddress= {};
                    $('#addressModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }

    }

    function getEmpAddress(data) { 
        employeeControllerScope.spinner = true; 
        employeeService.getEmpAddress(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeAddressList = res.results;
                console.log('employeeControllerScope.employeeAddressList', employeeControllerScope.employeeAddressList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
        });
    }

    function gotoAddEmpAddress(){
        employeeControllerScope.empAddress;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function goToEditEmpAddress(data){
        console.log('goToEditEmpAddress', data);
        employeeControllerScope.empAddress = data;
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
        getState(data.countryId);
        employeeControllerScope.empAddress.addressTypeId = data.addressTypeId;
    }

    function updateEmpAddress(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empAddress.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empAddress.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpAddressDetails(employeeControllerScope.empAddress).then(function(res){
            console.log('updateEmpEducation', res);
            if(res.successflag == 'true'){
                getEmpAddress($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#addressModal').modal('hide');
    } 

    function addEmpBankDetails(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.empBankDetails) {
            employeeControllerScope.empBankDetails.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empBankDetails.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpBankDetails(employeeControllerScope.empBankDetails).then(function(res) {
                console.log('addEmpAddress====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpBankDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empBankDetails= {};
                    $('#bankModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpBankDetails(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpBankDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeBankDetailsList = res.results;
                console.log('employeeControllerScope.employeeBankDetailsList', employeeControllerScope.employeeBankDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
        });
    }

    function goToEditBankDetails(data){
        console.log('goToEditBankDetails', data);
        employeeControllerScope.empBankDetails = data[0];
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }  

    function gotoAddEmpBankDetails(){
        employeeControllerScope.empBankDetails;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function updateEmpBankDetails(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empBankDetails.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empBankDetails.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpBankDetails(employeeControllerScope.empBankDetails).then(function(res){
            console.log('updateEmpBankDetails', res);
            if(res.successflag == 'true'){
                getEmpBankDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#bankModal').modal('hide');
    }

    function getTerminateReason(){
        configurationService.getTerminateReason().then(function(res) {
            if (res.successflag === 'true') {
                 employeeControllerScope.terminateReasonList = res.results; 
            }
        }); 
    }

    function addEmpTerminate(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.empTerminate) {
            console.log('employeeControllerScope.empTerminate', employeeControllerScope.empTerminate);
            employeeControllerScope.empTerminate.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empTerminate.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpTerminateDetails(employeeControllerScope.empTerminate).then(function(res) {
                console.log('addEmpAddress====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpTerminateDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empTerminate= {};
                    $('#terminateModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpTerminateDetails(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpTerminateDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeTerminateDetailsList = res.results;
                console.log('employeeControllerScope.employeeTerminateDetailsList', employeeControllerScope.employeeTerminateDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
        });
    }

    function addEmpCertification(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.empCertification) {
            employeeControllerScope.empCertification.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empCertification.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpCertificateDetails(employeeControllerScope.empCertification).then(function(res) {
                console.log('addEmpAddress====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpCertificateDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empCertification= {};
                    $('#certificateModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpCertificateDetails(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpCertificateDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeCertificateDetailsList = res.results;
                console.log('employeeControllerScope.employeeCertificateDetailsList', employeeControllerScope.employeeCertificateDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
        });
    }

    function gotoEditEmpCetificate(data){
        employeeControllerScope.empCertification = data;
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }  

    function gotoAddEmpCertificate(){
        employeeControllerScope.empCertification;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function updateEmpCertification(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empCertification.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empCertification.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpCertificateDetails(employeeControllerScope.empCertification).then(function(res){
            console.log('updateEmpCertificateDetails', res);
            if(res.successflag == 'true'){
                getEmpCertificateDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#certificateModal').modal('hide');
    }

    function addEmpAdditionalInformation(){
         employeeControllerScope.spinner = true;
         if(employeeControllerScope.empAdditionalInfo.spouseName == undefined){
            employeeControllerScope.empAdditionalInfo.spouseName = '';
         }
         if(employeeControllerScope.empAdditionalInfo.children == undefined){
            employeeControllerScope.empAdditionalInfo.children = '';
         }
        if (employeeControllerScope.empAdditionalInfo) {
            employeeControllerScope.empAdditionalInfo.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empAdditionalInfo.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpAdditionalInformation(employeeControllerScope.empAdditionalInfo).then(function(res) {
                console.log('addEmpAddress====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpAdditionalInfoDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empAdditionalInfo= {};
                    $('#additionalModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpAdditionalInfoDetails(data){
         employeeControllerScope.spinner = true; 
        employeeService.getEmpAdditionalInfoDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeAdditionalInfoDetailsList = res.results;
                console.log('employeeControllerScope.employeeAdditionalInfoDetailsList',employeeControllerScope.employeeAdditionalInfoDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    function gotoAddEmpAdditionalInfo(){
        employeeControllerScope.empAdditionalInfo;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function gotoEditEmpAdditionalInfo(data){
        employeeControllerScope.empAdditionalInfo = data[0];
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }

    function UpdateAdditionalInfo(){
        console.log('UpdateAdditionalInfo');
         employeeControllerScope.spinner = true;
        employeeControllerScope.empAdditionalInfo.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empAdditionalInfo.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpAdditionalInfoDetails(employeeControllerScope.empAdditionalInfo).then(function(res){
            console.log('updateEmpCertificateDetails', res);
            if(res.successflag == 'true'){
                getEmpAdditionalInfoDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#additionalModal').modal('hide');
    }

    function getUserInformation(){
        employeeControllerScope.spinner = true; 
        employeeService.getUserInformationDetails($localStorage.spheresuite.viewEmployeeId).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeUserInformationDetailsList = res.results;
                console.log('getUserInformation',employeeControllerScope.employeeUserInformationDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    function getUserInformationMyInfo(){
        employeeControllerScope.spinner = true; 
        employeeService.getUserInformationDetails($localStorage.spheresuite.id).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeUserInformationDetailsList = res.results;
                console.log('getUserInformationMyInfo',employeeControllerScope.employeeUserInformationDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    var filesName = [];
    function addUploadedFile(){
        commonService.formValNotManditory(employeeControllerScope.uploadFileForm, employeeControllerScope.uploadFile).then(function(data) {
        filesName = [];
        if(data.file && data.file != ""){
            for(i in data.file){
                console.log('i name',data.file[i].name)
                filesName.push(data.file[i].name)
            }
            console.log('files',filesName)
            Upload
            .base64DataUrl(data.file)
            .then(function(urls) {
                data.file = '';
                empFileUpload(data,urls);
            });
        }else{
            data.file = "";
            empFileUpload(data);
        }

    },function(err){

    });
    }

    function empFileUpload(data,files){
            console.log('empFileUpload', files);
                if(files){ 
                    for(var i = 0; i < files.length; i++){
                        employeeService.uploadDocument({id: $localStorage.spheresuite.viewEmployeeId, photo: files[i], name: filesName[i]}).then(function(res1){
                            console.log('res1',i, files.length)
                            if(i == files.length){
                                employeeControllerScope.spinner = false;
                            }
                        },function(err){
                            console.log('err1',err1);
                        });
                    }
                }else{
                    employeeControllerScope.spinner = false;
                }
            
        
    }

    function getEmpFileUpload(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpFileUpload(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.uploadFileList = res.results;
                console.log('getEmpFileUpload',employeeControllerScope.uploadFileList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) {
        employeeControllerScope.spinner = false; 
        });
    }

    function addEmpDriversLicense(){
        employeeControllerScope.spinner = true;
        if(employeeControllerScope.empDriverLicense.address2 == undefined){
            employeeControllerScope.empDriverLicense.address2 = '';
        }
        if (employeeControllerScope.empDriverLicense) {
            employeeControllerScope.empDriverLicense.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empDriverLicense.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpDriversLicense(employeeControllerScope.empDriverLicense).then(function(res) {
                console.log('addEmpDriversLicense====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpDriverLicenseDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empDriverLicense= {};
                    $('#licenseModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpDriverLicenseDetails(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpDriverLicenseDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeDriverLicenseDetailsList = res.results;
                console.log('employeeControllerScope.employeeDriverLicenseDetailsList',employeeControllerScope.employeeDriverLicenseDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    function goToEditEmpDriverLicense(data){
        console.log('goToEditEmpDriverLicense', data);
        employeeControllerScope.empDriverLicense = data[0];
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
        getState(data[0].countryId);
    }  

    function gotoAddEmpDriverLicense(){
        employeeControllerScope.empDriverLicense;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function updateEmpDriverLicenseDetails(){
        employeeControllerScope.spinner = true;
        console.log('employeeControllerScope.empDriverLicense===', employeeControllerScope.empDriverLicense)
       employeeControllerScope.empDriverLicense.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empDriverLicense.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpDriverLicenseDetails(employeeControllerScope.empDriverLicense).then(function(res){
            console.log('updateEmpDriverLicenseDetails', res);
            if(res.successflag == 'true'){
                getEmpDriverLicenseDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#licenseModal').modal('hide');
    }

    function getEmpCompensation(){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpCompensationDetails($localStorage.spheresuite.viewEmployeeId).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeCompensationDetailsList = res.results;
                console.log('getEmpCompensation',employeeControllerScope.employeeCompensationDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    function goToEditContactDetails(data){
        console.log('goToEditContactDetails', data);
        employeeControllerScope.empContactInfo = data[0];
        employeeControllerScope.saveUpdate = true;
        employeeControllerScope.saveShow = false;
    }  

    function gotoAddEmpContactDetails(){
        employeeControllerScope.empContactInfo;
        employeeControllerScope.saveUpdate = false;
        employeeControllerScope.saveShow = true;
    }

    function addEmpContactDetails(){
        employeeControllerScope.spinner = true;
        if(employeeControllerScope.empContactInfo.homePhone == undefined){
            employeeControllerScope.empContactInfo.homePhone = '';
        }
        if(employeeControllerScope.empContactInfo.otherEmail == undefined){
            employeeControllerScope.empContactInfo.otherEmail = '';
        }
        if (employeeControllerScope.empContactInfo) {
            employeeControllerScope.empContactInfo.updatedBy = $localStorage.spheresuite.id;
            employeeControllerScope.empContactInfo.empId = $localStorage.spheresuite.viewEmployeeId;
            employeeService.addEmpContactInformation(employeeControllerScope.empContactInfo).then(function(res) {
                console.log('addEmpDriversLicense====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.spinner = false;
                    getEmpContactInformationDetails($localStorage.spheresuite.viewEmployeeId);
                    employeeControllerScope.empContactInfo= {};
                    $('#contactModal').modal('hide');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        }
    }

    function getEmpContactInformationDetails(data){
        employeeControllerScope.spinner = true; 
        employeeService.getEmpContactInfoDetails(data).then(function(res) { 
            if (res.successflag === 'true') {
                employeeControllerScope.spinner = false;
                employeeControllerScope.employeeContactDetailsList = res.results;
                console.log('employeeControllerScope.employeeContactDetailsList', employeeControllerScope.employeeContactDetailsList);  
            } else
            employeeControllerScope.spinner = false;
            employeeControllerScope.employeeSkills = { empId :  res.results }; 
        }, function(err) { 
            employeeControllerScope.spinner = false;
        });
    }

    function updateEmpContactDetails(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empContactInfo.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empContactInfo.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateEmpContactInfoDetails(employeeControllerScope.empContactInfo).then(function(res){
            console.log('updateEmpContactDetails', res);
            if(res.successflag == 'true'){
                getEmpContactInformationDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#contactModal').modal('hide');
    }

    function familyInfoDecline(){
        employeeControllerScope.empFamily = null;
        employeeControllerScope.empFamilyForm.$setUntouched();
        employeeControllerScope.empContactInfo = null;
        employeeControllerScope.empContactForm.$setUntouched();
        employeeControllerScope.empContactForm.$setPristine();
        employeeControllerScope.empBankDetails = null;
        employeeControllerScope.empBankForm.$setUntouched();
        employeeControllerScope.empEducation = null;
        employeeControllerScope.empEducationForm.$setUntouched(); 
        employeeControllerScope.empDriverLicense = null;
        employeeControllerScope.driversLicenseForm.$setUntouched();
        employeeControllerScope.empCertification = null;
        employeeControllerScope.empCertificateForm.$setUntouched();
        employeeControllerScope.empAdditionalInfo = null;
        employeeControllerScope.empAdditionalInfoForm.$setUntouched(); 
        employeeControllerScope.empAddress = null;
        employeeControllerScope.addressInfoForm.$setUntouched();  
    }
    
    function sendOnboardRequest(){console.log('empOnboard====1 ', employeeControllerScope.empOnboard);
    	employeeControllerScope.spinner = true;
    	if(employeeControllerScope.empOnboard){
    		employeeControllerScope.empOnboard.updatedBy = $localStorage.spheresuite.id;
            employeeService.sendOnboardRequest(employeeControllerScope.empOnboard).then(function(res) {
                console.log('empOnboard====', res);
                if (res.successflag === 'true') {
                    employeeControllerScope.empOnboard = {};
                    $('#employeeOnboard').modal('hide');
                    $('#employeeOnboardSuccess').modal('show');
                    getEmployeeOnboard();
                    employeeControllerScope.spinner = false;
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                    $('#employeeOnboard').modal('hide');
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
    	}
    }
    
    function updateEmployeeOnboard(){
        employeeControllerScope.spinner = true;
        if (employeeControllerScope.employeeonboard) {
        	console.log('employeeControllerScope.employeeonboard   ',  employeeControllerScope.employeeonboard)
        	employeeControllerScope.employeeonboard.id = $localStorage.spheresuite.employeeOnboardId;
            employeeControllerScope.employeeonboard.updatedBy= $localStorage.spheresuite.id;
            if(employeeControllerScope.employeeonboard.reason == undefined){
            	employeeControllerScope.employeeonboard.reason = "";
            }
            if(employeeControllerScope.employeeonboard.status == undefined){
                employeeControllerScope.employeeonboard.status = "p";
            }
            console.log('employeeControllerScope.employeeonboard   ',  employeeControllerScope.employeeonboard)
            commonService.formValNotManditory(employeeControllerScope.employeeonboardForm, employeeControllerScope.employeeonboard).then(function(data) {
                if (data) {
                	console.log('employeeControllerScope.employeeonboard11   ',  data)
                    data.updatedBy = $localStorage.spheresuite.id;
                    employeeService.updateEmployeeOnboard(data).then(function(res) {
                        if (res.successflag === 'true') {
                        	getEmployeeOnboard();
                            $location.path('/employee/onboard');
                           // delete $localStorage.spheresuite.employeeOnboardId;

                        } else {
                            employeeControllerScope.spinner = false;
                            employeeControllerScope.msg = res.errors;
                        }

                    }, function(err) {
                        employeeControllerScope.spinner = false;
                    });
                } else {
                    employeeControllerScope.spinner = false;
                }
            });
        } else
            employeeControllerScope.spinner = false;
    }
    
    function updateRejectOnboard(){
        employeeControllerScope.spinner = true;
        console.log('employeeControllerScope.empOnboardDatav  ', employeeControllerScope.empOnboardData)
    	if(employeeControllerScope.empOnboardData){
    		if(employeeControllerScope.employeeonboardReject){
    			employeeControllerScope.empOnboardData.reason =  employeeControllerScope.employeeonboardReject.reason;
    			employeeControllerScope.empOnboardData.status = 'r';
    		}
    		else
    			employeeControllerScope.empOnboardData.reason = "";
				employeeControllerScope.empOnboardData.updatedBy = $localStorage.spheresuite.id;
            employeeService.updateEmployeeOnboard(employeeControllerScope.empOnboardData).then(function(res) {
                if (res.successflag === 'true') {
                	getEmployeeOnboard();
                	employeeControllerScope.rejectStatus = true;
                    $location.path('/employee/onboard');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        } else {
            employeeControllerScope.spinner = false;
        }
    }
    
    function updateAcceptOnboard(data){
        employeeControllerScope.spinner = true;
        console.log('employeeControllerScope.empOnboardDatav  ', employeeControllerScope.empOnboardData)
    	if(data){
			data.status = 'a';
			data.updatedBy = $localStorage.spheresuite.id;
            employeeService.updateEmployeeOnboard(data).then(function(res) {
                if (res.successflag === 'true') {
                	getEmployeeOnboard();
                   // $location.path('/employee/onboard');
                } else {
                    employeeControllerScope.spinner = false;
                    employeeControllerScope.msg = res.errors;
                }

            }, function(err) {
                employeeControllerScope.spinner = false;
            });
        } else {
            employeeControllerScope.spinner = false;
        }
    }
    
    function getEmployeeOnboard(){
        employeeControllerScope.spinner = true;
        employeeService.getEmployeeOnboard().then(function(res){
    		console.log('getEmployeeOnboard res',res)
    		if(res.successflag == 'true' && res.results.length > 0){
    			employeeControllerScope.employeeonboardList = res.results;
    		}
            employeeControllerScope.spinner = false;
    	},function(err){
            employeeControllerScope.spinner = false;
    	});
    }
    
    function rejectPopup(data){
    	if(data){
	    	employeeControllerScope.empOnboardData = data;
	    	$('#employeeOnboardReject').modal('show');
    	}
    }
    function acceptPopup(data){
    	if(data){
	    	employeeControllerScope.employee = data;
            employeeService.getEmployeeId().then(function(res) { 
            employeeControllerScope.empIdAcceptPopup = res.results; 
            employeeControllerScope.employee.empId = JSON.stringify(employeeControllerScope.empIdAcceptPopup);
            if(employeeControllerScope.employee.reason == undefined){
                employeeControllerScope.employee.reason = '';
            }
        }, function(err) { 
        });

    	}
    }
    
    function getEmployeeAssignment(data){
        employeeControllerScope.spinner = true;
        employeeService.getEmployeeAssignment(data).then(function(res){
    		if(res.successflag == 'true' && res.results.length > 0){
    			employeeControllerScope.employeeAssignmentList = res.results;
    		}
            employeeControllerScope.spinner = false;
    	},function(err){
            employeeControllerScope.spinner = false;
    	});
    }
    
    function gotoViewProject(projectId){
    	if(projectId){
    		$localStorage.spheresuite.projectId = projectId;
    		$location.path('/project/view');
    	}    	
    }

    function updateTerminateDetails(){
        employeeControllerScope.spinner = true;
       employeeControllerScope.empTerminate.empId = $localStorage.spheresuite.viewEmployeeId;
        employeeControllerScope.empTerminate.updatedBy= $localStorage.spheresuite.id;
        employeeService.updateTerminateDetails(employeeControllerScope.empTerminate).then(function(res){
            console.log('empTerminate', res);
            if(res.successflag == 'true'){
                getEmpTerminateDetails($localStorage.spheresuite.viewEmployeeId)
            } else {
                employeeControllerScope.spinner = false;
            }
        }, function(err){
                employeeControllerScope.spinner = false;
        });
        $('#terminateModal').modal('hide');
    }

    function gotoCancelTerminateDetails(data){
        console.log('gotoCancelTerminateDetailsCtrl', data);
        employeeControllerScope.spinner = true;
        employeeService.gotoCancelTerminateDetails(data).then(function(res){
            console.log('terrres', res);
            if(res.successflag == 'true'){
               $route.reload();
            }
            employeeControllerScope.spinner = false;
        },function(err){
            employeeControllerScope.spinner = false;
        });
    }

    $(document).ready(function () {
        $(".showMarried").hide();
        $("#Married").click(function () {
            $(".showMarried").show();
        });
        $("#Unmarried").click(function () {
            $(".showMarried").hide();
        });
    });
    
}
app.directive('spheresuitenavigation', function() {
    var directive = {};

    directive.restrict = 'E'; /* restrict this directive to elements */
    directive.scope = {
            'listItem' : "=",
            'restrictTo' : '@'
        }

    directive.template = '<button ng-repeat="i in listItem" ng-if="$index % restrictTo == 0" ng-click="employeeControllerScope.gotoPage($index)"><span ng-if="$index == 0">{{$index + 1}}</span><span ng-if="$index != 0">{{$index / restrictTo + 1}}</span></button>';
    return directive;
})