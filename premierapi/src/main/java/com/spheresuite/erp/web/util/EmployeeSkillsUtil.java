package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeSkillsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeSkillsUtil {
	
	private EmployeeSkillsUtil() {}
	
	public static JSONObject getEmployeeSkillsList(List<EmployeeSkillsDO> employeeSkillsDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeSkillsDO employeeSkillsDO : employeeSkillsDOList) {
				resultJSONArray.put(getEmployeeSkillsDetailObject(employeeSkillsDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeSkillsDetailObject(EmployeeSkillsDO employeeSkillsDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeSkillsDO.getId()));
		result.put(CommonConstants.NAME, String.valueOf(employeeSkillsDO.getName()));
		return result;
	}
}
