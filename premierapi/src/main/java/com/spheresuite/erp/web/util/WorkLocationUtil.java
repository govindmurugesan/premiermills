package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.dao.CountryDAO;
import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.util.CommonConstants;

public class WorkLocationUtil {
	
	private WorkLocationUtil() {}
	
	public static JSONObject getworkLocationList(List<WorkLocationDO> workLocationList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (WorkLocationDO workLocation : workLocationList) {
				resultJSONArray.put(getWorkLocationDetailObject(workLocation));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getWorkLocationDetailObject(WorkLocationDO workLocation)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(workLocation.getId()));
		result.put(CommonConstants.CITY, String.valueOf(workLocation.getCity()));
		result.put(CommonConstants.COUNTRY_ID, String.valueOf(workLocation.getCountryId()));
		result.put(CommonConstants.STATE_ID, String.valueOf(workLocation.getStateId()));
		result.put(CommonConstants.STATUS, String.valueOf(workLocation.getStatus()));
		CountryDAO obj = new CountryDAO();
		CountryDO workLocationdo = obj.retrieveByID(workLocation.getCountryId());
		if(workLocationdo != null){
			result.put(CommonConstants.COUNTRY_NAME, String.valueOf(workLocationdo.getName()));
		}
		StateService stateObj = new StateService();
		List<StateDO> stateDO = stateObj.retrieveById(workLocation.getStateId());
		if(stateDO != null && stateDO.size() > 0){
			result.put(CommonConstants.STATE_NAME, String.valueOf(stateDO.get(0).getName()));
		}
		
		if(workLocation.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(workLocation.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(workLocation.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(workLocationdo.getUpdatedon())));

		return result;
	}
}
