package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProjectDAO;
import com.spheresuite.erp.domainobject.ProjectDO;
import com.spheresuite.erp.exception.AppException;

public class ProjectService {
	static Logger logger = Logger.getLogger(ProjectService.class.getName());
	
	public ProjectDO persist(ProjectDO projectDO) throws AppException {
		return new ProjectDAO().persist(projectDO);
	}

	public List<ProjectDO> retrieveById(Long Id) throws AppException {
		return new ProjectDAO().retrieveById(Id);
	}
	
	public List<ProjectDO> retrieveByOpportunityId(Long Id) throws AppException {
		return new ProjectDAO().retrieveByOpportunityId(Id);
	}
	
	public List<ProjectDO> retrieve(List<Long> Id) throws AppException {
		return new ProjectDAO().retrieve(Id);
	}
	
	public List<ProjectDO> retrieveAll() throws AppException {
		return new ProjectDAO().retrieveAll();
	}
	
	public ProjectDO update(ProjectDO projectDO) throws AppException {
		return new ProjectDAO().update(projectDO);
	}
	
	public boolean persistList(List<ProjectDO> projectDO) throws AppException {
		return new ProjectDAO().persistList(projectDO);
	}
}
