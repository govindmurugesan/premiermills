package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.AllowanceTypeDAO;
import com.spheresuite.erp.domainobject.AllowanceTypeDO;
import com.spheresuite.erp.exception.AppException;

public class AllowanceTypeService {
	static Logger logger = Logger.getLogger(AllowanceTypeService.class.getName());
	
	public AllowanceTypeDO persist(AllowanceTypeDO allowanceTypeDO) throws AppException {
		return new AllowanceTypeDAO().persist(allowanceTypeDO);
	}

	public List<AllowanceTypeDO> retrieveActive() throws AppException {
		return new AllowanceTypeDAO().retrieveActive();
	}
	
	public List<AllowanceTypeDO> retrieveById(Long id) throws AppException {
		return new AllowanceTypeDAO().retrieveById(id);
	}
	
	public List<AllowanceTypeDO> retrieve() throws AppException {
		return new AllowanceTypeDAO().retrieve();
	}
	
	public AllowanceTypeDO update(AllowanceTypeDO allowanceTypeDO) throws AppException {
		return new AllowanceTypeDAO().update(allowanceTypeDO);
	}
}
