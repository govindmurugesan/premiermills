package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="country")
@TableGenerator(name ="country", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "country.findAll", query = "SELECT r FROM CountryDO r"),
    @NamedQuery(name = "country.findById", query = "SELECT r FROM CountryDO r where r.id =:id"),
    @NamedQuery(name = "country.findByStatus", query = "SELECT r FROM CountryDO r where r.status =:status"),
})
public class CountryDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "country.findAll";
	
	public static final String FIND_BY_ID = "country.findById";
	
	public static final String FIND_BY_STATUS = "country.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "country")
	private Long id;
	private String name;
	private String code;
	private String callingcode;
	private String currencyType;
	private char status;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getCallingcode() {
		return callingcode;
	}
	public void setCallingcode(String callingcode) {
		this.callingcode = callingcode;
	}
	public String getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(String currencyType) {
		this.currencyType = currencyType;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}