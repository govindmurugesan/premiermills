package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TerminateReasonTypeDO;
import com.spheresuite.erp.service.TerminateReasonTypeService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.TerminateReasonTypeUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/terminatereasontype")
public class TerminateReasonTypeRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TerminateReasonTypeRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TerminateReasonTypeDO terminateReasonTypeDO = new TerminateReasonTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		terminateReasonTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		terminateReasonTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			terminateReasonTypeDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			terminateReasonTypeDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		terminateReasonTypeDO.setStatus('a');
			 	}
				new TerminateReasonTypeService().persist(terminateReasonTypeDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TerminateReasonTypeDO> terminateReasonTypeList = new TerminateReasonTypeService().retrieveActive();
				respJSON = TerminateReasonTypeUtil.getTerminateReasonTypeList(terminateReasonTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TerminateReasonTypeDO> terminateReasonTypeList = new TerminateReasonTypeService().retrieve();
				respJSON = TerminateReasonTypeUtil.getTerminateReasonTypeList(terminateReasonTypeList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				TerminateReasonTypeDO terminateReasonTypeDO = new TerminateReasonTypeDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<TerminateReasonTypeDO> terminateReasonTypeList = new TerminateReasonTypeService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		terminateReasonTypeDO = terminateReasonTypeList.get(0);
			 		terminateReasonTypeDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		terminateReasonTypeDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			terminateReasonTypeDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		terminateReasonTypeDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new TerminateReasonTypeService().update(terminateReasonTypeDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
