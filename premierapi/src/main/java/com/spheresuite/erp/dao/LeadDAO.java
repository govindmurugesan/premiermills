package com.spheresuite.erp.dao;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class LeadDAO {
	static Logger logger = Logger.getLogger(LeadDAO.class.getName());
	private EntityManager em = null;
	
	public LeadDO persist(LeadDO LeadDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && LeadDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(LeadDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return LeadDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieve(String type, List<Long> Id) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_ALL);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.ID, Id);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveAll(String type) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_ALL_DATE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveByEmp(String type, Long empId) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_EMP);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieve(List<Long> Id) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND);
				q.setParameter(CommonConstants.ID, Id);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveAll() throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_ALL_LEAD);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveById(Long Id) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveBetweenDate(Date fromDate, Date toDate) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_DATE);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.STATUS, "n");
				q.setParameter(CommonConstants.TYPE, CommonConstants.L);
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveBetweenDateByEmp(Date fromDate, Date toDate, String empId) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_DATE_BY_EMP);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.TYPE, CommonConstants.L);
				q.setParameter(CommonConstants.EMPID, Long.parseLong(empId));
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveCustomerBetweenDateByEmp(Date fromDate, Date toDate, String empId) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_DATE_BY_EMP);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.TYPE, CommonConstants.C);
				q.setParameter(CommonConstants.STATUS, "n");
				q.setParameter(CommonConstants.EMPID, Long.parseLong(empId));
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	public LeadDO update(LeadDO LeadDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(LeadDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return LeadDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveCustomerBetweenDate(Date fromDate, Date toDate) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_CUSTOMER_BY_DATE);
				q.setParameter(CommonConstants.FROMDATE, fromDate);
				q.setParameter(CommonConstants.TODATE, toDate);
				q.setParameter(CommonConstants.TYPE, CommonConstants.C);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	

	public boolean persistList(List<LeadDO> leadDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leadDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (LeadDO leadDO2 : leadDO) {
					em.persist(leadDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByEmp(Long empId, String type) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_EMP);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByTransferType(String transferType, String type) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_TRANSFERTYPE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, "n");
				q.setParameter(CommonConstants.TRANSFER_TYPE, transferType);
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveLeadByAllTransferType(String type) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_ALL_TRANSFERTYPE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, "n");
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;

	}
	
	@SuppressWarnings("unchecked")
	public List<LeadDO> retrieveTransferLeadById(String transferType, String type, Long empID) throws AppException {
		List<LeadDO> leadList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeadDO.FIND_BY_TRANSFERTYPE_BY_ID);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, "n");
				q.setParameter(CommonConstants.TRANSFER_TYPE, transferType);
				q.setParameter(CommonConstants.EMPID, empID);
				leadList = (List<LeadDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leadList;

	}

	public boolean update(List<LeadDO> LeadList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (LeadDO leadDO : LeadList) {
					em.merge(leadDO);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
}
