package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="mailbox")
@TableGenerator(name ="mailbox", initialValue =100101, allocationSize =1)
@NamedQueries({
	@NamedQuery(name = "mailbox.findAll", query = "SELECT r FROM MailboxDO r"),
    @NamedQuery(name = "mailbox.findById", query = "SELECT r FROM MailboxDO r where r.empId =:empId"),
})
public class MailboxDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "mailbox.findAll";
	
	public static final String FIND_BY_ID = "mailbox.findById";
	
	public static final String FIND_BY_STATUS = "mailbox.findByStatus";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "mailbox")
	private Long id;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long empId;
	private String message;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}