package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.TaxSlabsSettingsDO;
import com.spheresuite.erp.service.TaxSlabsSettingsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.TaxSlabsSettingsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/taxslabssettings")
public class TaxSlabsSettingsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(TaxSlabsSettingsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				TaxSlabsSettingsDO taxSlabsSettingsDO = new TaxSlabsSettingsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		taxSlabsSettingsDO.setAmountfrom(!inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMAMOUNT).toString() : "");
			 		taxSlabsSettingsDO.setTaxpayeeId(!inputJSON.get(CommonConstants.TAXPAYEEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.TAXPAYEEID).toString()) : null);
			 		taxSlabsSettingsDO.setAmountto(!inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.TOAMOUNT).toString() : "");
			 		taxSlabsSettingsDO.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			taxSlabsSettingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			taxSlabsSettingsDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		taxSlabsSettingsDO.setStatus('a');
			 		taxSlabsSettingsDO.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
			 		taxSlabsSettingsDO.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
			 		taxSlabsSettingsDO.setUpdatedon(new Date());
			 	}
				new TaxSlabsSettingsService().persist(taxSlabsSettingsDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Deduction Settings Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxSlabsSettingsDO> adeductionSettingList = new TaxSlabsSettingsService().retrieve();
				respJSON = TaxSlabsSettingsUtil.getTaxSettingsList(adeductionSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<TaxSlabsSettingsDO> adeductionSettingList = new TaxSlabsSettingsService().retrieveActive();
				respJSON = TaxSlabsSettingsUtil.getTaxSettingsList(adeductionSettingList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<TaxSlabsSettingsDO> adeductionSettingList = new TaxSlabsSettingsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(adeductionSettingList != null && adeductionSettingList.size() > 0){
			 				TaxSlabsSettingsDO taxSetting = new TaxSlabsSettingsDO();
			 				taxSetting =adeductionSettingList.get(0);
			 				/*taxSlabsSettingsDO.setStatus('i');
			 				if(taxSlabsSettingsDO.getEnddate() == null){
			 					if(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty()){
			 						taxSlabsSettingsDO.setEnddate(inputJSON.get(CommonConstants.STARTDATE).toString());
			 					}
			 				}
		 					if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			taxSlabsSettingsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			taxSlabsSettingsDO.setUpdatedon(new Date());
					 			new TaxSlabsSettingsService().update(taxSlabsSettingsDO);
					 		}
			 				TaxSlabsSettingsDO taxSetting = new TaxSlabsSettingsDO();*/
			 				taxSetting.setAmountfrom(!inputJSON.get(CommonConstants.FROMAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.FROMAMOUNT).toString() : "");
			 				taxSetting.setTaxpayeeId(!inputJSON.get(CommonConstants.TAXPAYEEID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.TAXPAYEEID).toString()) : null);
			 				taxSetting.setAmountto(!inputJSON.get(CommonConstants.TOAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.TOAMOUNT).toString() : "");
			 				taxSetting.setPercentageamount(!inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString().isEmpty() ? inputJSON.get(CommonConstants.PERCENTAGEAMOUNT).toString() : "");
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			taxSetting.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			taxSetting.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		taxSetting.setStatus('a');
					 		taxSetting.setStartdate(!inputJSON.get(CommonConstants.STARTDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.STARTDATE).toString() : null);
					 		taxSetting.setEnddate(!inputJSON.get(CommonConstants.ENDDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.ENDDATE).toString() : null);
					 		taxSetting.setUpdatedon(new Date());
					 		new TaxSlabsSettingsService().persist(taxSetting);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Deduction Settings Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
