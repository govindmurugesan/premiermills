package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeOnboardDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeOnboardDAO {
	static Logger logger = Logger.getLogger(EmployeeOnboardDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeOnboardDO persist(EmployeeOnboardDO employeeOnboardDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeOnboardDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeOnboardDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOnboardDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOnboardDO> retrieve() throws AppException {
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeOnboardDO.FIND_ALL);
				q.setParameter(CommonConstants.STATUS, 'a');
				employeeOnboardList = (List<EmployeeOnboardDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOnboardList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<EmployeeOnboardDO> retrieveById(Long id) throws AppException {
		List<EmployeeOnboardDO> employeeOnboardList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeOnboardDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				q.setParameter(CommonConstants.STATUS, 'p');
				employeeOnboardList = (List<EmployeeOnboardDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOnboardList;
	}
	
	public EmployeeOnboardDO update(EmployeeOnboardDO employeeOnboardDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeOnboardDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeOnboardDO;
	}

}
