package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.AllowanceSettingDO;
import com.spheresuite.erp.domainobject.AllowanceTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.AllowanceTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class AllowanceSettingsUtil {
	
	private AllowanceSettingsUtil() {}
	
	public static JSONObject getAllowanceSettingsList(List<AllowanceSettingDO> allowanceSettingList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AllowanceSettingDO allowanceSettingDO : allowanceSettingList) {
				resultJSONArray.put(getAllowanceSettingsDetailObject(allowanceSettingDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAllowanceSettingsListForAmount(List<AllowanceSettingDO> allowanceSettingList, Double ctc)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (AllowanceSettingDO allowanceSettingDO : allowanceSettingList) {
				resultJSONArray.put(getAllowanceSettingsAmount(allowanceSettingDO, ctc));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getAllowanceSettingsAmount(AllowanceSettingDO allowanceSettingDO, Double ctc)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Double grossPay = ctc/12;
		Double basicPay = 0D;
		Double monthlypay=0D;
		Double ytd=0D;
		
		if(allowanceSettingDO != null){
			if(allowanceSettingDO.getName() != null && allowanceSettingDO.getName().equalsIgnoreCase("Basic")){
				if(allowanceSettingDO.getBasicgrosspay() != null && allowanceSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
					if(allowanceSettingDO.getPercentageamount() != null){
						basicPay = (grossPay * Double.parseDouble(allowanceSettingDO.getPercentageamount()))/100;
						ytd = basicPay * 12;
						monthlypay = basicPay;
					}
				}
			}else{
				basicPay = (grossPay*Double.parseDouble(CommonConstants.BACSICPAY))/100;
				
				if(allowanceSettingDO.getType() != null && allowanceSettingDO.getType().equalsIgnoreCase("P")){
					if(allowanceSettingDO.getBasicgrosspay() != null && allowanceSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
						if(allowanceSettingDO.getPercentageamount() != null){
							monthlypay = (basicPay * Long.parseLong(allowanceSettingDO.getPercentageamount()))/100;
							ytd = monthlypay*12;
						}
					}else if(allowanceSettingDO.getBasicgrosspay() != null && allowanceSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
						if(allowanceSettingDO.getPercentageamount() != null){
							monthlypay = (grossPay * Double.parseDouble(allowanceSettingDO.getPercentageamount()))/100;
							ytd = monthlypay*12;
						}
					}
				}else if(allowanceSettingDO.getType() != null && allowanceSettingDO.getType().equalsIgnoreCase("F")){
					if(allowanceSettingDO.getFixedamount() != null){
						ytd = Double.parseDouble(allowanceSettingDO.getFixedamount());
						monthlypay = ytd/12;
					}
				}
			}
		}
		result.put(CommonConstants.MONTHLY, String.valueOf(monthlypay));
		result.put(CommonConstants.YTD, String.valueOf(ytd));
		
		return result;
	}

	public static JSONObject getAllowanceSettingsDetailObject(AllowanceSettingDO allowanceSettingDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(allowanceSettingDO.getId() != null ? allowanceSettingDO.getId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(allowanceSettingDO.getName() != null ? allowanceSettingDO.getName() : ""));
		result.put(CommonConstants.NOTES, String.valueOf(allowanceSettingDO.getNotes() != null ? allowanceSettingDO.getNotes() : ""));
		result.put(CommonConstants.TAXABLE, String.valueOf(allowanceSettingDO.getTaxable() != null ? allowanceSettingDO.getTaxable() : ""));
		result.put(CommonConstants.TYPE, String.valueOf(allowanceSettingDO.getType() != null ? allowanceSettingDO.getType() : ""));
		result.put(CommonConstants.SECTION, String.valueOf(allowanceSettingDO.getSection() != null ? allowanceSettingDO.getSection() : ""));
		result.put(CommonConstants.FIXEDAMOUNT, String.valueOf(allowanceSettingDO.getFixedamount() != null ? allowanceSettingDO.getFixedamount() : ""));
		result.put(CommonConstants.PERCENTAGEAMOUNT, String.valueOf(allowanceSettingDO.getPercentageamount() != null ? allowanceSettingDO.getPercentageamount() : ""));
		result.put(CommonConstants.BASICGROSS_PAY, String.valueOf(allowanceSettingDO.getBasicgrosspay() != null ? allowanceSettingDO.getBasicgrosspay() : ""));
		String displayName="";
		if(allowanceSettingDO.getType().equalsIgnoreCase("P") ){
			String name="";
			if(allowanceSettingDO.getBasicgrosspay().equalsIgnoreCase("G")){
				name = "Gross";
			}else if(allowanceSettingDO.getBasicgrosspay().equalsIgnoreCase("B")){
				name = "Basic";
			}
			displayName = allowanceSettingDO.getName() +"-"+ allowanceSettingDO.getPercentageamount()+"% Of "+name;
		}else{
			displayName = allowanceSettingDO.getName() +" Rs "+ allowanceSettingDO.getFixedamount();
		}
		
		result.put(CommonConstants.SETTINGNAME, String.valueOf(displayName));
		
		if(allowanceSettingDO.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(allowanceSettingDO.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON, String.valueOf(""));
		}
		result.put(CommonConstants.DISPLAYNAME, String.valueOf(allowanceSettingDO.getDisplayname() != null ? allowanceSettingDO.getDisplayname() : ""));
		
		result.put(CommonConstants.STARTDATE,String.valueOf(allowanceSettingDO.getStartdate() != null ? CommonUtil.convertDateToStringWithdatetime(allowanceSettingDO.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(allowanceSettingDO.getEnddate() != null ? CommonUtil.convertDateToStringWithdatetime(allowanceSettingDO.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(allowanceSettingDO.getStatus()));
		if(allowanceSettingDO.getUpdatedBy() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(allowanceSettingDO.getUpdatedBy()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		result.put(CommonConstants.ALLOWANCE_TYPE_ID, String.valueOf(allowanceSettingDO.getAllowancetypeid() != null ? allowanceSettingDO.getAllowancetypeid() : ""));
		
		if(allowanceSettingDO.getAllowancetypeid() != null){
			List<AllowanceTypeDO> allowanceType = new AllowanceTypeService().retrieveById(allowanceSettingDO.getAllowancetypeid());
			if(allowanceType !=null && allowanceType.size() > 0){
				result.put(CommonConstants.ALLOWANCETYPE_NAME, String.valueOf(allowanceType.get(0).getName()));
			}else{
				result.put(CommonConstants.ALLOWANCETYPE_NAME,"");
			}
		}else{
			result.put(CommonConstants.ALLOWANCETYPE_NAME,"");
		}
		return result;
	}
}
