package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class DepartmentUtil {
	
	private DepartmentUtil() {}
	
	public static JSONObject getDepartmentList(List<DepartmentDO> departmentList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DepartmentDO department : departmentList) {
				resultJSONArray.put(getDepartmentDetailObject(department));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDepartmentDetailObject(DepartmentDO department)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(department.getId()));
		result.put(CommonConstants.NAME, String.valueOf(department.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(department.getUpdatedon())));
		
		result.put(CommonConstants.FROMTIME, String.valueOf(department.getFromTime() != null ? department.getFromTime() : ""));
		result.put(CommonConstants.TOTIME, String.valueOf(department.getToTime() != null ? department.getToTime() : ""));
		if(department.getFromTime() != null){
			String[] s = department.getFromTime().split(":");
			if(s != null && s.length > 0 && s[0] != null){
				Long time = Long.parseLong(s[0].trim());
				if(time > 12){
					time = time - 12;
					result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(time+" PM"));
				}else{
					result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(time+" AM"));
				}
			}
			
		}else{
			result.put(CommonConstants.FROMTIMEDISPLAY, String.valueOf(""));
		}
		
		if(department.getToTime() != null){
			String[] s = department.getToTime().split(":");
			if(s != null && s.length > 0 && s[0] != null){
				Long time = Long.parseLong(s[0].trim());
				if(time > 12){
					time = time - 12;
					result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(time+" PM"));
				}else{
					result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(time+" AM"));
				}
			}
			
		}else{
			result.put(CommonConstants.TOTIMEDISPLAY, String.valueOf(""));
		}
		
		
		result.put(CommonConstants.STATUS, String.valueOf(department.getStatus()));
		if(department.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(department.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(department.getIsManager() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(department.getIsManager());
			if(empList != null && empList.size() > 0){
				result.put(CommonConstants.EMPID, String.valueOf(department.getIsManager()));
 				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPID, "");
				result.put(CommonConstants.EMPNAME, "");
			}
			
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, "");
		}
		
		
		return result;
	}
}
