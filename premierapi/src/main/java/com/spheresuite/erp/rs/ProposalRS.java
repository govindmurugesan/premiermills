package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.InvoiceTermService;
import com.spheresuite.erp.service.ProposalDefaultService;
import com.spheresuite.erp.service.ProposalDetailsService;
import com.spheresuite.erp.service.ProposalService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ProposalUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/proposals")
public class ProposalRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ProposalRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String persist(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				ProposalDO proposalDO = new ProposalDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			proposalDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
			 		}

			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			proposalDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
			 			proposalDO.setProjectTypeId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
			 			proposalDO.setQunatity(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
			 		}
			 		proposalDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
			 		proposalDO.setRate(!inputJSON.get(CommonConstants.RATE).toString().isEmpty() ? inputJSON.get(CommonConstants.RATE).toString() : null);
			 		if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
			 			proposalDO.setPaymentTermId(Long.parseLong(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYMENTTERM).toString() : null));
			 		}
			 		if(!inputJSON.get(CommonConstants.INVOICETERM).toString().isEmpty()){
			 			proposalDO.setInvoiceTerm(inputJSON.get(CommonConstants.INVOICETERM).toString());
			 		}
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			proposalDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		proposalDO.setUpdatedon(new Date());
			 		proposalDO = new ProposalService().persist(proposalDO);
			 		if(!inputJSON.get(CommonConstants.INVOICETERM_LIST).toString().isEmpty()){
			 			List<InvoiceTermDO> invoiceTermList = new ArrayList<InvoiceTermDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.INVOICETERM_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			InvoiceTermDO invoiceTerm = new InvoiceTermDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			invoiceTerm.setDescripetion(inputJSON1.get(CommonConstants.COMMENT).toString());
				 			invoiceTerm.setProjectPercentage(inputJSON1.get(CommonConstants.PERCENTAGE).toString());
				 			invoiceTerm.setProposalId(proposalDO.getId());
				 			invoiceTerm.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			invoiceTerm.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			invoiceTerm.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
				 			invoiceTermList.add(invoiceTerm);
				 			
				 		}
				 		new InvoiceTermService().persist(invoiceTermList);
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.FIELD_LIST).toString().isEmpty()){
			 			List<ProposalDetailsDO> proposalDetailsList = new ArrayList<ProposalDetailsDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.FIELD_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDetailsDO proposalDetails = new ProposalDetailsDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDetails.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
				 			proposalDetails.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDetails.setProposalId(proposalDO.getId());
				 			proposalDetails.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			proposalDetails.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			proposalDetailsList.add(proposalDetails);
				 			
				 		}
				 		new ProposalDetailsService().persist(proposalDetailsList);
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.DEFAULT_LIST).toString().isEmpty()){
			 			List<ProposalDefaultDO> proposalDetailsList = new ArrayList<ProposalDefaultDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.DEFAULT_LIST));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDefaultDO proposalDefault = new ProposalDefaultDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDefault.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
				 			proposalDefault.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDefault.setProposalId(proposalDO.getId());
				 			proposalDefault.setUpdatedon(new Date());
				 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			proposalDefault.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 		}
				 			proposalDetailsList.add(proposalDefault);
				 			
				 		}
				 		new ProposalDefaultService().persist(proposalDetailsList);
			 		}
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Proposal Created");
			 		
			 		/*if(!inputJSON.get(CommonConstants.FILE).toString().isEmpty()){
			 			List<ProposalDocumentsDO> proposalDocumentList = new ArrayList<ProposalDocumentsDO>(); 
				 		JSONArray resultJSONArray = new JSONArray();
			 			resultJSONArray.put(inputJSON.get(CommonConstants.FILE));
				 		String rec = (String) resultJSONArray.get(0).toString();
				 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
				 		for (int i = 0; i < resultJSONArray1.size(); i++) {
				 			ProposalDocumentsDO proposalDocuments = new ProposalDocumentsDO(); 
				 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
				 			proposalDocuments.setProposalDocuments(inputJSON1.get(CommonConstants.VALUE).toString());
				 			proposalDocuments.setProposalId(proposalDO.getId());
				 			proposalDocuments.setUpdatedon(new Date());
				 			if(userServiceList != null && userServiceList.size() > 0){
				 				proposalDocuments.setUpdatedby(userServiceList.get(0).getName());
				 			}
				 			proposalDocumentList.add(proposalDocuments);
				 			
				 		}
				 		new ProposalDocumentsService().persist(proposalDocumentList);
			 		}*/
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProposalDO> proposalList = new ProposalService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				int i = 0;
				List<Long> deptIds = new ArrayList<Long>();
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
				}
				if(i==1){
					List<ProposalDO> proposalList = new ProposalService().retrieveAll();
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}else{
					//List<ProposalDO> proposalList = new ProposalService().retrieveAll();
					//respJSON = ProposalUtil.getProposalList(proposalList).toString();
					List<ProposalDO> proposalList = new ProposalService().retrieve(empIds);
					respJSON = ProposalUtil.getProposalList(proposalList).toString();
				}
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ProposalDO> ProposalDetail = new ProposalService().retrieveCustomerId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ProposalUtil.getProposalList(ProposalDetail).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ProposalDO proposalDO = new ProposalDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<ProposalDO> proposalList = new ProposalService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(proposalList != null && proposalList.size() > 0){
			 			proposalDO = proposalList.get(0);
				 		
			 			if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
				 			proposalDO.setProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
				 		}

				 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
				 			proposalDO.setCustomerId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
				 		}
				 		if(!inputJSON.get(CommonConstants.PROJECTTYPE).toString().isEmpty()){
				 			proposalDO.setProjectTypeId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTTYPE).toString()));
				 		}
				 		
				 		if(!inputJSON.get(CommonConstants.QUANTITY).toString().isEmpty()){
				 			proposalDO.setQunatity(Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()));
				 		}
				 		proposalDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
				 		proposalDO.setRate(!inputJSON.get(CommonConstants.RATE).toString().isEmpty() ? inputJSON.get(CommonConstants.RATE).toString() : null);
				 		if(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty()){
				 			proposalDO.setPaymentTermId(Long.parseLong(!inputJSON.get(CommonConstants.PAYMENTTERM).toString().isEmpty() ? inputJSON.get(CommonConstants.PAYMENTTERM).toString() : null));
				 		}
				 		if(!inputJSON.get(CommonConstants.INVOICETERM).toString().isEmpty()){
				 			proposalDO.setInvoiceTerm(inputJSON.get(CommonConstants.INVOICETERM).toString());
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		proposalDO.setUpdatedon(new Date());
				 		proposalDO = new ProposalService().update(proposalDO);
				 		List<InvoiceTermDO> invoiceTermList = new ArrayList<InvoiceTermDO>();
				 		if(!inputJSON.get(CommonConstants.INVOICETERM_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.INVOICETERM_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			InvoiceTermDO invoiceTerm = new InvoiceTermDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			invoiceTerm.setDescripetion(inputJSON1.get(CommonConstants.COMMENT).toString());
					 			invoiceTerm.setProjectPercentage(inputJSON1.get(CommonConstants.PERCENTAGE).toString());
					 			invoiceTerm.setProposalId(proposalDO.getId());
					 			invoiceTerm.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			invoiceTermList.add(invoiceTerm);
					 			
					 		}
					 		new InvoiceTermService().persist(invoiceTermList);
				 		}else{
				 			new InvoiceTermService().delete(proposalDO.getId());
				 		}
				 		
				 		List<ProposalDetailsDO> proposalDetailsList = new ArrayList<ProposalDetailsDO>(); 
				 		if(!inputJSON.get(CommonConstants.FIELD_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.FIELD_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDetailsDO proposalDetails = new ProposalDetailsDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDetails.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
					 			proposalDetails.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDetails.setProposalId(proposalDO.getId());
					 			proposalDetails.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			proposalDetailsList.add(proposalDetails);
					 		}
					 		new ProposalDetailsService().persist(proposalDetailsList);
				 		}else{
				 			new ProposalDetailsService().delete(proposalDO.getId());
				 		}
				 		
				 		List<ProposalDefaultDO> proposalDefaultList = new ArrayList<ProposalDefaultDO>(); 
				 		if(!inputJSON.get(CommonConstants.DEFAULT_LIST).toString().isEmpty()){
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.DEFAULT_LIST));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDefaultDO proposalDefault = new ProposalDefaultDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDefault.setProposalKey(inputJSON1.get(CommonConstants.KEY).toString());
					 			proposalDefault.setProposalValue(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDefault.setProposalId(proposalDO.getId());
					 			proposalDefault.setUpdatedon(new Date());
					 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
						 			proposalDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
						 		}
					 			proposalDefaultList.add(proposalDefault);
					 		}
					 		new ProposalDefaultService().persist(proposalDefaultList);
				 		}else{
				 			new ProposalDefaultService().delete(proposalDO.getId());
				 		}
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Proposal Updated");
				 		/*if(!inputJSON.get(CommonConstants.FILE).toString().isEmpty()){
				 			List<ProposalDocumentsDO> proposalDocumentList = new ArrayList<ProposalDocumentsDO>(); 
					 		JSONArray resultJSONArray = new JSONArray();
				 			resultJSONArray.put(inputJSON.get(CommonConstants.FILE));
					 		String rec = (String) resultJSONArray.get(0).toString();
					 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
					 		for (int i = 0; i < resultJSONArray1.size(); i++) {
					 			ProposalDocumentsDO proposalDocuments = new ProposalDocumentsDO(); 
					 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
					 			proposalDocuments.setProposalDocuments(inputJSON1.get(CommonConstants.VALUE).toString());
					 			proposalDocuments.setProposalId(proposalDO.getId());
					 			proposalDocuments.setUpdatedon(new Date());
					 			if(userServiceList != null && userServiceList.size() > 0){
					 				proposalDocuments.setUpdatedby(userServiceList.get(0).getName());
					 			}
					 			proposalDocumentList.add(proposalDocuments);
					 			
					 		}
					 		new ProposalDocumentsService().persist(proposalDocumentList);
				 		}*/
				 		
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
