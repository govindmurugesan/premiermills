package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.ProjectTypeDAO;
import com.spheresuite.erp.domainobject.ProjectTypeDO;
import com.spheresuite.erp.exception.AppException;

public class ProjectTypeService {
	static Logger logger = Logger.getLogger(ProjectTypeService.class.getName());
	
	public ProjectTypeDO persist(ProjectTypeDO projectTypeDO) throws AppException {
		return new ProjectTypeDAO().persist(projectTypeDO);
	}

	public List<ProjectTypeDO> retrieveActive() throws AppException {
		return new ProjectTypeDAO().retrieveActive();
	}
	
	public List<ProjectTypeDO> retrieveById(Long Id) throws AppException {
		return new ProjectTypeDAO().retrieveById(Id);
	}
	
	public List<ProjectTypeDO> retrieve() throws AppException {
		return new ProjectTypeDAO().retrieve();
	}
	
	public ProjectTypeDO update(ProjectTypeDO projectTypeDO) throws AppException {
		return new ProjectTypeDAO().update(projectTypeDO);
	}
}
