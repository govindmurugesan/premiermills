package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.RolesDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.service.ContactService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.RolesService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.ContactUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/contact")
public class ContactRS {

	String validation = null;
	static Logger logger = Logger.getLogger(ContactRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContactDO contactDO = new ContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){

			 		if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty() && inputJSON.get(CommonConstants.LEADTYPE) != null){
			 			contactDO.setLeadtype(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
			 			contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 		}
			 		
			 		contactDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		contactDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		contactDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		contactDO.setDesignation((!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		if(!inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
			 			contactDO.setContattype((Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString())));
			 		}
			 		
			 		contactDO.setPrimaryemail((!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : ""));
			 		contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : ""));
			 		contactDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : "");
			 		contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : "");
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			contactDO.setMobile1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
			 			contactDO.setMobile2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			contactDO.setPhone1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
			 			contactDO.setPhone2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE1).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
			 			contactDO.setPhone3(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
			 			contactDO.setPhone4(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE3).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.FAX).toString().isEmpty()){
			 			contactDO.setFax(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FAX).toString() : ""));
			 		}
			 		
			 		
			 		contactDO.setWebsite((!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : ""));
			 		contactDO.setRequirements((!inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : ""));
			 		contactDO.setNotes((!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : ""));
			 		contactDO.setComments((!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : ""));

			 		contactDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			contactDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		contactDO.setIsDeleted("n");
			 	}
				new ContactService().persist(contactDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ContactDO> contactList = new ContactService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<Long> deptIds = new ArrayList<Long>();
				int i = 0;
				if(request.getSession().getAttribute("deptIds") != null){
					deptIds = (List<Long>) request.getSession().getAttribute("deptIds");
				}
				List<Long> empIds = new ArrayList<Long>();
				if(deptIds != null && deptIds.size() > 0){
					empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}else if(request.getSession().getAttribute("empIds") != null){
					empIds = new ArrayList<Long>();
					Long empid = Long.parseLong(request.getSession().getAttribute("empIds").toString());
					empIds.add(empid);
					List<UserDO> userList = new UserService().retriveByEmpId(empid);
					if(userList.get(0).getRoleId() != null){
						List<RolesDO> roleList = new RolesService().retriveById(userList.get(0).getRoleId());
						if(roleList != null && roleList.size() > 0){
							if(roleList.get(0).getName().equalsIgnoreCase(CommonConstants.SUPERADMIN)){
								i=1;
							}
						}else{
							empIds = new ArrayList<Long>();
						}
					}else{
						empIds = new ArrayList<Long>();
					}
					//empIds = new EmployeeService().retrieveByDeptIds(deptIds);
				}
				
				if(i==1){
					List<ContactDO> contactList = new ContactService().retrieveAll();
					respJSON = ContactUtil.getContactList(contactList).toString();
				}else{
					List<ContactDO> contactList = new ContactService().retrieve(empIds);
					respJSON = ContactUtil.getContactList(contactList).toString();
					//List<ContactDO> contactList = new ContactService().retrieveAll();
					//respJSON = ContactUtil.getContactList(contactList).toString();
				}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				ContactDO contactDO = new ContactDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<ContactDO> contactList = new ContactService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		contactDO = contactList.get(0);
	 		if(!inputJSON.get(CommonConstants.LEADTYPE).toString().isEmpty() && inputJSON.get(CommonConstants.LEADTYPE) != null){
			 			contactDO.setLeadtype(Long.parseLong(inputJSON.get(CommonConstants.LEADTYPE).toString()));
			 		}
			 		if(!inputJSON.get(CommonConstants.SALUTATION).toString().isEmpty()){
			 			contactDO.setSalutation(Long.parseLong(inputJSON.get(CommonConstants.SALUTATION).toString()));
			 		}
			 		contactDO.setFirstname(!inputJSON.get(CommonConstants.FIRSTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.FIRSTNAME).toString() : "");
			 		contactDO.setLastname(!inputJSON.get(CommonConstants.LASTNAME).toString().isEmpty() ? inputJSON.get(CommonConstants.LASTNAME).toString() : "");
			 		contactDO.setMiddlename(!inputJSON.get(CommonConstants.MIDDLENAME).toString().isEmpty() ? inputJSON.get(CommonConstants.MIDDLENAME).toString() : "");
			 		contactDO.setDesignation((!inputJSON.get(CommonConstants.DESIGNATION).toString().isEmpty() ? inputJSON.get(CommonConstants.DESIGNATION).toString() : ""));
			 		if(!inputJSON.get(CommonConstants.CONTACTTYPE).toString().isEmpty()){
			 			contactDO.setContattype((Long.parseLong(inputJSON.get(CommonConstants.CONTACTTYPE).toString())));
			 		}
			 		contactDO.setPrimaryemail((!inputJSON.get(CommonConstants.PRIMARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PRIMARYEMAIL).toString() : ""));
			 		contactDO.setSecondaryemail((!inputJSON.get(CommonConstants.SECONDARYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.SECONDARYEMAIL).toString() : ""));
			 		contactDO.setAddress1(!inputJSON.get(CommonConstants.ADDRESS1).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS1).toString() : "");
			 		contactDO.setAddress2(!inputJSON.get(CommonConstants.ADDRESS2).toString().isEmpty() ? inputJSON.get(CommonConstants.ADDRESS2).toString() : "");
			 		if(!inputJSON.get(CommonConstants.MOBILE).toString().isEmpty()){
			 			contactDO.setMobile1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.MOBILE2).toString().isEmpty()){
			 			contactDO.setMobile2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.MOBILE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty()){
			 			contactDO.setPhone1(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE1).toString().isEmpty()){
			 			contactDO.setPhone2(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE1).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE2).toString().isEmpty()){
			 			contactDO.setPhone3(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE2).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.PHONE3).toString().isEmpty()){
			 			contactDO.setPhone4(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PHONE3).toString() : ""));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.FAX).toString().isEmpty()){
			 			contactDO.setFax(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.FAX).toString() : ""));
			 		}
			 		contactDO.setWebsite((!inputJSON.get(CommonConstants.WEBSITE).toString().isEmpty() ? inputJSON.get(CommonConstants.WEBSITE).toString() : ""));
			 		contactDO.setRequirements((!inputJSON.get(CommonConstants.REQUIREMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.REQUIREMENT).toString() : ""));
			 		contactDO.setNotes((!inputJSON.get(CommonConstants.NOTE).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTE).toString() : ""));
			 		contactDO.setComments((!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : ""));
			 		contactDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			contactDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new ContactService().update(contactDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Contact Updated");
			 	}

			 }else{
					return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<ContactDO> contactList = new ContactService().retriveByLeadId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = ContactUtil.getContactList(contactList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/importContact/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<ContactDO> contactDOlist = new ArrayList<ContactDO>();
				
				for (int i=0; i < fileData.size(); i++){
					ContactDO contactDO = new ContactDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.LEADTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString().isEmpty() && rowJSON.get(colName.get(CommonConstants.LEADTYPE)) != null){
				 			contactDO.setLeadtype(Long.parseLong(rowJSON.get(colName.get(CommonConstants.LEADTYPE)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.SALUTATION) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.SALUTATION)).toString().isEmpty()){
				 			contactDO.setSalutation(Long.parseLong(rowJSON.get(colName.get(CommonConstants.SALUTATION)).toString()));
				 		}
					}
			 		if(colName.get(CommonConstants.FIRSTNAME) != null){
				 		contactDO.setFirstname(!rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.FIRSTNAME)).toString() : "");
			 		}
					if(colName.get(CommonConstants.LASTNAME) != null){
				 		contactDO.setLastname(!rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.LASTNAME)).toString() : "");
					}
					if(colName.get(CommonConstants.MIDDLENAME) != null){
						contactDO.setMiddlename(!rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.MIDDLENAME)).toString() : "");
					}
					if(colName.get(CommonConstants.DESIGNATION) != null){
						contactDO.setDesignation((!rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.DESIGNATION)).toString() : ""));
					}
					if(colName.get(CommonConstants.CONTACTTYPE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CONTACTTYPE)).toString().isEmpty()){
				 			contactDO.setContattype((Long.parseLong(rowJSON.get(colName.get(CommonConstants.CONTACTTYPE)).toString())));
				 		}
					}
					if(colName.get(CommonConstants.PRIMARYEMAIL) != null){
				 		contactDO.setPrimaryemail((!rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.PRIMARYEMAIL)).toString() : ""));
					}
					if(colName.get(CommonConstants.PRIMARYEMAIL) != null){
				 		contactDO.setSecondaryemail((!rowJSON.get(colName.get(CommonConstants.SECONDARYEMAIL)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.SECONDARYEMAIL)).toString() : ""));
					}	
					if(colName.get(CommonConstants.ADDRESS1) != null){
						contactDO.setAddress1(!rowJSON.get(colName.get(CommonConstants.ADDRESS1)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS1)).toString() : "");
					}
					if(colName.get(CommonConstants.ADDRESS2) != null){
						contactDO.setAddress2(!rowJSON.get(colName.get(CommonConstants.ADDRESS2)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.ADDRESS2)).toString() : "");
					}
					if(colName.get(CommonConstants.MOBILE) != null){
						if(!rowJSON.get(colName.get(CommonConstants.MOBILE)).toString().isEmpty()){
				 			contactDO.setMobile1(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.MOBILE2) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.MOBILE2)).toString().isEmpty()){
				 			contactDO.setMobile2(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.MOBILE2)).toString() : ""));
				 		}
					}
				 		
					if(colName.get(CommonConstants.PHONE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE)).toString().isEmpty()){
				 			contactDO.setPhone1(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE1) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE1)).toString().isEmpty()){
				 			contactDO.setPhone2(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE1)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE2) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE2)).toString().isEmpty()){
				 			contactDO.setPhone3(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE2)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.PHONE3) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.PHONE3)).toString().isEmpty()){
				 			contactDO.setPhone4(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.PHONE3)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.FAX) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.FAX)).toString().isEmpty()){
				 			contactDO.setFax(Long.parseLong(rowJSON != null ? rowJSON.get(colName.get(CommonConstants.FAX)).toString() : ""));
				 		}
					}
					if(colName.get(CommonConstants.WEBSITE) != null){
				 		contactDO.setWebsite((!rowJSON.get(colName.get(CommonConstants.WEBSITE)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.WEBSITE)).toString() : ""));
					}
					if(colName.get(CommonConstants.REQUIREMENT) != null){
				 		contactDO.setRequirements((!rowJSON.get(colName.get(CommonConstants.REQUIREMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.REQUIREMENT)).toString() : ""));
					}
					if(colName.get(CommonConstants.COMMENT) != null){
				 		contactDO.setNotes((!rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.NOTES)).toString() : ""));
					}
					if(colName.get(CommonConstants.COMMENT) != null){
				 		contactDO.setComments((!rowJSON.get(colName.get(CommonConstants.COMMENT)).toString().isEmpty() ? rowJSON.get(colName.get(CommonConstants.COMMENT)).toString() : ""));
					}
			 		contactDO.setUpdatedon(new Date());
			 		if(updatedBy != null && ! updatedBy.toString().isEmpty()){
			 			contactDO.setUpdatedby(updatedBy.toString());
			 			contactDO.setEmpId(Long.parseLong(updatedBy.toString()));
			 		}
			 		contactDO.setIsDeleted("n");
					contactDOlist.add(contactDO);
				}
				
				new ContactService().persistList(contactDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public @ResponseBody String delete(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				List<ContactDO> contactDOList = new ArrayList<ContactDO>();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.IDS) != null){
			 		JSONArray resultJSONArray = new JSONArray();
			 		resultJSONArray.put(inputJSON.get(CommonConstants.IDS));
			 		String rec = (String) resultJSONArray.get(0).toString();
			 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
			 		String updatedBy = "";
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				updatedBy = inputJSON.get(CommonConstants.UPDATED_BY).toString();
			 		}
			 		for (int i = 0; i < resultJSONArray1.size(); i++) {
			 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
			 			boolean flag = true;
			 			if(!inputJSON1.get(CommonConstants.ID).toString().isEmpty()){
			 				
			 				List<OpportunitiesDO> opportunitiesList = new OpportunitiesService().retrieveByContactId(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
			 				if(opportunitiesList != null && opportunitiesList.size() > 0){
			 					flag = false;
			 				}
			 				
			 				
			 				if(resultJSONArray1.size() == 1){
			 					if(flag){
				 					List<ContactDO> contactList = new ContactService().retriveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(contactList != null && contactList.size() > 0){
				 						contactList.get(0).setIsDeleted("y");
				 						contactList.get(0).setUpdatedby(updatedBy);
				 						contactDOList.add(contactList.get(0));
				 					}
				 				}else{
				 					return CommonWebUtil.buildErrorResponse("Cannot Delete").toString();
			 					}
			 				}else{
			 					if(flag){
			 						List<ContactDO> contactList = new ContactService().retriveById(Long.parseLong(inputJSON1.get(CommonConstants.ID).toString()));
				 					if(contactList != null && contactList.size() > 0){
				 						contactList.get(0).setIsDeleted("y");
				 						contactList.get(0).setUpdatedby(updatedBy);
				 						contactDOList.add(contactList.get(0));
				 					}
				 				}
			 				}
			 				
			 			}
			 		}
			 		new ContactService().update(contactDOList);
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
