package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="employeecertification")
@TableGenerator(name ="employeecertification", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "employeecertification.findAll", query = "SELECT r FROM EmployeeCertificationDO r"),
    @NamedQuery(name = "employeecertification.findById", query = "SELECT r FROM EmployeeCertificationDO r where r.id =:id"),
    @NamedQuery(name = "employeecertification.findByEmpId", query = "SELECT r FROM EmployeeCertificationDO r where r.empId =:empId order by r.id desc"),
})
public class EmployeeCertificationDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "employeecertification.findAll";
	
	public static final String FIND_BY_ID = "employeecertification.findById";
	
	public static final String FIND_BY_EMPID = "employeecertification.findByEmpId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "employeecertification")
	private Long id;
	private String month;
	private String title;
	private String comment;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String updatedBy;
	private Long createdBy;
	private Long empId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
}