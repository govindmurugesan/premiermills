package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.StateDAO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;

public class StateService {
	static Logger logger = Logger.getLogger(StateService.class.getName());
	
	public StateDO persist(StateDO countryDO) throws AppException {
		return new StateDAO().persist(countryDO);
	}

	public List<StateDO> retrieveActive() throws AppException {
		return new StateDAO().retrieveActive();
	}
	
	public List<StateDO> retrieve() throws AppException {
		return new StateDAO().retrieve();
	}
	
	public List<StateDO> retrieveById(Long Id) throws AppException {
		return new StateDAO().retrieveById(Id);
	}
	
	public List<StateDO> retrieveByCountryId(Long Id) throws AppException {
		return new StateDAO().retrieveByCountryId(Id);
	}
	
	public StateDO update(StateDO countryDO) throws AppException {
		return new StateDAO().update(countryDO);
	}
}
