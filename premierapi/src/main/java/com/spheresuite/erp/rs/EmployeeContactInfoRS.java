package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.EmployeeContactInfoDO;
import com.spheresuite.erp.service.EmployeeContactInfoService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmployeeContactInfoUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/employeecontactinfo")
public class EmployeeContactInfoRS {

	String validation = null;
	static Logger logger = Logger.getLogger(EmployeeContactInfoRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeContactInfoDO employeeContactInfoDO = new EmployeeContactInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		employeeContactInfoDO.setHomePhone(!inputJSON.get(CommonConstants.HOMEPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.HOMEPHONE).toString()) : null);
			 		employeeContactInfoDO.setCellPhone(!inputJSON.get(CommonConstants.CELLPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.CELLPHONE).toString()) : null);
			 		employeeContactInfoDO.setWorkPhone(!inputJSON.get(CommonConstants.WORKPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.WORKPHONE).toString()) : null);
			 		employeeContactInfoDO.setCompanyEmail(!inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANYEMAIL).toString() : null);
			 		employeeContactInfoDO.setPersonalEmail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
			 		employeeContactInfoDO.setOtherEmail(!inputJSON.get(CommonConstants.OTHEREMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.OTHEREMAIL).toString() : null);
			 		employeeContactInfoDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		employeeContactInfoDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeContactInfoDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			employeeContactInfoDO.setCreatedBy(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 	}
				new EmployeeContactInfoService().persist(employeeContactInfoDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<EmployeeContactInfoDO> employeeContactInfoList = new EmployeeContactInfoService().retrieveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = EmployeeContactInfoUtil.getEmployeeContactInfoList(employeeContactInfoList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				EmployeeContactInfoDO employeeContactInfoDO = new EmployeeContactInfoDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<EmployeeContactInfoDO> employeeContactInfoList = new EmployeeContactInfoService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		employeeContactInfoDO = employeeContactInfoList.get(0);
			 		employeeContactInfoDO.setHomePhone(!inputJSON.get(CommonConstants.HOMEPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.HOMEPHONE).toString()) : null);
			 		employeeContactInfoDO.setCellPhone(!inputJSON.get(CommonConstants.CELLPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.CELLPHONE).toString()) : null);
			 		employeeContactInfoDO.setWorkPhone(!inputJSON.get(CommonConstants.WORKPHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.WORKPHONE).toString()) : null);
			 		employeeContactInfoDO.setCompanyEmail(!inputJSON.get(CommonConstants.COMPANYEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.COMPANYEMAIL).toString() : null);
			 		employeeContactInfoDO.setPersonalEmail(!inputJSON.get(CommonConstants.PERSONALEMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.PERSONALEMAIL).toString() : null);
			 		employeeContactInfoDO.setOtherEmail(!inputJSON.get(CommonConstants.OTHEREMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.OTHEREMAIL).toString() : null);
			 		employeeContactInfoDO.setEmpId(!inputJSON.get(CommonConstants.EMPID).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()) : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			employeeContactInfoDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		new EmployeeContactInfoService().update(employeeContactInfoDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Allowance Type Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
