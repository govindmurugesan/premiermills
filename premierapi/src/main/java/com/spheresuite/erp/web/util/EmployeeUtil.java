package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeTypeDO;
import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.domainobject.UserDO;
import com.spheresuite.erp.domainobject.WorkLocationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.DepartmentService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.EmployeeTypeService;
import com.spheresuite.erp.service.StateService;
import com.spheresuite.erp.service.UserService;
import com.spheresuite.erp.service.WorkLocationService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeUtil {
	
	private EmployeeUtil() {}
	
	public static JSONObject getEmployeeList(List<EmployeeDO> employeeList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeDO employee : employeeList) {
				resultJSONArray.put(getEmployeeDetailObject(employee));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeListWithOutPhoto(List<EmployeeDO> employeeList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeDO employee : employeeList) {
				resultJSONArray.put(getEmployeeDetailObjectWithOutPhoto(employee));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getEmployeeListWithCount(List<EmployeeDO> employeeList, int count)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			resultJSON.put(CommonConstants.COUNT, count);
			JSONArray resultJSONArray = new JSONArray();
			if(employeeList != null && employeeList.size() > 0){
				for (EmployeeDO employee : employeeList) {
					resultJSONArray.put(getEmployeeDetailObject(employee));
				}
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	

	public static JSONObject getEmployeeDetailObject(EmployeeDO employee)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employee.getId()));
		result.put(CommonConstants.WORKLOCATION, String.valueOf(employee.getWorkLocationId()));
		if(employee.getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(employee.getEmpId()));
		}else{
			result.put(CommonConstants.EMPID,"");
		}
		String name="";
		if(employee.getFirstname() != null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(employee.getFirstname()));
			name = name+employee.getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME,"");
		}
		if(employee.getMiddlename() != null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(employee.getMiddlename()));
			name = name+" "+employee.getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME,"");
		}
		if(employee.getLastname() != null){
			result.put(CommonConstants.LASTNAME, String.valueOf(employee.getLastname()));
			name = name+" "+employee.getLastname();
		}else{
			result.put(CommonConstants.LASTNAME,"");
		}
		
		result.put(CommonConstants.NAME, name);
		if(employee.getPersonalcontact() != null){
			result.put(CommonConstants.PERSONALCONTACT, String.valueOf(employee.getPersonalcontact()));
		}else{
			result.put(CommonConstants.PERSONALCONTACT,"");
		}
		if(employee.getPrimarycontact() != null){
			result.put(CommonConstants.PRIMARYCONTACT, String.valueOf(employee.getPrimarycontact()));
		}else{
			result.put(CommonConstants.PRIMARYCONTACT,"");
		}
		if(employee.getCompanyemail() != null){
			result.put(CommonConstants.COMPANYEMAIL, String.valueOf(employee.getCompanyemail()));
		}else{
			result.put(CommonConstants.COMPANYEMAIL,"");
		}
		if(employee.getPersonalemail() != null){
			result.put(CommonConstants.PERSONALEMAIL, String.valueOf(employee.getPersonalemail()));
		}else{
			result.put(CommonConstants.PERSONALEMAIL,"");
		}
		if(employee.getAadhar() != null){
			result.put(CommonConstants.AADHAR, String.valueOf(employee.getAadhar()));
		}else{
			result.put(CommonConstants.AADHAR,"");
		}
		if(employee.getPanno() != null){
			result.put(CommonConstants.PANNO, String.valueOf(employee.getPanno()));
		}else{
			result.put(CommonConstants.PANNO,"");
		}
		if(employee.getDateofbirth() != null){
			result.put(CommonConstants.DATEOFBIRTH, String.valueOf(employee.getDateofbirth()));
		}else{
			result.put(CommonConstants.DATEOFBIRTH,"");
		}
		
		if(employee.getTitle() != null){
			result.put(CommonConstants.TITLE, String.valueOf(employee.getTitle()));
		}else{
			result.put(CommonConstants.TITLE,"");
		}
		
		
		if(employee.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(employee.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		
		result.put(CommonConstants.GENDER, String.valueOf(employee.getGender()));
		result.put(CommonConstants.EMAIL, String.valueOf(employee.getEmailId()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employee.getUpdatedon())));
		if(employee.getUpdatedby() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employee.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		WorkLocationService workservice = new WorkLocationService();
		List<WorkLocationDO> workList = workservice.retrieveActiveById(employee.getWorkLocationId());
		String state="";
		if(workList != null && workList.size()> 0){
			if(workList.get(0).getStateId() != null){
				List<StateDO> stateList = new StateService().retrieveById(workList.get(0).getStateId());
				if(stateList != null && stateList.size() > 0){
					state = stateList.get(0).getName();
				}
			}
			String country="";
			if(workList.get(0).getCountryId() != null){
				List<CountryDO> countryList = new CountryService().retrieveById(workList.get(0).getCountryId());
				if(countryList != null && countryList.size() > 0){
					country = countryList.get(0).getName();
				}
			}
			result.put(CommonConstants.WORKLOCATION_CITY, String.valueOf(workList.get(0).getCity()+", "+state+", "+country));
			
		}else{
			result.put(CommonConstants.WORKLOCATION_CITY,"");
		}
		if(employee.getEmptype() != null){
			EmployeeTypeService emptypeObject = new EmployeeTypeService();
			List<EmployeeTypeDO> employeeDOList = emptypeObject.retrieveById(employee.getEmptype());
			if(employeeDOList != null && employeeDOList.size() > 0){
				result.put(CommonConstants.EMPTYPE_NAME, String.valueOf(employeeDOList.get(0).getName()));
			}else{
				result.put(CommonConstants.EMPTYPE_NAME,"");
			}
			result.put(CommonConstants.EMPTYPE, String.valueOf(employee.getEmptype()));
		}else{
			result.put(CommonConstants.EMPTYPE,"");
			result.put(CommonConstants.EMPTYPE_NAME,"");
		}
		
		if(employee.getReportto() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveById(employee.getReportto());
			if(employeeList != null && employeeList.size() > 0){
				String firstName="";
				String middleName="";
				String lastName="";
				if(employeeList.get(0).getFirstname() != null){
					firstName = employeeList.get(0).getFirstname();
				}
				if(employeeList.get(0).getLastname() != null){
					middleName = employeeList.get(0).getLastname(); 		
				}
				if(employeeList.get(0).getMiddlename() != null){
					lastName = employeeList.get(0).getMiddlename();
				}
				result.put(CommonConstants.REPORTTO_NAME, firstName+" "+middleName+" "+lastName);
				result.put(CommonConstants.REPORTTO,String.valueOf(employee.getReportto()));
			}else{
				result.put(CommonConstants.REPORTTO_NAME,"");
				result.put(CommonConstants.REPORTTO,"");
			}
		}else{
			result.put(CommonConstants.REPORTTO_NAME,"");
			result.put(CommonConstants.REPORTTO,"");
		}
		DepartmentService deptservice = new DepartmentService();
		if(employee.getEmpdept() != null){
			List<DepartmentDO> deptserviceList = deptservice.retrieveById(employee.getEmpdept());
			if(deptserviceList != null && deptserviceList.size() > 0){
				result.put(CommonConstants.DEPT_NAME, String.valueOf(deptserviceList.get(0).getName()));
				result.put(CommonConstants.DEPT, String.valueOf(employee.getEmpdept()));
			}else{
				result.put(CommonConstants.DEPT_NAME,"");
				result.put(CommonConstants.DEPT, String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.DEPT_NAME,"");
			result.put(CommonConstants.DEPT, String.valueOf(""));
		}
		
		if(employee.getPhoto() != null){
			result.put(CommonConstants.PHOTO,employee.getPhoto());
		}else{
			result.put(CommonConstants.PHOTO,"");
		}
		result.put(CommonConstants.STATUS,String.valueOf(employee.getStatus()));
		return result;
	}
	
	public static JSONObject getEmployeeDetailObjectWithOutPhoto(EmployeeDO employee)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employee.getId()));
		result.put(CommonConstants.WORKLOCATION, String.valueOf(employee.getWorkLocationId()));
		if(employee.getEmpId() != null){
			result.put(CommonConstants.EMPID, String.valueOf(employee.getEmpId()));
		}else{
			result.put(CommonConstants.EMPID,"");
		}
		result.put(CommonConstants.STATUS,String.valueOf(employee.getStatus()));
		String name="";
		if(employee.getFirstname() != null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(employee.getFirstname()));
			name = name+employee.getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME,"");
		}
		if(employee.getMiddlename() != null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(employee.getMiddlename()));
			name = name+" "+employee.getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME,"");
		}
		if(employee.getLastname() != null){
			result.put(CommonConstants.LASTNAME, String.valueOf(employee.getLastname()));
			name = name+" "+employee.getLastname();
		}else{
			result.put(CommonConstants.LASTNAME,"");
		}
		
		result.put(CommonConstants.NAME, name);
		if(employee.getPersonalcontact() != null){
			result.put(CommonConstants.PERSONALCONTACT, String.valueOf(employee.getPersonalcontact()));
		}else{
			result.put(CommonConstants.PERSONALCONTACT,"");
		}
		if(employee.getPrimarycontact() != null){
			result.put(CommonConstants.PRIMARYCONTACT, String.valueOf(employee.getPrimarycontact()));
		}else{
			result.put(CommonConstants.PRIMARYCONTACT,"");
		}
		if(employee.getCompanyemail() != null){
			result.put(CommonConstants.COMPANYEMAIL, String.valueOf(employee.getCompanyemail()));
		}else{
			result.put(CommonConstants.COMPANYEMAIL,"");
		}
		if(employee.getPersonalemail() != null){
			result.put(CommonConstants.PERSONALEMAIL, String.valueOf(employee.getPersonalemail()));
		}else{
			result.put(CommonConstants.PERSONALEMAIL,"");
		}
		if(employee.getAadhar() != null){
			result.put(CommonConstants.AADHAR, String.valueOf(employee.getAadhar()));
		}else{
			result.put(CommonConstants.AADHAR,"");
		}
		if(employee.getPanno() != null){
			result.put(CommonConstants.PANNO, String.valueOf(employee.getPanno()));
		}else{
			result.put(CommonConstants.PANNO,"");
		}
		if(employee.getDateofbirth() != null){
			result.put(CommonConstants.DATEOFBIRTH, String.valueOf(employee.getDateofbirth()));
		}else{
			result.put(CommonConstants.DATEOFBIRTH,"");
		}
		if(employee.getEmptype() != null){
			result.put(CommonConstants.EMPTYPE, String.valueOf(employee.getEmptype()));
		}else{
			result.put(CommonConstants.EMPTYPE,"");
		}
		if(employee.getReportto() != null){
			result.put(CommonConstants.REPORTTO, String.valueOf(employee.getReportto()));
		}else{
			result.put(CommonConstants.REPORTTO,"");
		}
		if(employee.getEmpdept() != null){
			result.put(CommonConstants.DEPT, String.valueOf(employee.getEmpdept()));
		}else{
			result.put(CommonConstants.DEPT,"");
		}
		if(employee.getTitle() != null){
			result.put(CommonConstants.TITLE, String.valueOf(employee.getTitle()));
		}else{
			result.put(CommonConstants.TITLE,"");
		}
		if(employee.getJobdesc() != null){
			result.put(CommonConstants.JOBDESC, String.valueOf(employee.getJobdesc()));
		}else{
			result.put(CommonConstants.JOBDESC,"");
		}
		if(employee.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(employee.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(employee.getEmailId() != null){
			result.put(CommonConstants.EMAIL, String.valueOf(employee.getEmailId()));
		}else{
			result.put(CommonConstants.EMAIL, "");
		}
		
		if(employee.getEmailPassword() != null){
			result.put(CommonConstants.PASSWORD, String.valueOf(employee.getEmailPassword()));
		}else{
			result.put(CommonConstants.PASSWORD, "");
		}
		
		if(employee.getEmailType() != null){
			result.put(CommonConstants.TYPE, String.valueOf(employee.getEmailType()));
		}else{
			result.put(CommonConstants.TYPE, "");
		}
		
		result.put(CommonConstants.GENDER, String.valueOf(employee.getGender()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(employee.getUpdatedon())));
		if(employee.getUpdatedby() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(employee.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		WorkLocationService workservice = new WorkLocationService();
		List<WorkLocationDO> workList = workservice.retrieveActiveById(employee.getWorkLocationId());
		String state="";
		if(workList != null && workList.size()> 0){
			if(workList.get(0).getStateId() != null){
				List<StateDO> stateList = new StateService().retrieveById(workList.get(0).getStateId());
				if(stateList != null && stateList.size() > 0){
					state = stateList.get(0).getName();
				}
			}
			String country="";
			if(workList.get(0).getCountryId() != null){
				List<CountryDO> countryList = new CountryService().retrieveById(workList.get(0).getCountryId());
				if(countryList != null && countryList.size() > 0){
					country = countryList.get(0).getName();
				}
			}
			result.put(CommonConstants.WORKLOCATION_CITY, String.valueOf(workList.get(0).getCity()+", "+state+", "+country));
			
		}else{
			result.put(CommonConstants.WORKLOCATION_CITY,"");
		}
		if(employee.getEmptype() != null){
			EmployeeTypeService emptypeObject = new EmployeeTypeService();
			List<EmployeeTypeDO> employeeDOList = emptypeObject.retrieveById(employee.getEmptype());
			if(employeeDOList != null && employeeDOList.size() > 0){
				result.put(CommonConstants.EMPTYPE_NAME, String.valueOf(employeeDOList.get(0).getName()));
			}else{
				result.put(CommonConstants.EMPTYPE_NAME,"");
			}
			result.put(CommonConstants.EMPTYPE, String.valueOf(employee.getEmptype()));
		}else{
			result.put(CommonConstants.EMPTYPE,"");
			result.put(CommonConstants.EMPTYPE_NAME,"");
		}
		
		
		if(employee.getReportto() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveById(employee.getReportto());
			if(employeeList != null && employeeList.size() > 0){
				String firstName="";
				String middleName="";
				String lastName="";
				if(employeeList.get(0).getFirstname() != null){
					firstName = employeeList.get(0).getFirstname();
				}
				if(employeeList.get(0).getLastname() != null){
					middleName = employeeList.get(0).getLastname(); 		
				}
				if(employeeList.get(0).getMiddlename() != null){
					lastName = employeeList.get(0).getMiddlename();
				}
				result.put(CommonConstants.REPORTTO_NAME, firstName+" "+middleName+" "+lastName);
				result.put(CommonConstants.REPORTTO,String.valueOf(employee.getReportto()));
			}else{
				result.put(CommonConstants.REPORTTO_NAME,"");
				result.put(CommonConstants.REPORTTO,String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.REPORTTO_NAME,"");
			result.put(CommonConstants.REPORTTO,String.valueOf(""));
		}
		DepartmentService deptservice = new DepartmentService();
		if(employee.getEmpdept() != null){
			List<DepartmentDO> deptserviceList = deptservice.retrieveById(employee.getEmpdept());
			if(deptserviceList != null && deptserviceList.size() > 0){
				result.put(CommonConstants.DEPT, String.valueOf(employee.getEmpdept()));
				result.put(CommonConstants.DEPT_NAME, String.valueOf(deptserviceList.get(0).getName()));
			}else{
				result.put(CommonConstants.DEPT_NAME,"");
				result.put(CommonConstants.DEPT, String.valueOf(""));
			}
		}else{
			result.put(CommonConstants.DEPT_NAME,"");
			result.put(CommonConstants.DEPT, String.valueOf(""));
		}
		return result;
	}
	
	
	public static JSONObject getEmployeeReportingList(List<EmployeeDO> employeeList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeDO employee : employeeList) {
				resultJSONArray.put(getEmployeeReportingDetailObject(employee));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static JSONObject getEmployeeReportingDetailObject(EmployeeDO employee)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employee.getId()));
		if(employee.getReportto() != null){                   
			result.put(CommonConstants.REPORTTO, String.valueOf(employee.getReportto()));
		}else{
			result.put(CommonConstants.REPORTTO,"");
		}
		
		if(employee.getReportto() != null){
			List<EmployeeDO> employeeList = new EmployeeService().retriveById(employee.getReportto());
			if(employeeList != null && employeeList.size() > 0){
				String firstName="";
				String middleName="";
				String lastName="";
				if(employeeList.get(0).getFirstname() != null){
					firstName = employeeList.get(0).getFirstname();
				}
				if(employeeList.get(0).getLastname() != null){
					middleName = employeeList.get(0).getLastname(); 		
				}
				if(employeeList.get(0).getMiddlename() != null){
					lastName = employeeList.get(0).getMiddlename();
				}
				result.put(CommonConstants.REPORTTO_NAME, firstName+" "+middleName+" "+lastName);
				result.put(CommonConstants.REPORTTO_EMAIL, employeeList.get(0).getCompanyemail() );
			}else{
				result.put(CommonConstants.REPORTTO_NAME,"");
				result.put(CommonConstants.REPORTTO_EMAIL, "" );
			}
		}else{
			result.put(CommonConstants.REPORTTO_NAME,"");
		}
		
		if(employee.getEmpdept() != null){
			result.put(CommonConstants.DEPT, String.valueOf(employee.getEmpdept()));
		}else{
			result.put(CommonConstants.DEPT,"");
		}
		
		DepartmentService deptservice = new DepartmentService();
		
		if(employee.getEmpdept() != null){
			List<DepartmentDO> deptserviceList = deptservice.retrieveById(employee.getEmpdept());
			if(deptserviceList != null && deptserviceList.size() > 0){
				result.put(CommonConstants.DEPT_NAME, String.valueOf(deptserviceList.get(0).getName()));
				List<EmployeeDO> deptMangr = new EmployeeService().retriveById(deptserviceList.get(0).getIsManager());
				if(deptMangr != null && deptMangr.size() > 0){
					String firstName="";
					String middleName="";
					String lastName="";
					if(deptMangr.get(0).getFirstname() != null){
						firstName = deptMangr.get(0).getFirstname();
					}
					if(deptMangr.get(0).getLastname() != null){
						middleName = deptMangr.get(0).getLastname(); 		
					}
					if(deptMangr.get(0).getMiddlename() != null){
						lastName = deptMangr.get(0).getMiddlename();
					}
					result.put(CommonConstants.DEPTMNGR_NAME, firstName+" "+middleName+" "+lastName);
					result.put(CommonConstants.DEPTMNGR_ID, String.valueOf(deptserviceList.get(0).getIsManager()));
					result.put(CommonConstants.DEPHEAD_EMAIL, String.valueOf(deptMangr.get(0).getCompanyemail()));
				}else{
					result.put(CommonConstants.DEPTMNGR_NAME,"");
					result.put(CommonConstants.DEPTMNGR_ID, "");
					result.put(CommonConstants.DEPHEAD_EMAIL, "");
				}
			}
		}else{
			result.put(CommonConstants.DEPT_NAME,"");
		}
		return result;
	}
	
}
