package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.HrPoliciesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class HrPoliciesDAO {
	static Logger logger = Logger.getLogger(HrPoliciesDAO.class.getName());
	private EntityManager em = null;
	
	public HrPoliciesDO persist(HrPoliciesDO hrPoliciesDOList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && hrPoliciesDOList != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(hrPoliciesDOList);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return hrPoliciesDOList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieve() throws AppException {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HrPoliciesDO.FIND_ALL);
				hrPoliciesList = (List<HrPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return hrPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieveById(Long Id) throws AppException {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HrPoliciesDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				hrPoliciesList = (List<HrPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return hrPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<HrPoliciesDO> retrieveByRoleId(Long Id) throws AppException {
		List<HrPoliciesDO> hrPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(HrPoliciesDO.FIND_BY_ROLEID);
				q.setParameter(CommonConstants.ID, Id);
				hrPoliciesList = (List<HrPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return hrPoliciesList;
	}
	
	public HrPoliciesDO update(HrPoliciesDO hrPoliciesDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(hrPoliciesDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return hrPoliciesDO;
	}

}
