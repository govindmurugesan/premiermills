package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PaymentModeDAO;
import com.spheresuite.erp.domainobject.PaymentModeDO;
import com.spheresuite.erp.exception.AppException;

public class PaymentModeService {
	static Logger logger = Logger.getLogger(HolidaysService.class.getName());
	
	public PaymentModeDO persist(PaymentModeDO paymentMode) throws AppException {
		return new PaymentModeDAO().persist(paymentMode);
	}
	
	public List<PaymentModeDO> retrieveById(Long Id) throws AppException {
		return new PaymentModeDAO().retrieveById(Id);
	}
	
	public List<PaymentModeDO> retrieve() throws AppException {
		return new PaymentModeDAO().retrieve();
	}
	
	public PaymentModeDO update(PaymentModeDO paymentDetail) throws AppException {
		return new PaymentModeDAO().update(paymentDetail);
	}
}
