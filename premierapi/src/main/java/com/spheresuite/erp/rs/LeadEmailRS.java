package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.LeadEmailDO;
import com.spheresuite.erp.service.LeadEmailService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.EmailProxyUtil;
import com.spheresuite.erp.web.util.LeadEmailUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/leademail")
public class LeadEmailRS {

	String validation = null;
	static Logger logger = Logger.getLogger(LeadEmailRS.class.getName());
	
	@RequestMapping(value = "/persist/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String add(@PathVariable String inputParams,Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				
				try {
					List<LeadEmailDO> emails = new ArrayList<LeadEmailDO>();
					Collections.reverse(emails);
					Session session = null;
					String file="INBOX";
					URLName url = new URLName(CommonConstants.IMAPS,"imap.gmail.com",993,file,CommonConstants.USERNAME, CommonConstants.EMAILPASSWORD); 
					if (session == null) {
			            Properties props = null;
			            try {
			                props = System.getProperties();
			            } catch (SecurityException sex) {
			                props = new Properties();
			            }
			            session = Session.getInstance(props, null);
			        }
			        Store store = session.getStore(url);
			        store.connect();
			        Folder folder = store.getFolder(url);
			        folder.open(Folder.READ_WRITE);
			        Message[] arrayMessages = folder.getMessages();
			        /*Folder[] folderList = store.getDefaultFolder().list();*/
					JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
					/*List<LeadEmailDO> leadEmailDOlist = new ArrayList<LeadEmailDO>();*/
					if(emails != null && emails.size() > 0){
						java.util.Date utilDate = new java.util.Date(emails.get(0).getSentDate().getTime());
						SearchTerm sender = new FromTerm(new InternetAddress("lshruthi@saptalabs.com"));
						arrayMessages = folder.search(sender);
						EmailProxyUtil.getEmailBasedOnDate(inputJSON, arrayMessages, utilDate,"lshruthi@saptalabs.com");
					}else{
						SearchTerm sender = new FromTerm(new InternetAddress("lshruthi@saptalabs.com"));
						arrayMessages = folder.search(sender);
						EmailProxyUtil.getAllEmails(inputJSON, arrayMessages);
					}
					store.close();
				} catch (NoSuchProviderException ex) {
					ex.printStackTrace();
				} catch (MessagingException ex) {
					ex.printStackTrace();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveByLeadId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByLeadId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
				List<LeadEmailDO> LeadList = new LeadEmailService().retrieveByLead(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
			}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request,HttpServletResponse response) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<LeadEmailDO> LeadList = new LeadEmailService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					response.setContentType("application/x-msdownload");            
					response.setHeader("Content-disposition", "filename=\"" +  LeadList.get(0).getAttachFiles() + "\"");
					byte[] fileBytes=LeadList.get(0).getAttachmentFile();
					ServletOutputStream out = response.getOutputStream();
					out.write(fileBytes);
					out.close();
				}
			
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveAllMail", method = RequestMethod.GET)
	public @ResponseBody String retrieveAllMail(Model model, HttpServletRequest request,HttpServletResponse response) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				
					List<LeadEmailDO> LeadList = new LeadEmailService().retrieveAllMail();
					respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/retrieveInbox", method = RequestMethod.GET)
	public @ResponseBody String retrieveInbox(Model model, HttpServletRequest request,HttpServletResponse response) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				Session session = null;
				String file="INBOX";
				URLName url = new URLName(CommonConstants.IMAPS,"imap.gmail.com",993,file,CommonConstants.USERNAME, CommonConstants.EMAILPASSWORD); 
				if (session == null) {
		            Properties props = null;
		            try {
		                props = System.getProperties();
		            } catch (SecurityException sex) {
		                props = new Properties();
		            }
		            props.put("mail.smtp.port", "587");
	            	props.put("mail.smtp.auth", "true");
	            	props.put("mail.smtp.starttls.enable", "true");
		            session = Session.getInstance(props, null);
		        }
		        Store store = session.getStore(url);
		        store.connect();
		        Folder folder = store.getFolder(url);
		        folder.open(Folder.READ_WRITE);
		        Message[] arrayMessages = folder.getMessages();
				/*SearchTerm sender = new FromTerm(new InternetAddress("lshruthi@saptalabs.com"));
				arrayMessages = folder.search(sender);*/
				List<LeadEmailDO> LeadList = EmailProxyUtil.getAllInbox(arrayMessages);
				respJSON = LeadEmailUtil.getLeadEmailList(LeadList).toString();
				store.close();
					
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
