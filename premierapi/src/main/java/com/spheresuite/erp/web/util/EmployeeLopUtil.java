
package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.EmployeeLopDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class EmployeeLopUtil {
	
	private EmployeeLopUtil() {}
	
	public static JSONObject getEmployeeLopList(List<EmployeeLopDO> employeeLopList, String frequency, String period)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			if(frequency != null){
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(frequency));
			}else{
				resultJSON.put(CommonConstants.FREQUENCY, String.valueOf(""));
			}
			
			if(period != null){
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(period));
			}else{
				resultJSON.put(CommonConstants.PERIOD, String.valueOf(""));
			}
			JSONArray resultJSONArray = new JSONArray();
			for (EmployeeLopDO employeeLopDO : employeeLopList) {
				resultJSONArray.put(getEmployeeLopDetailObject(employeeLopDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getEmployeeLopDetailObject(EmployeeLopDO employeeLopDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(employeeLopDO.getId()));
		result.put(CommonConstants.STARTDATE,String.valueOf(employeeLopDO.getStartdate() != null ? CommonUtil.convertDateToStringWithOutComma(employeeLopDO.getStartdate()) : ""));
		result.put(CommonConstants.ENDDATE,String.valueOf(employeeLopDO.getEnddate() != null ? CommonUtil.convertDateToStringWithOutComma(employeeLopDO.getEnddate()) : ""));
		result.put(CommonConstants.STATUS, String.valueOf(employeeLopDO.getStatus()));
		if(employeeLopDO.getEmpId() != null ){
			result.put(CommonConstants.EMPID, String.valueOf(employeeLopDO.getEmpId()));
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(employeeLopDO.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.EMPNAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.EMPNAME, ""); 
			}
		}else{
			result.put(CommonConstants.EMPID, "");
			result.put(CommonConstants.EMPNAME, ""); 
		}
		
		return result;
	}
}
