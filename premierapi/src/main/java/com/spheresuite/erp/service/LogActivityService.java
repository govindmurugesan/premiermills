package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LogActivityDAO;
import com.spheresuite.erp.domainobject.LogActivityDO;
import com.spheresuite.erp.exception.AppException;

public class LogActivityService {
	static Logger logger = Logger.getLogger(LogActivityService.class.getName());
	
	public LogActivityDO persist(LogActivityDO logDO) throws AppException {
		return new LogActivityDAO().persist(logDO);
	}
	
	public LogActivityDO update(LogActivityDO logDO) throws AppException {
		return new LogActivityDAO().update(logDO);
	}

	public List<LogActivityDO> retrieveById(Long Id) throws AppException {
		return new LogActivityDAO().retrieveById(Id);
	}

	public List<LogActivityDO> retrieveByLeadId(Long Id) throws AppException {
		return new LogActivityDAO().retrieveByLeadId(Id);
	}

	
	public List<LogActivityDO> retrieve(String type) throws AppException {
		return new LogActivityDAO().retrieve(type);
	}
}
