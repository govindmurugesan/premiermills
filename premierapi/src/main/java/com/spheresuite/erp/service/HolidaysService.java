package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HolidaysDAO;
import com.spheresuite.erp.domainobject.HolidaysDO;
import com.spheresuite.erp.exception.AppException;

public class HolidaysService {
	static Logger logger = Logger.getLogger(HolidaysService.class.getName());
	
	public HolidaysDO persist(HolidaysDO holidayDetail) throws AppException {
		return new HolidaysDAO().persist(holidayDetail);
	}
	
	public List<HolidaysDO> retrieveById(Long Id) throws AppException {
		return new HolidaysDAO().retrieveById(Id);
	}
	
	public List<HolidaysDO> retrieveByHoliday(String desc, String year, String date) throws AppException {
		return new HolidaysDAO().retrieveByHoliday(desc, year, date);
	}
	
	public List<HolidaysDO> retrieveByHolidayForUpdate(Long id, String desc, String year, String date) throws AppException {
		return new HolidaysDAO().retrieveByHolidayForUpdate(id, desc, year, date);
	}
	
	public List<HolidaysDO> retrieve() throws AppException {
		return new HolidaysDAO().retrieve();
	}
	
	public HolidaysDO update(HolidaysDO holidayDetail) throws AppException {
		return new HolidaysDAO().update(holidayDetail);
	}
}
