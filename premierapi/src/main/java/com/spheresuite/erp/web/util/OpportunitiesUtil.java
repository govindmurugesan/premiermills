package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.OpportunitiesDocDO;
import com.spheresuite.erp.domainobject.ProjectTypeDO;
import com.spheresuite.erp.domainobject.SalesStageDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.ContactService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.OpportunitiesDocService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.ProjectTypeService;
import com.spheresuite.erp.service.SalesStageService;
import com.spheresuite.erp.util.CommonConstants;

public class OpportunitiesUtil {
	
	private OpportunitiesUtil() {}
	
	public static JSONObject getProjectList(List<OpportunitiesDO> projectList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OpportunitiesDO project : projectList) {
				resultJSONArray.put(getProjectDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectListWithOutDoc(List<OpportunitiesDO> projectList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (OpportunitiesDO project : projectList) {
				resultJSONArray.put(getProjectWithOutDocDetailObject(project));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	public static JSONObject getProjectChartList(List<SalesStageDO> salesStageList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (SalesStageDO salesStage : salesStageList) {
				resultJSONArray.put(getProjectForChartDetailObject(salesStage));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
		
	}
	
	public static JSONObject getProjectForChartDetailObject(SalesStageDO salesStage)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		Long totalAmount = new OpportunitiesService().retrieveBySalesStageId(salesStage.getId());
		if(totalAmount == null){
			totalAmount = 0L;
		}
		result.put(CommonConstants.NAME, String.valueOf(salesStage.getName()));
		result.put(CommonConstants.VALUE, String.valueOf(totalAmount));
		return result;
	}

	public static JSONObject getProjectDetailObject(OpportunitiesDO project)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getId()));
		
		if(project.getCustomerId() != null){
			List<LeadDO> leadList = new LeadService().retrieveById(project.getCustomerId());
			result.put(CommonConstants.CUSTOMERID, String.valueOf(project.getCustomerId()));
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, String.valueOf(leadList.get(0).getName()));
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");
			}
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getProjectcode() != null){
			result.put(CommonConstants.PROJECTCODE, String.valueOf(project.getProjectcode()));
		}else{
			result.put(CommonConstants.PROJECTCODE,"");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(project.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(project.getUpdatedby() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(project.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(project.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		if(project.getProjectType() != null){
			List<ProjectTypeDO> projectTypeList = new ProjectTypeService().retrieveById(project.getProjectType());
			if(projectTypeList != null && projectTypeList.size() > 0){
				result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(projectTypeList.get(0).getName()));
				result.put(CommonConstants.PROJECTTYPE, String.valueOf(project.getProjectType()));
			}else{
				result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(project.getContactId() != null){
			result.put(CommonConstants.CONTACT_ID, String.valueOf(project.getContactId()));
			List<ContactDO> contactList = new ContactService().retriveById(project.getContactId());
			if(contactList != null && contactList.size() > 0 ){
				String name = "";
				if(contactList.get(0).getFirstname() != null){
					name = name + contactList.get(0).getFirstname();
				}else if(contactList.get(0).getMiddlename() != null){
					name = name + contactList.get(0).getMiddlename();
				}else if(contactList.get(0).getLastname() != null){
					name = name + contactList.get(0).getLastname();
				}
				result.put(CommonConstants.CONTACT_NAME, name);
			}else{
				result.put(CommonConstants.CONTACT_NAME, "");
			}
		}else{
			result.put(CommonConstants.CONTACT_ID, "");
			result.put(CommonConstants.CONTACT_NAME, "");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getSalesStage() != null){
			result.put(CommonConstants.SALESSTAGE, String.valueOf(project.getSalesStage()));
			List<SalesStageDO> salesStageList = new  SalesStageService().retrieveById(project.getSalesStage());
			if(salesStageList != null && salesStageList.size() > 0){
				result.put(CommonConstants.SALESSTAGE_NAME, salesStageList.get(0).getName());
			}else{
				result.put(CommonConstants.SALESSTAGE_NAME, "");
			}
		}else{
			result.put(CommonConstants.SALESSTAGE, "");
		}
		
		JSONArray resultJSONArray = new JSONArray();
		
		List<OpportunitiesDocDO> opportunitiesDocDOList = new OpportunitiesDocService().retrieveByOppId(project.getId());
		if(opportunitiesDocDOList != null && opportunitiesDocDOList.size()  > 0){
			for (OpportunitiesDocDO doc : opportunitiesDocDOList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	
	public static JSONObject getDocDetailObject(OpportunitiesDocDO doc)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
	

	public static JSONObject getProjectWithOutDocDetailObject(OpportunitiesDO project)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(project.getId()));
		
		if(project.getCustomerId() != null){
			List<LeadDO> leadList = new LeadService().retrieveById(project.getCustomerId());
			result.put(CommonConstants.CUSTOMERID, String.valueOf(project.getCustomerId()));
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.CUSTOMERNAME, String.valueOf(leadList.get(0).getName()));
			}else{
				result.put(CommonConstants.CUSTOMERNAME, "");
			}
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.CUSTOMERNAME, "");
		}
		if(project.getProjectname() != null){
			result.put(CommonConstants.PROJECTNAME, String.valueOf(project.getProjectname()));
		}else{
			result.put(CommonConstants.PROJECTNAME,"");
		}
		if(project.getDisplayname() != null){
			result.put(CommonConstants.DISPLAYNAME, String.valueOf(project.getDisplayname()));
		}else{
			result.put(CommonConstants.DISPLAYNAME,"");
		}
		if(project.getProjectcode() != null){
			result.put(CommonConstants.PROJECTCODE, String.valueOf(project.getProjectcode()));
		}else{
			result.put(CommonConstants.PROJECTCODE,"");
		}
		if(project.getStartdate() != null){
			result.put(CommonConstants.STARTDATE, String.valueOf(project.getStartdate()));
		}else{
			result.put(CommonConstants.STARTDATE,"");
		}
		if(project.getEnddate() != null){
			result.put(CommonConstants.ENDDATE, String.valueOf(project.getEnddate()));
		}else{
			result.put(CommonConstants.ENDDATE,"");
		}
		if(project.getEmpId() != null){
			result.put(CommonConstants.UPDATED_BY, String.valueOf(project.getEmpId()));
		}else{
			result.put(CommonConstants.UPDATED_BY,"");
		}
		if(project.getProjectduration() != null){
			result.put(CommonConstants.PROJECTDURATION, String.valueOf(project.getProjectduration()));
		}else{
			result.put(CommonConstants.PROJECTDURATION,"");
		}
		if(project.getComment() != null){
			result.put(CommonConstants.COMMENT, String.valueOf(project.getComment()));
		}else{
			result.put(CommonConstants.COMMENT,"");
		}
		if(project.getUpdatedby() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(project.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(project.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY_NAME, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY_NAME, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		if(project.getUpdatedon() != null){
			result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(project.getUpdatedon())));
		}else{
			result.put(CommonConstants.UPDATEDON,"");
		}
		if(project.getProjectType() != null){
			List<ProjectTypeDO> projectTypeList = new ProjectTypeService().retrieveById(project.getProjectType());
			if(projectTypeList != null && projectTypeList.size() > 0){
				result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(projectTypeList.get(0).getName()));
				result.put(CommonConstants.PROJECTTYPE, String.valueOf(project.getProjectType()));
			}else{
				result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			result.put(CommonConstants.PROJECTTYPE, "");
		}
		
		if(project.getContactId() != null){
			result.put(CommonConstants.CONTACT_ID, String.valueOf(project.getContactId()));
			List<ContactDO> contactList = new ContactService().retriveById(project.getContactId());
			if(contactList != null && contactList.size() > 0 ){
				result.put(CommonConstants.CONTACT_NAME, "");
			}else{
				result.put(CommonConstants.CONTACT_NAME, "");
			}
		}else{
			result.put(CommonConstants.CONTACT_ID, "");
			result.put(CommonConstants.CONTACT_NAME, "");
		}
		
		if(project.getAmount() != null){
			result.put(CommonConstants.COST, String.valueOf(project.getAmount()));
		}else{
			result.put(CommonConstants.COST, "");
		}
		
		if(project.getProbability() != null){
			result.put(CommonConstants.PROBOBILITY, String.valueOf(project.getProbability()));
		}else{
			result.put(CommonConstants.PROBOBILITY, "");
		}
		
		if(project.getSalesStage() != null){
			result.put(CommonConstants.SALESSTAGE, String.valueOf(project.getSalesStage()));
			List<SalesStageDO> salesStageList = new  SalesStageService().retrieveById(project.getSalesStage());
			if(salesStageList != null && salesStageList.size() > 0){
				result.put(CommonConstants.SALESSTAGE_NAME, salesStageList.get(0).getName());
			}else{
				result.put(CommonConstants.SALESSTAGE_NAME, "");
			}
		}else{
			result.put(CommonConstants.SALESSTAGE, "");
		}
		return result;
	}
	
}
