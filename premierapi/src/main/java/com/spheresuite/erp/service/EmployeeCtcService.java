package com.spheresuite.erp.service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeCtcDAO;
import com.spheresuite.erp.domainobject.EmployeeCtcDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeCtcService {
	static Logger logger = Logger.getLogger(EmployeeCtcService.class.getName());
	
	public EmployeeCtcDO persist(EmployeeCtcDO empCompensationDO) throws AppException {
		return new EmployeeCtcDAO().persist(empCompensationDO);
	}

	public List<EmployeeCtcDO> retrieveActive(Long empId) throws AppException {
		return new EmployeeCtcDAO().retrieveActive(empId);
	}
	
	public List<EmployeeCtcDO> retrieveActiveWithDate(Long empId, Date date) throws AppException {
		return new EmployeeCtcDAO().retrieveActiveWithDate(empId, date);
	}
	
	public List<EmployeeCtcDO> retrieveById(Long id) throws AppException {
		return new EmployeeCtcDAO().retrieveById(id);
	}
	
	public List<Long> retrieveByBatchId(Long id) throws AppException {
		return new EmployeeCtcDAO().retrieveByBatchId(id);
	}
	
	public List<EmployeeCtcDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeCtcDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeCtcDO> retrieve() throws AppException {
		return new EmployeeCtcDAO().retrieve();
	}
	
	public EmployeeCtcDO update(EmployeeCtcDO empCompensationDO) throws AppException {
		return new EmployeeCtcDAO().update(empCompensationDO);
	}
}
