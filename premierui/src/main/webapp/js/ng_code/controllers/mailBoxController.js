angular
    .module('spheresuite')
    .controller('mailBoxController', mailBoxController);

mailBoxController.$inject = ['$scope', '$rootScope', '$location', '$localStorage', 'emailService', 'employeeService', 'Upload'];

function mailBoxController($scope, $rootScope, $location, $localStorage, emailService, employeeService, Upload) {
    var mailBoxControllerScope = this;
	
    $rootScope.headerMenu = "Mailbox";
    
    mailBoxControllerScope.checkbox = false;
    mailBoxControllerScope.divValue = 'Inbox';
	//mailBoxControllerScope.isSelectProvider = true;
    mailBoxControllerScope.isShowMail = false;
    mailBoxControllerScope.spinner = false;

    mailBoxControllerScope.isBcc = false;
    mailBoxControllerScope.isCc = false;
    mailBoxControllerScope.currentMailIndex = 0;
    mailBoxControllerScope.inboxList = [];
    mailBoxControllerScope.loginData;
    mailBoxControllerScope.loginForm;
    mailBoxControllerScope.mail;
    mailBoxControllerScope.mailIndex = 1;
    mailBoxControllerScope.mailList = [];
    mailBoxControllerScope.mailOfCount = 0;
    mailBoxControllerScope.mailToShowCount = 50;
    mailBoxControllerScope.msg;
    mailBoxControllerScope.sentmailList = [];    
    mailBoxControllerScope.inbox;
    mailBoxControllerScope.sentmail;
    
    mailBoxControllerScope.changeDiv = changeDiv;
    mailBoxControllerScope.decline = decline;
    mailBoxControllerScope.emailLogin = emailLogin;
    mailBoxControllerScope.getInbox = getInbox;
    mailBoxControllerScope.getMails = getMails;
    mailBoxControllerScope.getNextMail = getNextMail;
    mailBoxControllerScope.getNextMailSet = getNextMailSet;
    mailBoxControllerScope.getPrevMail = getPrevMail;
    mailBoxControllerScope.getPrevMailSet = getPrevMailSet;
    mailBoxControllerScope.getSentmail = getSentmail;
    mailBoxControllerScope.isEnterKeyPressed = isEnterKeyPressed;
    mailBoxControllerScope.logout = logout;
    //mailBoxControllerScope.selectProvider = selectProvider;
    mailBoxControllerScope.showMail = showMail;
    mailBoxControllerScope.showMailList = showMailList;
    mailBoxControllerScope.toggleCheck = toggleCheck;
    mailBoxControllerScope.readmail = false;
    mailBoxControllerScope.toggleCc = toggleCc;
    mailBoxControllerScope.toggleBcc = toggleBcc;
    mailBoxControllerScope.declineEmail = declineEmail;
    mailBoxControllerScope.getEmployeeUser = getEmployeeUser;
    mailBoxControllerScope.composeDiv = composeDiv;
    mailBoxControllerScope.sendEmail = sendEmail;
    mailBoxControllerScope.sendEmailAttachments = sendEmailAttachments;
    mailBoxControllerScope.addAttachments = addAttachments;
    mailBoxControllerScope.isEmail = false;
    mailBoxControllerScope.email = {};
	
    if($location.path() == '/mailbox' && $localStorage.spheresuite && $localStorage.spheresuite.id){
    	getInbox($localStorage.spheresuite.id);
		getEmployeeUser();
    }

    function changeDiv(selectedDiv) {
    	mailBoxControllerScope.mailList = [];
        if (selectedDiv == 'Inbox') {
        	mailBoxControllerScope.divValue = 'Inbox';
            mailBoxControllerScope.isShowMail = false;
        	if(!mailBoxControllerScope.inboxList || mailBoxControllerScope.inboxList.length == 0){
        		getInbox($localStorage.spheresuite.id);
        	}else{console.log('inbox   ====  ',angular.copy(mailBoxControllerScope.inboxList));
        		mailBoxControllerScope.mailList = angular.copy(mailBoxControllerScope.inboxList);
        	}
        } else if (selectedDiv == 'Sent Mail') {
        	mailBoxControllerScope.divValue = 'Sent Mail';
            mailBoxControllerScope.isShowMail = false;
        	if(!mailBoxControllerScope.sentmailList || mailBoxControllerScope.sentmailList.length == 0){
        		getSentmail($localStorage.spheresuite.id);
        	}else{
                console.log(angular.copy(mailBoxControllerScope.sentmailList));
        		mailBoxControllerScope.mailList = angular.copy(mailBoxControllerScope.sentmailList);
        	}
        }
		mailBoxControllerScope.currentMailIndex = 0;
		if(mailBoxControllerScope.mailList.length > mailBoxControllerScope.mailToShowCount)
			mailBoxControllerScope.mailOfCount = angular.copy(mailBoxControllerScope.mailToShowCount);
		else
			mailBoxControllerScope.mailOfCount = mailBoxControllerScope.mailList.length;
	}
    
    function declineEmail() {
    	mailBoxControllerScope.isCc = false;
    	mailBoxControllerScope.isBcc = false;
		mailBoxControllerScope.email = {};
    	mailBoxControllerScope.emailForm.$setPristine();
    	mailBoxControllerScope.emailForm.$setUntouched();
    }
    
    function toggleCc() {
    	mailBoxControllerScope.isCc = !mailBoxControllerScope.isCc;
    }

    function toggleBcc() {
    	mailBoxControllerScope.isBcc = !mailBoxControllerScope.isBcc;
    }
    
    function emailLogin(){console.log("login", mailBoxControllerScope.loginData)
    	if(mailBoxControllerScope.loginData){
            mailBoxControllerScope.spinner = true;
    		mailBoxControllerScope.loginData.id = $localStorage.spheresuite.id;
    		emailService.emailLogin(mailBoxControllerScope.loginData).then(function(res){
    			if(res.successflag == 'true'){
			    	$rootScope.configuremail = true;
    				$location.path('/mailbox');
    			}else{
			    	$rootScope.configuremail = false;
    			}
    			mailBoxControllerScope.spinner = false;
    		},function(err){
		    	$rootScope.configuremail = false;
		        mailBoxControllerScope.spinner = false;
    		});
    	}
    }
    
    function decline(){
    	mailBoxControllerScope.loginData = null;
        //mailBoxControllerScope.isSelectProvider = true;
        if(mailBoxControllerScope.loginForm){
        	mailBoxControllerScope.loginForm.$setPristine();
        	mailBoxControllerScope.loginForm.$setUntouched();
        }
    }
    
    function getInbox(data){
        mailBoxControllerScope.spinner = true;
        mailBoxControllerScope.msg = '';
    	emailService.getInbox(data).then(function(res){
    		console.log("get mail",res)
    		if(res && res.successflag == 'true' && res.results.length > 0){
    			mailBoxControllerScope.inboxList = angular.copy(res.results);
    			angular.forEach(mailBoxControllerScope.inboxList,function(val){
    				val.name = val.address.substring(0,val.address.indexOf(' <'));
    				val.isChecked = mailBoxControllerScope.checkbox;    				

//    			    if(val.attachFile != ''){
//    			    	var contentType = val.contentType.substring(0, val.contentType.indexOf(';') + 1);
//    			    	val.ext = val.contentType.substring(0, contentType.indexOf('/'));
//    			    	val.attachedFile = 'data:' + contentType + 'base64,' + val.attachment;
//    			    	delete val['attachment'];
//    			    	delete val['contentType'];
//    			    }
    				
    			});
    			mailBoxControllerScope.currentMailIndex = 0;
    	    	if(mailBoxControllerScope.divValue == 'Inbox'){
    	    		mailBoxControllerScope.mailList =  angular.copy(mailBoxControllerScope.inboxList);
    	    	}
    			if(mailBoxControllerScope.inboxList.length > mailBoxControllerScope.mailToShowCount){
    				mailBoxControllerScope.mailOfCount = angular.copy(mailBoxControllerScope.mailToShowCount);
    			}
    			else{
    				mailBoxControllerScope.mailOfCount = mailBoxControllerScope.inboxList.length;
    			}
        	    mailBoxControllerScope.spinner = false;
    		}else{
    		    mailBoxControllerScope.msg = "Mails Not Available";
        		mailBoxControllerScope.mailOfCount = 0;
        	    mailBoxControllerScope.spinner = false;
    		}
    	},function(err){
		    mailBoxControllerScope.msg = "Mails Not Available";
			mailBoxControllerScope.mailOfCount = 0;
    	    mailBoxControllerScope.spinner = false;
    	});
    }
    
    function getMails(){
    	if(mailBoxControllerScope.divValue == 'Inbox'){
    		getInbox($localStorage.spheresuite.id);
    	}else if(mailBoxControllerScope.divValue == 'Sent Mail'){
        	getSentmail($localStorage.spheresuite.id);
        }
    }
    
   function getSentmail(data){
        mailBoxControllerScope.spinner = true;
        mailBoxControllerScope.msg = '';
    	emailService.getSentmail(data).then(function(res){
    		if(res.successflag == 'true' && res.results.length > 0){
    			mailBoxControllerScope.sentmailList = angular.copy(res.results);
    			angular.forEach(mailBoxControllerScope.sentmailList,function(val){
    				val.name = val.to.substring(0,val.to.indexOf(' <'));
    				val.isChecked = mailBoxControllerScope.checkbox; 				

    			    if(val.attachFile != ''){
    			    	var contentType = val.contentType.substring(0, val.contentType.indexOf(';') + 1);
    			    	val.ext = val.contentType.substring(0, contentType.indexOf('/'));
    			    	val.attachedFile = 'data:' + contentType + 'base64,' + val.attachment;
    			    	delete val['attachment'];
    			    	delete val['contentType'];
    			    }
    				
    			});
    			mailBoxControllerScope.currentMailIndex = 0;
    	    	if(mailBoxControllerScope.divValue == 'Sent Mail'){
    	    		mailBoxControllerScope.mailList = angular.copy(mailBoxControllerScope.sentmailList);
    	    	}
    			if(mailBoxControllerScope.sentmailList.length > mailBoxControllerScope.mailToShowCount){
    				mailBoxControllerScope.mailOfCount = angular.copy(mailBoxControllerScope.mailToShowCount);
    			}
    			else{
    				mailBoxControllerScope.mailOfCount = mailBoxControllerScope.sentmailList.length;
    			}
        	    mailBoxControllerScope.spinner = false;
    		}else {
    		    mailBoxControllerScope.msg = "Sent Mails Not Available";
        		mailBoxControllerScope.mailOfCount = 0;
        	    mailBoxControllerScope.spinner = false;
    		}
    	},function(err){
		    mailBoxControllerScope.msg = "Sent Mails Not Available";
    		mailBoxControllerScope.mailOfCount = 0;
    	    mailBoxControllerScope.spinner = false;
    	});
    }  

    function getNextMail(type){
        mailBoxControllerScope.spinner = true;
		if(mailBoxControllerScope.mailList.length > 0 && mailBoxControllerScope.mailList[mailBoxControllerScope.mailIndex + 1]){
            console.log(mailBoxControllerScope.mailList[mailBoxControllerScope.mailIndex + 1]);
			showMail(mailBoxControllerScope.mailList[mailBoxControllerScope.mailIndex + 1]['uId'], mailBoxControllerScope.mailIndex + 1, mailBoxControllerScope.divValue);
			if(mailBoxControllerScope.mailIndex == mailBoxControllerScope.mailOfCount){
				getNextMailSet(type);
			}
		}else{
            mailBoxControllerScope.spinner = false; 
        }        
    }

    function getNextMailSet(){
        mailBoxControllerScope.spinner = true; 
    	if((mailBoxControllerScope.mailOfCount < mailBoxControllerScope.mailList.length) && !mailBoxControllerScope.isShowMail){
	    	if((mailBoxControllerScope.mailOfCount + mailBoxControllerScope.mailToShowCount) <= mailBoxControllerScope.mailList.length){
		    	mailBoxControllerScope.mailOfCount += mailBoxControllerScope.mailToShowCount;
	    	}else {
		    	mailBoxControllerScope.mailOfCount = mailBoxControllerScope.mailList.length;
		    }
	    	mailBoxControllerScope.currentMailIndex += mailBoxControllerScope.mailToShowCount;
    	}
        mailBoxControllerScope.spinner = false; 
    }
    
    
    function getPrevMail(type){
        mailBoxControllerScope.spinner = true;
		if(mailBoxControllerScope.mailList.length > 0 && mailBoxControllerScope.mailList[mailBoxControllerScope.mailIndex - 1]){
			showMail(mailBoxControllerScope.mailList[mailBoxControllerScope.mailIndex - 1]['uId'],mailBoxControllerScope.mailIndex - 1, mailBoxControllerScope.divValue); 
			if(mailBoxControllerScope.mailIndex == mailBoxControllerScope.mailOfCount){
				getPrevMailSet(type);
			}
		} else {
            mailBoxControllerScope.spinner = false; 
        }
    }
    
    function getPrevMailSet(){
        mailBoxControllerScope.spinner = true;
    	if((mailBoxControllerScope.currentMailIndex - mailBoxControllerScope.mailToShowCount) >= 0){
		    if(!mailBoxControllerScope.isShowMail && (mailBoxControllerScope.mailOfCount > mailBoxControllerScope.mailToShowCount) && (mailBoxControllerScope.mailOfCount %  mailBoxControllerScope.mailToShowCount) == 0){
	        	mailBoxControllerScope.mailOfCount -= mailBoxControllerScope.mailToShowCount;
	        }else{
	        	mailBoxControllerScope.mailOfCount -= mailBoxControllerScope.mailOfCount %  mailBoxControllerScope.mailToShowCount;
	        }
	    	mailBoxControllerScope.currentMailIndex -= mailBoxControllerScope.mailToShowCount;
        }
        mailBoxControllerScope.spinner = false; 
    }
    
    function isEnterKeyPressed(keyCode){
    	if(keyCode && keyCode == 13){
    		if(mailBoxControllerScope.loginData && mailBoxControllerScope.loginData.email && mailBoxControllerScope.loginData.password && keyCode){
    			emailLogin();
        	}
    	}
    }
    
    function logout(){
    	if($localStorage.spheresuite && $localStorage.spheresuite.id){
	        mailBoxControllerScope.spinner = true;
			emailService.emailLogout($localStorage.spheresuite.id).then(function(res){
		        mailBoxControllerScope.spinner = true;
				if(res.successflag == 'true'){
			    	$rootScope.configuremail = false;
					$location.path('/mailbox/configuremail');
				}
			},function(err){
		        mailBoxControllerScope.spinner = true;
			});
		}
    }
    
    /*function selectProvider(provider){
    	if(provider){
    		mailBoxControllerScope.loginData = {type: provider};
    		mailBoxControllerScope.isSelectProvider = false;
    	}
    }*/

    function showMail(uid, index, selectedDiv){
    	if(uid && (index || index === 0)){
            mailBoxControllerScope.spinner = true;
            var data = {
            		empId: $localStorage.spheresuite.id,
            		id: uid,
                    label: selectedDiv
            }
            emailService.getSingleMail(data).then(function(res){
            	console.log('getSingleMail res',res)
            	if(res.successflag == 'true' && res.results.length > 0){
	    		    mailBoxControllerScope.mail = res.results[0];
                    mailBoxControllerScope.readmail = true;
	    		    if(mailBoxControllerScope.mail.attachFile != ''){
    			    	var contentType = mailBoxControllerScope.mail.contentType.substring(0, mailBoxControllerScope.mail.contentType.indexOf(';') + 1);
    			    	mailBoxControllerScope.mail.ext = mailBoxControllerScope.mail.contentType.substring(0, contentType.indexOf('/'));
    			    	mailBoxControllerScope.mail.attachedFile = 'data:' + contentType + 'base64,' + mailBoxControllerScope.mail.attachment;
//    			    	delete val['attachment'];
//    			    	delete val['contentType'];
    			    }
	    		    if(mailBoxControllerScope.isShowMail){
	    		    	mailBoxControllerScope.mailIndex = (mailBoxControllerScope.currentMailIndex) + index;
	    		    }else{
	    		    	mailBoxControllerScope.mailIndex = (mailBoxControllerScope.currentMailIndex - 1) + index;		    	
	    		    }
	    	    	mailBoxControllerScope.isShowMail = true;
                    angular.forEach(mailBoxControllerScope.mailList, function(key){
                        if(key.uId == uid){
                            key.emailseen = true;
                        }
                    })
            	}
    	        mailBoxControllerScope.spinner = false;

            },function(err){
    	        mailBoxControllerScope.spinner = false;
            	
            });    
    	}
       /* mailBoxControllerScope.mail = uid;
        if(mailBoxControllerScope.mail.attachFile != ''){
            var contentType = mailBoxControllerScope.mail.contentType.substring(0, mailBoxControllerScope.mail.contentType.indexOf(';') + 1);
            mailBoxControllerScope.mail.ext = mailBoxControllerScope.mail.contentType.substring(0, contentType.indexOf('/'));
            mailBoxControllerScope.mail.attachedFile = 'data:' + contentType + 'base64,' + mailBoxControllerScope.mail.attachment;
//                      delete val['attachment'];
//                      delete val['contentType'];
        }
        if(mailBoxControllerScope.isShowMail){
            mailBoxControllerScope.mailIndex = (mailBoxControllerScope.currentMailIndex) + index;
        }else{
            mailBoxControllerScope.mailIndex = (mailBoxControllerScope.currentMailIndex - 1) + index;               
        }
        mailBoxControllerScope.isShowMail = true;*/
    }
    
    function showMailList(){
    	mailBoxControllerScope.isShowMail = false;
        mailBoxControllerScope.readmail = true;
    }
    
    function toggleCheck(type){
		angular.forEach(mailBoxControllerScope.mailList,function(val, key){
			if(mailBoxControllerScope.currentMailIndex <= key && mailBoxControllerScope.mailOfCount >= key)
				val.isChecked = mailBoxControllerScope.checkbox;
		});
    }
	
	function getEmployeeUser() {
        mailBoxControllerScope.spinner = true;
        var myUser = $localStorage.spheresuite.id;
        employeeService.getEmployee(myUser).then(function(res) {
        	console.log('getEmployee(myUser)', res)
               if (res.successflag === 'true' && res.results.length > 0) {
                    mailBoxControllerScope.noEmployeeDetail = true;
                    mailBoxControllerScope.userEmployeeList = res.results[0];
                    console.log('getEmployee(myUser)  =====   ', mailBoxControllerScope.userEmployeeList)
                }   else {
                    mailBoxControllerScope.spinner = false; 
                    mailBoxControllerScope.dataMsg = "Information Not Avalible";
                }
        }, function(err) {
            mailBoxControllerScope.spinner = false;
        });
    }
    
    function composeDiv() {
    	if(mailBoxControllerScope.userEmployeeList){
    		if(mailBoxControllerScope.userEmployeeList.email != ''){
    			mailBoxControllerScope.isEmail = true;
    			mailBoxControllerScope.email.from = mailBoxControllerScope.userEmployeeList.email;
        	}
    	}
    }
    
    function sendEmail() {
    	filesName = [];
    	var filesType = [];
    	mailBoxControllerScope.spinner = true;
    	 if (mailBoxControllerScope.email) {
    		 mailBoxControllerScope.email.password = mailBoxControllerScope.userEmployeeList.password;
    		 if(mailBoxControllerScope.email.cc == undefined || mailBoxControllerScope.email.cc == '' || mailBoxControllerScope.email.cc == 'undefined'){
    			 mailBoxControllerScope.email.cc = '';
    		 }
    		 if(mailBoxControllerScope.email.bcc == undefined || mailBoxControllerScope.email.bcc == '' || mailBoxControllerScope.email.bcc == 'undefined'){
    			 mailBoxControllerScope.email.bcc = '';
    		 }
    		 if(mailBoxControllerScope.email.file && mailBoxControllerScope.email.file != ""){
         		for(i in mailBoxControllerScope.email.file){
         			filesName.push(mailBoxControllerScope.email.file[i].name)
         			filesType.push(mailBoxControllerScope.email.file[i].type)
         			/*mailBoxControllerScope.email.type = mailBoxControllerScope.email.file[i].type
         			mailBoxControllerScope.email.fileName = mailBoxControllerScope.email.file[i].name;*/
         		}
             	Upload
					.base64DataUrl(mailBoxControllerScope.email.file)
					.then(function(urls) {
						//mailBoxControllerScope.email.file = urls;//[0].split(',')[1];
						addAttachments(urls, filesName, filesType, mailBoxControllerScope.email);
					});
         	}else{
         		//mailBoxControllerScope.email.file = '';
         		/*mailBoxControllerScope.email.type = '';
         		mailBoxControllerScope.email.fileName = '';*/
         		sendEmailAttachments(mailBoxControllerScope.email);
         	}
         }
    }
    
    function addAttachments(files, filesName, filesType, data){
    	console.log('files    5555  ',files)
    	if(files){
    		for(var i = 0; i < files.length; i++){
		    	emailService.uploadAttachment({file: files[i].split(',')[1], fileName: filesName[i], type: filesType[i]}).then(function(res1){
					console.log('email ',res1)
		    		if(i == files.length){
		    			sendEmailAttachments(data);
		    		}
				},function(err){
					console.log('err1',err1);
				});
    		}
    	}
    }
    
    function sendEmailAttachments(data){
    	mailBoxControllerScope.spinner = true;
    	if(data){
	    	emailService.sendEmail(data).then(function(res) {
	            if (res.successflag === 'true') {
	         	    mailBoxControllerScope.spinner = false;
	         	    mailBoxControllerScope.isCc = false;
	         	    mailBoxControllerScope.isBcc = false;
	         	    mailBoxControllerScope.email = {};
	             }else {
	             	mailBoxControllerScope.spinner = false;
	             	mailBoxControllerScope.isCc = false;
	         	    mailBoxControllerScope.isBcc = false;
	             	mailBoxControllerScope.email = {};
	             }
		     }, function(err) {
		         mailBoxControllerScope.spinner = false;
		         mailBoxControllerScope.isCc = false;
         	     mailBoxControllerScope.isBcc = false;
		         mailBoxControllerScope.email = {};
		     });
    	}
    }
}  