package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.EmployeeBonusDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class EmployeeBonusDAO {
	static Logger logger = Logger.getLogger(EmployeeBonusDAO.class.getName());
	private EntityManager em = null;
	
	public EmployeeBonusDO persist(EmployeeBonusDO employeeBonusDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && employeeBonusDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(employeeBonusDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpId(Long empId) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID);
				q.setParameter(CommonConstants.EMPID, empId);
				employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdWithDate(List<Long> empIds, String bonusMonth) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(empIds != null && empIds.size() > 0){
					Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_MONTH);
					q.setParameter(CommonConstants.EMPID, empIds);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter(CommonConstants.MONTHLY, bonusMonth);
					employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
				}else{
					Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_MONTH);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter(CommonConstants.MONTHLY, bonusMonth);
					employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdBetweenDate(List<Long> empIds, String fromMonth, String toMonth) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				if(empIds != null && empIds.size() > 0){
					Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_BETWEENMONTH);
					q.setParameter(CommonConstants.EMPID, empIds);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
				}else{
					Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BETWEENMONTH);
					q.setParameter(CommonConstants.STATUS, 'a');
					q.setParameter("fromMonth", fromMonth);
					q.setParameter("toMonth", toMonth);
					employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
				}
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonth(Long empId, String month) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_DATE);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.MONTHLY, month);
				q.setParameter(CommonConstants.STATUS, 'a');
				employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	public Long retrieveByEmpIdBetweenMonth(Long empId, String fromMonth, String toMonth) throws AppException {
		Long employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_BETWEEN_DATE);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter("fromMonth", fromMonth);
				q.setParameter("toMonth", toMonth);
				q.setParameter(CommonConstants.STATUS, 'a');
				employeeBonusList = (Long) q.getSingleResult();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveByEmpIdAndMonthWithOutStatus(Long empId, String month) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_EMPID_DATE);
				q.setParameter(CommonConstants.EMPID, empId);
				q.setParameter(CommonConstants.MONTHLY, month);
				employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieve() throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_ALL);
				employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	@SuppressWarnings("unchecked")
	public List<EmployeeBonusDO> retrieveById(Long id) throws AppException {
		List<EmployeeBonusDO> employeeBonusList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(EmployeeBonusDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				employeeBonusList = (List<EmployeeBonusDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusList;
	}
	
	public EmployeeBonusDO update(EmployeeBonusDO employeeBonusDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(employeeBonusDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeBonusDO;
	}

}
