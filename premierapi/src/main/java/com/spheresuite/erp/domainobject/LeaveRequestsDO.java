package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="leaverequests")
@TableGenerator(name ="leaverequests", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "leaverequests.findAll", query = "SELECT r FROM LeaveRequestsDO r"),
    @NamedQuery(name = "leaverequests.findById", query = "SELECT r FROM LeaveRequestsDO r where r.id =:id"),
    @NamedQuery(name = "leaverequests.findByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.empId =:id"),
    @NamedQuery(name = "leaverequests.findLeaveByEmpId", query = "SELECT r FROM LeaveRequestsDO r where r.empId =:id and r.leavetypefromDate =:leaveTypeFromDate and r.leavetypeToDate =:leaveTypeToDate and r.status =:status and r.requestType =:type "),
   // @NamedQuery(name = "leaverequests.findActive", query = "SELECT r FROM LeaveRequestsDO r where r.status =:status"),
})
public class LeaveRequestsDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "leaverequests.findAll";
	public static final String FIND_BY_ID = "leaverequests.findById";
	public static final String FINDBY_EMP_ID = "leaverequests.findByEmpId";
	public static final String FIND_LEAVE_BY_EMP_ID = "leaverequests.findLeaveByEmpId";
	//public static final String FIND_ACTIVE = "leaverequests.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "leaverequests")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private String description;
	private Long requestType;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long numberofhours;
	private String date;
	private String fromDate;
	private String toDate;
	private char status;
	private Long empId;
	private String comments;
	private String leaveDay;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	private String leavetypefromDate;
	private String leavetypeToDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getRequestType() {
		return requestType;
	}
	public void setRequestType(Long requestType) {
		this.requestType = requestType;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getNumberofhours() {
		return numberofhours;
	}
	public void setNumberofhours(Long numberofhours) {
		this.numberofhours = numberofhours;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getLeaveDay() {
		return leaveDay;
	}
	public void setLeaveDay(String leaveDay) {
		this.leaveDay = leaveDay;
	}
	public Date getCreatedon() {
		return createdon;
	}
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}
	public String getLeavetypefromDate() {
		return leavetypefromDate;
	}
	public void setLeavetypefromDate(String leavetypefromDate) {
		this.leavetypefromDate = leavetypefromDate;
	}
	public String getLeavetypeToDate() {
		return leavetypeToDate;
	}
	public void setLeavetypeToDate(String leavetypeToDate) {
		this.leavetypeToDate = leavetypeToDate;
	}
	
	
	
}