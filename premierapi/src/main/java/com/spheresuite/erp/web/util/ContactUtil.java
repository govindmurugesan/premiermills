package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.ContactDO;
import com.spheresuite.erp.domainobject.ContactTypeDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.ContactTypeService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.SalutationService;
import com.spheresuite.erp.util.CommonConstants;

public class ContactUtil {
	
	private ContactUtil() {}
	
	public static JSONObject getContactList(List<ContactDO> contactList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ContactDO contact : contactList) {
				resultJSONArray.put(getContactDetailObject(contact));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getContactDetailObject(ContactDO contact)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(contact.getId()));
		if(contact.getLeadtype() !=null){
			result.put(CommonConstants.LEADTYPE, String.valueOf(contact.getLeadtype()));
		}else{
			result.put(CommonConstants.LEADTYPE, "");
		}
		if(contact.getSalutation() !=null){
			result.put(CommonConstants.SALUTATION, String.valueOf(contact.getSalutation()));
		}else{
			result.put(CommonConstants.SALUTATION, "");
		}
		String name="";
		if(contact.getFirstname() !=null){
			result.put(CommonConstants.FIRSTNAME, String.valueOf(contact.getFirstname()));
			name = name+" "+contact.getFirstname();
		}else{
			result.put(CommonConstants.FIRSTNAME, "");
		}
		if(contact.getMiddlename() !=null){
			result.put(CommonConstants.MIDDLENAME, String.valueOf(contact.getMiddlename()));
			name = name+" "+contact.getMiddlename();
		}else{
			result.put(CommonConstants.MIDDLENAME, "");
		}
		if(contact.getLastname() !=null){
			result.put(CommonConstants.LASTNAME, String.valueOf(contact.getLastname()));
			name = name+" "+contact.getLastname();
		}else{
			result.put(CommonConstants.LASTNAME, "");
		}
		result.put(CommonConstants.NAME, name);
		
		if(contact.getSecondaryemail() !=null){
			result.put(CommonConstants.SECONDARYEMAIL, String.valueOf(contact.getSecondaryemail()));
		}else{
			result.put(CommonConstants.SECONDARYEMAIL, "");
		}
		
		if(contact.getPrimaryemail() !=null){
			result.put(CommonConstants.PRIMARYEMAIL, String.valueOf(contact.getPrimaryemail()));
		}else{
			result.put(CommonConstants.PRIMARYEMAIL, "");
		}
		
		if(contact.getAddress1() !=null){
			result.put(CommonConstants.ADDRESS1, String.valueOf(contact.getAddress1()));
		}else{
			result.put(CommonConstants.ADDRESS1, "");
		}
		
		if(contact.getAddress2() !=null){
			result.put(CommonConstants.ADDRESS2, String.valueOf(contact.getAddress2()));
		}else{
			result.put(CommonConstants.ADDRESS2, "");
		}
		
		if(contact.getContattype() !=null){
			result.put(CommonConstants.CONTACTTYPE, String.valueOf(contact.getContattype()));
		}else{
			result.put(CommonConstants.CONTACTTYPE, "");
		}
		
		if(contact.getDesignation() !=null){
			result.put(CommonConstants.DESIGNATION, String.valueOf(contact.getDesignation()));
		}else{
			result.put(CommonConstants.DESIGNATION, "");
		}
		
		if(contact.getMobile1() !=null){
			result.put(CommonConstants.MOBILE, String.valueOf(contact.getMobile1()));
		}else{
			result.put(CommonConstants.MOBILE, "");
		}
		
		if(contact.getMobile2() !=null){
			result.put(CommonConstants.MOBILE2, String.valueOf(contact.getMobile2()));
		}else{
			result.put(CommonConstants.MOBILE2, "");
		}
		
		if(contact.getPhone1() !=null){
			result.put(CommonConstants.PHONE, String.valueOf(contact.getPhone1()));
		}else{
			result.put(CommonConstants.PHONE, "");
		}
		
		if(contact.getPhone2() !=null){
			result.put(CommonConstants.PHONE1, String.valueOf(contact.getPhone2()));
		}else{
			result.put(CommonConstants.PHONE1, "");
		}
		
		if(contact.getPhone3() !=null){
			result.put(CommonConstants.PHONE2, String.valueOf(contact.getPhone3()));
		}else{
			result.put(CommonConstants.PHONE2, "");
		}
		
		if(contact.getPhone4() !=null){
			result.put(CommonConstants.PHONE3, String.valueOf(contact.getPhone4()));
		}else{
			result.put(CommonConstants.PHONE3, "");
		}
		
		if(contact.getFax() !=null){
			result.put(CommonConstants.FAX, String.valueOf(contact.getFax()));
		}else{
			result.put(CommonConstants.FAX, "");
		}
		
		if(contact.getWebsite() !=null){
			result.put(CommonConstants.WEBSITE, String.valueOf(contact.getWebsite()));
		}else{
			result.put(CommonConstants.WEBSITE, "");
		}
		
		if(contact.getRequirements() !=null){
			result.put(CommonConstants.REQUIREMENT, String.valueOf(contact.getRequirements()));
		}else{
			result.put(CommonConstants.REQUIREMENT, "");
		}
		
		if(contact.getNotes() !=null){
			result.put(CommonConstants.NOTE, String.valueOf(contact.getNotes()));
		}else{
			result.put(CommonConstants.NOTE, "");
		}
		
		if(contact.getComments() !=null){
			result.put(CommonConstants.COMMENT, String.valueOf(contact.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(contact.getUpdatedon())));
		if(contact.getUpdatedby() != null){
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(contact.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(contact.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		
		if(contact.getLeadtype() !=null){
			List<LeadDO> leadList = new LeadService().retrieveById(contact.getLeadtype());
			if(leadList != null && leadList.size() > 0){
				result.put(CommonConstants.LEAD_TYPE_NAME, String.valueOf(leadList.get(0).getName()));
			}else{
				result.put(CommonConstants.LEAD_TYPE_NAME, "");
			}
			
		}else{
			result.put(CommonConstants.LEAD_TYPE_NAME, "");
		}
		
		if(contact.getSalutation() != null){
			SalutationService salservice = new SalutationService();
			List<SalutationDO> salutationDO = salservice.retrieveById(contact.getSalutation());
			if(salutationDO != null && salutationDO.size() > 0){
				result.put(CommonConstants.SALUTATION_NAME, salutationDO.get(0).getName());
			}else{
				result.put(CommonConstants.SALUTATION_NAME, "");
			}
			
		}else{
			result.put(CommonConstants.SALUTATION_NAME, "");
		}
		
		if(contact.getContattype() != null){
			ContactTypeService contactTypeService = new ContactTypeService();
			
			List<ContactTypeDO> contactTypeDO =  contactTypeService.retrieveById(contact.getContattype());
			if(contactTypeDO != null && contactTypeDO.size() > 0){
				result.put(CommonConstants.CONTACTTYPE_NAME, contactTypeDO.get(0).getName());
			}
			
		}else{
			result.put(CommonConstants.CONTACTTYPE_NAME, "");
		}
		
		return result;
	}
}
