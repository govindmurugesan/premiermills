package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.HrRequestTypeDO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.HrRequestTypeService;
import com.spheresuite.erp.service.HrRequestDocService;
import com.spheresuite.erp.util.CommonConstants;

public class HrRequestsUtil {
	
	private HrRequestsUtil() {}
	
	public static JSONObject getHrRequestsList(List<HrRequestsDO> hrRequestsList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (HrRequestsDO hrRequest : hrRequestsList) {
				resultJSONArray.put(getHrRequestsDetailObject(hrRequest));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getHrRequestsDetailObject(HrRequestsDO hrRequest)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(hrRequest.getId()));
		result.put(CommonConstants.DESC, String.valueOf(hrRequest.getDescription()));
		
		if(hrRequest.getRequestType() != null){
			List<HrRequestTypeDO> hrRequestType = new HrRequestTypeService().retrieveById(hrRequest.getRequestType());
			if(hrRequestType != null && hrRequestType.size() > 0){
				if(hrRequestType.get(0).getRequestType() != null){
					result.put(CommonConstants.TYPE, String.valueOf(hrRequestType.get(0).getId()));
					result.put(CommonConstants.TYPENAME, String.valueOf(hrRequestType.get(0).getRequestType()));
				}
				else{
					result.put(CommonConstants.TYPE, "");
					result.put(CommonConstants.TYPENAME, "");
				}
			}else{
				result.put(CommonConstants.TYPE, "");
				result.put(CommonConstants.TYPENAME, "");
			}
		}
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(hrRequest.getUpdatedon())));
		result.put(CommonConstants.CREATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(hrRequest.getCreatedon())));
		result.put(CommonConstants.STATUS, String.valueOf(hrRequest.getStatus()));
		if(hrRequest.getComments() != null ){
			result.put(CommonConstants.COMMENT, String.valueOf(hrRequest.getComments()));
		}else{
			result.put(CommonConstants.COMMENT, "");
		}
		
		if(hrRequest.getUpdatedby() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(hrRequest.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.UPDATED_BY, "");
			}
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		
		if(hrRequest.getEmpId() != null){
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(hrRequest.getEmpId());
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null){
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				}
				else{
					result.put(CommonConstants.CREATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
				}
			}else{
				result.put(CommonConstants.CREATED_BY, "");
			}
		}else{
			result.put(CommonConstants.CREATED_BY, "");
		}
		JSONArray resultJSONArray = new JSONArray();
		List<HrRequestDocDO> hrRequestDocList = new HrRequestDocService().retrieveByOppId(hrRequest.getId());
		if(hrRequestDocList != null && hrRequestDocList.size()  > 0){
			for (HrRequestDocDO doc : hrRequestDocList) {
				resultJSONArray.put(getDocDetailObject(doc));
			}
		}
		result.put(CommonConstants.FILE, resultJSONArray);
		return result;
	}
	

	public static JSONObject getDocDetailObject(HrRequestDocDO doc)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.FILE, String.valueOf(doc.getPhoto()));
		if(doc.getFileName() != null){
			result.put(CommonConstants.NAME, String.valueOf(doc.getFileName()));
		}else{
			result.put(CommonConstants.NAME, "");	
		}
		
		return result;
	}
}
