package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="allowancesetting")
@TableGenerator(name ="allowancesetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "allowancesetting.findAll", query = "SELECT r FROM AllowanceSettingDO r"),
    @NamedQuery(name = "allowancesetting.findById", query = "SELECT r FROM AllowanceSettingDO r where r.id =:id"),
    @NamedQuery(name = "allowancesetting.findActive", query = "SELECT r FROM AllowanceSettingDO r where r.status =:status and r.name =:name"),
    @NamedQuery(name = "allowancesetting.findBetweenDate", query = "SELECT r FROM AllowanceSettingDO r where r.status =:status and r.name =:name"),
    
})
public class AllowanceSettingDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "allowancesetting.findAll";
	public static final String FIND_BY_ID = "allowancesetting.findById";
	public static final String FIND_ACTIVE = "allowancesetting.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "allowancesetting")
	private Long id;
	private Long allowancetypeid;
	private String name;
	private String section;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdby;
	private String taxable;
	private String notes;
	private String displayname;
	private String type;
	private String fixedamount;
	private String percentageamount;
	private String basicgrosspay;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private char status;
	@Transient
	private String allowanceSettingName;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAllowancetypeid() {
		return allowancetypeid;
	}
	public void setAllowancetypeid(Long allowancetypeid) {
		this.allowancetypeid = allowancetypeid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public String getTaxable() {
		return taxable;
	}
	public void setTaxable(String taxable) {
		this.taxable = taxable;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFixedamount() {
		return fixedamount;
	}
	public void setFixedamount(String fixedamount) {
		this.fixedamount = fixedamount;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getBasicgrosspay() {
		return basicgrosspay;
	}
	public void setBasicgrosspay(String basicgrosspay) {
		this.basicgrosspay = basicgrosspay;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getAllowanceSettingName() {
		return allowanceSettingName;
	}
	public void setAllowanceSettingName(String allowanceSettingName) {
		this.allowanceSettingName = allowanceSettingName;
	}
}