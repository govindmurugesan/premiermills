package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeDriverLicenseDAO;
import com.spheresuite.erp.domainobject.EmployeeDriverLicenseDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeDriverLicenseService {
	static Logger logger = Logger.getLogger(EmployeeDriverLicenseService.class.getName());
	
	public EmployeeDriverLicenseDO persist(EmployeeDriverLicenseDO employeeDriverLicenseDO) throws AppException {
		return new EmployeeDriverLicenseDAO().persist(employeeDriverLicenseDO);
	}

	public List<EmployeeDriverLicenseDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeDriverLicenseDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeDriverLicenseDO> retrieveById(Long id) throws AppException {
		return new EmployeeDriverLicenseDAO().retrieveById(id);
	}
	
	public EmployeeDriverLicenseDO update(EmployeeDriverLicenseDO employeeDriverLicenseDO) throws AppException {
		return new EmployeeDriverLicenseDAO().update(employeeDriverLicenseDO);
	}
}
