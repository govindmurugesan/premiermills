package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.TimesheetDAO;
import com.spheresuite.erp.domainobject.TimesheetDO;
import com.spheresuite.erp.exception.AppException;

public class TimesheetService {
	static Logger logger = Logger.getLogger(TimesheetService.class.getName());
	
	public TimesheetDO persist(TimesheetDO timesheet) throws AppException {
		return new TimesheetDAO().persist(timesheet);
	}

	public List<TimesheetDO> retrieve() throws AppException {
		return new TimesheetDAO().retrieve();
	}
	
	public List<TimesheetDO> retrieveById(Long Id) throws AppException {
		return new TimesheetDAO().retrieveById(Id);
	}
	
	public List<TimesheetDO> retrieveByWeekendDate(String date, Long empId) throws AppException {
		return new TimesheetDAO().retrieveByWeekendDate(date, empId);
	}
	
	
	
	public TimesheetDO update(TimesheetDO timesheet) throws AppException {
		return new TimesheetDAO().update(timesheet);
	}
}
