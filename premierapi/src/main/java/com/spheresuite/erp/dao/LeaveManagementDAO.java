package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.LeaveManagementDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class LeaveManagementDAO {
	static Logger logger = Logger.getLogger(LeaveManagementDAO.class.getName());
	private EntityManager em = null;
	
	public LeaveManagementDO persist(LeaveManagementDO leaveDetail) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && leaveDetail != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(leaveDetail);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieve() throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_ALL);
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByDept(List<Long> ids) throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_ALL_BY_DEPT);
				q.setParameter(CommonConstants.ID, ids);
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveById(Long Id) throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	
	public LeaveManagementDO update(LeaveManagementDO leaveDetail) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(leaveDetail);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveDetail;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByEmpId(Long id) throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FINDBY_EMP_ID);
				q.setParameter(CommonConstants.ID, id);
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}*/
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retriveActiveById(Long id) throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_ACTIVE_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveByType(Long type) throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_ACTIVE_BY_TYPE);
				q.setParameter(CommonConstants.TYPE, type);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	
	@SuppressWarnings("unchecked")
	public List<LeaveManagementDO> retrieveAllActive() throws AppException {
		List<LeaveManagementDO> leaveList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(LeaveManagementDO.FIND_ACTIVE);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				
				leaveList = (List<LeaveManagementDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return leaveList;
	}
	

}
