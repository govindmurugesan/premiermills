package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.CustomerDAO;
import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.exception.AppException;

public class CustomerService {
	static Logger logger = Logger.getLogger(CustomerService.class.getName());
	
	public CustomerDO persist(CustomerDO customerDO) throws AppException {
		return new CustomerDAO().persist(customerDO);
	}

	public List<CustomerDO> retrieveById(Long Id) throws AppException {
		return new CustomerDAO().retrieveById(Id);
	}
	
	public List<CustomerDO> retrieveByActive() throws AppException {
		return new CustomerDAO().retrieveByActive();
	}
	
	public List<CustomerDO> retrieve() throws AppException {
		return new CustomerDAO().retrieve();
	}
	
	public CustomerDO update(CustomerDO customerDO) throws AppException {
		return new CustomerDAO().update(customerDO);
	}
	
	public boolean persistList(List<CustomerDO> customerDO) throws AppException {
		return new CustomerDAO().persistList(customerDO);
	}
}
