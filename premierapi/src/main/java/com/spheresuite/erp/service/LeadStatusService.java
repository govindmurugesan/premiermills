package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeadStatusDAO;
import com.spheresuite.erp.domainobject.LeadStatusDO;
import com.spheresuite.erp.exception.AppException;

public class LeadStatusService {
	static Logger logger = Logger.getLogger(LeadStatusService.class.getName());
	
	public LeadStatusDO persist(LeadStatusDO leadStatusDO) throws AppException {
		return new LeadStatusDAO().persist(leadStatusDO);
	}

	public List<LeadStatusDO> retrieveActive() throws AppException {
		return new LeadStatusDAO().retrieveActive();
	}
	
	public List<LeadStatusDO> retrieveById(Long Id) throws AppException {
		return new LeadStatusDAO().retrieveById(Id);
	}
	
	public List<LeadStatusDO> retrieve() throws AppException {
		return new LeadStatusDAO().retrieve();
	}
	
	public LeadStatusDO update(LeadStatusDO leadStatusDO) throws AppException {
		return new LeadStatusDAO().update(leadStatusDO);
	}
}
