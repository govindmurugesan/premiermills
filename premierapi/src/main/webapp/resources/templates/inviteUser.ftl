<!DOCTYPE html>
<html>
  <head>
    <title>Sphere Suite</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     <style type="text/css">
      @media (min-width:320px) and (max-width:360px) {
           .respTextAlign{
               padding: 27px 0 !important;
           }
      }

    </style> 
  </head>
  <body style="background-color: #ccc; margin-top: 100px;">
    <div style="position: relative;overflow-x: hidden;">
      <div style="background-image: url(http://www.spheresuite.com/images/mail_back_img.png);background-repeat: no-repeat;background-size: cover;background-position: center; height: 100%;width: 50%; background-color: #fff;margin: 10px auto;border: 10px solid #00254d;">

    <div style="font-family: sans-serif;">
      <div style="">
        <div style="border-bottom: 2px solid #ccc; height: 70px; text-align: center;">
          <img src="http://www.spheresuite.com/images/logo_img.png" style="padding: 21px;">
        </div>
        <div style="color: #00254d;padding: 30px;">
          <h1 style="font-weight: 600; margin: 0 !important;">Hi ${userName}, </h1>
          </br>
          <h3>Congratulations !</h3>
          </br>
          <p style="margin: 0 !important; font-size: 16px;color: #555;">You have been invited to be a part of SphereSuite, kindly activate your account and enjoy our services</p>
          </br>
          <div style="text-align: center; padding:10px">
           <a href="${link}"> <button style="color: #fff !important; background-color: #00254d; border: none; outline: 0; padding: 10px 20px; font-size: 16px;">Activate</button></a>
          </div>
        </div>
        
        <div class="col-lg-12 col-xs-12" style=" background-color: #00254d; text-align: center;color: #fff; border-top: 2px solid #ccc; height: 90px;">
          <p class="respTextAlign" style="color: white !important;padding: 36px 0;font-weight: 300;margin: 0;">� &nbsp;SphereSuite&nbsp;2017&nbsp;|&nbsp;All Rights Reserved</p>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>