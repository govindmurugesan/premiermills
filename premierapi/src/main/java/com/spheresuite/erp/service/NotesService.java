package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.NotesDAO;
import com.spheresuite.erp.domainobject.NotesDO;
import com.spheresuite.erp.exception.AppException;

public class NotesService {
	static Logger logger = Logger.getLogger(NotesService.class.getName());
	
	public NotesDO persist(NotesDO notesDO) throws AppException {
		return new NotesDAO().persist(notesDO);
	}
	
	public NotesDO update(NotesDO notesDO) throws AppException {
		return new NotesDAO().update(notesDO);
	}

	public List<NotesDO> retrieveById(Long Id) throws AppException {
		return new NotesDAO().retrieveById(Id);
	}

	public List<NotesDO> retrieveByLeadId(Long Id) throws AppException {
		return new NotesDAO().retrieveByLeadId(Id);
	}
	
	public List<NotesDO> retrieveByLeadRecentNote(Long Id) throws AppException {
		return new NotesDAO().retrieveByLeadRecentNote(Id);
	}

	
	public List<NotesDO> retrieve() throws AppException {
		return new NotesDAO().retrieve();
	}
}
