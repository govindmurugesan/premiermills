package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.CompanyDAO;
import com.spheresuite.erp.domainobject.CompanyDO;
import com.spheresuite.erp.exception.AppException;

public class CompanyService {
	static Logger logger = Logger.getLogger(CompanyService.class.getName());
	
	public CompanyDO persist(CompanyDO countryDO) throws AppException {
		return new CompanyDAO().persist(countryDO);
	}

	public List<CompanyDO> retrieveById(Long Id) throws AppException {
		return new CompanyDAO().retrieveById(Id);
	}
	
	public List<CompanyDO> retrieve() throws AppException {
		return new CompanyDAO().retrieve();
	}
	
	public CompanyDO update(CompanyDO countryDO) throws AppException {
		return new CompanyDAO().update(countryDO);
	}
}
