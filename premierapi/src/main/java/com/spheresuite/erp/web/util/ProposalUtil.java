package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.domainobject.InvoiceTermDO;
import com.spheresuite.erp.domainobject.LeadDO;
import com.spheresuite.erp.domainobject.PaymentTermDO;
import com.spheresuite.erp.domainobject.OpportunitiesDO;
import com.spheresuite.erp.domainobject.ProjectTypeDO;
import com.spheresuite.erp.domainobject.ProposalDO;
import com.spheresuite.erp.domainobject.ProposalDefaultDO;
import com.spheresuite.erp.domainobject.ProposalDetailsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.service.InvoiceTermService;
import com.spheresuite.erp.service.LeadService;
import com.spheresuite.erp.service.PaymentTermService;
import com.spheresuite.erp.service.OpportunitiesService;
import com.spheresuite.erp.service.ProjectTypeService;
import com.spheresuite.erp.service.ProposalDefaultService;
import com.spheresuite.erp.service.ProposalDetailsService;
import com.spheresuite.erp.util.CommonConstants;

public class ProposalUtil {
	
	private ProposalUtil() {}
	
	public static JSONObject getProposalList(List<ProposalDO> proposalList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (ProposalDO proposal : proposalList) {
				resultJSONArray.put(getProposalDetailObject(proposal));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}
	
	
	public static JSONObject getProposalDetailObject(ProposalDO proposal)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(proposal.getId()));
		if(proposal.getCustomerId() != null){
			result.put(CommonConstants.CUSTOMERID, String.valueOf(proposal.getCustomerId()));
			List<LeadDO> customerList = new LeadService().retrieveById(proposal.getCustomerId());
			if(customerList != null && customerList.size() > 0){
				result.put(CommonConstants.NAME, String.valueOf(customerList.get(0).getName()));
			}else{
				result.put(CommonConstants.NAME, "");
			}
		}else{
			result.put(CommonConstants.CUSTOMERID, "");
			result.put(CommonConstants.NAME, "");
		}
		
		if(proposal.getProjectId() != null){
			result.put(CommonConstants.PROJECTID, String.valueOf(proposal.getProjectId()));
			List<OpportunitiesDO> projectList = new OpportunitiesService().retrieveById(proposal.getProjectId());
			if(projectList != null && projectList.size() > 0){
				result.put(CommonConstants.PROJECTNAME, projectList.get(0).getProjectname());
			}else{
				result.put(CommonConstants.PROJECTNAME,"");
			}
		}else{
			result.put(CommonConstants.PROJECTID, "");
			result.put(CommonConstants.PROJECTNAME,"");
		}

		if(proposal.getProjectTypeId() != null){
			result.put(CommonConstants.PROJECTTYPE, String.valueOf(proposal.getProjectTypeId()));
			List<ProjectTypeDO> projectTypeDOs = new ProjectTypeService().retrieveById(proposal.getProjectTypeId());
			if(projectTypeDOs != null && projectTypeDOs.size() >0){
				result.put(CommonConstants.PROJECT_TYPE_NAME, String.valueOf(projectTypeDOs.get(0).getName()));
			}else{
				result.put(CommonConstants.PROJECT_TYPE_NAME, "");
			}
		}else{
			result.put(CommonConstants.PROJECTTYPE, "");
			result.put(CommonConstants.PROJECT_TYPE_NAME, "");
		}
		result.put(CommonConstants.CURRENCYTYPE, String.valueOf(proposal.getCurrencyType() != null ? proposal.getCurrencyType() : ""));
		if(proposal.getCurrencyType() != null){
			List<CountryDO> countryList = new CountryService().retrieveById(Long.parseLong(proposal.getCurrencyType()));
			if(countryList != null && countryList.size() > 0){
				result.put(CommonConstants.CURRENCYTYPE_NAME,countryList.get(0).getCurrencyType());
			}else{
				result.put(CommonConstants.CURRENCYTYPE_NAME,"");
			}
		}
		result.put(CommonConstants.RATE, String.valueOf(proposal.getRate() != null ? proposal.getRate() : ""));
		result.put(CommonConstants.QUANTITY, String.valueOf(proposal.getQunatity() != null ? proposal.getQunatity() : ""));
		
		if(proposal.getPaymentTermId() != null){
			result.put(CommonConstants.PAYMENTTERM, String.valueOf(proposal.getPaymentTermId()));
			List<PaymentTermDO> paymentTermList = new PaymentTermService().retrieveById(proposal.getPaymentTermId());
			if(paymentTermList != null && paymentTermList.size() > 0){
				result.put(CommonConstants.PAYMENTTERM_NAME, paymentTermList.get(0).getName());
			}else{
				result.put(CommonConstants.PAYMENTTERM_NAME, "");
			}
		}else{
			result.put(CommonConstants.PAYMENTTERM, "");
		}
		result.put(CommonConstants.INVOICETERM, String.valueOf(proposal.getInvoiceTerm() != null ? proposal.getInvoiceTerm(): ""));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(proposal.getUpdatedon())));
		if(proposal.getUpdatedby() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(proposal.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(proposal.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}
		JSONArray resultInvoiceTerm = new JSONArray();
		List<InvoiceTermDO> invoicetermList = new InvoiceTermService().retrieveById(proposal.getId());
		if(invoicetermList != null && invoicetermList.size() > 0){
			for (InvoiceTermDO invoiceTerm : invoicetermList) {
				resultInvoiceTerm.put(getInvoiceTermObject(invoiceTerm));
			}
			result.put(CommonConstants.INVOICETERM_LIST, resultInvoiceTerm);
		}else{
			result.put(CommonConstants.INVOICETERM_LIST, "");
		}
		

		List<ProposalDetailsDO> proposalDetailsList = new ProposalDetailsService().retrieveById(proposal.getId());
		JSONArray proposalDetails = new JSONArray();
		if(proposalDetailsList != null && proposalDetailsList.size() > 0){
			for (ProposalDetailsDO proposalDetail : proposalDetailsList) {
				proposalDetails.put(getProposalObject(proposalDetail));
			}
			result.put(CommonConstants.FIELD_LIST, proposalDetails);
		}else{
			result.put(CommonConstants.FIELD_LIST, "");
		}
		
		List<ProposalDefaultDO> proposalDefultList = new ProposalDefaultService().retrieveById(proposal.getId());
		JSONArray proposalDefault = new JSONArray();
		if(proposalDetailsList != null && proposalDetailsList.size() > 0){
			for (ProposalDefaultDO proposalDetail : proposalDefultList) {
				proposalDefault.put(getProposalDefaultObject(proposalDetail));
			}
			result.put(CommonConstants.DEFAULT_LIST, proposalDefault);
		}else{
			result.put(CommonConstants.DEFAULT_LIST, "");
		}
		
		
		return result;
	}
	
	public static JSONObject getProposalObject(ProposalDetailsDO proposalDetails)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.KEY, String.valueOf(proposalDetails.getProposalKey()));
		result.put(CommonConstants.VALUE, String.valueOf(proposalDetails.getProposalValue()));
		return result;
	}
	
	public static JSONObject getProposalDefaultObject(ProposalDefaultDO proposalDetails)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.KEY, String.valueOf(proposalDetails.getProposalKey()));
		result.put(CommonConstants.VALUE, String.valueOf(proposalDetails.getProposalValue()));
		return result;
	}
	
	public static JSONObject getInvoiceTermObject(InvoiceTermDO invoiceTerm)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.COMMENT, String.valueOf(invoiceTerm.getDescripetion()));
		result.put(CommonConstants.PERCENTAGE, String.valueOf(invoiceTerm.getProjectPercentage()));
		return result;
	}
}
