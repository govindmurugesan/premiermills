package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.SalutationDAO;
import com.spheresuite.erp.domainobject.SalutationDO;
import com.spheresuite.erp.exception.AppException;

public class SalutationService {
	static Logger logger = Logger.getLogger(SalutationService.class.getName());
	
	public SalutationDO persist(SalutationDO salutationDO) throws AppException {
		return new SalutationDAO().persist(salutationDO);
	}

	public List<SalutationDO> retrieveActive() throws AppException {
		return new SalutationDAO().retrieveActive();
	}
	
	public List<SalutationDO> retrieveById(Long Id) throws AppException {
		return new SalutationDAO().retrieveById(Id);
	}
	
	public List<SalutationDO> retrieve() throws AppException {
		return new SalutationDAO().retrieve();
	}
	
	public SalutationDO update(SalutationDO salutationDO) throws AppException {
		return new SalutationDAO().update(salutationDO);
	}
}
