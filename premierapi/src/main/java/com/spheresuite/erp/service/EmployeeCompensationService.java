package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeCompensationDAO;
import com.spheresuite.erp.domainobject.EmployeeCompensationDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeCompensationService {
	static Logger logger = Logger.getLogger(EmployeeCompensationService.class.getName());
	
	public EmployeeCompensationDO persist(EmployeeCompensationDO empCompensationDO) throws AppException {
		return new EmployeeCompensationDAO().persist(empCompensationDO);
	}

	public List<EmployeeCompensationDO> retrieveActive(Long empId) throws AppException {
		return new EmployeeCompensationDAO().retrieveActive(empId);
	}
	
	public List<EmployeeCompensationDO> retrieveById(Long id) throws AppException {
		return new EmployeeCompensationDAO().retrieveById(id);
	}
	
	public List<EmployeeCompensationDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeCompensationDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeCompensationDO> retrieve() throws AppException {
		return new EmployeeCompensationDAO().retrieve();
	}
	
	public EmployeeCompensationDO update(EmployeeCompensationDO empCompensationDO) throws AppException {
		return new EmployeeCompensationDAO().update(empCompensationDO);
	}
}
