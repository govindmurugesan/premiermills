package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.RequirementsDO;
import com.spheresuite.erp.service.RequirementsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.RequirementsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/requirements")
public class RequirementsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(RequirementsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				RequirementsDO requirementsDO = new RequirementsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
			 			requirementsDO.setProjectid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PROJECTID).toString() : null));
			 		}
			 		
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			requirementsDO.setCustomerid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CUSTOMERID).toString() : null));
			 		}
			 		
			 		requirementsDO.setMinexp(!inputJSON.get(CommonConstants.MINEXP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MINEXP).toString()) : null);
			 		requirementsDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		requirementsDO.setMaxexp(!inputJSON.get(CommonConstants.MAXEXP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MAXEXP).toString()) : null);
			 		requirementsDO.setCost(!inputJSON.get(CommonConstants.COST).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.COST).toString()) : null);
			 		requirementsDO.setJobdesc(!inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty() ? inputJSON.get(CommonConstants.JOBDESC).toString() : null);
			 		requirementsDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
			 		requirementsDO.setNoofposition(!inputJSON.get(CommonConstants.NOOFPOSITION).toString().isEmpty() ? inputJSON.get(CommonConstants.NOOFPOSITION).toString() : null);
			 		requirementsDO.setNoticeperiod(!inputJSON.get(CommonConstants.NOTICE_PEROID).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTICE_PEROID).toString() : null);
			 		
			 		requirementsDO.setJoiningdate(!inputJSON.get(CommonConstants.JOININGDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.JOININGDATE).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			requirementsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			requirementsDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		requirementsDO.setStatus('O');
			 		requirementsDO.setUpdatedon(new Date());
			 	}
				new RequirementsService().persist(requirementsDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Requirements Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<RequirementsDO> requirementsList = new RequirementsService().retrieve();
				respJSON = RequirementsUtil.getRequirementsList(requirementsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/retrieveByStatus/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByStatus(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
					List<RequirementsDO> requirementsList = new RequirementsService().retrieveByStatus((char)inputJSON.get(CommonConstants.STATUS));
					respJSON = RequirementsUtil.getRequirementsList(requirementsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByProjectId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByProjectId(@PathVariable String inputParams,Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
					List<RequirementsDO> requirementsList = new RequirementsService().retrieveByProjectId(Long.parseLong(inputJSON.get(CommonConstants.PROJECTID).toString()));
					respJSON = RequirementsUtil.getRequirementsList(requirementsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<RequirementsDO> requirementsList = new RequirementsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = RequirementsUtil.getRequirementsList(requirementsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				RequirementsDO requirementsDO = new RequirementsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 			List<RequirementsDO> requirementsList = new RequirementsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 			if(requirementsList != null && requirementsList.size() > 0){
			 				requirementsDO = requirementsList.get(0);

			 				if(!inputJSON.get(CommonConstants.PROJECTID).toString().isEmpty()){
					 			requirementsDO.setProjectid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.PROJECTID).toString() : null));
					 		}
					 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
					 			requirementsDO.setCustomerid(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.CUSTOMERID).toString() : null));
					 		}
					 		requirementsDO.setMinexp(!inputJSON.get(CommonConstants.MINEXP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MINEXP).toString()) : null);
					 		requirementsDO.setMaxexp(!inputJSON.get(CommonConstants.MAXEXP).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.MAXEXP).toString()) : null);
					 		requirementsDO.setCost(!inputJSON.get(CommonConstants.COST).toString().isEmpty() ?  Long.parseLong(inputJSON.get(CommonConstants.COST).toString()) : null);
					 		requirementsDO.setJobdesc(!inputJSON.get(CommonConstants.JOBDESC).toString().isEmpty() ? inputJSON.get(CommonConstants.JOBDESC).toString() : null);
					 		requirementsDO.setComments(!inputJSON.get(CommonConstants.COMMENT).toString().isEmpty() ? inputJSON.get(CommonConstants.COMMENT).toString() : null);
					 		requirementsDO.setNoofposition(!inputJSON.get(CommonConstants.NOOFPOSITION).toString().isEmpty() ? inputJSON.get(CommonConstants.NOOFPOSITION).toString() : null);
					 		requirementsDO.setNoticeperiod(!inputJSON.get(CommonConstants.NOTICE_PEROID).toString().isEmpty() ? inputJSON.get(CommonConstants.NOTICE_PEROID).toString() : null);
					 		requirementsDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
					 		requirementsDO.setJoiningdate(!inputJSON.get(CommonConstants.JOININGDATE).toString().isEmpty() ? inputJSON.get(CommonConstants.JOININGDATE).toString() : null);
					 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
					 			requirementsDO.setUpdatedBy(inputJSON.get(CommonConstants.UPDATED_BY).toString());
					 			requirementsDO.setCreatedby(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
					 		}
					 		requirementsDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
					 		new RequirementsService().update(requirementsDO);
					 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Sales Requirements Updated");
			 			}
			 		}
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
