
package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.HrRequestDocDO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.service.HrRequestDocService;
import com.spheresuite.erp.service.HrRequestsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.HrRequestsUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/hrrequests")
public class HrRequestsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(HrPoliciesRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		HrRequestsDO hrRequestDO = new HrRequestsDO(); 
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
				if (inputJSON != null){
		 			hrRequestDO.setRequestType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
		 			hrRequestDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
		 			hrRequestDO.setUpdatedon(new Date());
		 			hrRequestDO.setCreatedon(new Date());
		 			if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 				hrRequestDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
		 				hrRequestDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
		 			//hrRequestDO.setStatus(CommonConstants.STATUS_OPEN);
		 			hrRequestDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
		 			new HrRequestsService().persist(hrRequestDO);
		 			CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Hr Request Created");
		 			CommonUtil.sendMailToReportingHeads(request,inputJSON,"hrrequests","hrRequest");
			 	}
			}else{
		 		return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(hrRequestDO.getId()).toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<HrRequestsDO> hrRequestsList = new HrRequestsService().retrieve();
				respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && inputJSON.get(CommonConstants.ID) != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
		 				HrRequestsDO hrRequestsDO = new HrRequestsDO(); 
		 				List<HrRequestsDO> hrRequestList = new HrRequestsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
		 				if(hrRequestList != null && hrRequestList.size() > 0){
		 					hrRequestsDO = hrRequestList.get(0);
		 				}
		 				hrRequestsDO.setRequestType(Long.parseLong(inputJSON.get(CommonConstants.TYPE).toString()));
		 				hrRequestsDO.setDescription(inputJSON.get(CommonConstants.DESC).toString());
		 				if(inputJSON.get(CommonConstants.COMMENT).toString() != null ){
		 					hrRequestsDO.setComments(inputJSON.get(CommonConstants.COMMENT).toString());
		 				}
		 				hrRequestsDO.setUpdatedon(new Date());
		 				if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
		 					hrRequestsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
		 				hrRequestsDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
		 				new HrRequestsService().update(hrRequestsDO);
		 				new HrRequestDocService().delete(hrRequestsDO.getId());
		 				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Hr Request Updated");
			 		}
			 	}
				
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveByEmpId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByEmpId(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.EMPID).toString().isEmpty()){
					List<HrRequestsDO> hrRequestsList = new HrRequestsService().retriveByEmpId(Long.parseLong(inputJSON.get(CommonConstants.EMPID).toString()));
					respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveById(@PathVariable String inputParams, Model model, HttpServletRequest request)  {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<HrRequestsDO> hrRequestsList = new HrRequestsService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = HrRequestsUtil.getHrRequestsList(hrRequestsList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/updatepic", method = RequestMethod.POST)
	public @ResponseBody String updatepic(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				HrRequestDocDO hrRequestDoc = new HrRequestDocDO();
			 	if (request.getParameter("file") != null && request.getParameter("id") != null){
			 		hrRequestDoc.setHrrequestId(Long.parseLong(request.getParameter("id")));
			 		hrRequestDoc.setPhoto(request.getParameter("file"));
			 		hrRequestDoc.setFileName(request.getParameter("name"));
			 		new HrRequestDocService().persist(hrRequestDoc);
 			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
}
