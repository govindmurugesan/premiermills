package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="weeklytimesheet")
@TableGenerator(name ="weeklytimesheet", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "weeklytimesheet.findAll", query = "SELECT r FROM WeeklyTimesheetDO r"),
    @NamedQuery(name = "weeklytimesheet.findAllByDept", query = "SELECT r FROM WeeklyTimesheetDO r where r.empId IN :id"),
    @NamedQuery(name = "weeklytimesheet.findById", query = "SELECT r FROM WeeklyTimesheetDO r where r.id =:id"),
    @NamedQuery(name = "weeklytimesheet.findByDate", query = "SELECT r FROM WeeklyTimesheetDO r where r.date =:weekenddate and r.empId =:empId"),
    @NamedQuery(name = "weeklytimesheet.findByEmpId", query = "SELECT r FROM WeeklyTimesheetDO r where r.empId =:empId"),
    @NamedQuery(name = "weeklytimesheet.findSubmitted", query = "SELECT r FROM WeeklyTimesheetDO r where r.status =:status"),
})
public class WeeklyTimesheetDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "weeklytimesheet.findAll";
	public static final String FIND_BY_ID = "weeklytimesheet.findById";
	public static final String FIND_BY_DATE = "weeklytimesheet.findByDate";
	public static final String FIND_BY_EMPID = "weeklytimesheet.findByEmpId";
	public static final String FIND_SUBMITTED = "weeklytimesheet.findSubmitted";
	public static final String FIND_ALL_BY_DEPT = "weeklytimesheet.findAllByDept";
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "weeklytimesheet")
	private Long id;
	@Column(columnDefinition="LONGTEXT")
	private Long hours;
	private String date;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private char status;
	private Long empId;
	private String comments;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getHours() {
		return hours;
	}
	public void setHours(Long hours) {
		this.hours = hours;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getUpdatedby() {
		return updatedby;
	}
	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}