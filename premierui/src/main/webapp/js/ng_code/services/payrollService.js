angular
    .module('spheresuite')
    .service('payrollService', payrollService);

payrollService.$inject = ['constants', 'commonService'];

function payrollService(constants, commonService) {

    var payrollServices = {
    		addAllowance: addAllowance,
    		addDeduction: addDeduction,
    		editAllowance: editAllowance,
    		editDeduction: editDeduction,
    		getAllowance: getAllowance,
    		getDeduction: getDeduction,
            getTax : getTax,
    		getLeadByEmp: getLeadByEmp,
    		getAllowanceById: getAllowanceById,
    		getDeductionById: getDeductionById,
            getTaxById : getTaxById,
    		getRunPayroll: getRunPayroll,
    		getRunPayrollMonthly: getRunPayrollMonthly,
    		updateEmployeeCompensation: updateEmployeeCompensation,
    		processPayroll: processPayroll,
    		getPayrollSummary:getPayrollSummary,
    		submitPayroll: submitPayroll,
    		viewAllEmployee: viewAllEmployee,
    		getRunPayrollWithDate: getRunPayrollWithDate,
    		getPayrollByEmp:getPayrollByEmp,
            addEmployeeLop : addEmployeeLop,
            getEmployeeLop : getEmployeeLop,
            updateEmployeeLopDetails : updateEmployeeLopDetails,
            cancelEmployeeLopDetails : cancelEmployeeLopDetails,
            addEmpBonusDetails : addEmpBonusDetails,
            getEmployeeBonusDetails : getEmployeeBonusDetails,
            updateEmployeeBonusDetails : updateEmployeeBonusDetails,
            cancelEmployeeBonusDetails : cancelEmployeeBonusDetails,
            addEmpTax : addEmpTax,
            editTax : editTax,
            getPayrollByEmpDate : getPayrollByEmpDate,
            getPayrollDetails : getPayrollDetails,
            getLeadByEmpEmpCompensation : getLeadByEmpEmpCompensation,
            empCompShowData : empCompShowData,
            getEmpCompensation : getEmpCompensation,
            getItSavingsByDate : getItSavingsByDate,
            addMyItSavings : addMyItSavings,
            getItSavings : getItSavings,
            uploadDocument : uploadDocument,
            getEmployeeItSavings : getEmployeeItSavings,
            updateEmployeeItSavingsVerified : updateEmployeeItSavingsVerified,
            submitForSummary : submitForSummary,
            declineForSummary : declineForSummary,
            addTaxSlabs : addTaxSlabs,
            getTaxSlabs : getTaxSlabs,
            editTaxSlabs : editTaxSlabs,
            getpayrollDetailByEmpId : getpayrollDetailByEmpId,
            getPTDetails: getPTDetails,
            getFYActiveDetails: getFYActiveDetails
    }
    return payrollServices;
    
    function getPayrollDetails(data) {  
    	data = {id: data};
       	return commonService.httpCallGetById(constants.payrollsummaryDetails, data);
    } 
    
    function getpayrollDetailByEmpId(empId){
       	return commonService.httpCallGetById(constants.getpayrollDetailById, empId);
    }
    
    function getItSavingsByDate(data) {
    	if(data){
	    	data = {
	    			fromDate: data.fromDate,
	    			toDate: data.toDate,
	    		};
	       	return commonService.httpCallGetById(constants.retrieveItSavings, data); 
    	}
    } 

    function addAllowance(data){
    	return commonService.apiCall(constants.payrollAllowancePersist, data);
    }
    
    function uploadDocument(data){
        return commonService.httpCallForUploadingImage(constants.itSavingsUploadDocument, data);
    }
    
    function addMyItSavings(data){
    	return commonService.apiCall(constants.itSavingsPersist, data);
    }

    function addEmpBonusDetails(data){
        return commonService.apiCall(constants.empBonusPersist, data);
    }

    function addEmployeeLop(data){
        console.log('dataaa===', data);
        return commonService.apiCall(constants.employeeLopPersist, data);
    } 
    
    function getPayrollSummary() {
        return commonService.httpCallGetAll(constants.payrollsummary);
    }
    
    function getItSavings(data) {
    	data = {id: data};
        return commonService.httpCallGetById(constants.itSavingsRetrieveById, data);
    }
    
    function addDeduction(data){
    	return commonService.apiCall(constants.payrollDeductionPersist, data);
    }

    function addEmpTax(data){
        return commonService.apiCall(constants.payrollTaxPersist, data);
    }

    function editAllowance(data){
    	return commonService.apiCall(constants.payrollAllowanceUpdate, data);
    }  
    
    function editDeduction(data){
    	return commonService.apiCall(constants.payrollDeductionUpdate, data);
    }

    function editTax(data){
        return commonService.apiCall(constants.payrollTaxUpdate, data);
    }
    
    function getLeadByEmp(data) {  
        	data = {id: data};
           	return commonService.httpCallGetById(constants.payrollgetLeadByEmpRetrieveById, data);
         
    } 

    function getLeadByEmpEmpCompensation(empId, date) {  
            data = {id: empId, paydate : date};
            return commonService.httpCallGetById(constants.payrollgetLeadByEmpRetrieveById, data);
         
    } 
    
    function getPayrollByEmp(data) {  
    	data = {id: data};
       	return commonService.httpCallGetById(constants.payrollgetEmpRetrieveById, data);
     
    } 

    function getPayrollByEmpDate(data, month) {  
        data = {id: data, monthly : month};
        console.log('dataa', data);
        return commonService.httpCallGetById(constants.payrollgetEmpDateRetrieveById, data);
     
    }

    function getEmpCompensation(data) {  
        data = {id: data};
        console.log('dataa', data);
        return commonService.httpCallGetById(constants.empCompensationRetrieveById, data);
     
    }

    function getEmployeeLop() {
        return commonService.httpCallGetAll(constants.employeeGetLopDetail);
     
    }

    function getEmployeeBonusDetails() {
        return commonService.httpCallGetAll(constants.employeeGetBonusDetail);
     
    }
    
    function viewAllEmployee(data) {        
    	data = {payrollMonth: data.payrollMonth, payrollBatchId: data.payrollBatchId, payrollType : data.payrollType, status : data.status};
       	return commonService.httpCallGetById(constants.payrollsummaryRetrieveAllEmployee, data);
    } 
    
    function getAllowance(data) {
    	if(!data){
        	return commonService.httpCallGetAll(constants.payrollAllowanceRetrieve);
        } else {
        	data = {id: data};
           	return commonService.httpCallGetById(constants.payrollAllowanceRetrieveById, data);
        }
    } 
    function  getAllowanceById(id,ctc) { 
        	data = {id: id,
        			empctc:ctc };
           	return commonService.apiCall(constants.payrollGetAllowanceById, data); 
    } 
  
    function  getDeductionById(id,ctc,monthly) {
    	if(id && ctc && monthly){
	    	data = {id: id,
	    			empctc: ctc,
	    			basicPay: monthly};
	       	return commonService.apiCall(constants.payrollGetDeductionById, data);
    	}
    } 

    function  getTaxById(id,ctc,monthly) {
        if(id && ctc && monthly){
            data = {id: id,
                    empctc: ctc,
                    basicPay: monthly};
            return commonService.apiCall(constants.payrollGetTaxById, data);
        }
    } 
     
    function getDeduction(data) {
    	if(!data){
        	return commonService.httpCallGetAll(constants.payrollDeductionRetrieve);
        } else {
        	data = {id: data};
           	return commonService.httpCallGetById(constants.payrollDeductionRetrieveById, data);
        }
    }

    function getTax(data) {
        if(!data){
            return commonService.httpCallGetAll(constants.payrollTaxRetrieve);
        } else {
            data = {id: data};
            return commonService.httpCallGetById(constants.payrollTaxRetrieveById, data);
        }
    }
    
    function getRunPayroll(data){
    	if(!data){
    		return commonService.httpCallGetAll(constants.runPayrollRetrieve);
        } else {
        	data = {id: data};
           	return commonService.httpCallGetById(constants.runPayrollRetrieve, data);
        }
    	
    }
    
    function getRunPayrollWithDate(data){
    	if(!data){
    		return commonService.httpCallGetAll(constants.runPayrollRetrieveWithDate);
        } else {
        	data = {id: data.payrollBatchId,monthly : data.payPeriod};
           	return commonService.httpCallGetById(constants.runPayrollRetrieveWithDateForStatus, data);
        }
    }
    
    function getRunPayrollMonthly(data, status){
    	console.log('stats',status)
    	if(status == 'monthly')
    		return commonService.httpCallGetById(constants.payrollProcessRetrieveByMonth, data);
    	else if(status == 'status')
    		return commonService.httpCallGetById(constants.payrollProcessRetrieveByStatus, data);
    }
  
    function updateEmployeeCompensation(data){
    	return commonService.apiCall(constants.empcompensation, data);
    }

    function updateEmployeeLopDetails(data){
        console.log('updateEmployeeLopDetails', data);
        return commonService.apiCall(constants.updateEmpLopDetails, data);
    }

    function updateEmployeeBonusDetails(data){
        return commonService.apiCall(constants.updateEmpBonusDetails, data);
    }

    function processPayroll(data){
    	return commonService.apiCall(constants.payrollProcess, data);
    }
    
    function submitPayroll(data){
    	return commonService.apiCall(constants.payrollProcessUpdate, data);
    }

    function cancelEmployeeLopDetails(data){
        return commonService.apiCall(constants.cancelLopEmpDetails, data);
    }

    function cancelEmployeeBonusDetails(data){
        return commonService.apiCall(constants.cancelBonusEmpDetails, data);
    }

    function empCompShowData() {
        return commonService.httpCallGetAll(constants.empCompensationShowData);
     
    }

    function getEmployeeItSavings() {
        return commonService.httpCallGetAll(constants.getEmployeeItSavingsRetrieve);
     
    }

    function updateEmployeeItSavingsVerified(data){
        console.log('updateEmployeeItSavingsVerified11service', data);
        return commonService.apiCall(constants.updateEmployeeItSavingsVerifiedPersist, data);
    }

    function submitForSummary(data){
        return commonService.apiCall(constants.payrollSummaryProcessUpdate, data);
    }

    function declineForSummary(data){
        return commonService.apiCall(constants.payrollSummaryDeclineUpdate, data);
    }

    function addTaxSlabs(data){
        return commonService.apiCall(constants.payrollTaxSlabsPersist, data);
    }

    function getTaxSlabs(data) {
        if(!data){
            return commonService.httpCallGetAll(constants.payrollTaxSlabsRetrieve);
        } else {
            data = {id: data};
            return commonService.httpCallGetById(constants.payrollTaxSlabsRetrieveById, data);
        }
    }

    function editTaxSlabs(data){
        return commonService.apiCall(constants.payrollTaxSlabsUpdate, data);
    }
    
    function getPTDetails(data) {  
    	data = {id: data};
       	return commonService.httpCallGetById(constants.PTDetailsByStateId, data);
     
    }
    
    function getFYActiveDetails(data) {  
    	data = {fromDate: data.fyFrom, toDate: data.fyTo};
       	return commonService.httpCallGetById(constants.FYRetrieveActivePayee, data);
     
    }
}