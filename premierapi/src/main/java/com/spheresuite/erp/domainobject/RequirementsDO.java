package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="requirements")
@TableGenerator(name ="requirements", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "requirements.findAll", query = "SELECT r FROM RequirementsDO r"),
    @NamedQuery(name = "requirements.findById", query = "SELECT r FROM RequirementsDO r where r.id =:id"),
    @NamedQuery(name = "requirements.findByStatus", query = "SELECT r FROM RequirementsDO r where r.status =:status"),
    @NamedQuery(name = "requirements.findByProjectId", query = "SELECT r FROM RequirementsDO r where r.projectid =:id"),
    @NamedQuery(name = "requirements.findByCustomerId", query = "SELECT r FROM RequirementsDO r where r.customerid =:id"),
})
public class RequirementsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "requirements.findAll";
	public static final String FIND_BY_ID = "requirements.findById";
	public static final String FIND_BY_STATUS = "requirements.findByStatus";
	public static final String FIND_BY_PROJECTID = "requirements.findByProjectId";
	public static final String FIND_BY_CUSTOMERID = "requirements.findByCustomerId";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "requirements")
	private Long id;
	private Long customerid;
	private Long projectid;
	private Long maxexp;
	private Long minexp;
	private Long cost;
	private String jobdesc;
	private String comments;
	private String noticeperiod;
	private String noofposition;
	private String joiningdate;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdby;
	private char status;
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerid() {
		return customerid;
	}
	public void setCustomerid(Long customerid) {
		this.customerid = customerid;
	}
	public Long getProjectid() {
		return projectid;
	}
	public void setProjectid(Long projectid) {
		this.projectid = projectid;
	}
	public Long getMaxexp() {
		return maxexp;
	}
	public void setMaxexp(Long maxexp) {
		this.maxexp = maxexp;
	}
	public Long getMinexp() {
		return minexp;
	}
	public void setMinexp(Long minexp) {
		this.minexp = minexp;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public String getJobdesc() {
		return jobdesc;
	}
	public void setJobdesc(String jobdesc) {
		this.jobdesc = jobdesc;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getNoticeperiod() {
		return noticeperiod;
	}
	public void setNoticeperiod(String noticeperiod) {
		this.noticeperiod = noticeperiod;
	}
	public String getNoofposition() {
		return noofposition;
	}
	public void setNoofposition(String noofposition) {
		this.noofposition = noofposition;
	}
	public String getJoiningdate() {
		return joiningdate;
	}
	public void setJoiningdate(String joiningdate) {
		this.joiningdate = joiningdate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}