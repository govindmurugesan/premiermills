package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user")
@TableGenerator(name ="user", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "UserDO.findAll", query = "SELECT u FROM UserDO u"),
    @NamedQuery(name = "UserDO.findById", query = "SELECT u FROM UserDO u where u.id =:userid and u.isDeleted=0"),
    //@NamedQuery(name = "UserDO.findByUserName", query = "SELECT u FROM UserDO u where u.name =:name  and  u.idDeleted=0"),
    @NamedQuery(name = "UserDO.findByStatus", query = "SELECT u FROM UserDO u where u.status =:status  and  u.isDeleted=0"),
    @NamedQuery(name = "UserDO.findByEmailId", query = "SELECT u FROM UserDO u where u.email =:email  and  u.isDeleted=0 and u.status = 'a'"),
    @NamedQuery(name = "UserDO.findForLogin", query = "SELECT u FROM UserDO u where u.email =:email  and  u.password=:password and u.isDeleted=0 and u.status =:status"),
    @NamedQuery(name = "UserDO.findBytempPassword", query = "SELECT u FROM UserDO u where u.temppassowrd =:temppassword  and  u.isDeleted=0 and u.email =:email"),
    @NamedQuery(name = "UserDO.findByEmailForEmp", query = "SELECT u FROM UserDO u where (u.email =:personal or u.email =:secondary) and  u.isDeleted=0"),
    @NamedQuery(name = "UserDO.findByEmpId", query = "SELECT u FROM UserDO u where u.empId =:userid and u.isDeleted=0"),
    @NamedQuery(name = "UserDO.findSuperAdmin", query = "SELECT u FROM UserDO u where u.status =:status  and  u.isDeleted=0 and u.roleId =:adminId"),
    
})
public class UserDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "UserDO.findAll";
	
	public static final String FIND_BY_ID = "UserDO.findById";
	
	public static final String FIND_BY_EMAIL = "UserDO.findByEmailId";
	
	public static final String FIND_BY_EMAIL_FOR_EMP = "UserDO.findByEmailForEmp";
	
	//public static final String FIND_BY_NAME = "UserDO.findByName";
	
	public static final String FIND_BY_STATUS = "UserDO.findByStatus";
	
	public static final String FIND_BY_USER_NAME = "UserDO.findByUserName";
	
	public static final String FIND_FOR_LOGIN = "UserDO.findForLogin";
	
	public static final String FIND_BY_TEMPPASSWORD = "UserDO.findBytempPassword";
	
	public static final String FIND_BY_EMP_ID = "UserDO.findByEmpId";
	
	public static final String FIND_SUPERADMIN = "UserDO.findSuperAdmin";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "user")
	private Long id;
	

	//private String name;

	private Long empId;

	

	private String email;

	private Long roleId;
	
	private char status;
	
	private String updatedby;
	
	private String temppassowrd;
	
	private String password;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private Long isDeleted;
	
	private String createdby;
	
	public UserDO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/*public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}*/

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getTemppassowrd() {
		return temppassowrd;
	}

	public void setTemppassowrd(String temppassowrd) {
		this.temppassowrd = temppassowrd;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Long isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
}