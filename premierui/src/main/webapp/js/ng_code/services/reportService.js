angular
    .module('spheresuite')
    .service('reportService', reportService);

reportService.$inject = ['constants', 'commonService'];

function reportService(constants, commonService) {

    var reportsService = {
        addreport: addreport,
        getreport: getreport,
        getActiveFinancialYear : getActiveFinancialYear,
        getActiveBatch : getActiveBatch,
        viewReport : viewReport,
        viewBonusReport : viewBonusReport,
        viewTaxReport : viewTaxReport,
        viewLopReport : viewLopReport,
        viewYtdReport : viewYtdReport,
        getDeduction : getDeduction,
        getTax : getTax
    };

    return reportsService;

    function addreport(data) { 
        return commonService.apiCall(constants.report, data);
    }

    function getreport(data) { 
        if(!data){
        	return commonService.httpCallGetAll(constants.report);
        }else{
        	data = {id: data};
        	return commonService.httpCallGetById(constants.report, data);
        }
    }

    function getActiveFinancialYear(){
         return commonService.httpCallGetAll(constants.configFYRetrieveActive);
    }

    function getActiveBatch() {
        return commonService.httpCallGetAll(constants.configPayrollBatchRetrieveActive);
    }

    function viewReport(data) {
        console.log('viewreport', data);
    return commonService.httpCallGetById(constants.viewReportPersist, data);
  }

  function viewTaxReport(data) {
        console.log('viewreport', data);
    return commonService.httpCallGetById(constants.viewTaxReportPersist, data);
  }

  function viewBonusReport(data) {
        console.log('viewreport', data);
    return commonService.httpCallGetById(constants.viewBonusReportPersist, data);
  }

  function viewLopReport(data) {
        console.log('viewreport', data);
    return commonService.httpCallGetById(constants.viewLopReportPersist, data);
  }

  function viewYtdReport(data) {
        console.log('viewreport', data);
    return commonService.httpCallGetById(constants.viewYtdReportPersist, data);
  }

  function getDeduction(data) {
        if(!data){
            return commonService.httpCallGetAll(constants.payrollDeductionRetrieve);
        } else {
            data = {id: data};
            return commonService.httpCallGetById(constants.payrollDeductionRetrieveById, data);
        }
    }

    function getTax(data) {
        if(!data){
            return commonService.httpCallGetAll(constants.payrollTaxRetrieve);
        } else {
            data = {id: data};
            return commonService.httpCallGetById(constants.payrollTaxRetrieveById, data);
        }
    }
}