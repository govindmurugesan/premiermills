package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.HrRequestsDAO;
import com.spheresuite.erp.domainobject.HrRequestsDO;
import com.spheresuite.erp.exception.AppException;

public class HrRequestsService {
	static Logger logger = Logger.getLogger(HrRequestsService.class.getName());
	
	public HrRequestsDO persist(HrRequestsDO hrRequestDetail) throws AppException {
		return new HrRequestsDAO().persist(hrRequestDetail);
	}
	
	public List<HrRequestsDO> retrieve() throws AppException {
		return new HrRequestsDAO().retrieve();
	}
	
	public List<HrRequestsDO> retrieveById(Long Id) throws AppException {
		return new HrRequestsDAO().retrieveById(Id);
	}
	
	public HrRequestsDO update(HrRequestsDO hrRequest) throws AppException {
		return new HrRequestsDAO().update(hrRequest);
	}
	
	public List<HrRequestsDO> retriveByEmpId(Long id) throws AppException {
		return new HrRequestsDAO().retrieveByEmpId(id);
	}
	
}
