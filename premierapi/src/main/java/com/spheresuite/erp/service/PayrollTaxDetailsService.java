package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.PayrollTaxDetailsDAO;
import com.spheresuite.erp.domainobject.PayrollTaxDetailsDO;
import com.spheresuite.erp.exception.AppException;

public class PayrollTaxDetailsService {
	static Logger logger = Logger.getLogger(PayrollTaxDetailsService.class.getName());
	
	public List<PayrollTaxDetailsDO> persist(List<PayrollTaxDetailsDO> payrollTaxDetailsDO) throws AppException {
		return new PayrollTaxDetailsDAO().persist(payrollTaxDetailsDO);
	}

	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		return new PayrollTaxDetailsDAO().retrieveByEmpCompensationId(id);
	}
	
	public List<PayrollTaxDetailsDO> retrieve() throws AppException {
		return new PayrollTaxDetailsDAO().retrieve();
	}
	
	public PayrollTaxDetailsDO update(PayrollTaxDetailsDO payrollTaxDetailsDO) throws AppException {
		return new PayrollTaxDetailsDAO().update(payrollTaxDetailsDO);
	}
	
	public List<PayrollTaxDetailsDO> retrieveByEmpCompensationIds(List<Long> id) throws AppException {
		return new PayrollTaxDetailsDAO().retrieveByEmpCompensationIds(id);
	}
}
