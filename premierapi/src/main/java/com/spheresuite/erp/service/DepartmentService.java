package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.DepartmentDAO;
import com.spheresuite.erp.domainobject.DepartmentDO;
import com.spheresuite.erp.exception.AppException;

public class DepartmentService {
	static Logger logger = Logger.getLogger(DepartmentService.class.getName());
	
	public DepartmentDO persist(DepartmentDO departmentDO) throws AppException {
		return new DepartmentDAO().persist(departmentDO);
	}

	public List<DepartmentDO> retrieveActive() throws AppException {
		return new DepartmentDAO().retrieveActive();
	}
	
	public List<DepartmentDO> retrieveById(Long Id) throws AppException {
		return new DepartmentDAO().retrieveById(Id);
	}
	
	public List<DepartmentDO> retrieve() throws AppException {
		return new DepartmentDAO().retrieve();
	}
	
	public DepartmentDO update(DepartmentDO departmentDO) throws AppException {
		return new DepartmentDAO().update(departmentDO);
	}
	
	public List<Long> retrieveisManager(Long Id) throws AppException {
		return new DepartmentDAO().retrieveisManager(Id);
	}
}
