package com.spheresuite.erp.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name="taxsetting")
@TableGenerator(name ="taxsetting", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "taxsetting.findAll", query = "SELECT r FROM TaxSettingsDO r"),
    @NamedQuery(name = "taxsetting.findById", query = "SELECT r FROM TaxSettingsDO r where r.id =:id"),
    @NamedQuery(name = "taxsetting.findActive", query = "SELECT r FROM TaxSettingsDO r where r.status =:status and r.name =:name"),
})
public class TaxSettingsDO implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final String FIND_ALL = "taxsetting.findAll";
	public static final String FIND_BY_ID = "taxsetting.findById";
	public static final String FIND_ACTIVE = "taxsetting.findActive";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "taxsetting")
	private Long id;
	private String name;
	private String section;
	private String updatedBy;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private Long createdby;
	private String preposttax;
	private String type;
	private String fixedamount;
	private String percentageamount;
	private String basicgrosspay;
	private String displayname;
	private String notes;
	@Temporal(TemporalType.DATE)
	private Date startdate;
	@Temporal(TemporalType.DATE)
	private Date enddate;
	private char status;
	@Transient
	private String settingName;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedon() {
		return updatedon;
	}
	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}
	public Long getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Long createdby) {
		this.createdby = createdby;
	}
	public String getPreposttax() {
		return preposttax;
	}
	public void setPreposttax(String preposttax) {
		this.preposttax = preposttax;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFixedamount() {
		return fixedamount;
	}
	public void setFixedamount(String fixedamount) {
		this.fixedamount = fixedamount;
	}
	public String getPercentageamount() {
		return percentageamount;
	}
	public void setPercentageamount(String percentageamount) {
		this.percentageamount = percentageamount;
	}
	public String getBasicgrosspay() {
		return basicgrosspay;
	}
	public void setBasicgrosspay(String basicgrosspay) {
		this.basicgrosspay = basicgrosspay;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getDisplayname() {
		return displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public char getStatus() {
		return status;
	}
	public void setStatus(char status) {
		this.status = status;
	}
	public String getSettingName() {
		return settingName;
	}
	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}
}