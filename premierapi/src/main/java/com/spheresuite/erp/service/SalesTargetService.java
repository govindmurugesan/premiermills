package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.SalesTargetDAO;
import com.spheresuite.erp.domainobject.SalesTargetDO;
import com.spheresuite.erp.exception.AppException;

public class SalesTargetService {
	static Logger logger = Logger.getLogger(SalesTargetService.class.getName());
	
	public SalesTargetDO persist(SalesTargetDO salesTargetDO) throws AppException {
		return new SalesTargetDAO().persist(salesTargetDO);
	}

	public List<SalesTargetDO> retriveById(Long id) throws AppException {
		return new SalesTargetDAO().retrieveById(id);
	}
	
	public List<SalesTargetDO> retrieveByEmpId(Long empId) throws AppException {
		return new SalesTargetDAO().retrieveByEmpId(empId);
	}
	
	public List<SalesTargetDO> retrieveByYear(Long fromDate, Long toDate) throws AppException {
		return new SalesTargetDAO().retrieveByYear(fromDate, toDate);
	}
	
	public List<SalesTargetDO> retrieveByYearForEmp(Long fromDate, Long toDate, Long empId) throws AppException {
		return new SalesTargetDAO().retrieveByYearForEmp(fromDate, toDate, empId);
	}
	
	public List<SalesTargetDO> retrieve(List<Long> id) throws AppException {
		return new SalesTargetDAO().retrieve(id);
	}
	
	public List<SalesTargetDO> retrieveAll() throws AppException {
		return new SalesTargetDAO().retrieveAll();
	}
	
	public SalesTargetDO update(SalesTargetDO salesTargetDO) throws AppException {
		return new SalesTargetDAO().update(salesTargetDO);
	}
}
