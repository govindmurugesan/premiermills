package com.spheresuite.erp.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spheresuite.erp.domainobject.CustomerDO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.service.EmployeeService;
import com.spheresuite.erp.util.CommonConstants;

public class CustomerUtil {
	
	private CustomerUtil() {}
	
	public static JSONObject getCustomerList(List<CustomerDO> customerList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CustomerDO customer : customerList) {
				resultJSONArray.put(getCustomerDetailObject(customer));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCustomerDetailObject(CustomerDO customer)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(customer.getId()));
		result.put(CommonConstants.NAME, String.valueOf(customer.getName()));
		result.put(CommonConstants.STATUS, String.valueOf(customer.getStatus()));
		result.put(CommonConstants.SOURCE, String.valueOf(customer.getSourceId()));
		result.put(CommonConstants.INDUSTRY, String.valueOf(customer.getIndustryId()));
		result.put(CommonConstants.MOBILE, String.valueOf(customer.getMobile()));
		result.put(CommonConstants.PHONE, String.valueOf(customer.getPone()));
		result.put(CommonConstants.EMAIL, String.valueOf(customer.getEmail()));
		result.put(CommonConstants.ADDRESS, String.valueOf(customer.getAddress()));
		result.put(CommonConstants.COMMENT, String.valueOf(customer.getComment()));
		result.put(CommonConstants.UPDATEDON, String.valueOf(CommonUtil.convertDateToStringWithdatetime(customer.getUpdatedon())));
		if(customer.getUpdatedby() != null){
			/*List<UserDO> userList = new UserService().retriveById(Long.parseLong(customer.getUpdatedby()));
			if(userList != null && userList.size() > 0){
				result.put(CommonConstants.UPDATED_BY, String.valueOf(userList.get(0).getName())); 
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}*/
			List<EmployeeDO> empList = new EmployeeService().retriveByEmpId(Long.parseLong(customer.getUpdatedby()));
			if(empList != null && empList.size() > 0){
				if(empList.get(0).getMiddlename() != null) result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " +empList.get(0).getMiddlename() + " " + empList.get(0).getLastname()));
				else result.put(CommonConstants.UPDATED_BY, String.valueOf(empList.get(0).getFirstname() + " " + empList.get(0).getLastname()));
			}else{
				result.put(CommonConstants.UPDATED_BY, ""); 
			}
			
		}else{
			result.put(CommonConstants.UPDATED_BY, ""); 
		}		return result;
	}
}
