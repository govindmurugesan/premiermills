package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.SalesPoliciesDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class SalesPoliciesDAO {
	static Logger logger = Logger.getLogger(SalesPoliciesDAO.class.getName());
	private EntityManager em = null;
	
	public SalesPoliciesDO persist(SalesPoliciesDO salesPoliciesList) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && salesPoliciesList != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
					em.persist(salesPoliciesList);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesPoliciesList;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SalesPoliciesDO> retrieve(List<Long> id) throws AppException {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesPoliciesDO.FIND_ALL);
				q.setParameter(CommonConstants.ID, id);
				salesPoliciesList = (List<SalesPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesPoliciesDO> retrieveAll() throws AppException {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesPoliciesDO.FIND_ALL_DATA);
				salesPoliciesList = (List<SalesPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesPoliciesList;
	}
	
	@SuppressWarnings("unchecked")
	public List<SalesPoliciesDO> retrieveById(Long Id) throws AppException {
		List<SalesPoliciesDO> salesPoliciesList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(SalesPoliciesDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				salesPoliciesList = (List<SalesPoliciesDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesPoliciesList;
	}
	
	public SalesPoliciesDO update(SalesPoliciesDO salesPoliciesDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(salesPoliciesDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return salesPoliciesDO;
	}

}
