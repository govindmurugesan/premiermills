package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.LeaveRequestsDAO;
import com.spheresuite.erp.domainobject.LeaveRequestsDO;
import com.spheresuite.erp.exception.AppException;

public class LeaveRequestsService {
	static Logger logger = Logger.getLogger(LeaveRequestsService.class.getName());
	
	public LeaveRequestsDO persist(LeaveRequestsDO leaveRequestDetail) throws AppException {
		return new LeaveRequestsDAO().persist(leaveRequestDetail);
	}
	
	public List<LeaveRequestsDO> retrieve() throws AppException {
		return new LeaveRequestsDAO().retrieve();
	}
	
	public List<LeaveRequestsDO> retrieveById(Long Id) throws AppException {
		return new LeaveRequestsDAO().retrieveById(Id);
	}
	
	public LeaveRequestsDO update(LeaveRequestsDO leaveRequest) throws AppException {
		return new LeaveRequestsDAO().update(leaveRequest);
	}
	
	public List<LeaveRequestsDO> retriveByEmpId(Long id) throws AppException {
		return new LeaveRequestsDAO().retrieveByEmpId(id);
	}
	
	public List<LeaveRequestsDO> retrieveAvailableLeavesByEmpId(Long id, String leaveTypeFromDate, String leaveTypeToDate, Long type) throws AppException {
		return new LeaveRequestsDAO().retrieveAvailableLeavesByEmpId(id, leaveTypeFromDate, leaveTypeToDate, type);
	}
	
	/*public List<LeaveRequestsDO> retrieveActive() throws AppException {
		return new LeaveRequestsDAO().retrieveActive();
	}
	*/
}
