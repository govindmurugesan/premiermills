package com.spheresuite.erp.rs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.BillsDO;
import com.spheresuite.erp.service.BillsService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.BillsUtil;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/bills")
public class BillsRS {

	String validation = null;
	static Logger logger = Logger.getLogger(BillsRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				BillsDO billsDO = new BillsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
			 			billsDO.setVendorId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
			 		}
			 		
			 		billsDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
			 		if(!inputJSON.get(CommonConstants.RATE).toString().isEmpty()){
			 			billsDO.setRate(Double.parseDouble(inputJSON.get(CommonConstants.RATE).toString()));
			 		}
			 		billsDO.setPaymentMode(!inputJSON.get(CommonConstants.PAYMENTMODE).toString().isEmpty()? inputJSON.get(CommonConstants.PAYMENTMODE).toString() : null);
			 		billsDO.setChequeNumbers(!inputJSON.get(CommonConstants.CHEQUENUMBER).toString().isEmpty()? inputJSON.get(CommonConstants.CHEQUENUMBER).toString() : null);
			 		billsDO.setBillNumber(!inputJSON.get(CommonConstants.BILLNUMBER).toString().isEmpty()? inputJSON.get(CommonConstants.BILLNUMBER).toString() : null);
			 		billsDO.setBillDate(!inputJSON.get(CommonConstants.BILLDATE).toString().isEmpty()? inputJSON.get(CommonConstants.BILLDATE).toString() : null);
			 		billsDO.setDueDate(!inputJSON.get(CommonConstants.DUEDATE).toString().isEmpty()? inputJSON.get(CommonConstants.DUEDATE).toString() : null);
			 		billsDO.setComments(!inputJSON.get(CommonConstants.DESC).toString().isEmpty()? inputJSON.get(CommonConstants.DESC).toString() : null);
			 		
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			billsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			billsDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		billsDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()? inputJSON.get(CommonConstants.STATUS).toString() : null);
			 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
			 			String status = inputJSON.get(CommonConstants.STATUS).toString();
			 			if(status.equalsIgnoreCase("Paid")){
			 				billsDO.setPaidDate(!inputJSON.get(CommonConstants.PAIDDATE).toString().isEmpty()? inputJSON.get(CommonConstants.PAIDDATE).toString() : null);
			 			}else{
			 				billsDO.setPaidDate(null);
			 			}
			 		}
			 		billsDO.setUpdatedon(new Date());
			 		new BillsService().persist(billsDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bills Created");
			 	}
				
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<BillsDO> proposalList = new BillsService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = BillsUtil.getBillsList(proposalList).toString();
				}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {

		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<BillsDO> proposalList = new BillsService().retrieve();
				respJSON = BillsUtil.getBillsList(proposalList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			if (WebManager.authenticateSession(request)) {
				BillsDO billsDO = new BillsDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		
			 		List<BillsDO> proposalList = new BillsService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		if(proposalList != null && proposalList.size() > 0){
			 			billsDO = proposalList.get(0);
				 		
			 			if(!inputJSON.get(CommonConstants.CUSTOMERID).toString().isEmpty()){
				 			billsDO.setVendorId(Long.parseLong(inputJSON.get(CommonConstants.CUSTOMERID).toString()));
				 		}
				 		
				 		billsDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty()? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
				 		if(!inputJSON.get(CommonConstants.RATE).toString().isEmpty()){
				 			billsDO.setRate(Double.parseDouble(inputJSON.get(CommonConstants.RATE).toString()));
				 		}
				 		
				 		billsDO.setBillNumber(!inputJSON.get(CommonConstants.BILLNUMBER).toString().isEmpty()? inputJSON.get(CommonConstants.BILLNUMBER).toString() : null);
				 		billsDO.setBillDate(!inputJSON.get(CommonConstants.BILLDATE).toString().isEmpty()? inputJSON.get(CommonConstants.BILLDATE).toString() : null);
				 		billsDO.setDueDate(!inputJSON.get(CommonConstants.DUEDATE).toString().isEmpty()? inputJSON.get(CommonConstants.DUEDATE).toString() : null);
				 		billsDO.setComments(!inputJSON.get(CommonConstants.DESC).toString().isEmpty()? inputJSON.get(CommonConstants.DESC).toString() : null);
				 		billsDO.setStatus(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()? inputJSON.get(CommonConstants.STATUS).toString() : null);
				 		billsDO.setPaymentMode(!inputJSON.get(CommonConstants.PAYMENTMODE).toString().isEmpty()? inputJSON.get(CommonConstants.PAYMENTMODE).toString() : null);
				 		billsDO.setChequeNumbers(!inputJSON.get(CommonConstants.CHEQUENUMBER).toString().isEmpty()? inputJSON.get(CommonConstants.CHEQUENUMBER).toString() : null);
				 		if(!inputJSON.get(CommonConstants.STATUS).toString().isEmpty()){
				 			String status = inputJSON.get(CommonConstants.STATUS).toString();
				 			if(status.equalsIgnoreCase("Paid")){
				 				billsDO.setPaidDate(!inputJSON.get(CommonConstants.PAIDDATE).toString().isEmpty()? inputJSON.get(CommonConstants.PAIDDATE).toString() : null);
				 			}else{
				 				billsDO.setPaidDate(null);
				 			}
				 		}
				 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
				 			billsDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
				 		}
				 		billsDO.setUpdatedon(new Date());
				 		new BillsService().update(billsDO);
				 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Bills Updated");
			 		}
			 	}

			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	
	@RequestMapping(value = "/importBills/{inputParams:.+}", method = RequestMethod.GET)
	public @ResponseBody String importEmp(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				Object updatedBy =  inputJSON.get(CommonConstants.UPDATED_BY); 
				org.json.simple.JSONArray fileData = CommonWebUtil.getInputParamsArray(inputJSON.get(CommonConstants.FILEDATA).toString());
				
				JSONObject colName = CommonWebUtil.getInputParams(inputJSON.get(CommonConstants.COLUMFIELDS).toString());
				List<BillsDO> billDOlist = new ArrayList<BillsDO>();
				
				for (int i=0; i < fileData.size(); i++){
					BillsDO billsDO = new BillsDO();
					JSONObject rowJSON = CommonWebUtil.getInputParams(fileData.get(i).toString());
					if(colName.get(CommonConstants.CUSTOMERID) != null){
						if(!rowJSON.get(colName.get(CommonConstants.CUSTOMERID)).toString().isEmpty()){
				 			billsDO.setVendorId(Long.parseLong(rowJSON.get(colName.get(CommonConstants.CUSTOMERID)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.CURRENCY_TYPE) != null){
						billsDO.setCurrencyType(!rowJSON.get(colName.get(CommonConstants.CURRENCY_TYPE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.CURRENCY_TYPE)).toString() : null);
					}
					if(colName.get(CommonConstants.RATE) != null){
				 		if(!rowJSON.get(colName.get(CommonConstants.RATE)).toString().isEmpty()){
				 			billsDO.setRate(Double.parseDouble(rowJSON.get(colName.get(CommonConstants.RATE)).toString()));
				 		}
					}
					if(colName.get(CommonConstants.BILLNUMBER) != null){
						billsDO.setBillNumber(!rowJSON.get(colName.get(CommonConstants.BILLNUMBER)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.BILLNUMBER)).toString() : null);
					}
					if(colName.get(CommonConstants.BILLDATE) != null){
						billsDO.setBillDate(!rowJSON.get(colName.get(CommonConstants.BILLDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.BILLDATE)).toString() : null);
					}
					if(colName.get(CommonConstants.DUEDATE) != null){
						billsDO.setDueDate(!rowJSON.get(colName.get(CommonConstants.DUEDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.DUEDATE)).toString() : null);
					}
					if(colName.get(CommonConstants.DESC) != null){
						billsDO.setComments(!rowJSON.get(colName.get(CommonConstants.DESC)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.DESC)).toString() : null);
					}
			 		if(updatedBy != null && ! updatedBy.toString().isEmpty()){
			 			billsDO.setUpdatedby(updatedBy.toString());
			 			billsDO.setEmpId(Long.parseLong(updatedBy.toString()));
			 		}
			 		if(colName.get(CommonConstants.STATUS) != null){
			 			billsDO.setStatus(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.STATUS)).toString() : null);
			 		}
			 		if(colName.get(CommonConstants.STATUS) != null){
			 			if(!rowJSON.get(colName.get(CommonConstants.STATUS)).toString().isEmpty()){
				 			String status = rowJSON.get(colName.get(CommonConstants.STATUS)).toString();
				 			if(colName.get(CommonConstants.PAIDDATE) != null){
					 			if(status.equalsIgnoreCase("Paid")){
					 				billsDO.setPaidDate(!rowJSON.get(colName.get(CommonConstants.PAIDDATE)).toString().isEmpty()? rowJSON.get(colName.get(CommonConstants.PAIDDATE)).toString() : null);
					 			}else{
					 				billsDO.setPaidDate(null);
					 			}
				 			}
			 			}
			 		}
			 		billsDO.setUpdatedon(new Date());
					billDOlist.add(billsDO);
				}
				
				new BillsService().persistList(billDOlist);
				
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getBillNumber", method = RequestMethod.GET)
	public @ResponseBody String getBillNumber(Model model, HttpServletRequest request) {
		String billNumber = null;
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<BillsDO> proposalList = new BillsService().retrieveBillNumber();
				if(proposalList.size() > 0){
					int number = Integer.parseInt(proposalList.get(0).getBillNumber().toString());
					billNumber = String.valueOf( number + 1);
				}else{
					int year = Calendar.getInstance().get(Calendar.YEAR);
					String billnumber = null;
					int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
				    if (month < 3) {
				         billnumber = String.valueOf(year-1).substring(2).concat(String.valueOf(year).substring(2)).concat("001");
				    } else {
				         billnumber = String.valueOf(year).substring(2).concat(String.valueOf(year+1).substring(2)).concat("001");
				    }
				    billNumber =   billnumber;
				}
				JSONObject responseJSON = new JSONObject();
				JSONObject resultJSON = new JSONObject();
				resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
				resultJSON.put(CommonConstants.ERRORS, "");
				resultJSON.put(CommonConstants.RESPONSE, billNumber);
				responseJSON.put(CommonConstants.RESPONSE, billNumber);
				respJSON = resultJSON.toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
}
