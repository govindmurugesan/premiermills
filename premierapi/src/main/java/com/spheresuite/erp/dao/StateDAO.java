package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.StateDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class StateDAO {
	static Logger logger = Logger.getLogger(StateDAO.class.getName());
	private EntityManager em = null;
	
	public StateDO persist(StateDO stateDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && stateDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(stateDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return stateDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieve() throws AppException {
		List<StateDO> countryList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(StateDO.FIND_ALL);
				countryList = (List<StateDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return countryList;
	}
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveActive() throws AppException {
		List<StateDO> stateList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(StateDO.FIND_BY_STATUS);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				stateList = (List<StateDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return stateList;
	}
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveById(Long stateId) throws AppException {
		List<StateDO> stateDO = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(StateDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, stateId);
				stateDO = (List<StateDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return stateDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<StateDO> retrieveByCountryId(Long countryId) throws AppException {
		List<StateDO> stateDO = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(StateDO.FIND_BY_COUNTRY_ID);
				q.setParameter(CommonConstants.COUNTRY_ID, countryId);
				stateDO = (List<StateDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return stateDO;
	}
	
	public StateDO update(StateDO stateDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(stateDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return stateDO;
	}

}
