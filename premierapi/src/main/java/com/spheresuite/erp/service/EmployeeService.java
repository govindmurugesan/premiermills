package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeDAO;
import com.spheresuite.erp.domainobject.EmployeeDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeService {
	static Logger logger = Logger.getLogger(EmployeeService.class.getName());
	
	public EmployeeDO persist(EmployeeDO employeeDO) throws AppException {
		return new EmployeeDAO().persist(employeeDO);
	}

	public List<EmployeeDO> retriveById(Long id) throws AppException {
		return new EmployeeDAO().retrieveById(id);
	}
	
	public List<EmployeeDO> retrieve() throws AppException {
		return new EmployeeDAO().retrieve();
	}
	
	public List<EmployeeDO> retrieveActiveNoCtc(List<Long> empIds) throws AppException {
		return new EmployeeDAO().retrieveActiveNoCtc(empIds);
	}
	
	public List<EmployeeDO> retrieveActive() throws AppException {
		return new EmployeeDAO().retrieveActive();
	}
	public List<EmployeeDO> isEmpActive(Long id) throws AppException {
		return new EmployeeDAO().isEmpActive(id);
	}
	public List<EmployeeDO> retrieveByEmail(String email) throws AppException {
		return new EmployeeDAO().retrieveByEmail(email);
	}
	public EmployeeDO update(EmployeeDO employeeDO) throws AppException {
		return new EmployeeDAO().update(employeeDO);
	}
	
	public List<EmployeeDO> retriveByEmpId(Long id) throws AppException {
		return new EmployeeDAO().retrieveByEmpId(id);
	}
	
	public String checkDuplicate(String empId, String companyEmail) throws AppException{
		return new EmployeeDAO().checkDuplicate(empId, companyEmail);
	}
	
	public String checkDuplicateUpdate(String empId, String companyEmail, Long Id) throws AppException{
		return new EmployeeDAO().checkDuplicateUpdate(empId, companyEmail, Id);
	}

	public List<Long> retrieveByDeptIds(List<Long> id) throws AppException {
		return new EmployeeDAO().retrieveByDeptIds(id);
	}

	public boolean persistList(List<EmployeeDO> empDO) throws AppException {
		return new EmployeeDAO().persistList(empDO);
	}

}
