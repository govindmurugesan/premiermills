package com.spheresuite.erp.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.spheresuite.erp.domainobject.PayrollDeductionDetailsDO;
import com.spheresuite.erp.exception.AppException;
import com.spheresuite.erp.exception.CustomPropertyManager;
import com.spheresuite.erp.exception.ExceptionConstant;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.util.PersistenceUnitNames;
import com.spheresuite.erp.util.SessionManager;
import com.spheresuite.erp.util.TransactionManager;

public class PayrollDeductionDetailsDAO {
	static Logger logger = Logger.getLogger(PayrollDeductionDetailsDAO.class.getName());
	private EntityManager em = null;
	
	public List<PayrollDeductionDetailsDO> persist(List<PayrollDeductionDetailsDO> payrollDeductionDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && payrollDeductionDetailsDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (PayrollDeductionDetailsDO payrollDeductionDetailsDO2 : payrollDeductionDetailsDO) {
					em.persist(payrollDeductionDetailsDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PayrollDeductionDetailsDO> retrieve() throws AppException {
		List<PayrollDeductionDetailsDO> payrollDeductionDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollDeductionDetailsDO.FIND_ALL);
				payrollDeductionDetailsList = (List<PayrollDeductionDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationId(Long id) throws AppException {
		List<PayrollDeductionDetailsDO> payrollDeductionDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollDeductionDetailsDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, id);
				payrollDeductionDetailsList = (List<PayrollDeductionDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsList;
	}
	
	public Double retrieveAllDeduction(Long id) throws AppException {
		Double payrollDeductionDetailsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollDeductionDetailsDO.FIND_ALL_DEDUCTION);
				q.setParameter(CommonConstants.ID, id);
				payrollDeductionDetailsList = (Double) q.getSingleResult();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsList;
	}
	public PayrollDeductionDetailsDO update(PayrollDeductionDetailsDO payrollDeductionDetailsDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(payrollDeductionDetailsDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return payrollDeductionDetailsDO;
	}

	@SuppressWarnings("unchecked")
	public List<PayrollDeductionDetailsDO> retrieveByEmpCompensationIds(List<Long> ids) throws AppException {
		List<PayrollDeductionDetailsDO> employeeEarningsList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(PayrollDeductionDetailsDO.FIND_BY_IDS);
				q.setParameter(CommonConstants.ID, ids);
				employeeEarningsList = (List<PayrollDeductionDetailsDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return employeeEarningsList;
	}
}
