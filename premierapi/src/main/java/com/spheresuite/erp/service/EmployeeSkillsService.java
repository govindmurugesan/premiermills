package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeSkillsDAO;
import com.spheresuite.erp.domainobject.EmployeeSkillsDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeSkillsService {
	static Logger logger = Logger.getLogger(EmployeeSkillsService.class.getName());
	
	public EmployeeSkillsDO persist(EmployeeSkillsDO employeeSkillsDO) throws AppException {
		return new EmployeeSkillsDAO().persist(employeeSkillsDO);
	}

	public List<EmployeeSkillsDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeSkillsDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeSkillsDO> retrieveById(Long id) throws AppException {
		return new EmployeeSkillsDAO().retrieveById(id);
	}
	
	public EmployeeSkillsDO update(EmployeeSkillsDO employeeSkillsDO) throws AppException {
		return new EmployeeSkillsDAO().update(employeeSkillsDO);
	}
}
