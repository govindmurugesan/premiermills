package com.spheresuite.erp.service;

import java.util.List;
import java.util.logging.Logger;

import com.spheresuite.erp.dao.EmployeeFamilyDAO;
import com.spheresuite.erp.domainobject.EmployeeFamilyDO;
import com.spheresuite.erp.exception.AppException;

public class EmployeeFamilyService {
	static Logger logger = Logger.getLogger(EmployeeFamilyService.class.getName());
	
	public EmployeeFamilyDO persist(EmployeeFamilyDO employeeFamilyDO) throws AppException {
		return new EmployeeFamilyDAO().persist(employeeFamilyDO);
	}

	public List<EmployeeFamilyDO> retrieveByEmpId(Long empId) throws AppException {
		return new EmployeeFamilyDAO().retrieveByEmpId(empId);
	}
	
	public List<EmployeeFamilyDO> retrieveById(Long id) throws AppException {
		return new EmployeeFamilyDAO().retrieveById(id);
	}
	
	public EmployeeFamilyDO update(EmployeeFamilyDO employeeFamilyDO) throws AppException {
		return new EmployeeFamilyDAO().update(employeeFamilyDO);
	}
}
