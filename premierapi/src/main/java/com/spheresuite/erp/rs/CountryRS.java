package com.spheresuite.erp.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spheresuite.erp.domainobject.CountryDO;
import com.spheresuite.erp.service.CountryService;
import com.spheresuite.erp.util.CommonConstants;
import com.spheresuite.erp.web.util.CommonUtil;
import com.spheresuite.erp.web.util.CommonWebUtil;
import com.spheresuite.erp.web.util.CountryUtil;
import com.spheresuite.erp.web.util.WebManager;

@Controller
@RequestMapping(value = "/country")
public class CountryRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CountryRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CountryDO countryDO = new CountryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null){
			 		countryDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		countryDO.setCode(!inputJSON.get(CommonConstants.CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CODE).toString() : null);
			 		countryDO.setCallingcode(!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null );
			 		countryDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			countryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 			countryDO.setEmpId(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()));
			 		}
			 		countryDO.setUpdatedon(new Date());
			 		countryDO.setStatus('a');
			 	}
				new CountryService().persist(countryDO);
				CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Country Created");
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	
	@RequestMapping(value = "/retrieveActive", method = RequestMethod.GET)
	public @ResponseBody String retrieveActive(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CountryDO> countryList = new CountryService().retrieveActive();
				respJSON = CountryUtil.getCountryList(countryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			if (WebManager.authenticateSession(request)) {
				List<CountryDO> countryList = new CountryService().retrieve();
				respJSON = CountryUtil.getCountryList(countryList).toString();
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(@PathVariable String inputParams,  Model model, HttpServletRequest request) {
			
		try {
			if (WebManager.authenticateSession(request)) {
				CountryDO countryDO = new CountryDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CountryDO> countryList = new CountryService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		countryDO = countryList.get(0);
			 		countryDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString() : null);
			 		countryDO.setCode(!inputJSON.get(CommonConstants.CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CODE).toString() : null);
			 		countryDO.setCallingcode(!inputJSON.get(CommonConstants.CALLING_CODE).toString().isEmpty() ? inputJSON.get(CommonConstants.CALLING_CODE).toString() : null );
			 		countryDO.setCurrencyType(!inputJSON.get(CommonConstants.CURRENCY_TYPE).toString().isEmpty() ? inputJSON.get(CommonConstants.CURRENCY_TYPE).toString() : null);
			 		countryDO.setUpdatedon(new Date());
			 		if(inputJSON.get(CommonConstants.UPDATED_BY).toString() != null && !inputJSON.get(CommonConstants.UPDATED_BY).toString().isEmpty()){
			 			countryDO.setUpdatedby(inputJSON.get(CommonConstants.UPDATED_BY).toString());
			 		}
			 		countryDO.setStatus((char) (inputJSON != null ? inputJSON.get(CommonConstants.STATUS).toString().charAt(0) : ""));
			 		new CountryService().update(countryDO);
			 		CommonUtil.persistRecentAcvtivity(Long.parseLong(inputJSON.get(CommonConstants.UPDATED_BY).toString()), "Country Updated");
			 	}
			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
