var app = angular.module('spheresuite')
app.controller('hrRequestController', hrRequestController);

hrRequestController.$inject = ['$scope', '$rootScope', '$localStorage', '$location', '$filter', 'commonService', 'configurationService', 'hrRequestService', 'Upload', 'employeeService'];

function hrRequestController($scope, $rootScope, $localStorage, $location, $filter, commonService, configurationService, hrRequestService, Upload, employeeService) {
    var hrRequestControllerScope = this;

    $rootScope.headerMenu = "HR Request";
    
    hrRequestControllerScope.showMenus = true;

    hrRequestControllerScope.hrRequest;
    hrRequestControllerScope.hrRequestForm;
    hrRequestControllerScope.hrRequestList = [];
    hrRequestControllerScope.hrRequestTypeList;
    hrRequestControllerScope.isAcceptRequest = false;
    hrRequestControllerScope.isUpdate = false;
    //hrRequestControllerScope.limitToShow = 10;
    hrRequestControllerScope.msg = '';
    hrRequestControllerScope.request;
    hrRequestControllerScope.requestForm;

    hrRequestControllerScope.acceptRequest = acceptRequest;
    hrRequestControllerScope.addMoreItems = addMoreItems;
    hrRequestControllerScope.createHrRequest = createHrRequest;
    hrRequestControllerScope.checkEmptyList = checkEmptyList;
    hrRequestControllerScope.decline = decline;
    hrRequestControllerScope.declineAccept = declineAccept;
    hrRequestControllerScope.editHrRequest = editHrRequest;
    hrRequestControllerScope.exportData = exportData;
    hrRequestControllerScope.getHrRequest = getHrRequest;
    hrRequestControllerScope.getHrRequestType = getHrRequestType;
    hrRequestControllerScope.gotoEdit = gotoEdit;
    hrRequestControllerScope.gotoMyRequests = gotoMyRequests;
    hrRequestControllerScope.gotoViewRequest = gotoViewRequest;
    hrRequestControllerScope.toggleAcceptRequest = toggleAcceptRequest;
    hrRequestControllerScope.showPrevNav = showPrevNav;
    hrRequestControllerScope.gotoPage = gotoPage;
    hrRequestControllerScope.isDataAvailable = false;
   
    hrRequestControllerScope.showNextNav = showNextNav;
    hrRequestControllerScope.isNextDisabled = false;

    $rootScope.limitToShow = 10;
    $rootScope.beginFrom = 0;

    hrRequestControllerScope.buttonBeginFrom = 0;
    hrRequestControllerScope.buttonLimitToShow = angular.copy($rootScope.limitToShow);
    
    if($location.path() == '/hrrequest/companiesrequest'){
    	delete $localStorage.spheresuite['hrRequestId'];
    	getHrRequest();
    }else if($location.path() == '/hrrequests'){
    	delete $localStorage.spheresuite['hrRequestId'];
    	getHrRequest($localStorage.spheresuite.id);
    }else if($location.path() == '/hrrequest/edit'){
        hrRequestControllerScope.isUpdate = true;
        getHrRequestType();
    	getHrRequest($localStorage.spheresuite.hrRequestId, 'edit');
    }else if($location.path() == '/hrrequest/view' || $location.path() == '/hrrequest/companiesrequest/view'){
    	getHrRequest($localStorage.spheresuite.hrRequestId, 'view');
    }else if($location.path() == '/hrrequest/add'){
        getHrRequestType();
        getReportingDetails();
    	delete $localStorage.spheresuite['hrRequestId'];
    }
    
    function acceptRequest(){
    	if(hrRequestControllerScope.hrRequest){
    		hrRequestControllerScope.spinner = true;
	        commonService.formValNotManditory(hrRequestControllerScope.requestForm, hrRequestControllerScope.hrRequest).then(function(data) {
	        	if(data){
	        		data.updatedBy = $localStorage.spheresuite.id;
	        		hrRequestService.acceptRequest(data).then(function(res){
		        		hrRequestControllerScope.spinner = false;
		        		if(res.successflag == 'true'){
	        				declineAccept();
	        			}
	        		},function(err){
		        		hrRequestControllerScope.spinner = false;
	        		})
	        	}else{
	        		hrRequestControllerScope.spinner = false;
	        	}
	        },function(err){
	    		hrRequestControllerScope.spinner = false;
	        });
    	}
    }
    
    function addMoreItems(){
        hrRequestControllerScope.limitToShow += 5;
    }
    
    /*function createHrRequest(){
    	if(hrRequestControllerScope.hrRequest){
	        hrRequestControllerScope.spinner = true;
	        commonService.formValNotManditory(hrRequestControllerScope.hrRequestForm, hrRequestControllerScope.hrRequest).then(function(data) {
	        	if(data){
	        		data.updatedBy = $localStorage.spheresuite.id;
	        		data.status = 's';
	        		hrRequestService.createHrRequest(data).then(function(res){
	        			if(res.successflag == 'true'){
	            	        hrRequestControllerScope.spinner = false;
	        				decline();
	        				$location.path('/hrrequests');
	        			}else{
	        				hrRequestControllerScope.spinner = false;
	        			}
	        		},function(err){
	        	        hrRequestControllerScope.spinner = false;
	        		})
	        	}else{
	        		hrRequestControllerScope.spinner = false;
	        		}
	        });
    	}
    }*/

    var filesName = [];
    function createHrRequest() {
        if (hrRequestControllerScope.hrRequest) {
            hrRequestControllerScope.spinner = true;
            commonService.formValNotManditory(hrRequestControllerScope.hrRequestForm, hrRequestControllerScope.hrRequest).then(function (data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    data.status = 's';
                   if(hrRequestControllerScope.reportingList.deptHeadEmail) data.deptHeadEmail = hrRequestControllerScope.reportingList.deptHeadEmail;
                   else data.deptHeadEmail =  "";
                   if(hrRequestControllerScope.reportingList.reportingEmail) data.reportingEmail = hrRequestControllerScope.reportingList.reportingEmail;
                   else data.reportingEmail =  "";
                   if(hrRequestControllerScope.reportingList.reportToName) data.reportToName = hrRequestControllerScope.reportingList.reportToName;
                   else data.reportToName =  "";
                   if(hrRequestControllerScope.reportingList.deptMngrName) data.deptMngrName = hrRequestControllerScope.reportingList.deptMngrName;
                   else data.deptMngrName =  "";
                   data.comment =  hrRequestControllerScope.hrRequest.desc;
                    data.requestBy =  $localStorage.spheresuite.name;
                    console.log('data setting', data)
                    if(data.file && data.file != ""){
                        for(i in data.file){
                            console.log('i name',data.file[i].name)
                            filesName.push(data.file[i].name)
                        }
                        console.log('files',filesName)
                        Upload
                        .base64DataUrl(data.file)
                        .then(function(urls) {
                            data.file = '';
                            itSavingFiles(data,urls);
                        });
                    }else{
                        data.file = "";
                        itSavingFiles(data);
                    }  
                    /*payrollService.addMyItSavings(data).then(function (res) {
                        console.log('addMyItSavings res', res)
                        if (res.successflag == 'true') {
                            
                            declineMyItSavings();
                            getItSavings($localStorage.spheresuite.id);
                            $('#itsavings').modal('hide');
                        }
                        hrRequestControllerScope.spinner = false;
                    }, function (err) {
                        hrRequestControllerScope.spinner = false;
                    });*/
                } else {
                    hrRequestControllerScope.spinner = false;
                }
            });
        }
    }
    
    function itSavingFiles(data,files){
        hrRequestService.createHrRequest(data).then(function (res) {
            console.log('addOpportunity res',res);
            if (res.successflag === 'true' && res.results) {
                if(files && res.results != ''){
                    for(var i = 0; i < files.length; i++){
                        hrRequestService.uploadDocument({id: res.results, photo: files[i], name: filesName[i]}).then(function(res1){
                            console.log('res1',i, files.length)
                            if(i == files.length){
                                decline(); 
                              getHrRequest($localStorage.spheresuite.id);                       
                            hrRequestControllerScope.spinner = false;
                            }
                        },function(err){
                            console.log('err1',err1);
                        });
                    }
                }else{
                    hrRequestControllerScope.spinner = false;  
                    decline();
                    getHrRequest($localStorage.spheresuite.id);
                }
            } else {
                hrRequestControllerScope.spinner = false;
            }
        }, function(err) {
            hrRequestControllerScope.spinner = false;
        });
    }



    
    function checkEmptyList(){
    	hrRequestControllerScope.msg = '';
    	if(hrRequestControllerScope.search && hrRequestControllerScope.hrRequestList){
    		var len = ($filter('filter')(hrRequestControllerScope.hrRequestList, hrRequestControllerScope.search)).length;
    			
    		if(len == 0)
    			hrRequestControllerScope.msg = 'HR Requests Not Available';
    	}
    }

    function decline(){
    	if(hrRequestControllerScope.isUpdate){
			$location.path('/hrrequests');
    	}else{
	        hrRequestControllerScope.hrRequest = null;
	        hrRequestControllerScope.hrRequestForm.$setPristine();
	        hrRequestControllerScope.hrRequestForm.$setUntouched();
    	}
    }
    
    function declineAccept(){
        console.log('$localStorage.spheresuite.hrRequestId  ', $localStorage.spheresuite.hrRequestId)
        if($location.path() == '/hrrequest/companiesrequest/view'){
            hrRequestControllerScope.hrRequest = null;
            getHrRequest($localStorage.spheresuite.hrRequestId, 'view');
        }
        hrRequestControllerScope.requestForm.$setPristine();
        hrRequestControllerScope.requestForm.$setUntouched();
        toggleAcceptRequest();
    }
    
    /*function editHrRequest(){
    	if(hrRequestControllerScope.hrRequest){
	        hrRequestControllerScope.spinner = true;
	        commonService.formValNotManditory(hrRequestControllerScope.requestForm, hrRequestControllerScope.hrRequest).then(function (data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    //data.status = 's';
                    console.log('data setting', data)
                    if(data.file && data.file != ""){
                        for(i in data.file){
                            console.log('i name',data.file[i].name)
                            filesName.push(data.file[i].name)
                        }
                        console.log('files',filesName)
                        Upload
                        .base64DataUrl(data.file)
                        .then(function(urls) {
                            data.file = '';
                            itSavingEditFiles(data,urls);
                        });
                    }else{
                        data.file = "";
                        itSavingEditFiles(data);
                    } 
                } else {
                    hrRequestControllerScope.spinner = false;
                }
            });
    	}    	
    }*/

    function editHrRequest(){
        if(hrRequestControllerScope.hrRequest){
            hrRequestControllerScope.spinner = true;
            /*commonService.formValNotManditory(hrPolicyControllerScope.hrPolicyForm, hrPolicyControllerScope.hrPolicy).then(function (data) {
                if (data) {
                    data.updatedBy = $localStorage.spheresuite.id;
                    //data.status = 's';
                    console.log('data setting', data)
                    if(data.file && data.file != ""){
                        for(i in data.file){
                            console.log('i name',data.file[i].name)
                            filesName.push(data.file[i].name)
                        }
                        console.log('files',data)
                        Upload
                        .base64DataUrl(data.file)
                        .then(function(urls) {
                            data.file = '';
                            itSavingEditFiles(data,urls);
                        });
                    }else{
                        data.file = "";
                        itSavingEditFiles(data);
                    } 
                    $('#hrPolicy').modal('hide');
                } else {
                    hrPolicyControllerScope.spinner = false;
                    $('#hrPolicy').modal('hide');
                }
            });*/
            console.log('hrRequestControllerScope.hrRequest.file ',hrRequestControllerScope.hrRequest)
            if(hrRequestControllerScope.hrRequest.file && hrRequestControllerScope.hrRequest.file != ""){
                
                console.log('hrRequestControllerScope.hrRequest.file',hrRequestControllerScope.hrRequest.file)
                for(i in hrRequestControllerScope.hrRequest.fromEditFile){
                    filesName.push(hrRequestControllerScope.hrRequest.fromEditFile[i].name)
                }
                for(i in hrRequestControllerScope.hrRequest.file){
                    console.log('i name',hrRequestControllerScope.hrRequest.file[i].name)
                    filesName.push(hrRequestControllerScope.hrRequest.file[i].name)
                }
                console.log('files ',filesName)
                Upload
                .base64DataUrl(hrRequestControllerScope.hrRequest.file)
                .then(function(urls) {
                    hrRequestControllerScope.hrRequest.file = '';

                    for(i in hrRequestControllerScope.hrRequest.fromEditFile){
                        urls.push(hrRequestControllerScope.hrRequest.fromEditFile[i].file)
                    }
                    console.log('urls',urls)
                    itSavingEditFiles(hrRequestControllerScope.hrRequest,urls);
                });
            }else{
                itSavingEditFiles(hrRequestControllerScope.hrRequest,hrRequestControllerScope.hrRequest.fromEditFile);
            }
        }       
    }

    /*function itSavingEditFiles(data,files){
        hrRequestService.editHrRequest(data).then(function (res) {
            console.log('addOpportunity res',res);
            if (res.successflag === 'true' && res.results) {
                if(files && res.results != ''){
                    for(var i = 0; i < files.length; i++){
                        hrRequestService.uploadDocument({id: data.id, photo: files[i], name: filesName[i]}).then(function(res1){
                            console.log('res1',i, files.length)
                            if(i == files.length){
                                hrRequestControllerScope.spinner = false;
                    if($location.path() == '/hrrequest/companiesrequest/view'){
                        $location.path('/hrrequest/companiesrequest');
                    }else{
                        //decline();
                    }
                            }
                        },function(err){
                            console.log('err1',err1);
                        });
                    }
                }else{
                    hrRequestControllerScope.spinner = false;  
                }
            } else {
                hrRequestControllerScope.spinner = false;
            }
        }, function(err) {
            hrRequestControllerScope.spinner = false;
        });
    }*/

    function itSavingEditFiles(data,files){
        data.fromEditFile = '';
        data.updatedBy = $localStorage.spheresuite.id;
        hrRequestService.editHrRequest(data).then(function(res) {
            hrRequestControllerScope.spinner = true;
            if (res.successflag === 'true') {
                if(files){
                    if(files == ''|| files.length == 0){
                        hrRequestControllerScope.spinner = false;
                        $location.path('/hrrequest/companiesrequest');
                        
                    }else{
                        for(var i = 0; i < files.length; i++){
                            var fileName = filesName.length > 0?filesName[i]:files[i].name;
                            var fileData = (files[i].file!="" && files[i].file != undefined)?files[i].file:files[i];
                            console.log('fileData   ',fileData)
                            console.log('fileName   ',fileName)
                            console.log('id   ',data.id)
                            hrRequestService.uploadDocument({id: data.id, photo: fileData, name: fileName}).then(function(res1){
                                console.log('res1 ',files.length)
                                if(i == files.length){
                                    hrRequestControllerScope.spinner = false;
                                    getHrRequest();
                                    $location.path('/hrrequest/companiesrequest');
                                }
                            },function(err){
                                console.log('err1',err1);
                                hrRequestControllerScope.spinner = false;
                            });
                        }
                    }
                }else{
                    hrRequestControllerScope.spinner = false;
                    getHrRequest();
                    $location.path('/hrrequest/companiesrequest');
                }
            }else{
                hrRequestControllerScope.spinner = false;
                    getHrRequest();
                    $location.path('/hrrequest/companiesrequest');
            }
        }, function(err) {
            hrRequestControllerScope.spinner = false;
        });
    }


    hrRequestControllerScope.fromEditFile = [];
    function getHrRequest(data, type){
        console.log('hereprint');
		hrRequestControllerScope.msg = '';
        hrRequestControllerScope.getSpinner = true;
        hrRequestService.getHrRequest(data, type).then(function(res){

        	if(res.successflag == 'true' && res.results.length > 0){
                hrRequestControllerScope.isDataAvailable = true;
        		if(data && type && type == 'edit' || type == 'view'){
        			if($location.path() == '/hrrequest/edit' || $location.path() == '/hrrequest/view' || $location.path() == '/hrrequest/companiesrequest/view' ){
        				hrRequestControllerScope.hrRequest = res.results[0];
                        hrRequestControllerScope.fromEditFile = hrRequestControllerScope.hrRequest.file;
                        console.log('getHrRequest', hrRequestControllerScope.hrRequest);
        			}
        		}else {
        			hrRequestControllerScope.hrRequestList = res.results;
                    hrRequestControllerScope.hrRequestListBackup = angular.copy(hrRequestControllerScope.hrRequestList);
                    hrRequestControllerScope.isDataAvailable = true;

                    for(var i = 0; i < hrRequestControllerScope.hrRequestListBackup.length; i++){
                        hrRequestControllerScope.hrRequestListBackup[i].isActiveClass = false;
                        hrRequestControllerScope.hrRequestListBackup[i].isSelect = false;
                    }
                    hrRequestControllerScope.hrRequestList = angular.copy(hrRequestControllerScope.hrRequestListBackup);
                    if((hrRequestControllerScope.buttonBeginFrom + hrRequestControllerScope.buttonLimitToShow) * hrRequestControllerScope.buttonLimitToShow >= hrRequestControllerScope.hrRequestList.length)
                        hrRequestControllerScope.isNextDisabled = true;
                    hrRequestControllerScope.hrRequestList[0].isActiveClass = true;
                    console.log('hrRequestControllerScope.payrollsummary 00000 ',hrRequestControllerScope.hrRequestList )
        		}
        		hrRequestControllerScope.getSpinner = false;
        	}else{
				hrRequestControllerScope.msg = 'HR Requests Not Available';
        		hrRequestControllerScope.getSpinner = false;
        	}
        },function(err){
            hrRequestControllerScope.getSpinner = false;
        });
    }
    
    function getHrRequestType(){
        hrRequestControllerScope.spinner = true;
    	configurationService.gethrRequestType().then(function(res){
    		if(res.successflag == 'true' && res.results.length > 0){
    			hrRequestControllerScope.hrRequestTypeList = res.results; 
    		}
    		hrRequestControllerScope.spinner = false;
    	},function(err){
    		hrRequestControllerScope.spinner = false;
    	});
    }
    
    function gotoEdit(data){

        hrRequestControllerScope.hrRequest = data;
        hrRequestControllerScope.fromEditFile = data.file;
    	$location.path('/hrrequest/edit');
    }

    function gotoMyRequests(){
    	if($location.path() == '/hrrequest/companiesrequest/view'){
    		$location.path('/hrrequest/companiesrequest');
    	}else if($location.path() == '/hrrequest/view'){
    		$location.path('/hrrequests');
    	}
    }
    
    function gotoViewRequest(id){
    	if(id){
    		$localStorage.spheresuite.hrRequestId = id;
        	if($location.path() == '/hrrequests'){
    			$location.path('/hrrequest/view');
    		}else if($location.path() == '/hrrequest/companiesrequest'){
        		$location.path('/hrrequest/companiesrequest/view');
        	}
    	}
    }
    
    function toggleAcceptRequest(){
    	hrRequestControllerScope.isAcceptRequest = !hrRequestControllerScope.isAcceptRequest;
    	hrRequestControllerScope.showMenus = !hrRequestControllerScope.showMenus;
    }
    

    function exportData() {
        $scope.fileName = "HR Request";
        $scope.exportData = []; 
        $scope.exportData.push(["Id", "Created By", "Created On", "Status", "Type", "Type Name", "Desc", "Comment", "Updatedon", "UpdatedBy"]);
        $scope.Filterdata = hrRequestControllerScope.hrRequestList;
        var firstFiter = $filter('filter')(hrRequestControllerScope.hrRequestList, { status: hrRequestControllerScope.status });
        $scope.Filterdata = $filter('filter')(firstFiter, hrRequestControllerScope.search);
        angular.forEach($scope.Filterdata, function(value, key) {
            $scope.exportData.push([value.id, value.createdBy, value.createdon, value.status, value.type, value.typeName, value.desc, value.comment, value.updatedon, value.updatedBy]);
        });
        function datenum(v, date1904) {
            if (date1904) v += 1462;
            var epoch = Date.parse(v);
            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
        };

        function getSheet(data, opts) {
            var ws = {};
            var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
            for (var R = 0; R != data.length; ++R) {
                for (var C = 0; C != data[R].length; ++C) {
                    if (range.s.r > R) range.s.r = R;
                    if (range.s.c > C) range.s.c = C;
                    if (range.e.r < R) range.e.r = R;
                    if (range.e.c < C) range.e.c = C;
                    var cell = { v: data[R][C] };
                    if (cell.v == null) continue;
                    var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                    if (typeof cell.v === 'number') cell.t = 'n';
                    else if (typeof cell.v === 'boolean') cell.t = 'b';
                    else if (cell.v instanceof Date) {
                        cell.t = 'n';
                        cell.z = XLSX.SSF._table[14];
                        cell.v = datenum(cell.v);
                    } else cell.t = 's';

                    ws[cell_ref] = cell;
                }
            }
            if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
            return ws;
        };

        function Workbook() {
            if (!(this instanceof Workbook)) return new Workbook();
            this.SheetNames = [];
            this.Sheets = {};
        }

        var wb = new Workbook(),
            ws = getSheet($scope.exportData); 
        wb.SheetNames.push($scope.fileName);
        wb.Sheets[$scope.fileName] = ws;
        var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

        function s2ab(s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        }

        saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), $scope.fileName + '.xlsx');

    }

    function showPrevNav(){
        hrRequestControllerScope.buttonBeginFrom -= hrRequestControllerScope.buttonLimitToShow;
        gotoPage(hrRequestControllerScope.buttonBeginFrom)
        hrRequestControllerScope.isNextDisabled = false;
    }

    function showNextNav(){
      hrRequestControllerScope.buttonBeginFrom += hrRequestControllerScope.buttonLimitToShow;
        gotoPage(hrRequestControllerScope.buttonBeginFrom)
        if((hrRequestControllerScope.buttonBeginFrom + hrRequestControllerScope.buttonLimitToShow) * hrRequestControllerScope.buttonLimitToShow >= hrRequestControllerScope.hrRequestList.length)
            hrRequestControllerScope.isNextDisabled = true;  
    }

    function gotoPage(index, activeIndex){
        $rootScope.beginFrom = index;
        for(var i = 0; i < hrRequestControllerScope.hrRequestList.length; i++){
            console.log(' data[i].isActiveClass', hrRequestControllerScope.hrRequestList[i].isActiveClass);
            console.log(' hrRequestList', hrRequestControllerScope.hrRequestList);
            hrRequestControllerScope.hrRequestList[i].isActiveClass = false;
        }
        if(!activeIndex)
            activeIndex = index        
        console.log('index',index, hrRequestControllerScope.hrRequestList[index].isActiveClass);
        return hrRequestControllerScope.hrRequestList[activeIndex].isActiveClass = true;
    }

    function getReportingDetails(){
        hrRequestControllerScope.spinner = true;
        var userId = $localStorage.spheresuite.id;
        employeeService.getReportingDetails(userId).then(function(res) {
               if (res.successflag === 'true' && res.results.length > 0) {
            	   hrRequestControllerScope.isReportingMangager = true;
            	   hrRequestControllerScope.reportingList = res.results[0];
            	   hrRequestControllerScope.spinner = false;
                } else {
                	hrRequestControllerScope.spinner = false; 
                }
        }, function(err) {
        	hrRequestControllerScope.spinner = false;
        });
    }
        
    $('.select1').select2();
}